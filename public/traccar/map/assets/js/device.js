var markers={};
var hash='';


var timer;

var jmarkers=[];

var tpos=0;

$(function() {


$('#animate').on('click',function() {
  reset_map();
});

fetch_locations();

});





function reset_map() {
  try {
  //attempt resetting the map
  if(jmarkers) {
    for(var i=0; i< jmarkers.length; i++)
    {
      jmarkers[i].setMap(null);
    }
  }
    tpos++; //stop plotting
    poly.setPath([]);
  } catch(e) {}

  initialize();
}

function fetch_locations() {
  var request = $.ajax({
    url: "../device.php",
    type: "POST",
    data: {device : device,from:from,to:to},
    cache: false,
    dataType: "json"
  });

  request.done(function(json) {
    if(json.hash!=hash) {
      hash=json.hash;
      markers=json.markers; //update markers
      reset_map(); //reset map
    }
  });
  request.fail(function(jqXHR, textStatus) {
    //alert( "Request failed: " + textStatus );
  });
  request.complete(function() {
    //alert("Complete");

    setTimeout(function() {
      fetch_locations();
    },3000);
  });

}



function animatePath(map, route, marker, pathCoords) {
  tpos++;

  cpos=tpos;

  var index = 0;
  route.setPath([]);
  for (var index = 0; index < pathCoords.length; index++)
    timer=setTimeout(function(offset) {
      if(tpos!=cpos) {return;}
      route.getPath().push(pathCoords.getAt(offset));
      marker.setPosition(pathCoords.getAt(offset));
      map.panTo(pathCoords.getAt(offset));
    }, index * 30, index);
}

var map;
var mapOptions;
var poly;
var traceMarker;
var path;

function initialize() {

  mapOptions = {
    center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
    zoom: 20,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };

  map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);

  var infoWindow = new google.maps.InfoWindow();
  var latlngbounds = new google.maps.LatLngBounds();
  for (i = 0; i < markers.length; i++) {
    var data = markers[i];
    markers[i].latLng = new google.maps.LatLng(data.lat, data.lng);
    var marker = new google.maps.Marker({
      position: markers[i].latLng,
      map: map
      //title: data.title
    });

    jmarkers.push(marker);

    //marker.description = markers[i].description;
    latlngbounds.extend(marker.position);

    /*
    google.maps.event.addListener(marker, "click", function(e) {
      infoWindow.setContent(this.description);
      infoWindow.open(map, this);
    });
    */


  }
  map.setCenter(latlngbounds.getCenter());
  map.fitBounds(latlngbounds);

  //Initialize the Path Array
  path = new google.maps.MVCArray();

  //Initialize the Direction Service
  var service = new google.maps.DirectionsService();

  // Get the route between the points on the map
  var wayPoints = [];
  for (var i = 1; i < markers.length - 1; i++) {
    wayPoints.push({
      location: markers[i].latLng,
      stopover: false
    });
  }


  //Initialize the path
  poly = new google.maps.Polyline({
    map: map,
    strokeColor: '#4986E7'
  });
  traceMarker = new google.maps.Marker({
    map: map,
    icon: "http://maps.google.com/mapfiles/ms/micons/blue.png"
  });

  if (markers.length >= 2) {
    service.route({
      origin: markers[0].latLng,
      destination: markers[markers.length - 1].latLng,
      waypoints: wayPoints,
      optimizeWaypoints: false,
      travelMode: google.maps.DirectionsTravelMode.DRIVING
    }, function(result, status) {
      if (status == google.maps.DirectionsStatus.OK) {
        for (var j = 0, len = result.routes[0].overview_path.length; j < len; j++) {
          path.push(result.routes[0].overview_path[j]);
        }
        animatePath(map, poly, traceMarker, path);
      }
    });
  }

  /*
  document.getElementById("animate").addEventListener("click", function() {
    // Animate the path when the button is clicked
    animatePath(map, poly, traceMarker, path);
  });
  */

}
