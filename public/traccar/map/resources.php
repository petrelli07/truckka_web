
<!DOCTYPE html>
<html>
  <head>
    <title>Resources Locations</title>
    <meta name="viewport" content="initial-scale=1.0">
    <meta charset="utf-8">
    <link href="assets/dist/css/bootstrap.min.css" rel="stylesheet">

    <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map_canvas {
        height: 100%;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
    </style>
  </head>
  <body>

    <script type="text/javascript">
    var params="<?= $_GET['params'] ?>";
    </script>

    <button id="animate" class="hide btn btn-warning" style="position: absolute;z-index:1000;left:200px;top:10px;">Restart Animation</button>

    <div id="map_canvas"></div>

    <script src="assets/dist/js/jquery.min.js"></script>

    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyCd0QhvdM_Fe5WMVPtPO3jEx09E5VYvgyI"></script>

    <script src="assets/js/resources.js?v=<?= time() ?>"></script>

  </body>
</html>
