<?php
die();
?>

This can be accessed this way
http://truckka.dev/traccar/map/resources.php?params=W3siaWQiOjEsIm5hbWUiOiJkZmdoIiwibGFiZWwiOiJnaGtsIiwiZ3BzaWQiOiI4NjQ4OTUwMzAxNTk2NDIifSx7ImlkIjoyLCJuYW1lIjoiSm9obiBMb2JpIiwibGFiZWwiOiJCYWdzIG9mIFN1Z2FyIiwiZ3BzaWQiOiI4NjQ4OTUwMzAxNTk3NjYifSx7ImlkIjozLCJuYW1lIjoiZGZnaCIsImxhYmVsIjoiZ2hrbCIsImdwc2lkIjoiODY0ODk1MDMwMTU5MzUyIn1d
or
http://truckka.com.ng/traccar/map/resources.php?params=W3siaWQiOjEsIm5hbWUiOiJkZmdoIiwibGFiZWwiOiJnaGtsIiwiZ3BzaWQiOiI4NjQ4OTUwMzAxNTk2NDIifSx7ImlkIjoyLCJuYW1lIjoiSm9obiBMb2JpIiwibGFiZWwiOiJCYWdzIG9mIFN1Z2FyIiwiZ3BzaWQiOiI4NjQ4OTUwMzAxNTk3NjYifSx7ImlkIjozLCJuYW1lIjoiZGZnaCIsImxhYmVsIjoiZ2hrbCIsImdwc2lkIjoiODY0ODk1MDMwMTU5MzUyIn1d


Track individual devices like this:
http://truckka.dev/traccar/map/device.php?device=864895030159287
or
http://truckka.com.ng/traccar/map/device.php?device=864895030159287

It can accept the from and to parameters for the date:
http://truckka.com.ng/traccar/map/device.php?device=864895030159287&from=12-11-2016&to=13-11-2018
