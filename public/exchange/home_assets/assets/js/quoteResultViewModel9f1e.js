﻿function QuoteResultViewModel() {
    var txtPickUpAddress = null;
    var txtDelivery = null;
    var equipmentType = null;
    var btnGetQuote = null;
    var PickupError = null;
    var DelieryError = null;
    var bookpopup = null;
    var btnLogOut = null;
    var btnBookPopUp = null;
    var txtPickUpDate = $("#txtPickUpDate");
    function Initialize() {
        $(document).ready(function (e) {
            $('#btnSignUpForFreeQuote ,.quoteMsgClose').click(function () {
                $('.freeQuoteMsg').hide();
            });

            $('.quoteMsg ,.dataMsgClose').click(function () {
                $('.dataNotAvailable').hide();
            })
            $('#txtPickupAddress1').on("cut copy paste", function (e) {
                e.preventDefault();
            });

            $('#txtDeliveryAddress1').on("cut copy paste", function (e) {
                e.preventDefault();
            });

            if ($('#hid_verification').val() != "" && $('#hid_verification').val() != undefined) {
                $('.emailverification').show();
                $('#hid_verification').val("");
            }
            $('.close-popup').click(function () {
                $('.emailverification').hide();
            });

            var pathname = window.location.pathname;
            var path = pathname.toLowerCase();

            txtPickUpAddress = $("#txtPickupAddress1");
            txtDelivery = $("#txtDeliveryAddress1");
            txttrucktypeError = $("#txttrucktypeError");
            equipmentType = $(".equipments");
            PickupError = $("#txtPickupAddress1Error");
            DelieryError = $("#txtDeliveryAddress1Error");
            btnGetQuote = $("#btnGetQuote");
            bookpopup = $("#myModal");
            btnLogOut = $("#btnLogOut");
            txtSelectWeightError = $("#SelectedWeightError");
            txttruckCategoryError = $("#SelectFeetError");
            SelectedWeightVal = $("#SelectedWeightVal");
            ddlFootageSize = $("#ddlSize");

            //for slider
            SelectedFeetVal = $("#SelectedFeetVal");
            txtSelectFeetError = $("#SelectFeetError");

            function Events() {
                txtPickUpAddress.on("keypress", function (e) {
                    if (e.which === 13)
                        e.preventDefault();
                });
                txtDelivery.on("keypress", function (e) {
                    if (e.which === 13)
                        e.preventDefault();
                });
                equipmentType.on("keypress", function (e) {
                    if (e.which === 13 && (txtPickUpAddress.val() == "" || txtDelivery.val() == ""))
                        e.preventDefault();
                });
                btnGetQuote.on("keypress", function (e) {
                    if (e.which === 13 && (txtPickUpAddress.val() == "" || txtDelivery.val() == ""))
                        e.preventDefault();
                });
                btnGetQuote.on("click", function (e) {
                    CalculateQuote();
                });
                $('#GetTruckBook').unbind().click(function (e) {
                    $('#txtPickupAddress1Error').text("");
                    $('#txtDeliveryAddress1Error').text("");
                    $('#txtDeliveryDateError').text("");
                    $('#SelectedWeightError').text("");
                    $('#txttrucktypeError').text("");
                    $('#txttruckCategoryError').text("");
                    $("#txtUserName").val("");
                    $("#txtEmail").val("");
                    $("#txtPhone").val("");
                    $("#txtLoginPassword").val("");
                    $("#txtConfirmPassword").val("");
                    $("#txtErrUserName").text("");
                    $("#txtErrEmail").text("");
                    $("#txtErrPhone").text("");
                    $("#PasswordError").text("");
                    $("#ConfirmPasswordError").text("");
                    //feet
                    $('#SelectFeetError').text("");

                    var result = false;
                    $(".field-validation-valid").text("");
                    if ($('#txtPickupAddress1').val().trim() == "") {
                        $('#txtPickupAddress1Error').text("Pickup City and State is required");
                        result = true;
                    }
                    if ($('#txtPickupAddress1').val().trim() != "") {
                        if ($('#txtPickupAddress1').val().split(",").length < 3) {
                            $('#txtPickupAddress1Error').text("Enter the pickup City and State");
                            result = true;
                        }
                    }

                    if ($('#txtDeliveryAddress1').val().trim() == "") {
                        $('#txtDeliveryAddress1Error').text("Delivery City and State is required");
                        result = true;
                    }

                    if ($('#txtDeliveryAddress1').val().trim() != "") {
                        if ($('#txtDeliveryAddress1').val().split(",").length < 3) {
                            $('#txtDeliveryAddress1Error').text("Enter the delivery City and State");
                            result = true;
                        }
                    }

                    if ($("#txtPickUpDate").val().trim() == "") {
                        $('#txtDeliveryDateError').text("Pickup date is required field");
                        result = true;
                    }

                    if ($("#SelectedWeightVal").val().trim() == "0LBS") {
                        $("#SelectedWeightError").text("Please select weight.");
                        result = true;
                    }

                    //feet
                    if ($("#SelectedFeetVal").val().trim() == "0") {
                        $("#SelectFeetError").text("Please select Feet.");
                        result = true;
                    }
                    if ($('.equipments:checked').val() == undefined) {
                        $("#txttrucktypeError").text("Please select truck type.");
                        result = true;
                    }
                    if ($("#IsTruckNotAvailable").val() == "1") {
                        $("#pickUpPhoneNumber").val("");
                        $("#spnPhoneNumber").text("");
                        result = true;
                        $(".quoteNotAvailable").show();
                    }
                    if (path.includes("/orders/new") || path.includes("/Orders/New")) {
                        if ($("#adminQuoteUserId").val() == "") {
                            $("#txtUserError").text("Please select user");
                            result = true;
                        }
                    }

                    if (result == true) {
                        return false;
                    }
                    else {
                        if ($("#GetTruckBook").hasClass("paymentPage")) {
                            //Assign PickUp,Delivery City and State Info....
                            $("#txtPickUpCityInfo").val($('#txtPickupAddress1').val().split(',')[0])
                            $("#txtPickUpStateInfo").val($("#txtPickupAddress1").val().split(',')[1])
                            $("#txtDeliveryCityInfo").val($('#txtDeliveryAddress1').val().split(',')[0])
                            $("#txtDeliveryStateInfo").val($('#txtDeliveryAddress1').val().split(',')[1])
                            Common.IsBusy(true);
                          
                            if ($("#SelectedFeetVal").val().split(' ')[0] != $("#TruckLoad").val())
                                $("#TruckLoad").val($("#SelectedFeetVal").val().split(' ')[0]);
                            var jsonData = {
                                PickUpAddress1: $("#txtPickupAddress1").val(), DeliveryAddress1: $("#txtDeliveryAddress1").val(), TotalMiles: $(".spnPickUpMiles").text(), TotalPaymentAmount: $('.spanTotalAmount').text(), IsSuccess: true, PickUpCity: $('#txtPickupAddress1').val().split(',')[0], PickUpState: $("#txtPickupAddress1").val().split(',')[1],
                                Deliverycity: $('#txtDeliveryAddress1').val().split(',')[0], DeliveryState: $('#txtDeliveryAddress1').val().split(',')[1], EquipmentType: $('.equipments:checked').val(), IsEdit: "N",
                                Weigth: $("#SelectedWeightVal").val().trim(), AverageDays: $('.spnPickUpAvgDays').text(), PickUpDate: $("#txtPickUpDate").val().trim(),
                                TruckLoadType: $("#TruckLoadType").val(), TruckLoad: $("#TruckLoad").val(), OrderId: $('#hOrderId').val(), QuoteId: $('#id').val()
                            };

                            $.ajax({
                                // url: "/Order/OrderPlace",
                                url: "/OrderApi/Create",
                                type: "POST",
                                data: jsonData,
                                success: function (data) {
                                    var bookht = $('#step2-pickupinfo').height();
                                    $(".map-ht").css({ "height": bookht });
                                    $('#step1-bookingpage').hide();
                                    $('#step2-pickupinfo').show();
                                    $('#orderId').val(data.OrderId);

                                    if (path.includes("/orders/new") || path.includes("/Orders/New"))
                                    {
                                        $('.map-ht').hide();
                                        $('#textPickupAddress').text($("#txtPickupAddress1").val());
                                        $('#textDeliveryAddress').text($("#txtDeliveryAddress1").val());
                                        $('#textType').text($('.equipments:checked').val());
                                        $('#textDate').text($("#txtPickUpDate").val());
                                        $('#textWeight').text($("#SelectedWeightVal").val());
                                        $('#textFeet').text($("#SelectedFeetVal").val());
                                    }
                                    Common.IsBusy(false);
                                }
                            });
                        }
                    }
                });

                btnLogOut.on("click", function (e) {
                    appCtx.DisposeCtx();
                });
            };

            $(document).on('click', '#equipmentType,#equipmentType1', function (e) {
                if (document.getElementById('equipmentType').checked) {
                    $('.spnPickUpTruck').text(document.getElementById('equipmentType').value);
                } else {
                    if (document.getElementById('equipmentType1').checked) {
                        $('.spnPickUpTruck').text(document.getElementById('equipmentType1').value);
                    }
                }
                if (e.handled !== true) // This will prevent event triggering more then once
                {
                    if (path.includes("/user/quote") || path.includes("/orders/new") || path.includes("/Orders/New")) {
                        CalculateQuote();
                        e.handled = true;
                    }
                }
            })

            $(document).on('changeDate', '#txtPickUpDate', function (e, ui) {
                e.preventDefault();
                if (path.includes("/user/quote") || path.includes("/orders/new") || path.includes("/Orders/New"))
                    CalculateQuote();
            });

            $(document).on('slidechange', '#weightSlider', function (e, ui) {
                if (e.handled !== true) // This will prevent event triggering more then once
                {
                    if (path.includes("/user/quote") || path.includes("/orders/new") || path.includes("/Orders/New")) {
                        CalculateQuote();
                        e.handled = true;
                    }
                }
            });

            $(document).on('slidechange', '#ddlSize', function (e, ui) {
                if (e.handled !== true) // This will prevent event triggering more then once
                {
                    if (path.includes("/user/quote") || path.includes("/orders/new") || path.includes("/Orders/New")) {
                        CalculateQuote();
                        e.handled = true;
                    }
                }
            });

            function CalculateQuote() {

                PickupError.text("");
                DelieryError.text("");
                txttrucktypeError.text("");
                txtSelectWeightError.text("");
                txttruckCategoryError.text("");

                //for slider
                txtSelectFeetError.text("");

                var isCheck = true;
                if (txtPickUpAddress.val() == "") {
                    isCheck = false;
                    PickupError.text("Please enter the PickUp City and State");
                }
                else if (txtPickUpAddress.val().split(",").length < 3) {

                    PickupError.text("Please enter the PickUp City and State");
                    isCheck = false;
                }
                else {
                    PickupError.text("");
                }
                if (txtDelivery.val() == "") {
                    isCheck = false;
                    DelieryError.text("Please enter the Delivery City And State");
                }
                else if (txtDelivery.val().split(",").length < 3) {
                    DelieryError.text("Please enter the Delivery City And State");
                    isCheck = false;
                }
                else {
                    DelieryError.text("");
                }
                if (equipmentType.is(':checked') == false) {
                    txttrucktypeError.text("Please select truck type");
                    isCheck = false;
                }

                if (SelectedWeightVal.val() == "0LBS") {
                    isCheck = false;
                    txtSelectWeightError.text("Please select weight");
                }

                //for feet slider
                if (SelectedFeetVal.val() == "") {
                    isCheck = false;
                    txtSelectFeetError.text("Please select Feet");
                }
                if (path.includes("/orders/new") || path.includes("/Orders/New")) {
                    if ($("#adminQuoteUserId").val() == "") {
                        $("#txtUserError").text("Please select user");
                        isCheck = false;
                    } else {
                        $("#txtUserError").text("");
                    }
                }
                if (isCheck) {
                    Common.IsBusy(true);
                    var date = new Date();
                    var time = date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
                    var data;
                    if (!path.includes("/orders/new") && !path.includes("/Orders/New")) {
                        data = {
                            Origin: $("#txtPickupAddress1").val(), Destination: $("#txtDeliveryAddress1").val(), Equipment_Type: $(".equipments:checked").val().toString(),
                            Date: $("#txtPickUpDate").val() + " " + time, Weight: $("#SelectedWeightVal").val().trim(), Feet: parseInt($("#SelectedFeetVal").val()),
                            PickUpLng: $("#pickUpLong").val(), PickUpLat: $("#pickUpLat").val(), DeliveryLng: $("#deliveryLong").val(), DeliveryLat: $("#deliveryLat").val()
                        };
                    } else {
                        data = {
                            Origin: $("#txtPickupAddress1").val(), Destination: $("#txtDeliveryAddress1").val(), Equipment_Type: $(".equipments:checked").val().toString(),
                            Date: $("#txtPickUpDate").val() + " " + time, Weight: $("#SelectedWeightVal").val().trim(), Feet: parseInt($("#SelectedFeetVal").val()),
                            PickUpLng: $("#pickUpLong").val(), PickUpLat: $("#pickUpLat").val(), DeliveryLng: $("#deliveryLong").val(), DeliveryLat: $("#deliveryLat").val(),UserId:$("#adminQuoteUserId").val()
                        };
                    }
                    $.ajax({
                        url: "/api/quotes",
                        type: "POST",
                        data: data,
                        success: function (result) {
                            Common.IsBusy(false);
                            if (result.status == true && result.result != null) {
                                if (!path.includes("/user/quote") && !path.includes("/orders/new") && !path.includes("/Orders/New")) {
                                    location.href = "/user/quote?id=" + result.result.id;
                                }
                                else {
                                    var amount = format2(result.result.quoteAmount, "$");
                                    $('.spanTotalAmount').text(amount);
                                    $('#id').val(result.result.id);
                                    $('.spnPickUpMiles').text(result.result.miles);
                                    $('.spnPickUpAvgDays').text(result.result.estimatedTime);
                                }
                            } else {
                                if (result.message == "Data Not Available") {
                                    $('.dataNotAvailable').show();
                                    $('.spanTotalAmount').text(0);
                                } else {
                                    $('.freeQuoteMsg').show();
                                    $('.spanTotalAmount').text(0);
                                }
                            }
                        }
                    });
                } else {
                    Common.IsBusy(false);
                }
            };

            function BookPopUp() {
                function Initialize() {
                    AssignElementToVariable();
                    Events();
                };
                function AssignElementToVariable() {
                    btnBookPopUp = $("#btnBookPopUp");
                };
                function Events() {
                    btnBookPopUp.on("click", function (e) {
                        RedirectToLTL($("#hiddenPickUp").val(), $("#hiddenDelivery").val(), $("#hiddenEquipmentType").val());
                    });
                };
                Initialize();
            };

            function format2(n, currency) {
                return currency + " " + n.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
            }
            Events();

            google.maps.event.addDomListener(window, 'load', function () {
                var options = {
                    types: ['(cities)'],
                    componentRestrictions: { country: "us" }
                };
                var places = new google.maps.places.Autocomplete(document.getElementById('txtPickupAddress1'), options);
                google.maps.event.addListener(places, 'place_changed', function (e) {
                    var placeData = places.getPlace();
                    $('#pickUpLong').val(placeData.geometry.location.lng());
                    $('#pickUpLat').val(placeData.geometry.location.lat());
                    Common.GetGoogleMapLocation($('#txtPickupAddress1'), $('#txtDeliveryAddress1'));
                    if (path.includes("/user/quote") || path.includes("/orders/new") || path.includes("/Orders/New"))
                        CalculateQuote(e);
                });
            });
            google.maps.event.addDomListener(window, 'load', function () {
                $('#IndexLoader').fadeOut(1000);
                var options = {
                   types: ['(cities)'],
                    componentRestrictions: { country: "us" }
                };
                var places = new google.maps.places.Autocomplete(document.getElementById('txtDeliveryAddress1'), options);
                google.maps.event.addListener(places, 'place_changed', function (e) {
                    var placeData = places.getPlace();
                    $('#deliveryLong').val(placeData.geometry.location.lng());
                    $('#deliveryLat').val(placeData.geometry.location.lat());
                    Common.GetGoogleMapLocation($('#txtPickupAddress1'), $('#txtDeliveryAddress1'));
                    if (path.includes("/user/quote") || path.includes("/orders/new") || path.includes("/Orders/New"))
                        CalculateQuote(e);
                });
            });

            function isNumber(evt) {
                evt = (evt) ? evt : window.event;
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                    return false;
                }
                return true;
            }
            //map-script
            var httop = $(".top_dec").height();
            $(".cn-map1").css({
                height: httop
            });

            $('#adminDashboardLoad,#adminOrderDetailsLoad,#adminMarketRateLoad,#adminMarketCityLoad,#adminUserListLoad,#adminCurrentNotificationLoad,#adminReplyNotificationLoad,#adminProfileLoad,#adminLogOutLoad,#reports,#adminFootagLoad,#adminWeightLoad').click(function () {
                $("#isAdminBusy").css("display", "inline-block");
            })

            //*Step-1 Booking Info Start*
            var sliderCreate = false;
            $(function () {
                function refreshSliderIcon() {
                    var weightSlider = $("#weightSlider").slider("value");
                }
                $("#weightSlider").slider({
                    value: 0,
                    min: 0,
                    orientation: "horizontal",
                    range: "min",
                    max: 45000,
                    step: 1000,
                    slide: function (event, ui) {
                        refreshSliderIcon();
                        $("#SelectedWeightVal").val(ui.value + "LBS");
                    }, create: function (event, ui) {
                        sliderCreate = true;
                    },
                }).unbind('slidechange', function (event, ui) { });


                if (!path.includes("/user/quote") && !path.includes("/orders/new") && !path.includes("/Orders/New")) {
                    var weight = $("#weightSlider").slider("value");
                    $("#SelectedWeightVal").val(weight + "LBS");
                }
                else {
                    var weight = $('#SelectedWeightVal').val().split('L');
                    var slideLength = weight[0] / 1000;
                    var slideLeftWidth = "2.22222" * slideLength;
                    slideLeftWidth = slideLeftWidth + "%";
                    $('#weightSlider .ui-slider-handle').css('left', slideLeftWidth)
                    $("#weightSlider .ui-slider-range").css("width", slideLeftWidth)
                    $("#weightSlider .ui-slider-range").css("background", "#1a74bc")
                }
            });
            //Feet
            var sliderFeetCreate = false;
            $(function () {
                function refreshSliderFeetIcon() {
                    var weightSlider = $("#ddlSize").slider("value");
                }
                $("#ddlSize").slider({
                    value: 0,
                    min: 4,
                    orientation: "horizontal",
                    range: "min",
                    max: 53,
                    step: 1,
                    slide: function (event, ui) {
                        refreshSliderFeetIcon();
                        $("#SelectedFeetVal").val(ui.value + " FEET");
                    }, create: function (event, ui) {
                        sliderFeetCreate = true;
                    },
                }).unbind('slidechange', function (event, ui) { });
                if (!path.includes("/user/quote") && !path.includes("/orders/new") && !path.includes("/Orders/New"))
                    $("#SelectedFeetVal").val($("#ddlSize").slider("value") + " FEET");
                else {
                    var feet = $('#SelectedFeetVal').val().split(' ')[0];
                    if (feet > 4) {
                        var slideFeetLeftWidth = (feet - 4) * 100 / (53 - 4);
                        slideFeetLeftWidth = slideFeetLeftWidth + "%";
                        $('#ddlSize .ui-slider-handle').css('left', slideFeetLeftWidth)
                        $("#ddlSize .ui-slider-range").css("width", slideFeetLeftWidth)
                        $("#ddlSize .ui-slider-range").css("background", "#1a74bc")
                    } else {
                        $("#ddlSize .ui-slider-range").css("background", "#1a74bc")
                    }
                }
            });


            if ($("#calculateQuote").val()) {
                CalculateQuote();
            }
        });

    };

    Initialize();
}

qouteResult = new QuoteResultViewModel();