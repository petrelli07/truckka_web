﻿$('#IndexLoader').fadeOut(500);

$(".dwn-arw a,.list-unstyled li .slide").click(function (evn) {
    evn.preventDefault();
    if ($($(this).attr('href')).offset() != undefined) {
        $('html, body').animate({ scrollTop: $($(this).attr('href')).offset().top }, 500, 'linear');
    }
});

$('.carousel').carousel({
    interval: 12000
});

$("#aboutSlide").click(function (evn) {
    evn.preventDefault();
    if (window.location.pathname == "/home1/faq" || window.location.pathname == "/home1/contact" || window.location.pathname == "/faq" || window.location.pathname == "/contact" || window.location.pathname == "/truck/rates")
        window.open("home1/index/index.html#about", '_blank');
    if ($($(this).attr('href')).offset() != undefined) {
        $('html, body').animate({ scrollTop: $($(this).attr('href')).offset().top - 40 }, 500, 'linear');
    }
});

$("#goToAbout").click(function () {
    tq.common.redirectToUrl("home1/index/index.html#about");
});

$(".about-btn a").click(function (evn) {
    evn.preventDefault();
    $('html, body').animate({ scrollTop: $($(this).attr('href')).offset().top - 40 }, 500, 'linear');
});
