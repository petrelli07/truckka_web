﻿Common = {
    USERIP: "",
    USERCITY: "",
    USERREGION: "",
    TruckTotalFeet: 53,
    FullLoadTruckPrice: 1000,
    PageSize: 10,
    USCITIES: null,
    SmallDeviceWidth: 640,
    ValidateFunction: function () {
   
        return false;
    },
    GetSessionContext: function (callback) {

        var result = null;
        $.ajax({
            url: "/Common/Common/GetContext",
            type: "POST",
            contenttype: "application/json",
            async: false,
            success: function (data) {
                result = data;
            }
        });
        return result;
    },
    ValidateEmail: function (email) {
        var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        return expr.test(email);
    },
    ValidateNumber: function (value) {
        jQuery.validator.addMethod("phoneUS", function (value, element) {
            value = value.replace(/\s+/g, "");
            return this.optional(element) || value.length > 9 &&
                value.match(/^(1-?)?(\([2-9]\d{2}\)|[2-9]\d{2})-?[2-9]\d{2}-?\d{4}$/);
        }, "Please specify a valid phone number");
    },
    SendEmail: function (emailFrom, emailTo, subject, body) {
      
        $.ajax({
            url: "/Common/Common/SendEmail",
            type: "POST",
            contenttype: "application/json",
            async: true,
            data: { emailFrom: emailFrom, emailTo: emailTo, subject: subject, body: encodeURIComponent(body) },
            success: function (data) {
                var result = data;
            }
        });

    },
    ConvertToDate: function (date) {
        var date = new Date(parseInt(date, 10));
        var curr_month = ('0' + (date.getMonth() + 1));
        var curr_date = ('0' + (date.getDate()));
        var curr_year = date.getFullYear();
        return curr_year + "/" + curr_month + "/" + curr_year;
    },
    ConvertToTime: function (date) {
        var date = new Date(parseInt(date, 10));
        var hours = date.getHours();
        var min = date.getMinutes();
        return hours + ":" + min;
    },
 
 GetGoogleMapLocation: function (picklocation, deliverylocation) {
        function init() {
            var directionsService = new google.maps.DirectionsService;
            var directionsDisplay = new google.maps.DirectionsRenderer;
            if (document.getElementById('map') != undefined && document.getElementById('map') != null) {
                var map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 7,
                    center: { lat: 41.85, lng: -87.65 }
                });
                directionsDisplay.setMap(map);
                calculateAndDisplayRoute(directionsService, directionsDisplay);
            }
        }

        function calculateAndDisplayRoute(directionsService, directionsDisplay) {
            directionsService.route({
                origin: picklocation.val(),
                destination: deliverylocation.val(),
                travelMode: 'DRIVING'
            }, function (response, status) {
                if (status === 'OK') {
                    directionsDisplay.setDirections(response);
                } else {
                }
            });
        }
        init();
    },
    GetCookie: function (cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    },
    SetCookie: function (cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = cname + "=" + cvalue + "; " + expires;
    },
    PopUp: function (msg) {
        var html = '<div id="myModal" tabindex="-1" class="modal-outer-popup-custom"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">' +
        '<div class="modal-header">' +
            '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>' +
            '<h3 id="myModalLabel">Alert Message</h3>' +
        '</div>' +
        '<div class="modal-body">' +
            '<p ><span>' + msg + '</span></p>' +
        '</div>' +
        '<div class="modal-footer">' +
        '<button class="btn btn-primary" id="btnPopUpRegister">Register</button>' +
        '</div>' +
    '</div>';

        $('#dialogDiv').modal({
            backdrop: 'static',
            keyboard: true
        }, 'show');
        $('#dialogContent').children().remove();
        $('#dialogContent').append(html);

        $("#btnPopUpRegister").on("click", function (e) {
            $('#dialogDiv').modal("hide");
            $('.log-regis-pop').slideToggle(200);
        });

    },
    IsBusy: function (isBusy) {
        if (isBusy) {
            $("#isLoaderBusy").css("display", "inline-block");
        } else {
            $("#isLoaderBusy").css("display", "none");
        }
    },
    StringBuilder: function () {
        var data = [];
        var counter = 0;
        return {
            // adds string s to the stringbuilder
            append: function (s) { data[counter++] = s; return this; },
            // removes j elements starting at i, or 1 if j is omitted
            remove: function (i, j) { data.splice(i, j || 1); return this; },
            // inserts string s at i
            insert: function (i, s) { data.splice(i, 0, s); return this; },
            // builds the string
            toString: function (s) { return data.join(s || ""); return this; },
        };
    },
    CalculateEstimateTime: function (miles) {
        var m = Math.floor(miles / 600);
        var r = miles % 600;
        if (r > 0) {
            m += 1;
        }
        return m;
    },
    RunSchedule: function () {
        var data = DataBase.ExecuteDataset("SELECT * FROM TQ_Scheduler WHERE SchedulerName='DAT Scheduler'");
        if (data[0].Status == "Y") {
            DataBase.AjaxRequestWithOutParameter("/Common/Scheduler/RunScheduler", true, "POST");
        }
    },
    FormatCurrency: function (ctrl) {
        return ctrl.formatCurrency();
    },
  
  RedirectToRoute: function (url, type, data) {
        var result = null;
        $.ajax({
            url: url,
            type: type,
            contenttype: "application/json",
            async: false,
            data: data,
            success: function (data) {
                result = data;
                if (result.errorMessage == "Session Expired") {
                    window.location.href = "Common/Common/SessionExpire.html";
                    return;
                }
                Common.IsBusy(false);
            }

        });
        return result;
    },
    SlideToggel: function () {
        if ($(window).width() <= Common.SmallDeviceWidth) {
            var effect = 'slide';

            // Set the options for the effect type chosen
            var options = { direction: 'left' };

            // Set the duration (default: 400 milliseconds)
            var duration = 700;

            $('.tr-user-left-sidebar').toggle(effect, options, duration, function () {

                $(".tr-right-user-content").removeClass("tq-right-content-full");
                $(".tr-right-user-content").removeClass("tq-right-content");
                if ($('.tr-user-left-sidebar').css("display") == "none") {
                    $(".tr-right-user-content").css({ "width": "100%", "margin-left": "0px" });
                }
                else {
                }
            });
        }
    }
}

Route = {
    RedirectOrderDetail: function (data) {

        $("#ulAdminPanel").children().removeClass("active");
        $("#ulAdminPanel").find("#btnAdminUserOrderDetails").addClass("active");
        Common.IsBusy(true);
        $("#headerContainer").text("Order Detials");
        Common.SlideToggel();
        window.setTimeout(function (e) {
            var result = Common.RedirectToRoute("/orders", "POST", data);
            $("#userPanelRenderBody").children().not(".tr-user-details-outer").remove();
            $("#userPanelRenderBody").append(result);
            $("#cmbSearchOrderStatus").selectpicker();
            $(".cmbupdateorderstatus").selectpicker();
            $("#cmbTruckType").selectpicker();
            $("#cmbTruckLoad").selectpicker();
            Common.IsBusy(false);
        }, 100);
    },
    RedirectOrderDetailByStatus: function (data) {
        $("#ulAdminPanel").children().removeClass("active");
        $("#ulAdminPanel").find("#btnAdminUserOrderDetails").addClass("active");
        Common.SlideToggel();
        Common.IsBusy(true);
        window.setTimeout(function (e) {

            var result = Common.RedirectToRoute("/Admin/Admin/GetOrdersByStatus", "GET", data);
            $("#headerContainer").text("Order Detials");
            $("#userPanelRenderBody").children().not(".tr-user-details-outer").remove();
            $("#userPanelRenderBody").append(result);
            $("#cmbSearchOrderStatus").selectpicker();
            $(".cmbupdateorderstatus").selectpicker();
            $("#cmbTruckType").selectpicker();
            $("#cmbTruckLoad").selectpicker();
            Common.IsBusy(false);
        }, 100);
    }
};

DataBase = {
    ExecuteDataset: function (query) {
        var result = "";
        $.ajax({
            url: "/Common/Common/ExecuteDataset",
            type: "GET",
            contenttype: "application/json",
            async: false,
            data: { query: query },
            success: function (data) {
                result = data;
            }
        });
        return result;
    },
    ExecuteQuery: function (query) {
        var result = "";
        $.ajax({
            url: "/Common/Common/JsonResult",
            type: "GET",
            contenttype: "application/json",
            async: false,
            data: { query: query },
            success: function (data) {
                result = data;
            }
        });
        return result;
    },
    AjaxRequest: function (url, data, async, type) {

        var result = "";
        $.ajax({
            url: url,
            type: type,
            contenttype: "application/json",
            async: async,
            data: data,
            success: function (data) {
                result = data;

            }
        });
        return result;
    },
    AjaxRequestWithOutParameter: function (url, async, type) {
        var result = "";
        $.ajax({
            url: url,
            type: type,
            contenttype: "application/json",
            async: async,
            success: function (data) {
                result = data;

            }
        });
        return result;
    },
}