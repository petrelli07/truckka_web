﻿//Angular notifiation controller code

app.controller('Notification', function ($scope, $http, $modal) {
    $scope.notifications = [];
    $scope.allNotifications = [];
    $scope.notificationCount;

    $scope.init = function () {
        setInterval(function () {
            getNotification();
        }, 60 * 1000);
        //  $scope.ConnectUser();
        getNotification();
    };
    function getNotification() {
        $http.get("/api/notification/GetAllUnReadNotification")
            .then(function (response) {
                $scope.notificationCount = response.data.length;
                $scope.notifications = response.data;
            });
    }

    $scope.ConnectUser = function () {
        $.connection.hub.logging = true;

        registerClientMethods(chatHub);

        // Start Hub
        $.connection.hub.start().done(function () {
            registerEvents(chatHub);
            chatHub.server.connect();
        });
    };

    function registerClientMethods(chatHub) {
        // Calls when user s logged in
        chatHub.client.onConnected = function () {
            alert("connected");
        }

        // On New User Connected
        chatHub.client.onNewUserConnected = function (id, name, email) {
            // AddUser(chatHub, id, name, email);
        }

        // On User Disconnected
        chatHub.client.onUserDisconnected = function (id, userName) {
            chatHub.server.getOnlineUsers(userId, locationId);
        }

        // On User Disconnected Existing
        chatHub.client.onUserDisconnectedExisting = function (id, userName) {

        }

        chatHub.client.messageReceived = function (userName, message) {
            //alert(message);
        }

        chatHub.client.sendPrivateMessage = function (fromUserId, message, userId, toUserId) {
            // private chat callback 
        }
    }

    function registerEvents(chatHub) {

    }

    $scope.getAllNotification = function () {
        $http.get("/api/notification/GetAllNotification")
        .then(function (response) {
            $scope.allNotifications = response.data;
        });
    };

    $scope.markAsRead = function () {
        $http.get("/api/notification/markasread")
        .then(function (response) {
            $scope.notificationCount = 0;
        });
    }
});

//End Angular notifiation controller code