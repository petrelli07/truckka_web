$(document).ready(function() {

    $(".newCarrDets").unbind('click').click(function(e){

        e.preventDefault();


        var _token = $("input[name='_token']").val();

        $.ajax({

            async:false,

            processData: false,

            contentType: false,

             url: baseURI+"/createNewCarrier",
             //url: "http://truckka.com.ng:8082/createNewCarrier",

            type:'POST',

            data: new FormData($("#carrDetails")[0]),

            success: function(data) {

                if($.isEmptyObject(data.error)){

                    printSuccessMsg(data.success);

                    $("#carrDetails")[0].reset();

                }else{

                    printErrorMsg(data.error);

                }

            }

        });


    });
    function printErrorMsg (msg) {

        $(".print-error-msg").find("ul").html('');

        $(".print-error-msg").css('display','block');

        $.each( msg, function( key, value ) {

            $(".print-error-msg").find("ul").append('<li>'+value+'</li>');
            $(".print-error-msg").fadeOut(5000);
        });

    }

    function printSuccessMsg (success) {

        $(".print-success-msg").find("ul").html('');

        $(".print-success-msg").css('display','block');


        $(".print-success-msg").find("ul").append(success);
        $(".print-success-msg").fadeOut(5000);

    }

});


