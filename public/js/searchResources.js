$(document).ready(function() {

    $(".haulageRequest").unbind('click').click(function(e){

        e.preventDefault();


        var _token = $("input[name='_token']").val();

        $.ajax({

            async:false,

            processData: false,

            contentType: false,

            //url: "http://206.189.121.111:8082/sendHaulageRequest",
            url: "http://localhost/tmsa/public/sendHaulageRequest",

            type:'POST',

            data: new FormData($("#haulReq")[0]),

            success: function(data) {

                if($.isEmptyObject(data.error)){

                    alert(data.success);
                    $(".haulageRequest").hide();
                    
                    //window.location.replace("http://206.189.121.111:8082/viewOrders");
                    window.location.replace("http://localhost/tmsa/public/viewOrders");

                }else{

                    alert(data.error);

                    $(".haulageRequest").show();
                }

            }

        });


    });
    function printErrorMsg (msg) {

        $(".print-error-msg").find("ul").html('');

        $(".print-error-msg").css('display','block');

        $.each( msg, function( key, value ) {

            $(".print-error-msg").find("ul").append('<li>'+value+'</li>');
            $(".print-error-msg").fadeOut(5000);
        });

    }

    function printSuccessMsg (success) {

        $(".print-success-msg").find("ul").html('');

        $(".print-success-msg").css('display','block');


        $(".print-success-msg").find("ul").append(success);
        $(".print-success-msg").fadeOut(5000);

    }

});


