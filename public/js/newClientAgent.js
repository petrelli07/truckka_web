$(document).ready(function() {

    $(".clientAgent").click(function(e){
        e.preventDefault();
        var _token = $("input[name='_token']").val();/*


        alert(new FormData($("#clientDetails")[0]));*/

        $.ajax({

            async:false,

            processData: false,

            contentType: false,

            //url: "http://truckka.com.ng:8082/createNewClientAgent",
            url: "http://localhost/tmsa/public/createNewClientAgent",

            type:'POST',

            data: new FormData($("#clientAgentForm")[0]),
            success: function(data) {


                if($.isEmptyObject(data.error)){
                    printSuccessMsg(data.success);

                    $("#clientAgentForm")[0].reset();

                }else{

                    printErrorMsg(data.error);

                }

            }

        });


    });
    function printErrorMsg (msg) {

        $(".print-error-msg").find("ul").html('');

        $(".print-error-msg").css('display','block');

        $.each( msg, function( key, value ) {

            $(".print-error-msg").find("ul").append('<li>'+value+'</li>');
            $(".print-error-msg").fadeOut(20000);
        });

    }

    function printSuccessMsg (success) {

        $(".print-success-msg").find("ul").html('');

        $(".print-success-msg").css('display','block');


        $(".print-success-msg").find("ul").append(success);
        $(".print-success-msg").fadeOut(20000);

    }

});


