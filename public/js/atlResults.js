$(document).ready(function() {

    $(".enterATL").unbind('click').click(function(e){

    //$(".uploadDocs").click(function(e){

        e.preventDefault();

        $.ajax({

            async:false,

            processData: false,

            contentType: false,

            url: "http://localhost/tmsa/public/searchATL",
            //url: "http://206.189.121.111:8083/searchATL",

            type:'POST',

            data: new FormData($("#searchATL")[0]),

            success: function(data) {

                var length = data.success;
                var trueLength = length.length;

                if($.isEmptyObject(data.error)){                           
                    
                            var plateNumber = data.success[0].plateNumber;
                            var resourceType = data.success[0].resourceType;
                            var user_id = data.success[0].user_id;
                            var id = data.success[0].id;
                            var service_id = data.success[1]; 

                            document.getElementById("plateNumber").innerHTML = plateNumber;
                            document.getElementById("truckType").innerHTML = resourceType;
                            document.getElementById("serviceID").value = service_id;
                            document.getElementById("plateNumberInput").value = plateNumber;
                            document.getElementById("resourceType").value = resourceType;
                            document.getElementById("user_id").value = user_id;
                            document.getElementById("resourceID").value = id;

                        $("#edit").modal('show')
                }else{
                    alert(data.error);
                }
            }
        });
    });
    function printErrorMsg (msg) {

        $(".print-error-msg").find("ul").html('');

        $(".print-error-msg").css('display','block');

        $.each( msg, function( key, value ) {

            $(".print-error-msg").find("ul").append('<li>'+value+'</li>');
            $(".print-error-msg").fadeOut(5000);
        });

    }

    function printSuccessMsg (success) {

        $(".print-success-msg").find("ul").html('');

        $(".print-success-msg").css('display','block');


        $(".print-success-msg").find("ul").append(success);
        $(".print-success-msg").fadeOut(5000);

    }

});


