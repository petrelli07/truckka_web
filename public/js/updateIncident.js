$(document).ready(function() {

    $(".incidentUpdateThread").unbind('click').click(function(e){

        e.preventDefault();

        var formValues = $('#updateIncident').serialize();


        $.ajax({

            //url: "http://206.189.121.111:8082/public/editIncident",
            url: "http://localhost/tmsa/public/editIncident",

            type:'POST',

            data: formValues,

            success: function(data) {

                if($.isEmptyObject(data.error)){
                    alert(data.success);

                }else{

                    alert(data.error);

                }

            }

        });


    });
    function printErrorMsg (msg) {

        $(".print-error-meter-msg").find("ul").html('');

        $(".print-error-meter-msg").css('display','block');

        $.each( msg, function( key, value ) {

            $(".print-error-meter-msg").find("ul").append('<li>'+value+'</li>');
            $(".print-error-meter-msg").fadeOut(5000);
        });

    }

    function printSuccessMsg (success) {

        $(".print-success-meter-msg").find("ul").html('');

        $(".print-success-meter-msg").css('display','block');


        $(".print-success-meter-msg").find("ul").append(success);
        $(".print-success-meter-msg").fadeOut(5000);

    }

});

