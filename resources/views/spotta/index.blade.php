<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SPOTTA</title>
    <link rel="stylesheet" href="{{url('spotta/assets/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{url('spotta/assets/fonts/ionicons.min.css')}}">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Aclonica">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto+Condensed">
    <link rel="stylesheet" href="{{url('spotta/assets/css/Footer-Dark.css')}}">
    <link rel="stylesheet" href="{{url('spotta/assets/css/styles.css')}}">
     <script type="text/javascript">
        var baseURI     =  "{{url('/')}}";
    </script>

    

</head>

<body>
    <nav class="navbar navbar-dark navbar-expand-md fixed-top d-flex" style="background-color:#f9ab14;">
        <div class="container-fluid"><a class="navbar-brand" href="#">Brand</a><button class="navbar-toggler" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="navbar-toggler-icon bg-dark"></span></button>
            <div class="collapse navbar-collapse"
                id="navcol-1">
                <ul class="nav navbar-nav flex-grow-1 justify-content-between">
                    <li class="nav-item" role="presentation"><a class="nav-link active" href="#">First Item</a></li>
                    <li class="nav-item" role="presentation"><a class="nav-link" href="#">Second Item</a></li>
                    <li class="nav-item" role="presentation"><a class="nav-link" href="#">Third Item</a></li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="container" >
        <div class="row">
            <div class="col-7 offset-md-0" id="how-it-works">
                <div class="jumbotron jumbotron-fluid" style="font-family:Roboto, sans-serif;">
                    <h1>Heading text</h1>
                    <p>Nullam id dolor id nibh ultricies vehicula ut id elit. Cras justo odio, dapibus ac facilisis in, egestas eget quam.</p>
                    <div class="row">
                        <div class="col-12"><img class="img-fluid" alt="how it works" width="700px" height="900px"></div>
                    </div>
                    <p></p><a class="btn btn-primary" role="button" href="#">Learn more</a></div>
            </div>

    <div class="col-5 mb-auto" id="new-order" style="border:1px solid black">
                <h4 class="text-center">GET STARTED NOW!</h4>
                

    <form id="originForm" style="display:none;">
        {{csrf_field()}}
        <input type="hidden" id="origin2" name="originToServer">
    </form>

<!-- begining of form div -->

<div id="form">

        <form class="new-order" id="getQuoteForm">
        {{csrf_field()}}
          <div class="row">
            <div class="col">
            <label>WHAT DO YOU WANT TO MOVE?</label>
              <textarea class="form-control" name="description" placeholder="Describe what you want to move"></textarea>
            </div>
          </div>

          <div class="row" style="padding-top:20px;">
            <div class="col">
            <label>WHAT IS THE WEIGHT IN TONNES?</label>
              <input type="text" name="weight" class="form-control" placeholder="What is the weight in Tonnes">
            </div>
          </div>

          <div class="row" style="padding-top:20px;">
            <div class="col">
                <label>WHERE ARE YOU MOVING IT FROM?</label>
                    <select class="custom-select" required id="standardOrigin" name="origin">
                      <option selected value="">Select a location</option>
                        @for ($i=0; $i < count($uniqueOrigins); $i++) 
                        <option value="{{$uniqueOrigins[$i]}}">{{$uniqueOrigins[$i]}}</option>
                        @endfor
                     
                    </select>
            </div>

            <div class="col">
                <label>WHERE ARE YOU MOVING IT TO?</label>
                    <select class="custom-select" required id="destinationSelect" name="destination">
                      
                    </select>
            </div>
          </div>

          <div class="row" style="padding-top:20px;">
            <div class="col">
                <label>CHOOSE TRUCK TYPE</label>
                    <select class="custom-select" required name="truckType">
                      <option selected>Select a Type of Truck</option>
                      @foreach($truckTypes as $truckType)
                      <option value="{{$truckType->id}}">{{$truckType->resource_type_description}}</option>
                      @endforeach
                    </select>
            </div>

            <div class="col">
                <label>CHOOSE PICKUP DATE</label>
                    <input class="form-control" type="date" required name="pickupdate">
            </div>

          </div>

          <div class="form-group" id="form-components" style="padding-top:20px;">
              <div class="col">
                <button class="btn btn-primary getQuoteButton" type="button">SUBMIT</button>
              </div>
          </div>

    </form>
</div>
<!-- end of form div -->

<!-- beginning of quote div -->



<div id="quoteDiv" style="padding-bottom:10px; display:none;">

    <div >Quote</div>

        <div class="row">
            <div class="col-md-6">
            Cargo Description:
            </div>

            <div class="col-md-6" id="description">
            
            </div>  
        </div>

        <div class="row">
            <div class="col-md-6 col-xs-12">
            DESTINATION:
            </div>

            <div class="col-md-6 col-xs-12" id="destination">
            
            </div> 
        </div>

        <div class="row">
            <div class="col-md-6">
            PICKUP POINT:
            </div>

            <div class="col-md-6" id="origin">
            
            </div>  
        </div>

        <div class="row">
            <div class="col-md-6 col-xs-12">
            WEIGHT:
            </div>

            <div class="col-md-6 col-xs-12" id="weight">
            
            </div>  
        </div>

        <div class="row">
            <div class="col-md-6 col-xs-12">
            AMOUNT:
            </div>

            <div class="col-md-6 col-xs-12" id="amount">
            
            </div>  
        </div>

        <div class="row">
            <div class="col-md-6 col-lg-6 col-xs-12" >
            VAT (5%):
            </div>

            <div class="col-md-6 col-lg-6 col-xs-12" id="vat">
            
            </div>  
        </div>

        <div class="row">
            <div class="col-md-6 col-lg-6 col-xs-12">
            TOTAL:
            </div>

            <div class="col-md-6 col-lg-6 col-xs-12" id="total">
            
            </div>  
        </div>

        <form id ="quotesubmit" action="{{url('/spotta/submitQuote')}}" method="POST">
            {{csrf_field()}}
            <input name="origin" type="hidden" id="formOrigin">
            <input name="destination" type="hidden" id="formDestination">
            <input name="description" type="hidden" id="formDescription">
            <input name="amount" type="hidden" id="formAmount">
            <input name="vat" type="hidden" id="formVat">
            <input name="total" type="hidden" id="formtotal">
            <input name="weight" type="hidden" id="formweight">
            <input name="pickupDate" type="hidden" id="formpickupDate">
            <input name="resourceType" type="hidden" id="formresourceType">
            @if (Route::has('login'))
            @auth
                <div class="form-group" id="form-components" style="padding-top:20px;">
                    <div class="col">
                        <button type="submit" class="btn-md btn-success" value="loggedInUser" name="submitQuoteButton">CONTINUE</button>
                    </div>
                </div>
            @else
                <div class="form-group" id="form-components" style="padding-top:20px;">
                    <div class="col">
                        <button type="submit" class="btn-md btn-success" value="register" name="submitQuoteButton">
                        Register to Continue</button>
                            <div class="help-block">
                                <a href="#">Already Registered?</a>
                            </div>
                    </div>
                </div>
            @endauth
            @endif
        </form>
    </div>
<!-- end of qoute div -->

</div>

    <section id="about-us-section" class="about-us-section">
        <div>
            <h4 class="text-center">ABOUT US</h4>
        </div>
        <div class="row" id="about-us-row">
            <div class="col-6" id="who-we-are">
                             <p>Lorem ipsum dolor sit amet, lobortis elit ut. Dui potenti ac luctus eu orci sit, hymenaeos dapibus diam et, erat ultrices. Sodales interdum ac curabitur consectetuer semper et, neque erat ut felis, mattis aute vulputate lorem hac risus, amet metus orci. Fringilla nullam euismod adipiscing libero orci wisi, auctor non. Facilisis massa erat aut accumsan suspendisse vitae, facilis nunc duis vivamus, rutrum consectetuer amet.</p>

                            <p>Montes sed. Dolor fringilla blandit felis etiam suspendisse euismod, sed porta nonummy nisl elit, ipsum vel litora, est purus eget, at sit. A a vel neque justo leo dolor, mus erat donec lobortis nullam mauris consectetuer, curabitur etiam, vestibulum neque porro purus consectetuer risus, in accumsan. Imperdiet risus quam per elementum, nulla posuere ornare nec vel nihil augue, tortor nec urna sapien pellentesque dolor rutrum. Velit lectus sit, id integer tortor purus qui, vulputate elit placerat vel. Urna quam ante eros ipsum, placerat nulla quis, libero mauris. Sed libero dolor turpis, habitant ac cras consequat nibh. Turpis eget sit suspendisse mi.</p>
                        
            </div>

            <div class="col-6" id="what-we-do">
                
                        
                            <p>Lorem ipsum dolor sit amet, lobortis elit ut. Dui potenti ac luctus eu orci sit, hymenaeos dapibus diam et, erat ultrices. Sodales interdum ac curabitur consectetuer semper et, neque erat ut felis, mattis aute vulputate lorem hac risus, amet metus orci. Fringilla nullam euismod adipiscing libero orci wisi, auctor non. Facilisis massa erat aut accumsan suspendisse vitae, facilis nunc duis vivamus, rutrum consectetuer amet.</p>

                            <p>Montes sed. Dolor fringilla blandit felis etiam suspendisse euismod, sed porta nonummy nisl elit, ipsum vel litora, est purus eget, at sit. A a vel neque justo leo dolor, mus erat donec lobortis nullam mauris consectetuer, curabitur etiam, vestibulum neque porro purus consectetuer risus, in accumsan. Imperdiet risus quam per elementum, nulla posuere ornare nec vel nihil augue, tortor nec urna sapien pellentesque dolor rutrum. Velit lectus sit, id integer tortor purus qui, vulputate elit placerat vel. Urna quam ante eros ipsum, placerat nulla quis, libero mauris. Sed libero dolor turpis, habitant ac cras consequat nibh. Turpis eget sit suspendisse mi.</p>
                        
            </div>
        </div>
    </section>


    <section id="contact-us-section">
        <div>
            <h4 class="text-center">CONTACT US</h4>
        </div>
        <div class="row" id="contact-us-form">
            <div class="col-12" id="contact-form">
                <form class="contact-form">
                    <div class="form-group" id="name-contact-form"><label>Name *</label><input class="form-control" type="text" required=""></div>
                    <div class="form-group" id="email-contact-form"><label>Email *</label><input class="form-control" type="text" required=""></div>
                    <div class="form-group" id="phone-contact-form"><label>Phone Number</label><input class="form-control" type="tel"></div>
                    <div class="form-group" id="inquiry-contact-form"><label>Message *</label><textarea class="form-control" required=""></textarea></div><button class="btn btn-primary" type="button">SUBMIT</button></form>
            </div>
        </div>
    </section>
    </div>
</div>

    <div class="footer-dark">
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-md-3 item">
                        <h3>Services</h3>
                        <ul>
                            <li><a href="#">Web design</a></li>
                            <li><a href="#">Development</a></li>
                            <li><a href="#">Hosting</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-6 col-md-3 item">
                        <h3>About</h3>
                        <ul>
                            <li><a href="#">Company</a></li>
                            <li><a href="#">Team</a></li>
                            <li><a href="#">Careers</a></li>
                        </ul>
                    </div>
                    <div class="col-md-6 item text">
                        <h3>Company Name</h3>
                        <p>Praesent sed lobortis mi. Suspendisse vel placerat ligula. Vivamus ac sem lacus. Ut vehicula rhoncus elementum. Etiam quis tristique lectus. Aliquam in arcu eget velit pulvinar dictum vel in justo.</p>
                    </div>
                    <div class="col item social"><a href="#"><i class="icon ion-social-facebook"></i></a><a href="#"><i class="icon ion-social-twitter"></i></a><a href="#"><i class="icon ion-social-snapchat"></i></a><a href="#"><i class="icon ion-social-instagram"></i></a></div>
                </div>
                <p class="copyright">Company Name © 2017</p>
            </div>
        </footer>
    </div>



    <div class="modal fade" role="dialog" tabindex="-1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Modal Title</h4><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button></div>
                <div class="modal-body">
                    <p>The content of your modal.</p>
                </div>
                <div class="modal-footer"><button class="btn btn-light" type="button" data-dismiss="modal">Close</button><button class="btn btn-primary" type="button">Save</button></div>
            </div>
        </div>
    </div>

    <script src="{{url('spotta/assets/js/jquery.min.js')}}"></script>
    <script src="{{url('spotta/assets/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>

    <script src="{{url('spotta/assets/js/getQuote.js')}}"></script>
    <script>
        $(document).ready(function() {

                    

                     $('#standardOrigin').on('change', function() {

                    var val = this.value;
                    var _token = $("input[name='_token']").val();

                    document.getElementById('origin2').value = val;
                    var original = document.getElementById('origin2').value;

                    if (original === '') {
                        alert('Select a location');
                    }else if(original !== '')
                    {


                    var APP_URL = {!! json_encode(url('/spotta/')) !!}

                    $.ajax({

                        url: APP_URL+"/checkOrigins",
                        //url: "http://localhost/tmsc/public/checkOrigins",

                        type:'POST',

                        data: {_token:_token, originToServer:original},

                        success: function(data) {

                            if($.isEmptyObject(data.error)){

                                var length = data.success;
                                var trueLength = length.length;

                                if(trueLength > 0){
                                    var catOptions = "";
                                    for(var i = 0; i < trueLength; i++) {

                                        var company = data.success[i].destination;

                                        catOptions += "<option value="+ company +">" + company + "</option>";
                                    }

                                   document.getElementById("destinationSelect").innerHTML = catOptions;
                                    
                                }else{
                                    alert("nothing");
                                }



                            }else{

                                alert('error');

                            }

                        }

                    });
                }
            })
        });
    </script>


<script>
    $(document).ready(function() {

        $(".getQuoteButton").unbind('click').click(function(e){

            e.preventDefault();

            $("#getQuoteForm").css("display", "none");
            $("#quoteDiv").fadeIn("slow");  

            e.preventDefault();

        var APP_URL = {!! json_encode(url('/spotta/')) !!}

        $.ajax({

            async:false,

            processData: false,

            contentType: false,

            url: APP_URL+"/getQuote",

            type:'POST',

            data: new FormData($("#getQuoteForm")[0]),

            success: function(data) {

                if($.isEmptyObject(data.error)){

                    $("#description").html(data.success.description);
                    $("#origin").html(data.success.origin);
                    $("#destination").html(data.success.destination);
                    $("#weight").html(data.success.weight);
                    $("#amount").html(data.success.amount);
                    $("#vat").html(data.success.vat);
                    $("#total").html(data.success.total);

                    $("#formDescription").val(data.success.description);
                    $("#formOrigin").val(data.success.origin);
                    $("#formDestination").val(data.success.destination);
                    $("#formweight").val(data.success.weight);
                    $("#formAmount").val(data.success.amount);
                    $("#formVat").val(data.success.vat);
                    $("#formtotal").val(data.success.total);
                    $("#formpickupDate").val(data.success.pickupDate);
                    $("#formresourceType").val(data.success.resourceType);

                }else{

                    alert(data.error);

                }

            }

        });

        });

    });
</script>

/<script>
// $(document).ready(function() {

//     $(".register").unbind('click').click(function(e){

//         e.preventDefault();

//         /*$("#quoteDiv").css("display", "none");
//         $("#getQuoteForm").fadeIn("slow");*/  

//         e.preventDefault();

//         var APP_URL = {!! json_encode(url('/spotta/')) !!}

//         $.ajax({

//             async:false,

//             processData: false,

//             contentType: false,

//             url: APP_URL+"/submitquote",

//             type:'POST',

//             data: new FormData($("#quotesubmit")[0]),

//             success: function(data) {

//                 if($.isEmptyObject(data.error)){

//                     alert(data.success);

//                     $("#description").html(data.success.description);
//                     $("#origin").html(data.success.origin);
//                     $("#destination").html(data.success.destination);
//                     $("#weight").html(data.success.weight);
//                     $("#amount").html(data.success.amount);
//                     $("#vat").html(data.success.vat);
//                     $("#total").html(data.success.total);

//                     $("#formDescription").val(data.success.description);
//                     $("#formOrigin").val(data.success.origin);
//                     $("#formDestination").val(data.success.destination);
//                     $("#formweight").val(data.success.weight);
//                     $("#formAmount").val(data.success.amount);
//                     $("#formVat").val(data.success.vat);
//                     $("#formtotal").val(data.success.total);

//                 }else{

//                     alert(data.error);
//                 }
//             }
//         });
//     });
// });
// </script>

</body>

</html>