
    @include('client.includes.header')
    @include('client.includes.head')
    <!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-content-white page-md">
        <div class="page-wrapper">
            <!-- BEGIN HEADER -->
                @include('client.includes.menuBar')

            <!-- END HEADER -->
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
            <!-- END HEADER & CONTENT DIVIDER -->
            <!-- BEGIN CONTAINER -->
            <div class="page-container">
                <!-- BEGIN SIDEBAR -->

                <!-- END SIDEBAR -->
                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content" style="margin-left: 0px;">
                        <!-- BEGIN PAGE HEADER-->
                        <!-- BEGIN THEME PANEL -->
                       
                        <!-- END THEME PANEL -->
                        <!-- BEGIN PAGE BAR -->
                        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    <a href="index.html">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>General</span>
                                </li>
                            </ul>
                            
                        </div>
                        <!-- END PAGE BAR -->
                        <!-- BEGIN PAGE TITLE-->
                       
                        <!-- END PAGE TITLE-->
                        <!-- END PAGE HEADER-->
                        <div class="invoice">
                        @foreach($invoiceDetails as $inv)

                          <div class="" style="background: white;padding: 5px;">
                            <div class="flex-space-between flex-container">
                                <div class="">
                                   <div style="margin:20px 0px;">
                                      <img src="{{ url('assets/layouts/layout/img/logo2.jpg')}}" alt="truckka logo" class="logo-default" style="width: 30%;" /> 
                                   </div>
                                  
                                   <div class="flex-container" style="padding: 5px;">
                                     <div class="" style="padding:10px;padding-right: 20px;">
                                       <div>
                                         <h4 class="bold f-paragraph-2 margin-0 padding-0" style="margin: 0px;">INVOICE INFORMATION</h4>
                                       </div>
                                       <div class="flex-container">
                                        <div class="" style="padding: 10px;padding-left: 0px;">
                                            <div>                                            
                                                <p class="f-paragraph bold margin-0 padding-0" padding:0px;>Invoice Date</p>
                                            </div>
                                            <div>
                                                <p class="f-paragraph" style="margin:0px;padding:0px;" >{{ ucfirst(date('jS F Y')) }}</p>
                                            </div>
                                        </div>
                                        <div class="" style="padding: 10px;">
                                            <div>                                            
                                                <p class="f-paragraph bold margin-0 padding-0" padding:0px;margin:0px;font-weight:lighter>Invoice Number</p>
                                            </div>
                                            <div>
                                                <p class="f-paragraph margin-0 padding-0" >{{$inv->invoiceNo}}`</p>
                                            </div>
                                        </div>
                                         <!-- <p class="f-paragraph" style="margin: 0px;">Tincan Apapa</p> -->
                                       </div>
                                     </div>
                                    <div class="" style="padding:10px;">
                                       <div>
                                         <h4 class="bold f-paragraph-2 margin-0 padding-0" style="margin: 0px;">PAYMENT INFORMATION</h4>
                                       </div>
                                       <div class="flex-container">
                                        <div class="col44" style="padding: 10px;padding-left: 0px;">
                                            <div>                                            
                                                <p class="f-paragraph bold margin-0 padding-0" padding:0px;>Bank</p>
                                            </div>
                                            <div>
                                                <p class="f-paragraph" style="margin:0px;padding:0px;" >First City Monument Bank Limited</p>
                                            </div>
                                        </div>
                                        <div class="col33" style="padding: 10px 0px;">
                                            <div>                                            
                                                <p class="f-paragraph bold margin-0 padding-0" padding:0px;margin:0px;font-weight:lighter>Account Name</p>
                                            </div>
                                            <div>
                                                <p class="f-paragraph margin-0 padding-0" >TRUCKKA Logistics Limited</p>
                                            </div>
                                        </div>
                                        <div class="col44" style="padding: 10px;">
                                            <div>                                            
                                                <p class="f-paragraph bold margin-0 padding-0" padding:0px;margin:0px;font-weight:lighter>Account Number</p>
                                            </div>
                                            <div>
                                                <p class="f-paragraph margin-0 padding-0" >#00000000000</p>
                                            </div>
                                        </div>
                                         <!-- <p class="f-paragraph" style="margin: 0px;">Tincan Apapa</p> -->
                                       </div>
                                     </div>
                                   </div>

                                    
                                </div>
                                <div class="" style="text-align: right;margin: 20px 0px;">
                                    <p class="f-paragraph-2 bold margin-0" style="padding:10px;">
                                        BILLED TO
                                    </p>
                                    <p class="f-paragraph bold margin-0 padding-0">
                                       {{$companyDetail->companyName}}
                                    </p>
                                    <p class="f-paragraph margin-0 padding-0">
                                       {{$companyDetail->address}}
                                    </p>
                                   
                                </div>
                            </div>
                            <div class="row" >
                                <div class="col-xs-12" style=" margin-top: 50px;">
                                    <table class="table table-hover table-responsive">
                                        <thead class=" white" style="background: #F9A81A;">

                                          <tr>
                                              <!-- <td><strong>Date</strong></td> -->
                                              <td class="text-center"><strong>ORIGIN</strong></td>
                                              <td class="text-center"><strong>DESTINATION</strong></td>

                                              <td class="text-center"><strong>TRUCK NO</strong></td>

                                              <td class="text-center"><strong>WAYBILL NO</strong></td><!-- 
                                              <td class="text-center"><strong>CUSTOMER NAME</strong></td> -->

                                              <td class="text-center"><strong>QTY</strong></td>

                                          </tr>
                                        </thead>
                                    <tbody>
                                    @foreach($resDetsResult as $invoice)
                                      @foreach($invoice as $in)
                                      <tr>
                                        <!-- <td class="text-center">30-Aug</td> -->
                                        <td class="text-center">{{$in['origin']}}</td>
                                        <td  class="text-center"> {{$in['deliverTo']}}</td>
                                        <td class="text-center" style="text-transform: uppercase;">{{$in['plateNumber']}}</td>
                                        <td class="text-center">{{$in['waybillNo']}}</td>
<!--                                         <td class="text-center">Kano Warehouse</td>
 -->                                        <td class="text-center">
                                            <!--   pollyfill for invoices with numberofbags missing -->
                                              @if(isset($in['numberOfBags']))
                                                {{$in['numberOfBags']}}
                                              @endif

                                            </td>
                                      </tr>
                                      @endforeach
                                    @endforeach
                                     
                                      <tr class="">
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td class="text-center"><b>Gross Amount</b></td>
                                        <td class="text-center"><b>&#8358;{{number_format($inv->price, 2, '.', ',')}} </b></td>
                                      </tr>
                                      <tr class="gross-amount">
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>

                                        <td class="text-center"><b>VAT (5%)</b></td>
                                        <td class="text-center"><b>&#8358;{{number_format($vat, 2, '.', ',')}} </b></td>
                                      </tr>

                                    </tbody>
                            </table>
                                <div class="flex-space-between flex-container" style="align-items: start;">
                                    
                                    <div class="truckka-back flex-container">
                                        <div class="col44">
                                            <p class="f-paragraph">Grand Total</p>
                                        </div>
                                        <div class="col66">
                                            <p class="f-paragraph bold" style="text-transform: uppercase;">&#8358; {{number_format($pricePlusVat, 2, '.', ',')}}</p>
                                        </div>
                                    </div> 
                                </div>
                                <div class="truckka-back" style="padding: 5px; margin-top: 20px;">
                                        <p class="white align-center f-paragraph bold margin-0 padding-0"> 14, Adebisi Close, Idiroko Estate Maryland, Lagos | +234 (0) 805 309 9999 | +234 (0) 809 309 1111 | truckka.ng</p>
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="row">
                              <div class="col-md-12">
                                <p class="f-paragraph" style="margin:0px">Thank you for your business!</p>
                              </div>
                              <div class="col-md-12">
                                <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ_OaINPT1iIUcPE_SiQHB0Rt8-I-HTCOeGvWpx09AqXUpOtQx0" style="width: 100px;">
                              </div>
                              
                            </div> -->
                          </div>
                        @endforeach
                            <div class="row" style="margin-top: 50px;">
                                <div class="col-xs-4 invoice-block">
                                    
                                    <a class="btn btn-lg blue hidden-print margin-bottom-5" onclick="javascript:window.print();"> Print
                                        <i class="fa fa-print"></i>
                                    </a>
                                    
                                </div>
                               
                                
                            </div>
                            
                      </div>
                    </div>
                    <!-- END CONTENT BODY -->
                </div>
                <!-- END CONTENT -->
                <!-- BEGIN QUICK SIDEBAR -->
                <a href="javascript:;" class="page-quick-sidebar-toggler">
                    <i class="icon-login"></i>
                </a>
               
                <!-- END QUICK SIDEBAR -->
            </div>
            <!-- END CONTAINER -->
            <!-- BEGIN FOOTER -->
            
            <!-- END FOOTER -->
        </div>
        <!-- BEGIN QUICK NAV -->
        <nav class="quick-nav">
            <a class="quick-nav-trigger" href="#0">
                <span aria-hidden="true"></span>
            </a>
           
            <span aria-hidden="true" class="quick-nav-bg"></span>
        </nav>
        <div class="quick-nav-overlay"></div>
        <!-- END QUICK NAV -->
        <!--[if lt IE 9]>
<script src="../assets/global/plugins/respond.min.js"></script>
<script src="../assets/global/plugins/excanvas.min.js"></script> 
<script src="../assets/global/plugins/ie8.fix.min.js"></script> 
<![endif]-->
      @include('client.includes.footer')