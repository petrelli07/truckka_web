
    @include('carrier.includes.header')
    @include('carrier.includes.head')
    <!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-content-white page-md">
      
        <div class="page-wrapper">
            <!-- BEGIN HEADER -->
                @include('carrier.includes.menuBar')

            <!-- END HEADER -->
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
            <!-- END HEADER & CONTENT DIVIDER -->
            <!-- BEGIN CONTAINER -->
            <div class="page-container">
                <!-- BEGIN SIDEBAR -->
                    @include('carrier.includes.sidebar')
                <!-- END SIDEBAR -->
                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <!-- BEGIN PAGE HEADER-->
                        <!-- BEGIN THEME PANEL -->
                       
                        <!-- END THEME PANEL -->
                        <!-- BEGIN PAGE BAR -->
                        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    <a href="index.html">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Dashboard</span>
                                </li>
                            </ul>
                            <div class="page-toolbar">
                                <div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
                                    <i class="icon-calendar"></i>&nbsp;
                                    <span class="thin uppercase hidden-xs"></span>&nbsp;
                                    <i class="fa fa-angle-down"></i>
                                </div>
                            </div>
                        </div>
                        <!-- END PAGE BAR -->
                        <!-- BEGIN PAGE TITLE-->
                        <h1 class="page-title">
                        </h1>
                        <!-- END PAGE TITLE-->
                        <!-- END PAGE HEADER-->
<!--  -->                        <div class="row">
                            <div class="col-lg-10 col-lg-offset-1 col-xs-12 col-sm-12">
                                <div class="portlet light ">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <span class="caption-subject font-dark bold uppercase">Add A Resource</span>                                         
                                      </div>
                                        
                                    </div>
                                     <div class="portlet-body form form-container">
                                        <form role="form" class="resourceForm ajax-form-submit " method="post" id="userform" ajax-submit="true" action="{{url('/spotta/carrier/resources/add-new-resource')}}">
                                         {{csrf_field()}}
                                            <div class="form-body">
                                              <div class="row">
                                                <div class="col-md-6">
                                                  <div class="form-group form-md-line-input">
                                                    <input type="text" class="form-control form-copy validate" id="itemDescription" placeholder="Enter Description Here"  bind-to="summaryHaulageDec" name="platenumber">
                                                    <label for="form_control_1">Plate Number</label>
                                                    <span class="help-block">Please provide the correct plate number  </span>
                                                  </div>
                                                </div>
                                                <div class="col-md-6">
                                                   <div class="form-group form-md-line-input has-info">
                                                     
                                                      <select class="form-control form-copy validate " id="form_control_1 "  name="capacity"  bind-to="summaryRes" >
                                                            <option value="">[-Specify the capacity-]</option>
                                                            <option value="0"></option>
                                                      </select>
                                                      <label for="form_control_1">Capacity</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                  <div class="form-group form-md-line-input">
                                                    <input type="text" class="form-control form-copy validate" id="itemDescription" placeholder="Enter Description Here"  bind-to="summaryHaulageDec" name="drivername">
                                                    <label for="form_control_1">Driver's Full Name</label>
                                                    <span class="help-block">Please give the full name of the driver</span>
                                                  </div>
                                                </div>
                                                <div class="col-md-6">
                                                  <div class="form-group form-md-line-input">
                                                    <input type="text" class="form-control form-copy validate" id="itemDescription" placeholder="e.g 08154098158,0803118103"  bind-to="summaryHaulageDec" name="drivernumber">
                                                    <label for="form_control_1">Contact Number</label>
                                                    <span class="help-block">For multiple numbers, seperate with a comma ','</span>
                                                  </div>
                                                </div>
                                                <div class="col-md-6">
                                                  <div class="form-group form-md-line-input">
                                                    <input type="number" class="form-control form-copy validate" id="itemDescription" placeholder="e.g 08154098158,0803118103"  bind-to="summaryHaulageDec" name="imei">
                                                    <label for="form_control_1">Tracker's Imei</label>
                                                    <span class="help-block">use '00000' for no imeis'</span>
                                                  </div>
                                                </div>
                                                
                                              </div>
                                            
                                            </div>
                                            <div class="form-actions noborder">
                                                <button type="submit" class="btn btn-primary validate-form-button" >Create Resource</button>
                                               
                                            </div>
                                        </form>
                                    </div>
                                    <div class="flex-container flex-center">
                                       <button class="btn btn-danger duplicator" duplicate-dom="userform"  append-to="form-container" style="margin-top: 50px;">Add new resource</button>
                                    </div>
                                     <div class="flex-container">
                                       <button class="btn btn-success save-all" form-group-name="resourceForm" style="margin-top: 50px;">Save All</button>
                                    </div>
                                    
                                </div>
                            </div>
                            
                        </div>
                        
                    </div>
                    <!-- END CONTENT BODY -->
                </div>
                <!-- END CONTENT -->
                <!-- BEGIN QUICK SIDEBAR -->
              
                <!-- END QUICK SIDEBAR -->
            </div>
            <!-- END CONTAINER -->
            <!-- BEGIN FOOTER -->
            <div class="page-footer">
                <div class="page-footer-inner"> 2018 &copy; Truckka  &nbsp;|&nbsp;
                   
                </div>
                <div class="scroll-to-top">
                    <i class="icon-arrow-up"></i>
                </div>
            </div>
            <!-- END FOOTER -->
        </div>
        <!-- BEGIN QUICK NAV -->
        
        <div class="quick-nav-overlay"></div>
 @include('carrier.includes.footer')
