@extends('tmsw.client.layout.content')
@section('content')

<div class="row">
    <div class="box col-md-12">
        <!--if @ session -->
        @if(session('message'))
            <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    {{session('message')}}
            </div>
        @endif
        <!--endif-->

        <!--endif-->
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-plus"></i> Create New Department</h2>

            </div>
            <div class="box-content row">
            <form role="form" method="post" class="validate"action="{{url('tmsw/admin/group/save')}}"  enctype="multipart/form-data">
                 {{ csrf_field() }}                      
                 
                 <div class="box col-md-6">
                    <div class="form-group">
                        <label class="control-label" for="pick-up-date"> Department Name</label>
                        <input type="text" name="name" class="form-control required"  placeholder="Enter Name" value="{{old('name')}}">
                        @if($errors->has('name'))
                            <div class="alert alert-info">
                                <strong>{{ $errors->first('name') }}</strong>
                            </div>
                        @endif
                    </div>
                </div>

                        
                 
                
                <div class="box col-md-6">
                    <div class="form-group">
                        <label class="control-label" for="origin">User Role</label>
                        <div class="controls">
                            <select id="selectError" name="roles[]" class="col-md-12" multiple="multiple" data-rel="chosen" name="userlevel">
                            <option value='0'>choose a role</option>
                            @foreach($roles as $role)
                            <option value='{{$role->id}}'>{{$role->name}}</option>

                            @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                
                <div class="box col-md-12">
                    <div class="form-group">
                        <button type="submit" class="btn btn-custom">
                            <i class="glyphicon glyphicon-plus"></i>Submit
                        </button>
                    </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
    <!--/span-->

</div><!--/row-->



@endsection