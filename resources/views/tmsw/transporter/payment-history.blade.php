@extends('tmsw.transporter.layout.content')
@section('content')



<div class="row">
    <div class="box col-md-12">
    <div class="box-inner">
    <div class="box-header well" data-original-title="">
        <h2><i class="glyphicon glyphicon-eye-open"></i> Payment History</h2>
    </div>
    <div class="box-content">
    <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
    <thead>

    <tr>
        
        <th>Order Number</th>
        <th>Origin</th>
        <th>Destination</th>
        <th>Plate Number</th>
        <th>Driver Name</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    @forelse($user as $users)
    <tr>
        <td class="center">{{$users->reference_number}}</td>
        <td class="center">{{$users->originStateName}}</td>
        <td class="center">{{$users->destStateName}}</td>
        <td class="center">{{$users->plateNumber}}</td>
        <td class="center">{{$users->driverName}}</td>
        <td class="center">
            @if($users->waybillNumber == Null)
            <a class="btn btn-info btn-sm" href='{{url("tmsw/transporter/upload_waybill/{$users->id}")}}'>
                Upload Waybill Info
            </a>
            @else
            <a class="btn btn-info btn-sm" href='{{url("tmsw/transporter/invoice_details/{$users->id}")}}'>
                View Invoice Details
            </a>
            @endif
        </td>
    </tr>
    @empty
    <tr>
        <td class="center"></td>
        <td class="center"></td>
        <td class="center"></td>
        <td class="center"></td>
        <td class="center"></td>
        <td class="center"></td>
        <td class="center"></td>
    </tr>
    @endforelse
    </tbody>
    </table>
    </div>
    </div>
    </div>
    <!--/span-->

    </div>



@endsection