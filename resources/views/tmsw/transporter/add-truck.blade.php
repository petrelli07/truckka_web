@extends('tmsw.transporter.layout.content')
@section('content')

<!--@add more rows dynamically-->
<script type="text/javascript">
    $(document).ready(function() {
        var max_fields      = 5; //maximum input boxes allowed
        var wrapper         = $(".wrap"); //Fields wrapper
        var add_button      = $(".add_field_button"); //Add button ID

        var x = 1; //initlal text box count
        $(add_button).click(function(e){ //on add input button click

            e.preventDefault();
            if(x < max_fields){ //max input box allowed
                x++; //text box increment
                $(wrapper).append('<div class="box-content row"><div class="box col-md-4"><div class="form-group"><label class="control-label" for="truck_plate_number">Truck Plate Number</label><input type="text" name="truck_plate_number[]" class="form-control required" placeholder="Truck Plate Number"></div></div><div class="box col-md-4"><div class="form-group"><label class="control-label" for="driver_name">Driver Name</label><input type="text" name="driver_name[]" class="form-control required" placeholder="Driver Name"></div></div><div class="box col-md-4"><div class="form-group"><label class="control-label" for="driver_name">Driver Phone</label><input type="text" name="driver_phone[]" class="form-control required" placeholder="Driver Phone"></div></div><a href="#" class="remove_field_prerequsite">Remove</a></div>'); //add input box
            }
        });

        $(wrapper).on("click",".remove_field_prerequsite", function(e){ //user click on remove text
            e.preventDefault(); $(this).parent('div').remove(); x--;
        })
    });
</script>
<!--@end add more rows dynamically-->

<div class="row">
    <div class="box col-md-12">
        <!--if @ session -->
            @if (session('message'))
            <div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert">x</button>
                {{ session('message') }}
            </div>
            @endif
        <!--endif-->





        <form role="form" method="post" class="validate"  action="{{url('/tmsw/transporter/add_truck_details')}}"  enctype="multipart/form-data">
        <input type="hidden" name="order_id" value="{{$order_id}}">
            {{csrf_field()}}
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-plus"></i> Add New Truck</h2>

            </div>
            <div class="box-content row">


                <div class="box col-md-4">
                    <div class="form-group">
                        <label class="control-label" for="truck_plate_number">Truck Plate Number</label>
                        <input type="text" name="truck_plate_number[]" class="form-control required" placeholder="Truck Plate Number">
                    </div>
                </div>

                <div class="box col-md-4">
                    <div class="form-group">
                        <label class="control-label" for="driver_name">Driver Name</label>
                        <input type="text" name="driver_name[]" class="form-control required" placeholder="Driver Name">
                    </div>
                </div>

                <div class="box col-md-4">
                    <div class="form-group">
                        <label class="control-label" for="driver_name">Driver Phone</label>
                        <input type="text" name="driver_phone[]" class="form-control required" placeholder="Driver Phone">
                    </div>
                </div>

                <div class="box col-md-8 wrap">
                    <div class="form-group">
                        <a class="btn btn-sm btn-success add_field_button">
                            <i class="glyphicon glyphicon-plus"></i>Add More
                        </a>
                    </div>
                </div>

            </div>
        </div>

            <div class="box col-md-6">
                <div class="form-group">
                    <button type="submit" class="btn btn-custom">
                        <i class="glyphicon glyphicon-plus"></i>Submit
                    </button>
                </div>
            </div>

        </form>
    </div>
    <!--/span-->

</div><!--/row-->



@endsection