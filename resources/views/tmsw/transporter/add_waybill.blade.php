@extends('tmsw.transporter.layout.content')
@section('content')

<div class="row">
    <div class="box col-md-12">
        <!--if @ session -->
        @if (session('message'))
        <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert">x</button>
            {{ session('message') }}
        </div>
        @endif
        <!--endif-->





        <form role="form" method="post" class="validate"  action="{{url('/tmsw/transporter/submit_waybill')}}"  enctype="multipart/form-data">
            {{csrf_field()}}
            <input type="hidden" name="truck_order_id" value="{{$id}}">
            <div class="box-inner">
                <div class="box-header well" data-original-title="">
                    <h2><i class="glyphicon glyphicon-plus"></i> Submit Waybill For Payment</h2>

                </div>
                <div class="box-content row">


                    <div class="box col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="truck_plate_number">Waybill Number</label>
                            <input type="text" name="waybill_number" class="form-control required" placeholder="Enter Waybill Number">
                        </div>
                    </div>

                    <div class="box col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="driver_name">Upload Waybill</label>
                            <input type="file" name="waybill_file" class="form-control required" required>
                        </div>
                    </div>



                </div>
            </div>

            <div class="box col-md-6">
                <div class="form-group">
                    <button type="submit" class="btn btn-custom">
                        <i class="glyphicon glyphicon-plus"></i>Submit
                    </button>
                </div>
            </div>
        </form>
    </div>
    <!--/span-->

</div><!--/row-->



@endsection