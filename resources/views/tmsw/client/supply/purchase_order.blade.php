@extends('tmsw.client.layout.content')
@section('content')


<div class="row" xmlns="http://www.w3.org/1999/html">
    <div class="box col-md-12">


        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-plus"></i> View More</h2>

            </div>
            <div class="box-content row">
                <!--if @ session -->
                @if (session('message'))
                <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    {{ session('message') }}
                </div>
                @endif
                <!--endif-->
                <div class="box col-md-12">
                    <div class="box col-md-12">

                        <img src="{{url('/tmsw/clientPurchaseOrders/'.$purchase_order)}}"  height="500" width="100%" class="img-responsive">

                    </div>
                </div>

                <div class="box col-md-12">
                    <div class="box-content">
                        <table class="table table-striped table-bordered  responsive">
                            <thead>
                            <tr>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td class="center">
                                    <a class="btn btn-success btn-md" href='{{url("tmsw/supplier/supply/order/all")}}'>Go Back</a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                </div>






            </div>
        </div>
    </div>
    <!--/span-->

</div><!--/row-->



@endsection