@extends('tmsw.client.layout.content')
@section('content')



<div class="row">
    <div class="box col-md-12">
    <div class="box-inner">
    <div class="box-header well" data-original-title="">
        <h2><i class="glyphicon glyphicon-eye-open"></i>{{$tableTitle}}</h2>
    </div>
    <div class="box-content">
    <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
    <thead>

    <tr>
        
        <th>S/N</th>
        <th> Name</th>
        <th>status</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
        @foreach($groups as $group)
    <tr>
        
        <td class="center">1</td>
        <td class="center">{{$group->groupName}}</td>
        <td class="center">

          @if($group->status == 1)
            Active
          @elseif($group->status == 2)
            Deactive
          @endif

        </td>
        <td class="center">
          @if($group->status == 1)
            <a class="btn btn-info btn-sm" href="{{url('tmsw/supplier/status/group/'.$group->id.'/deactivate')}}">
                <i class="glyphicon glyphicon-eye-open icon-white"></i>
                Deactivate
            </a>
          @elseif($group->status == 2)
            <a class="btn btn-info btn-sm"  href="{{url('tmsw/supplier/status/group/'.$group->id.'/activate')}}">
                <i class="glyphicon glyphicon-eye-open icon-white"></i>
                Activate
            </a>
          @endif
            
             <a class="btn btn-danger btn-sm"  href="{{url('tmsw/supplier/edit/'.$group->id)}}">
                <i class="glyphicon glyphicon-eye-open icon-white"></i>
               Edit
            </a>
        </td>
    </tr>
    @endforeach
    </tbody>
    </table>
    </div>
    </div>
    </div>
    <!--/span-->

    </div>



@endsection