@extends('tmsw.client.layout.content')
@section('content')

<div class="row">
    <div class="box col-md-12">
        @if(session('message'))
        <!--if @ session -->
            <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                  {{session('message')}}
            </div>
        <!--endif-->
        @endif
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-plus"></i> Create New Orders</h2>

            </div>
            <div class="box-content row">
                 <form role="form" method="post" class="validate"  action="@if($supplier == null){{url('tmsw/supplier/save')}}@else{{url('tmsw/supplier/edit/save/'.$supplier->id)}}@endif"  enctype="multipart/form-data">
                    @csrf
                    <div class="box col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="weight">Supplier Name</label>
                            <input type="text" name="supplier_name" class="form-control required"  placeholder="Name" value="@if(!is_null($supplier)){{$supplier->supplierName}}@endif
                            ">
                        </div>
                    </div>
                    <div class="box col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="weight">Supplier Address</label>
                            <input type="text" name="supplier_address" class="form-control required" placeholder="Address"value="@if(!is_null($supplier)){{$supplier->supplierAddress}}@endif"
                            >
                        </div>
                    </div>
                    @if($supplier == null)
                    <div class="box col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="weight">Supplier Email</label>
                            <input type="email" name="email" class="form-control required" placeholder="email">
                        </div>
                    </div>
                    <div class="box col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="weight">Account Holder Name</label>
                            <input type="text" name="name" class="form-control required" placeholder="Address">
                        </div>
                    </div>
                    @endif


                 <div class="box col-md-12">
                    <div class="form-group">
                        <label class="control-label" for="origin">Add to group</label>
                        <div class="controls">
                            <select id="selectError" class="col-md-12" multiple="multiple" data-rel="chosen" name="group[]">
                           @foreach($groups as $group)
                            <option value='{{$group->id}}'
                            @if(!is_null($supplier) && in_array($group->id,$selectedGroups))
                                selected="selected"
                            @endif
                            >{{$group->groupName}}</option>
                            @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="box col-md-6">
                    <div class="form-group">
                        <button type="submit" class="btn btn-custom">
                            <i class="glyphicon glyphicon-plus"></i>Submit
                        </button>
                    </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
    <!--/span-->

</div><!--/row-->



@endsection