@extends('tmsw.client.layout.content')
@section('content')



<div class="row">
    <div class="box col-md-12">
    <div class="box-inner">
    <div class="box-header well" data-original-title="">
        <h2><i class="glyphicon glyphicon-eye-open"></i> My Suppliers</h2>
    </div>
    <div class="box-content">
    <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
    <thead>

    <tr>
        
        <th>S/N</th>
        <th> Description</th>
        <th>Quantity</th>
        <th>Created By</th>
        <th>status</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
      <?php  $counter = 1;?>
        @foreach($supplies as $supply)
    <tr>
        
        <td class="center">{{$counter++}}</td>
        <td class="center">{{$supply->orderDescription}}</td>
        <td class="center">{{$supply->orderQuantity}}</td>
        <td class="center">{{$supply->name}}</td>
        <td>

         @if($supply->supply_status == 1 &&  strtotime(date("Y-m-d h:i:sa")) < strtotime($supply->expires_at))
                Active
            @elseif($supply->supply_status == 2 && strtotime(date("Y-m-d h:i:sa")) < strtotime($supply->expires_at))
                De-Active
            @else
              Expired
            @endif
          </td>
        <td class="center">
           @if($supply->supply_status == 1 && strtotime(date("Y-m-d h:i:sa")) < strtotime($supply->expires_at))
            <a class="btn btn-info btn-sm" href="{{url('tmsw/supplier/status/supply/'.$supply->id.'/deactivate')}}">
                <i class="glyphicon glyphicon-eye-open icon-white"></i>
                Cancel
            </a>
            @elseif($supply->supply_status == 2 && strtotime(date("Y-m-d h:i:sa")) < strtotime($supply->expires_at))
               <a class="btn btn-info btn-sm"  href="{{url('tmsw/supplier/status/supply/'.$supply->id.'/activate')}}">
                <i class="glyphicon glyphicon-eye-open icon-white"></i>
                Activate
            </a>
            @endif
            
            <a class="btn btn-danger btn-sm" href="{{url('tmsw/supply/order/participant/'.$supply->order_id)}}">
                <i class="glyphicon glyphicon-eye-open icon-white"></i>
               Participants
            </a>

             <a class="btn btn-danger btn-sm" href="{{url('tmsw/supply/order/view_purchase_order/'.$supply->order_id)}}">
                <i class="glyphicon glyphicon-eye-open icon-white"></i>
               Purchase Order
            </a>
        </td>
    </tr>
    @endforeach
    </tbody>
    </table>
    </div>
    </div>
    </div>
    <!--/span-->

    </div>



@endsection