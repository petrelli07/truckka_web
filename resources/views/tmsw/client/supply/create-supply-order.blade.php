@extends('tmsw.client.layout.content')
@section('content')

<div class="row">
    <div class="box col-md-12">

        @if(session('message'))
        <!--if @ session -->
            <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                  {{session('message')}}
            </div>
        <!--endif-->
        @endif

        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-plus"></i> Create New Supply Order</h2>

            </div>
            <div class="box-content row">
                 <form role="form" method="post" class="validate"  action="{{url('tmsw/supplier/supply/order/save')}}"  enctype="multipart/form-data">
                 

                @csrf

                 <div class="box col-md-6">
                    <div class="form-group">
                        <label class="control-label" for="weight">Item</label>
                        <input type="text" name="Item" class="form-control required" placeholder="">
                    </div>
                </div>
            <div class="box col-md-6">
                <div class="form-group">
                        <label class="control-label" for="pick-up-date">Quantity</label>
                        <input type="date" name="quantity" class="form-control required" placeholder="E.g 50 bags, 30 cartons">
                </div>
            </div>
            <div class="box col-md-6">
                <div class="form-group">
                        <label class="control-label" for="pick-up-date">Expiration Date</label>
                        <input type="text" name="expiration" class="form-control required" data-field="date">
                </div>
            </div>
            
            <div class="box col-md-6">
                <div class="form-group">
                        <label class="control-label" for="pick-up-date">Upload purchase order</label>
                        <input type="file" name="file" class="form-control required">
                </div>
            </div>
                    
                <div id="dtBox"></div>

                <div class="box col-md-12">
                    <div class="form-group">
                        <label class="control-label" for="origin">Send to group</label>
                        <div class="controls">
                            <select id="selectError" class="col-md-12" multiple="multiple" data-rel="chosen" name="group[]">
                           @foreach($groups as $group)
                            <option value='{{$group->id}}'>{{$group->groupName}}</option>
                            @endforeach
                            </select>
                        </div>
                    </div>
                </div>

              
                <div class="box col-md-6">
                    <div class="form-group">
                        <button type="submit" class="btn btn-custom">
                            <i class="glyphicon glyphicon-plus"></i>Submit
                        </button>
                    </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
    <!--/span-->

</div><!--/row-->




@endsection