@extends('tmsw.client.layout.content')
@section('content')

<div class="row">
    <div class="box col-md-12">
        <!--if @ session -->
        @if(session('message'))
            <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    {{session('message')}}
            </div>
        @endif
        <!--endif-->

        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-plus"></i> Create New User</h2>

            </div>
            <div class="box-content row">
            <form role="form" method="post" class="validate"action="{{ route('client.store.user')}}"  enctype="multipart/form-data">
                 {{ csrf_field() }}                      
                 
                 <div class="box col-md-6">
                    <div class="form-group">
                        <label class="control-label" for="pick-up-date">Name</label>
                        <input type="text" name="name" class="form-control required"  placeholder="Enter Name" value="{{old('name')}}">
                        @if($errors->has('name'))
                            <div class="alert alert-info">
                                <strong>{{ $errors->first('name') }}</strong>
                            </div>
                        @endif
                    </div>
                </div>

                <div class="box col-md-6">
                    <div class="form-group">
                        <label class="control-label" for="pick-up-date">Email</label>
                        <input type="text" name="email" class="form-control required"  placeholder="Enter Email Address" value="{{old('email')}}">
                        @if($errors->has('email'))
                            <div class="alert alert-info">
                                <strong>{{ $errors->first('email') }}</strong>
                            </div>
                        @endif
                    </div>
                </div>

                <div class="box col-md-6">
                    <div class="form-group">
                        <label class="control-label" for="pick-up-date">Phone</label>
                        <input type="text" name="phone" class="form-control required"  placeholder="Enter Phone" value="{{old('phone')}}">
                        @if($errors->has('phone'))
                            <div class="alert alert-info">
                                <strong>{{ $errors->first('phone') }}</strong>
                            </div>
                        @endif
                    </div>
                </div>
                        
                  <div class="box col-md-6">
                    <div class="form-group">
                        <label class="control-label" for="origin"> Department</label>
                        <div class="controls">
                            <select id="getRoles" class="col-md-12" multiple="multiple" data-rel="chosen" name="userlevel">
                            <option value='0' >select a department</option>
                            @foreach($groups as $group)
                                <option value='{{$group->id}}' >{{$group->name}}</option>
                            @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                 <div class="box col-md-6">
                    <div class="form-group">
                        <label class="control-label" for="origin">role</label>
                        <div class="controls">
                            <select id="destinationSelect2" class="col-md-12"  name="userlevel">
                                    <option value='0' disabled="disabled">select a role</option>

                            </select>
                        </div>
                    </div>
                </div>


                <!-- <div class="box col-md-12">
                    <div class="form-group">
                        <label class="control-label" for="origin">Company</label>
                        <div class="controls">
                            <select id="selectError" class="col-md-12" data-rel="chosen" name="company">
                            <option value='0'>choose Company</option>
                                @foreach($company as $row)
                                <option  type="hidden" value='{{$row->id}}'>{{ucwords($row->company_name)}}</option>
                                @endforeach
                            
                            </select>
                        </div>
                    </div>
                </div> -->

                
                <div class="box col-md-12">
                    <div class="form-group">
                        <button type="submit" class="btn btn-custom">
                            <i class="glyphicon glyphicon-plus"></i>Submit
                        </button>
                    </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
    <!--/span-->

</div><!--/row-->



@endsection