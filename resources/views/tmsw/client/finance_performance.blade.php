@extends('tmsw.client.layout.content')
@section('content')
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {

        var data = google.visualization.arrayToDataTable([
            ['Task', 'Percentage of Paid Invoices to Unpaid Invoice'],
            ['Paid',     11],
            ['Unpaid',      2],
        ]);

        var options = {
            title: 'Invoice Processing Rate'
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
    }
</script>

<div class="row" xmlns="http://www.w3.org/1999/html">
    <div class="box col-md-12">

        <!--if @ session -->
        @if (session('message'))
        <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert">x</button>
            {{ session('message') }}
        </div>
        @endif

        <!--endif-->

        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-plus"></i> Reports &amp; Analytics</h2>

            </div>
            <div class="box-content row">


                <div class="box col-md-6">

                    <div class="box-content">
                        <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
                            <thead>
                            <tr>
                                <th>Origin</th>
                                <th>Destination</th>
                                <th>Number of Trucks</th>
                                <th>Type of Truck</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td class="center">Lagos</td>
                                <td class="center">Abuja</td>
                                <td class="center">5</td>
                                <td class="center">30 Ton Flatbed</td>
                                <td class="center">Invoice Paid</td>
                            </tr>
                            <tr>
                                <td class="center">Lagos</td>
                                <td class="center">Abuja</td>
                                <td class="center">5</td>
                                <td class="center">30 Ton Flatbed</td>
                                <td class="center">Invoice Paid</td>
                            </tr>
                            <tr>
                                <td class="center">Lagos</td>
                                <td class="center">Abuja</td>
                                <td class="center">5</td>
                                <td class="center">30 Ton Flatbed</td>
                                <td class="center">Invoice Unpaid</td>
                            </tr>
                            <tr>
                                <td class="center">Lagos</td>
                                <td class="center">Abuja</td>
                                <td class="center">5</td>
                                <td class="center">30 Ton Flatbed</td>
                                <td class="center">Invoice Paid</td>
                            </tr>
                            <tr>
                                <td class="center">Lagos</td>
                                <td class="center">Abuja</td>
                                <td class="center">5</td>
                                <td class="center">30 Ton Flatbed</td>
                                <td class="center">Invoice Paid</td>
                            </tr>
                            <tr>
                                <td class="center">Lagos</td>
                                <td class="center">Abuja</td>
                                <td class="center">5</td>
                                <td class="center">30 Ton Flatbed</td>
                                <td class="center">Invoice Unpaid</td>
                            </tr>

                            </tbody>
                        </table>
                    </div>

                </div>


                <div class="box col-md-6">

                    <div class="box-content">
                        <div id="piechart">

                        </div>
                    </div>

                </div>






            </div>
        </div>
    </div>
    <!--/span-->

</div><!--/row-->



@endsection