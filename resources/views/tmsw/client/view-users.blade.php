@extends('tmsw.client.layout.content')
@section('content')



<div class="row">
    <div class="box col-md-12">
     <!--if @ session -->
     @if(session('message'))
            <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    {{session('message')}}
            </div>
        @endif
        <!--endif-->
        
    <div class="box-inner">
    <div class="box-header well" data-original-title="">
        <h2><i class="glyphicon glyphicon-eye-open"></i> All Users</h2>
    </div>
    <div class="box-content">
    <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
    <thead>

    <tr>
        
        <th>ID</th>
        <th>Name</th>
        <th>Email</th>
        <th>Company Name</th>
        <th>Status</th>
        <th>Action</th>
      
    </tr>
    </thead>
    <tbody>
    <?php $x =1; ?>
        @forelse($users as $row)
       
    <tr>
     
        <td class="center">{{$x++}}</td>
        <td class="center">{{$row->name}}</td>
        <td class="center">{{$row->email}}</td>
        <td class="center">{{$row->company_name}}</td>
        @if($row->status == 1)
        <td>Active</td>
        @else
        <td>Suspended</td>
        @endif
        <td class="center">

        @if($row->status == 1)
        <!-- THIS IS TO SUSPEND -->

        <form action="{{ route('client.suspend.user') }}" method="post" class="login"> 
        {{ csrf_field() }}
            <input type="hidden" name="id" value="{{$row->user_id}}"  />
            <button class="btn btn-success btn-sm view-shop" type="submit">
            <i class="glyphicon glyphicon-edit icon-white"></i> Suspend
            </button>
        </form>
         <!-- END OF THIS IS TO SUSPEND -->
         @else
        
        <!-- THIS IS TO SUSPEND -->

        <form action="{{ route('client.unsuspend.user') }}" method="post" class="login"> 
        {{ csrf_field() }}
            <input type="hidden" name="id" value="{{$row->user_id}}"  />
            <button class="btn btn-info btn-sm view-shop" type="submit">
            <i class="glyphicon glyphicon-edit icon-white"></i> Unsuspend
            </button>
        </form>
         <!-- END OF THIS IS TO SUSPEND -->

        @endif

        </td>
        @empty

   
    </tr>
    @endforelse
    </tbody>
    </table>
    </div>
    </div>
    </div>
    <!--/span-->

    </div>



@endsection