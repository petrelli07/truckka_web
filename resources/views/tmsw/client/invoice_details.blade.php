@extends('tmsw.client.layout.content')
@section('content')
<link href="{{URL::to('home_assets/assets/css/invoice.css')}}" rel="stylesheet" />

<div class="row">
    @forelse($invoice as $invoices)
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-eye-open"></i> Invoice Details</h2>
            </div>
            <div class="box-content">
                <table class="neworder">

                    <thead class="theadorder">
                    <tr class="tr_order bgtable">
                        <th class="th_order" colspan="6">INVOICE INFORMATION</th>

                    </tr>
                    <tr class="tr_order">
                        <td class="td_order" colspan="6">
                            <strong>Invoice No: </strong>{{$invoices->invoice_reference}}<br><br>
                            <strong>Invoice Status: </strong> @if($invoices->description === "Invoice Unpaid")<b style="color: red">{{$invoices->description}}</b>@elseif($invoices->description === "Invoice Paid")<b style="color: green">{{$invoices->description}}</b>@endif<br><br>
                            <!--<strong>Date: </strong> xxxxxxxx<br>-->
                        </td>

                    </tr>
                    </thead>
                    <tbody>
                    <tr class="tr_order bgtable">
                        <th class="th_order">Description</th>
                        <th class="th_order">Origin</th>
                        <th class="th_order">Destination</th>
                        <th class="th_order">Number of Trucks</th>
                        <th class="th_order">Unit Price</th>
                        <th class="th_order">Total Amount</th>
                    </tr>
                    <tr class="tr_order">
                        <td class="td_order">Payment for One {{$invoices->truck_type_description}} Truck from {{$invoices->originStateName}} to {{$invoices->destStateName}} </td>
                        <td class="td_order">{{$invoices->originStateName}}</td>
                        <td class="td_order">{{$invoices->destStateName}}</td>
                        <td class="td_order">1</td>
                        <td class="td_order">&#x20A6;{{$invoices->price}}</td>
                        <td class="td_order">&#x20A6;{{$invoices->price}}</td>
                    </tr>
                    </tbody>
                    <tfoot>
                    <tr class="tr_order">
                        <th class="th_order" colspan="5">Total</th>
                        <td class="td_order"> &#x20A6;{{$invoices->price}}</td>
                    </tr>
                    <!--<tr class="tr_order">
                        <th class="th_order" colspan="4">Service Charge</th>
                        <td class="td_order"> 10% </td>
                        <td class="td_order">&#x20A6;xxxxxxxx</td>
                    </tr>
                    <tr class="tr_order">
                        <th class="th_order" colspan="5">Net Total Amount</th>
                        <td class="td_order">&#x20A6;xxxxxxxx</td>
                    </tr>-->

                    </tfoot>
                </table>

                <table class="neworder">

                    <tr class="tr_order bgtable">
                        <th class="th_order" colspan="6">MAKE PAYMENT TO:</th>

                    </tr>
                    <tr class="tr_order">
                        <td class="td_order" colspan="2">
                            <strong>Bank Name</strong><br>
                            <p>{{$invoices->bank_name}}</p>
                        </td>

                        <td class="td_order" colspan="2">
                            <strong>ACCOUNT NAME</strong><br>
                            <p>{{$invoices->account_name}}</p>
                        </td>
                        <td  class="td_order" colspan="2">
                            <strong>ACCOUNT NUMBER:</strong><br>
                            <p>{{$invoices->account_number}}</p>
                        </td>
                    </tr>



                </table>
                <div class="row">
                    <!--<div class="col-md-6">
                        <a class="btn btn-block btn-success" href='#'>Pay Now</a>

                    </div>-->
                    <div class="col-md-12">
                        <a class="btn btn-block btn-danger" href='{{url("/tmsw/finance/home")}}'>Return Back</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @empty
    @endforelse
    <!--/span-->

</div>


@endsection