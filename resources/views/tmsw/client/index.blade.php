@extends('tmsw.client.layout.content')
@section('content')



<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-eye-open"></i> Client Orders</h2>
            </div>
            <div class="box-content">
                <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
                    <thead>

                    <tr>

                        <th>Order ID</th>
                        <th>Origin</th>
                        <th>Destination</th>
                        <th>Number of Trucks</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($user as $users)
                    <tr>
                        <td class="center">{{$users->reference_number}}</td>
                        <td class="center">{{$users->originStateName}}</td>
                        <td class="center">{{$users->destStateName}}</td>
                        <td class="center">{{$users->truck_number}}</td>
                        <td class="center">
                            {{$users->description}}
                        </td>
                        <td>
                            <a href='{{url("/tmsw/client/view_truck_details/{$users->reference_number}")}}' class="btn-sm btn-success btn">View Truck Details</a>
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td class="center"></td>
                        <td class="center"></td>
                        <td class="center"></td>
                        <td class="center"></td>
                        <td class="center"></td>
                        <td class="center"></td>
                    </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!--/span-->

</div>



@endsection