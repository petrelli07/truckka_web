@extends('tmsw.client.layout.content')
@section('content')


<div class="row" xmlns="http://www.w3.org/1999/html">
    <div class="box col-md-12">

        <!--if @ session -->
        @if (session('message'))
        <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert">x</button>
            {{ session('message') }}
        </div>
        @endif

        <!--endif-->

        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-plus"></i> Reports &amp; Analytics</h2>

            </div>
            <div class="box-content row">


                <div class="box col-md-6">

                    <div class="box-content">
                        <table class="table table-striped table-bordered  responsive">
                            <thead>
                            <tr>
                                <th>Category</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td class="center"><a href="{{url('tmsw/performance/analytics/truck_orders')}}">Truck Orders</a></td>
                            </tr>
                            <tr>
                                <td class="center"><a href="{{url('tmsw/performance/analytics/supply')}}">Supply Requests</a></td>
                            </tr>
                            <tr>
                                <td class="center"><a href="{{url('tmsw/performance/analytics/finance')}}">Finance</a></td>
                            </tr>
                            <tr>
                                <td class="center"><a href="{{url('tmsw/performance/analytics/human_resources')}}">Human Resources</a></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                </div>






            </div>
        </div>
    </div>
    <!--/span-->

</div><!--/row-->



@endsection