@extends('tmsw.client.layout.content')
@section('content')



<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-eye-open"></i> All Invoices</h2>
            </div>
            <div class="box-content">
                <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
                    <thead>

                    <tr>

                        <th>Order Number</th>
                        <th>Origin</th>
                        <th>Destination</th>
                        <th>Plate Number</th>
                        <th>Amount</th>
                        <th>Action</th>

                    </tr>
                    </thead>
                    <tbody>
                    @forelse($invoice as $invoices)
                    <tr>
                        <td class="center">{{$invoices->reference_number}}</td>
                        <td class="center">{{$invoices->originStateName}}</td>
                        <td class="center">{{$invoices->destStateName}}</td>
                        <td class="center">{{$invoices->plateNumber}}</td>
                        <td class="center">{{$invoices->price}}</td>
                        @if($invoices->tmsw_status_id == 12)
                        <td class="center">
                            <a href='{{url("/tmsw/finance/confirm_truck_details/{$invoices->invoice_reference}")}}' class="btn btn-primary btn-md">View Details</a>| <a href='{{url("/tmsw/finance/view_invoice_details/{$invoices->invoice_reference}")}}' class="btn btn-danger btn-md">View Invoice Details</a>
                        </td>
                        @else
                        {{$invoices->description}}
                        @endif
                    </tr>
                    @empty
                    <tr>
                        <td class="center"></td>
                        <td class="center"></td>
                        <td class="center"></td>
                        <td class="center"></td>
                        <td class="center"></td>
                        <td class="center"></td>
                    </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!--/span-->

</div>



@endsection