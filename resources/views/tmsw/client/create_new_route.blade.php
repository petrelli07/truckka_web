@extends('tmsw.client.layout.content')
@section('content')

<div class="row">
    <div class="box col-md-12">
        <!--if @ session -->
        <!--<div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert">x</button>
               success
        </div>-->
        <!--endif-->

        <!--origin to server form-->
        <form id="originForm" style="display:none;">
            {{csrf_field()}}
            <input type="hidden" id="origin2" name="originToServer">
        </form>
        <!--end origin to server form-->

        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-plus"></i> Create New Orders</h2>

            </div>
            <div class="box-content row">
                <!--if has session message-->
                @if (session('message'))
                <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    {{ session('message') }}
                </div>
                @endif
                <!--endif-->
                <form role="form" method="post" class="validate"  action="{{url('/tmsw/manager/routes/create_new_route')}}"  enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="box col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="origin">Origin</label>
                            <div class="controls">
                                <select  class="form-control col-md-12" name="origin" required>
                                    <option value=''>[-Origin-]</option>
                                    @forelse($states as $state)
                                    <option value="{{$state->id}}">{{$state->state_description}}</option>
                                    @empty
                                    @endforelse
                                </select>
                            </div>
                            @if ($errors->has('origin'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('origin') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>


                    <div class="box col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="destination">Destination</label>
                            <div class="controls">
                                <select class="col-md-12 form-control" name="destination" required>
                                    <option value=''>[-Destination-]</option>
                                    @forelse($states as $state)
                                    <option value="{{$state->id}}">{{$state->state_description}}</option>
                                    @empty
                                    @endforelse
                                </select>
                            </div>
                            @if ($errors->has('destination'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('destination') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>


                    <div class="box col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="Type of cargo">Price</label>
                            <div class="controls">
                                <input type="text" class="col-md-12 form-control" id="price" name="price" required/>
                            </div>
                            @if ($errors->has('price'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('price') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>


                    <div class="box col-md-6">

                    </div>


                    <div class="box col-md-6">
                        <div class="form-group">
                            <button type="submit" class="btn btn-custom">
                                <i class="glyphicon glyphicon-plus"></i>Submit
                            </button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
    <!--/span-->

</div><!--/row-->



@endsection