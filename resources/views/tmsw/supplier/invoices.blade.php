@extends('admin.layout.content')
@section('content')



<div class="row">
    <div class="box col-md-12">
    <div class="box-inner">
    <div class="box-header well" data-original-title="">
        <h2><i class="glyphicon glyphicon-eye-open"></i> Invoices</h2>
    </div>
    <div class="box-content">
    <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
    <thead>

    <tr>
        
        <th>Invoice Number</th>
        <th>Order Number</th>
        <th>Amount</th>
        <th>Status</th>
        <th>Date Created</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        
        <td class="center">1</td>
        <td class="center">23j91j</td>
        <td class="center">23,000</td>
        <td class="center">success</td>
        <td class="center">21/11/2018</td>
        <td class="center">
           
            <a class="btn btn-info btn-sm" href="#">
                <i class="glyphicon glyphicon-eye-open icon-white"></i>
                View more
            </a>
            
            <a class="btn btn-danger btn-sm" href="#">
                <i class="glyphicon glyphicon-eye-open icon-white"></i>
                Order details
            </a>
        </td>
    </tr>
    </tbody>
    </table>
    </div>
    </div>
    </div>
    <!--/span-->

    </div>

    
@endsection