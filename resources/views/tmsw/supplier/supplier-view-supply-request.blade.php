@extends('tmsw.supplier.layout.content')
@section('content')



<div class="row">
    <div class="box col-md-12">
    <div class="box-inner">
    <div class="box-header well" data-original-title="">
        <h2><i class="glyphicon glyphicon-eye-open"></i>{{$tableTitle}}</h2>
    </div>
    <div class="box-content">
    <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
    <thead>

    <tr>
        
        <th>S/N</th>
        <th>Created On</th>
        <th>Expires On</th>        
        <th>Item</th>
        <th>Quantity</th>
        <th>Status</th>
        <th>Upload Quote</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
        @foreach($orders as $order)
    <tr>
        
        <td class="center">1</td>
        <td class="center">{{date('F jS , Y',strtotime($order->created_at))}}</td>        
        <td class="center">{{date('F jS , Y',strtotime($order->order_expire_date))}}</td>
        <td class="center">{{$order->orderDescription}}</td>
        <td class="center">{{$order->orderQuantity}}</td>
        <td class="center">

            @if($order->order_status == 2 )
               
               order cancelled
            @elseif(strtotime(date("Y-m-d h:i:sa")) > strtotime($order->expires_at))
                Order Expired
            @elseif($order->request_status == 3)
               Quote Rejected (upload new quote)
            @elseif($order->request_status == 5)
                Response Pending
            @elseif(($order->request_status == 1 || $order->request_status == 3) && $order->order_status == 1 )
                Upload a Quote
            @endif

        </td>
         <form action="{{url('tmsw/supplier/orders/quote/save/'.$order->supply_order_id)}}" method="post" enctype="multipart/form-data">

        <td class="center">
            @if((($order->request_status == 1 && $order->order_status == 1) || ($order->request_status == 3 && $order->order_status == 1)) && strtotime(date("Y-m-d h:i:sa")) < strtotime($order->expires_at))
                
                @csrf
               
               <div class="form-group">
                            <input type="file" name="file" class="form-control required">

                </div>

            @endif
                
        </td>
        <td class="center">
         
             @if((($order->request_status == 1 && $order->order_status == 1) || ($order->request_status == 3 && $order->order_status == 1)) && strtotime(date("Y-m-d h:i:sa")) < strtotime($order->expires_at))
               
              <button class="btn btn-info btn-sm">submit</button>

            @endif
           
        </td>
        </form>
    </tr>
    @endforeach
    </tbody>
    </table>
    </div>
    </div>
    </div>
    <!--/span-->

    </div>



@endsection