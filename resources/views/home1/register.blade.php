@extends('home.layout.content')
@section('content')

<div class="container-fluid">
    <div class="row forget-bg" >
        <div class="forget-overlay"></div>
        <div class="container">
            <div class="row">
                
                <div class="forget-content ">
                    <div class="col-xs-12 forget-content-al">
                        <div class="forget-title col-xs-12 no-padding">
                            <h2>
                                Register
                            </h2>
                        </div>
                        <div class="col-xs-12 no-padding">
                            <label class="forget-label">Email</label>
                            <input type="email" class="text-forget" id="txtForgotEmail" name="Email" placeholder="Email">
                            <span class="field-validation-valid c-red" data-valmsg-for="UserName" id="forgotEmailError" ></span>
                        </div>

                         <div class="col-xs-12 no-padding">
                            <label class="forget-label">Register As</label>
                            <select class="text-forget">
                            <option type="email"  class="text-forget"> ? </option>
                            <option type="email"  class="text-forget"> Truck owners / Supplier</option>
                            <option type="email"  class="text-forget"> Users</option>
                
                            </select>
                        </div>

                         <div class="col-xs-12 no-padding">
                            <label class="forget-label">Password</label>
                            <input type="email" class="text-forget" id="txtForgotEmail" name="password" placeholder="Password">
                            <span class="field-validation-valid c-red" data-valmsg-for="Password"  ></span>
                        </div>

                         <div class="col-xs-12 no-padding">
                            <label class="forget-label">Confirm Password</label>
                            <input type="email" class="text-forget" id="txtForgotEmail" name="password" placeholder="Password">
                            <span class="field-validation-valid c-red" data-valmsg-for="Password"  ></span>
                        </div>

                        <div class="col-xs-12">
                            <div class="row">
                                <div class="forget-btn">
                                    <button type="submit" id="btnForgotSend" class="">Submit</button>
                                    <button type="submit" id="backBtn" class="payment-cancel ">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>



@endsection