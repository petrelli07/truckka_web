@extends('home.layout.content')
@section('content')

<style>
.banner {
    background: none;
    position: relative;
    background-color:#F2E786;
    height: 500px;
}
</style>


<input type="hidden" id="returnUrl" />
<div class="container-fluid">
    <div class="row banner">
    
        <div class="col-sm-6 col-xs-12 no-padding">
                <div class="col-xs-12">
                    <div class="col-xs-12 no-padding">
                        <h3>Welcome to Truckka! Our freight Exchange </h3>
                        <!-- <div id="animate1">
                        <h1 class="mobileh1">How may we help you ?</h1>
                        </div> -->
                            <div class="margin-optipns">
                                <div class="ineedOptions">
                                <a href="#" class="ineedtruck hvr-underline-from-left i_need_truck">i need truck</a>
                                </div>

                                <div id="changed" class="ineedOptions">
                                <a href="#" class="ineedtruck hvr-underline-from-left i_can_give_truck">i have truck</a>
                                </div>

                                <div id="changed" class="ineedOptions">
                                <a href="#" class="ineedtruck hvr-underline-from-left">i need freight</a>
                                </div>

                                <div id="changed" class="ineedOptions">
                                <a href="#" class="ineedtruck hvr-underline-from-left">enterprise solutions</a>
                                </div>

                                <div id="changed" class="ineedOptions">
                                <a href="#" class="ineedtruck hvr-underline-from-left">contact sales/ support</a>
                                </div>
                        </div>
                        
                

                    </div>
                </div>
        </div>

        <div class="col-sm-6 col-xs-12 no-padding" >
                <div class="col-xs-12 no-padding" >
                    <div class="col-xs-12 no-padding">

                        <div id="loaded_image" class="img-left-ops mobile-pos">
                        <!-- <img  class="mobile-image-s" src="{{URL::to('home_assets/assets/images/how-the-exchange-work-new2.png')}}"  > 
                        <div class="container-fluid">

                            <div class="row banner">
                                
                            </div> -->
                        </div>


                <!---- THIS IS FOR I NEED TRUCK ------>
                        <div id="i_need_truck" class="img-left-ops">
                        <div class="location-content white white-lbl">
                        <div class="col-xs-12 no-padding">

                        <div class="col-sm-6 col-xs-12 no-padding">
                                <div class="col-xs-12">
                                    <div class="col-xs-12 no-padding">
                                        <div class="col-xs-12 no-padding">
                                            <div class="booking-label-change mtp-lbl">
                                                <label>Origin</label>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 no-padding">
                                            <select class="text-booking-change">
                                            <option class="text-booking-change" name="origin">?</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12 no-padding">
                                <div class="col-xs-12">
                                    <div class="col-xs-12 no-padding">
                                        <div class="col-xs-12 no-padding">
                                            <div class="booking-labe-changel">
                                                <label>Destination</label>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 no-padding">
                                            <select class="text-booking-change">
                                            <option class="text-booking-change" name="origin">?</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!--listing 2 -->
                        <div class="col-xs-12 no-padding">
                        <div class="col-sm-6 col-xs-12 no-padding">
                                <div class="col-xs-12">
                                    <div class="col-xs-12 no-padding">
                                        <div class="col-xs-12 no-padding">
                                            <div class="booking-labe-changel">
                                                <label>Choose Truck</label>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 no-padding">
                                            <select class="text-booking-change">
                                            <option class="text-booking-change" name="origin">?</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-sm-6 col-xs-12 no-padding">
                                <div class="col-xs-12">
                                    <div class="col-xs-12 no-padding">
                                        <div class="col-xs-12 no-padding">
                                            <div class="booking-labe-changel">
                                                <label>Pick Up Date</label>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 no-padding">
                                            <input class="text-booking-change1"  data-val="true" data-val-date="The field PickUpDateReady must be a date." id="datepicker" name="Order.PickUpDateReady" placeholder="MM/DD/YYYY" value="11/28/2018" tabindex="5">
                                            <div id="datetime"></div>
                                            <div class="date-ic"><img src="{{URL::to('home_assets/assets/images/date-ic-trackka.png')}}" alt="TQDate" class="img-responsive"></div>
                                            <span class="field-validation-valid col-xs-12 no-padding c-red" data-valmsg-for="UserName" id="txtDeliveryDateError" data-valmsg-replace="true"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end it-->


                        <div class="col-xs-12 no-padding">
                        <div class="col-sm-12 col-xs-12 no-padding">
                                <div class="col-xs-12">
                                    <div class="col-xs-12 no-padding">
                                        <div class="col-xs-12 no-padding">
                                            <div class="booking-label-change mtp-lbl">
                                                <label>Description of items</label>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-lg-12 no-padding">
                                            <textarea class="text-booking-change-textarea"  name="origin" placeholder="Enter Description"  type="text"></textarea>
                                            <span class="field-validation-valid c-red" data-valmsg-for="origin"  data-valmsg-replace="true"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 no-padding">
                            
                            
                            <div class="col-xs-12">
                                <div class="quote-btn col-xs-12 no-padding margin-top-55">
                                    <a id="btnGetQuote" class="cursor-pointer" tabindex="8">Get Quote</a>
                                    <a id="backtooptions3" class="cursor-pointer" tabindex="8">Back to Options</a>
                                </div>

                                
                            </div>
                        </div>
                        </div>

                        </div>
                                    
                       

<!------------------------------------------------------------- END OF IT --------------------------------------------->


                       

                        <!---- -------------------------------------------THIS IS FOR I CAN GIVE TRUCKS ----------------------------------->

                       <div id="i_can_give_truck" class="img-left-ops">

                                <div class="location-content white white-lbl">
                                <div class="col-xs-12 no-padding">

                                <div class="col-sm-6 col-xs-12 no-padding">
                                        <div class="col-xs-12">
                                            <div class="col-xs-12 no-padding">
                                                <div class="col-xs-12 no-padding">
                                                    <div class="booking-label-change mtp-lbl">
                                                        <label>Type of Truck</label>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 no-padding">
                                                    <select class="text-booking-change">
                                                    <option class="text-booking-change" name="origin">?</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-xs-12 no-padding">
                                        <div class="col-xs-12">
                                            <div class="col-xs-12 no-padding">
                                                <div class="col-xs-12 no-padding">
                                                    <div class="booking-labe-changel">
                                                        <label>Location Covered</label>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 no-padding">

                                                <ul class="report-radio-buttons tax-radio-btn">
                                                    <li class="">
                                                        <input type="checkbox"  class="text-booking-change2"id="defaultInline1">
                                                        <span class="lbl-chng-span22">NW</span>
                                                    </li>
                                                    <li class="">
                                                        <input type="checkbox" class="text-booking-change2" id="defaultInline1">
                                                        <span class="lbl-chng-span22">NE</span>
                                                    </li>
                                                    <li class="">
                                                        <input type="checkbox" class="text-booking-change2" id="defaultInline1">
                                                        <span class="lbl-chng-span22">SE</span>
                                                    </li>
                                                    <li class="">
                                                        <input type="checkbox"  class="text-booking-change2"id="defaultInline1">
                                                        <span class="lbl-chng-span22">SW</span>
                                                    </li>
                                                    <li class="">
                                                        <input type="checkbox"  class="text-booking-change2" id="defaultInline1">
                                                        <span class="lbl-chng-span22">SS</span>
                                                    </li>

                                                    <li class="">
                                                        <input type="checkbox"  class="text-booking-change2" id="defaultInline1">
                                                        <span class="lbl-chng-span22">NC</span>
                                                    </li>
                                                    </ul>

                                                        


                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!--listing 2 -->
                                <div class="col-xs-12 no-padding">
                                <div class="col-sm-6 col-xs-12 no-padding">
                                        <div class="col-xs-12">
                                            <div class="col-xs-12 no-padding">
                                                <div class="col-xs-12 no-padding">
                                                    <div class="booking-labe-changel">
                                                        <label>Name</label>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 no-padding">
                                                    
                                                    <input type="text" class="text-booking-change" name="name" placeholder="Enter Name" />
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-sm-6 col-xs-12 no-padding">
                                        <div class="col-xs-12">
                                            <div class="col-xs-12 no-padding">
                                                <div class="col-xs-12 no-padding">
                                                    <div class="booking-labe-changel">
                                                        <label>Phone Number</label>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 no-padding">
                                                <input type="text" class="text-booking-change" name="name" placeholder="Enter Phone Number" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--end it-->

                                <div class="col-sm-6 col-xs-12 no-padding">
                                    <div class="col-xs-12">
                                        <div class="col-xs-12 no-padding">
                                            <div class="col-xs-12 no-padding">
                                                <div class="booking-labe-changel">
                                                    <label>Relationship with the truck</label>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 no-padding">
                                                <ul class="report-radio-buttons tax-radio-btn">
                                                    <li class="">
                                                        <input id="radio-1" class="radio-custom equipments" name="equipmentType" type="radio" value="Vans" checked  tabindex="3">
                                                        <label for="radio-1" class="radio-custom-label lbl-chng-txt-change"><span class="lbl-chng-span">Truck Owner</span></label>
                                                    </li>
                                                    <li class="m-l-30">
                                                        <input id="radio-2" class="radio-custom equipments" value="Flatbeds" name="equipmentType" type="radio" tabindex="4">
                                                        <label for="radio-2" class="radio-custom-label lbl-chng-txt-change"><span class="lbl-chng-span">Aggregator</span></label>
                                                    </li>
                                                </ul>
                                            
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xs-12 no-padding">
                                    
                                    
                                    <div class="col-xs-12">
                                        <div class="quote-btn col-xs-12 no-padding margin-top-55">
                                            <a id="btnGetQuote" class="cursor-pointer" tabindex="8">Get Quote</a>
                                            <a id="backtooptions2" class="cursor-pointer" tabindex="8">Back to Options</a>
                                        </div>

                                        
                                    </div>
                                </div>
                                </div>

                                </div>


<!-------------------------------------- END OF IT ---------------------------------------------------->



                         <div id="i_can_give_truck3" class="img-left-ops">
                                
                         </div>


                         <div id="i_can_give_truck3" class="img-left-ops">
                                
                         </div>



                </div>
            </div>
        </div>

    </div>
</div>



<!--Benefits of the exchange-->
<div class="container-fluid" id="benefits">
    <div class="row about-al">
        <div class="container">
            <div class="row">
                <div class="work-title">
                    <h2>
                    BENEFITS OF THE EXCHANGE
                    </h2>
                </div>
                <div class="col-xs-12 no-padding work-block-al">
                    
                    

                    <div class="col-sm-9 col-md-6 col-lg-4">
                        <div class="work-block-horizontal">
                        <div class="work-imm">
                            <div class="image-freight">

                                <img src="{{URL::to('home_assets/assets/images/map-with-truck-ful.png')}}"  >           
                               
                            </div>
                            <div class ="content-freight">
                                <p class="p-horizon">
                                Peace of knowing in real-time the location of your truck / freight</p>
                        </div>
                        </div>
                    </div>
                    </div>

                    <div class="col-sm-9 col-md-6 col-lg-4">
                        <div class="work-block-horizontal">
                        <div class="work-imm">
                            <div class="image-freight">
                                <img src="{{URL::to('home_assets/assets/images/pig-white.png')}}" alt="No Hiden fee"> 
                            </div>          
                            <div class ="content-freight">
                                        <p class="p-horizon">
                                        Save funds through transparent / price efficiency.
                                    </p>
                            </div>
                            </div>
                               
                        </div>
                    </div>

                    <div class="col-sm-9 col-md-6 col-lg-4">
                        <div class="work-block-horizontal">
                        <div class="work-imm">
                            <div class="image-freight">
                                <img src="{{URL::to('home_assets/assets/images/worrying-less-white.png')}}">     
                            </div>      
                            <div class ="content-freight">
                                        <p class="p-horizon">
                                        Worry less on finding freight / truck
                                    </p>
                            </div>
                            </div>
                               
                        </div>
                    </div>

                    <div class="col-sm-9 col-md-6 col-lg-6">
                        <div class="work-block-horizontal">
                        <div class="work-imm">
                            <div class="image-freight">
                                <img src="{{URL::to('home_assets/assets/images/lap-phone-white.png')}}"> 
                            </div>          
                            <div class ="content-freight">
                                        <p class="p-horizon">
                                        Easy to use web / mobile technology 
                                    </p>
                            </div>
                            </div>
                               
                        </div>
                    </div>

                     <div class="col-sm-9 col-md-6 col-lg-6">
                     <div class="work-block-horizontal">
                            <div class="work-imm">
                            <div class="image-freight">
                                <img src="{{URL::to('home_assets/assets/images/customer_support_hover.png')}}" alt="No Hiden fee">
                            </div>
                            <div class ="content-freight">
                                <p class="p-horizon">
                                Helpful / Courteous Customer Support
                                </p>
                            </div>
                            </div>
                               
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end of Benefits of exchange-->




<!--Our Services-->

<div class="container-fluid margin-for-deter-service" >
<div class="container">
            <div class="row">
    
                <div class="work-title">
                    <h2>
                        OUR SERVICES
                    </h2>
                </div>

				<div class="section group">
					<div class="col span_1_of_3 col-determinate-service">
					<div class="board-images">
                        <img src="{{URL::to('home_assets/assets/images/contract logistics.jpg')}}">
                    </div>
                    <div class="content-d-service">
                        <h3>CONTRACT LOGISTICS</h3>
                        <p>We provide Full Truck Load (FTL) on an outsourced contractual basis </p>
                    </div>
                    </div>

                    
					<div class="col span_1_of_3 col-determinate-service">
					<div class="board-images">
                        <img src="{{URL::to('home_assets/assets/images/roadrunner-truck-angle_0.jpg')}}">
                    </div>
                    <div class="content-d-service">
                        <h3>Freight/Transportation Exchange</h3>
                        <p>Our digital mobile & web enabled freight-truck matching platform </p>
                    </div>
                    </div>
                    

					<div class="col span_1_of_3 col-determinate-service">
					<div class="board-images">
                        <img src="{{URL::to('home_assets/assets/images/tms icon.jpg')}}">
                    </div>

                     <div class="content-d-service">
                        <h3>Transportation Management System (TMS)</h3>
                        <p>bespoke TMS delivers visibility, control and seamless communication across your enterprise </p>
                    </div>

					</div>
				</div>
              
                    
</div>
</div>
</div> 
<!--end services-->





<!--About Truckka-->
<div class="container-fluid" >
    <div class="row about-al p-relative">
        <div class="benifits-overlay"></div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ben-pad">
            <div class="benefits-title col-xs-12 no-padding">
                <h2>
                    About Truckka
                </h2>
            </div>
            <div class="benefits-info col-xs-12 no-padding">
                
                <div class="col-sm-12 col-xs-12 pad-left">
                    <div class="benefits-info1">
                        
                        <p>
                        TRUCKKA Logistics Limited is a fourth-party logistics (4PL) tech-solutions company. 
                        We leverage our resources and capabilities with those of our partners to provide logistics services and tech-enabled solutions within the logistics supply chain.  
                        </p>
                    </div>
                </div>


                <div class="col-sm-12 col-xs-12 pad-left">
                    <div class="benefits-info1 margin-top-40">
                        
                        <p>
                        We are a team of professionals with rich and diverse corporate executive background with a focus to 
                        create an efficient marketplace for the logistics industry in Nigeria. 
                        </p>
                    </div>
                </div>


                <div class="col-sm-12 col-xs-12 pad-left">
                    <div class="benefits-info1 margin-top-40">
                        
                        <p>
                        We are therefore positioned as your go-to logistics solutions provider in a
                         difficult and infrastructure-challenged environment where players yearn for safety,
                          price-efficiency, professionalism and trust - our core-value proposition. 
                        </p>
                    </div>
                </div>


            </div>
            
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding">
            <div class="benifits-bg-im"></div>
        </div>
    </div>
</div>
<!--End of it-->


<!--Mission and vision-->
<div class="container-fluid" id="about">
    <div class="row about-al">
        <div class="container">
            <div class="row">
                <div class="about-title">
                    <h2>
                        VISION / MISSION
                    </h2>
                    <p class="about-p">
                    To build an efficient digital marketplace that matches owners of freight to truck services on demand. 
                     We equally provide enterprise logistics services within chosen segments.
                    </p>
                </div>
                <div class="about-btn">
                   
                </div>
            </div>
        </div>
    </div>
</div>
<!--end of mission and vision-->




<!--Mission and vision-->
<div class="container-fluid" id="about">
    <div class="row about-al">
        <div class="container">
            <div class="row">
                <div class="about-title">
                    <h2>
                	OUR CUSTOMER SEGMENTS
                    </h2>
                    
                    <img src="{{URL::to('home_assets/assets/images/truckka-segment.png')}}" style="width:100%;">
                    
                </div>
                
            </div>
        </div>
    </div>
</div>
<!--end of mission and vision-->



<!--How it works-->
<div class="container-fluid" id="howitworks">
    <div class="row about-al p-relative">
        <div class="benifits-overlay2"></div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ben-pad">
            <div class="benefits-title2 col-xs-12 no-padding">
                <h2>
                	OUR SERVICE PROMISE / QUALITY
                </h2>
            </div>
            <div class="benefits-info col-xs-12 no-padding">
                
                <div class="col-sm-10 col-xs-12 pad-left">
                    <div class="benefits-info1">
                        <h3> <img src="{{URL::to('home_assets/assets/images/good-mark.png')}}" style="width: 30px;"> 
                    	We will strive to match your freight / truck or refund your money within 48hours if no match</h3>
                       
                    </div>
                </div>
            </div>
            <div class="benefits-info col-xs-12 no-padding">
               
                <div class="col-sm-10 col-xs-12 pad-left">
                    <div class="benefits-info1">
                        <h3><img src="{{URL::to('home_assets/assets/images/good-mark.png')}}" style="width: 30px;"> 	We will act professionally and courteously always</h3>
                        
                    </div>
                </div>
            </div>
            <div class="benefits-info col-xs-12 no-padding">
                
                <div class="col-sm-10 col-xs-12 pad-left">
                    <div class="benefits-info1">
                        <h3><img src="{{URL::to('home_assets/assets/images/good-mark.png')}}" style="width: 30px;">  We will seek to give you peace of mind and invest more on your core-business </h3>
                        
                    </div>
                </div>
            </div>
            
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding">
            
        </div>
    </div>
</div>
<!--How it works-->


<!--Benefits-->
<div class="container-fluid margin-for-board-mem">
<div class="container">
            <div class="row">
                <div class="work-title">
                    <h2>
                        BOARD MEMBERS
                    </h2>
                </div>
               
               <div class="section group">
					<div class="col span_1_of_4 col-determinate">
                        <div class="board-images2">
                        <img src="{{URL::to('home_assets/assets/images/contract logistics.jpg')}}">
                        </div>
                            <div class="board-content">
                                <div class="section-board-c">
                                    <h3>GMD/CEO</h3>
                                    <h4>Abiodun Adesanya</h4>
                                </div>
                                <p><b>Southern Streams Logistics</b></p>
                                <p>- Technology/Bus. Devt.</p>
                                <p>- Strategy/Sales/Marketing</p>

                                <p><b>AVP - FCMB Plc</b></p>
                                <p>- Sales/Product Development</p>
                                <p>  - I.T Arch. & Infra Management </p>
                                <p>  - Alternate & e-Channel Dev</p>
                                <p>Manager - Sterling Bank Plc
                                    - Infrastructure Management </p>

                                    <div class="social-media-board">
                                    <a href="https://www.linkedin.com/in/abiodun-adesanya-2922832/">
                                    <img src="{{URL::to('home_assets/assets/images/tw.png')}}">
                                    </a>

                            </div>
                              
                        </div>
                    </div>
                    
					<div class="col span_1_of_4 col-determinate">
                        <div class="board-images2">
                        <img src="{{URL::to('home_assets/assets/images/contract logistics.jpg')}}">
                        </div>
                            <div class="board-content">
                                <div class="section-board-c">
                                    <h3>DIRECTOR</h3>
                                    <h4>Gbolahan, Mark-George</h4>
                                </div>
                                <p>- Partner,  Mark-George Consultants</p>
                                <p>- Senior Consultant,  Enclude Holding</p>
                                <p>- Finance Sector Specialist : Nigeria Infrastructure Advisory Facility</p>

                                <p>- CEO, MicroCred, MFB Nig.</p>
                                <p>- Country Dir., GroFin Nig. Ltd</p>
                               

                                    <div class="social-media-board">
                                    <a href="https://www.linkedin.com/in/olugbolahan-mark-george-83396a29/?originalSubdomain=ng">
                                    <img src="{{URL::to('home_assets/assets/images/tw.png')}}">
                                    </a>

                            </div>
                              
                        </div>
                    </div>
                    
                    
					<div class="col span_1_of_4 col-determinate">
                        <div class="board-images2">
                        <img src="{{URL::to('home_assets/assets/images/contract logistics.jpg')}}">
                        </div>
                            <div class="board-content">
                                <div class="section-board-c">
                                    <h3>DIRECTOR</h3>
                                    <h4>Enitan Adesanya</h4>
                                </div>
                                <p><b>Easterseals, Bay Area, Oakland, California, USA</b></p>
                                <p>- Chief Administration & Finance Officer.</p>
                               

                                <p><b>KAISER PERMANENTE (KP), Oakland, CA, USA</b></p>
                                <p>- V.P, Enterprise Shared Services (ESS) (2016-Present)</p>
                                <p>  - V.P, National Facilities Services (2010-2016)  </p>
                                
                                    <div class="social-media-board">
                                    <a href="https://www.linkedin.com/in/enitanadesanya/">
                                    <img src="{{URL::to('home_assets/assets/images/tw.png')}}">
                                    </a>

                            </div>
                              
                        </div>
                    </div>


					<div class="col span_1_of_4 col-determinate">
                        <div class="board-images2">
                        <img src="{{URL::to('home_assets/assets/images/contract logistics.jpg')}}">
                        </div>
                            <div class="board-content">
                                <div class="section-board-c">
                                    <h3>DIRECTOR</h3>
                                    <h4>Anino Akperi</h4>
                                </div>
                                <p><b>Seal Towers Ltd , Director </b></p>
                                <p><b>FCMB Plc </b></p>
                                <p>- V.P,  Risk Management & Commercial Banking
                                Chief Risk Officer,  FINBANK
                                Zonal Head, Comm.  Banking
                                </p>

                                <p><b>Manager –  UBA  Ltd Head, Consumer Credits</b></p>
                                

                                    <div class="social-media-board">
                                    <a href="https://www.linkedin.com/in/anino-akperi-4b723215/">
                                    <img src="{{URL::to('home_assets/assets/images/tw.png')}}">
                                    </a>

                            </div>
                              
                        </div>
                    </div>
				</div>
            </div>
            </div>            
      
</div>
<!--Benefits-->





@endsection