<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>TMSC</title>
    <!-- Bootstrap --><link rel="stylesheet" type="text/css" href="{{ url('css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ url('css/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('css/indexupdate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('css/tmsa.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('css/loader.css') }}">
    <link href="{{ url('https://fonts.googleapis.com/css?family=Titillium+Web|Concert+One|Josefin+Sans|Roboto') }}" rel="stylesheet">
    <script type="text/javascript" src="{{ url('js/admin_dashboard.js')}}"></script>
    <script type="text/javascript" src="{{ url('js/jquery-3.2.1.js')}}"></script>
    <script type="text/javascript" src="{{ url('js/bootstrap.js')}}"></script>
    <script type="text/javascript" src="{{ url('js/newOrder.js')}}"></script>
    <link rel="stylesheet" href="{{url('//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css')}}">

    <script>
        $(document).ready(function() {

            $(".newOrder").hide();
            $(".newNonStandardOrder").hide();

            $(".ShowMeStandard").hide();

            $('#requestType').on('change', function() {
                if ( this.value == '1'){

                    $(".ShowMeStandard").show();
                    $(".dateReqStandard").hide();
                    $(".newOrder").hide();
                    $(".newNonStandardOrder").show();
                    $(".newOrderConfirm").hide();
                }else if ( this.value == '0'){

                    $(".ShowMeStandard").hide();
                    $(".dateReqStandard").show();
                    $(".newOrderConfirm").show();
                    $(".newOrder").show();
                    $(".newNonStandardOrder").hide();

                }

            });
        });
    </script>

    <script>
        $(document).ready(function() {

            $(".general").hide();

            $('#haulageType').on('change', function() {
                if ( this.value == '0'){

                    $(".general").show();

                }else if ( this.value >= '0'){

                    alert("This Request cannot be processed at the moment. Please contact Systems Administrator");
                    $(".general").hide();
                }

            });
        });
    </script>



    <script>
        $(document).ready(function() {
            $('.newOrderConfirm').click(function(e){

                /*var haulageType = document.getElementById('haulageType').value;
                if(haulageType == "0"){
                    document.getElementById('haulageTypeConfirm').innerText = "Land Freight";
                     document.getElementById('confirmHaulageType').value = haulageType;
                }else if(haulageType == "1"){
                    document.getElementById('haulageTypeConfirm').innerText = "N/A";
                }else if(haulageType == "2"){
                    document.getElementById('haulageTypeConfirm').innerText = "N/A";
                }*/

                /*var reqType = document.getElementById('requestType').value;
                if(haulageType == "0"){
                    document.getElementById('requestTypeConfirm').innerText = "Standard Order";

                    document.getElementById('confirmRequestType').value = reqType;
                }else if(haulageType == "1"){
                    document.getElementById('requestTypeConfirm').innerText = "Non Standard Order";
                    document.getElementById('confirmRequestType').innerText = reqType;
                }*/

                document.getElementById('pickupTimeConfirm').innerText = document.getElementById('pickupTime').value;
                document.getElementById('confirmPickUpTime').value = document.getElementById('pickupTime').value;

                document.getElementById('pickupDateConfirm').innerText = document.getElementById('pickupDate').value;
                document.getElementById('confirmPickUpDate').value = document.getElementById('pickupDate').value;

                document.getElementById('confirmDestination').value = document.getElementById('standardDestination').value;

                //document.getElementById('originConfirm').innerText = document.getElementById('standardOrigin').value;
                document.getElementById('confirmOrigin').value = document.getElementById('standardOrigin').value;

                document.getElementById('haulageValConfirm').innerText = document.getElementById('haulageVal').value;
                document.getElementById('confirmHaulageValue').value = document.getElementById('haulageVal').value;

                document.getElementById('weightConfirm').innerText = document.getElementById('weight').value;
                document.getElementById('confirmWeight').value = document.getElementById('weight').value;

                document.getElementById('itemDescriptionConfirm').innerText = document.getElementById('itemDescription').value;
                document.getElementById('confirmItemDescription').value = document.getElementById('itemDescription').value;

                document.getElementById('contactNameConfirm').innerText = document.getElementById('contactName').value;
                document.getElementById('confirmContactName').value = document.getElementById('contactName').value;

                document.getElementById('contactPhoneConfirm').innerText = document.getElementById('contactPhone').value;
                document.getElementById('confirmContactPhone').value = document.getElementById('contactPhone').value;

                document.getElementById('confirmpaymentType').value = document.getElementById('paymentType').value;

            });
        });
    </script>

    <script>
        $(document).ready(function() {

            $("#standardDestination").hide();
            $("#originDestinationAddon").hide();
            $("#destinationLabel").hide();

            $('#standardOrigin').on('change', function() {
                var val = this.value;
                var _token = $("input[name='_token']").val();

                document.getElementById('origin2').value = val;
                var original = document.getElementById('origin2').value;

                $.ajax({

                    url: "http://truckka.com.ng:8083/checkOrigins",
                    //url: "http://localhost/tmsc/public/checkOrigins",

                    type:'POST',

                    data: {_token:_token, originToServer:original},

                    success: function(data) {

                        if($.isEmptyObject(data.error)){

                            var length = data.success;
                            var trueLength = length.length;

                            if(trueLength > 0){
                                var catOptions = "";
                                for(var i = 0; i < trueLength; i++) {

                                    var company = data.success[i].destination;
                                    var region = data.success[i].region;
                                    catOptions += "<option width='50%' value="+ data.success[i].id +">" + company + "</option>";
                                }
                                document.getElementById('region').value = region;
                                document.getElementById("standardDestination").innerHTML = catOptions;
                                $("#standardDestination").show();
                                $("#originDestinationAddon").show();
                                $("#destinationLabel").show();
                            }else{
                                alert("nothing");
                            }



                        }else if(trueLength == 0){

                            alert('error');

                        }

                    }

                });
            })
        });
    </script>
    <!-- <script src="{{url('//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js')}}"></script> -->

    <script>
    //     $(document).ready(function(){
    //         $('input.timepicker').timepicker({
    //             timeFormat: 'h:mm p',
    //             minTime: '08:00:00',
    //             maxTime: '18:00:00'
    //         });
    //     });
    </script>

    <script type="text/javascript">
        $(document).ready(function() {

            $(document).ajaxStart(function(){
                document.getElementById('modal').style.display = 'block';
                document.getElementById('fade').style.display = 'block';
            });

            $(document).ajaxComplete(function(){
                document.getElementById('modal').style.display = 'none';
                document.getElementById('fade').style.display = 'none';
            });
        });
        
    </script>


</head>
<body class="col-md-12-col-sm-6 col-xs-12" style="padding:0;">

<!--TMSA-->
<div class="tmsa" >
<div class="row" style="height:100%; margin-right:0; margin-left:0;">
<div class="side_bar col-md-3">
    <div class=" identifier hidden-xs hidden-sm" >
        <span class="navbar-brand type" >T</span>
        <span class="navbar-brand type">M</span>
        <span class="navbar-brand type">S</span>
        <span class="navbar-brand type">C</span>
    </div>
    <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed pull-left" data-toggle="collapse" data-target="#hidden_bar" aria-expanded="false" style="border: 2px solid #fff ; margin-left:10px;">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar" style="border:1px solid #fff"></span>
            <span class="icon-bar" style="border:1px solid #fff"></span>
            <span class="icon-bar" style="border:1px solid #fff"></span>
        </button>
<!--        <a class="navbar-brand" href="#">-->
<!--            <div class="brand">-->
<!--                <span class="navbar-brand  logoT" style="border: 1px solid #ffffff; margin-left: 2px; border-radius: 10px 0 0 0; font-family: 'Concert One', cursive; color: #ffffff" >T</span>-->
<!--                <span class="navbar-brand  logoR" style="border: 1px solid #ffffff;  margin-left: 2px; font-family: 'Concert One', cursive; background: #ffffff; color: #3baba0;">R</span>-->
<!--                <span class="navbar-brand   logoU" style="border: 1px solid #ffffff;  margin-left: 2px; font-family: 'Concert One', cursive; color: #ffffff;">U</span>-->
<!--                <span class="navbar-brand   logoC" style="border: 1px solid #ffffff;  margin-left: 2px;font-family: 'Concert One', cursive; background: #ffffff; color: #3baba0;">C</span>-->
<!--                <span class="navbar-brand  logoK" style="border: 1px solid #ffffff;  margin-left: 2px;font-family: 'Concert One', cursive; color: #ffffff"; >K</span>-->
<!--                <span class="navbar-brand   logoK" style="border: 1px solid #ffffff;  margin-left: 2px;font-family: 'Concert One', cursive; background: #ffffff; color: #3baba0;">K</span>-->
<!--                <span class="navbar-brand   logoA" style="border: 1px solid #ffffff;  margin-left: 2px; border-radius: 0 0 10px 0;font-family: 'Concert One', cursive; color: #ffffff;">A</span>-->
<!--            </div>-->
<!--        </a>-->
    </div>
    <!--NAVBAR FOR MOBIE SCREENS-->
    <ul class="nav navbar-nav collapse" id="hidden_bar">

        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">My Orders <span class="glyphicon glyphicon-file pull-right"></span></a>
            <ul class="dropdown-menu">
                <li>
                    <a href="{{url('/home')}}">Create New Order <span class="glyphicon glyphicon-plus-sign pull-right"></span></a>
                </li>
                <li class="divider"></li>
                <li><a href="{{ url('/viewOrders') }}">View My Orders <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                <li class="divider"></li>
            </ul>
        </li>

        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Financial Management <span class="fa fa-dollar pull-right"></span></a>
            <ul class="dropdown-menu">
                <li>
                    <a href="{{url('/allInvoices')}}">My Invoices <span class="glyphicon glyphicon-plus-sign pull-right"></span></a>
                </li>
                <li class="divider"></li>
                <li><a href="{{url('/demurrage')}}">Demurrage <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                <li class="divider"></li>
                <li><a href="{{url('/settlement')}}">Settlement <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                <li class="divider"></li>
            </ul>
        </li>

        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Documentation <span class="fa fa-file-open pull-right"></span></a>
            <ul class="dropdown-menu">
                <li>
                    <a href="{{url('/allOrders')}}">All Orders<span class="glyphicon glyphicon-plus-sign pull-right"></span></a>
                </li>
                <li class="divider"></li>

            </ul>
        </li>



        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Performance Management <span class="fa fa-line-chart pull-right"></span></a>
            <ul class="dropdown-menu">
                <li>
                    <a href="{{url('/performance')}}">Finance<span class="fa fa-dollar pull-right"></span></a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="{{url('/performance')}}">Haulage<span class="glyphicon glyphicon-shopping-cart pull-right"></span></a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="{{url('/performance')}}">Time<span class="glyphicon glyphicon-time pull-right"></span></a>
                </li>
                <li class="divider"></li>

            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Track and Trace <span class="glyphicon glyphicon-map-marker pull-right"></span></a>
            <ul class="dropdown-menu">
                <li><a href='{{ url("/allGPS") }}'>Track Orders <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                <li class="divider"></li>
            </ul>
        </li>



        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Documentation <span class="fa fa-file-open pull-right"></span></a>
            <ul class="dropdown-menu">
                <li>
                    <a href="{{url('/allOrders')}}">All Orders<span class="glyphicon glyphicon-plus-sign pull-right"></span></a>
                </li>
                <li class="divider"></li>

            </ul>
        </li>

        <li class="dropdown">
            <a href="{{ route('logout') }}"
               onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();" class="dropdown-toggle" data-toggle="dropdown">Log Out <span class="glyphicon glyphicon-log-out pull-right"></span></a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </li>



    </ul>
    <!--NAVBAR FOR laptop SCREENS-->
    <ul class="nav navbar-nav hidden-xs hidden-sm">
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">My Orders <span class="glyphicon glyphicon-file pull-right"></span></a>
            <ul class="dropdown-menu">
                <li>
                    <a href="{{url('/home')}}">Create New Order <span class="glyphicon glyphicon-plus-sign pull-right"></span></a>
                </li>
                <li class="divider"></li>
                <li><a href="{{ url('/viewOrders') }}">View My Orders <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                <li class="divider"></li>
            </ul>
        </li>

        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Financial Management <span class="fa fa-dollar pull-right"></span></a>
            <ul class="dropdown-menu">
                <li>
                    <a href="{{url('/allInvoices')}}">My Invoices <span class="glyphicon glyphicon-plus-sign pull-right"></span></a>
                </li>
                <li class="divider"></li>
                <li><a href="{{url('/demurrage')}}">Demurrage <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                <li class="divider"></li>
                <li><a href="{{url('/settlement')}}">Settlement <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                <li class="divider"></li>
            </ul>
        </li>

        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Documentation <span class="fa fa-file-open pull-right"></span></a>
            <ul class="dropdown-menu">
                <li>
                    <a href="{{url('/allOrders')}}">All Orders<span class="glyphicon glyphicon-plus-sign pull-right"></span></a>
                </li>
                <li class="divider"></li>

            </ul>
        </li>



        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Performance Management <span class="fa fa-line-chart pull-right"></span></a>
            <ul class="dropdown-menu">
                <li>
                    <a href="{{url('/performance')}}">Finance<span class="fa fa-dollar pull-right"></span></a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="{{url('/performance')}}">Haulage<span class="glyphicon glyphicon-shopping-cart pull-right"></span></a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="{{url('/performance')}}">Time<span class="glyphicon glyphicon-time pull-right"></span></a>
                </li>
                <li class="divider"></li>

            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Track and Trace <span class="glyphicon glyphicon-map-marker pull-right"></span></a>
            <ul class="dropdown-menu">
                <li><a href='{{ url("/allGPS") }}'>Track Orders <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                <li class="divider"></li>
            </ul>
        </li>



        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Documentation <span class="fa fa-file-open pull-right"></span></a>
            <ul class="dropdown-menu">
                <li>
                    <a href="{{url('/allOrders')}}">All Orders<span class="glyphicon glyphicon-plus-sign pull-right"></span></a>
                </li>
                <li class="divider"></li>

            </ul>
        </li>

        <li class="dropdown">
            <a href="{{ route('logout') }}"
               onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();" class="dropdown-toggle" data-toggle="dropdown">Log Out <span class="glyphicon glyphicon-log-out pull-right"></span></a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </li>


    </ul>
</div>
<!-- TSMA MAIN PAGE -->
<div class="tmsa_main_body col-md-9" >
<div class="tmsa_user_page" >
<!session message starts-->

@if(session('message'))
<p>
    {{ session('message') }}
</p>
@endif

<!session message ends-->
<div class="alert alert-danger print-error-msg" style="display:none">

    <ul></ul>
</div>

<div class="alert alert-success print-success-msg" style="display:none">

    <ul></ul>
</div>
<!-- FORM -->
<form id="originalForm">
    {{csrf_field()}}
    <fieldset style="background: #ffffff">

        <!-- Form Name -->
        <div class="form-group" >
            <!session message starts-->

            @if(session('message'))
            <p>
                {{ session('message') }}
            </p>
            @endif

            <!session message ends-->

            <div class="alert alert-danger print-error-msg" style="display:none">

                <ul></ul>
            </div>

            <div class="alert alert-success print-success-msg" style="display:none">

                <ul></ul>
            </div>

            <div class="input-group" style="margin-left:5%; margin-right: 5%; margin-bottom: 3%; border-radius:10px;">
                <label for="addUser" class="input-group-addon create_order" > CREATE AN ORDER <span class="glyphicon glyphicon-plus-sign"></span></label>
            </div>
        </div>
        <input class="haulageType" type="hidden" id="haulageType" value="0">
        <div class="las">
        <!-- Name input-->
        <!-- <div class="form-group" style="margin-left:5%; margin-right: 5%; margin-bottom: 3%;">
            <label><h4 class="h4">Haulage Type</h4></label>
            <div class="col-md-12">
             -->
                <!-- <select class="form-control" name="resourceType" class="haulageType" id="haulageType">
                    <option value="">[-Haulage Type-]</option>
                    <option value="0">Land Freight</option>
                    <option value="1">Sea Freight</option>
                    <option value="2">Air Freight</option>
                </select> -->
           <!--  </div>
        </div> -->

        <div class="input-group" style="margin-left:5%; margin-right: 5%; margin-bottom: 3%; margin-top: 8%;">
            <!--<label for="usr">Haulage Value</label>-->
            <div style="color:black;" class="h4">Pick Up Date</div>
                <input width="50%" type="date" class="input-md form-control" name="pickupDate" id="pickupDate">
<!--                <input type="text" class="input-md timepicker form-control" id="pickupTime" name="pickupTime">-->
            <span class="input-group-addon"></span>
            <div style="color:black;" class="h4">Pick Up Time Window</div>
            <select name="pickupTime" class="form-control" id="pickupTime" required>
                        <option value="">[-Pick Up Time-]</option>
                        <option value="0000Hrs - 0600Hrs">0000Hrs - 0600Hrs</option>
                        <option value="0600Hrs - 1200Hrs">0600Hrs - 1200Hrs</option>
                        <option value="1200Hrs - 1800Hrs">1200Hrs - 1800Hrs</option>
                        <option value="1800Hrs - 0600Hrs">1800Hrs - 0600Hrs</option>
            </select>
            <!-- <input type="text" class="input-md timepicker form-control" id="pickupTime" name="pickupTime"> -->
<!--                <input width="50%" type="date" class="input-md form-control" name="pickupDate" id="pickupDate">-->
        </div>

        <!-- <div class="form-group" style="margin-left:5%; margin-right: 5%; margin-bottom: 3%;">
            <label><h4 class="h4">Request Type</h4></label>
            <div class="col-md-12"> -->

            <input class="haulageType" type="hidden" id="requestType" value="0">
                    <!-- <select name="haulageType" class="form-control" name="haulageType" id="requestType">
                        <option value="">[-REQUEST TYPE-]</option>
                        <option value="0">Standard</option>
                        <option value="1">Non Standard</option>
                    </select> -->
            <!-- </div>
        </div> -->

        <!-- <div class="form-group" style="margin-left:5%; margin-right: 5%; margin-bottom: 3%;">
            <label><h4 class="h4">Payment Type</h4></label>
            <div class="col-md-12">
                    <select name="paymentType" class="form-control" id="paymentType">
                        <option value="">[-PAYMENT TYPE-]</option>
                        <option value="0">50% POST LOADING</option>
                        <option value="1">NO UPFRONT</option>
                    </select>
            </div>
        </div> -->

        <div class="input-group" style="margin-left:5%; margin-right: 5%; margin-bottom: 3%; margin-top: 8%;">
            <!--<label for="usr">Haulage Value</label>-->
            <div style="color:black;" class="h4">Origin</div>
            <select name="standardOrigin" class="form-first-name form-control" id="standardOrigin">
                <option value="">[-SELECT ORIGIN-]</option>
                
                @for ($i=0; $i < count($uniqueOrigins); $i++) 
                    <option value="{{$uniqueOrigins[$i]}}">{{$uniqueOrigins[$i]}}</option>
                 @endfor
                
            </select>
            <span id="originDestinationAddon" class="input-group-addon"></span>
            <div style="color:black;" id="destinationLabel" class="h4">Destination</div>
            <select name="standardDestination" class="form-control" id="standardDestination">

            </select>
        </div>
    </div>
    <div class="nexting hidden">
        <div class="input-group" style="margin-left:5%; margin-right: 5%; margin-bottom: 3%; margin-top: 8%;">
            <!--<label for="usr">Haulage Value</label>-->
            <div style="color:black;" class="h4">Approximate Weight (Tonnes)</div>
            <input width="50%" type="text" class="input-md form-control" name="weight" id="weight">
            <span class="input-group-addon"></span>
            <div style="color:black;" class="h4">Haulage Value (&#8358;)</div>
            <input width="50%" type="text" class="input-md form-control" name="haulageVal" id="haulageVal">
        </div>

        <div class="form-group" style="margin-left:5%; margin-right: 5%; margin-bottom: 15%; margin-top: 8%;">
            <label><h4 class="h4">DESCRIBE THE ITEM(S) TO BE MOVED</h4></label>
            <div class="col-md-12">
                <textarea class="form-control" name="itemDescription" id="itemDescription"></textarea>
            </div>
        </div>

        <div class="input-group" style="margin-left:5%; margin-right: 5%; margin-bottom: 3%; margin-top: 8%;">
            <div style="color:black;" class="h4">Contact Name</div>
            <input type="text" class="input-md form-control" name="contactName" id="contactName">
            <span class="input-group-addon"></span>
            <div style="color:black;" class="h4">Contact Phone</div>
            <input type="text" class="input-md form-control" name="contactPhone" id="contactPhone">
        </div>

        <div class="ShowMeStandard">

            <div class="input-group" style="margin-left:5%; margin-right: 5%; margin-bottom: 3%; margin-top: 8%;">
                <div style="color:black;">Destination </div>
                <input type="text" class="form-control" name="deliverTo" id="deliverTo">

                <span class="input-group-addon"></span>
                <div style="color:black;"> Origin</div>
                <input type="text" class="form-first-name form-control" name="deliverFrom" id="deliverFrom">
            </div>

            <div class="form-group">
                <label for="usr"><h4>Offering Amount</h4></label>
                <input type="text" class="form-first-name form-control" name="amount" id="amount">
            </div>
            <br/>

        </div>
    </div>
        <div align="center" class="pull-center">
            <button type="button" id="btn_registrar" name="btn_registrar" class="btn button_login nexta pull-center">Next</button>
        </div>
        <div align="center" class="pull-center hidden sub">
            <button type="button" id="btn_registrar" name="btn_registrar" class="btn button_login nextas pull-center">Previous</button>
            <button type="button" data-toggle="modal" data-target="#myModal" id="btn_registrar" name="btn_registrar" class="btn button_login newOrderConfirm pull-center">Create Order</button>
            <button type="submit" class="btn btn-primary newNonStandardOrder">Create New Non Standard Order</button>
        </div>
</div>

    </fieldset>
</form>

<form id="originForm" style="display:none;">
    <input type="hidden" id="origin2" name="originToServer">
</form>


    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">

                   <!--  <div class="alert alert-danger print-error-msg" style="display:none">

                        <ul></ul>
                    </div>

                    <div class="alert alert-success print-success-msg" style="display:none">

                        <ul></ul>
                    </div> -->

                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                    <h4 class="modal-title custom_align" id="Heading">ORDER SUMMARY</h4>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-6">
                            <h4>Haulage Type</h4>
                            <div id="haulageTypeConfirm"></div>
                        </div>
                        <div class="col-md-6">
                            <h4>Request Type</h4>
                            <div id="requestTypeConfirm"></div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <h4>Pick Up Time Window</h4>
                            <div id="pickupTimeConfirm"></div>
                        </div>
                        <div class="col-md-6">
                            <h4>Pick Up Date</h4>
                            <div id="pickupDateConfirm"></div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <h4>Haulage Value(&#8358;)</h4>
                            <div id="haulageValConfirm"></div>
                        </div>
                        <div class="col-md-6">
                            <h4>Weight</h4>
                            <div id="weightConfirm"></div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <h4>Contact Name</h4>
                            <div id="contactNameConfirm"></div>
                        </div>
                        <div class="col-md-6">
                            <h4>Contact Phone</h4>
                            <div id="contactPhoneConfirm"></div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <h4>Item Description</h4>
                            <div id="itemDescriptionConfirm"></div>
                        </div>
                    </div>


                    <form  id="newOrder">
                        {{ csrf_field() }}

                        <div class="input-group">
                            <table class="table table-bordered">
                            
                                <tbody>
                                <tr>
                                <div class="input-group">
                                    <div style="color:black;">Resource Type</div>
                                    <select name="resourceType[]" required>
                                        <option value="">[-Choose a Resource Type-]</option>
                                        <option value="30ton">30Ton</option>
                                        <!-- @foreach($ClientRequiredResource as $st)
                                        <option value="{{$st->resourceType}}">{{$st->resourceType}}</option>
                                        @endforeach -->
                                    </select><span class="input-group-addon"></span><div style="color:black;">Choose a Resource Number</div><select name="resourceNumber[]" required>
                                        <option value="">[-Choose a Resource Number-]</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                    </select>
                                </tr>
                                <!-- <tr>
                                    <div class="stepresults_wrap" style="margin-bottom: 30px;">
                                        <button class="stepresults_add_field_button btn-md btn-success">Add More</button>
                                    </div>
                                </tr> -->
                                </div>
                                </tbody>

                            </table>
                        </div>
                        <br/>

                        <!--<input type="hidden" name="haulageType0" id="confirmHaulageTypex">-->
                        <input type="hidden" name="haulageType" id="confirmRequestType">
                        <input type="hidden" name="standardOrigin" id="confirmOrigin">
                        <input type="hidden" name="standardDestination" id="confirmDestination">
                        <input type="hidden" name="pickupDate" id="confirmPickUpDate">
                        <input type="hidden" name="pickupTime" id="confirmPickUpTime">
                        <input type="hidden" name="haulageVal" id="confirmHaulageValue">
                        <input type="hidden" name="itemDescription" id="confirmItemDescription">
                        <input type="hidden" name="weight" id="confirmWeight">
                        <input type="hidden" name="contactName" id="confirmContactName">
                        <input type="hidden" name="contactPhone" id="confirmContactPhone">
                        <input type="hidden" name="confirmpaymentType" id="confirmpaymentType">
                        <input type="hidden" id="region" name="region">
                        <button data-dismiss="modal" class="btn button_login btn-submit newStanOrder data-dismiss='modal' " id="btn_registrar" name="btn_registrar">Submit</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>

                    </form>

                </div>
                <div class="modal-footer ">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Close</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

</div>

    <div id="fade"></div>
        <div id="modal">
            <img id="loader" src="{{url('images/69.gif')}}" />
        </div>
</div>


<script type="text/javascript">
        $(document).ready(function() {
            var max_fields      = 5; //maximum input boxes allowed
            var wrapper         = $(".stepresults_wrap"); //Fields wrapper
            var add_button      = $(".stepresults_add_field_button"); //Add button ID

            var x = 1; //initlal text box count
            $(add_button).click(function(e){ //on add input button click
                e.preventDefault();
                if(x < max_fields){ //max input box allowed
                    x++; //text box increment
                    $(wrapper).append('<div class="input-group"><div style="color:black;">Resource Type</div><select name="resourceType[]" required><option value="">[-Choose a Resource Type-]</option>@foreach($ClientRequiredResource as $st)<option value="{{$st->resourceType}}">{{$st->resourceType}}</option>@endforeach</select><span class="input-group-addon"></span><div style="color:black;">Number</div><input type="text" name="resourceNumber[]"><a href="#" class="remove_field_stepresults">Remove</a></div>'); //add input box
                }
            });

            $(wrapper).on("click",".remove_field_stepresults", function(e){ //user click on remove text
                e.preventDefault(); $(this).parent('div').remove(); x--;
            })
        });
    </script>
    <!--BOTTOM NAV BAR-->

<!--     ************************************************** -->
</body>
</html>

