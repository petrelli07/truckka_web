<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>TMSHC</title>
    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css" href="{{ url('css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ url('css/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('css/indexupdate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('css/tmsa.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('css/loader.css') }}">
    <link href="{{ url('https://fonts.googleapis.com/css?family=Titillium+Web|Concert+One|Josefin+Sans|Roboto') }}" rel="stylesheet">
    <script type="text/javascript" src="{{ url('js/admin_dashboard.js')}}"></script>
    <script type="text/javascript" src="{{ url('js/jquery-3.2.1.js')}}"></script>
    <script type="text/javascript" src="{{ url('js/bootstrap.js')}}"></script>
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web|Concert+One|Josefin+Sans|Roboto" rel="stylesheet">
    <link rel="stylesheet" href="{{url('//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css')}}">
    <script src="{{url('//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js')}}"></script>

    <script>
        $(document).ready(function(){
            $('input.timepicker').timepicker({
                timeFormat: 'h:mm p'
            });
        });
    </script>

    <script>
        $(document).ready(function(){
            $('.destinationTable').hide();
            $('.hideOnNext').hide();
            $(".next").click(function(){
                $('.originTable').hide();
                $('.destinationTable').show();
                $('.next').hide();
                $('.hideOnNext').show();
            });
            $(".previous").click(function(){
                $('.originTable').show();
                $('.destinationTable').hide();
                $('.next').show();
                $('.hideOnNext').hide();
            });
        });
    </script>


    <script type="text/javascript">
        $(document).ready(function() {

            $(document).ajaxStart(function(){
                document.getElementById('modal').style.display = 'block';
                document.getElementById('fade').style.display = 'block';
            });

            $(document).ajaxComplete(function(){
                document.getElementById('modal').style.display = 'none';
                document.getElementById('fade').style.display = 'none';
            });
        });
        
    </script>


</head>
<body class="col-md-12-col-sm-6 col-xs-12" style="padding:0; height: auto">
<div class="row" style="height:100vh; margin-right:0; margin-left:0;">
<div class="side_bar col-md-3">
    <div class=" identifier hidden-xs hidden-sm" >
        <span class="navbar-brand type" >T</span>
        <span class="navbar-brand type">M</span>
        <span class="navbar-brand type">S</span>
        <span class="navbar-brand type">H</span>
        <span class="navbar-brand type">C</span>
    </div>
    <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed pull-left" data-toggle="collapse" data-target="#hidden_bar" aria-expanded="false" style="border: 2px solid #fff ; margin-left:10px;">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar" style="border:1px solid #fff"></span>
            <span class="icon-bar" style="border:1px solid #fff"></span>
            <span class="icon-bar" style="border:1px solid #fff"></span>
        </button>
        <a class="navbar-brand" href="#">
            <div class="brand">
                <span class="navbar-brand  logoT" style="border: 1px solid #ffffff; margin-left: 2px; border-radius: 10px 0 0 0; font-family: 'Concert One', cursive; color: #ffffff" >T</span>
                <span class="navbar-brand  logoR" style="border: 1px solid #ffffff;  margin-left: 2px; font-family: 'Concert One', cursive; background: #ffffff; color: #3baba0;">R</span>
                <span class="navbar-brand   logoU" style="border: 1px solid #ffffff;  margin-left: 2px; font-family: 'Concert One', cursive; color: #ffffff;">U</span>
                <span class="navbar-brand   logoC" style="border: 1px solid #ffffff;  margin-left: 2px;font-family: 'Concert One', cursive; background: #ffffff; color: #3baba0;">C</span>
                <span class="navbar-brand  logoK" style="border: 1px solid #ffffff;  margin-left: 2px;font-family: 'Concert One', cursive; color: #ffffff"; >K</span>
                <span class="navbar-brand   logoK" style="border: 1px solid #ffffff;  margin-left: 2px;font-family: 'Concert One', cursive; background: #ffffff; color: #3baba0;">K</span>
                <span class="navbar-brand   logoA" style="border: 1px solid #ffffff;  margin-left: 2px; border-radius: 0 0 10px 0;font-family: 'Concert One', cursive; color: #ffffff;">A</span>
            </div>
        </a>
    </div>
    <!--NAVBAR FOR MOBIE SCREENS-->
    <ul class="nav navbar-nav collapse" id="hidden_bar">
        <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage Haulage Requests<span class="glyphicon glyphicon-file pull-right"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="{{url('/viewRequests')}}">View All Requests <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                    <li class="divider"></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Incident Management <span class="glyphicon glyphicon-file pull-right"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="{{url('/incidentManagement')}}">View All Incidents <span class="glyphicon glyphicon-open-file pull-right"></span></a></li>
                    <li class="divider"></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage Resources <span class="glyphicon glyphicon-wrench pull-right"></span></a>
                <ul class="dropdown-menu">
                    <li><a href='{{ url("/home") }}'>View All Resources <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                    <li class="divider"></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();" class="dropdown-toggle" data-toggle="dropdown">Log Out <span class="glyphicon glyphicon-log-out pull-right"></span></a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </li>



    </ul>
    <!--NAVBAR FOR laptop SCREENS-->
    <ul class="nav navbar-nav hidden-xs hidden-sm">
        <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage Haulage Requests<span class="glyphicon glyphicon-file pull-right"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="{{url('/viewRequests')}}">View All Requests <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                    <li class="divider"></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Incident Management <span class="glyphicon glyphicon-file pull-right"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="{{url('/incidentManagement')}}">View All Incidents <span class="glyphicon glyphicon-open-file pull-right"></span></a></li>
                    <li class="divider"></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage Resources <span class="glyphicon glyphicon-wrench pull-right"></span></a>
                <ul class="dropdown-menu">
                    <li><a href='{{ url("/home") }}'>View All Resources <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                    <li class="divider"></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();" class="dropdown-toggle" data-toggle="dropdown">Log Out <span class="glyphicon glyphicon-log-out pull-right"></span></a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </li>



    </ul>
</div>
<!-- TSMA VIEW ORDERS PAGE -->
<div class="tmsa_main_body col-md-9" style="height: 100vh;" >
    <div class="row" style="height: 100vh;">
        <div class="col-md-9 view-order" >
            <h4 align="center" class="btn-lg" style=" font-family: 'Josefin Sans', sans-serif; margin-left: 5%; margin-right: 5%; background: #ffffff; border: 2px solid #3baba0;">All Resources</h4>
            <div class="table-responsive">
                    <form id="orderDocument" enctype="multipart/form-data">
                <table id="mytable" class="table table-bordred table-striped originTable" style=" font-family: 'Josefin Sans'; font-size: 80%;">
                        {{csrf_field()}}
                    <thead >
                    <th align="center">Plate Number</th>
                    <th align="center">Haulage Details (Origin)</th>
                    <th align="center">Upload Document (Origin)</th>
                    </thead>
                    <tbody>
                    @foreach($resourceDetail as $request)
                    @foreach($request as $allRequest)
                    <tr class="row-content">
                        <td><h5>{{$allRequest->GPSID}}</h5></td>
                        <td>
                        <div class="row">
                            <div class="col-md-4">
                            <p>Time In</p>
                            <input type="text" class="input-md timepicker" placeholder="Time In" name="timeIn[]">
                            </div>
                            <div class="col-md-4">
                            <p>Time Out</p>
                            <input type="text" class="input-md timepicker" placeholder="Time Out" name="timeOut[]">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                            <p>Cargo Weight</p>
                            <input type="text" placeholder="Cargo Weight" name="cargoWeight[]">
                            </div>
                            <div class="col-md-4">
                            <p>Cargo Description</p>
                            <input type="text" placeholder="Cargo Description" name="cargoDescription[]">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-md-offset-4">
                                <p>Comments (Optional)</p>
                                <textarea name="comments[]"></textarea>
                            </div>
                        </div>
                        </td>
                        <td>
                        <input type="file" name="orderDocument[]" required>
                        <input type="hidden" name="gpsid[]" value="{{$allRequest->GPSID}}" required>
                        </td>
                    </tr>
                    @endforeach
                    @endforeach
                    </tbody>

                </table>
<div class="btn btn-md btn-success next">Next</div>
                <table id="mytable" class="table table-bordred table-striped destinationTable" style=" font-family: 'Josefin Sans'; font-size: 80%;">
                    <thead >
                    <th align="center">Plate Number</th>
                    <th align="center">Haulage Details (Destination)</th>
                    <th align="center">Upload Document (Destination)</th>
                    </thead>
                    <tbody>
                    @foreach($resourceDetail as $request)
                    @foreach($request as $allRequest)
                    <tr class="row-content">
                        <td><h5>{{$allRequest->GPSID}}</h5></td>
                        <td>
                        <div class="row">
                            <div class="col-md-4">
                            <p>Time In</p>
                            <input type="text" class="input-md timepicker" placeholder="Time In" name="timeIndest[]">
                            </div>
                            <div class="col-md-4">
                            <p>Time Out</p>
                            <input type="text" class="input-md timepicker" placeholder="Time Out" name="timeOutdest[]">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                            <p>Cargo Weight</p>
                            <input type="text" placeholder="Cargo Weight" name="cargoWeightdest[]">
                            </div>
                            <div class="col-md-4">
                            <p>Cargo Description</p>
                            <input type="text" placeholder="Cargo Description" name="cargoDescriptiondest[]">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-md-offset-4">
                                <p>Comments (Optional)</p>
                                <textarea name="commentsdest[]"></textarea>
                            </div>
                        </div>
                        </td>
                        <td>
                        <input type="file" name="orderDocumentdest[]" required>
                        <input type="hidden" name="gpsiddest[]" value="{{$allRequest->GPSID}}" required>
                        </td>
                    </tr>
                    @endforeach
                    @endforeach
                    </tbody>
                    
                <input type="hidden" name="service_id" value="{{$service_id}}">
                </table>
                    </form>
                <div class="clearfix"></div>
            </div>
                <div class="row hideOnNext">
                    <div class="col-md-4">
                        <button class="btn btn-success btn-md previous">Previous</button>
                    </div>
                    <div class="col-md-4">
                        <button class="btn btn-success btn-md uploadDocs">Upload</button>
                    </div>
                </div>
        </div>
    </div>

    <div id="fade"></div>
        <div id="modal">
            <img id="loader" src="{{url('images/69.gif')}}" />
        </div>
</div>
<!--     ************************************************** -->
<!--BOTTOM NAV BAR-->

<script type="text/javascript" src="{{url('js/tmsa.js')}}"></script>
<script type="text/javascript" src="{{url('js/uploadDocs.js')}}"></script>
</body>
</html>

