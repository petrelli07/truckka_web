<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>TMSHC</title>
    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css" href="{{ url('css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ url('css/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('css/indexupdate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('css/tmsa.css') }}">
    <link href="{{ url('https://fonts.googleapis.com/css?family=Titillium+Web|Concert+One|Josefin+Sans|Roboto') }}" rel="stylesheet">
    <script type="text/javascript" src="{{ url('js/admin_dashboard.js')}}"></script>
    <script type="text/javascript" src="{{ url('js/jquery-3.2.1.js')}}"></script>
    <script type="text/javascript" src="{{ url('js/bootstrap.js')}}"></script>
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web|Concert+One|Josefin+Sans|Roboto" rel="stylesheet">


</head>
<body class="col-md-12-col-sm-6 col-xs-12" style="padding:0; height: auto">
<div class="row" style="height:100vh; margin-right:0; margin-left:0;">
<div class="side_bar col-md-3">
    <div class=" identifier hidden-xs hidden-sm" >
        <span class="navbar-brand type" >T</span>
        <span class="navbar-brand type">M</span>
        <span class="navbar-brand type">S</span>
        <span class="navbar-brand type">H</span>
        <span class="navbar-brand type">C</span>
    </div>
    <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed pull-left" data-toggle="collapse" data-target="#hidden_bar" aria-expanded="false" style="border: 2px solid #fff ; margin-left:10px;">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar" style="border:1px solid #fff"></span>
            <span class="icon-bar" style="border:1px solid #fff"></span>
            <span class="icon-bar" style="border:1px solid #fff"></span>
        </button>
        <a class="navbar-brand" href="#">
            <div class="brand">
                <span class="navbar-brand  logoT" style="border: 1px solid #ffffff; margin-left: 2px; border-radius: 10px 0 0 0; font-family: 'Concert One', cursive; color: #ffffff" >T</span>
                <span class="navbar-brand  logoR" style="border: 1px solid #ffffff;  margin-left: 2px; font-family: 'Concert One', cursive; background: #ffffff; color: #3baba0;">R</span>
                <span class="navbar-brand   logoU" style="border: 1px solid #ffffff;  margin-left: 2px; font-family: 'Concert One', cursive; color: #ffffff;">U</span>
                <span class="navbar-brand   logoC" style="border: 1px solid #ffffff;  margin-left: 2px;font-family: 'Concert One', cursive; background: #ffffff; color: #3baba0;">C</span>
                <span class="navbar-brand  logoK" style="border: 1px solid #ffffff;  margin-left: 2px;font-family: 'Concert One', cursive; color: #ffffff"; >K</span>
                <span class="navbar-brand   logoK" style="border: 1px solid #ffffff;  margin-left: 2px;font-family: 'Concert One', cursive; background: #ffffff; color: #3baba0;">K</span>
                <span class="navbar-brand   logoA" style="border: 1px solid #ffffff;  margin-left: 2px; border-radius: 0 0 10px 0;font-family: 'Concert One', cursive; color: #ffffff;">A</span>
            </div>
        </a>
    </div>
    <!--NAVBAR FOR MOBIE SCREENS-->
    <ul class="nav navbar-nav collapse" id="hidden_bar">
        <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage Haulage Requests<span class="glyphicon glyphicon-file pull-right"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="{{url('/viewRequests')}}">View All Requests <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                    <li class="divider"></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Incident Management <span class="glyphicon glyphicon-file pull-right"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="{{url('/incidentManagement')}}">View All Incidents <span class="glyphicon glyphicon-open-file pull-right"></span></a></li>
                    <li class="divider"></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage Resources <span class="glyphicon glyphicon-wrench pull-right"></span></a>
                <ul class="dropdown-menu">
                    <li><a href='{{ url("/home") }}'>View All Resources <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                    <li class="divider"></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();" class="dropdown-toggle" data-toggle="dropdown">Log Out <span class="glyphicon glyphicon-log-out pull-right"></span></a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </li>


    </ul>
    <!--NAVBAR FOR laptop SCREENS-->
    <ul class="nav navbar-nav hidden-xs hidden-sm">
        <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage Haulage Requests<span class="glyphicon glyphicon-file pull-right"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="{{url('/viewRequests')}}">View All Requests <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                    <li class="divider"></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Incident Management <span class="glyphicon glyphicon-file pull-right"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="{{url('/incidentManagement')}}">View All Incidents <span class="glyphicon glyphicon-open-file pull-right"></span></a></li>
                    <li class="divider"></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage Resources <span class="glyphicon glyphicon-wrench pull-right"></span></a>
                <ul class="dropdown-menu">
                    <li><a href='{{ url("/home") }}'>View All Resources <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                    <li class="divider"></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();" class="dropdown-toggle" data-toggle="dropdown">Log Out <span class="glyphicon glyphicon-log-out pull-right"></span></a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </li>


    </ul>
</div>
<!-- TSMA VIEW ORDERS PAGE -->
<div class="tmsa_main_body col-md-9" style="height: 100vh;" >
    <div class="row" style="height: 100vh;">
        <div class="col-md-9 view-order" >
            <h4 align="center" class="btn-lg" style=" font-family: 'Josefin Sans', sans-serif; margin-left: 5%; margin-right: 5%; background: #ffffff; border: 2px solid #3baba0;">All Orders</h4>
            <div class="table-responsive">
                <table id="mytable" class="table table-bordred table-striped" style=" font-family: 'Josefin Sans'; font-size: 80%;">

                    <thead >
                    <th align="center">Order ID</th>
                    <th align="center">Destination</th>
                    <th align="center">Origin</th>
                    <th align="center">Status</th>
                    <th align="center">Action</th>
                    </thead>
                    <tbody>
                    @forelse($allRequests as $allRequest)
                    <tr class="row-content">
                        <td><h5>{{$allRequest->serviceIDNo}}</h5></td>
                        <td><h5>{{$allRequest->deliverTo}}</h5></td>
                        <td><h5>{{$allRequest->deliverFrom}}</h5></td>
                        <td>
                            <h5>
                                @if($allRequest->orderStatus == 0)
                                Pending(Under Review)
                                @elseif($allRequest->orderStatus == 1)
                                Review Passed (Pending)
                                @elseif($allRequest->orderStatus == 2)
                                Processing
                                @elseif($allRequest->orderStatus==3)
                                Processing
                                @elseif($allRequest->orderStatus==4)
                                Processing
                                @elseif($allRequest->orderStatus == 5)
                                Invoice Created
                                @elseIf($allRequest->orderStatus == 6)
                                Invoice Approved
                                @elseif($allRequest->orderStatus == 7)
                                Carrier Mobilized
                                @elseif($allRequest->orderStatus == 8)
                                Carrier Clocked In
                                @elseif($allRequest->orderStatus == 9)
                                Carrier Clocked Out
                                @elseif($allRequest->orderStatus == 10)
                                Transaction Closed
                                @endif
                            </h5>
                        </td>
                        <td>
                            <h5>
                                @if($allRequest->orderStatus >= 7)
                                <a class="btn btn-success edit" href='{{ url("/viewOrderResources/{$allRequest->serviceIDNo}") }}' title="View Order Details">
                                    View Order Resource Details  <i class="fa fa-eye" aria-hidden="true"></i>
                                </a>
                                @endif
                            </h5>
                        </td>
                    </tr>
                    @empty
                    No orders Exist Yet
                    @endforelse
                    {{$allRequests->links()}}
                    </tbody>

                </table>

                <div class="clearfix"></div>


            </div>

        </div>
    </div>

    <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                    <h4 class="modal-title custom_align" id="Heading">Edit Your Detail</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <input class="form-control " type="text" placeholder="Mohsin">
                    </div>
                    <div class="form-group">

                        <input class="form-control " type="text" placeholder="Irshad">
                    </div>
                    <div class="form-group">
                        <textarea rows="2" class="form-control" placeholder="CB 106/107 Street # 11 Wah Cantt Islamabad Pakistan"></textarea>


                    </div>
                </div>
                <div class="modal-footer ">
                    <button type="button" class="btn btn-warning btn-lg" style="width: 100%;"><span class="glyphicon glyphicon-ok-sign"></span> Update</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>



    <div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                    <h4 class="modal-title custom_align" id="Heading">Delete this entry</h4>
                </div>
                <div class="modal-body">

                    <div class="alert alert-danger"><span class="glyphicon glyphicon-warning-sign"></span> Are you sure you want to delete this Record?</div>

                </div>
                <div class="modal-footer ">
                    <button type="button" class="btn btn-success" ><span class="glyphicon glyphicon-ok-sign"></span> Yes</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> No</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

</div>
<!--     ************************************************** -->
<!--BOTTOM NAV BAR-->

<script type="text/javascript" src="{{url('js/tmsa.js')}}"></script>
</body>
</html>

