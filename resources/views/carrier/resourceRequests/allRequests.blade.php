<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>TMSA</title>
    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css" href="{{ url('css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ url('css/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('css/indexupdate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('css/tmsa.css') }}">
    <link href="{{ url('https://fonts.googleapis.com/css?family=Titillium+Web|Concert+One|Josefin+Sans|Roboto') }}" rel="stylesheet">
    <script type="text/javascript" src="{{ url('js/admin_dashboard.js')}}"></script>
    <script type="text/javascript" src="{{ url('js/jquery-3.2.1.js')}}"></script>
    <script type="text/javascript" src="{{ url('js/bootstrap.js')}}"></script>

    
</head>


<body class="col-md-12-col-sm-6 col-xs-12" style="padding:0;">
<div class="row" style="height:100vh; margin-right:0; margin-left:0; background-color: #232323;">
    <div class="side_bar col-md-3">
        <div class=" identifier hidden-xs hidden-sm" >
            <span class="navbar-brand type" >T</span>
            <span class="navbar-brand type">M</span>
            <span class="navbar-brand type">S</span>
            <span class="navbar-brand type">H</span>
            <span class="navbar-brand type">C</span>
        </div>
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed pull-left" data-toggle="collapse" data-target="#hidden_bar" aria-expanded="false" style="border: 2px solid #fff ; margin-left:10px;">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar" style="border:1px solid #fff"></span>
                <span class="icon-bar" style="border:1px solid #fff"></span>
                <span class="icon-bar" style="border:1px solid #fff"></span>
            </button>
            <a class="navbar-brand" href="#">
                <div class="brand">
                    <span class="navbar-brand  logoT" style="border: 1px solid #ffffff; margin-left: 2px; border-radius: 10px 0 0 0; font-family: 'Concert One', cursive; color: #ffffff" >T</span>
                    <span class="navbar-brand  logoR" style="border: 1px solid #ffffff;  margin-left: 2px; font-family: 'Concert One', cursive; background: #ffffff; color: #3baba0;">R</span>
                    <span class="navbar-brand   logoU" style="border: 1px solid #ffffff;  margin-left: 2px; font-family: 'Concert One', cursive; color: #ffffff;">U</span>
                    <span class="navbar-brand   logoC" style="border: 1px solid #ffffff;  margin-left: 2px;font-family: 'Concert One', cursive; background: #ffffff; color: #3baba0;">C</span>
                    <span class="navbar-brand  logoK" style="border: 1px solid #ffffff;  margin-left: 2px;font-family: 'Concert One', cursive; color: #ffffff"; >K</span>
                    <span class="navbar-brand   logoK" style="border: 1px solid #ffffff;  margin-left: 2px;font-family: 'Concert One', cursive; background: #ffffff; color: #3baba0;">K</span>
                    <span class="navbar-brand   logoA" style="border: 1px solid #ffffff;  margin-left: 2px; border-radius: 0 0 10px 0;font-family: 'Concert One', cursive; color: #ffffff;">A</span>
                </div>
            </a>
        </div>
        <!--NAVBAR FOR mobile SCREENS-->
        <ul class="nav navbar-nav collapse" id="hidden_bar" style="margin-top:50px;">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage Haulage Requests<span class="glyphicon glyphicon-file pull-right"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="{{url('/viewRequests')}}">View All Requests <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                    <li class="divider"></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Incident Management <span class="glyphicon glyphicon-file pull-right"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="{{url('/incidentManagement')}}">View All Incidents <span class="glyphicon glyphicon-open-file pull-right"></span></a></li>
                    <li class="divider"></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage Resources <span class="glyphicon glyphicon-wrench pull-right"></span></a>
                <ul class="dropdown-menu">
                    <li><a href='{{ url("/home") }}'>View All Resources <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                    <li class="divider"></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();" class="dropdown-toggle" data-toggle="dropdown">Log Out <span class="glyphicon glyphicon-log-out pull-right"></span></a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </li>


        </ul>
        <!--NAVBAR FOR laptop SCREENS-->
        <ul class="nav navbar-nav hidden-xs hidden-sm">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage Haulage Requests<span class="glyphicon glyphicon-file pull-right"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="{{url('/viewRequests')}}">View All Requests <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                    <li class="divider"></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Incident Management <span class="glyphicon glyphicon-file pull-right"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="{{url('/incidentManagement')}}">View All Incidents <span class="glyphicon glyphicon-open-file pull-right"></span></a></li>
                    <li class="divider"></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage Resources <span class="glyphicon glyphicon-wrench pull-right"></span></a>
                <ul class="dropdown-menu">
                    <li><a href='{{ url("/home") }}'>View All Resources <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                    <li class="divider"></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();" class="dropdown-toggle" data-toggle="dropdown">Log Out <span class="glyphicon glyphicon-log-out pull-right"></span></a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </li>


        </ul>
        <!--NAVBAR FOR laptop SCREENS-->
        <ul class="nav navbar-nav hidden-xs hidden-sm">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage Haulage Requests<span class="glyphicon glyphicon-file pull-right"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="{{url('/viewRequests')}}">View All Requests <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                    <li class="divider"></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Incident Management <span class="glyphicon glyphicon-file pull-right"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="{{url('/incidentManagement')}}">View All Incidents <span class="glyphicon glyphicon-open-file pull-right"></span></a></li>
                    <li class="divider"></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage Resources <span class="glyphicon glyphicon-wrench pull-right"></span></a>
                <ul class="dropdown-menu">
                    <li><a href='{{ url("/home") }}'>View All Resources <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                    <li class="divider"></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();" class="dropdown-toggle" data-toggle="dropdown">Log Out <span class="glyphicon glyphicon-log-out pull-right"></span></a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </li>


        </ul>

        <!--NAVBAR FOR laptop SCREENS-->
        <ul class="nav navbar-nav hidden-xs hidden-sm">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage Haulage Requests<span class="glyphicon glyphicon-file pull-right"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="{{url('/viewRequests')}}">View All Requests <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                    <li class="divider"></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Incident Management <span class="glyphicon glyphicon-file pull-right"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="{{url('/incidentManagement')}}">View All Incidents <span class="glyphicon glyphicon-open-file pull-right"></span></a></li>
                    <li class="divider"></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage Resources <span class="glyphicon glyphicon-wrench pull-right"></span></a>
                <ul class="dropdown-menu">
                    <li><a href='{{ url("/home") }}'>View All Resources <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                    <li class="divider"></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();" class="dropdown-toggle" data-toggle="dropdown">Log Out <span class="glyphicon glyphicon-log-out pull-right"></span></a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </li>


        </ul>


        </ul>
    </div>
    <!-- TSMA VIEW ORDERS PAGE -->
    <div class="tmsa_main_body col-md-9" style="height:100vh;">
        <div class="row" style="height:100vh;">
            <div class="col-md-9 view-order" style="height:auto;" >
                <h4 align="center" class="btn-lg" style=" font-family: 'Josefin Sans', sans-serif; margin-left: 5%; margin-right: 5%; background: #ffffff; border: 2px solid #C6FF00;">All Haulage Requests</h4>
                <div class="table-responsive">
                    <table id="mytable" class="table table-bordred table-striped" style=" font-family: 'Josefin Sans'; font-size: 80%;">

                        <thead >
                        <th>Order ID</th>
                        <th>Action</th>
                        </thead>
                        <tbody>

                        @foreach($haulageRequests as $collection)

                        @foreach($collection as $item)
                        <tr class="row-content">
                            <td><h5>{{$item->serviceIDNo}}</h5></td>
                            <td>
                                @if($item->orderStatus == 1)
                                <a class="btn btn-success edit" href='{{ url("/confirm/{$item->serviceIDNo}") }}' title="Approve">
                                    <i class="fa fa-check-circle-o" aria-hidden="true"></i>
                                </a>|
                                <a class="btn btn-success edit" href='{{ url("/view_order/{$item->serviceIDNo}") }}' title="View Details">
                                    View Order Details <i class="fa fa-eye" aria-hidden="true"></i>
                                </a>
                                @elseif($item->orderStatus == 2)
                                <a class="btn btn-success edit" href='{{ url("/view_order/{$item->serviceIDNo}") }}' title="View Details">
                                    View Order Details <i class="fa fa-eye" aria-hidden="true"></i>
                                </a>
                                @elseif($item->orderStatus == 3)
                                <a class="btn btn-success edit" href='{{ url("/view_order/{$item->serviceIDNo}") }}' title="View Details">
                                    View Order Details <i class="fa fa-eye" aria-hidden="true"></i>
                                </a>
                                @elseif($item->orderStatus == 4)
                                <a class="btn btn-success edit" href='{{ url("/view_order/{$item->serviceIDNo}") }}' title="View Details">
                                    View Order Details <i class="fa fa-eye" aria-hidden="true"></i>
                                </a>
                                @elseif($item->orderStatus == 5)
                                <a class="btn btn-success edit" href='{{ url("/view_order/{$item->serviceIDNo}") }}' title="View Details">
                                    View Order Details   <i class="fa fa-eye" aria-hidden="true"></i>
                                </a>|
                                <a class="btn btn-success edit" href='{{ url("/generateToken/{$item->serviceIDNo}") }}' title="View Details">
                                    Generate Driver Token<i class="fa fa-eye" aria-hidden="true"></i>
                                </a>
                                @elseif($item->orderStatus == 6)
                                <a class="btn btn-success edit" href='{{ url("/view_order/{$item->serviceIDNo}") }}' title="View Details">
                                    View Order Details  <i class="fa fa-eye" aria-hidden="true"></i>
                                </a>
                                @if($item->carrierDocStatus == 0)
                                |<a class="btn btn-success edit" href='{{ url("/viewOrderResources/{$item->serviceIDNo}") }}' title="Upload Order Documentation">
                                    Upload Order Documentation<i class="fa fa-eye" aria-hidden="true"></i>
                                </a>
                                @else
                                |<a class="btn btn-success edit" href='{{ url("/creditNote/{$item->serviceIDNo}") }}' title="View Credit Note">
                                    View Credit Note<i class="fa fa-eye" aria-hidden="true"></i>
                                </a>
                                @endif
                                @elseif($item->orderStatus == 7)
                                <a class="btn btn-success edit" href='{{ url("/view_order/{$item->serviceIDNo}") }}' title="View Details">
                                    View Order Details   <i class="fa fa-eye" aria-hidden="true"></i>
                                </a>
                                @if($item->carrierDocStatus == 0)
                                |<a class="btn btn-success edit" href='{{ url("/viewOrderResources/{$item->serviceIDNo}") }}' title="Upload Order Documentation">
                                    Upload Order Documentation<i class="fa fa-eye" aria-hidden="true"></i>
                                </a>
                                @else
                                |<a class="btn btn-success edit" href='{{ url("/creditNote/{$item->serviceIDNo}") }}' title="View Credit Note">
                                    View Credit Note<i class="fa fa-eye" aria-hidden="true"></i>
                                </a>
                                @endif
                                @elseif($item->orderStatus == 8)
                                <a class="btn btn-success edit" href='{{ url("/view_order/{$item->serviceIDNo}") }}' title="View Details">
                                    View Order Details   <i class="fa fa-eye" aria-hidden="true"></i>
                                </a>
                                @if($item->carrierDocStatus == 0)
                                |<a class="btn btn-success edit" href='{{ url("/viewOrderResources/{$item->serviceIDNo}") }}' title="Upload Order Documentation">
                                    Upload Order Documentation<i class="fa fa-eye" aria-hidden="true"></i>
                                </a>
                                @else
                                |<a class="btn btn-success edit" href='{{ url("/creditNote/{$item->serviceIDNo}") }}' title="View Credit Note">
                                    View Credit Note<i class="fa fa-eye" aria-hidden="true"></i>
                                </a>
                                @endif
                                @elseif($item->orderStatus == 9)
                                <a class="btn btn-success edit" href='{{ url("/view_order/{$item->serviceIDNo}") }}' title="View Details">
                                    View Order Details   <i class="fa fa-eye" aria-hidden="true"></i>
                                </a>
                                @if($item->carrierDocStatus == 0)
                                |<a class="btn btn-success edit" href='{{ url("/viewOrderResources/{$item->serviceIDNo}") }}' title="Upload Order Documentation">
                                    Upload Order Documentation<i class="fa fa-eye" aria-hidden="true"></i>
                                </a>
                                @else
                                |<a class="btn btn-success edit" href='{{ url("/creditNote/{$item->serviceIDNo}") }}' title="View Credit Note">
                                    View Credit Note<i class="fa fa-eye" aria-hidden="true"></i>
                                </a>
                                @endif
                                @endif
                            </td>
                        </tr>
                        @endforeach
                        @endforeach

                        </tbody>

                    </table>

                </div>

            </div>
        </div>

        



        

    </div>
    <!--     ************************************************** -->
    <!--BOTTOM NAV BAR-->


</body>

</html>