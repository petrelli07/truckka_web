
    @include('carrier.includes.header')
    @include('carrier.includes.head')
    <!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-content-white page-md">
        <div class="page-wrapper">
            <!-- BEGIN HEADER -->
                @include('carrier.includes.menuBar')

            <!-- END HEADER -->
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
            <!-- END HEADER & CONTENT DIVIDER -->
            <!-- BEGIN CONTAINER -->
            <div class="page-container">
                <!-- BEGIN SIDEBAR -->
                    @include('carrier.includes.sidebar')
                <!-- END SIDEBAR -->
                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <!-- BEGIN PAGE HEADER-->
                        <!-- BEGIN THEME PANEL -->
                       
                        <!-- END THEME PANEL -->
                        <!-- BEGIN PAGE BAR -->
                        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    <a href="index.html">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Dashboard</span>
                                </li>
                            </ul>
                            <div class="page-toolbar">
                                <div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
                                    <i class="icon-calendar"></i>&nbsp;
                                    <span class="thin uppercase hidden-xs"></span>&nbsp;
                                    <i class="fa fa-angle-down"></i>
                                </div>
                            </div>
                        </div>
                        <!-- END PAGE BAR -->
                        <!-- BEGIN PAGE TITLE-->
                        <h1 class="page-title">
                        </h1>
                        <!-- END PAGE TITLE-->
                        <!-- END PAGE HEADER-->
                        <div class="row">
                            <div class="col-lg-12  col-xs-12 col-sm-12">
                                      <div class="portlet box green">
                                                    <div class="portlet-title">
                                                        <div class="caption">
                                                          My Orders </div>
                                                        <div class="tools"> </div>
                                                    </div>
                                                    <div class="portlet-body table-responsive">
                                                        <table class="table table-striped table-bordered table-hover " id="sample_2">
                                                            <thead>
                                                                <tr>
                                                                <th align="center">Select</th>               
                                                                <th align="center">Order ID</th>
                                                                    <th align="center">Destination</th>
                                                                    <th align="center">Origin</th>
                                                                    <th align="center">Pickup Date</th>
                                                                    <th align="center">Resource Allocation</th>
                                                                    <th align="center">Action</th>
                                                                    <!-- <th> OrderID </th>
                                                                    <th> Origin</th>
                                                                    <th> Destination </th>
                                                                    <th> Status</th>
                                                                    <th> Action </th> -->
                                                                </tr>
                                                            </thead>
                                                            <tbody>

                                                            @forelse($orders as $allRequest)
                                                                <tr>
                                                                    <td><input type="checkbox" value="{{$allRequest->id}}" name="resource[]"></td>                                                                    <td><h5>{{$allRequest->serviceIDNo}}</h5></td>
                                                                    <td><h5>{{$allRequest->deliverTo}}</h5></td>
                                                                    <td><h5>{{$allRequest->deliverFrom}}</h5></td>
                                                                    <td>
                                                                        <h5>
                                                                           {{date("l, jS M, Y", strtotime($allRequest->pickUpDate))}}
                                                                        </h5>
                                                                    </td>
                                                                    <td>
                                                                        <div class="progress" style="height: 15px!important;margin-bottom: 0px !important;">
                                                                            <?php
                                                                               $resourceLimit   = $allRequest->resource_limit;
                                                                               $resourceNumber  = $allRequest->resource_number;
                                                                               $calcuations = floor(($resourceNumber/$resourceLimit) *100);
                                                                               if ($calcuations <= 30) {
                                                                                    $status     =    'progress-bar-warning';
                                                                               }elseif($calcuations > 30 && $calcuations  <= 70){
                                                                                    $status     =   'progress-bar-warning';

                                                                               }elseif ($calcuations > 70 && $calcuations  < 100) {
                                                                                     $status     =   'progress-bar-info';
                                                                               }elseif ($calcuations == 100) {
                                                                                    $status     =   'progress-bar-success';
                                                                               }
                                                                            ?>

                                                                          <div class="progress-bar {{$status }} progress-bar-striped active" role="progressbar"
                                                                          aria-valuenow="{{$calcuations}}" aria-valuemin="0" aria-valuemax="100" style="width:{{$calcuations}}%;">

                                                                          </div>
                                                                        </div>
                                                                         <p class="f-paragraph bold align-center
                                                                         " style="margin-bottom: 0px;">{{$resourceNumber}}/{{$resourceLimit}}</p>
                                                                    </td>
                                                                    <td><a href="" class="btn btn-primary btn-md">view</a> 
                                                                        @if($resourceNumber < $resourceLimit)
                                                                            <a href="{{url('/add-resource-to-order/'.$allRequest->service_request_id)}}" class="btn btn-danger btn-md">Add Resource</a>
                                                                        @endif
                                                                    </td>
                                                                </tr>
                                                                @empty
                                                                No orders Exist Yet
                                                                @endforelse

                                                              <!-- <tr>
                                                                <td>1003</td>
                                                                <td>Tin Can A - Lagos</td>
                                                                <td>Owerri</td>
                                                                <td><span class="label label-sm label-success">Pending</span></td>
                                                                <td><button class="btn btn-primary">View</button> <button data-toggle="modal" href="#cancel" class="btn btn-danger">Cancel</button></td>
                                                              </tr> -->
                                                            </tbody>
                                                        </table>
                                                    </div>
                                      </div>

                            </div>
                            
                        </div>
                        
                    </div>
                    <!-- END CONTENT BODY -->
                </div>
                <!-- END CONTENT -->
                <!-- BEGIN QUICK SIDEBAR -->
                <a href="javascript:;" class="page-quick-sidebar-toggler">
                    <i class="icon-login"></i>
                </a>
               
                <!-- END QUICK SIDEBAR -->
            </div>
            <!-- END CONTAINER -->
            <!-- BEGIN FOOTER -->
           <div class="page-footer">
                <div class="page-footer-inner"> 2018 &copy; Truckka 
                      </div>
                <div class="scroll-to-top">
                    <i class="icon-arrow-up"></i>
                </div>
            </div>|&nbsp;
                    
                </div>
                <div class="scroll-to-top">
                    <i class="icon-arrow-up"></i>
                </div>
            </div>
            <!-- END FOOTER -->
        </div>
        <!-- BEGIN QUICK NAV -->
        
        <div class="quick-nav-overlay"></div>
 @include('carrier.includes.footer')
