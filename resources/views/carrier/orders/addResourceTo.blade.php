
    @include('carrier.includes.header')
    @include('carrier.includes.head')
    <!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-content-white page-md">
      
        <div class="page-wrapper">
            <!-- BEGIN HEADER -->
                @include('carrier.includes.menuBar')

            <!-- END HEADER -->
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
            <!-- END HEADER & CONTENT DIVIDER -->
            <!-- BEGIN CONTAINER -->
            <div class="page-container">
                <!-- BEGIN SIDEBAR -->
                    @include('carrier.includes.sidebar')
                <!-- END SIDEBAR -->
                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <!-- BEGIN PAGE HEADER-->
                        <!-- BEGIN THEME PANEL -->
                       
                        <!-- END THEME PANEL -->
                        <!-- BEGIN PAGE BAR -->
                        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    <a href="index.html">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Dashboard</span>
                                </li>
                            </ul>
                            
                        </div>
                        <h1 class="page-title"> Olam Enterprise
                        </h1>
                        <!-- END PAGE TITLE-->
                        <!-- END PAGE HEADER-->
                    <?php $higherId      = 0?>
                    @foreach($orders as $order)
                     <?php $distinctId   =   $higherId++;?>
                      <div class="row">
                            <div class="col-lg-10 col-lg-offset-1 col-xs-12 col-sm-12">
                                <div class="portlet light ">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <span class="caption-subject font-dark bold uppercase">Add Resources to <i>Order :</i> {{$order->serviceIDNo}}</span>                                         
                                        </div>
                                        
                                    </div>
                                     <div class="portlet-body form">
                                        <div class="append-selected-resources append-selected-resources-{{$distinctId}} flex-container flex-wrap">

                                        </div>


                                        <form role="form" action="{{url('/attach-resource')}}"  class="resourceForm ajax-form-submit" id="resource-form" data-check-limit="{{$order->resource_limit}}" data-check-attached="{{sizeof(array_filter(explode(',', $order->attachResource)))}}">
                                            {{csrf_field()}}
                                            <input type="text"  name="orderId" value="{{$order->service_request_id}}" hidden="hidden">
                                            <div class="form-body">
                                              <div class="form-group form-md-checkboxes">
                                                <div class="md-checkbox-inline">
                                                    <?php $selectedResources = explode(',', $order->attachResource);?>
                                                    @foreach($resources as $resource)
                                                        @if(!in_array($resource->id, $selectedResources)) 
                                                            <div class="md-checkbox">
                                                                <input type="checkbox" value="{{$resource->id}}" appendTo="append-selected-resources-{{$distinctId}}"  
                                                                
                                                                   autocomplete="off" 
                                                                id="{{$resource->plateNumber.$distinctId}}" class="md-check mt-select-resource" number="{{$resource->plateNumber}}" name="resources[]">
                                                                <label for="{{$resource->plateNumber.$distinctId}}">
                                                                    <span></span>
                                                                    <span class="check"></span>
                                                                    <span class="box"></span> {{$resource->plateNumber}} </label>
                                                            </div>
                                                        @endif
                                                    @endforeach
                                                </div>
                                              </div>
                                               
                                            </div>
                                            <div class="form-actions noborder">
                                                <button type="submit" class="btn btn-primary" data-toggle="modal" href="#order_summary">Submit</button>
                                               
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        @endforeach
                        
                    </div>
                    <!-- END CONTENT BODY -->
                </div>
                <!-- END CONTENT -->
                <!-- BEGIN QUICK SIDEBAR -->
                <a href="javascript:;" class="page-quick-sidebar-toggler">
                    <i class="icon-login"></i>
                </a>
                
                <!-- END QUICK SIDEBAR -->
            </div>
            <!-- END CONTAINER -->
            <!-- BEGIN FOOTER -->
            <div class="page-footer">
                <div class="page-footer-inner"> 2016 &copy; Metronic Theme By
                    <a target="_blank" href="http://keenthemes.com">Keenthemes</a> &nbsp;|&nbsp;
                    <a href="http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes" title="Purchase Metronic just for 27$ and get lifetime updates for free" target="_blank">Purchase Metronic!</a>
                </div>
                <div class="scroll-to-top">
                    <i class="icon-arrow-up"></i>
                </div>
            </div>
            <!-- END FOOTER -->
        </div>
        <!-- BEGIN QUICK NAV -->
    
        <div class="quick-nav-overlay"></div>
 @include('carrier.includes.footer')
