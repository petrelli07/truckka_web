
    @include('carrier.includes.header')
    @include('carrier.includes.head')
    <!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-content-white page-md">
        <div class="page-wrapper">
            <!-- BEGIN HEADER -->
                @include('carrier.includes.menuBar')

            <!-- END HEADER -->
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
            <!-- END HEADER & CONTENT DIVIDER -->
            <!-- BEGIN CONTAINER -->
            <div class="page-container">
                <!-- BEGIN SIDEBAR -->
                    @include('carrier.includes.sidebar')
                <!-- END SIDEBAR -->
                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <!-- BEGIN PAGE HEADER-->
                        <!-- BEGIN THEME PANEL -->
                       
                        <!-- END THEME PANEL -->
                        <!-- BEGIN PAGE BAR -->
                        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    <a href="index.html">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Dashboard</span>
                                </li>
                            </ul>
                            <div class="page-toolbar">
                                <div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
                                    <i class="icon-calendar"></i>&nbsp;
                                    <span class="thin uppercase hidden-xs"></span>&nbsp;
                                    <i class="fa fa-angle-down"></i>
                                </div>
                            </div>
                        </div>
                        <!-- END PAGE BAR -->
                        <!-- BEGIN PAGE TITLE-->
                        <h1 class="page-title">
                        </h1>
                        <!-- END PAGE TITLE-->
                        <!-- END PAGE HEADER-->
                        <div class="row">
                            <div class="col-lg-12  col-xs-12 col-sm-12">
                                      <div class="portlet box green">
                                                    <div class="portlet-title">
                                                        <div class="caption">
                                                          My Resources </div>
                                                        <div class="tools"> </div>
                                                    </div>
                                                    <div class="portlet-body">
                                                    <form id="myResourceForm" method="POST">
                                                        {{csrf_field()}}
                                                        <table class="table table-striped table-bordered table-hover" id="sample_2">
                                                            <thead>
                                                                <tr>
                                                                <th align="center">Select</th>
                                                                <th align="center">Platenumber</th>
                                                                <th align="center">Drivername</th>
                                                                <th align="center">Drivernumber</th>
                                                                <th align="center">Capacity</th>
                                                                <th align="center">Status</th>
                                                                <th align="center">Action</th>
                                                                    <!-- <th> OrderID </th>
                                                                    <th> Origin</th>
                                                                    <th> Destination </th>
                                                                    <th> Status</th>
                                                                    <th> Action </th> -->
                                                                </tr>
                                                            </thead>
                                                                
                                                            <tbody>
                                                                @foreach($user_resources as $resource)
                                                                <tr>
                                                                    <td><input type="checkbox" value="{{$resource->id}}" name="resource[]"></td>
                                                                    <td><h5>{{$resource->plateNumber}}</h5></td>
                                                                    <td><h5>{{$resource->driverName}}</h5></td>
                                                                    <td><h5>{{$resource->driverPhoneNumber}}</h5></td>
                                                                    <td><h5>{{$resource->capacity_value}}</h5></td>
                                                                    <td>
                                                                        <h5>
                                                                          @if($resource->resourceStatus == 0)
                                                                           <span style="color: green">Ready <i class="fas fa-circle"></i></span>
                                                                          @elseif($resource->resourceStatus == 1)
                                                                            <span style="color: red">Blocked <i class="fas fa-circle"></i></span>
                                                                          @endif
                                                                        </h5>
                                                                    </td>
                                                                    <td>
                                                                    <div class="btn-group col55">
                                                                          <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                                                              <i class="fa fa-angle-down"></i>
                                                                          </button>
                                                                          <ul class="dropdown-menu pull-left" role="menu">
                                                                              <li>
                                                                                  <a href="{{url('carrier/map/view/'.$resource->id)}}">
                                                                                    <label class="label label-success">Track</label>
                                                                                  </a>
                                                                              </li>
                                                                              <li>
                                                                                 @if($resource->resourceStatus == 0)
                                                                                  <a href="{{url('/deactivate-resource/'.$resource->id)}}" notify-text="This resource would be able to take part in any order." class="notify">
                                                                                    <label class="label label-info">De-activate</label>
                                                                                  </a>
                                                                                  @else
                                                                                    <a href="javascript:;">
                                                                                      <label class="label label-info">Activate</label>
                                                                                    </a>
                                                                                  @endif

                                                                              </li>
                                                                             <li>
                                                                                  <a href="{{url('/edit-resources/'.$resource->id)}}">
                                                                                    <label class="label label-info">Edit</label>
                                                                                  </a>
                                                                              </li>
                                                                             
                                                                          </ul>
                                                                      </div>
                                                                    </td>
                                                                </tr>
                                                                @endforeach

                                                              <!-- <tr>
                                                                <td>1003</td>
                                                                <td>Tin Can A - Lagos</td>
                                                                <td>Owerri</td>
                                                                <td><span class="label label-sm label-success">Pending</span></td>
                                                                <td><button class="btn btn-primary">View</button> <button data-toggle="modal" href="#cancel" class="btn btn-danger">Cancel</button></td>
                                                              </tr> -->
                                                            </tbody>

                                                        </table>
                                                        <div class="flex-container">
                                                            <p class="f-paragraph ">With Selected:</p> 
                                                            <div class="btn-group col55">
                                                                          <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                                                              <i class="fa fa-angle-down"></i>
                                                                          </button>
                                                                          <ul class="dropdown-menu pull-left" role="menu">
                                                                              
                                                                              
                                                                              <li style="margin-bottom: 5px; margin-left: 5px;">
                                                                                    <button type="submit" class="btn btn-info submitSelect btn-xs" form-action="{{url('/activate-resource')}}" form-target="myResourceForm">Activate</button  >
                                                                              </li>
                                                                             <li style="margin-bottom: 5px; margin-left: 5px;">
                                                                                    <button type="submit" class="btn btn-info submitSelect btn-xs" form-action="{{url('/edit-resources')}}" form-target="myResourceForm">Edit</button >
                                                                              </li>
                                                                              <li style="margin-bottom: 5px; margin-left: 5px;">
                                                                                   <button type="submit" class="btn btn-info submitSelect btn-xs" form-action="{{url('/deactivate-resource')}}" form-target="myResourceForm">De-activate</button >
                                                                              </li>
                                                                             
                                                                          </ul>
                                                                      </div>
                                                        </div>
                                                    </form>
                                                    </div>
                                      </div>

                            </div>
                            
                        </div>
                        
                    </div>
                    <!-- END CONTENT BODY -->
                </div>
                <!-- END CONTENT -->
                <!-- BEGIN QUICK SIDEBAR -->
                <a href="javascript:;" class="page-quick-sidebar-toggler">
                    <i class="icon-login"></i>
                </a>
               
                <!-- END QUICK SIDEBAR -->
            </div>
            <!-- END CONTAINER -->
            <!-- BEGIN FOOTER -->
           <div class="page-footer">
                <div class="page-footer-inner"> 2018 &copy; Truckka 
                      </div>
                <div class="scroll-to-top">
                    <i class="icon-arrow-up"></i>
                </div>
            </div>|&nbsp;
                    
                </div>
                <div class="scroll-to-top">
                    <i class="icon-arrow-up"></i>
                </div>
            </div>
            <!-- END FOOTER -->
        </div>
        <!-- BEGIN QUICK NAV -->
        
        <div class="quick-nav-overlay"></div>
 @include('carrier.includes.footer')
