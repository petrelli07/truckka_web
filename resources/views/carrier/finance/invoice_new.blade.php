
    @include('includes.header')
    @include('includes.head')
    <!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-content-white page-md">
        <div class="page-wrapper">
            <!-- BEGIN HEADER -->
                @include('includes.menuBar')

            <!-- END HEADER -->
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
            <!-- END HEADER & CONTENT DIVIDER -->
            <!-- BEGIN CONTAINER -->
            <div class="page-container">
                <!-- BEGIN SIDEBAR -->
                    @include('includes.sidebar')
                <!-- END SIDEBAR -->
                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <!-- BEGIN PAGE HEADER-->
                        <!-- BEGIN THEME PANEL -->
                       
                        <!-- END THEME PANEL -->
                        <!-- BEGIN PAGE BAR -->
                        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    <a href="index.html">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>General</span>
                                </li>
                            </ul>
                            
                        </div>
                        <!-- END PAGE BAR -->
                        <!-- BEGIN PAGE TITLE-->
                       
                        <!-- END PAGE TITLE-->
                        <!-- END PAGE HEADER-->
                        <div class="invoice">
                            <div class="row invoice-logo">
                                <div class="col-xs-6 invoice-logo-space">
                                    <img src="{{url('assets/')}}" class="img-responsive" alt="" /> </div>
                                <div class="col-xs-6">
                                   
                                </div>
                            </div>
                            <hr/>
                            <div class="row">
                            
                                
                            <div class="col-sm-4">
                                <address>
                                    <strong>Order No:</strong>
                                    {{$details[0]->serviceIDNo}}
                                    <br><strong>Invoice No:</strong>
                                    {{$details[0]->invoice_id}}
                                </address>
                            </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <table class="table table-striped table-hover">
                                        <thead>
                                            <tr>
                                    <td><strong>#</strong></td>
                                    <td class="text-center"><strong>Description</strong></td>
                                    <td class="text-center"><strong>Amount</strong></td>
                                        </tr>
                                        </thead>
                                    <tbody>
                                <?php $counter = 1;?>
                                <!-- foreach ($order->lineItems as $line) or some such thing here -->
                                @foreach($details as $detail)
                                <tr>
                                    <td>{{ $counter++}}</td>
                                    <td class="text-center">{{ $detail->description }}</td>
                                    <td class="text-center"> 
                                        @if($detail->cr != Null)
                                          &#8358; {{ $detail->cr}}
                                        @else
                                           &#8358; {{$detail->dr}}
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                                </div>
                            </div>
                            <div class="row" style="margin-bottom: 100px;">
                                <div class="col-xs-4 invoice-block">
                                    
                                    <a class="btn btn-lg blue hidden-print margin-bottom-5" onclick="javascript:window.print();"> Print
                                        <i class="fa fa-print"></i>
                                    </a>
                                    
                                </div>
                                <div class="col-xs-4 col-xs-offset-4">
                                    <div class="well">
                                        <ul class="list-unstyled amounts">
                                        <li>
                                            <strong>Gross amount:</strong>  &#8358; @if($detail->cr != Null)
                                            {{$details[0]->cr}}
                                        @else
                                            {{$details[0]->dr}}
                                        @endif </li>
                                           
                                        </ul>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <!-- END CONTENT BODY -->
                </div>
                <!-- END CONTENT -->
                <!-- BEGIN QUICK SIDEBAR -->
                <a href="javascript:;" class="page-quick-sidebar-toggler">
                    <i class="icon-login"></i>
                </a>
               
                <!-- END QUICK SIDEBAR -->
            </div>
            <!-- END CONTAINER -->
            <!-- BEGIN FOOTER -->
            
            <!-- END FOOTER -->
        </div>
        <!-- BEGIN QUICK NAV -->
        <nav class="quick-nav">
            <a class="quick-nav-trigger" href="#0">
                <span aria-hidden="true"></span>
            </a>
           
            <span aria-hidden="true" class="quick-nav-bg"></span>
        </nav>
        <div class="quick-nav-overlay"></div>
        <!-- END QUICK NAV -->
        <!--[if lt IE 9]>
<script src="../assets/global/plugins/respond.min.js"></script>
<script src="../assets/global/plugins/excanvas.min.js"></script> 
<script src="../assets/global/plugins/ie8.fix.min.js"></script> 
<![endif]-->
      @include('includes.footer')