
    @include('carrier.includes.header')
    @include('carrier.includes.head')
    <!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-content-white page-md">
        <div class="page-wrapper">
            <!-- BEGIN HEADER -->
                @include('carrier.includes.menuBar')

            <!-- END HEADER -->
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
            <!-- END HEADER & CONTENT DIVIDER -->
            <!-- BEGIN CONTAINER -->
            <div class="page-container">
                <!-- BEGIN SIDEBAR -->
                    @include('carrier.includes.sidebar')
                <!-- END SIDEBAR -->
                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <!-- BEGIN PAGE HEADER-->
                        <!-- BEGIN THEME PANEL -->
                       
                        <!-- END THEME PANEL -->
                        <!-- BEGIN PAGE BAR -->
                        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    <a href="index.html">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Dashboard</span>
                                </li>
                            </ul>
                            <div class="page-toolbar">
                                <div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
                                    <i class="icon-calendar"></i>&nbsp;
                                    <span class="thin uppercase hidden-xs"></span>&nbsp;
                                    <i class="fa fa-angle-down"></i>
                                </div>
                            </div>
                        </div>
                        <!-- END PAGE BAR -->
                        <!-- BEGIN PAGE TITLE-->
                        <h1 class="page-title">
                        </h1>
                        <!-- END PAGE TITLE-->
                        <!-- END PAGE HEADER-->
                        <div class="row">
                            <div class="col-lg-12  col-xs-12 col-sm-12">
                                      <div class="portlet box green">
                                                    <div class="portlet-title">
                                                        <div class="caption">
                                                          Attached Resources </div>
                                                        <div class="tools"> </div>
                                                    </div>
                                                    <div class="portlet-body table-responsive">
                                                        <table class="table table-striped table-bordered table-hover " id="sample_2">
                                                            <thead>
                                                                <tr>
<!--                                                                 <th align="center">Select</th>               
 -->                                                                <th align="center">Order ID</th>
                                                                    <th>Plate Number</th>
                                                                    <th align="center">Origin</th>
                                                                    <th align="center">Destination</th>
                                                                    <th align="center">Action</th>
                                                                    <!-- <th> OrderID </th>
                                                                    <th> Origin</th>
                                                                    <th> Destination </th>
                                                                    <th> Status</th>
                                                                    <th> Action </th> -->
                                                                </tr>
                                                            </thead>
                                                            <tbody>

                                                            @forelse($resources as $allRequest)
                                                                <tr>
                                                                    <td><h5>{{$allRequest->serviceIDNo}}</h5></td>
                                                                    <td><h5>{{$allRequest->plateNumber}}</h5></td>

                                                                    <td><h5>{{$allRequest->deliverTo}}</h5></td>
                                                                    <td><h5>{{$allRequest->deliverFrom}}</h5></td>
                                                                    
                                                                    
                                                                    <td><!-- <a href="" class="btn btn-primary btn-md">view</a> --> 
                                                                            <a href="{{url('/carrier/map/view/'.$allRequest->id)}}" class="btn btn-danger btn-md">Track</a>
                                                                    </td>
                                                                </tr>
                                                                @empty
                                                                No orders Exist Yet
                                                                @endforelse

                                                              <!-- <tr>
                                                                <td>1003</td>
                                                                <td>Tin Can A - Lagos</td>
                                                                <td>Owerri</td>
                                                                <td><span class="label label-sm label-success">Pending</span></td>
                                                                <td><button class="btn btn-primary">View</button> <button data-toggle="modal" href="#cancel" class="btn btn-danger">Cancel</button></td>
                                                              </tr> -->
                                                            </tbody>
                                                        </table>
                                                    </div>
                                      </div>

                            </div>
                            
                        </div>
                        
                    </div>
                    <!-- END CONTENT BODY -->
                </div>
                <!-- END CONTENT -->
                <!-- BEGIN QUICK SIDEBAR -->
                <a href="javascript:;" class="page-quick-sidebar-toggler">
                    <i class="icon-login"></i>
                </a>
               
                <!-- END QUICK SIDEBAR -->
            </div>
            <!-- END CONTAINER -->
            <!-- BEGIN FOOTER -->
           <div class="page-footer">
                <div class="page-footer-inner"> 2018 &copy; Truckka 
                      </div>
                <div class="scroll-to-top">
                    <i class="icon-arrow-up"></i>
                </div>
            </div>|&nbsp;
                    
                </div>
                <div class="scroll-to-top">
                    <i class="icon-arrow-up"></i>
                </div>
            </div>
            <!-- END FOOTER -->
        </div>
        <!-- BEGIN QUICK NAV -->
        
        <div class="quick-nav-overlay"></div>
 @include('carrier.includes.footer')
