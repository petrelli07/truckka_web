
<!DOCTYPE html>
<html>

<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
    <title>Truckka</title>
    <meta charset="utf-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />
    <meta name="format-detection" content="telephone=no">
        <link rel="canonical" href="index.html" />
    
    <link rel="shortcut icon" type="image/x-icon" href="">
    <link href='http://fonts.googleapis.com/css?family=Nunito' rel='stylesheet'>
    <link rel="stylesheet" href="{{URL::to('home_assets/assets/css-font/v5.1.0/css/all.css')}}" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    <link href="{{URL::to('home_assets/assets/css/bootstrap.min.css')}}" rel="stylesheet" />
    <link href="{{URL::to('home_assets/assets/css/datepicker.css')}}" rel="stylesheet" />
    <link href="{{URL::to('exchange/home_assets/assets/css/custom.css')}}" rel="stylesheet" />

    <link href="{{URL::to('home_assets/assets/css/media.css')}}" rel="stylesheet" />
    <!--THIS IS FOR TABLETS SIZES -->
    <link href="{{URL::to('home_assets/assets/css/ipad.css')}}" rel="stylesheet" />
    <link href="{{URL::to('home_assets/assets/css/galaxy-tab.css')}}" rel="stylesheet" />
    <!-- END OF TABLETS SIZES -->
    <link href="{{URL::to('home_assets/assets/css/bootstrap-multiselect.css')}}" rel="stylesheet" />
    <link rel="stylesheet" href="{{URL::to('home_assets/assets/ajax/libs/toastr.js/latest/css/toastr.min.css')}}" />
    <link href="{{URL::to('home_assets/assets/css/style.css')}}" rel="stylesheet" />
<!--     <script src="{{URL::to('dashboard_assets/assets/js/jquery.min.js')}}"></script>
 -->
    <link rel="stylesheet" href="{{URL::to('home_assets/assets/nav/css/normalize.min.css')}}">
    <link rel="stylesheet" href="{{URL::to('home_assets/assets/nav/css/defaults.min.css')}}">
    <link rel="stylesheet" href="{{URL::to('home_assets/assets/nav/css/nav-core.min.css')}}">
    <link rel="stylesheet" href="{{URL::to('home_assets/assets/nav/css/nav-layout.min.css')}}">
    <script src="{{URL::to('home_assets/assets/nav/js/rem.min.js')}}"></script>
    <script src="{{URL::to('exchange/home_assets/assets/js/spinner.js')}}"></script>
    <script>
        var APP_URL = {!! json_encode(url('/exchange/')) !!}
    </script>
    <script src="{{URL::to('exchange/js/transporter_submit.js')}}"></script>
    <script src="{{URL::to('exchange/phone_num_mask/dist/jquery-input-mask-phone-number.js')}}"></script>

    <script>
        $( function() {
            $( "#datepicker" ).datepicker({ c });
        } );
    </script>
    <script src="{{URL::to('dashboard_assets/assets/js/jquery.min.js')}}"></script>
</head>
<body>


<!-- THIS IS THE MOBILE VERSION -->
<div class="mobile-version-mv">
<header>
<div class="logo">
        <a href="{{url('/')}}"><img src="{{URL::to('dashboard_assets/assets/images/logo.png')}}" alt="Truckka"></a>

    </div>
</header>

<a href="#" class="nav-button"></a>

<nav class="nav">
    <ul>
        <li><a href="{{route('home.gethome')}}" >Home</a></li>
        <li ><a href="{{route('home.gethome')}}">The Exchange</a>
                                    
        <li ><a href="{{route('home.aboutdexchange')}}" > How the Exchange works</a></li>
        <li ><a href="{{route('home.benefit.exchange')}}" > Benefits of The Exchange</a></li>
                                    
                                    
        <li ><a href="{{route('home.get.our.services')}}" >Our Services</a>
        <li><a href="{{route('home.get.customer.seg')}}" >Our Ecosystem</a></li>
        <li ><a href="{{route('home.get.about.us')}}">About Us</a>
        <li><a href="{{route('home.contact.us')}}" >Contact Us</a></li>
                  
        <li><p style="margin-left:10px;"><span><img class="phone-icon-right" src="{{URL::to('home_assets/assets/images/phone-icon-11-256.png')}}" alt="Phone"></span>+234 8142 33 0001</p></li>
        <li><a href="{{url('/login')}}" id="IndexLogin"  class="right-info-link1 loginClr">LOGIN</a></li>
        <li><a href="#"  id="signup"  class="right-info-link1 loginClr">SIGN UP</a></li>

                                
    </ul>
</nav>

<a href="#" class="nav-close">Close Menu</a>

</div>

<!-- END OF THE MOBILE VERSION -->


    <div ng-app="truckQuote" >
        <header class="desktop-vs">
            <div class="container-fluid banner-header">
                <div class="row">
                
                    <div class="col-sm-2 col-xs-12 logo-section ">
                   
                        <div class="logo">
                        
                            <a href="{{route('home.gethome')}}"><img src="{{URL::to('dashboard_assets/assets/images/logo.png')}}" alt="Truckka"></a>
                        </div>
                    </div>

                     <div class="col-sm-5 col-xs-12 logo-section ">
                   
                     <nav class="nav nav-margin-left">
                            
                            <ul>

                            <li class="margin-left-li-new margintli"><a href="{{route('home.gethome')}}" class="biggerface">Home</a></li>
                            <li class="nav-submenu"><a href="{{route('home.gethome')}}" class="biggerface">The Exchange</a>
                            <ul>
                            <li ><a href="{{route('home.aboutdexchange')}}" > How the Exchange works</a></li>
                            <li ><a href="{{route('home.benefit.exchange')}}" > Benefits of The Exchange</a></li>
                            </ul>
                            
                            </li>
   
                                <li class="nav-submenu"><a href="{{route('home.get.our.services')}}"class="biggerface" >Our Services</a>
                                    <ul>
                                        <li><a href="{{route('home.get.customer.seg')}}">Our Ecosystem</a></li>
                                      
                                    </ul>
                                </li>

                                <li class="nav-submenu"><a href="{{route('home.get.about.us')}}"class="biggerface" >About Us</a>
                                    <ul>
                                        <li><a href="{{route('home.contact.us')}}" >Contact Us</a></li>
                                    </ul>
                                </li>
                                
                               
                               
                             
                            </ul>
                        </nav>
                                
               </div>
               <div class="col-sm-2 col-xs-12 logo-section logo-section2">
                        <nav class="nav nav-margin-left2">
                            
                            <ul>
                                 <li class="phone-arrows margin-left-phone lineheightchanged" >
                                <p class="pstyledLI"><img class="phone-icon-right" 
                                    src="{{URL::to('home_assets/assets/images/phone-icon-11-256.png')}}" alt="Phone" >
                                +234 903 000 2705</p>
                                <p class="pstyledLI1"><img class="phone-icon-right" src="{{URL::to('home_assets/assets/images/email.png')}}" width="25px">
                                </span>support@truckka.ng</p></li>
                            </ul>
                        </nav>            
                </div>

                 <div class="col-sm-2 col-xs-12 logo-section logo-section2">
                        <nav class="nav nav-margin-left3">
                            
                            <ul>
                            
                                  
                                <li style="float:right;margin-right:40px;"><a href="{{url('/login')}}" id="IndexLogin"  class="right-info-link1 loginClr">LOGIN</a></li>
                                <li style="float:right;margin-right:10px;"><a href="{{url('/exchange/customer/sign_up_customer_view')}}"  id="signup"  class="right-info-link2 signUpClr">SIGN UP</a></li>
                             
                            </ul>
                        </nav>            
                </div>


                    

                        
                
                </div>
                
            </div>
        </header> 

  
  <script>
            $(document).ready(function() {


                //$(".numeric").numeric();

                $('#i_can_give_truck').hide();
                $('#i_need_truck').show();
                $('#get_quote').hide();
                $('#hide_finish').hide();

                $('.i_need_truck').click(function() {

                     ///for banner///
                     document.getElementById("adjust-bg").classList.remove("banner-adjust");
                    document.getElementById("adjust-bg").classList.add("banner");
                    /// end of banner, beginning content //
                    document.getElementById("adjust").classList.remove("location-content-adjust");
                    document.getElementById("adjust").classList.add("location-content");

                    ///end ot it//
                    $('#i_need_truck').show();
                    $('#i_can_give_truck').hide();
                    $('#get_quote').hide();
                });

                $('#no_thanks').click(function() {
                    $('#i_need_truck').show();
                    $('#i_can_give_truck').hide();
                    $('#get_quote').hide();
                    $('#hide_finish').hide();
                });

                $('#finish').click(function() {
                    $('#hide_finish').show();
                    $('#buttons_continue').hide();
                });

                 $('.i_can_give_truck').click(function() {
                     ///for banner///
                    document.getElementById("adjust-bg").classList.remove("banner");
                    document.getElementById("adjust-bg").classList.add("banner-adjust");
                    /// end of banner, beginning content //
                    document.getElementById("adjust").classList.remove("location-content");
                    document.getElementById("adjust").classList.add("location-content-adjust");

                    ///end ot it//

                    $('#i_can_give_truck').show();
                    $('#i_need_truck').hide();
                    $('#get_quote').hide();
                });


                $('.btnGetQuotenew').click(function() {
                    $('#i_need_truck').hide();
                    $('#i_can_give_truck').hide();
                    $('#get_quote').show();
                });

                $('.btnGetQuotenew2').click(function() {
                    $('#i_need_truck').hide();
                    $('#i_can_give_truck').hide();
                    $('#get_quote').show();
                });
            });
        </script>

        <script>
            $(document).ready(function() {
                $('#standardOrigin').on('change', function() {

                    var val = this.value;
                    var _token = $("input[name='_token']").val();

                    document.getElementById('origin2').value = val;
                    var original = document.getElementById('origin2').value;

                    if (original === '') {
                        alert('Select a location');
                    }else if(original !== '')
                    {

                        loaderOpen();

                    $.ajax({

                        url: APP_URL+"/checkOrigins",

                        type:'POST',

                        data: {_token:_token, originToServer:original},

                        success: function(data) {

                            if($.isEmptyObject(data.error)){

                                var length = data.success;
                                var trueLength = length.length;

                                if(trueLength > 0){
                                    var catOptions = "";
                                    for(var i = 0; i < trueLength; i++) {

                                        var company = data.success[i];

                                        catOptions += "<option value="+ company +">" + company + "</option>";
                                    }

                                    document.getElementById("destinationSelect").innerHTML = catOptions;
                                    loaderClose();
                                }else{
                                    alert("nothing selected");
                                    loaderClose();
                                    $('#i_can_give_truck').hide();
                                    $('#i_need_truck').show();
                                    $('#get_quote').hide();
                                    $('#hide_finish').hide();
                                }



                            }else{

                                alert('error');
                                loaderClose()
                                $('#i_can_give_truck').hide();
                                $('#i_need_truck').show();
                                $('#get_quote').hide();
                                $('#hide_finish').hide();

                            }

                        }

                    });
                }
            })
            });
        </script>


        <script>

        </script>


        <script>
            $(document).ready(function() {

                $("#get_quote").css("display", "none");

                $(".getQuoteButton").unbind('click').click(function(e){

                    loaderOpen();

                    e.preventDefault();

                    $("#i_need_truck").css("display", "none");
                    $("#truckka-options").css("display", "none");

                    $("#get_quote").fadeIn("slow");

                    e.preventDefault();

                    var APP_URL = {!! json_encode(url('/exchange/')) !!}

                $.ajax({

                    async:false,

                    processData: false,

                    contentType: false,

                    url: APP_URL+"/getQuote",

                    type:'POST',

                    data: new FormData($("#getQuoteForm")[0]),

                    success: function(data) {

                        if($.isEmptyObject(data.error)){

                            var origin                  = $('#standardOrigin').find(":selected").text();
                            var destination             = $('#destinationSelect').find(":selected").text();
                            var truck_number            = $('#number_of_truck').val();
                            var description             = $('#description').val();
                            var pickupDate              = $('#datepicker').val();
                            var contactPhone            = $('#contact_phone').val();
                            var pickupAddress           = $('#pickup_address').val();

                             var amount = data.success.amount;
                             var amount_formatted   =   '&#8358;'+parseFloat(amount).toLocaleString('en');

                             var vat = data.success.vat;
                             var vat_formatted   =   '&#8358;'+parseFloat(vat).toLocaleString('en');

                             var total = data.success.total;
                             var total_formatted   =   '&#8358;'+parseFloat(total).toLocaleString('en');

                            $("#originSummary").html(origin);
                            $("#destinationSummary").html(destination);
                            $("#truckTypeSummary").html(data.success.truckTypeDescription);
                            $("#numberOfTruckSummary").html(truck_number);
                            $("#priceSummary").html(amount_formatted);
                            $("#vatSummary").html(vat_formatted);
                            $("#totalSummary").html(total_formatted);

                            $("#routeIDForm").val(data.success.route_id);
                            $("#truckTypeIDForm").val(data.success.truckTypeID);
                            $("#truckNumberForm").val(truck_number);/*
                            $("#itemDescriptionForm").val(description);
                            $("#pickUpDateForm").val(pickupDate);*/
                            $("#totalAmtForm").val(total);
                            loaderClose();
                        }else{

                            alert(data.error);
                            loaderClose()
                            $('#i_can_give_truck').hide();
                            $('#i_need_truck').show();
                            $('#get_quote').hide();
                            $('#hide_finish').hide();

                        }

                    }

                });

                loaderClose()
            });

            });
        </script>


        <div id="cover-spin"></div>

        <style>
        #cover-spin {
        position:fixed;
        width:100%;
        left:0;right:0;top:0;bottom:0;
        background-color: rgba(255,255,255,0.7);
        z-index:9999;
        display:none;
    }

@-webkit-keyframes spin {
	from {-webkit-transform:rotate(0deg);}
	to {-webkit-transform:rotate(360deg);}
}

@keyframes spin {
	from {transform:rotate(0deg);}
	to {transform:rotate(360deg);}
}

#cover-spin::after {
    content:'';
    display:block;
    position:absolute;
    left:48%;top:40%;
    width:40px;height:40px;
    border-style:solid;
    border-color:black;
    border-top-color:transparent;
    border-width: 4px;
    border-radius:50%;
    -webkit-animation: spin .8s linear infinite;
    animation: spin .8s linear infinite;
}

        </style>
        

@yield('content')	
	
@extends('home.layout.footer')
