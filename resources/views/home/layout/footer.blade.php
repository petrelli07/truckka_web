<!-- FOOTER -->
<footer>
            <div class="container-fluid" id="footer-mv">
                <div class="row footer-bottom">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-4 col-xs-12 col-sm-6 col-md-6">
                                <div class="module-heading">
                                    <h4 class="module-title">Truckka</h4>
                                </div>
                                <div class="module-body" id="aboutbottom">
                                    <ul class="list-unstyled">
                                        <li><a href="{{route('home.get.about.us')}}" title="About us">About Us</a></li>
                                        <!-- <li><a href="#" title="FAQ">FAQ</a></li> -->
                                        <li><a href="{{route('home.contact.us')}}" title="Contact Us">Contact Us</a></li>
                                        
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg-4 col-xs-12 col-sm-6 col-md-6">
                                <div class="module-heading">
                                    <h4 class="module-title">Contact Us</h4>
                                </div>
                                <div class="module-body">
                                    <ul class="toggle-footer">
                                        <li class="media">
                                            <div class="pull-left"> <span class="icon fa-stack fa-lg"><img src="{{URL::to('home_assets/assets/images/location.png')}}" width="22px"></span> </div>
                                            <div class="media-body">
                                                <p class="m-t-10">6 Prince Bode Oluwo Street Mende Maryland Lagos</p>
                                            </div>
                                        </li>
                                        <li class="media">
                                            <div class="pull-left"> <span class="icon fa-stack fa-lg"> <img src="{{URL::to('home_assets/assets/images/email.png')}}" width="25px"> </span> </div>
                                            <div class="media-body"> <span><a href="mailto:support@truckka.ng" class="m-t-10">support@truckka.ng</a></span> </div>
                                        </li>
                                        <li class="media">
                                            <div class="pull-left"> <span class="icon fa-stack fa-lg"> <img src="{{URL::to('home_assets/assets/images/phone-icon-11-256.png')}}" width="20px"></span> </div>
                                            <div class="media-body">
                                                <p class="m-t-10">+234 8142 330 001</p>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg-4 col-sm-12 col-md-12 col-xs-12 p-relative">
                                <!--<div class="col-xs-12 no-padding social">
                                    <ul class="link">
                                        <li class="fb">
                                            <a target="_blank" rel="nofollow" href="#" title="Facebook">
                                                <img src="{{URL::to('home_assets/assets/images/fb.png')}}" class="img-display img-responsive">
                                                <img src="{{URL::to('home_assets/assets/images/fb2.png')}}" class="img-display1 img-responsive">
                                            </a>
                                        </li>
                                        <li class="tw">
                                            <a target="_blank" rel="nofollow" href="#" title="Twitter">
                                                <img src="{{URL::to('home_assets/assets/images/tw.png')}}" class="img-display img-responsive">
                                                <img src="{{URL::to('home_assets/assets/images/tw1.png')}}" class="img-display1 img-responsive">
                                            </a>
                                        </li>
                                       
                                    </ul>
                                </div>-->
                                <div class="module-heading col-xs-12">
                                    <p class="t-align"><a href="#" target="_blank" title="Terms & Conditions">2018 © Truckka. Terms & Conditions</a></p>
                                    <p class="t-align"><a href="#" target="_blank" title="Privacy Policy">Privacy Policy</a></p>
                                    <p class="t-align">All Rights Reserved</p>
                                </div>
                                <div class="col-xs-12">
                                    <div class="footer-logo">
                                        <p>
                                            <span>
                                                <img src="{{URL::to('dashboard_assets/assets/images/logo-transparent.png')}}"  alt="Truckka" />
                                            </span>
                                          
                                        </p>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
<!-- END FOOTER -->

<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
(function(){ var widget_id = 'JyQN2VdpPL';var d=document;var w=window;function l(){var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true;s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();
</script>
<!-- {/literal} END JIVOSITE CODE -->
<script>
    $("#datetime").datetimepicker({
        format: 'yyyy-mm-dd hh:ii'
    });
</script>


    

<script src="{{URL::to('home_assets/assets/ajax/libs/jquery/1.12.4/jquery.min.js')}}"></script>
    <!-- <script src="{{URL::to('home_assets/assets/ajax/libs/angularjs/1.5.6/angular.min.js')}}"></script>
    <script src="{{URL::to('home_assets/assets/ajax/libs/angular.js/1.4.0/angular-route.min.js')}}"></script>
    <script src="{{URL::to('home_assets/assets/ajax/libs/toastr.js/latest/toastr.min.js')}}"></script>
     -->
   <!--  <script src="{{URL::to('home_assets/assets/js/ui-bootstrap-tpls-0.12.1.js')}}"></script>
    <script src="{{URL::to('home_assets/assets/js/rangeweightslider.js')}}"></script>
    <script src="{{URL::to('home_assets/assets/js/bootstrap.js')}}"></script>
 -->
     <script src="{{URL::to('home_assets/assets/js/bootstrap-datepicker.js')}}"></script>
    
 <script src="{{URL::to('home_assets/assets/js/User/UserUIe209.js?v=1.0.0')}}"></script>
    <script src="{{URL::to('home_assets/assets/js/Common/SharedViewModele209.js?v=1.0.0')}}"></script>
    <script src="{{URL::to('home_assets/assets/js/User/UserViewModele209.js?v=1.0.0')}}"></script>
    <script src="{{URL::to('home_assets/assets/js/Common/Angular.Sharede209.js?v=1.0.0')}}"></script>
    <script src="{{URL::to('home_assets/assets/js/User/Angular.UserViewModele209.js?v=1.0.0')}}"></script>
   
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script> -->
<!--     <script src="{{URL::to('home_assets/assets/nav/js/nav.jquery.min.js')}}"></script>
 -->    <script>
        $('.nav').nav();
    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDz45dPojJCLeQZtPizla8CX2s4rOvoky0&callback=initMap&libraries=geometry,places">
    </script>
    <script type="text/javascript">

        function initMap () {
          $(document).ready(function  (argument) {
            
            let pickupInput          = document.getElementById('pickup_address');
            let deliveryInput        = document.getElementById('delivery_address');
            let resultFields         = ['formatted_address','place_id'];
            // let restriction          = google.maps.places.ComponentRestrictions;
            let restriction             =  {country:'NG'};
            
            let autoCompletePickup    = new google.maps.places.Autocomplete(pickupInput);
            let autoCompleteDelivery  = new google.maps.places.Autocomplete(deliveryInput);


            autoCompletePickup.setFields(resultFields);
            autoCompleteDelivery.setFields(resultFields);

            autoCompletePickup.setComponentRestrictions(restriction);
            autoCompleteDelivery.setComponentRestrictions(restriction);


            google.maps.event.addListener(autoCompletePickup, 'place_changed', function() {
                
                var place = autoCompletePickup.getPlace();
                $('#pickup_address').val(place.formatted_address);
                $('#pickup_placeId').val(place.place_id);

            });

            google.maps.event.addListener(autoCompleteDelivery, 'place_changed', function() {
              
                var place = autoCompleteDelivery.getPlace();
                $('#deliveryInput').val(place.formatted_address);
                $('#delivery_placeId').val(place.place_id);

            });

          })

        }
       
     
    </script>

