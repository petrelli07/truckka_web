<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
							<html xmlns="http://www.w3.org/1999/xhtml">
							  <head>
								<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
								<meta name="viewport" content="width=device-width, initial-scale=1.0">
                                <title>Invoice</title>
                                <link href="{{URL::to('home_assets/assets/css/invoice.css')}}" rel="stylesheet" />
							
							  </head>
							  <body style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;margin:0;padding:0;width:100% !important; background-color:#f6f4f5 !important;">
								<div class="block">
								  <!-- Start of preheader -->
								  <table width="100%" bgcolor="#f6f4f5" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="preheader" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;margin:0;padding:0;width:100% !important;line-height:100% !important">
									<tbody>
									  <tr>
										<td width="100%" style="border-collapse:collapse">
										  <table width="580" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt">
											<tbody>
											  <!-- Spacing -->
											  <tr>
												<td width="100%" height="5" style="border-collapse:collapse"></td>
											  </tr>
											  <!-- Spacing -->
											  <!-- Spacing -->
											  <tr>
												<td width="100%" height="5" style="border-collapse:collapse"></td>
											  </tr>
											  <!-- Spacing -->
											</tbody>
										  </table>
										</td>
									  </tr>
									</tbody>
								  </table>
								  <!-- End of preheader -->
								</div>
								<div class="block">
								  <!-- start of header -->
								  <table width="100%" bgcolor="#f6f4f5" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="header" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;margin:0;padding:0;width:100% !important;line-height:100% !important">
									<tbody>
									  <tr>
										<td style="border-collapse:collapse">
										  <table width="700"  cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth bg" hlitebg="edit" shadow="edit" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt">
											<tbody>
											  <tr>
												<td style="border-collapse:collapse">
												  <!-- logo -->
												  <table width="280" cellpadding="0" cellspacing="0" border="0" align="left" class="devicewidth" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt">
													<tbody>
													  <tr>
														<td valign="middle" width="270" style="border-collapse:collapse;padding: 10px 0 10px 20px;" class="logo">
														  <div class="imgpop">
															<a href="{{url('/')}}"><img src="{{URL::to('home_assets/assets/images/truckka-orange.png')}}" alt="Truckka.ng" border="0" style="width:150px;outline:none;text-decoration:none;border:none;-ms-interpolation-mode:bicubic;border:none;display:block; border:none; outline:none; text-decoration:none;" st-image="edit" class="logo"></a>
														  </div>
														</td>
													  </tr>
													</tbody>
												  </table>
												  <!-- End of logo -->
												  <!-- menu -->
												  <table width="280" cellpadding="0" cellspacing="0" border="0" align="right" class="devicewidth" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt">
													<tbody>
													  <tr>
														<td width="270" valign="middle" style="border-collapse:collapse;font-family: Helvetica, Arial, sans-serif;font-size: 14px; color: #ffffff;line-height: 24px; padding: 10px 0;padding-top:40px;" align="right" class="menu" st-content="menu">
														  <a href="{{url('/')}}"   target="_blank" style="text-decoration: none; color: #ffffff;">Home</a>

                                                            &nbsp;|&nbsp;
														
																   <a href="{{url('/our-services')}}"  target="_blank"  style="text-decoration: none; color: #ffffff;">Our Services</a>
														  &nbsp;|&nbsp;
																   <a href="{{url('/about-us')}}"  target="_blank" style="text-decoration: none; color: #ffffff;">About Us</a>
														</td>
														<td width="20" style="border-collapse:collapse"></td>
													  </tr>
													</tbody>
												  </table>
												  <!-- End of Menu -->
												</td>
											  </tr>
											</tbody>
										  </table>
										</td>
									  </tr>
									</tbody>
								  </table>
								  <!-- end of header -->
								</div>
								<div  >
								  <!-- image + text -->
                                  <div style="display:flex;justify-content:center;overflow-x:auto;">
                                  <div style="width:700px;background-color:#fff;padding:10px;">

                                 
                                        <table class="neworder">

                                        <thead class="theadorder">
                                        <tr class="tr_order">
                                        <th class="th_order" colspan="6">INVOICE INFORMATION</th>
                                       
                                        </tr>
                                        <tr class="tr_order">
                                        <td class="td_order" colspan="6">
                                            <strong>Invoice No: </strong>#123456789<br> 
                                            <strong>Date: </strong> 14 January 2025<br> 
                                       
                                        </td>
                                       
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr class="tr_order">
                                        <th class="th_order">Description</th>
                                        <th class="th_order">Origin</th>
                                        <th class="th_order">Destination</th>
                                        <th class="th_order">Number of truck</th>
                                        <th class="th_order">Unit Price</th>
                                        <th class="th_order">Amount</th>
                                        </tr>
                                        <tr class="tr_order">
                                        <td class="td_order">Goods,on hhm hms </td>
                                        <td class="td_order">Lagos</td>
                                        <td class="td_order">Abuja</td>
                                        <td class="td_order">5</td>
                                        <td class="td_order">200</td>
                                        <td class="td_order">300,000</td>
                                        </tr>
                                        <tr class="tr_order">
                                        <td class="td_order">Goods,on hhm hms </td>
                                        <td class="td_order">Lagos</td>
                                        <td class="td_order">Abuja</td>
                                        <td class="td_order">5</td>
                                        <td class="td_order">200</td>
                                        <td class="td_order">300,000</td>
                                        </tr>
                                        </tbody>
                                        <tfoot>
                                        <tr class="tr_order">
                                        <th class="th_order" colspan="5">Total</th>
                                        <td class="td_order"> 110.00</td>
                                        </tr>
                                        <tr class="tr_order">
                                        <th class="th_order" colspan="4">Charges</th>
                                        <td class="td_order"> 8% </td>
                                        <td class="td_order">8.80</td>
                                        </tr>
                                        <tr class="tr_order">
                                        <th class="th_order" colspan="5">Net Total Amount</th>
                                        <td class="td_order">₦118.80</td>
                                        </tr>

                                        </tfoot>
                                        </table>

                                          <table class="neworder">

                                        <tr class="tr_order">
                                        <th class="th_order" colspan="6">MAKE PAYMENT TO:</th>
                                        
                                        </tr>
                                        <tr class="tr_order">
                                        <td class="td_order" colspan="2">
                                            <strong>Bank Name</strong><br> 
                                            <p>Uba bank</p>
                                        </td>

                                         <td class="td_order" colspan="2">
                                            <strong>ACCOUNT NAME</strong><br> 
                                            <p>james paul</p>
                                        </td>
                                        <td  class="td_order" colspan="2">
                                            <strong>ACCOUNT NUMBER:</strong><br>
                                           <p>202028678 </p>
                                        </td>
                                        </tr>

                                        
                                          
                                          </table>

                                                  <table width="540" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidthinner" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt">
													<tbody>
														
													  <tr>
														<td width="100%" height="10" style="border-collapse:collapse"></td>
													  </tr>
													  <!-- button -->
													  <!-- /button -->
													  <!-- Spacing -->
													  <tr>
														<td width="100%" height="20" style="border-collapse:collapse;text-align:center;"><a href="http://www.truckka.ng" style="text-decoration: none; color: #0db9ea;font-family:arial;">Truccka.ng</a></td>
													  </tr>
													  <!-- Spacing -->

                                                       <tr>
														<td width="100%" height="10" style="border-collapse:collapse"></td>
													  </tr>
													</tbody>
												  </table>
												</td>
											  </tr>
											</tbody>
										  </table>
										</td>
									  </tr>
									</tbody>
								  </table>
								  
								</div>
                                </div>
                                <br /><br /><br />
							  </body>
							</html>