@extends('home.layout.content')
@section('content')

<div class="container-fluid">
    <div id="adjust-bg" class="row banner">
        <div class="location-center-align">
            <div class="location-center">
                <div class="location-title width-100 text-center">
                    <h1 class="text-center font-22 text-center animate-h1">Welcome to Truckka. Your Reliable freight Exchange</span></h1>
                </div>

                        <div class="location-search" >
                           <div  id="adjust" class="location-content white white-lbl">
                    
                                        <div class="freight-exc">
                                            <ul class="report-radio-buttons tax-radio-btn">
                                                <li class="">
                                                    <input  id="radio-1" class="radio-custom equipments " name="equipmentType" type="radio"  checked tabindex="3">

                                                    <label for="radio-1" class="radio-custom-label lbl-chng-txt-change i_need_truck"><span class="lbl-chng-span">I NEED TRUCK</span></label>

                                                </li>
                                                <li class="m-l-30">
                                                    <input  id="radio-2" class="radio-custom equipments"  name="equipmentType" type="radio" tabindex="4">
                                                    <label for="radio-2" class="radio-custom-label lbl-chng-txt-change i_can_give_truck"><span class="lbl-chng-span">I NEED FREIGHT</span></label>
                                                </li>
                                            </ul>
                                        </div>


                                        <!--Hidden for origin-destination form-->
                                        <form id="originForm" style="display:none;">
                                            {{csrf_field()}}
                                            <input type="hidden" id="origin2" name="originToServer">
                                        </form>
                                        <!--end Hidden for origin-destination form-->

                    <!---- THIS IS FOR I NEED TRUCK ------>
                            <div class="for-i-need-truck">
                            <div id="i_need_truck">
                            <form id="getQuoteForm" @if (Route::has('login')) @auth action="{{url('/exchange/customer/client-area/submit_new_order')}}"@else action="{{url('/exchange/submitQuote')}}"  @endauth @endif method="post">
                                    {{csrf_field()}}

                            <div class="col-xs-12 no-padding">
                            <div class="col-sm-6 col-xs-12 no-padding">
                                <div class="col-xs-12">
                                    <div class="col-xs-12 no-padding">
                                        <div class="col-xs-12 no-padding">
                                            <div class="booking-label-change mtp-lbl">
                                                <label>Origin</label>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 no-padding">
                                            <select class="text-booking-change" name="origin" required id="standardOrigin">
                                                <option class="text-booking-change">[-Select Origin-]</option>
                                                @for ($i=0; $i < count($uniqueOrigins); $i++)
                                                <option class="text-booking-change" value="{{$uniqueOrigins[$i]}}">{{$uniqueOrigins[$i]}}</option>
                                                @endfor
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12 no-padding">
                                <div class="col-xs-12">
                                    <div class="col-xs-12 no-padding">
                                        <div class="col-xs-12 no-padding">
                                            <div class="booking-labe-changel">
                                                <label>Destination</label>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 no-padding">
                                            <select class="text-booking-change" required id="destinationSelect" name="destination">
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>



                            <div class="col-sm-6 col-xs-12 no-padding">
                                <div class="col-xs-12">
                                    <div class="col-xs-12 no-padding">
                                        <div class="col-xs-12 no-padding">
                                            <div class="booking-label-change mtp-lbl">
                                                <label>Pickup Address</label>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-lg-12 no-padding">
                                            <input class="text-booking-change-textarea" id="pickup_address"  name="pickup_address" placeholder="Enter Pickup Address"  type="text" required/>
                                            <input type="" hidden="hidden" name="pickup_placeId" id="pickup_placeId">
                                            <span class="field-validation-valid c-red" data-valmsg-for="?"  data-valmsg-replace="true"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12 no-padding">
                                <div class="col-xs-12">
                                    <div class="col-xs-12 no-padding">
                                        <div class="col-xs-12 no-padding">
                                            <div class="booking-labe-changel">
                                                <label>Destination Address</label>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-lg-12 no-padding">
                                            <input type="text" class="text-booking-change-textarea"  name="delivery_address" placeholder="Enter Destination Address"  type="text" id="delivery_address" required>
                                             <input type="" hidden="hidden" name="delivery_placeId" id="delivery_placeId">
                                            <span class="field-validation-valid c-red" data-valmsg-for="?"  data-valmsg-replace="true"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <!--listing 2 -->
                        <div class="col-xs-12 no-padding">
                        <div class="col-sm-6 col-xs-12 no-padding">
                                <div class="col-xs-12">
                                    <div class="col-xs-12 no-padding">
                                        <div class="col-xs-12 no-padding">
                                            <div class="booking-labe-changel">
                                                <label>Choose Truck</label>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 no-padding">
                                            <select class="text-booking-change" name="truckType" required>
                                                <option class="text-booking-change" required value="">[-Select Truck Type-]</option>
                                                @foreach($truckTypes as $truckType)
                                                <option class="text-booking-change" value="{{$truckType->id}}">{{$truckType->truck_type_description}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6 col-xs-12 no-padding">
                                <div class="col-xs-12">
                                    <div class="space-above">
                                        
                                        <div class="col-xs-6-init no-padding" style="margin-right:5px;">
                                        <select class="text-booking-change" name="truckCapacity" required>
                                                <option class="text-booking-change" required value="">[-Select capacity-]</option>
                                                <option class="text-booking-change" value="30 Tonnes">30 Tonnes</option>
                                                <option class="text-booking-change" value="45 Tonnes">45 Tonnes</option>
                                                <option class="text-booking-change" value="60 Tonnes">60 Tonnes</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div >
                                        <!-- <div class="col-xs-8 no-padding">
                                            <div class="booking-labe-changel">
                                                <label>Number of Trucks</label>
                                            </div>
                                        </div> -->
                                        <div class="col-xs-6-init no-padding">
                                            <input max="99" class="text-booking-change1 numeric"   placeholder="Number of Trucks Needed"  type="number" min="1" id="number_of_truck" name="number_of_truck" required>
                                        </div>
                                    </div>
                                </div>
                            </div>



                             <div class="col-sm-6 col-xs-12 no-padding">
                                <div class="col-xs-12">
                                    <div class="col-xs-12 no-padding">
                                        <div class="col-xs-12 no-padding">
                                            <div class="booking-labe-changel">
                                                <label>Pickup date</label>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 no-padding">
                                        <input class="text-booking-change1 numeric" placeholder="Pickup Date"  type="text" name="pickup_date" id="datepicker">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6 col-xs-12 no-padding">
                                <div class="col-xs-12">
                                    <div class="col-xs-12 no-padding">
                                        <div class="col-xs-12 no-padding">
                                            <div class="booking-labe-changel">
                                                <label>Description of Cargo</label>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-lg-12 no-padding">
                                            <textarea class="text-booking-change-textarea"  name="cargo_description" placeholder="Enter Description of Cargo"  type="text" required></textarea>
                                            <span class="field-validation-valid c-red" data-valmsg-for="?"  data-valmsg-replace="true"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!--end it-->
                       
                        <!-- <div class="col-xs-12 no-padding">
                        <div class="col-sm-12 col-xs-12 no-padding">
                                <div class="col-xs-12">
                                    <div class="col-xs-12 no-padding">
                                        <div class="col-xs-12 no-padding">
                                            <div class="booking-label-change mtp-lbl">
                                                <label>Description of items</label>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-lg-12 no-padding">
                                            <textarea class="text-booking-change-textarea"  name="origin" placeholder="Enter Description"  type="text"></textarea>
                                            <span class="field-validation-valid c-red" data-valmsg-for="origin"  data-valmsg-replace="true"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>-->
                        <div class="col-xs-12 no-padding">
                            
                            
                            <div class="col-xs-12">
                                <br />
                            <div class="about-btn " style="width:50%;">
                                    <!--<button  class="cursor-pointer btnGetQuotenew" tabindex="8">Get Quote</button>-->
                                    <!--<button class="cursor-pointer" tabindex="8">Get Quote</button>-->
                                    <button class="cursor-pointer" type="submit" tabindex="8">Get Quote</button>

                            </div>

                                
                            </div>
                        </div>
                    </div>
                                </form>
           
                                </div>
                    
 <!------------------------------------------------------------- END OF IT --------------------------------------------->


 <!---- -------------------------------------------THIS IS FOR I CAN GIVE TRUCKS ----------------------------------->
                      <form id="carrier_form">

                          {{csrf_field()}}
                       <div class="this-for-i-can-give-trucks">
                            <div id="i_can_give_truck">
                            <div class="col-xs-12 no-padding">

                                <div class="alert alert-danger print-error-msg" style="display:none">

                                    <ul></ul>
                                </div>

                                <div class="alert alert-success print-success-msg" style="display:none">

                                    <ul></ul>
                                </div>

                            <div class="col-sm-6 col-xs-12 no-padding">

                                <div class="col-xs-12">
                                        <div class="col-xs-12 no-padding">
                                            <div class="col-xs-12 no-padding">
                                                <div class="booking-labe-changel">
                                                    <label>Transporter Company Name</label>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 no-padding">
                                                
                                                <input type="text" class="text-booking-change" name="transporter_name" placeholder="Enter Your Company Name" required/>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <!-- <div class="col-xs-12">
                                        <div class="col-xs-12 no-padding">

                                        <form method="post" action="{{url('/exchange/register_supplier_capacity')}}">
                                            {{csrf_field()}}
                                            <div class="col-xs-12 no-padding">
                                                <div class="booking-label-change mtp-lbl">
                                                    <label>Type of Truck</label>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 no-padding">
                                                <select class="text-booking-change" name="truckType" required>
                                                <option class="text-booking-change" value="">[-Select Truck Type-]</option>
                                                    @foreach($truckTypes as $type)
                                                    <option class="text-booking-change" value="{{$type->id}}">{{$type->truck_type_description}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div> -->
                                </div>
                                <div class="col-sm-6 col-xs-12 no-padding">
                                    <div class="col-xs-12">
                                        <div class="col-xs-12 no-padding">
                                            <div class="col-xs-12 no-padding">
                                                <div class="booking-labe-changel">
                                                    <label>Location Covered</label>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 no-padding">

                                               <ul class="report-radio-buttons tax-radio-btn">

                                               <div style="margin-bottom:10px;">
                                                   @foreach($area as $areas)
                                                <li class="">
                                                    <input type="checkbox" value="{{$areas->id}}" name="coverageArea[]" class="text-booking-change2 checkbox-custom-exchange"id="defaultInline1" >
                                                    <span class="lbl-chng-span22">{{$areas->coverage_area_description}}</span>
                                                </li>
                                                   @endforeach

                                                </div>

                                                </ul>

                                                    


                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--listing 2 -->
                             <div class="col-xs-12 no-padding">
                            <div class="col-sm-6 col-xs-12 no-padding">
                                    <div class="col-xs-12">
                                        <div class="col-xs-12 no-padding">
                                            <div class="col-xs-12 no-padding">
                                                <div class="booking-labe-changel">
                                                    <label>Email Address</label>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 no-padding">
                                                
                                                <input type="email" class="text-booking-change" name="email_address" placeholder="Enter Email Address" required/>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-sm-6 col-xs-12 no-padding">
                                <div class="col-xs-12">
                                        <div class="col-xs-12 no-padding">
                                            <div class="col-xs-12 no-padding">
                                                <div class="booking-labe-changel">
                                                    <label>Phone Number</label>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 no-padding">
                                                
                                                <input type="text" class="text-booking-change" name="phone_number" placeholder="Enter Phone Number" required/>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end it-->

                            <div class="col-xs-12 no-padding">
                                
                                
                                <div class="col-xs-12" >
                                <br />
                                    @if (Route::has('login'))
                                    @auth
                                    @else
                                    <div class="about-btn" style="width:50%;margin-top:30px;">
                                        <button type="submit" class="cursor-pointer carrier_form_sumbit" tabindex="8">Get Freight</button>
                                    </div>
                                    @endauth
                                    @endif

                                    
                                </div>
                            </div>
                                </form>
                            </div>

                            </div>


<!-------------------------------------- END OF IT ---------------------------------------------------->




<!---- -------------------------------------------THIS IS FOR GET QUOTE SUMMERY ----------------------------------->

                    <div class="for-quote-summary">

                    <div id="get_quote">
                            <div class="col-xs-12 no-padding">
>
                                            <form action="{{url('/exchange/submitQuote')}}" method="POST">
                                                @if (Route::has('login'))
                                                @auth
                                                @else
                                                <div class="col-xs-12 no-padding">
                                                        <div class="about-btn" style="width:50%;">
                                                        <h3 style="color:#fff;">Register or Log in to view Quote</h3>
                                                        </div>
                                                </div>
                                                @endauth
                                                @endif

                                                <div class="col-xs-12 no-padding">

                                                    <div class="col-xs-12">
                                                        {{csrf_field()}}
                                                        <input id="routeIDForm" type="hidden" name="routeID">
                                                        <input id="truckNumberForm" type="hidden" name="truckNumberForm">
                                                        <input id="truckTypeIDForm" type="hidden" name="truckTypeIDForm">
                                                        <input id="totalAmtForm" type="hidden" name="totalAmtForm">

                                                        <div class="col-xs-12 no-padding">
                                                            <div class="col-xs-12">
                                                                <div class="quote-btn col-xs-12 no-padding">
                                                                    @if (Route::has('login'))
                                                                    @auth
                                                                    <div class="col-xs-12">
                                                                        <div class="about-btn" style="width:50%;">
                                                                            <button   class="cursor-pointer" tabindex="8" value="loggedInUser" name="submitQuoteButton">Finish Order</button>
                                                                        </div>
                                                                    </div>
                                                                    @else
                                                                    <div class="col-xs-12">
                                                                        <div class="about-btn" style="width:50%;">
                                                                            <button   class="cursor-pointer" tabindex="8" value="register_customer" name="submitQuoteButton">Register</button>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-xs-12">
                                                                        <div class="about-btn" style="width:50%;">
                                                                            <button   class="cursor-pointer" tabindex="8" value="login" name="submitQuoteButton">Login</button>
                                                                        </div>
                                                                    </div>
                                                                    @endauth
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>

                                            </form>

                                            </div>
                                        </div>
                            </div>

                    </div>


<!-------------------------------------- END OF IT ---------------------------------------------------->

        
            </div>
            </div>
            </div>
        </div>
    </div>
</div>

<!--Benefits of the exchange-->
<div class="container-fluid" id="benefits">
    <div class="row about-al">
        <div class="container">
            <div class="row">
                <div class="work-title">
                    <h2>
                    THE TRUCKKA ADVANTAGE
                    </h2>
                </div>
                <div class="col-xs-12 no-padding work-block-al">
                    
                    

                    <div class="col-sm-9 col-md-6 col-lg-6">
                        <div class="work-block-horizontal2">
                        <div class="work-imm">
                            <div class="image-freight">

                                <img src="{{URL::to('home_assets/assets/images/map-with-truck-ful.png')}}"  >           
                               
                            </div>
                            <div class ="content-freight">
                                <p class="p-horizon">
                                <b>TRANSPARENT PRICING</b></p>
                                <p class="p-horizon">

                                Our fee structure brings both freight and truck owners
                                directly together and removes all layers of middle men to ensure reduced freight charges.</p>
                        </div>
                        </div>
                    </div>
                    </div>

                

                    <div class="col-sm-9 col-md-6 col-lg-6">
                        <div class="work-block-horizontal">
                        <div class="work-imm">
                            <div class="image-freight">
                                <img src="{{URL::to('home_assets/assets/images/delivery-truck_with_freight2.png')}}">     
                            </div>      
                            <div class ="content-freight">
                                        <p class="p-horizon">


                                        <b>CONSISTENT FREIGHT / TRUCK MATCHING </b></p>
                                        <p class="p-horizon">
                                            Logistical redundancy like travelling empty or time wastage due to difficulty in finding trucks is eliminated as you can now find trucks via the exchange on a continuous basis.
                                        </p>

                            </div>
                            </div>
                               
                        </div>
                    </div>

                    <div class="col-sm-9 col-md-6 col-lg-6">
                        <div class="work-block-horizontal2">
                        <div class="work-imm">
                            <div class="image-freight">
                                <img src="{{URL::to('home_assets/assets/images/lap-phone-white.png')}}"> 
                            </div>          
                            <div class ="content-freight">
                                        <p class="p-horizon">
                                        <b>EASY TO USE TECHNOLOGY </b> </p>
                                        <p class="p-horizon">

                                            Our web and mobile applications make it fast and easy to sign up, request and pay for trucks or search for freight online. All material movement is also trackable in real-time (for your peace of mind).


                                    </p>
                            </div>
                            </div>
                               
                        </div>
                    </div>

                     <div class="col-sm-9 col-md-6 col-lg-6">
                     <div class="work-block-horizontal">
                            <div class="work-imm">
                            <div class="image-freight">
                                <img src="{{URL::to('home_assets/assets/images/customer_support_hover.png')}}" alt="No Hiden fee">
                            </div>
                            <div class ="content-freight">
                                <p class="p-horizon">
                                <b>EFFICIENT CUSTOMER SERVICE</b>
                                </p>
                                <p class="p-horizon">
                                    Our polite and knowledgeable support team is equipped to attend to your needs in a timely manner either via phone, email or any of our social media channels.

                                </p>
                            </div>
                            </div>
                               
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end of Benefits of exchange-->




<!--Our Services-->

<div class="container-fluid margin-for-deter-service" id="our-services-mv">
<div class="container">
            <div class="row">
    
                <div class="work-title">
                    <h2>
                        OUR SERVICES
                    </h2>
                </div>

				<div class="section group">
					<div class="col span_1_of_3 col-determinate-service">
					<div class="board-images">
                        <img src="{{URL::to('home_assets/assets/images/contract logistics.jpg')}}">
                    </div>
                    <div class="content-d-service">
                        <h3>CONTRACT LOGISTICS</h3>
                        <p>We provide freight movement services for Full Truck Load (FTL) to enterprises on an outsourced contractual basis. </p>
                    </div>
                    </div>

                    
					<div class="col span_1_of_3 col-determinate-service">
					<div class="board-images">
                        <img src="{{URL::to('home_assets/assets/images/roadrunner-truck-angle_0.jpg')}}">
                    </div>
                    <div class="content-d-service">

                        <h3>FREIGHT EXCHANGE</h3>
                        <p>An easy-to-use digital mobile and web enabled freight-truck matching platform. </p>
                    </div>
                    </div>
                    

					<div class="col span_1_of_3 col-determinate-service">
					<div class="board-images">
                        <img src="{{URL::to('home_assets/assets/images/tms icon.jpg')}}">
                    </div>

                     <div class="content-d-service">
                        <h3>TRANSPORTATION MANAGEMENT SYSTEM (TMS)</h3>

                        <p>We offer a bespoke TMS service which delivers visibility, control and seamless communication across your enterprise.   </p>

                    </div>

					</div>
				</div>
              
                    
</div>
</div>
</div> 
<!--end services-->





<!--About Truckka-->
<div class="container-fluid" >
    <div class="row about-al p-relative">
        <div class="benifits-overlay"></div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ben-pad">
            <div class="benefits-title col-xs-12 no-padding">
                <h2>
                    About Truckka
                </h2>
            </div>
            <div class="benefits-info col-xs-12 no-padding">
                
                <div class="col-sm-12 col-xs-12 pad-left">
                    <div class="benefits-info1">
                        
                        <p>
                            TRUCKKA Logistics Limited is a fourth-party logistics (4PL) tech-solutions company. We leverage our resources and capabilities with those of our partners to provide logistic services and tech-enabled solutions within the logistics supply chain.
                        </p>
                    </div>
                </div>


                <div class="col-sm-12 col-xs-12 pad-left">
                    <div class="benefits-info1 margin-top-40">
                        
                        <p>
                            We are a team of professionals with a rich and diverse corporate executive background focused on creating an efficient marketplace for the logistics industry in Nigeria.
                            </p>
                    </div>
                </div>


                <div class="col-sm-12 col-xs-12 pad-left">
                    <div class="benefits-info1 margin-top-40">
                        <p>
                            Our aim is to be your go-to logistics solutions provider within a difficult and infrastructure-challenged environment where players yearn for safety, price-efficiency, trust and professionalism - our core value proposition.
                        </p>
                    </div>
                </div>
            </div>
            
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding">
            <div class="benifits-bg-im-22">
           
            </div>
        </div>
    </div>
</div>
<!--End of it-->


<!--Mission and vision-->
<div class="container-fluid" id="about">
    <div class="row about-al">
        <div class="container">
            <div class="row">
                <div class="about-title">
                    <h2>
                        OUR MISSION
                    </h2>
                    <p class="about-p">
                        To build an efficient marketplace that seamlessly matches freight to truck services on demand and to provide enterprise logistics services within our chosen segments.
                    </p>
                </div>
                <div class="about-btn">
                   
                </div>
            </div>
        </div>
    </div>
</div>
<!--end of mission and vision-->




<!--Mission and vision-->
<div class="container-fluid" id="about">
    <div class="row about-al">
        <div class="container">
            <div class="row">
                <div class="about-title">
                    <h2>
                	OUR ECOSYSTEM
                    </h2>
                    
                    <img src="{{URL::to('home_assets/assets/images/truckka-segment.png')}}" style="width:100%;">
                    
                </div>
                
            </div>
        </div>
    </div>
</div>
<!--end of mission and vision-->



<!--How it works-->
<div class="container-fluid" id="howitworks">
    <div class="row about-al p-relative">
        <div class="benifits-overlay2"></div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ben-pad">
            <div class="benefits-title2 col-xs-12 no-padding">
                <h2>
                	OUR PROMISE
                </h2>
            </div>
            <div class="benefits-info col-xs-12 no-padding">
                
                <div class="col-sm-10 col-xs-12 pad-left">
                    <div class="benefits-info1">
                        <h3> <img src="{{URL::to('home_assets/assets/images/good-mark.png')}}" style="width: 30px;"> 
                    	We will match your freight to truck or refund your money within 72 hours</h3>
                       
                    </div>
                </div>
            </div>
            <div class="benefits-info col-xs-12 no-padding">
               
                <div class="col-sm-10 col-xs-12 pad-left">
                    <div class="benefits-info1">
                        <h3><img src="{{URL::to('home_assets/assets/images/good-mark.png')}}" style="width: 30px;">
                            We will act professionally and courteously at all times</h3>
                        
                    </div>
                </div>
            </div>
            <div class="benefits-info col-xs-12 no-padding">
                
                <div class="col-sm-10 col-xs-12 pad-left">
                    <div class="benefits-info1">

                        <h3><img src="{{URL::to('home_assets/assets/images/good-mark.png')}}" style="width: 30px;">
                            We will provide support service while your freight is in transit</h3>

                        
                    </div>
                </div>
            </div>


             <div class="benefits-info col-xs-12 no-padding">
                
                <div class="col-sm-10 col-xs-12 pad-left">
                    <div class="benefits-info1">

                        <h3><img src="{{URL::to('home_assets/assets/images/good-mark.png')}}" style="width: 30px;">
                            We will provide a goods in transit insurance coverage for your goods </h3>

                        
                    </div>
                </div>
            </div>
            
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding">
            
        </div>
    </div>
</div>
<!--How it works-->


<!--Board members-->
@include('home.board-members')
<!--end of Board members--> 





<!--Mission and vision-->
<div class="container-fluid" id="about">
    <div class="row about-al-edit">
        <div class="container">
            <div class="row">
                <div class="about-title">
                    <h2>
                	All Freight is insured by
                    </h2>
                    
                    
                           
                            <p><img src="{{URL::to('home_assets/assets/client-logo/NSIA.png')}}"  > </p>
    
                    
                </div>
                
            </div>
        </div>
    </div>
</div>
<!--end of mission and vision-->



<div class="container-fluid" id="benefits">
    <div class="row about-al">
        <div class="container">
            <div class="row">
                <div class="work-title-client">
                    <h2>
                    <u>OUR CLIENTS</u>
                    </h2>
                </div>
               <div class="col-sm-9 col-md-6 col-lg-12">
                        <div class="our-client">
                       
                            <div class="image-client">

                                <img src="{{URL::to('home_assets/assets/client-logo/olem-grains.png')}}"  > 
                                <img src="{{URL::to('home_assets/assets/client-logo/crown-banner2.png')}}"  > 
                                <img src="{{URL::to('home_assets/assets/client-logo/wacot-limited.png')}}"  >               
                                <img src="{{URL::to('home_assets/assets/client-logo/olem-pasta.png')}}"  > 
                            </div>
                
                    </div>
                    </div>

                

                    <!-- <div class="col-sm-9 col-md-6 col-lg-3">
                        <div class="our-client2">    
                        <span><b>All Freight is insured by; </b></span>   
                            <img src="{{URL::to('home_assets/assets/client-logo/nsia.png')}}"  > 
                           
                        </div>
                       
                    </div> -->

           
            </div>
        </div>
    </div>
</div>





@endsection
