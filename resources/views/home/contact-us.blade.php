@extends('home.layout.content')
@section('content')



<div class="container-fluid">
    <div class="row p-relative">
        <div class="contact-map">
            <iframe src="https://maps.google.com/maps?q=6%20Prince%20Bode%20Oluwo%20Street%20Mende%20Maryland%20Lagos%20&t=&z=13&ie=UTF8&iwloc=&output=embed" width="600" height="450" frameborder="0" class="b-0" allowfullscreen=""></iframe>
        </div>
        <div class="container contact-al">
            <div class="row">
                <div class="col-xs-12 no-padding ht-ch">
                    <div class="col-sm-8 col-xs-12 book-ht pad-left">

                        <div class="col-xs-12">
                            <div class="contact-title">
                                <h2>
                                    Get In Touch<span>(*Required Field)</span>
                                </h2>
                            </div>
                        </div>
                        <div class="col-sm-6 col-xs-12 pad-left">
                            <div class="col-xs-12 contact-block margin-top">
                                <label>Your Name*</label>
                                <div class="frm-tx2">
                                    <input type="text" placeholder="Enter Your Name" id="contactName" class="text-contact">
                                    <span class="field-validation-valid c-red" data-valmsg-for="UserName" data-valmsg-replace="true"></span>
                                </div>
                            </div>
                            <div class="col-xs-12 contact-block">
                                <label>Email*</label>
                                <div class="frm-tx2">
                                    <input type="text" placeholder="Enter Your Email" id="contactEmail" class="text-contact">
                                    <span class="field-validation-valid c-red" data-valmsg-for="UserName" data-valmsg-replace="true"></span>
                                </div>
                            </div>
                            <div class="col-xs-12 contact-block">
                                <label>Phone</label>
                                <div class="frm-tx2">
                                    <input type="text" id="PhoneNumber" placeholder="Enter Your Phone Number" class="text-contact">
                                    <span class="field-validation-valid c-red" data-valmsg-for="UserName" data-valmsg-replace="true"></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <div class="col-xs-12 contact-block">
                                <label>Message</label>
                                <div class="frm-tx2">
                                    <textarea class="text-contact ht-resize" id="Message"></textarea>
                                    <span class="field-validation-valid c-red" data-valmsg-for="UserName" data-valmsg-replace="true"></span>
                                </div>
                            </div>
                            <div class="col-xs-12 contact-block">
                                <div class="profile-btn">
                                    <a href="javascript:void(0);" data-dismiss="modal" id="btnContact">Submit</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-xs-12">
                        <div class="col-xs-12">
                            <div class="contact-title">
                                <h2>
                                    Contact Us
                                </h2>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <ul class="toggle-footer">
                                <li class="media">
                                    <div class="pull-left"> <span class="icon fa-stack1 fa-lg"><img src="{{URL::to('home_assets/assets/images/location.png')}}" style="width:20px;"></span> </div>
                                    <div class="media-body1">
                                        <p class="m-t-10">6 Prince Bode Oluwo Street Mende Maryland Lagos </p>
                                    </div>
                                </li>
                                <li class="media">
                                    <div class="pull-left"> <span class="icon fa-stack1 fa-lg"> <img src="{{URL::to('home_assets/assets/images/email.png')}}" style="width:20px;"> </span> </div>
                                    <div class="media-body1"> <span><a href="mailto:support@truckka.ng" class="m-t-10">support@truckka.ng</a></span> </div>
                                </li>
                                <li class="media">
                                    <div class="pull-left"> <span class="icon fa-stack1 fa-lg"> <img src="{{URL::to('home_assets/assets/images/phone-icon-11-256.png')}}" style="width:20px;"></span> </div>
                                    <div class="media-body1">
                                        <p class="m-t-10">+234 8142 330 001</p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection