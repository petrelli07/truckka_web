
    @include('client.includes.header')
    @include('client.includes.head')
    <!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-content-white page-md">
        <div class="page-wrapper">
            <!-- BEGIN HEADER -->
                @include('client.includes.menuBar')

            <!-- END HEADER -->
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
            <!-- END HEADER & CONTENT DIVIDER -->
            <!-- BEGIN CONTAINER -->
            <div class="page-container">
                <!-- BEGIN SIDEBAR -->
                    @include('client.includes.sidebar')
                <!-- END SIDEBAR -->
                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <!-- BEGIN PAGE HEADER-->
                        <!-- BEGIN THEME PANEL -->
                       
                        <!-- END THEME PANEL -->
                        <!-- BEGIN PAGE BAR -->
                        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    <a href="index.html">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Dashboard</span>
                                </li>
                            </ul>
                            <div class="page-toolbar">
                                <div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
                                    <i class="icon-calendar"></i>&nbsp;
                                    <span class="thin uppercase hidden-xs"></span>&nbsp;
                                    <i class="fa fa-angle-down"></i>
                                </div>
                            </div>
                        </div>
                        <!-- END PAGE BAR -->
                        <!-- BEGIN PAGE TITLE-->
                        <h1 class="page-title">
                        </h1>
                        <!-- END PAGE TITLE-->
                        <!-- END PAGE HEADER-->
                        <div class="row">
                            <div class="col-lg-12  col-xs-12 col-sm-12">
                                      <div class="portlet box green">
                                                    <div class="portlet-title">
                                                        <div class="caption">
                                                          My Orders </div>
                                                        <div class="tools"> </div>
                                                    </div>
                                                    <div class="portlet-body table-responsive">
                                                        <table class="table table-striped table-bordered table-hover " id="sample_2">
                                                            <thead>
                                                                <tr>
                                                                <th align="center">Order ID</th>
                                                                    <th align="center">Destination</th>
                                                                    <th align="center">Origin</th>
                                                                    <th align="center">Status</th>
                                                                    <th align="center">Validity</th>
                                                                    <th align="center">Action</th>
                                                                    <!-- <th> OrderID </th>
                                                                    <th> Origin</th>
                                                                    <th> Destination </th>
                                                                    <th> Status</th>
                                                                    <th> Action </th> -->
                                                                </tr>
                                                            </thead>
                                                            <tbody>

                                                            @forelse($allRequests as $allRequest)
                                                                <tr>
                                                                    <td><h5>{{$allRequest->serviceIDNo}}</h5></td>
                                                                    <td><h5>{{$allRequest->deliverTo}}</h5></td>
                                                                    <td><h5>{{$allRequest->deliverFrom}}</h5></td>
                                                                    <td>
                                                                        <h5>
                                                                            @if($allRequest->orderStatus == -1)
                                                                            Cancelled
                                                                            @elseif($allRequest->orderStatus == 0)
                                                                            Pending(Under Review)
                                                                            @elseif($allRequest->orderStatus == 1)
                                                                            Approved
                                                                            @elseif($allRequest->orderStatus == 2)
                                                                            Processing
                                                                            @elseif($allRequest->orderStatus==3)
                                                                            Invoice Created
                                                                            @elseif($allRequest->orderStatus==4)
                                                                            Processing
                                                                            @elseif($allRequest->orderStatus == 5)
                                                                            Upload Order Documentation(Origin)
                                                                            @elseif($allRequest->orderStatus == 6)
                                                                            Upload Order Documentation(Destination)
                                                                            @elseif($allRequest->orderStatus == 7)
                                                                            Transaction Cleared for Closure
                                                                            @elseif($allRequest->orderStatus == 8)
                                                                            Dispute Raised 
                                                                            @elseif($allRequest->orderStatus == 9)
                                                                            Approved
                                                                            @elseif($allRequest->orderStatus == 10)
                                                                            Approved
                                                                            @endif
                                                                        </h5>
                                                                    </td>
                                                                    <td>
                                                                        <h5>
                                                                            {{ date("l, j F,Y",strtotime($allRequest->expires_at)) }}
                                                                        </h5>
                                                                    </td>
                                                                    <td>
                                                                        <h5>
                                                                            @if($allRequest->orderStatus == 0)
                                                                            <a class="btn btn-primary btn-md edit" href='{{ url("/viewOrderDetailsClient/{$allRequest->serviceIDNo}") }}' title="View Order Details">
                                                                                Order Details   
                                                                            </a>|
                                                                            <a class="btn btn-danger btn-md edit" href='{{ url("/cancelOrder/{$allRequest->serviceIDNo}") }}' title="Cancel Order">
                                                                                Cancel Order 
                                                                            </a>
                                                                            @elseif($allRequest->orderStatus == 1)
                                                                            <a class="btn btn-primary btn-md edit" href='{{ url("/viewOrderDetailsClient/{$allRequest->serviceIDNo}") }}' title="View Order Details"> Order Details   
                                                                            </a>
                                                                            @elseif($allRequest->orderStatus == 2)
                                                                            <a class="btn btn-primary btn-md edit" href='{{ url("/viewOrderDetailsClient/{$allRequest->serviceIDNo}") }}' title="View Order Details">
                                                                                 Order Details   
                                                                            </a>
                                                                            @elseif($allRequest->orderStatus == 3)
                                                                            <a class="btn btn-primary btn-md edit" href='{{ url("/viewOrderDetailsClient/{$allRequest->serviceIDNo}") }}' title="View Order Details">
                                                                                 Order Details   
                                                                            </a>|
                                                                            <a class="btn btn-primary btn-md edit" href='{{ url("/viewInvoice/{$allRequest->serviceIDNo}") }}' title="View Invoice Details">
                                                                            Invoice Details 
                                                                            </a>|
                                                                            <a class="btn btn-primary btn-md edit" href='{{ url("/approveInvoice/{$allRequest->serviceIDNo}") }}' title="View Invoice Details">
                                                                            Approve Invoice<i class="fa fa-check-circle" aria-hidden="true"></i>
                                                                            </a>|
                                                                            <a href='{{url("/cancelOrder/$allRequest->serviceIDNo")}}' class="btn btn-danger btn-sm">Cancel</a>
                                                                            @elseif($allRequest->orderStatus == 4)
                                                                            <a class="btn btn-primary btn-md edit" href='{{ url("/viewOrderDetails/{$allRequest->serviceIDNo}") }}' title="View Order Details">Order Details   
                                                                            </a>
                                                                            @elseif($allRequest->orderStatus == 5)
                                                                            <a class="btn btn-primary btn-md edit" href='{{ url("/viewOrderDetailsClient/{$allRequest->serviceIDNo}") }}' title="View Order Details">
                                                                                Order Details   
                                                                            </a>|
                                                                            <a class="btn btn-primary btn-md edit" href='{{ url("/viewOrderResources/{$allRequest->serviceIDNo}") }}' title="Upload Order Documentation">
                                                                                Upload Docs(Origin) 
                                                                            </a>|
                                                                            <a class="btn btn-primary btn-md edit" href='{{ url("/viewInvoice/{$allRequest->serviceIDNo}") }}' title="View Invoice Details">
                                                                              Invoice Details 
                                                                            </a>
                                                                            @elseIf($allRequest->orderStatus == 6)
                                                                            <a class="btn btn-primary btn-md edit" href='{{ url("/viewOrderDetailsClient/{$allRequest->serviceIDNo}") }}' title="View Order Details">
                                                                                Order Details   
                                                                            </a>|
                                                                            <a class="btn btn-primary btn-md edit" href='{{ url("/viewOrderResourcesDest/{$allRequest->serviceIDNo}") }}' title="Upload Order Documentation">
                                                                                Upload Docs(Delivery) 
                                                                            </a>
                                                                            
                                                                            @elseif($allRequest->orderStatus == 7)
                                                                            <a class="btn btn-primary btn-md edit" href='{{ url("/viewOrderDetailsClient/{$allRequest->serviceIDNo}") }}' title="View Order Details">
                                                                                 Order Details   
                                                                            </a>|
                                                                            <a class="btn btn-primary btn-md edit" href='{{ url("/viewInvoice/{$allRequest->serviceIDNo}") }}' title="View Invoice Details">
                                                                              Invoice Details 
                                                                            </a>|
                                                                            <a class="btn btn-primary btn-md edit" href='{{ url("/finishTransaction/{$allRequest->serviceIDNo}") }}' title="Finish Transaction">Close Transaction 
                                                                                <i class="fa fa-chevron-circle-right" aria-hidden="true"></i>
                                                                            </a>| 
                                                                            <a class="btn btn-danger edit" href='{{ url("/openDispute/{$allRequest->serviceIDNo}") }}' title="Open Dispute">Open Dispute
                                                                                <i class="fa fa-times" aria-hidden="true"></i>
                                                                            </a>
                                                                            @elseif($allRequest->orderStatus == 8)
                                                                            <a class="btn btn-primary btn-md edit" href='{{ url("/viewOrderDetailsClient/{$allRequest->serviceIDNo}") }}' title="View Order Details">
                                                                                 Order Details   
                                                                            </a>|
                                                                            <a class="btn btn-primary btn-md edit" href='{{ url("/viewInvoice/{$allRequest->serviceIDNo}") }}' title="View Invoice Details">
                                                                              Invoice Details
                                                                            </a>
                                                                            @elseif($allRequest->orderStatus == 9)
                                                                            <a class="btn btn-primary btn-md edit" href='{{ url("/viewOrderDetailsClient/{$allRequest->serviceIDNo}") }}' title="View Order Details"> Order Details </a>
                                                                            @elseif($allRequest->orderStatus == 10)
                                                                            <a class="btn btn-primary btn-md edit" href='{{ url("/viewOrderDetailsClient/{$allRequest->serviceIDNo}") }}' title="View Order Details">
                                                                                 Order Details   
                                                                            </a>
                                                                            @elseif($allRequest->orderStatus == -1)
                                                                            Cancelled
                                                                            
                                                                            @endif
                                                                        </h5>
                                                                    </td>
                                                                </tr>
                                                                @empty
                                                                No orders Exist Yet
                                                                @endforelse
                                                                {{$allRequests->links()}}

                                                              <!-- <tr>
                                                                <td>1003</td>
                                                                <td>Tin Can A - Lagos</td>
                                                                <td>Owerri</td>
                                                                <td><span class="label label-sm label-success">Pending</span></td>
                                                                <td><button class="btn btn-primary">View</button> <button data-toggle="modal" href="#cancel" class="btn btn-danger">Cancel</button></td>
                                                              </tr> -->
                                                            </tbody>
                                                        </table>
                                                    </div>
                                      </div>

                            </div>
                            
                        </div>
                        
                    </div>
                    <!-- END CONTENT BODY -->
                </div>
                <!-- END CONTENT -->
                <!-- BEGIN QUICK SIDEBAR -->
                <a href="javascript:;" class="page-quick-sidebar-toggler">
                    <i class="icon-login"></i>
                </a>
               
                <!-- END QUICK SIDEBAR -->
            </div>
            <!-- END CONTAINER -->
            <!-- BEGIN FOOTER -->
           <div class="page-footer">
                <div class="page-footer-inner"> 2018 &copy; Truckka 
                      </div>
                <div class="scroll-to-top">
                    <i class="icon-arrow-up"></i>
                </div>
            </div>|&nbsp;
                    
                </div>
                <div class="scroll-to-top">
                    <i class="icon-arrow-up"></i>
                </div>
            </div>
            <!-- END FOOTER -->
        </div>
        <!-- BEGIN QUICK NAV -->
        
        <div class="quick-nav-overlay"></div>
 @include('client.includes.footer')
