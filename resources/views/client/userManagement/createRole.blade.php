
    @include('client.includes.header')
    @include('client.includes.head')
    <!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-content-white page-md">
     <!--  <div class="modal fade bs-modal-sm alert-modal flex-container flex-center flex-vertical-align flex-responsive " id="cancel" tabindex="-1" role="dialog" aria-hidden="true" style="display:inherit;background:rgba(25, 24, 24, 0.48);opacity:1;"><div class="col44 loader-container"><img src="{{url('/assets/pages/img/loader.gif')}}"></div></div> -->
     
    </div>
        <div class="page-wrapper">
            <!-- BEGIN HEADER -->
                @include('client.includes.menuBar')

            <!-- END HEADER -->
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
            <!-- END HEADER & CONTENT DIVIDER -->
            <!-- BEGIN CONTAINER -->
            <div class="page-container">
                <!-- BEGIN SIDEBAR -->
                    @include('client.includes.sidebar')
                <!-- END SIDEBAR -->
                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <!-- BEGIN PAGE HEADER-->
                        <!-- BEGIN THEME PANEL -->
                       
                        <!-- END THEME PANEL -->
                        <!-- BEGIN PAGE BAR -->
                        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    <a href="index.html">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Dashboard</span>
                                </li>
                            </ul>
                            <div class="page-toolbar">
                                <div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
                                    <i class="icon-calendar"></i>&nbsp;
                                    <span class="thin uppercase hidden-xs"></span>&nbsp;
                                    <i class="fa fa-angle-down"></i>
                                </div>
                            </div>
                        </div>
                        <!-- END PAGE BAR -->
                        <!-- BEGIN PAGE TITLE-->
                        <h1 class="page-title">
                        </h1>
                        <!-- END PAGE TITLE-->
                        <!-- END PAGE HEADER-->
<!--  -->                        <div class="row">
                            <div class="col-lg-10 col-lg-offset-1 col-xs-12 col-sm-12">
                                <div class="portlet light ">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <span class="caption-subject font-dark  uppercase">Create New Role</span>                                         
                                      </div>
                                        
                                    </div>
                                     <div class="portlet-body form">
                                        <form action="@if(isset($roleName)){{url('/updateRole')}}@else{{url('/saveNewRole')}}@endif" ajax-submit="true" class="ajax-form-submit"> 
                                            <div class="form-group form-md-line-input has-info" style="margin-bottom: 10px;">
                                                      {{csrf_field()}}
                                                      @if(isset($roleId))
                                                        <input type="" name="id" value="{{$roleId}}" hidden="hidden">
                                                      @endif
                                                      <div class="form-group form-md-line-input">
                                                        <input type="text" class="form-control  validate" autocomplete="off" placeholder="Enter Name Here" maxlength="20" name="name" value="@if(isset($roleName)){{$roleName}}@endif">
                                                        <label for="form_control_1">Role Name</label>
                                                        <span class="help-block">Enter A Descriptive Name</span>
                                                      </div>
                                            </div>
                                                                                                                                          
                                            <table class="table table-hover">
                                              <thead>
                                                <tr>
                                                  <th>Name</th>
                                                  <th>Description</th>
                                                  <th>Switch</th>
                                                </tr>
                                              </thead>
                                              <tbody class="tableBody">
                                               @foreach($companyModules as $module)
                                                  <tr>
                                                    <td>{{$module->moduleName}}</td>
                                                    <td>{{$module->moduleDescription}}</td>
                                                    <td>
                                                      @if(isset($permissionModules))
                                                     

                                                       <label class="switch switcha " style="margin: 0px;width: 60px;height: 33px;">
                                                          <input autocomplete="off" value="{{$module->id}}" name="modules[]" class="module-switch" type="checkbox"
                                                           @if(in_array($module->id, $permissionModules))
                                                                      checked="checked"
                                                            @endif
                                                       >
                                                          <span class="slider slidera round" style="margin:0px;"></span>
                                                        </label>
                                                       @else
                                                         <label class="switch switcha " style="margin: 0px;width: 60px;height: 33px;">
                                                          <input  autocomplete="off" value="{{$module->id}}" name="modules[]" class="module-switch" type="checkbox"
                                                          >
                                                          <span class="slider slidera round" style="margin:0px;"></span>
                                                        </label>
                                                      @endif
                                                    </td>
                                                  </tr>
                                               @endforeach
                                              </tbody>
                                            </table>
                                                          
                                                        <!--end profile-settings-->
                                                        <div class="margin-top-10">
                                                            <button type="submit" class="btn btn-primary"> save  </button>
<!--                                                             <a href="javascript:;" class="btn default"> Cancel </a>
 -->                                                        </div>
                                                    </form>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        
                    </div>
                    <!-- END CONTENT BODY -->
                </div>
                <!-- END CONTENT -->
                <!-- BEGIN QUICK SIDEBAR -->
              
                <!-- END QUICK SIDEBAR -->
            </div>
            <!-- END CONTAINER -->
            <!-- BEGIN FOOTER -->
            <div class="page-footer">
                <div class="page-footer-inner"> 2018 &copy; Truckka  &nbsp;|&nbsp;
                   
                </div>
                <div class="scroll-to-top">
                    <i class="icon-arrow-up"></i>
                </div>
            </div>
            <!-- END FOOTER -->
        </div>
        <!-- BEGIN QUICK NAV -->
        
        <div class="quick-nav-overlay"></div>
 @include('client.includes.footer')
