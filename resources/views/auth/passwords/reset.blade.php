@extends('home.layout.content')
@section('content')

<div class="container-fluid">
    <div class="row forget-bg" style="
    background-color: #e6e4e4;">
        <div class="forget-overlay"></div>
        <div class="container">
            <div class="row">
                <div class="forget-content ">
                    <!--if @ session -->
                    @if (session('message'))
                    <div class="alert alert-success" role="alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        {{ session('message') }}
                    </div>
                    @endif
                    <!--endif-->
                    <form method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}
                        <div class="col-xs-12 forget-content-al">
                            <div class="forget-title col-xs-12 no-padding">
                                <h2>
                                    Reset Password
                                </h2>
                            </div>
                            <div class="col-xs-12 no-padding">
                                <label class="forget-label">Email</label>
                                <input type="email" name="email" class="text-forget" id="txtForgotEmail" placeholder="Email" value="{{ $email or old('email') }}" required/>
                                <span class="field-validation-valid c-red" data-valmsg-for="UserName" id="forgotEmailError" >
                                    @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </span>
                            </div>

                            <div class="col-xs-12 no-padding">
                                <label class="forget-label">Password</label>
                                <input type="password" class="text-forget" id="txtForgotEmail" name="password" placeholder="Password" required>
                                <span class="field-validation-valid c-red" data-valmsg-for="Password"  >
                                    @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </span>
                            </div>

                            <div class="col-xs-12 no-padding">
                                <label class="forget-label">Confirm Password</label>
                                <input type="password" class="text-forget" id="txtForgotEmail" name="password_confirmation"  placeholder="Confirm Password" required>
                                <span class="field-validation-valid c-red" data-valmsg-for="Password"  >
                                    @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                    @endif
                                </span>
                            </div>

                            @if (session('status'))
                            <span class="help-block">
                                <strong>{{ session('status') }}</strong>
                            </span>
                            @endif

                            <div class="col-xs-12">
                                <div class="row">
                                    <div class="forget-btn" style="margin-top: 25px;">
                                        <button type="submit" id="btnForgotSend" class="btn btn-success btn-block">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>



@endsection