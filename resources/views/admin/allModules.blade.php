<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>TMSA</title>
    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css" href="{{ url('css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ url('css/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('css/indexupdate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('css/tmsa.css') }}">
        <link href="{{url('assets/global/css/style.css')}}" rel="stylesheet" id="style_components" type="text/css" />
    <link href="{{ url('https://fonts.googleapis.com/css?family=Titillium+Web|Concert+One|Josefin+Sans|Roboto') }}" rel="stylesheet">
    <script type="text/javascript" src="{{ url('js/admin_dashboard.js')}}"></script>
    <script type="text/javascript" src="{{ url('js/jquery-3.2.1.js')}}"></script>
    <script type="text/javascript" src="{{ url('js/bootstrap.js')}}"></script>
    <script type="text/javascript" src="{{ url('js/newUser.js')}}"></script>
    <script type="text/javascript" src="{{ url('js/newCarrier.js')}}"></script>
    <script type="text/javascript" src="{{ url('js/newClient.js')}}"></script>

</head>


<body class="col-md-12-col-sm-6 col-xs-12" style="padding:0; height: auto">
<div class="row" style="height:100vh; margin-right:0; margin-left:0; background-color: #232323;">
   

<!-- TSMA VIEW ORDERS PAGE -->
<div class="tmsa_main_body col-md-12" style="height:100vh;">
    <div class="row" style="height:100vh;">
      <div class="col-md-9 view-order" style="height:auto;">
        <form action="{{url('/deployModule')}}" method="POST">
        <?php $counter = -1;?>
          @foreach ($classes as $class => $methods)
            {{csrf_field()}}
            <input type="" hidden="hidden" name="userType" value="{{$userType}}">
            <h4 align="center" class="btn-lg" style=" font-family: 'Josefin Sans', sans-serif; margin-left: 5%; margin-right: 5%; background: #ffffff; border: 2px solid #3baba0;">{{$class}}</h4>
            <div class="table-responsive" style="height:100%;">
                
                <table id="mytable" class="table table-bordred table-striped" style=" font-family: 'Josefin Sans'; font-size: 80%;">

                    <thead >
                        <th align="center">Module Method</th>
                        <th align="center">Module Name</th>
                        <th align="center">Module Description</th>                        
                        <th align="center">Status</th>
                        <th align="center">Deploy</th>
                    </thead>
                    <tbody>
                    @foreach($methods as $classmethods)
                    <?php $counter++?>
                    <tr class="row-content">
                        <td>
                            {{$classmethods['name']}}
                        </td>
                        <td>
                            <input type="text" name="details[{{$counter}}][name]">
                        </td>
                        <td>
                            <input type="text" name="details[{{$counter}}][desc]">
                        </td>
                        <td>
                          @if (in_array($classmethods['name'], array_column((array)$deployedModules, 'moduleMethod'))) 
                            <span style="background: green;color: white;padding:5px;">Deployed</span>
                          @else
                            <span style="background: red;color: white;padding:5px;">Not Deployed</span>

                          @endif
                        </td>
                        <td>
                             @if (!in_array($classmethods['name'], array_column((array)$deployedModules, 'moduleMethod'))) 
                              <label>
                                <input type="checkbox" value="{{$classmethods['name']}}" name="details[{{$counter}}][method]">
                              </label>
                             @endif
                           <!--  <label class="switch" style="width: 56px;height: 25px;">
                              <input type="checkbox">
                              <span class="slider custom-slider"></span>
                            </label> -->
                        </td>
                        
                    </tr>
                   
                    @endforeach
                    </tbody>

                </table>
            </div>
            @endforeach
            @if(isset($companies))
            <select multiple="multiple" name="deployTo[]">
                @foreach($companies as $company)
                    <option value="{{$company->id}}">{{$company->companyName}}</option>
                @endforeach
            </select>
            @endif
            <br>
            <button type="submit" class="btn btn-primary">Deploy</button>
          </form>
        </div>
    </div>

    <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                    <h4 class="modal-title custom_align" id="Heading">Edit Your Detail</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <input class="form-control " type="text" placeholder="Mohsin">
                    </div>
                    <div class="form-group">

                        <input class="form-control " type="text" placeholder="Irshad">
                    </div>
                    <div class="form-group">
                        <textarea rows="2" class="form-control" placeholder="CB 106/107 Street # 11 Wah Cantt Islamabad Pakistan"></textarea>


                    </div>
                </div>
                <div class="modal-footer ">
                    <button type="button" class="btn btn-warning btn-lg" style="width: 100%;"><span class="glyphicon glyphicon-ok-sign"></span> Update</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>



    <div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                    <h4 class="modal-title custom_align" id="Heading">Delete this entry</h4>
                </div>
                <div class="modal-body">

                    <div class="alert alert-danger"><span class="glyphicon glyphicon-warning-sign"></span> Are you sure you want to delete this Record?</div>

                </div>
                <div class="modal-footer ">
                    <button type="button" class="btn btn-success" ><span class="glyphicon glyphicon-ok-sign"></span> Yes</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> No</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

</div>
<!--     ************************************************** -->
<!--BOTTOM NAV BAR-->


</body>

</html>