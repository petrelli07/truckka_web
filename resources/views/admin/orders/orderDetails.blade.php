<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>TMSA</title>
    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css" href="{{ url('css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ url('css/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('css/indexupdate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('css/tmsa.css') }}">
    <link href="{{ url('https://fonts.googleapis.com/css?family=Titillium+Web|Concert+One|Josefin+Sans|Roboto') }}" rel="stylesheet">
    <script type="text/javascript" src="{{ url('js/admin_dashboard.js')}}"></script>
    <script type="text/javascript" src="{{ url('js/jquery-3.2.1.js')}}"></script>
    <script type="text/javascript" src="{{ url('js/bootstrap.js')}}"></script>
    <script type="text/javascript" src="{{ url('js/searchResources.js')}}"></script>
    <script type="text/javascript" src="{{ url('js/preventNegative.js')}}"></script>

</head>
<body class="col-md-12-col-sm-6 col-xs-12" style="padding:0; height:auto">


<!--TMSA-->
<div class="tmsa" style="height:100%;">
    <div class="row" style="height:100vh; margin-right:0; margin-left:0;">
        <div class="side_bar col-md-3">
            <div class=" identifier hidden-xs hidden-sm" >
                <span class="navbar-brand type" >T</span>
                <span class="navbar-brand type">M</span>
                <span class="navbar-brand type">S</span>
                <span class="navbar-brand type">A</span>
            </div>
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed pull-left" data-toggle="collapse" data-target="#hidden_bar" aria-expanded="false" style="border: 2px solid #fff ; margin-left:10px;">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar" style="border:1px solid #fff"></span>
                    <span class="icon-bar" style="border:1px solid #fff"></span>
                    <span class="icon-bar" style="border:1px solid #fff"></span>
                </button>
                <a class="navbar-brand" href="#">
                    <div class="brand">
                        <span class="navbar-brand  logoT" style="border: 1px solid #ffffff; margin-left: 2px; border-radius: 10px 0 0 0; font-family: 'Concert One', cursive; color: #ffffff" >T</span>
                        <span class="navbar-brand  logoR" style="border: 1px solid #ffffff;  margin-left: 2px; font-family: 'Concert One', cursive; background: #ffffff; color: #3baba0;">R</span>
                        <span class="navbar-brand   logoU" style="border: 1px solid #ffffff;  margin-left: 2px; font-family: 'Concert One', cursive; color: #ffffff;">U</span>
                        <span class="navbar-brand   logoC" style="border: 1px solid #ffffff;  margin-left: 2px;font-family: 'Concert One', cursive; background: #ffffff; color: #3baba0;">C</span>
                        <span class="navbar-brand  logoK" style="border: 1px solid #ffffff;  margin-left: 2px;font-family: 'Concert One', cursive; color: #ffffff"; >K</span>
                        <span class="navbar-brand   logoK" style="border: 1px solid #ffffff;  margin-left: 2px;font-family: 'Concert One', cursive; background: #ffffff; color: #3baba0;">K</span>
                        <span class="navbar-brand   logoA" style="border: 1px solid #ffffff;  margin-left: 2px; border-radius: 0 0 10px 0;font-family: 'Concert One', cursive; color: #ffffff;">A</span>
                    </div>
                </a>
            </div>
            <!--NAVBAR FOR MOBIE SCREENS-->
            <ul class="nav navbar-nav collapse" id="hidden_bar" style="margin-top:50px;">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage User Groups<span class="glyphicon glyphicon-user pull-right"></span></a>
                    <ul class="dropdown-menu">

                            <li><a href="{{url('/home')}}">Create Users<span class="glyphicon glyphicon-plus-sign pull-right"></span></a></li>
                        <li class="divider"></li>
                        <li><a href="{{url('/viewAllUsers')}}">View All Users <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        
                        <li class="divider"></li>
                    </ul>
                </li>

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage Client Orders <span class="glyphicon glyphicon-file pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{url('/viewClientOrders')}}">View All Orders <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                        <li><a href="{{url('/createClientInvoice')}}">Client Invoices <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                        <li><a href="{{url('/manageATLs')}}">Manage Waybills<span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Track and Trace <span class="glyphicon glyphicon-map-marker pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href='{{ url("/allGPS") }}'>Track Orders <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>

                <li class="dropdown">
                    <a href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();" class="dropdown-toggle" data-toggle="dropdown">Log Out <span class="glyphicon glyphicon-log-out pull-right"></span></a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>


            </ul>
            <!--NAVBAR FOR laptop SCREENS-->
            <ul class="nav navbar-nav hidden-xs hidden-sm" style="margin-top:50px;">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage User Groups<span class="glyphicon glyphicon-user pull-right"></span></a>
                    <ul class="dropdown-menu">

                            <li><a href="{{url('/home')}}">Create Users<span class="glyphicon glyphicon-plus-sign pull-right"></span></a></li>
                        <li class="divider"></li>
                        <li><a href="{{url('/viewAllUsers')}}">View All Users <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        
                        <li class="divider"></li>
                    </ul>
                </li>

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage Client Orders <span class="glyphicon glyphicon-file pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{url('/viewClientOrders')}}">View All Orders <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                        <li><a href="{{url('/createClientInvoice')}}">Client Invoices <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                        <li><a href="{{url('/manageATLs')}}">Manage Waybills<span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Track and Trace <span class="glyphicon glyphicon-map-marker pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href='{{ url("/allGPS") }}'>Track Orders <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>

                <li class="dropdown">
                    <a href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();" class="dropdown-toggle" data-toggle="dropdown">Log Out <span class="glyphicon glyphicon-log-out pull-right"></span></a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>


            </ul>
        </div>
        <!-- TSMA MAIN PAGE -->
        <div class="tmsa_main_body col-md-9" style="height: 100vh">
            <div class="tmsa_user_page">

                <div class="row" style="height: 100vh">
                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9  toppad" >
                        <div class="panel" style=" font-family: 'Josefin Sans', sans-serif; background: #ffffff; border: 2px solid #3baba0;">
                            <div class="panel-heading" style="background: #3baba0; color:#000;">
                                <h3 class="panel-title">Order Details</h3>
                                
                                <div class="description">
                                    <div class="alert alert-danger print-error-msg" style="display:none">

                                        <ul></ul>
                                    </div>

                                    <div class="alert alert-success print-success-msg" style="display:none">

                                        <ul></ul>
                                    </div>
                                </div>
                            </div>

                            <div class="panel-body">
                                <div class="row">
                                <!session message starts-->

                                @if(session('message'))
                                <p>
                                    {{ session('message') }}
                                </p>
                                @endif

                                <!session message ends-->
                                    <div class=" col-md-6 col-lg-6">
                                        <h4 align="center" class="btn-lg" style=" font-family: 'Josefin Sans', sans-serif; margin-left: 5%; margin-right: 5%; background: #ffffff; border: 2px solid #3baba0;">Order Details</h4>
                                            <table class="table table-user-information">
                                                <tbody>
                                                @foreach($orderDetails as $orderDetail)
                                                <tr>
                                                    <td>Order No:</td>
                                                    <td>{{ $orderDetail->serviceIDNo }}</td>
                                                </tr>
                                                <!-- <tr>
                                                    <td>Weight(Tonnes):</td>
                                                    <td>{{ $orderDetail->estimatedWgt }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Value Of Haulage:</td>
                                                    <td>&#8358;{{ $orderDetail->valueOfHaulage }}</td>
                                                </tr> -->
                                                <tr>
                                                    <td>Origin:</td>
                                                    <td>{{ $orderDetail->deliverFrom }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Destination:</td>
                                                    <td>{{$orderDetail->deliverTo}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Item Description:</td>
                                                    <td>{{$orderDetail->itemDescription}}</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>

                                    <div class=" col-md-6 col-lg-6">
                                        <table class="table table-user-information">
                                                <thead>
                                                <tr>
                                                    <th>Resource Type</th>
                                                    <th>Number</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @forelse($resourceTypeNumber as $resource)
                                                <tr>
                                                    <td>{{ $resource->resourceType }}</td>
                                                    <td>{{ $resource->resourceNumber }}</td>
                                                </tr>
                                                @empty

                                                @endforelse
                                                </tbody>
                                        </table>
                                            @endforeach
                                    </div>
                                </div>
                            </div>

                            <div class="panel-body">
                                <div class="row">

                                    <div class=" col-md-12 col-lg-12">
                                        <h4 align="center" class="btn-lg" style=" font-family: 'Josefin Sans', sans-serif; margin-left: 5%; margin-right: 5%; background: #ffffff; border: 2px solid #3baba0;">Assign Carrier</h4>
                                                
                                                <form action="{{ url('/haulageResourceRequest') }}" method="post" enctype="multipart/form-data">

                                                @foreach($orderDetails as $orderDetail)
                                                <input name="orderID" value="{{ $orderDetail->id }}" type="hidden">
                                                @endforeach

                                                {{csrf_field()}}
                                            <table class="table table-user-information">
                                                <thead>
                                                    <tr>
                                                        <th>Select</th>
                                                        <th>Carrier Name</th>
                                                        <th>Required Number</th>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                <?php $index = 0; ?>
                                                @foreach($carrierDetails as $carrierDetail)
                                                <tr>
                                                    <td>
                                                    <input type="checkbox" name="carriers[{{$index}}][selected]" value="{{$carrierDetail->id}}">
                                                    </td>
                                                    <td>
                                                        {{$carrierDetail->company_name}}
                                                    </td>
                                                    <td>
                                                        <input type="number" name="carriers[{{$index}}][resourceNumber]" min="0">
                                                    </td>
                                                    <input type="hidden" name="resourceType[]" value="30T">
                                                </tr>
                                                <?php $index++ ?>
                                                @endforeach 
                                                </tbody>
                                            </table>
                                            <button type="submit" class="btn btn-md btn-success">Submit</button>
                                                </form>
                                        </div>
                                </div>
                            </div>
                            <div class="panel-footer" style="background: #3baba0; color:#000;">
                                <!--  <a data-original-title="Broadcast Message" data-toggle="tooltip" type="button" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-envelope"></i></a>
                                 <span class="pull-right">
                                     <a href="edit.html" data-original-title="Edit this user" data-toggle="tooltip" type="button" class="btn btn-sm btn-warning"><i class="glyphicon glyphicon-edit"></i></a>
                                     <a data-original-title="Remove this user" data-toggle="tooltip" type="button" class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-remove"></i></a>
                                 </span> -->

                                <!--<span class=" text-info panel_foot" style=" color:#000;" > <a style=" color:#000;" href="edit.html" >Edit Profile</a>&nbsp; &nbsp;&nbsp;May 05,2014,03:00 pm <a style=" color:#000;" href="edit.html" style="margin-left: 90%;">Logout</a></span>-->


                            </div>

                        </div>
                    </div>
                </div>





            </div>

        </div>
    </div>
    <!--BOTTOM NAV BAR-->

</div>
<!--     ************************************************** -->
</body>
</html>

