
@include('admin.new_admin.includes.header')
@include('client.includes.head')
<!-- END HEAD -->

<body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-content-white page-md">
<div class="page-wrapper">
<!-- BEGIN HEADER -->
@include('client.includes.menuBar')

<!-- END HEADER -->
<!-- BEGIN HEADER & CONTENT DIVIDER -->
<div class="clearfix"> </div>
<!-- END HEADER & CONTENT DIVIDER -->
<!-- BEGIN CONTAINER -->
<div class="page-container">
<!-- BEGIN SIDEBAR -->
@include('admin.new_admin.includes.sidebar')
<!-- END SIDEBAR -->
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <!-- BEGIN PAGE HEADER-->
    <!-- BEGIN THEME PANEL -->

    <!-- END THEME PANEL -->
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{url('/home')}}">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Dashboard</span>
            </li>
        </ul>
        <div class="page-toolbar">
            <div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
                <i class="icon-calendar"></i>&nbsp;
                <span class="thin uppercase hidden-xs"></span>&nbsp;
                <i class="fa fa-angle-down"></i>
            </div>
        </div>
    </div>
    <!-- END PAGE BAR -->
    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title">
    </h1>
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->
    <div class="row">
        <div class="col-lg-12  col-xs-12 col-sm-12">
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption">
                        All Users </div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body table-responsive">
                    <table class="table table-striped table-bordered table-hover " id="sample_2">
                        <thead>
                        <tr>
                            <th align="center">Name</th>
                            <th align="center">Email</th>
                            <th align="center">User Type</th>
                        </tr>
                        </thead>
                        <tbody>

                        @forelse($resource as $allRequest)
                        <tr>
                            <td><h5>{{$allRequest->name}}</h5></td>
                            <td><h5>{{$allRequest->email}}</h5></td>
                            <td><h5>
                                    @if($allRequest->userAccessLevel == 0)
                                    Admin
                                    @elseif($allRequest->userAccessLevel == 1)
                                    Client
                                    @elseif($allRequest->userAccessLevel == 2)
                                    Carrier
                                    @endif
                                </h5></td>
                        </tr>
                        @empty
                        No Users Exist Yet
                        @endforelse


                        </tbody>
                    </table>

                    {{$resource->links()}}
                </div>
            </div>

        </div>

    </div>

</div>
<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
<!-- BEGIN QUICK SIDEBAR -->
<a href="javascript:;" class="page-quick-sidebar-toggler">
    <i class="icon-login"></i>
</a>

<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="page-footer">
    <div class="page-footer-inner"> 2018 &copy; Truckka
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>|&nbsp;

</div>
<div class="scroll-to-top">
    <i class="icon-arrow-up"></i>
</div>
</div>
<!-- END FOOTER -->
</div>
<!-- BEGIN QUICK NAV -->

<div class="quick-nav-overlay"></div>
@include('client.includes.footer')
