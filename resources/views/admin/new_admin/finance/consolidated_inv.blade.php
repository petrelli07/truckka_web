
    @include('client.includes.header')
    @include('client.includes.head')
    <!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-content-white page-md">
        <div class="page-wrapper">
            <!-- BEGIN HEADER -->
                @include('client.includes.menuBar')

            <!-- END HEADER -->
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
            <!-- END HEADER & CONTENT DIVIDER -->
            <!-- BEGIN CONTAINER -->
            <div class="page-container">
                <!-- BEGIN SIDEBAR -->

                    @include('client.includes.sidebar')
                <!-- END SIDEBAR -->
                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <!-- BEGIN PAGE HEADER-->
                        <!-- BEGIN THEME PANEL -->
                       
                        <!-- END THEME PANEL -->
                        <!-- BEGIN PAGE BAR -->
                        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    <a href="index.html">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>General</span>
                                </li>
                            </ul>
                            
                        </div>
                        <!-- END PAGE BAR -->
                        <!-- BEGIN PAGE TITLE-->
                       
                        <!-- END PAGE TITLE-->
                        <!-- END PAGE HEADER-->
                        <div class="invoice">
                            
                          <div class="" style="background: white;padding: 5px;">
                           <div>
                              <img src="{{ url('assets/layouts/layout/img/logo2.jpg')}}" alt="truckka logo" class="logo-default" style="width: 40%;" /> 
                           </div>
                           @foreach($invoiceDetails as $inv)
                           <div class="flex-container flex-space-between" style="padding: 10px 15px; margin:10px 0px;color: white;background: #F9A81A">
                             <p class="f-paragraph white" style="margin:0px;"><b>INVOICE NO:</b> {{$inv->invoiceNo}}</p>
                             <p class="f-paragraph" style="margin:0px;">{{$inv->created_at}}</p>
                           </div>
                           <div class="flex-container" style="padding: 5px;">
                             <div class="col33" style="padding-right: 0px;">
                               <div style="border-bottom: 2px solid #F9A81A;">
                                 <p class="bold f-paragraph" style="margin: 0px;">BILL TO</p>
                               </div>
                               <div>
                                 <p class="f-paragraph" style="margin: 0px;">{{$companyDetail}}</p>
                                 <!-- <p class="f-paragraph" style="margin: 0px;">Tincan Apapa</p> -->
                               </div>
                             </div>
                             <div class="col77" style="padding-left: 0px;">
                               <div style="border-bottom: 2px solid #F9A81A;">
                                 <p class="bold f-paragraph" style="margin: 0px;">INSTRUCTIONS</p>
                               </div>
                               <div>
                                 <p class="f-paragraph" style="margin: 0px;"><i>Kindly pay using the following details</i></p>
                                 <p class="f-paragraph" style="margin: 0px;"><b>BANK NAME:</b> FIRST CITY MONUMENT BANK PLC (FCMB)</p>
                                 <p class="f-paragraph" style="margin: 0px;"><b>ACCOUNT NAME:</b> SOUTHERN STREAMS UNIVERSAL SERVICES LIMITED</p>
                                 <p class="f-paragraph" style="margin: 0px;"><b>ACCOUNT NO</b> 214 631 9018</p>
                               </div>
                             </div>
                           </div>
                            <div class="row" >
                                <div class="col-xs-12" style=" margin-top: 50px;">
                                    <table class="table table-striped table-hover table-responsive">
                                        <thead class=" white" style="background: #F9A81A">
                                          <tr>
                                              <!-- <td><strong>Date</strong></td> -->
                                              <td class="text-center"><strong>ORIGIN-DESTINATION</strong></td>
                                              <td class="text-center"><strong>TRUCK NO</strong></td>
                                              <td class="text-center"><strong>DELIVERY NOTE NO</strong></td><!-- 
                                              <td class="text-center"><strong>CUSTOMER NAME</strong></td>
                                              <td class="text-center"><strong>QTY</strong></td> -->
                                              <td class="text-center"><strong>PRICE</strong></td><!-- 
                                              <td class="text-center"><strong>AMOUNT</strong></td> -->

                                          </tr>
                                        </thead>
                                    <tbody>
                                    @foreach($resDetsResult as $invoice)
                                      @foreach($invoice as $in)
                                      <tr>
                                        <!-- <td class="text-center">30-Aug</td> -->
                                        <td class="text-center">{{$in['origin']}} - {{$in['deliverTo']}}</td>
                                        <td class="text-center">{{$in['plateNumber']}}</td>
                                        <td class="text-center">{{$in['waybillNo']}}</td><!-- 
                                        <td class="text-center">Kano Warehouse</td>
                                        <td class="text-center">600</td>
                                        <td class="text-center">1000</td> -->
                                        <td class="text-center">&#8358;{{number_format($in['price'], 2, '.', ',')}}</td>
                                      </tr>
                                      @endforeach
                                    @endforeach
                                      <tr>
                                        <td></td>
                                        <td></td>
                                        <td class="text-center"><b>TOTAL</b></td>
                                        <td class="text-center"><b>&#8358;{{number_format($inv->price, 2, '.', ',')}} </b></td>
                                      </tr>
                                      <tr>
                                        <td></td>
                                        <td></td>
                                        <td class="text-center"><b>5% VAT</b></td>
                                        <td class="text-center"><b>&#8358;{{number_format($vat, 2, '.', ',')}} </b></td>
                                      </tr>
                                      <tr>
                                        <td></td>
                                        <td></td>
                                        <td class="text-center"><b>TOTAL DUE (IMMEDIATELY)</b></td>
                                        <td class="text-center"><b>&#8358;{{number_format($pricePlusVat, 2, '.', ',')}}</b></td>
                                      </tr>
                                
                                    </tbody>
                            </table>
                                </div>
                            </div>
                            @endforeach
                            <div class="row">
                              <div class="col-md-12">
                                <p class="f-paragraph" style="margin:0px">Thank you for your business!</p>
                              </div>
                              <div class="col-md-12">
                                <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ_OaINPT1iIUcPE_SiQHB0Rt8-I-HTCOeGvWpx09AqXUpOtQx0" style="width: 100px;">
                              </div>
                              
                            </div>
                          </div>
                            <div class="row" style="margin-top: 50px;">
                                <div class="col-xs-4 invoice-block">
                                    
                                    <a class="btn btn-lg blue hidden-print margin-bottom-5" onclick="javascript:window.print();"> Print
                                        <i class="fa fa-print"></i>
                                    </a>
                                    
                                </div>
                               
                                
                            </div>
                      </div>
                    </div>
                    <!-- END CONTENT BODY -->
                </div>
                <!-- END CONTENT -->
                <!-- BEGIN QUICK SIDEBAR -->
                <a href="javascript:;" class="page-quick-sidebar-toggler">
                    <i class="icon-login"></i>
                </a>
               
                <!-- END QUICK SIDEBAR -->
            </div>
            <!-- END CONTAINER -->
            <!-- BEGIN FOOTER -->
            
            <!-- END FOOTER -->
        </div>
        <!-- BEGIN QUICK NAV -->
        <nav class="quick-nav">
            <a class="quick-nav-trigger" href="#0">
                <span aria-hidden="true"></span>
            </a>
           
            <span aria-hidden="true" class="quick-nav-bg"></span>
        </nav>
        <div class="quick-nav-overlay"></div>
        <!-- END QUICK NAV -->
        <!--[if lt IE 9]>
<script src="../assets/global/plugins/respond.min.js"></script>
<script src="../assets/global/plugins/excanvas.min.js"></script> 
<script src="../assets/global/plugins/ie8.fix.min.js"></script> 
<![endif]-->
      @include('client.includes.footer')