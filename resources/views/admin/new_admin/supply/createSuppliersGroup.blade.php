
    @include('client.includes.header')
    @include('client.includes.head')
    <!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-content-white page-md">
     <!--  <div class="modal fade bs-modal-sm alert-modal flex-container flex-center flex-vertical-align flex-responsive " id="cancel" tabindex="-1" role="dialog" aria-hidden="true" style="display:inherit;background:rgba(25, 24, 24, 0.48);opacity:1;"><div class="col44 loader-container"><img src="{{url('/assets/pages/img/loader.gif')}}"></div></div> -->
     
    </div>
        <div class="page-wrapper">
            <!-- BEGIN HEADER -->
                @include('client.includes.menuBar')

            <!-- END HEADER -->
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
            <!-- END HEADER & CONTENT DIVIDER -->
            <!-- BEGIN CONTAINER -->
            <div class="page-container">
                <!-- BEGIN SIDEBAR -->
                    @include('client.includes.sidebar')
                <!-- END SIDEBAR -->
                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <!-- BEGIN PAGE HEADER-->
                        <!-- BEGIN THEME PANEL -->
                       
                        <!-- END THEME PANEL -->
                        <!-- BEGIN PAGE BAR -->
                        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    <a href="index.html">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Dashboard</span>
                                </li>
                            </ul>
                            <div class="page-toolbar">
                                <div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
                                    <i class="icon-calendar"></i>&nbsp;
                                    <span class="thin uppercase hidden-xs"></span>&nbsp;
                                    <i class="fa fa-angle-down"></i>
                                </div>
                            </div>
                        </div>
                        <!-- END PAGE BAR -->
                        <!-- BEGIN PAGE TITLE-->
                        <h1 class="page-title">
                        </h1>
                        <!-- END PAGE TITLE-->
                        <!-- END PAGE HEADER-->
<!--  -->                        <div class="row">
                            <div class="col-lg-10 col-lg-offset-1 col-xs-12 col-sm-12">
                                <div class="portlet light ">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <span class="caption-subject font-dark bold uppercase">Create Supplier's Group</span>                                         
                                      </div>
                                        
                                    </div>
                                     <div class="portlet-body form">
                                        <form role="form" ajax-submit="true"  class="ajax-form-submit order-form" 
                                        @if(isset($editField))
                                          action="{{url('/suppliers/group/edit/save/'.$editField->id)}}" 
                                        @else 
                                          action="{{url('/suppliers/group/save')}}"
                                        @endif>
                                         {{csrf_field()}}
                                            <div class="form-body">
                                            	<div class="row">
                                            		<div class="col-md-6">
                                                  <div class="form-group form-md-line-input has-info">
                                                      <input class="form-control validate" name="groupName" value="@if(isset($editField)){{$editField->groupName}}@endif" id="contactName"  >
                                                        
                                                      <label for="form_control_1">Group Name</label>
                                                    </div>
                                                </div>
                                            	</div>
                                              <div class="portlet box green">
                                                    <div class="portlet-title">
                                                        <div class="caption">
                                                          Add Suppliers to group </div>
                                                        <div class="tools"> </div>
                                                    </div>
                                                    <div class="portlet-body table-responsive">
                                                        <table class="table table-striped table-bordered table-hover " id="sample_2">
                                                            <thead>
                                                                <tr>
                                                                    <th align="center">Supplier Name</th>
                                                                    <th align="center">Supplier Address</th>
                                                                    <th align="center">Action</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>

                                                            @forelse($supplierDetail as $supplier)
                                                                <tr>
                                                                    <td><h5>{{$supplier->supplierName}}</h5></td>
                                                                    <td><h5>{{$supplier->supplierAddress}}</h5></td>
                                                                    <td>
                                                                       <input type="checkbox" value="{{$supplier->id}}"  @if (in_array($supplier->id, $groupMembers)) 
                                                                         checked="checked"
                                                                        @endif name="suppliers[]">
                                                                    </td>
                                                                </tr>
                                                            @empty
                                                                
                                                            @endforelse

                                                            </tbody>
                                                        </table>
                                                    </div>
                                      </div>

                                           
                                            </div>
                                            <div class="form-actions noborder">
                                                <button type="submit" class="btn btn-primary validate-form-button" form-target="order-form" data-toggle="modal" href="#order_summary">Create</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        
                    </div>
                    <!-- END CONTENT BODY -->
                </div>
                <!-- END CONTENT -->
                <!-- BEGIN QUICK SIDEBAR -->
              
                <!-- END QUICK SIDEBAR -->
            </div>
            <!-- END CONTAINER -->
            <!-- BEGIN FOOTER -->
            <div class="page-footer">
                <div class="page-footer-inner"> 2018 &copy; Truckka  &nbsp;|&nbsp;
                   
                </div>
                <div class="scroll-to-top">
                    <i class="icon-arrow-up"></i>
                </div>
            </div>
            <!-- END FOOTER -->
        </div>
        <!-- BEGIN QUICK NAV -->
        
        <div class="quick-nav-overlay"></div>
 @include('client.includes.footer')
