
    @include('client.includes.header')
    @include('client.includes.head')
    <!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-content-white page-md">
     <!--  <div class="modal fade bs-modal-sm alert-modal flex-container flex-center flex-vertical-align flex-responsive " id="cancel" tabindex="-1" role="dialog" aria-hidden="true" style="display:inherit;background:rgba(25, 24, 24, 0.48);opacity:1;"><div class="col44 loader-container"><img src="{{url('/assets/pages/img/loader.gif')}}"></div></div> -->
     
    </div>
        <div class="page-wrapper">
            <!-- BEGIN HEADER -->
                @include('client.includes.menuBar')

            <!-- END HEADER -->
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
            <!-- END HEADER & CONTENT DIVIDER -->
            <!-- BEGIN CONTAINER -->
            <div class="page-container">
                <!-- BEGIN SIDEBAR -->
                    @include('client.includes.sidebar')
                <!-- END SIDEBAR -->
                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <!-- BEGIN PAGE HEADER-->
                        <!-- BEGIN THEME PANEL -->
                       
                        <!-- END THEME PANEL -->
                        <!-- BEGIN PAGE BAR -->
                        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    <a href="index.html">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Dashboard</span>
                                </li>
                            </ul>
                            <div class="page-toolbar">
                                <div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
                                    <i class="icon-calendar"></i>&nbsp;
                                    <span class="thin uppercase hidden-xs"></span>&nbsp;
                                    <i class="fa fa-angle-down"></i>
                                </div>
                            </div>
                        </div>
                        <!-- END PAGE BAR -->
                        <!-- BEGIN PAGE TITLE-->
                        <h1 class="page-title">
                        </h1>
                        <!-- END PAGE TITLE-->
                        <!-- END PAGE HEADER-->
<!--  -->                        <div class="row">
                            <div class="col-lg-10 col-lg-offset-1 col-xs-12 col-sm-12">
                                <div class="portlet light ">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <span class="caption-subject font-dark bold uppercase">Create New User</span>                                         
                                      </div>
                                        
                                    </div>
                                     <div class="portlet-body form">
                                        <form role="form" ajax-submit="true" class="ajax-form-submit" method="post" action="{{url('/supplier/save')}}" id="order-form">
                                         {{csrf_field()}}
                                            <div class="form-body">
                                            	<div class="row">
                                                    <div class="col-md-6">
                                                      <div class="form-group form-md-line-input">
                                                        <input type="text" class="form-control form-copy validate" id="itemDescription" placeholder="Enter Name Here"  bind-to="summaryHaulageDec" name="name">
                                                        <label for="form_control_1">FullName</label>
                                                        <span class="help-block">Enter the full name of the supplier</span>
                                                      </div>
                                                    </div>                                           		
                                                    <div class="col-md-6">
                                                      <div class="form-group form-md-line-input">
                                                        <input type="email" class="form-control form-copy validate" id="itemDescription" placeholder="Enter Email Here"  bind-to="summaryHaulageDec" name="email">
                                                        <label for="form_control_1">Email</label>
                                                        <span class="help-block">Enter the correct email of the supplier</span>
                                                      </div>
                                                    </div>  
                                                    <div class="col-md-6">
                                                      <div class="form-group form-md-line-input">
                                                        <input type="text" class="form-control form-copy validate" id="itemDescription" placeholder="Enter Name Here"  bind-to="summaryHaulageDec" name="address">
                                                        <label for="form_control_1">Address</label>
                                                        <span class="help-block">Enter the address of the supplier</span>
                                                      </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                      <div class="form-group form-md-line-input" style="padding-top: 0px;">
                                                        <label for="multiple1" class="control-label">Select Suppliers Group</label>
                                                        <select id="multiple1" class="form-control select2-multiple" name="group"  placeholder='select groups'>
                                                            <option></option>
                                                            @forelse($companyGroups as $group)
                                                              <option value="{{$group->id}}">{{$group->groupName}}</option>
                                                             @empty
                                                               no group
                                                            @endforelse
                                                              

                                                        </select>
                                                      </div>
                                                </div>
                                                                                          
                                            	</div>
                                            <input class="haulageType" type="hidden" id="haulageType" value="0">
                                            <input class="haulageType" type="hidden" id="requestType" value="0">
                                            </div>
                                            <div class="form-actions noborder">
                                                <button  type="submit" class="btn btn-primary validate-form-button" >Create</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        
                    </div>
                    <!-- END CONTENT BODY -->
                </div>
                <!-- END CONTENT -->
                <!-- BEGIN QUICK SIDEBAR -->
              
                <!-- END QUICK SIDEBAR -->
            </div>
            <!-- END CONTAINER -->
            <!-- BEGIN FOOTER -->
            <div class="page-footer">
                <div class="page-footer-inner"> 2018 &copy; Truckka  &nbsp;|&nbsp;
                   
                </div>
                <div class="scroll-to-top">
                    <i class="icon-arrow-up"></i>
                </div>
            </div>
            <!-- END FOOTER -->
        </div>
        <!-- BEGIN QUICK NAV -->
        
        <div class="quick-nav-overlay"></div>
 @include('client.includes.footer')
