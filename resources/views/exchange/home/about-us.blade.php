
@extends('home.layout.content')
@section('content')



<!--About Truckka-->
<div class="container-fluid" >
    <div class="row about-al p-relative">
        <div class="benifits-overlay"></div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ben-pad">
            <div class="benefits-title col-xs-12 no-padding">
                <h2>
                    About Truckka
                </h2>
            </div>
            <div class="benefits-info col-xs-12 no-padding">
                
                <div class="col-sm-12 col-xs-12 pad-left">
                    <div class="benefits-info1">
                        
                        <p>
                        TRUCKKA Logistics Limited is a fourth-party logistics (4PL) tech-solutions company. 
                        We leverage our resources and capabilities with those of our partners to provide logistics services and tech-enabled solutions within the logistics supply chain.  
                        </p>
                    </div>
                </div>


                <div class="col-sm-12 col-xs-12 pad-left">
                    <div class="benefits-info1 margin-top-40">
                        
                        <p>
                        We are a team of professionals with rich and diverse corporate executive background with a focus to 
                        create an efficient marketplace for the logistics industry in Nigeria. 
                        </p>
                    </div>
                </div>


                <div class="col-sm-12 col-xs-12 pad-left">
                    <div class="benefits-info1 margin-top-40">
                        
                        <p>
                        We are therefore positioned as your go-to logistics solutions provider in a
                         difficult and infrastructure-challenged environment where players yearn for safety,
                          price-efficiency, professionalism and trust - our core-value proposition. 
                        </p>
                    </div>
                </div>


            </div>
            
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding">
            <div class="benifits-bg-im-22"></div>
        </div>
    </div>
</div>
<!--End of it-->



<!--Mission and vision-->
<div class="container-fluid" id="about">
    <div class="row about-al">
        <div class="container">
            <div class="row">
                <div class="about-title">
                    <h2>
                        VISION / MISSION
                    </h2>
                    <p class="about-p">
                    To build an efficient digital marketplace that matches owners of freight to truck services on demand. 
                     We equally provide enterprise logistics services within chosen segments.
                    </p>
                </div>
                <div class="about-btn">
                   
                </div>
            </div>
        </div>
    </div>
</div>
<!--end of mission and vision-->


<!--Board members-->
@include('home.board-members')
<!--end of Board members-->



@endsection