@extends('home.layout.content')
@section('content')

<div class="container-fluid">
    <div class="row banner">
        <div class="location-center-align">
            <div class="location-center">
                <div class="location-title width-100 text-center">
                    <h1 class="text-center font-22 text-center animate-h1">Welcome to Truckka! Your freight Exchange</span></h1>
                </div>

                        <div class="location-search" >
                           <div class="location-content white white-lbl">
                    
                                        <div class="freight-exc">
                                            <ul class="report-radio-buttons tax-radio-btn">
                                                <li class="">
                                                    <input  id="radio-1" class="radio-custom equipments " name="equipmentType" type="radio"  checked tabindex="3">
                                                    <label for="radio-1" class="radio-custom-label lbl-chng-txt-change i_need_truck"><span class="lbl-chng-span">I NEED TRUCK</span></label>
                                                </li>
                                                <li class="m-l-30">
                                                    <input  id="radio-2" class="radio-custom equipments"  name="equipmentType" type="radio" tabindex="4">
                                                    <label for="radio-2" class="radio-custom-label lbl-chng-txt-change i_can_give_truck"><span class="lbl-chng-span">I HAVE TRUCK</span></label>
                                                </li>
                                            </ul>
                                        </div>



                    <!---- THIS IS FOR I NEED TRUCK ------>
                            <div class="for-i-need-truck">
                            <div id="i_need_truck">

                            <div class="col-xs-12 no-padding">
                            <div class="col-sm-6 col-xs-12 no-padding">
                                <div class="col-xs-12">
                                    <div class="col-xs-12 no-padding">
                                        <div class="col-xs-12 no-padding">
                                            <div class="booking-label-change mtp-lbl">
                                                <label>Origin</label>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 no-padding">
                                            <select class="text-booking-change">
                                            <option class="text-booking-change" name="origin">?</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12 no-padding">
                                <div class="col-xs-12">
                                    <div class="col-xs-12 no-padding">
                                        <div class="col-xs-12 no-padding">
                                            <div class="booking-labe-changel">
                                                <label>Destination</label>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 no-padding">
                                            <select class="text-booking-change">
                                            <option class="text-booking-change" name="origin">?</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                <!--Hidden for origin-destination form-->
                <form id="originForm" style="display:none;">
                    {{csrf_field()}}
                    <input type="hidden" id="origin2" name="originToServer">
                </form>
                <!--end Hidden for origin-destination form-->

                    <!---- THIS IS FOR I NEED TRUCK ------>

                       <div class="location-search" id="i_need_truck">

                           <form id="getQuoteForm">
                               {{csrf_field()}}

                               <div class="location-content white white-lbl">
                                   <div class="col-xs-12 no-padding">

                                       <div class="col-sm-6 col-xs-12 no-padding">
                                           <div class="col-xs-12">
                                               <div class="col-xs-12 no-padding">
                                                   <div class="col-xs-12 no-padding">
                                                       <div class="booking-label-change mtp-lbl">
                                                           <label>Origin</label>
                                                       </div>
                                                   </div>
                                                   <div class="col-xs-12 no-padding">
                                                       <select class="text-booking-change" name="origin" required id="standardOrigin">
                                                           <option class="text-booking-change">[-Select Origin-]</option>
                                                           @for ($i=0; $i < count($uniqueOrigins); $i++)
                                                           <option class="text-booking-change" value="{{$uniqueOrigins[$i]}}">{{$uniqueOrigins[$i]}}</option>
                                                           @endfor
                                                       </select>
                                                   </div>
                                               </div>
                                           </div>
                                       </div>

                                       <input id="number_of_truck" value="2" type="hidden">

                                       <div class="col-sm-6 col-xs-12 no-padding">
                                           <div class="col-xs-12">
                                               <div class="col-xs-12 no-padding">
                                                   <div class="col-xs-12 no-padding">
                                                       <div class="booking-labe-changel">
                                                           <label>Destination</label>
                                                       </div>
                                                   </div>
                                                   <div class="col-xs-12 no-padding">
                                                       <select class="text-booking-change" required id="destinationSelect" name="destination">

                                                       </select>
                                                   </div>
                                               </div>
                                           </div>
                                       </div>
                                   </div>

                                   <!--listing 2 -->
                                   <div class="col-xs-12 no-padding">
                                       <div class="col-sm-6 col-xs-12 no-padding">
                                           <div class="col-xs-12">
                                               <div class="col-xs-12 no-padding">
                                                   <div class="col-xs-12 no-padding">
                                                       <div class="booking-labe-changel">
                                                           <label>Choose Truck</label>
                                                       </div>
                                                   </div>
                                                   <div class="col-xs-12 no-padding">
                                                       <select class="text-booking-change" name="truckType">
                                                           <option class="text-booking-change" required value="">[-Select Truck Type-]</option>
                                                           @foreach($truckTypes as $truckType)
                                                           <option class="text-booking-change" value="{{$truckType->id}}">{{$truckType->truck_type_description}}</option>
                                                           @endforeach
                                                       </select>
                                                   </div>
                                               </div>
                                           </div>
                                       </div>

                                       <div class="col-sm-6 col-xs-12 no-padding">
                                           <div class="col-xs-12">
                                               <div class="col-xs-12 no-padding">
                                                   <div class="col-xs-12 no-padding">
                                                       <div class="booking-labe-changel">
                                                           <label>Pick Up Date</label>
                                                       </div>
                                                   </div>
                                                   <div class="col-xs-12 no-padding">
                                                       <input class="text-booking-change1"  data-val="true" data-val-date="The field PickUpDateReady must be a date." id="datepicker" name="pickupdate" placeholder="MM/DD/YYYY"  tabindex="5">
                                                       <div id="datetime"></div>
                                                       <div class="date-ic"><img src="{{URL::to('exchange/home_assets/assets/images/date-ic-trackka.png')}}" alt="TQDate" class="img-responsive"></div>
                                                       <span class="field-validation-valid col-xs-12 no-padding c-red" data-valmsg-for="UserName" id="txtDeliveryDateError" data-valmsg-replace="true"></span>
                                                   </div>
                                               </div>
                                           </div>
                                       </div>
                                   </div>
                                   <!--end it-->

                                   <!--listing 2 -->
                                   <div class="col-xs-12 no-padding">
                                       <div class="col-sm-6 col-xs-12 no-padding">
                                           <div class="col-xs-12">
                                               <div class="col-xs-12 no-padding">
                                                   <div class="col-xs-12 no-padding">
                                                       <div class="booking-labe-changel">
                                                           <label>Contact Phone Number</label>
                                                       </div>
                                                   </div>
                                                   <div class="col-xs-12 no-padding">
                                                       <input class="text-booking-change1"  data-val="true" data-val-date="Enter Contact Phone Number" name="contact_phone" id="contact_phone" placeholder="Enter Contact Phone Number"  tabindex="5">

                                                       <span class="field-validation-valid col-xs-12 no-padding c-red" data-valmsg-for="UserName" id="txtDeliveryDateError" data-valmsg-replace="true"></span>
                                                   </div>
                                               </div>
                                           </div>
                                       </div>

                                       <div class="col-sm-6 col-xs-12 no-padding">
                                           <div class="col-xs-12">
                                               <div class="col-xs-12 no-padding">
                                                   <div class="col-xs-12 no-padding">
                                                       <div class="booking-labe-changel">
                                                           <label>Pickup Address</label>
                                                       </div>
                                                   </div>
                                                   <div class="col-xs-12 col-lg-12 no-padding">
                                                       <textarea id="pickup_address" class="text-booking-change-textarea"  name="pickup_address" placeholder="Enter Description"  type="text"></textarea>
                                                       <span class="field-validation-valid c-red" data-valmsg-for="origin"  data-valmsg-replace="true"></span>
                                                   </div>
                                               </div>
                                           </div>
                                       </div>
                                   </div>
                                   <!--end it-->


                                   <div class="col-xs-12 no-padding">
                                       <div class="col-sm-12 col-xs-12 no-padding">
                                           <div class="col-xs-12">
                                               <div class="col-xs-12 no-padding">
                                                   <div class="col-xs-12 no-padding">
                                                       <div class="booking-label-change mtp-lbl">
                                                           <label>Description of items</label>
                                                       </div>
                                                   </div>
                                                   <div class="col-xs-12 col-lg-12 no-padding">
                                                       <textarea id="description" class="text-booking-change-textarea"  name="description" placeholder="Enter Description"  type="text"></textarea>
                                                       <span class="field-validation-valid c-red" data-valmsg-for="origin"  data-valmsg-replace="true"></span>
                                                   </div>
                                               </div>
                                           </div>
                                       </div>
                                   </div>
                                   <div class="col-xs-12 no-padding">


                                       <div class="col-xs-12">
                                           <div class="quote-btn col-xs-12 no-padding margin-top-55">
                                               <a id="btnGetQuote" class="cursor-pointer getQuoteButton" tabindex="8">Get Quote</a>
                                               <a id="backtooptions3" class="cursor-pointer" tabindex="8">Back to Options</a>
                                           </div>


                                       </div>
                                   </div>
                               </div>

                           </form>
>>>>>>> homepage

                        <div class="col-xs-12 no-padding">
                        <div class="col-sm-12 col-xs-12 no-padding">
                                <div class="col-xs-12">
                                    <div class="col-xs-12 no-padding">
                                        <div class="col-xs-12 no-padding">
                                            <div class="booking-label-change mtp-lbl">
                                                <label>Description of items</label>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-lg-12 no-padding">
                                            <textarea class="text-booking-change-textarea"  name="origin" placeholder="Enter Description"  type="text"></textarea>
                                            <span class="field-validation-valid c-red" data-valmsg-for="origin"  data-valmsg-replace="true"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 no-padding">
                            
                            
                            <div class="col-xs-12">
                            <div class="about-btn " style="width:50%;margin-top:30px;">
                                    <button  class="cursor-pointer btnGetQuotenew" tabindex="8">Get Quote</button>
                                   
                                </div>

                                
                            </div>
                        </div>
                    </div>
           
                </div>
                    
 <!------------------------------------------------------------- END OF IT --------------------------------------------->


 <!---- -------------------------------------------THIS IS FOR I CAN GIVE TRUCKS ----------------------------------->

                       <div class="this-for-i-can-give-trucks">
                            <div id="i_can_give_truck">
                            <div class="col-xs-12 no-padding">
                               


                            <div class="col-sm-6 col-xs-12 no-padding">
                                    <div class="col-xs-12">
                                        <div class="col-xs-12 no-padding">

                                        
                                            <div class="col-xs-12 no-padding">
                                                <div class="booking-label-change mtp-lbl">
                                                    <label>Type of Truck</label>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 no-padding">
                                                <select class="text-booking-change">
                                                <option class="text-booking-change" name="origin">?</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            <div class="col-sm-6 col-xs-12 no-padding">
                                    <div class="col-xs-12">
                                        <div class="col-xs-12 no-padding">
                                            <div class="col-xs-12 no-padding">
                                                <div class="booking-label-change mtp-lbl">
                                                    <label>Number of Trucks</label>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 no-padding">
                                                <input type="text" class="text-booking-change" name="number_of_truck"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-xs-12 no-padding">

                                <div class="col-sm-6 col-xs-12 no-padding">
                                    <div class="col-xs-12">
                                        <div class="col-xs-12 no-padding">
                                            <div class="col-xs-12 no-padding">
                                                <div class="booking-labe-changel">
                                                    <label>Location Covered</label>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 no-padding">

                                               <ul class="report-radio-buttons tax-radio-btn">
                                                <li class="">
                                                    <input type="checkbox"  class="text-booking-change2"id="defaultInline1">
                                                    <span class="lbl-chng-span22">NW</span>
                                                </li>
                                                <li class="">
                                                    <input type="checkbox" class="text-booking-change2" id="defaultInline1">
                                                    <span class="lbl-chng-span22">NE</span>
                                                </li>
                                                <li class="">
                                                    <input type="checkbox" class="text-booking-change2" id="defaultInline1">
                                                    <span class="lbl-chng-span22">SE</span>
                                                </li>
                                                <li class="">
                                                    <input type="checkbox"  class="text-booking-change2"id="defaultInline1">
                                                    <span class="lbl-chng-span22">SW</span>
                                                </li>
                                                <li class="">
                                                    <input type="checkbox"  class="text-booking-change2" id="defaultInline1">
                                                    <span class="lbl-chng-span22">SS</span>
                                                </li>

                                                <li class="">
                                                    <input type="checkbox"  class="text-booking-change2" id="defaultInline1">
                                                    <span class="lbl-chng-span22">NC</span>
                                                </li>
                                                </ul>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6 col-xs-12 no-padding">
                                    <div class="col-xs-12">
                                        <div class="col-xs-12 no-padding">
                                            <div class="col-xs-12 no-padding">
                                                <div class="booking-labe-changel">
                                                    <label>Location of Trucks</label>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 no-padding">
                                                <select class="text-booking-change" name="state">
                                                    <option class="text-booking-change" required value="">[-Select Truck Location-]</option>
                                                    @foreach($state as $states)
                                                    <option class="text-booking-change" value="{{$states->id}}">{{$states->state_description}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                           </div>

                            <!--listing 2 -->
                            <!-- <div class="col-xs-12 no-padding">
                            <div class="col-sm-6 col-xs-12 no-padding">
                                    <div class="col-xs-12">
                                        <div class="col-xs-12 no-padding">
                                            <div class="col-xs-12 no-padding">
                                                <div class="booking-labe-changel">
                                                    <label>Name</label>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 no-padding">
                                                
                                                <input type="text" class="text-booking-change" name="name" placeholder="Enter Name" />
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-sm-6 col-xs-12 no-padding">
                                    <div class="col-xs-12">
                                        <div class="col-xs-12 no-padding">
                                            <div class="col-xs-12 no-padding">
                                                <div class="booking-labe-changel">
                                                    <label>Phone Number</label>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 no-padding">
                                            <input type="text" class="text-booking-change" name="name" placeholder="Enter Phone Number" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                            <!--end it-->

                            <div class="col-sm-12 col-xs-12 no-padding">
                                <div class="col-xs-12">
                                    <div class="col-xs-12 no-padding">
                                        <div class="col-xs-12 no-padding">
                                            <div class="booking-labe-changel">
                                                <label>Relationship with the truck</label>
                                            </div>
                                        </div>

                                        <div class="col-xs-12 no-padding">
                                       
                                            <div class="col-xs-12 no-padding">
                                                <select class="text-booking-change">
                                                <option class="text-booking-change" name="origin">Truck Owner</option>
                                                <option class="text-booking-change" name="origin">Aggregator</option>
                                                </select>
                                            </div>
                                            </div>

                                       
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12 no-padding">
                                
                                
                                <div class="col-xs-12" >
                                    <div class="about-btn" style="width:50%;margin-top:30px;">
                                        <button class="cursor-pointer btnGetQuotenew2" tabindex="8">Get Quote</button>
                                        
                                    </div>

                                    
                                </div>
                            </div>
                            </div>

                            </div>


<!-------------------------------------- END OF IT ---------------------------------------------------->




<!---- -------------------------------------------THIS IS FOR GET QUOTE SUMMERY ----------------------------------->

                    <div class="for-quote-summary">

                    <div id="get_quote">
                            <div class="col-xs-12 no-padding">
                                <h3 style="color:#fff;"><u>Quote Summery</u><h3>

                                        <div class="col-sm-6 col-xs-12 no-padding">
                                        <span style="color:#fff;margin-right:4px;">Origin:</span>
                                        <span style="color:#ccc;">Nigeria</span>
                                        </div>

                                        <div class="col-sm-6 col-xs-12 no-padding">
                                        <span style="color:#fff;margin-right:4px;">Destination:</span>
                                        <span style="color:#ccc;"> lagos state</span>
                                        </div>

                                        <div class="col-sm-6 col-xs-12 no-padding">
                                        <span style="color:#fff;margin-right:4px;">Destination:</span>
                                        <span style="color:#ccc;"> lagos state</span>
                                        </div>

                                        <div class="col-sm-6 col-xs-12 no-padding">
                                        <span style="color:#fff;margin-right:4px;">Destination:</span>
                                        <span style="color:#ccc;"> lagos state</span>
                                        </div>


                                        <div class="col-xs-12 no-padding">
                                            
                                            
                                            <div class="col-xs-12">
                                                <div class="quote-btn col-xs-12 no-padding margin-top-55">
                                                    <a id="btnGetQuote" class="cursor-pointer" tabindex="8">Submit</a>
                                                    <a id="backtooptionscancle" class="cursor-pointer" tabindex="8">Cancle</a>
                                                </div>

                                                
                                            </div>
                                        </div>
                            </div>

                    </div>


                    <form action="{{url('/exchange/submitQuote')}}" method="POST">
                        {{csrf_field()}}
                        <input id="routeIDForm" type="hidden" name="routeID">
                        <input id="truckNumberForm" type="hidden" name="truckNumberForm">
                        <input id="truckTypeIDForm" type="hidden" name="truckTypeIDForm">
                        <input id="itemDescriptionForm" type="hidden" name="itemDescriptionForm">
                        <input id="pickUpDateForm" type="hidden" name="pickUpDateForm">
                        <input id="totalAmtForm" type="hidden" name="totalAmtForm">
                        <input id="pickupAddressForm" type="hidden" name="pickupAddressForm">
                        <input id="contactPhoneForm" type="hidden" name="contactPhoneForm">
                    </form>


<!-------------------------------------- END OF IT ---------------------------------------------------->

        
        </div></div> 
            </div>
        </div>
    </div>
</div>

<!--Benefits of the exchange-->
<div class="container-fluid" id="benefits">
    <div class="row about-al">
        <div class="container">
            <div class="row">
                <div class="work-title">
                    <h2>
                    BENEFITS OF THE EXCHANGE
                    </h2>
                </div>
                <div class="col-xs-12 no-padding work-block-al">
                    
                    

                    <div class="col-sm-9 col-md-6 col-lg-4">
                        <div class="work-block-horizontal2">
                        <div class="work-imm">
                            <div class="image-freight">

                                <img src="{{URL::to('home_assets/assets/images/map-with-truck-ful.png')}}"  >           
                               
                            </div>
                            <div class ="content-freight">
                                <p class="p-horizon">
                                Peace of knowing in real-time the location of your truck / freight</p>
                        </div>
                        </div>
                    </div>
                    </div>

                    <div class="col-sm-9 col-md-6 col-lg-4">
                        <div class="work-block-horizontal2">
                        <div class="work-imm">
                            <div class="image-freight">
                                <img src="{{URL::to('home_assets/assets/images/pig-white.png')}}" alt="No Hiden fee"> 
                            </div>          
                            <div class ="content-freight">
                                        <p class="p-horizon">
                                        Save funds through transparent / price efficiency.
                                    </p>
                            </div>
                            </div>
                               
                        </div>
                    </div>

                    <div class="col-sm-9 col-md-6 col-lg-4">
                        <div class="work-block-horizontal">
                        <div class="work-imm">
                            <div class="image-freight">
                                <img src="{{URL::to('home_assets/assets/images/worrying-less-white.png')}}">     
                            </div>      
                            <div class ="content-freight">
                                        <p class="p-horizon">
                                        Worry less on finding freight / truck
                                    </p>
                            </div>
                            </div>
                               
                        </div>
                    </div>

                    <div class="col-sm-9 col-md-6 col-lg-6">
                        <div class="work-block-horizontal2">
                        <div class="work-imm">
                            <div class="image-freight">
                                <img src="{{URL::to('home_assets/assets/images/lap-phone-white.png')}}"> 
                            </div>          
                            <div class ="content-freight">
                                        <p class="p-horizon">
                                        Easy to use web / mobile technology 
                                    </p>
                            </div>
                            </div>
                               
                        </div>
                    </div>

                     <div class="col-sm-9 col-md-6 col-lg-6">
                     <div class="work-block-horizontal">
                            <div class="work-imm">
                            <div class="image-freight">
                                <img src="{{URL::to('home_assets/assets/images/customer_support_hover.png')}}" alt="No Hiden fee">
                            </div>
                            <div class ="content-freight">
                                <p class="p-horizon">
                                Helpful / Courteous Customer Support
                                </p>
                            </div>
                            </div>
                               
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end of Benefits of exchange-->




<!--Our Services-->

<div class="container-fluid margin-for-deter-service" id="our-services-mv">
<div class="container">
            <div class="row">
    
                <div class="work-title">
                    <h2>
                        OUR SERVICES
                    </h2>
                </div>

				<div class="section group">
					<div class="col span_1_of_3 col-determinate-service">
					<div class="board-images">
                        <img src="{{URL::to('home_assets/assets/images/contract logistics.jpg')}}">
                    </div>
                    <div class="content-d-service">
                        <h3>CONTRACT LOGISTICS</h3>
                        <p>We provide Full Truck Load (FTL) on an outsourced contractual basis </p>
                    </div>
                    </div>

                    
					<div class="col span_1_of_3 col-determinate-service">
					<div class="board-images">
                        <img src="{{URL::to('home_assets/assets/images/roadrunner-truck-angle_0.jpg')}}">
                    </div>
                    <div class="content-d-service">
                        <h3>Freight/Transportation Exchange</h3>
                        <p>Our digital mobile & web enabled freight-truck matching platform </p>
                    </div>
                    </div>
                    

					<div class="col span_1_of_3 col-determinate-service">
					<div class="board-images">
                        <img src="{{URL::to('home_assets/assets/images/tms icon.jpg')}}">
                    </div>

                     <div class="content-d-service">
                        <h3>Transportation Management System (TMS)</h3>
                        <p>bespoke TMS delivers visibility, control and seamless communication across your enterprise </p>
                    </div>

					</div>
				</div>
              
                    
</div>
</div>
</div> 
<!--end services-->





<!--About Truckka-->
<div class="container-fluid" >
    <div class="row about-al p-relative">
        <div class="benifits-overlay"></div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ben-pad">
            <div class="benefits-title col-xs-12 no-padding">
                <h2>
                    About Truckka
                </h2>
            </div>
            <div class="benefits-info col-xs-12 no-padding">
                
                <div class="col-sm-12 col-xs-12 pad-left">
                    <div class="benefits-info1">
                        
                        <p>
                        TRUCKKA Logistics Limited is a fourth-party logistics (4PL) tech-solutions company. 
                        We leverage our resources and capabilities with those of our partners to provide logistics services and tech-enabled solutions within the logistics supply chain.  
                        </p>
                    </div>
                </div>


                <div class="col-sm-12 col-xs-12 pad-left">
                    <div class="benefits-info1 margin-top-40">
                        
                        <p>
                        We are a team of professionals with rich and diverse corporate executive background with a focus to 
                        create an efficient marketplace for the logistics industry in Nigeria. 
                        </p>
                    </div>
                </div>


                <div class="col-sm-12 col-xs-12 pad-left">
                    <div class="benefits-info1 margin-top-40">
                        
                        <p>
                        We are therefore positioned as your go-to logistics solutions provider in a
                         difficult and infrastructure-challenged environment where players yearn for safety,
                          price-efficiency, professionalism and trust - our core-value proposition. 
                        </p>
                    </div>
                </div>


            </div>
            
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding">
            <div class="benifits-bg-im-22">
           
            </div>
        </div>
    </div>
</div>
<!--End of it-->


<!--Mission and vision-->
<div class="container-fluid" id="about">
    <div class="row about-al">
        <div class="container">
            <div class="row">
                <div class="about-title">
                    <h2>
                        VISION / MISSION
                    </h2>
                    <p class="about-p">
                    To build an efficient digital marketplace that matches owners of freight to truck services on demand. 
                     We equally provide enterprise logistics services within chosen segments.
                    </p>
                </div>
                <div class="about-btn">
                   
                </div>
            </div>
        </div>
    </div>
</div>
<!--end of mission and vision-->




<!--Mission and vision-->
<div class="container-fluid" id="about">
    <div class="row about-al">
        <div class="container">
            <div class="row">
                <div class="about-title">
                    <h2>
                	OUR CUSTOMER SEGMENTS
                    </h2>
                    
                    <img src="{{URL::to('home_assets/assets/images/truckka-segment.png')}}" style="width:100%;">
                    
                </div>
                
            </div>
        </div>
    </div>
</div>
<!--end of mission and vision-->



<!--How it works-->
<div class="container-fluid" id="howitworks">
    <div class="row about-al p-relative">
        <div class="benifits-overlay2"></div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ben-pad">
            <div class="benefits-title2 col-xs-12 no-padding">
                <h2>
                	OUR SERVICE PROMISE / QUALITY
                </h2>
            </div>
            <div class="benefits-info col-xs-12 no-padding">
                
                <div class="col-sm-10 col-xs-12 pad-left">
                    <div class="benefits-info1">
                        <h3> <img src="{{URL::to('home_assets/assets/images/good-mark.png')}}" style="width: 30px;"> 
                    	We will strive to match your freight / truck or refund your money within 48hours if no match</h3>
                       
                    </div>
                </div>
            </div>
            <div class="benefits-info col-xs-12 no-padding">
               
                <div class="col-sm-10 col-xs-12 pad-left">
                    <div class="benefits-info1">
                        <h3><img src="{{URL::to('home_assets/assets/images/good-mark.png')}}" style="width: 30px;"> 	We will act professionally and courteously always</h3>
                        
                    </div>
                </div>
            </div>
            <div class="benefits-info col-xs-12 no-padding">
                
                <div class="col-sm-10 col-xs-12 pad-left">
                    <div class="benefits-info1">
                        <h3><img src="{{URL::to('home_assets/assets/images/good-mark.png')}}" style="width: 30px;">  We will seek to give you peace of mind and invest more on your core-business </h3>
                        
                    </div>
                </div>
            </div>
            
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding">
            
        </div>
    </div>
</div>
<!--How it works-->


<!--Board members-->
@include('exchange.home.board-members')
<!--end of Board members--> 


@endsection