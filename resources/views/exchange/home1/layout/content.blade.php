
<!DOCTYPE html>
<html>

<meta http-equiv="content-type" content="text/html;charset=utf-8" />

<head>
    <title>Truckka</title>
    <meta charset="utf-8" />
    <meta name="keywords" content="" />

    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />
    <meta name="format-detection" content="telephone=no">
        <link rel="canonical" href="{{url('/')}}" />
    
    <link rel="shortcut icon" type="image/x-icon" href="">
    <link href='http://fonts.googleapis.com/css?family=Nunito' rel='stylesheet'>
    <link rel="stylesheet" href="{{URL::to('exchange/home_assets/assets/css-font/v5.1.0/css/all.css')}}" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    <link href="{{URL::to('exchange/home_assets/assets/css/bootstrap.min.css')}}" rel="stylesheet" />
    <link href="{{URL::to('exchange/home_assets/assets/css/datepicker.css')}}" rel="stylesheet" />
    <link href="{{URL::to('exchange/home_assets/assets/css/media.css')}}" rel="stylesheet" />
    <link href="{{URL::to('exchange/home_assets/assets/css/bootstrap-multiselect.css')}}" rel="stylesheet" />
    <link rel="stylesheet" href="{{URL::to('exchange/home_assets/assets/ajax/libs/toastr.js/latest/css/toastr.min.css')}}" />
    <link href="{{URL::to('exchange/home_assets/assets/css/style.css')}}" rel="stylesheet" />
    <script src="{{URL::to('exchange/dashboard_assets/assets/js/jquery.min.js')}}"></script>
    
    <script>
    $( function() {
        $( "#datepicker" ).datepicker();
    } );
    </script>


</head>
<body>
    <div ng-app="truckQuote">
        <header>
            <div class="container-fluid banner-header">
                <div class="row">
                    <div class="col-sm-5 col-xs-12 logo-section">
                        <div class="logo">
                            <a href="{{url('/')}}"><img src="{{URL::to('exchange/dashboard_assets/assets/images/logo.png')}}" alt="Truckka"></a>
                        </div>
                    </div>
                    <div class="col-sm-7 col-xs-12">
                        <div class="right-info-section col-xs-12 no-padding">
                            <div class="right-info">

                                    <p><span><img class="phone-icon-right" src="{{URL::to('exchange/home_assets/assets/images/phone-icon-11-256.png')}}" alt="Phone"></span>070 300 533 09</p>
                                    <a href="{{url('login')}}" id="IndexLogin" class="right-info-link1 loginClr">LOGIN</a>
                                    <a href="{{url('register')}}"  id="signup" class="right-info-link2 signUpClr">SIGN UP</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>

         <script>
            $(document).ready(function() {


                $('#i_can_give_truck').hide();
                $('#i_need_truck').hide();

                $('.i_need_truck').click(function() {
                    $('#i_need_truck').show();
                    $('#truckka-options').hide();
                });

                 $('.i_can_give_truck').click(function() {
                    $('#i_can_give_truck').show();
                    $('#truckka-options').hide();
                });

                $('#backtooptions').click(function() {
                    $('#i_need_truck').hide();
                    $('#i_can_give_truck').hide();
                    $('#truckka-options').show();
                });

                $('#backtooptions2').click(function() {
                    $('#i_need_truck').hide();
                    $('#i_can_give_truck').hide();
                    $('#truckka-options').show();
                });


            });
        </script>

        <script>
            $(document).ready(function() {
                $('#standardOrigin').on('change', function() {

                    var val = this.value;
                    var _token = $("input[name='_token']").val();

                    document.getElementById('origin2').value = val;
                    var original = document.getElementById('origin2').value;

                    if (original === '') {
                        alert('Select a location');
                    }else if(original !== '')
                    {


                        var APP_URL = {!! json_encode(url('/exchange/')) !!}

                    $.ajax({

                        url: APP_URL+"/checkOrigins",

                        type:'POST',

                        data: {_token:_token, originToServer:original},

                        success: function(data) {

                            if($.isEmptyObject(data.error)){

                                var length = data.success;
                                var trueLength = length.length;

                                if(trueLength > 0){
                                    var catOptions = "";
                                    for(var i = 0; i < trueLength; i++) {

                                        var company = data.success[i];

                                        catOptions += "<option value="+ company +">" + company + "</option>";
                                    }

                                    document.getElementById("destinationSelect").innerHTML = catOptions;

                                }else{
                                    alert("nothing");
                                }



                            }else{

                                alert('error');

                            }

                        }

                    });
                }
            })
            });
        </script>

        <script>
            $(document).ready(function() {

                $("#get_quote").css("display", "none");

                $(".getQuoteButton").unbind('click').click(function(e){

                    e.preventDefault();

                    $("#i_need_truck").css("display", "none");
                    $("#truckka-options").css("display", "none");

                    $("#get_quote").fadeIn("slow");

                    e.preventDefault();

                    var APP_URL = {!! json_encode(url('/exchange/')) !!}

                $.ajax({

                    async:false,

                    processData: false,

                    contentType: false,

                    url: APP_URL+"/getQuote",

                    type:'POST',

                    data: new FormData($("#getQuoteForm")[0]),

                    success: function(data) {

                        if($.isEmptyObject(data.error)){

                            var origin                  = $('#standardOrigin').find(":selected").text();
                            var destination             = $('#destinationSelect').find(":selected").text();
                            var truck_number            = $('#number_of_truck').val();
                            var description             = $('#description').val();
                            var pickupDate              = $('#datepicker').val();
                            var contactPhone            = $('#contact_phone').val();
                            var pickupAddress           = $('#pickup_address').val();

                            $("#description").html(data.success.description);
                            $("#originSummary").html(origin);
                            $("#destinationSummary").html(destination);
                            $("#truckTypeSummary").html(data.success.truckTypeDescription);
                            $("#numberOfTruckSummary").html(truck_number);
                            $("#priceSummary").html(data.success.amount);
                            $("#vatSummary").html(data.success.vat);
                            $("#totalSummary").html(data.success.total);

                            $("#routeIDForm").val(data.success.route_id);
                            $("#truckTypeIDForm").val(data.success.truckTypeID);
                            $("#truckNumberForm").val(truck_number);
                            $("#itemDescriptionForm").val(description);
                            $("#pickUpDateForm").val(pickupDate);
                            $("#totalAmtForm").val(data.success.total);
                            $("#pickupAddressForm").val(pickupAddress);
                            $("#contactPhoneForm").val(contactPhone);

                        }else{

                            alert(data.error);

                        }

                    }

                });

            });

            });
        </script>
        

@yield('content')	
	
@extends('exchange.home.layout.footer')
