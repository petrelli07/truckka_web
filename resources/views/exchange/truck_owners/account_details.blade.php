@extends('exchange.truck_owners.layout.content')
@section('content')

<div class="row">
    <div class="box col-md-12">
        <!--if @ session -->
        @if (session('message'))
        <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert">x</button>
            {{ session('message') }}
        </div>
        @endif
        <!--endif-->

        <!--if @ errors -->
        @if ($errors->has('errors'))
        <div class="alert alert-danger" role="alert">
            <button type="button" class="close" data-dismiss="alert">x</button>
            @foreach ($errors->all() as $error)
            <div>{{ $error }}</div>
            @endforeach
        </div>
        @endif
        <!--endif-->

        <form role="form" method="post" class="validate"  action="{{url('/exchange/truck_owner/save_account_details')}}"  enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="box-inner">
                <div class="box-header well" data-original-title="">
                    <h2><i class="glyphicon glyphicon-plus"></i> Account Details</h2>

                </div>
                <div class="box-content row">


                    <div class="box col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="truck_plate_number">Select Bank</label>
                            <select class="form-control required" name="bank_id" required>
                                <option value="">[-SELECT BANK-]</option>
                                @foreach($banks as $bank)
                                <option value="{{$bank->id}}">{{$bank->bank_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="box col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="driver_name">Account Number</label>
                            <input type="text" name="account_number" class="form-control required" required placeholder="Account Number">
                        </div>
                    </div>

                    <div class="box col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="driver_name">Account Name</label>
                            <input type="text" name="account_name" class="form-control required" required placeholder="Account Name">
                        </div>
                    </div>


                    <br /><br />


                </div>
            </div>
            <div class="box col-md-6">
                <div class="form-group">
                    <button type="submit" class="btn btn-custom">
                        <i class="glyphicon glyphicon-plus"></i>Submit
                    </button>
                </div>
            </div>
        </form>
    </div>
    <!--/span-->

</div><!--/row-->



@endsection