@extends('exchange.truck_owners.layout.content')
@section('content')

<div class="row">
    <div class="box col-md-12">
    <div class="box-inner">
    <div class="box-header well" data-original-title="">
        <h2><i class="glyphicon glyphicon-eye-open"></i> View Order Details</h2>
    </div>
    <div class="box-content">
        <div class="row">

            <div class="col-md-4">
                <b>Pickup State:</b><br/>
                {{$details['origin']}}
                <br/>
                <br/>
            </div>
            <div class="col-md-4">
                <b>Destination State:</b><br/>
                {{$details['destination']}}
                <br/>
                <br/>
            </div>
            <div class="col-md-4">
                <b>Pickup Date:</b><br/>
                {{$details['pickup_date']}}
                <br/>
                <br/>
            </div>

        </div>

        <div class="row">

            <div class="col-md-4">
                <b>Pickup Address:</b><br/>
                {{$details['pickup_address']}}
                <br/>
                <br/>
            </div>
            <div class="col-md-4">
                <b>Item Description:</b><br/>
                {{$details['item_description']}}
                <br/>
                <br/>
            </div>
            <div class="col-md-4">
                <b>Contact Phone Number:</b><br/>
                {{$details['contact_phone']}}
                <br/>
                <br/>
            </div>

        </div>

    </div>
    </div>
    </div>
    <!--/span-->

    </div>

@endsection