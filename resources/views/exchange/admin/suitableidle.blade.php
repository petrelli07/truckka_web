@extends('exchange.admin.layout.content')
@section('content')


<div class="row">
    <div class="box col-md-12">
    <div class="box-inner">
    <div class="box-header well" data-original-title="">
        <h2><i class="glyphicon glyphicon-eye-open"></i> All IDLE Capacity</h2>
    </div>
    <div class="box-content">
    <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
    <thead>

    <tr>
        
        <th>Id</th>
        <th>Truck Supplier</th>
        <th>Number of Truck</th>
        <th>Action</th>
       
        
       
    </tr>
    </thead>
    <tbody>
    <?php $x =1; ?>
    @forelse($idle_capacity_details as $request)
    <tr>
        <td class="center">{{$x++}}</td>
        <td class="center">{{$request->user_name}}</td>
        <td class="center">{{$request->number_of_truck}}</td>

          <td class="center">
              @if($request->status == 1)
                     
              <button class="btn btn-success btn-sm view-shop">Already matched </button>
                @else
                <form method="post" action="{{url('exchange/admin/matching-suitable-idle')}}">
                    {{csrf_field()}}
                    <input type="hidden" name="user_id" value="{{$request->user_id}}">
                    <input type="hidden" name="exchange_order_id" value="{{$request->exchange_order_id}}">
                    <input type="hidden" name="exchange_status_id" value="{{$request->exchange_status_id}}">
                    <input type="hidden" name="idle_id" value="{{$request->id}}">
                    <button class="btn btn-success btn-sm view-shop">Match IDLE</button>
                </form>
           
                @endif
            </td>
    
              
    </tr>
    @empty
   
    @endforelse
    </tbody>
    </table>
    </div>
    </div>
    </div>
    <!--/span-->



@endsection