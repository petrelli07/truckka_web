@extends('exchange.admin.layout.content')
@section('content')

<div class="row">
    <div class="box col-md-12">
        <!--if @ session -->
        <!--<div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert">x</button>
               success
        </div>-->
        <!--endif-->

        <!--origin to server form-->
        <form id="originForm" style="display:none;">
            {{csrf_field()}}
            <input type="hidden" id="origin2" name="originToServer">
        </form>
        <!--end origin to server form-->

        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-plus"></i> Create New Orders</h2>

            </div>
            <div class="box-content row">
                <!--if has session message-->
                @if (session('message'))
                <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    {{ session('message') }}
                </div>
                @endif
                <!--endif-->
                <form role="form" method="post" class="validate"  action="{{url('/exchange/admin/route/submit_new_route')}}"  enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="box col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="origin">Origin</label>
                            <div class="controls">
                                <select id="standardOrigin" class="form-control col-md-12" name="origin" required>
                                    <option value=''>[-Origin-]</option>
                                    @forelse($states as $state)
                                    <option value="{{$state->id}}">{{$state->state_description}}</option>
                                    @empty
                                    @endforelse
                                </select>
                            </div>
                            @if ($errors->has('origin'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('origin') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>


                    <div class="box col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="destination">Destination</label>
                            <div class="controls">
                                <select id="destinationSelect" class="col-md-12 form-control" name="destination" required>
                                    <option value=''>[-Destination-]</option>
                                    @forelse($states as $state)
                                    <option value="{{$state->id}}">{{$state->state_description}}</option>
                                    @empty
                                    @endforelse
                                </select>
                            </div>
                            @if ($errors->has('destination'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('destination') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>


                    <div class="box col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="Type of cargo">Customer Base Price</label>
                            <div class="controls">
                                <input type="text" class="col-md-12 form-control price" id="price" name="customer_base_price" required/>
                            </div>
                            @if ($errors->has('customer_base_price'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('customer_base_price') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>


                    <div class="box col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="Type of cargo">Customer Service Charge</label>
                            <div class="controls">
                                <input type="text" class="col-md-12 form-control price" id="price" name="customer_service_charge" required/>
                            </div>
                            @if ($errors->has('customer_service_charge'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('customer_service_charge') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>


                    <div class="box col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="Type of cargo">Transporter Base Fare</label>
                            <div class="controls">
                                <input type="text" class="col-md-12 form-control price" id="price" name="transporter_base_price" required/>
                            </div>
                            @if ($errors->has('transporter_base_price'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('transporter_base_price') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>


                    <div class="box col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="Type of cargo">Transporter Service Charge</label>
                            <div class="controls">
                                <input type="text" class="col-md-12 form-control price" id="price" name="transporter_service_charge" required/>
                            </div>
                            @if ($errors->has('transporter_service_charge'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('transporter_service_charge') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>


                    <div class="box col-md-6">

                    </div>


                    <div class="box col-md-6">
                        <div class="form-group">
                            <button type="submit" class="btn btn-custom">
                                <i class="glyphicon glyphicon-plus"></i>Submit
                            </button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
    <!--/span-->

</div><!--/row-->



@endsection