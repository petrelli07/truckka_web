@extends('exchange.admin.layout.content')
@section('content')


<div class="row" xmlns="http://www.w3.org/1999/html">
    <div class="box col-md-12">

        <!--if @ session -->
        @if (session('message'))
        <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert">x</button>
            {{ session('message') }}
        </div>
        @endif

        <!--endif-->

        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-plus"></i> View Truck Details</h2>

            </div>
            <div class="box-content row">

                <div class="box col-md-12">
                    <div>
                        <a href='{{url("/exchange/admin/add_response/{$reference_number}")}}' class="btn btn-sm btn-info">Add Responses</a>
                    </div>
                    <div class="box-content">

                        <table class="table table-striped table-bordered  responsive">
                            <thead>
                            <tr>
                                <th>Plate Number</th>
                                <th>Driver Name</th>
                                <th>Driver Phone</th>
                                <th>Company Name</th>
                            </tr>
                            </thead>
                                <tbody>
                                @forelse($responses as $response)
                                <tr>
                                    <td class="center">{{$response->plate_number}}</td>
                                    <td class="center">{{$response->driver_name}}</td>
                                    <td class="center">{{$response->driver_phone_number}}</td>
                                    <td class="center">{{$response->name}}</td>
                                </tr>
                                @empty
                                <tr>

                                </tr>
                                @endforelse
                                </tbody>
                        </table>
                    </div>

                </div>






            </div>
        </div>
    </div>
    <!--/span-->

</div><!--/row-->



@endsection