@extends('exchange.admin.layout.content')
@section('content')


<div class="row">
    <div class="box col-md-12">
        <!--if @ session -->
        @if (session('message'))
        <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert">x</button>
            {{ session('message') }}
        </div>
        @endif
        <!--endif-->
    <div class="box-inner">
    <div class="box-header well" data-original-title="">
        <h2><i class="glyphicon glyphicon-eye-open"></i> All Orders</h2>
    </div>
    <div class="box-content">
    <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
    <thead>

    <tr>
        
        <th>Id</th>
        <th>Order Number</th>
        <th>Origin/Pickup Address</th>
        <th>Destination/Delivery Address</th>
        <th>Type of Truck (Number)</th>
        <th>Ordered By</th>
        <th>Pick Up date</th>
        <th>Action</th>
       
        
       
    </tr>
    </thead>
    <tbody>
    <?php $x =1; ?>
    @forelse($exchange_order_details as $request)
    <tr>
        <td class="center">{{$x++}}</td>
        <td class="center">{{$request->order_number}}</td>
        <td class="center">{{$request->originStateName}}</td>
        <td class="center">{{$request->destStateName}}</td>
        <td class="center">{{$request->number_of_trucks}}</td>
        <td class="center">{{$request->user_name}}</td>
        <td class="center">{{$request->pickup_date}}</td>

        <td class="center">
            @if($request->status_description === "Pending")
               <!--<a href='{{url("/exchange/admin/confirm_order/{$request->order_number}")}}' class="btn-success btn btn-sm">
                   Confirm Order
               </a>-->
            {{$request->status_description}}
            @elseif($request->status_description === "Payment Pending")
                {{$request->status_description}}
            @elseif($request->status_description === "Match in Progress" || $request->status_description === "Order Matched to Trucks")
            <a href='{{url("/exchange/admin/match_transporter/{$request->order_number}")}}' class="btn-success btn btn-sm">
                Match to Transporter
            </a>
            @endif
        </td>
        
    </tr>
    @empty
    
    @endforelse
    </tbody>
    </table>
    </div>
    </div>
    </div>
    <!--/span-->




@endsection