@extends('exchange.client.layout.content')
@section('content')

<div class="row" xmlns="http://www.w3.org/1999/html">
    <div class="box col-md-12">
        <!--if @ session -->
        @if (session('message'))
        <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert">x</button>
            {{ session('message') }}
        </div>
        @endif
        <!--endif-->

        <!--origin to server form-->
        <form id="originForm" style="display:none;">
            {{csrf_field()}}
            <input type="hidden" id="origin2" name="originToServer">
        </form>
        <!--end origin to server form-->

        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-plus"></i> Create New Orders</h2>

            </div>
            <div class="box-content row">
                 <form role="form" method="post" class="validate"  action="{{url('/exchange/customer/client-area/submit_new_order')}}"  enctype="multipart/form-data">
                     {{csrf_field()}}
                 <div class="box col-md-6">
                    <div class="form-group">
                        <label class="control-label" for="origin">Origin</label>
                        <div class="controls">
                            <select id="standardOrigin" class="form-control col-md-12" name="origin">
                            <option value=''>[-Origin-]</option>
                                @for($x=0; $x < count($uniqueOrigins); $x++)
                                    <option>{{$uniqueOrigins[$x]}}</option>
                                @endfor
                            </select>
                        </div>
                        @if ($errors->has('origin'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('origin') }}</strong>
                                </span>
                        @endif
                    </div>
                </div>


                <div class="box col-md-6">
                    <div class="form-group">
                        <label class="control-label" for="destination">Destination</label>
                        <div class="controls">
                            <select id="destinationSelect" class="col-md-12 form-control" name="destination">

                            </select>
                        </div>
                        @if ($errors->has('destination'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('destination') }}</strong>
                                </span>
                        @endif
                    </div>
                </div>



                 <div class="box col-md-6">
                     <div class="form-group">
                         <label class="control-label" for="Type of cargo">Pickup Address</label>
                         <div class="controls">
                             <input class="col-md-6 form-control" id="pickup_address"  name="pickup_address" placeholder="Enter Pickup Address"  type="text" required/>
                              <input type="" hidden="hidden" name="pickup_placeId" id="pickup_placeId">
                         </div>
                         @if ($errors->has('pickup_address'))
                            <span class="help-block">
                                <strong>{{ $errors->first('pickup_address') }}</strong>
                            </span>
                         @endif
                     </div>
                 </div>

                 <div class="box col-md-6">
                     <div class="form-group">
                         <label class="control-label" for="Type of cargo">Delivery Address</label>
                         <div class="controls">
                              <input type="text" class="col-md-6 form-control"  name="delivery_address" placeholder="Enter Destination Address"  type="text" id="delivery_address" required>
                              <input type="" hidden="hidden" name="delivery_placeId" id="delivery_placeId">
                         </div>
                         @if ($errors->has('delivery_address'))
                            <span class="help-block">
                                <strong>{{ $errors->first('delivery_address') }}</strong>
                            </span>
                         @endif
                     </div>
                 </div>

                 <div class="box col-md-4">
                     <div class="form-group">
                         <label class="control-label" for="Type of cargo">Type of Truck</label>
                         <div class="controls">
                             <select id="selectError" class="col-md-12 form-control" name="truckType" required>
                                 <option value="">[-Select Truck Type-]</option>
                                 @foreach($truckTypes as $truckType)
                                 <option value="{{$truckType->id}}">{{$truckType->truck_type_description}}</option>
                                 @endforeach
                             </select>
                         </div>
                         @if ($errors->has('truckType'))
                            <span class="help-block">
                                <strong>{{ $errors->first('truckType') }}</strong>
                            </span>
                         @endif
                     </div>
                 </div>

                     <div class="box col-md-4">
                         <div class="form-group">
                             <label class="control-label" for="Type of cargo">Truck Capacity</label>
                             <div class="controls">
                                 <select id="selectError" class="col-md-12 form-control" name="truckCapacity" required>
                                     <option value="">[-Truck Capacity-]</option>
                                     <option value="30 Tonnes">30 Tonnes</option>
                                     <option value="40 Tonnes">40 Tonnes</option>
                                     <option value="50 Tonnes">50 Tonnes</option>
                                 </select>
                             </div>
                             @if ($errors->has('truckCapacity'))
                            <span class="help-block">
                                <strong>{{ $errors->first('truckCapacity') }}</strong>
                            </span>
                             @endif
                         </div>
                     </div>


                 <div class="box col-md-4">
                    <div class="form-group">
                            <label class="control-label" for="Type of cargo">Number of Trucks</label>
                        <div class="controls">
                            <input type="text" class="col-md-12 form-control" name="number_of_truck" required/>
                        </div>
                        @if ($errors->has('number_of_truck'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('number_of_truck') }}</strong>
                                </span>
                        @endif
                    </div>
                </div>



                     <div class="box col-md-6">
                         <div class="form-group">
                             <label class="control-label" for="Type of cargo">Pickup Date</label>
                             <div class="controls">
                                 <input type="text" id="datepicker" class="col-md-6 form-control" name="pickup_date" placeholder="Enter a Pickup Date"/>
                             </div>
                             @if ($errors->has('pickup_date'))
                        <span class="help-block">
                            <strong>{{ $errors->first('pickup_date') }}</strong>
                        </span>
                             @endif
                         </div>
                     </div>

                 <div class="box col-md-6">
                    <div class="form-group">
                            <label class="control-label" for="Type of cargo">Description of Cargo</label>
                        <div class="controls">
                            <textarea  class="col-md-6 form-control" name="cargo_description" placeholder="Please Describe the Items to be Moved"></textarea>
                        </div>
                        @if ($errors->has('cargo_description'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('cargo_description') }}</strong>
                                </span>
                        @endif
                    </div>
                </div>


                <div class="box col-md-12">
                    <div class="form-group">
                        <button type="submit" class="btn btn-custom">
                            <i class="glyphicon glyphicon-plus"></i>Submit
                        </button>
                    </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
    <!--/span-->

</div><!--/row-->



@endsection