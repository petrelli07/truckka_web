@extends('exchange.client.layout.content')
@section('content')

<div class="row">
    <div class="box col-md-12">
    <div class="box-inner">
    <div class="box-header well" data-original-title="">
        <h2><i class="glyphicon glyphicon-eye-open"></i> My Orders</h2>
    </div>
    <div class="box-content">
        <!--if @ session -->
        @if (session('message'))
        <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert">x</button>
            {{ session('message') }}
        </div>
        @endif
        <!--endif-->
    <div style="overflow-x:auto;">
    <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
    <thead>

    <tr>
        
        <th>Reference Number</th>
        <th>Origin/Pickup Address</th>
        <th>Destination/Delivery Address</th>
        <th>Number of Trucks/Type</th>
        <th>Amount</th>
        <!--<th>Status</th>-->
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    @forelse($user as $users)
    <tr>
        <td class="center">{{$users->order_number}}</td>
        <td class="center">{{$users->originStateName}} / {{$users->pickup_address}}</td>
        <td class="center">{{$users->destStateName}} / {{$users->delivery_address}}</td>
        <td class="center">{{$users->number_of_trucks}} / {{ $users->truck_type_description}} Truck(s)</td>
        <td class="center">&#x20A6;{{($users->customer_base_price + $users->customer_service_charge) * $users->number_of_trucks}} </td>
        <!--<td class="center">
        {{$users->status_description}}
        </td>-->
        <td class="center">
            @if($users->status_description == "Pending")
            <a class="btn btn-info btn-sm" href='{{url("/exchange/customer/invoice_details/{$users->order_number}")}}'>
                View Invoice
            </a>
            @elseif($users->status_description == "Match in Progress")
                {{$users->status_description}}
            @elseif($users->status_description == "Order Matched to Trucks")
            <a href='{{url("/exchange/customer/view_truck_details/{$users->order_number}")}}'>View Truck Details</a> | <a class="btn btn-info btn-sm" href='{{url("/exchange/customer/invoice_details/{$users->order_number}")}}'>
                View Invoice
            </a>
            @endif
            <!--@if($users->status_description == "Match in Progress")
            <a class="btn btn-info btn-sm" href='{{url("/exchange/customer/invoice_details/{$users->reference_number}")}}'>
                View Invoice
            </a>
            @elseif($users->status_description == "Order to Supplier")
            <a class="btn btn-info btn-sm" href='{{url("/exchange/customer/invoice_details/{$users->reference_number}")}}'>
                View Invoice
            </a>
            @elseif($users->status_description == "Payment Pending")
            <a class="btn btn-info btn-sm" href='{{url("/exchange/customer/invoice_details/{$users->reference_number}")}}'>
                Pay Invoice
            </a>
            @elseif($users->status_description == "Invoice Paid")
            <a class="btn btn-info btn-sm" href='{{url("/exchange/customer/client-area/complete_order/{$users->reference_number}")}}'>
                Complete Order
            </a>
            @elseif($users->status_description == "Order Matched to Trucks")
            <a href='{{url("/exchange/customer/view_truck_details/{$users->order_number}")}}'>View Truck Details</a> | <a class="btn btn-info btn-sm" href='{{url("/exchange/customer/invoice_details/{$users->reference_number}")}}'>
                View Invoice
            </a>
            @endif-->
        </td>
    </tr>
    @empty
    <tr>
        <td class="center"></td>
        <td class="center"></td>
        <td class="center"></td>
        <td class="center"></td>
        <td class="center"></td>
        <td class="center"></td>
    </tr>
    @endforelse
    </tbody>
    </table>
</div>
    </div>
    </div>
    </div>
    </div>
    <!--/span-->

    </div>



@endsection