@extends('exchange.client.layout.content')
@section('content')

<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-eye-open"></i> View Order Details</h2>
            </div>
            <div class="box-content">
                <div class="row">


                    <div class="col-md-4">
                        <b>Origin:{{$array['origin']}}</b><br/>
                        <br/>
                        <br/>
                    </div>
                    <div class="col-md-4">
                        <b>Destination:{{$array['destination']}}</b><br/>
                        <br/>
                        <br/>
                    </div>
                    <div class="col-md-4">
                        <b>Pickup Date:{{$array['pickupdate']}}</b><br/>
                        <br/>
                        <br/>
                    </div>

                </div>

                <div class="row">

                    <div class="col-md-4">
                        <b>Amount:{{$array['amount']}}</b><br/>
                        <br/>
                        <br/>
                    </div>
                    <div class="col-md-4">
                        <b>VAT:{{$array['vat']}}</b><br/>
                        <br/>
                        <br/>
                    </div>
                    <div class="col-md-4">
                        <b>Total Amount:{{$array['total']}}</b><br/>
                        <br/>
                        <br/>
                    </div>

                </div>

                <form method="post" action="{{url('/exchange/customer/client-area/process-order')}}">
                    {{csrf_field()}}
                    <input type="hidden" name="truckNumber" value="{{$array['truckNumber']}}">
                    <input type="hidden" name="truckTypeID" value="{{$array['truckTypeID']}}">
                    <input type="hidden" name="pickupDate" value="{{$array['pickupdate']}}">
                    <input type="hidden" name="itemDescription" value="{{$array['description']}}">
                    <input type="hidden" name="exchangeRouteID" value="{{$array['route_id']}}">
                    <input type="hidden" name="pickupAddress" value="{{$array['pickupaddress']}}">
                    <input type="hidden" name="contactPhone" value="{{$array['contactPhone']}}">
                    <input type="hidden" name="totalAmount" value="{{$array['total']}}">
                    <button type="submit" class="btn btn-md btn-success">Continue to Payment</button>
                </form>

            </div>
        </div>
    </div>
    <!--/span-->

</div>

@endsection