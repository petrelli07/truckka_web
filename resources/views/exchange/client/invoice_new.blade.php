@extends('exchange.client.layout.content')
@section('content')
<link href="{{URL::to('home_assets/assets/css/invoice.css')}}" rel="stylesheet" />


<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <!-- <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-eye-open"></i> Invoices</h2>
            </div> -->
            <div class="box-content">
            <img src="{{URL::to('dashboard_assets/assets/images/logo2.png')}}" class="hidden-xs" style="width:300px;margin-bottom:10px;" alt="Truckka"/> </a>

                <table class="neworder">

                    <thead class="theadorder">
                    
                 
                    <tr class="tr_order">
                        <th class="td_order td_bold"  colspan="1">BILL To</th>
                        <th class="td_order td_bold"  colspan="2">INVOICE INFORMATION</th>
                        <!-- <th class="td_order td_bold"  colspan="2">PAYMENT INSTRUCTION</th> -->
                    </tr>

                    <tr class="tr_order">
                        <th class="td_order text-capitalize"  colspan="1" style="border-bottom: 1px solid #fff;">
                            <p>{{$data['bill_to']}}</p>
                            <p>{{$data['address']}}</p>
                        </th>
                        <th class="td_order"  colspan="1">Invoice Date</th>
                        <th class="td_order"  colspan="1">Invoice Number</th>
                        <!-- <th class="td_order"  colspan="1">Bank</th> -->
                        <!-- <th class="td_order"  colspan="1">Account Name</th>
                        <th class="td_order"  colspan="1">Account No</th> -->
                    </tr>

                    <tr class="tr_order">
                        <td class="td_order" colspan="1">
                           
                        </td>

                        <td class="td_order" colspan="1">
                            <strong>{{$data['date']}}</strong>
                            
                        </td>
                        <td class="td_order" colspan="1">
                            <strong>{{$data['invoice_number']}} </strong><br>
                            
                        </td>
                        <!-- <td class="td_order" colspan="1">
                            <strong>Invoice No: </strong>121212<br>
                            
                        </td> -->

                        <!-- <td class="td_order" colspan="1">
                            <strong>Invoice No: </strong>121212<br>
                            
                        </td>

                        <td class="td_order" colspan="1">
                            <strong>Invoice No: </strong>121212<br>
                            
                        </td> -->
                    </tr>

                    </tr>
                    
                    </thead>
                    </table>
                    <table class="neworder">
                
                    <thead class="theadorder">
                    <tbody>
                    <tr class="tr_order bgtable">
                        <th class="td_order">DATE</th>
                        <th class="td_order">ORIGIN-DESTINATION</th>
                        <th class="td_order">TRUCK #</th>
                        <th class="td_order">ORDER #</th>
                        <th class="td_order">CUSTOMER NAME</th>
                        <th class="td_order">QTY</th>
                        <th class="td_order">AMOUNT</th>
                    </tr>
                    <tr class="tr_order">
                        <td class="td_order">{{$data['date']}} </td>
                        <td class="td_order">{{$data['origin']}} - {{$data['destination']}}</td>
                        <td class="td_order">{{$data['truck_type_description']}}</td>
                        <td class="td_order">{{$data['order_number']}}</td>
                        <td class="td_order text-capitalize">{{$data['bill_to']}}</td>
                        <td class="td_order">{{$data['number_of_trucks']}}</td>
                        <td class="td_order">&#x20A6;{{$data['unit_price']}}</td>
                    </tr>
                    </tbody>
                    <tfoot>
                    <tr class="tr_order">
                        <th class="td_order" colspan="5"  style="text-align: right;border-bottom: 1px solid #fff;">
                        <span >Total Amount Due Immediately (in Words):<br /></span></th>
                        

                         <th class="th_order" colspan="1">
                        <span >Amount<span></th>
                        <td class="td_order"> &#x20A6;{{$data['unit_price'] * $data['number_of_trucks']}}</td>
                    </tr>

                     <tr class="tr_order">
                        <th class="td_order" colspan="5"  style="text-align: right;border-bottom: 1px solid #fff;font-weight:200;">
                        <p class="text-capitalize"><?php $digit = new NumberFormatter("en", NumberFormatter::SPELLOUT); echo $digit->format(($data['unit_price'] + $data['service_charge']) * $data['number_of_trucks']);?> Naira Only</p>
                    </th>
                        

                         <th class="th_order" colspan="1">
                        <span >Service Charge<span></th>
                        <td class="td_order"> &#x20A6;{{$data['service_charge'] * $data['number_of_trucks'] }}</td>
                    </tr>

                     <tr class="tr_order">
                        <th class="td_order" colspan="5"  >
                        </th>
                        

                         <th class="th_order" colspan="1" >
                        <span >Total Amount<span></th>
                        <td class="td_order"> &#x20A6;{{ ($data['unit_price'] + $data['service_charge']) * $data['number_of_trucks']}}</td>
                    </tr>
                    

                    </tfoot>
                </table>

                <div class="row">
                    <div class="col-md-2 div-right">   @if($data['status'] === 'Pending')
                        <a class="btn btn-block" style="background-color: #F9A81A
;color: #000000;" href='{{url("/exchange/customer/client-area/pay_invoice/{$data["invoice_number"]}")}}'>Pay Now</a>
                        @endif
                    </div>
                    <div class="col-md-2 div-right">
                        <a class="btn btn-block" style="background-color: #000000;" href='{{url("exchange/customer/update_customer")}}'>Back</a>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6 div-left" style="font-size: 10px; border:0.5px solid; margin-bottom: 10px; margin-left: 10px; padding-top: 10px;">
                        <p>* This invoice is valid for a period of 72 Hours after which the pricing is subject to change without notice.</p>
                        <p>** Where trucks are recalled because customer fails to provide freight in a timely manner, the customer will be refunded less a demurrage charge.</p>
                        <p>*** Each freight movement is covered in line with order request. Only freight paid for will be covered by our GIT Insurance and our support system.</p>
                        <p>**** Engagement with the driver or organization outside the terms referenced in this invoice is strictly at the risk of the customer.</p>
                    </div>
                </div>

                <div class="bgtablefoot">
                    <span>#6 Prince Bode Oluwo Street Mende Maryland, Lagos | +234 (0) 903 000 2705 | www.truckka.ng</span>
                </div>

                <div class="bgtablefoot2">
                    <span>Thank you for your Business</span>
                    <br />
                    <span>For: Truckka Logistics Limited</span>
                </div>
                
               
            </div>
        </div>
    </div>
    <!--/span-->

</div>


@endsection