@extends('registry.fieldAgent.layout.content')
@section('content')



<div class="row">
    <div class="box col-md-12">
    <div class="box-inner">
    <div class="box-header well" data-original-title="">
        <h2><i class="glyphicon glyphicon-eye-open"></i> Request Status</h2>
    </div>
    <div class="box-content">
    <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
    <thead>

    <tr>
        
        <th>Origin</th>
        <th>Destination</th>
        <th>Waybill Number</th>
        <th>Status</th>
    </tr>
    </thead>
    <tbody>
    @forelse($requests as $request)
    <tr>
        <td class="center">{{$request->origin}}</td>
        <td class="center">{{$request->destination}}</td>
        <td class="center">{{$request->waybill_number}}</td>
        <td class="center">{{$request->description}}</td>
    </tr>
    @empty
    <td class="center">No Requests Yet</td>
    <td class="center">No Requests Yet</td>
    <td class="center">No Requests Yet</td>
    <td class="center">No Requests Yet</td>
    @endforelse
    </tbody>
    </table>
    </div>
    </div>
    </div>
    <!--/span-->

    </div>



@endsection