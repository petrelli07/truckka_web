@extends('registry.fieldAgent.layout.content')
@section('content')



<div class="row">
    <div class="box col-md-12">
    <div class="box-inner">
    <div class="box-header well" data-original-title="">
        <h2><i class="glyphicon glyphicon-eye-open"></i> All Trucks</h2>
    </div>
    <div class="box-content">
    <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
    <thead>

    <tr>
        
        <th>Id</th>
        <th>Truck owners Name</th>
        <th>Driver Name</th>
        <th>Driver Phone</th>
        <th>Plate Number</th>
        <th>Type of truck</th>
        
       
    </tr>
    </thead>
    <tbody>
    <?php $x =1; ?>
    @forelse($truck_details as $request)
    <tr>
        <td class="center">{{$x++}}</td>
        <td class="center">{{$request->truck_owner_name}}</td>
        <td class="center">{{$request->driver_name}}</td>
        <td class="center">{{$request->driver_phone}}</td>
        <td class="center">{{$request->plate_number}}</td>
        <td class="center">{{$request->type_of_truck}}</td>
        
    </tr>
    @empty
    <td class="center">No Requests Yet</td>
   
    @endforelse
    </tbody>
    </table>
    </div>
    </div>
    </div>
    <!--/span-->

    </div>



@endsection