@extends('registry.fieldAgent.layout.content')
@section('content')


<div class="row">
    <div class="box col-md-12">
        <!--if @ session -->
            @if (session('message'))
                <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    {{ session('message') }}
                </div
            <p>Click <a href="{{url('/registry/fieldOps/truck_owners')}}">here</a> to add more trucks</p>
            @endif
        <!--endif-->

        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-plus"></i> Add New Truck</h2>

            </div>
            <div class="box-content row">
                 <form role="form" method="post" class="validate"  action="{{url('registry/fieldOps/add_truck_process')}}"  enctype="multipart/form-data">
                    {{csrf_field()}}
                 <input name="owner_id" value="{{$owner_id}}" type="hidden">
                 <div class="box col-md-6">
                    <div class="form-group">
                        <label class="control-label" for="driver_name">Driver Name</label>
                        <input type="text" name="driver_name" value="{{old('driver_name')}}" class="form-control required" placeholder="Driver Name">
                    </div>
                </div>


                 <div class="box col-md-6">
                    <div class="form-group">
                        <label class="control-label" for="driver_address">Plate Number</label>
                        <input type="text" name="plate_number" value="{{old('plate_number')}}" class="form-control required" placeholder="Plate Number">
                    </div>
                </div>

                 <div class="box col-md-6">
                    <div class="form-group">
                        <label class="control-label" for="phone_number">Phone Number</label>
                        <input type="text" name="driver_phone" value="{{old('driver_phone')}}" class="form-control required" placeholder="Phone Number">
                    </div>
                </div>

                 <div class="box col-md-6">
                    <div class="form-group">
                        <label class="control-label" for="driver_licence">Driver Licence</label>
                        <input type="file" name="driver_licence" class="form-control required" size="20" />
                    </div>
                </div>

                 <div class="box col-md-6">
                    <div class="form-group">
                        <label class="control-label" for="git">GIT</label>
                        <input type="file" name="git" class="form-control required" size="20" />
                    </div>
                </div>


                <div class="box col-md-6">
                    <div class="form-group">
                        <label class="control-label" for="vehicle_licence">Vehicle Licence</label>
                        <input type="file" name="vehicle_licence" class="form-control required" size="20" />
                    </div>
                </div>

                 <div class="box col-md-6">
                    <div class="form-group">
                        <label class="control-label" for="truck_insurance_paper">Truck Insurance Papers</label>
                        <input type="file" name="truck_insurance_paper" class="form-control required" size="20" />
                    </div>
                </div>

                <div class="box col-md-6">
                    <div class="form-group">
                        <label class="control-label" for="destination">Type Of Truck</label>
                        
                            <select id="selectError" class="form-control required" name="type_of_truck">
                            <option value=''>?</option>
                            <option value='Flatbed 30Ton'>Flatbed 30Ton</option>
                            </select>
                       
                    </div>
                </div>


                    <br /><br />
            
                <div class="box col-md-6">
                    <div class="form-group">
                        <button type="submit" class="btn btn-custom">
                            <i class="glyphicon glyphicon-plus"></i>Submit
                        </button>
                    </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
    <!--/span-->

</div><!--/row-->




@endsection