@extends('registry.mgt.layout.content')
@section('content')




<div class="row">
    <div class="box col-md-12">
    <div class="box-inner">
    <div class="box-header well" data-original-title="">
        <h2><i class="glyphicon glyphicon-eye-open"></i> Truck Registration Request</h2>
    </div>
    <div class="box-content">
    <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
    <thead>

    <tr>
        
        <th>Plate Id number</th>
        <th>Transporter name</th>
        <th>Field agent name</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        
        <td class="center">1</td>
        <td class="center">james</td>
        <td class="center">peter</td>
        <td class="center">
           
            <a class="btn btn-info btn-sm" href="#">
                <i class="glyphicon glyphicon-eye-open icon-white"></i>
              View more
            </a>
            
            
        </td>
    </tr>
    </tbody>
    </table>
    </div>
    </div>
    </div>
    <!--/span-->

    </div>




@endsection