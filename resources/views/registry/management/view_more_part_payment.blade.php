@extends('registry.management.layout.content')
@section('content')


<div class="row">
    <div class="box col-md-12">


        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-plus"></i> View More</h2>

            </div>
            <div class="box-content row">

                <div class="box col-md-12">
                    @foreach($details as $detail)
                    <img src="{{url('/waybillfiles/'.$detail->waybill_number.$detail->waybill_file_origin)}}"  height="500" width="100%" class="img-responsive">
                </div>

                <div class="box col-md-12">
                    <div class="box-content">

                        <div class="row">
                            <div class="col-md-4">
                                <b>Waybill Number: {{$detail->waybill_number}}</b>
                            </div>
                            <div class="col-md-4">
                                <b>Plate Number: {{$detail->plate_number}}</b>
                            </div>
                            <div class="col-md-4">
                                <b>Driver Name: {{$detail->driver_name}}</b>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <b>Driver Phone: {{$detail->driver_phone}}</b>
                            </div>
                            <div class="col-md-4">
                                <b>Field Agent Name: {{$detail->name}}</b>
                            </div>
                        </div>

                            <div>

                                <a class="btn btn-success btn-sm view-shop" href='{{url("registry/ops/approve_part_payment/{$detail->registry_number}")}}'>Approve</a>|
                                <a class="btn btn-danger btn-sm view-shop" href='{{url("registry/ops/decline_part_payment/{$detail->registry_number}")}}'>Decline</a>

                            </div>
                            @endforeach
                    </div>

                </div>






            </div>
        </div>
    </div>
    <!--/span-->

</div><!--/row-->



@endsection