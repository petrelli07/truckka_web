@extends('registry.management.layout.content')
@section('content')


<div class="row">
    <div class="box col-md-12">

    @if(session('response'))
            <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    {{session('response')}}
            </div>
        @endif
        
    <div class="box-inner">
    <div class="box-header well" data-original-title="">
        <h2><i class="glyphicon glyphicon-eye-open"></i> All Approve Balance Payment</h2>
    </div>
    <div class="box-content">
    <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
    <thead>

    <tr>
        <th>ID</th>
        <th>Registry #</th>
        <th>Origin</th>
        <th>Destination</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    @if(!empty($results))
    <?php $x =1; ?>
    @foreach($results as $row)
    <tr>
        <td class="center">{{$x++}}</td>
        <td class="center">{{$row->registry_number}}</td>
        <td class="center">{{$row->origin}}</td>
        <td class="center">{{$row->destination}}</td>
        <td class="center">
            <a class="btn btn-success btn-sm view-shop" href='{{url("registry/ops/view_more_balance/{$row->registry_number}")}}'>View More</a>
        </td>
    </tr>
    @endforeach
    @else

    <tr>
        
        <h2>No Data</h2>
    </tr>
    @endif
    </tbody>
    </table>
    </div>
    </div>
    </div>
    <!--/span-->

    </div>

@endsection