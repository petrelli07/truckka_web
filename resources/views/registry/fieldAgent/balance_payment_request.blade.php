@extends('registry.fieldAgent.layout.content')
@section('content')


<div class="row">
    <div class="box col-md-12">
        <!--if @ session -->
        @if (session('message'))
            <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                {{ session('message') }}
            </div>
        @endif
        <!--endif-->

        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-plus"></i> REQUEST BALANCE PAYMENT</h2>

            </div>
            <div class="box-content row">
                 <form role="form" method="post" class="validate"  action="{{url('/registry/fieldOps/request/process_balance_payment')}}"  enctype="multipart/form-data">
                 {{csrf_field()}}

                 <div class="box col-md-6">
                    <div class="form-group">
                        <label class="control-label" for="weight">WayBill Number</label>
                        <input type="text" name="waybill_number" class="form-control required" placeholder="Enter Waybill Number">
                        @if ($errors->has('waybill_number'))
                            <span class="help-block">
                                <strong>{{ $errors->first('waybill_number') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="box col-md-6">
                    <div class="form-group">
                        <label class="control-label" for="pick-up-date">Upload WayBill File </label>
                        <input type="file" name="waybill_file" class="form-control required" placeholder="Upload Waybill File">
                        @if ($errors->has('waybill_file'))
                            <span class="help-block">
                                <strong>{{ $errors->first('waybill_file') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="box col-md-6">
                    <div class="form-group">
                        <button type="submit" class="btn btn-custom">
                            <i class="glyphicon glyphicon-plus"></i>Submit
                        </button>
                    </div>
                </div>

                </form>

            </div>
        </div>
    </div>
    <!--/span-->

</div><!--/row-->

@endsection