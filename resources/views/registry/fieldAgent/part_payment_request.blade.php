@extends('registry.fieldAgent.layout.content')
@section('content')



<div class="row">
<!--if @ session -->
@if (session('message'))
<div class="alert alert-success" role="alert">
    <button type="button" class="close" data-dismiss="alert">x</button>
    {{ session('message') }}
</div>
@endif
<!--endif-->

    <!--select payment type-->
    <div class="box-inner">
        <div class="box-header well" data-original-title="">
            <h2><i class="glyphicon glyphicon-plus"></i> CHOOSE PART OR BALANCE PAYMENT TYPE</h2>

        </div>
        <div class="box-content row">
            @if($errors->has('errors'))
            @foreach ($errors->all() as $error)
            <div>{{ $error }}</div>
            @endforeach
            @endif
            <form role="form" class="validate" >

                <div class="box col-md-6">
                    <div class="form-group">
                        <label class="control-label" for="origin">Select Payment Type</label>
                        <div class="controls">
                            <select id="selectError" class="col-md-12 payment_type" data-rel="chosen">
                                <option value=''>[-Select Payment Type-]</option>
                                <option value='part_payment'>Part Payment</option>
                                <option value='balance_payment'>Balance Payment</option>
                            </select>
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>
    <!--end select payment type-->

    <!--    part payment div-->
    <div id="part_payment">
        <div class="box col-md-12">


            <div class="box-inner">
                <div class="box-header well" data-original-title="">
                    <h2><i class="glyphicon glyphicon-plus"></i> REQUEST PART PAYMENT</h2>

                </div>
                <div class="box-content row">
                    <form role="form" method="post" class="validate"  action="{{url('/registry/fieldOps/request/process_part_payment')}}"  enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="box col-md-6">
                            <div class="form-group">
                                <label class="control-label" for="origin">Origin</label>
                                <div class="controls">
                                    <select id="selectError" class="col-md-12 form-control"  name="origin">
                                        <option value=''>[-Select Origin-]</option>
                                        @forelse($routes as $route)
                                        <option value='{{$route->origin}}'>{{$route->origin}}</option>
                                        @empty
                                        <option value=''>[-Select Origin-]</option>
                                        @endforelse
                                    </select>
                                </div>
                                @if ($errors->has('origin'))
                            <span class="help-block">
                                <strong>{{ $errors->first('origin') }}</strong>
                            </span>
                                @endif
                            </div>
                        </div>

                        <div class="box col-md-6">
                            <div class="form-group">
                                <label class="control-label" for="destination">Destination</label>
                                <div class="controls">
                                    <select id="selectError" class="col-md-12 form-control"  name="destination">
                                        <option value=''>[-Select Destination-]</option>
                                        @forelse($routes as $route)
                                        <option value='{{$route->destination}}'>{{$route->destination}}</option>
                                        @empty
                                        <option value=''>[-Select Destination-]</option>
                                        @endforelse
                                    </select>
                                </div>
                                @if ($errors->has('destination'))
                            <span class="help-block">
                                <strong>{{ $errors->first('destination') }}</strong>
                            </span>
                                @endif
                            </div>
                        </div>

                        <div class="box col-md-6">
                            <div class="form-group">
                                <label class="control-label" for="weight">Total Amount for Truck</label>
                                <input type="text" name="amount" value="{{old('amount')}}" class="form-control required" placeholder="Enter Total Amount to Pay">
                                @if ($errors->has('amount'))
                            <span class="help-block">
                                <strong>{{ $errors->first('amount') }}</strong>
                            </span>
                                @endif
                            </div>
                        </div>

                        <div class="box col-md-6">
                            <div class="form-group">
                                <label class="control-label" for="weight">WayBill Number</label>
                                <input type="text" name="waybill_number" value="{{old('waybill_number')}}" class="form-control required" placeholder="Enter Waybill Number">
                                @if ($errors->has('waybill_number'))
                            <span class="help-block">
                                <strong>{{ $errors->first('waybill_number') }}</strong>
                            </span>
                                @endif
                            </div>
                        </div>

                        <div class="box col-md-6">
                            <div class="form-group">
                                <label class="control-label" for="pick-up-date">Upload WayBill File </label>
                                <input type="file" name="waybill_file" class="form-control required" placeholder="Upload Waybill File">
                                @if ($errors->has('waybill_file'))
                            <span class="help-block">
                                <strong>{{ $errors->first('waybill_file') }}</strong>
                            </span>
                                @endif
                            </div>
                        </div>

                        <div class="box col-md-6">
                            <div class="form-group">
                                <label class="control-label" for="pick-up-date">Plate Number</label>
                                <input type="text" name="plate_number" value="{{old('plate_number')}}" class="form-control required" placeholder="Enter Plate Number">
                                @if ($errors->has('plate_number'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('plate_number') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="box col-md-12">
                            <div class="form-group">
                                <label class="control-label" for="pick-up-date">Recipient Phone Number</label>
                                <input type="text" style="width: 50%" name="recipient_phone_number" value="{{old('recipient_phone_number')}}" class="form-control required" placeholder="Enter Recipient's Phone Number">
                                @if ($errors->has('recipient_phone_number'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('recipient_phone_number') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="box col-md-6">
                            <div class="form-group">
                                <button type="submit" class="btn btn-custom">
                                    <i class="glyphicon glyphicon-plus"></i>Submit
                                </button>
                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </div>
        <!--/span-->
    </div>
    <!--    end part payment div-->

    <!--    balance payment div-->
    <div id="balance_payment">
        <div class="box col-md-12">


            <div class="box-inner">
                <div class="box-header well" data-original-title="">
                    <h2><i class="glyphicon glyphicon-plus"></i> REQUEST BALANCE PAYMENT</h2>

                </div>
                <div class="box-content row">
                    <form role="form" method="post" class="validate"  action="{{url('/registry/fieldOps/request/process_balance_payment')}}"  enctype="multipart/form-data">
                        {{csrf_field()}}

                        <div class="box col-md-6">
                            <div class="form-group">
                                <label class="control-label" for="weight">WayBill Number</label>
                                <input type="text" name="waybill_number" class="form-control required" placeholder="Enter Waybill Number">
                                @if ($errors->has('waybill_number'))
                            <span class="help-block">
                                <strong>{{ $errors->first('waybill_number') }}</strong>
                            </span>
                                @endif
                            </div>
                        </div>

                        <div class="box col-md-6">
                            <div class="form-group">
                                <label class="control-label" for="pick-up-date">Upload WayBill File </label>
                                <input type="file" name="waybill_file" class="form-control" placeholder="Upload Waybill File">
                                @if ($errors->has('waybill_file'))
                            <span class="help-block">
                                <strong>{{ $errors->first('waybill_file') }}</strong>
                            </span>
                                @endif
                            </div>
                        </div>

                        <div class="box col-md-6">
                            <div class="form-group">
                                <button type="submit" class="btn btn-custom">
                                    <i class="glyphicon glyphicon-plus"></i>Submit
                                </button>
                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>
    <!--    end balance payment div-->


</div><!--/row-->

@endsection