@extends('registry.fieldAgent.layout.content')
@section('content')


<div class="row">
    <div class="box col-md-12">
        <!--if @ session -->
            @if (session('message'))
                <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    {{ session('message') }}
                </div
            @endif
        <!--endif-->

        <div class="box-inner">
            <div class="box-header well" data-original-title="">

            </div>
            <div class="box-content row">
                 <form role="form" method="post" class="validate"  action="{{url('registry/finalize_part_bulk_payment')}}"  enctype="multipart/form-data">
                    {{csrf_field()}}

                 <div class="box col-md-12">
                    <div class="form-group">
                        <label class="control-label" for="otp">Enter OTP</label>
                        <input type="text" style="width: 25%" name="otp" class="form-control required" placeholder="Enter OTP">
                    </div>
                </div>
                     <input type="hidden" value="{{$details['transfer_code']}}" name="transfer_code">
                     <input type="hidden" value="{{$details['amount_to_pay']}}" name="amount_to_pay">
                     <input type="hidden" value="{{$details['full_amount_to_pay']}}" name="full_amount_to_pay">
                     @for($x = 0; $x<count($agent_id);$x++)
                     <input type="hidden" value="{{$agent_id[$x]}}" name="agent_id[]">
                     @endfor

                     @for($i = 0; $i<count($registry_id);$i++)
                     <input type="hidden" value="{{$registry_id[$i]}}" name="registry_id[]">
                     @endfor

                <br /><br />
            
                <div class="box col-md-6">
                    <div class="form-group">
                        <button type="submit" class="btn btn-custom">
                            <i class="glyphicon glyphicon-plus"></i>Submit
                        </button>
                    </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
    <!--/span-->

</div><!--/row-->




@endsection