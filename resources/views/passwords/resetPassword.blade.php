<!DOCTYPE html>

<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>Truckka</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="{{url('assets/global/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{url('assets/global/plugins/simple-line-icons/simple-line-icons.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{url('assets/global/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{url('assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css')}}" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="{{url('assets/global/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{url('assets/global/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="{{url('assets/global/css/components-md.min.css')}}" rel="stylesheet" id="style_components" type="text/css" />
        <link href="{{url('assets/global/css/plugins-md.min.css')}}" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="{{url('assets/pages/css/login.min.css')}}" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" /> </head>
    <!-- END HEAD -->

    <body class=" login">
        <!-- BEGIN LOGO -->
        <!-- <div class="logo">
            <a href="index.html">
                <img src="{{url('assets/pages/img/logo-big.png')}}" alt="" /> </a>
        </div> -->
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="content">

                    <form id="logout-form" action="{{ route('logout') }}" class="ajax-form-submit"  method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>

                <div class="alert alert-danger print-error-msg" style="display:none">

                    <ul></ul>
                </div>

                <div class="alert alert-success print-success-msg" style="display:none">

                    <ul></ul>
                </div>

            <!-- BEGIN LOGIN FORM -->
            <form  id="resetPass" action="{{url('/resetPassword')}}" ajax-submit="true" method="POST"  redirect-to="{{url('/')}}" class="order-form  newTestCase login-form" >
            {{ csrf_field() }}
                <h3 class="form-title font-green">Change Default Password</h3>
                <!-- <div class="alert alert-danger display-hide">
                    <button class="close" data-close="alert"></button>
                    <span> Enter any username and password. </span>
                </div> -->
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} ">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">Enter New Password</label>
                    <input class="form-control form-control-solid placeholder-no-fix" placeholder="Enter New Password" type="password" autocomplete="off" id="name" name="password" required="required" /> 
                           
                </div>

                <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }} ">
                    <label class="control-label visible-ie8 visible-ie9">Re-Type New Password</label>
                    <input class="form-control form-control-solid placeholder-no-fix" placeholder="Re-Type New Password" type="password" autocomplete="off" id="password_confirmation" name="password_confirmation" required="required" /> 
                           
                </div>
                 

        <!-- Button (Double) -->
        <div class="form-group" >
            <div class="row">
                <div class="col-md-12">
                    <button align="left" id="btn_registrar" name="btn_registrar" class="btn button_logina btn btn-primary resetPassword">RESET PASSWORD</button>
                </div>
                
            </div>
        </div>
                    <!--BOTTOM NAV BAR-->

    </form>

        </div>



                    <!-- Select Basic -->
       
            <!-- END REGISTRATION FORM -->
        <div class="copyright"> 2018 &copy; Truckka </div>
        <!--[if lt IE 9]>
<script src="../assets/global/plugins/respond.min.js"></script>
<script src="../assets/global/plugins/excanvas.min.js"></script> 
<script src="../assets/global/plugins/ie8.fix.min.js"></script> 
<![endif]-->
 @include('client.includes.footer')

        <script type="text/javascript">
            var baseURI     =  "{{url('/')}}";
        </script>
        <script type="text/javascript">
            $(document).ready(function() {

    $(".resetPassword").click(function(e){
        e.preventDefault();


        var _token = $("input[name='_token']").val();/*
        var name = $("input[name='adminName']").val();*/

        $.ajax({

            async:false,

            processData: false,

            contentType: false,

             //url: "http://truckka.com.ng:8082/resetPassword",
            url: baseURI+"/resetPassword",

            type:'POST',

            data: new FormData($("#resetPass")[0]),

            success: function(data) {

                if($.isEmptyObject(data.error)){

                    printSuccessMsg(data.success);
                    document.getElementById('logout-form').submit();
                    //window.location.replace("http://localhost/tmsa/public/home");
                    //window.location.replace("http://truckka.com.ng:8082/home");

                }else{

                    alert(data.error);

                }

            }

        });


    });
        </script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <!-- END THEME LAYOUT SCRIPTS -->
    </body>

</html>