<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>TMSA</title>
    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css" href="{{ url('css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ url('css/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('css/indexupdate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('css/tmsa.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('css/loader.css') }}">
    <link href="{{ url('https://fonts.googleapis.com/css?family=Titillium+Web|Concert+One|Josefin+Sans|Roboto') }}" rel="stylesheet">
    <script type="text/javascript" src="{{ url('js/admin_dashboard.js')}}"></script>
     <script type="text/javascript">
        var baseURI     =  "{{url('/')}}";
    </script>
    <script type="text/javascript" src="{{ url('js/jquery-3.2.1.js')}}"></script>
    <script type="text/javascript" src="{{ url('js/bootstrap.js')}}"></script>
    <script type="text/javascript" src="{{ url('js/changePassword.js')}}"></script>

    <script>

        $(document).ready(function() {

            $('.createClient').hide();
            $('.createCarrier').hide();
            $('.createAdmin').show();
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function() {

            $(document).ajaxStart(function(){
                document.getElementById('modal').style.display = 'block';
                document.getElementById('fade').style.display = 'block';
            });

            $(document).ajaxComplete(function(){
                document.getElementById('modal').style.display = 'none';
                document.getElementById('fade').style.display = 'none';
            });
        });
        
    </script>

</head>
<body class="col-md-12-col-sm-6 col-xs-12" style="padding:0;background-color: #3baba0; height: auto">

</div>
<!--TMSA-->
<div class="tmsa" style="height:100vh">
    <div class="row" style="height:100vh; margin-right:0; margin-left:0;">
        <div class="side_bar col-md-3" >
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed pull-left" data-toggle="collapse" data-target="#hidden_bar" aria-expanded="false" style="border: 2px solid #fff ; margin-left:10px;">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar" style="border:1px solid #fff"></span>
                    <span class="icon-bar" style="border:1px solid #fff"></span>
                    <span class="icon-bar" style="border:1px solid #fff"></span>
                </button>
                <a class="navbar-brand" href="#">
                    <div class="brand">
                        <span class="navbar-brand  logoT" style="border: 1px solid #ffffff; margin-left: 2px; border-radius: 10px 0 0 0; font-family: 'Concert One', cursive; color: #ffffff" >T</span>
                        <span class="navbar-brand  logoR" style="border: 1px solid #ffffff;  margin-left: 2px; font-family: 'Concert One', cursive; background: #ffffff; color: #3baba0;">R</span>
                        <span class="navbar-brand   logoU" style="border: 1px solid #ffffff;  margin-left: 2px; font-family: 'Concert One', cursive; color: #ffffff;">U</span>
                        <span class="navbar-brand   logoC" style="border: 1px solid #ffffff;  margin-left: 2px;font-family: 'Concert One', cursive; background: #ffffff; color: #3baba0;">C</span>
                        <span class="navbar-brand  logoK" style="border: 1px solid #ffffff;  margin-left: 2px;font-family: 'Concert One', cursive; color: #ffffff"; >K</span>
                        <span class="navbar-brand   logoK" style="border: 1px solid #ffffff;  margin-left: 2px;font-family: 'Concert One', cursive; background: #ffffff; color: #3baba0;">K</span>
                        <span class="navbar-brand   logoA" style="border: 1px solid #ffffff;  margin-left: 2px; border-radius: 0 0 10px 0;font-family: 'Concert One', cursive; color: #ffffff;">A</span>
                    </div>
                </a>
            </div>
<!--NAVBAR FOR MOBIE SCREENS-->
            <ul class="nav navbar-nav collapse" id="hidden_bar" style="margin-top:50px;">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage User Groups<span class="glyphicon glyphicon-user pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{url('/home')}}">Create Users<span class="glyphicon glyphicon-plus-sign pull-right"></span></a></li>
                        <li class="divider"></li>
                        <li><a href="{{url('/viewAllUsers')}}">View All Users <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Client Agents<span class="glyphicon glyphicon-file pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{url('/viewAllClients')}}">Create Client Agents<span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage Client Orders <span class="glyphicon glyphicon-file pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{url('/viewOrders')}}">View All Orders <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Incident Management <span class="glyphicon glyphicon-file pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{url('/incidentManagement')}}">View All Incidents <span class="glyphicon glyphicon-open-file pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage Resources <span class="glyphicon glyphicon-wrench pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href='{{ url("/viewAllResources") }}'>View All Resources <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Track and Trace <span class="glyphicon glyphicon-map-marker pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href='{{ url("/allGPS") }}'>Track Orders <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();" class="dropdown-toggle" data-toggle="dropdown">Log Out <span class="glyphicon glyphicon-log-out pull-right"></span></a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>


            </ul>
            <!--NAVBAR FOR laptop SCREENS-->
            <ul class="nav navbar-nav hidden-xs hidden-sm" style="padding-top:0;">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage User Groups<span class="glyphicon glyphicon-user pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{url('/home')}}">Create Users<span class="glyphicon glyphicon-plus-sign pull-right"></span></a></li>
                        <li class="divider"></li>
                        <li><a href="{{url('/viewAllUsers')}}">View All Users <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Client Agents<span class="glyphicon glyphicon-file pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{url('/viewAllClients')}}">Create Client Agents<span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage Client Orders <span class="glyphicon glyphicon-file pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{url('/viewOrders')}}">View All Orders <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Incident Management <span class="glyphicon glyphicon-file pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{url('/incidentManagement')}}">View All Incidents <span class="glyphicon glyphicon-open-file pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage Resources <span class="glyphicon glyphicon-wrench pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href='{{ url("/viewAllResources") }}'>View All Resources <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Track and Trace <span class="glyphicon glyphicon-map-marker pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href='{{ url("/allGPS") }}'>Track Orders <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();" class="dropdown-toggle" data-toggle="dropdown">Log Out <span class="glyphicon glyphicon-log-out pull-right"></span></a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>

            </ul>
        </div>
        <!-- TSMA MAIN PAGE -->
        <div class="tmsa_main_body col-md-9" style="height:100vh">
            <div class="tmsa_user_page" >
                <!session message starts-->

                @if(session('message'))
                <p>
                    {{ session('message') }}
                </p>
                @endif

                <!session message ends-->
                <div class="alert alert-danger print-error-msg" style="display:none">

                    <ul></ul>
                </div>

                <div class="alert alert-success print-success-msg" style="display:none">

                    <ul></ul>
                </div>
                <!-- FORM -->
                   
                        <!-- Email input-->
                    <div class="createClient">
                        <form id="clientDetails"  class="newTestCase">
                            <fieldset>
                                {{ csrf_field() }}
                                <div class="form-group " style="margin-left:10%; margin-right: 10%;  margin-bottom: 3%;">
                                    <label><h4>Email Address</h4></label>
                                    <div class="col-md-12">
                                        <label class="sr-only" for="form-first-name">Email</label>
                                        <input placeholder="Email Address" class="input-md form-control" id="clientemail" name="clientemail" required="required">

                                    </div>
                                </div>

                                <div class="form-group " style="margin-left:10%; margin-right: 10%;  margin-bottom: 3%;">
                                    <label><h4>Client Name</h4></label>
                                    <div class="col-md-12">
                                        <label class="sr-only" for="form-first-name">Client Name</label>
                                        <input placeholder="Client Name" class="input-md form-control" id="clientname" name="clientname" required="required">

                                    </div>
                                </div>

                                <div class="form-group " style="margin-left:10%; margin-right: 10%;  margin-bottom: 3%;">
                                    <label><h4>Company Name</h4></label>
                                    <div class="col-md-12">
                                        <label class="sr-only" for="form-first-name">Company Name</label>
                                        <input placeholder="Company Name" class="input-md form-control"  id="companyName" name="companyName" required="required">
                                    </div>
                                </div>

                                <input type="hidden" name="clientcategory" value="1">

                                <div class="form-group " style="margin-left:10%; margin-right: 10%;  margin-bottom: 3%;">
                                    <label><h4>RC Number</h4></label>
                                    <div class="col-md-12">
                                        <label class="sr-only" for="form-first-name">RC Number</label>
                                        <input placeholder="RC Number" class="input-md form-control" id="rcNumber" name="rcNumber" required="required">
                                    </div>
                                </div>

                                <div class="form-group " style="margin-left:10%; margin-right: 10%;  margin-bottom: 3%;">
                                    <label><h4>CHoose Required Resource For Client</h4></label>
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <p>Resource Type:</p>
                                                <p> 40 Ton | <input type="checkbox" value="40ton" name="resourceType[]"></p>
                                            </div>
                                            <div class="col-md-3">
                                                <p>Resource Type:</p>
                                                <p> 30 Ton | <input type="checkbox" value="30ton" name="resourceType[]"></p>
                                            </div>
                                            <div class="col-md-3">
                                                <p>Resource Type:</p>
                                                <p> 15 Ton |<input type="checkbox" value="15ton" name="resourceType[]"></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group " style="margin-left:10%; margin-right: 10%;  margin-bottom: 3%;">
                                    <label><h4>Upload Client Standard Form Here</h4></label>
                                    <div class="col-md-12">
                                        <label class="sr-only" for="form-first-name">Upload Client Standard Form Here</label>
                                        <input type="file" name="clientForm" required class="form-control">
                                    </div>
                                </div>

                                <div class="form-group ">
                                    <div class="col-md-12">
                                        <button id="btn_registrar" name="btn_registrar" class="btn button_login newClientDets">CREATE USER</button>
                                    </div>
                                </div>

                            </fieldset>
                        </form>
                    </div>


                    <div class="createCarrier">
                        <form id="carrDetails"  class="newTestCase">
                            <fieldset>
                                {{ csrf_field() }}
                                <div class="form-group " style="margin-left:10%; margin-right: 10%;  margin-bottom: 3%;">
                                    <label><h4>Email Address</h4></label>
                                    <div class="col-md-12">
                                        <label class="sr-only" for="form-first-name">Email</label>
                                        <input placeholder="Email Address" class="input-md form-control" id="email" name="email" required="required">

                                    </div>
                                </div>

                                <div class="form-group " style="margin-left:10%; margin-right: 10%;  margin-bottom: 3%;">
                                    <label><h4>Name</h4></label>
                                    <div class="col-md-12">
                                        <label class="sr-only" for="form-first-name">Name</label>
                                        <input placeholder="Name" class="input-md form-control" id="name" name="name" required="required">

                                    </div>
                                </div>

                                <input type="hidden" name="category" value="2">

                                <div class="form-group " style="margin-left:10%; margin-right: 10%;  margin-bottom: 3%;">
                                    <label><h4>Company Name</h4></label>
                                    <div class="col-md-12">
                                        <label class="sr-only" for="form-first-name">Company Name</label>
                                        <input placeholder="Company Name" class="input-md form-control"  id="companyName" name="companyName" required="required">
                                    </div>
                                </div>

                                <div class="form-group " style="margin-left:10%; margin-right: 10%;  margin-bottom: 3%;">
                                    <label><h4>RC Number</h4></label>
                                    <div class="col-md-12">
                                        <label class="sr-only" for="form-first-name">RC Number</label>
                                        <input placeholder="RC Number" class="input-md form-control" id="rcNumber" name="rcNumber" required="required">
                                    </div>
                                </div>

                                <div class="form-group " style="margin-left:10%; margin-right: 10%;  margin-bottom: 3%;">
                                    <label><h4>Upload Carrier Resource File</h4></label>
                                    <div class="col-md-12">
                                        <label class="sr-only" for="form-first-name">Upload Carrier Resource File</label>
                                        <input type="file" name="carrierRoute" required>
                                    </div>
                                </div>

                                <div class="form-group " style="margin-left:10%; margin-right: 10%;  margin-bottom: 3%;">
                                    <label><h4>Carrier Resource Type and Number</h4></label>
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <p>Resource Type: 40 Ton</p>
                                                <p>Number: <input type="text" name="resourceNumber[]"></p>
                                                <input type="hidden" value="40ton" name="resourceType[]">
                                            </div>
                                            <div class="col-md-3">
                                                <p>Resource Type: 30 Ton</p>
                                                <p>Number: <input type="text" name="resourceNumber[]"></p>
                                                <input type="hidden" value="30ton" name="resourceType[]">
                                            </div>
                                            <div class="col-md-3">
                                                <p>Resource Type: 15 Ton</p>
                                                <p>Number: <input type="text" name="resourceNumber[]"></p>
                                                <input type="hidden" value="15ton" name="resourceType[]">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group " style="margin-left:10%; margin-right: 10%;">
                                    <label><h4>Choose Regions Covered by Carrier</h4></label>
                                    <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <p>SS</p>
                                                    <p>
                                                    <input type="checkbox" value="0" name="region[]">
                                                    </p>
                                                </div>
                                                <div class="col-md-4">
                                                    <p>SE</p>   
                                                    <p>
                                                    <input type="checkbox" value="1" name="region[]">
                                                    </p>
                                                </div>
                                                <div class="col-md-4">
                                                    <p>SW</p>
                                                    <p>
                                                    <input type="checkbox" value="2" name="region[]">
                                                    </p>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-4">
                                                    <p>NE</p>
                                                    <p>
                                                    <input type="checkbox" value="3" name="region[]">
                                                    </p>
                                                </div>
                                                <div class="col-md-4">
                                                    <p>NW</p>
                                                    <p>
                                                    <input type="checkbox" value="5" name="region[]">
                                                    </p>
                                                </div>
                                                <div class="col-md-4">
                                                    <p>NC</p>
                                                    <p>
                                                    <input type="checkbox" value="6" name="region[]">
                                                    </p>
                                                </div>
                                            </div>        
                                               
                                    </div>
                                </div>

                                <!-- <div class="form-group " style="margin-left:10%; margin-right: 10%;  margin-bottom: 3%;">
                                    <label><h4>Upload Carrier Standard Resource Data File</h4></label>
                                    <div class="col-md-12">
                                        <label class="sr-only" for="form-first-name">Upload Carrier Standard Resource Data File</label>
                                        <input type="file" name="carrierForm" required>
                                    </div>
                                </div>

                                <div class="form-group " style="margin-left:10%; margin-right: 10%;  margin-bottom: 3%;">
                                    <label><h4>Upload Carrier Standard Resource Details File</h4></label>
                                    <div class="col-md-12">
                                        <label class="sr-only" for="form-first-name">Upload Carrier Standard Resource Details File</label>
                                        <input type="file" name="resourceDetails" required>
                                    </div>
                                </div> -->

                            <div class="form-group ">
                                <div class="col-md-12">
                                    <button id="btn_registrar" name="btn_registrar" class="btn button_login newCarrDets">CREATE USER</button>
                                </div>
                            </div>

                            </fieldset>
                        </form>
                    </div>


                    <div class="createAdmin">
                        <form id="resetPass"  class="newTestCase">

                            <fieldset>

                            <div class="form-group" >
                            <div class="input-group" style="margin-left:10%; margin-right: 10%; margin-bottom: 3%; border-radius:10px;">
                                <label for="addUser" class="input-group-addon" style=" border-radius:10px; height: 50px; font-family: Roboto; font-size:150%; font-weight: 1000;"> CHANGE DEFAULT PASSWORD </label>
                            </div>
                        </div>
                                {{ csrf_field() }}
                                <!-- <div class="form-group " style="margin-left:10%; margin-right: 10%;  margin-bottom: 3%;">
                                    <label><h4>Default Password</h4></label>
                                    <div class="col-md-12">
                                        <label class="sr-only" for="form-first-name">Default Password</label>
                                        <input type="password" placeholder="Enter Default Password" class="input-md form-control" id="oldpassword" name="oldpassword" required="required">

                                    </div>
                                </div> -->

                                <div class="form-group " style="margin-left:10%; margin-right: 10%;  margin-bottom: 3%;">
                                    <label><h4>New Password</h4></label>
                                    <div class="col-md-12">
                                        <label class="sr-only" for="form-first-name">New Password</label>
                                        <input type="password" placeholder="Enter New Password" class="input-md form-control" id="name" name="password" required="required">

                                    </div>
                                </div>

                                <div class="form-group " style="margin-left:10%; margin-right: 10%;  margin-bottom: 3%;">
                                    <label><h4>Re-Type New Password</h4></label>
                                    <div class="col-md-12">
                                        <label class="sr-only" for="form-first-name">Re-Type New Password</label>
                                        <input type="password" placeholder="Re-Type New Password" class="input-md form-control" id="password_confirmation" name="password_confirmation" required="required">

                                    </div>
                                </div>

                            <div class="form-group ">
                                <div class="col-md-6 col-md-offset-3" style="padding-top:3%;">
                                    <button id="btn_registrar" name="btn_registrar" class="btn button_login resetPassword">RESET</button>
                                </div>
                            </div>

                            </fieldset>
                        </form>
                    </div>


                        <!-- Select Basic -->
                        <!--<div class="form-group" style="margin-left:20%; margin-right: 20%;  margin-bottom: 3%;">
                            <div class="col-md-12">
                                <select id="selectbasic" name="selectbasic" class="form-control" placeholder="Choose Category">
                                    <option value="1">Choose Category</option>
                                    <option value="2">Cajero</option>
                                    <option value="3">Liquidacion</option>
                                    <option value="4">Almacenero</option>
                                    <option value="5">Ayudante</option>
                                </select>
                            </div>
                        </div>-->

                        <!-- Button (Double) -->

            </div>

        </div>
    </div>
    <!--BOTTOM NAV BAR-->
    <!-- <div class = "bottom footer" style="height: auto; width:100%; background-color: #3baba0; ">
        <p class="text-center" style="color: #232323; margin-top: 5px; background-color: #3baba0;" >Copyright © TRUCKKA 2018. All rights reserved.</p>
    </div> -->
    <div id="fade"></div>
<div id="modal">
    <img id="loader" src="{{url('images/69.gif')}}" />
</div>
</div>

<!--     ************************************************** -->
</body>
</html>

