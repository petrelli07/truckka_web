<?php

use Illuminate\Database\Seeder;

class status extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        DB::table('status')->insert([
            'status_name' => 'Active'
        ]);

        DB::table('status')->insert([
            'status_name' => 'Banned'
        ]);
    }
}
