<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Osita',
            'email' => 'osita@gmail.com',
            'phone' => '08142330001',
            'userAccessLevel' => '0',
            'password' => bcrypt('password'),
            'default_password' => bcrypt('password'),
            'status' => 1,
        ]);

        DB::table('users')->insert([
            'name' => 'Parson',
            'email' => 'parson@field.com',
            'phone' => '08142330001',
            'userAccessLevel' => '7',
            'password' => bcrypt('12345'),
            'default_password' => bcrypt('12345'),
            'status' => 1,
        ]);

        DB::table('users')->insert([
            'name' => 'james',
            'email' => 'james@ops.com',
            'phone' => '08142330001',
            'userAccessLevel' => '9',
            'password' => bcrypt('12345'),
            'default_password' => bcrypt('12345'),
            'status' => 1,
        ]);

        DB::table('users')->insert([
            'name' => 'joy',
            'email' => 'joy@fin.com',
            'phone' => '08142330001',
            'userAccessLevel' => '8',
            'password' => bcrypt('12345'),
            'default_password' => bcrypt('12345'),
            'status' => 1,
        ]);
    }
}
