<?php

use Illuminate\Database\Seeder;

class UniqueSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        DB::table('unique_users_type')->insert([
            'typeName' => 'Super Admin'
        ]);
        
         DB::table('unique_users')->insert([
            'user_id' 	=> 1,
            'type_id'	=> 2
        ]);
    }
}
