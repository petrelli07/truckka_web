<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditCarrierHaulageRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('carrier_haulage_resource_requests', function (Blueprint $table) {
            // $table->dropForeign('carrier_id');
            $table->drop(array('driverName','carrierName','plateNumber'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('carrier_haulage_resource_requests', function (Blueprint $table) {
            //
        });
    }
}
