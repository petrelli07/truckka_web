<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpotCarrierTrucksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spot_carrier_trucks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('resource_number');
            $table->string('plate_number');

            $table->integer('user_id')
                    ->unsigned()
                    ->index();
            $table->foreign('user_id')
                    ->references('id')
                    ->on('users')
                    ->onDelete('cascade');

            $table->integer('spot_order_resource_type_id')
                    ->unsigned()
                    ->index();
            $table->foreign('spot_order_resource_type_id')
                    ->references('id')
                    ->on('spot_order_resource_types')
                    ->onDelete('cascade');

            $table->integer('spot_carrier_truck_status_id')
                    ->unsigned()
                    ->index();
            $table->foreign('spot_carrier_truck_status_id')
                    ->references('id')
                    ->on('spot_carrier_truck_statuses')
                    ->onDelete('cascade');

            $table->string('regions_covered');
            $table->string('driverName');
            $table->string('contactNumber');
            $table->string('driverLicenseFile');
            $table->string('insuranceDocumentFile');
            
            $table->string('gpsDetails')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spot_carrier_trucks');
    }
}
