<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RecreateCarrierHaulageRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carrier_haulage_resource_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('status')->default(0);
            $table->text('resourceTypeNumber');
            $table->text('resourceType');
            $table->integer('resourceNumber');
            $table->integer('resource_limit');
            $table->integer('carrier_id')->unsigned()->index();
            $table->foreign('carrier_id')->references('id')->on('carrier_company_details')->onDelete('cascade');
            $table->integer('service_request_id')->unsigned()->index();
            $table->foreign('service_request_id')->references('id')->on('service_requests')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carrier_haulage_resource_requests');
    }
}
