<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTmswRoutesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tmsw_routes', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('origin_state_id')
                ->unsigned()
                ->index();
            $table->foreign('origin_state_id')
                ->references('id')
                ->on('tmsw_states')
                ->onDelete('cascade');

            $table->integer('destination_state_id')
                ->unsigned()
                ->index();
            $table->foreign('destination_state_id')
                ->references('id')
                ->on('tmsw_states')
                ->onDelete('cascade');

            $table->double('price',10,2);

            $table->integer('tmsw_status_id')
                ->unsigned()
                ->index();
            $table->foreign('tmsw_status_id')
                ->references('id')
                ->on('tmsw_statuses')
                ->onDelete('cascade');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tmsw_routes');
    }
}
