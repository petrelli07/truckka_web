<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegistryPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('registry_payments', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('truck_order_registry_id')
                ->unsigned()
                ->index();
            $table->foreign('truck_order_registry_id')
                ->references('id')
                ->on('truck_order_registries')
                ->onDelete('cascade');

            $table->integer('registry_payment_status_id')
                ->unsigned()
                ->index();

            $table->foreign('registry_payment_status_id')
                ->references('id')
                ->on('registry_payment_statuses')
                ->onDelete('cascade');

            $table->integer('agent_id')
                ->unsigned()
                ->index()->nullable();
            $table->foreign('agent_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->integer('user_id')
                ->unsigned()
                ->index();
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->string('total_amount',10,2);

            $table->decimal('part_payment_amount',10,2);

            $table->decimal('balance_payment_amount',10,2)->nullable

                ();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registry_payments');
    }
}
