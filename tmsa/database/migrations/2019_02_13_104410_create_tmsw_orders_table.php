<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTmswOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tmsw_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('reference_number');
            $table->integer('truck_number');

            $table->integer('tmsw_truck_type_id')
                ->unsigned()
                ->index();
            $table->foreign('tmsw_truck_type_id')
                ->references('id')
                ->on('tmsw_truck_types')
                ->onDelete('cascade');

            $table->string('pickup_address')->nullable();
            $table->string('delivery_address')->nullable();
            $table->string('contact_phone')->nullable();
            $table->string('contact_name')->nullable();

            $table->integer('user_id')
                ->unsigned()
                ->index();
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->integer('tmsw_company_detail_id')
                ->unsigned()
                ->index();
            $table->foreign('tmsw_company_detail_id')
                ->references('id')
                ->on('tmsw_company_details')
                ->onDelete('cascade');



            $table->integer('tmsw_status_id')
                ->unsigned()
                ->index();
            $table->foreign('tmsw_status_id')
                ->references('id')
                ->on('tmsw_statuses')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tmsw_orders');
    }
}
