<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHaulageResourceRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('haulage_resource_requests', function (Blueprint $table) {
            $table->increments('id');
            //$table->integer('serviceIDNo');
            $table->integer('status')->default(0);
            $table->text('resourceTypeNumber');
            $table->integer('carrier_id')->unsigned()->index();
            $table->foreign('carrier_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('service_request_id')->unsigned()->index();
            $table->foreign('service_request_id')->references('id')->on('service_requests')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('haulage_resource_requests');
    }
}
