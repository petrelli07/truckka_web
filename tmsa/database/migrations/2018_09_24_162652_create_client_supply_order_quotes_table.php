<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientSupplyOrderQuotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_supply_order_quotes', function (Blueprint $table) {
            $table->increments('id');
            $table->date('deliveryDate');
            $table->double('unitPrice',10,2);
            $table->string('unit');
            $table->string('invoice');/*
            $table->integer('client_company_id')->unsigned()->index();
            $table->foreign('client_company_id')->references('id')->on('companies')->onDelete('cascade');*/
            $table->integer('supplier_id')->unsigned()->index();
            $table->foreign('supplier_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('supply_order_id')->unsigned()->index();
            $table->foreign('supply_order_id')->references('id')->on('client_supply_orders')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_supply_order_quotes');
    }
}
