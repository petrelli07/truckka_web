<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpotCarrierOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spot_carrier_orders', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('user_id')
                    ->unsigned()
                    ->index();
            $table->foreign('user_id')
                    ->references('id')
                    ->on('users')
                    ->onDelete('cascade');

            $table->integer('spot_order_id')
                    ->unsigned()
                    ->index();
            $table->foreign('spot_order_id')
                    ->references('id')
                    ->on('spot_orders')
                    ->onDelete('cascade');

            $table->integer('spot_carrier_truck_id')
                    ->unsigned()
                    ->index();
            $table->foreign('spot_carrier_truck_id')
                    ->references('id')
                    ->on('spot_carrier_trucks')
                    ->onDelete('cascade');

            $table->integer('spot_carrier_order_status_id')
                    ->unsigned()
                    ->index();
            $table->foreign('spot_carrier_order_status_id')
                    ->references('id')
                    ->on('spot_carrier_order_statuses')
                    ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spot_carrier_orders');
    }
}
