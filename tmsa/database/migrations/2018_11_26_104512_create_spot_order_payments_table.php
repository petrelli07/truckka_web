<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpotOrderPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::create('spot_order_payments', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('reference_number');

            $table->decimal('amount',10,2);

            $table->integer('user_id')
                    ->unsigned()
                    ->index();
                    
            $table->foreign('user_id')
                    ->references('id')
                    ->on('users')
                    ->onDelete('cascade');

            $table->integer('spot_order_invoice_id')
                    ->unsigned()
                    ->index();
                    
            $table->foreign('spot_order_invoice_id')
                    ->references('id')
                    ->on('spot_order_invoices')
                    ->onDelete('cascade');

            $table->integer('bank_id')
                    ->unsigned()
                    ->index();
                    
            $table->foreign('bank_id')
                    ->references('id')
                    ->on('banks')
                    ->onDelete('cascade');

            $table->string('depositor_name');
            $table->string('contact_number');

            $table->integer('spot_order_payment_status_id')
                    ->unsigned()
                    ->index();
            $table->foreign('spot_order_payment_status_id')
                    ->references('id')
                    ->on('spot_order_payment_statuses')
                    ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spot_order_payments');
    }
}
