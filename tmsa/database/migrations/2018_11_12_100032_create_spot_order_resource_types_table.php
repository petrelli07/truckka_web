<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpotOrderResourceTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spot_order_resource_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('resource_type_description');

            $table->integer('sort_statuses_id')
                    ->unsigned()->index();

            $table->foreign('sort_statuses_id')
                    ->references('id')
                    ->on('sort_statuses')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spot_order_resource_types');
    }
}
