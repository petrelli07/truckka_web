<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIdleCapacitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('idle_capacities', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('number_of_truck');

            $table->integer('user_id')
                ->unsigned()
                ->index();
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->integer('truck_supplier_id')
                ->unsigned()
                ->index();
            $table->foreign('truck_supplier_id')
                ->references('id')
                ->on('truck_suppliers')
                ->onDelete('cascade');

            $table->integer('exchange_truck_type_id')
                ->unsigned()
                ->index();
            $table->foreign('exchange_truck_type_id')
                ->references('id')
                ->on('exchange_truck_types')
                ->onDelete('cascade');

            $table->integer('exchange_coverage_area_id')
                ->unsigned()
                ->index();
            $table->foreign('exchange_coverage_area_id')
                ->references('id')
                ->on('exchange_coverage_areas')
                ->onDelete('cascade');

            $table->integer('exchange_status_id')
                ->unsigned()
                ->index();
            $table->foreign('exchange_status_id')
                ->references('id')
                ->on('exchange_statuses')
                ->onDelete('cascade');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('idle_capacities');
    }
}
