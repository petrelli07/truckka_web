<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderResourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_resources', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('resource_id')
                  ->unsigned()
                  ->index();

            $table->foreign('resource_id')
                  ->references('id')
                  ->on('carrier_resource_details')
                  ->onDelete('cascade');

            $table->integer('order_id')
                  ->unsigned()
                  ->index();

            $table->foreign('order_id')
                  ->references('id')
                  ->on('service_requests')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_resources');
    }
}
