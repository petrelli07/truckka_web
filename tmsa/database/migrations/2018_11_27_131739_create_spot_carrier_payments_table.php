<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpotCarrierPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spot_carrier_payments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('carrier_payment_number');
            $table->decimal('amount',10,2);

            $table->integer('user_id')
                    ->unsigned()
                    ->index();
            $table->foreign('user_id')
                    ->references('id')
                    ->on('users')
                    ->onDelete('cascade');

            $table->integer('spot_order_id')
                    ->unsigned()
                    ->index();
            $table->foreign('spot_order_id')
                    ->references('id')
                    ->on('spot_orders')
                    ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spot_carrier_payments');
    }
}
