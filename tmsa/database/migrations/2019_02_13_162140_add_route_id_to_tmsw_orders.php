<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRouteIdToTmswOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tmsw_orders', function (Blueprint $table) {


            $table->integer('tmsw_route_id')
                ->unsigned()
                ->index();
            $table->foreign('tmsw_route_id')
                ->references('id')
                ->on('tmsw_routes')
                ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tmsw_orders', function (Blueprint $table) {

        });
    }
}
