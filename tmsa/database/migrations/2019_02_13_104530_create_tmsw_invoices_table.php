<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTmswInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tmsw_invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->string('invoice_reference');

            $table->integer('tmsw_truck_order_id')
                ->unsigned()
                ->index();
            $table->foreign('tmsw_truck_order_id')
                ->references('id')
                ->on('tmsw_truck_orders')
                ->onDelete('cascade');


            $table->integer('tmsw_status_id')
                ->unsigned()
                ->index();
            $table->foreign('tmsw_status_id')
                ->references('id')
                ->on('tmsw_statuses')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tmsw_invoices');
    }
}
