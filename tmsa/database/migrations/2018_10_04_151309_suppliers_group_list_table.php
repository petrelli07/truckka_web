<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SuppliersGroupListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suppliers_group_lists', function (Blueprint $table) {
            $table->integer('supplier_id')
                  ->unsigned()
                  ->index();

            $table->foreign('supplier_id')
                  ->references('id')
                  ->on('client_supplier_details')
                  ->onDelete('cascade');

            $table->integer('group_id')
                  ->unsigned()
                  ->index();

            $table->foreign('group_id')
                  ->references('id')
                  ->on('suppliers_groups')
                  ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('suppliers_group_lists');
    }
}
