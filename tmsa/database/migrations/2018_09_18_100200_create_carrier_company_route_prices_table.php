<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarrierCompanyRoutePricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carrier_company_route_prices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('carrier_id')->unsigned()->index();
            $table->foreign('carrier_id')->references('id')->on('carrier_company_details')->onDelete('cascade');
            $table->string('origin');
            $table->string('destination');
            $table->decimal('price', 15,2);
            $table->string('resourceType');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carrier_company_route_prices');
    }
}
