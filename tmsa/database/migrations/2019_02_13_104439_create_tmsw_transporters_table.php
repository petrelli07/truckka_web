<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTmswTransportersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tmsw_transporters', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('reference_number');
            $table->integer('account_number');
            $table->string('account_name');

            $table->string('company_name');

            $table->integer('bank_id')
                ->unsigned()
                ->index();
            $table->foreign('bank_id')
                ->references('id')
                ->on('banks')
                ->onDelete('cascade');

            $table->string('address');
            $table->string('phone');

            $table->integer('user_id')
                ->unsigned()
                ->index();
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->integer('tmsw_status_id')
                ->unsigned()
                ->index();
            $table->foreign('tmsw_status_id')
                ->references('id')
                ->on('tmsw_statuses')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tmsw_transporters');
    }
}
