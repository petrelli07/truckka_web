<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderResourceGpsDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_resource_gps_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('carrier_id')->unsigned()->index();
            $table->foreign('carrier_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('plateNumber');
            $table->string('carrierName');
            $table->string('driverName');
            /*$table->integer('resource_gps_details_id')->unsigned()->index();
            $table->foreign('resource_gps_details_id')->references('id')->on('resource_gps_details')->onDelete('cascade');*/
            $table->integer('service_requests_id')->unsigned()->index();
            $table->foreign('service_requests_id')->references('id')->on('service_requests')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_resource_gps_details');
    }
}
