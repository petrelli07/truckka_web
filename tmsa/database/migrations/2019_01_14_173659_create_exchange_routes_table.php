<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExchangeRoutesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exchange_routes', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('origin_state_id')
                ->unsigned()
                ->index();
            $table->foreign('origin_state_id')
                ->references('id')
                ->on('states')
                ->onDelete('cascade');

            $table->integer('destination_state_id')
                ->unsigned()
                ->index();
            $table->foreign('destination_state_id')
                ->references('id')
                ->on('states')
                ->onDelete('cascade');

            $table->decimal('price',10,2);

            $table->integer('exchange_status_id')
                ->unsigned()
                ->index();
            $table->foreign('exchange_status_id')
                ->references('id')
                ->on('exchange_statuses')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exchange_routes');
    }
}
