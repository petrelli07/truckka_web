<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTmswStatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tmsw_states', function (Blueprint $table) {
            $table->increments('id');
            $table->string('state_description');

            $table->integer('tmsw_coverage_area_id')
                ->unsigned()
                ->index();
            $table->foreign('tmsw_coverage_area_id')
                ->references('id')
                ->on('tmsw_coverage_areas')
                ->onDelete('cascade');

            $table->integer('tmsw_status_id')
                ->unsigned()
                ->index();
            $table->foreign('tmsw_status_id')
                ->references('id')
                ->on('tmsw_statuses')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tmsw_states');
    }
}
