<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExchangeOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exchange_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_number');
            $table->integer('number_of_trucks');
            $table->date('pickup_date');
            $table->longText('item_description');
            $table->longText('pickup_address');
            $table->string('contact_phone');

            $table->integer('user_id')
                ->unsigned()
                ->index();
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->integer('exchange_route_id')
                ->unsigned()
                ->index();
            $table->foreign('exchange_route_id')
                ->references('id')
                ->on('exchange_routes')
                ->onDelete('cascade');

            $table->integer('exchange_truck_type_id')
                ->unsigned()
                ->index();
            $table->foreign('exchange_truck_type_id')
                ->references('id')
                ->on('exchange_truck_types')
                ->onDelete('cascade');

            $table->integer('exchange_status_id')
                ->unsigned()
                ->index();
            $table->foreign('exchange_status_id')
                ->references('id')
                ->on('exchange_statuses')
                ->onDelete('cascade');

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exchange_orders');
    }
}
