<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTmswTransporterCapacitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tmsw_transporter_capacities', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('tmsw_transporter_id')
                ->unsigned()
                ->index();
            $table->foreign('tmsw_transporter_id')
                ->references('id')
                ->on('tmsw_transporters')
                ->onDelete('cascade');



            $table->integer('tmsw_coverage_area_id')
                ->unsigned()
                ->index();
            $table->foreign('tmsw_coverage_area_id')
                ->references('id')
                ->on('tmsw_coverage_areas')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tmsw_transporter_capacities');
    }
}
