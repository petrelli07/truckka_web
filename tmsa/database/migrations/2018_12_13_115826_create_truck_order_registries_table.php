<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTruckOrderRegistriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('truck_order_registries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('registry_number');
            $table->string('origin');
            $table->string('destination');
            $table->string('waybill_number');
            $table->string('waybill_file_origin');
            $table->string('waybill_file_destination')->nullable();
            $table->string('amount');
            $table->string('recipient_phone_number');

            $table->integer('agent_id')
                ->unsigned()
                ->index();
            $table->foreign('agent_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->integer('registry_status_id')
                ->unsigned()
                ->index();
            $table->foreign('registry_status_id')
                ->references('id')
                ->on('registry_statuses')
                ->onDelete('cascade');

            $table->integer('truck_id')
                ->unsigned()
                ->index();
            $table->foreign('truck_id')
                ->references('id')
                ->on('trucks')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('truck_order_registries');
    }
}
