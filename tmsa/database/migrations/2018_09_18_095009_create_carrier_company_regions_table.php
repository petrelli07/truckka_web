<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarrierCompanyRegionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carrier_company_regions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('regions_covered');
            $table->integer('company_id')->unsigned()->index();
            $table->foreign('company_id')->references('id')->on('carrier_company_details')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carrier_company_regions');
    }
}
