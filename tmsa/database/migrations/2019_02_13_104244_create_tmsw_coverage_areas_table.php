<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTmswCoverageAreasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tmsw_coverage_areas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('coverage_area_description');

            $table->integer('tmsw_status_id')
                ->unsigned()
                ->index();
            $table->foreign('tmsw_status_id')
                ->references('id')
                ->on('tmsw_statuses')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tmsw_coverage_areas');
    }
}
