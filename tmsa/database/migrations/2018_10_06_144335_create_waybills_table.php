<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWaybillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('waybills', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('company_id')
                  ->unsigned()
                  ->index();

            $table->foreign('company_id')
                  ->references('id')
                  ->on('companies')
                  ->onDelete('cascade');

            $table->string('waybillNo');

            $table->string('waybillFile');

            $table->integer('resource_id')
                  ->unsigned()
                  ->index();

            $table->foreign('resource_id')
                  ->references('id')
                  ->on('carrier_resource_details')
                  ->onDelete('cascade');

            $table->integer('order_id')
                  ->unsigned()
                  ->index();

            $table->foreign('order_id')
                  ->references('id')
                  ->on('service_requests')
                  ->onDelete('cascade');

            $table->integer('status')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('waybills');
    }
}
