<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrucksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trucks', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('truck_owner_id')
                ->unsigned()
                ->index();
            $table->foreign('truck_owner_id')
                ->references('id')
                ->on('truck_owners')
                ->onDelete('cascade');

            $table->integer('truck_status_id')
                ->unsigned()
                ->index();
            $table->foreign('truck_status_id')
                ->references('id')
                ->on('truck_statuses')
                ->onDelete('cascade');

            $table->integer('field_agent_id')
                ->unsigned()
                ->index();
            $table->foreign('field_agent_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->integer('finance_officer_id')
                ->unsigned()
                ->index()->nullable();
            $table->foreign('finance_officer_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->string('plate_number');
            $table->string('driver_name');
            $table->string('driver_phone');
            $table->string('type_of_truck');
            $table->string('driver_license_doc');
            $table->string('git_doc');
            $table->string('vehicle_license_doc');
            $table->string('vehicle_insurance_doc');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trucks');
    }
}
