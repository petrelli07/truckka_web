<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::prefix('nWBbsRfgFm5JKb5FNSysh')->group(function () {
    Route::any('/', ['uses'=>'Exchange\GeneralController@home', 'as'=>'home.gethome']);
    Route::any('/how-the-exchange-works',['uses'=>'Home@getAboutTheExchange', 'as'=>'home.aboutdexchange']);
    Route::any('/benefits-of-the-exchange',['uses'=>'Home@getBenefistOfTheExchange', 'as'=>'home.benefit.exchange']);
    Route::any('/about-us',['uses'=>'Home@getAboutUS', 'as'=>'home.get.about.us']);
    Route::any('/our-services',['uses'=>'Home@getOurServices', 'as'=>'home.get.our.services']);
    Route::any('/our-ecosystem',['uses'=>'Home@getCustomerSegment','as'=>'home.get.customer.seg']);
    Route::any('/login', ['uses'=>'Home@getLogin','as'=>'home.anylogin']);
    Route::any('/register',['uses'=>'Home@getRegister', 'as'=>'home.any.register']);
	Route::any('/contact-us',['uses'=>'Home@ContactUS','as'=>'home.contact.us']);
	Route::any('/invoice', 'Home@Invoice');
//});

//// TRUCK OWNERS ////
Route::any('/truckowners','Home@anyTruckOwners');
Route::any('/add-new-truck','Home@AddNewTruck');
Route::any('/view-truck','Home@anyTruckDetails');
Route::any('/truck-owners-payment-history','Home@anyTruckOwnersPayment');

//// END OF TRUCK OWNERS ///

//// ADMIN ////
Route::any('/admin','Home@anyAdmin');
Route::any('/admin-payment','Home@anyAdminPayment');
Route::any('/admin-invoice','Home@anyAdminInvoice');
Route::any('/admin-assign-order','Home@anyAdminOrder');
//// END OF ADMIN ///
/*demo*/


Route::any('/nWBbsRfgFm5JKb5FNSyshBIERxakU2/demo', 'Exchange\GeneralController@home');

Route::any('/nWBbsRfgFm5JKb5FNSyshBIERxakU2/home','Home@getHome');

Route::any('nWBbsRfgFm5JKb5FNSyshBIERxakU2/about-the-exchange','Home@getAboutTheExchange');
Route::any('nWBbsRfgFm5JKb5FNSyshBIERxakU2/about-us','Home@getAboutUS');
Route::any('nWBbsRfgFm5JKb5FNSyshBIERxakU2/our-services','Home@getOurServices');
Route::any('nWBbsRfgFm5JKb5FNSyshBIERxakU2/our-customer-segment','Home@getCustomerSegment');
Route::any('nWBbsRfgFm5JKb5FNSyshBIERxakU2/login','Home@anyLogin');
Route::any('nWBbsRfgFm5JKb5FNSyshBIERxakU2/register','Home@anyRegister');
Route::any('nWBbsRfgFm5JKb5FNSyshBIERxakU2/contact-us','Home@ContactUS');
Route::any('nWBbsRfgFm5JKb5FNSyshBIERxakU2/client','Home@anyIndex');

//Route::any('/', 'Exchange\GeneralController@home1');


/*Route::any('/', function ()
{
    return view('auth.login');
});*/


Auth::routes();

/*General Purpose Routes and Controllers*/
Route::any('/unauthorized','GeneralPurposeController@unauthorized');

Route::any('/client/map/view/{id?}/{history?}','ClientControllers\TrackingController@getClientResourceView');
Route::any('/client/map/resources','ClientControllers\TrackingController@getResources');
Route::any('/client/map/','ClientControllers\TrackingController@get');


Route::any('/carrier/map/view/{id?}','CarrierControllers\TrackingController@getCarrierResourceView');
Route::any('/carrier/map/resources','CarrierControllers\TrackingController@getResources');


Route::any('/client/map/viewHistory','ClientControllers\TrackingController@getClientResourceHistory');

Route::any('/viewHistory/{gpsid}/{orderId}','ClientControllers\TrackingController@getResourceHistory');


Route::any('/admin/map','AdminControllers\TrackingController@allResources');



Route::get('/profile','UserProfileController@UserProfile');
Route::get('/testInvoice','GeneralPurposeController@testInvoice');



//////////////Modules/////////////////////////////////////////////////////
Route::any('/allmodules/client','AdminControllers\modulesCompiler@viewAllModules');

Route::any('adminSaveNewRole/{userType}','AdminControllers\modulesCompiler@saveNewRole');

Route::any('/admincreate/role/{userType}','AdminControllers\modulesCompiler@creatRoleView');

Route::any('/allmodules/admin','AdminControllers\modulesCompiler@viewAllModules');
Route::any('/allmodules/carrier','AdminControllers\modulesCompiler@viewAllModules');

Route::any('/deployedmodules/client','AdminControllers\modulesCompiler@viewdeployedModules');
Route::any('/deployedmodules/admin','AdminControllers\modulesCompiler@viewdeployedModules');
Route::any('/deployedmodules/carrier','AdminControllers\modulesCompiler@viewdeployedModules');

Route::any('/deployModule','AdminControllers\modulesCompiler@deployModules');
Route::any('/undeployModule','AdminControllers\modulesCompiler@undeployModules');

Route::any('{userType}/modules/manage/{companyId}','AdminControllers\modulesCompiler@CompanyModuleView');


//////////////////////////////////////////////////////////////////////////


Route::any('/changeYourPass','GeneralPurposeController@changePass')->name('changePass');

Route::any('/resetPassword', 'GeneralPurposeController@resetPassword');

Route::any('/performance', 'GeneralPurposeController@performance');
/*function () {
	    return view('client.performance.performance');
	});*/

Route::any('/checkOrigins', 'ClientControllers\ServiceRequestController@checkOrigins');




/*Admin Controllers and Routes*/

Route::middleware([/*'Admin',*/ 'checkPassword'])->group(function () {


	Route::any('/home', 'AdminControllers\HomeController@index')->name('home1');

	Route::any('/createNew', 'AdminControllers\UserManagementController@create');


	Route::any('/viewClientOrders', 'AdminControllers\ServiceRequestController@showAllForCustomer');
	Route::any('/viewAllClients', 'AdminControllers\UserManagementController@viewAllClients');
	Route::any('/createClientAgent/{id}', 'AdminControllers\UserManagementController@createClientAgent');
	Route::any('/viewClientAgent/{id}', 'AdminControllers\UserManagementController@viewClientAgent');
	Route::any('/createNewClientAgent', 'AdminControllers\UserManagementController@saveClientAgent');
	Route::any('/assignResourceToOrder', 'AdminControllers\ServiceRequestController@assignResourceToOrder');
	Route::any('/createNewUser', 'AdminControllers\UserManagementController@createNewUser');
	Route::any('/createNewClient', 'AdminControllers\UserManagementController@createNewClient');
	Route::any('/createNewCarrier', 'AdminControllers\UserManagementController@createNewCarrier');
	Route::any('/createAdmin', 'AdminControllers\UserManagementController@createAdmin');
	//Route::any('/viewOrders', 'ServiceRequestController@showAllForCustomer');
	Route::any('/viewClientOrderDetails/{serviceIDNo}', 'AdminControllers\ServiceRequestController@viewOrderDetails');
	Route::any('/searchResources/{serviceIDNo}', 'AdminControllers\ServiceRequestController@searchResources');
	Route::any('/carrierDetail/{user_id}/{serviceIDNo}', 'AdminControllers\ServiceRequestController@carrierDetail');
	Route::any('/viewResource/{resource_id}', 'AdminControllers\ServiceRequestController@viewResource');
	Route::any('/sendHaulageRequest', 'AdminControllers\ServiceRequestController@sendHaulageRequest');
	Route::any('/createInvoiceForCarrier/{serviceIDNo}', 'AdminControllers\ServiceRequestController@createInvoiceForCarrier');
	Route::any('/createInvoiceForClient/{serviceIDNo}', 'AdminControllers\ServiceRequestController@createInvoiceForClient');
	Route::any('/carrierProceed/{id}', 'AdminControllers\ServiceRequestController@carrierProceed');
	Route::any('/viewAllResources', 'AdminControllers\ReportController@viewAllResources');
	Route::any('/viewAllUsers', 'AdminControllers\ReportController@allUsers');
	Route::any('/editIncident', 'AdminControllers\IncidentManagementController@editIncidentThread');
	Route::any('/incidentThread/{id}', 'AdminControllers\IncidentManagementController@viewIncidentThread');
	Route::any('/updateIncidentThread/{id}', 'AdminControllers\IncidentManagementController@updateIncidentThread');
	Route::any('/closeIncidentThread/{id}', 'AdminControllers\IncidentManagementController@closeIncidentThread');;
	Route::any('/incidentManagement', 'AdminControllers\IncidentManagementController@viewAllIncidents');
	Route::any('/allGPS', 'AdminControllers\TrackingController@fetchAllOrders');
	Route::any('/viewResourcesGPSDetails/{id}', 'AdminControllers\TrackingController@fetchAllGPSDetails');
	Route::any('/viewOnMap/{gpsid}', 'AdminControllers\TrackingController@viewOnMap');
	Route::any('/newRoute', 'AdminControllers\RouteController@newRoute');
	Route::any('/newRoute', 'AdminControllers\RouteController@newRoute');
	Route::any('/loadSelected', 'AdminControllers\ServiceRequestController@loadSelected');
	Route::any('/assignCarriers', 'AdminControllers\ServiceRequestController@assignResourceToOrder');
	Route::any('/closedOrders', 'AdminControllers\ServiceRequestController@closedOrders');
	Route::any('/manageATLs', 'AdminControllers\ServiceRequestController@manageATLS');
	Route::any('/searchATL', 'AdminControllers\ServiceRequestController@searchATL');
	

	Route::any('/getResourceOrders', 'AdminControllers\ServiceRequestController@getResourceOrders');
	Route::any('/selectOrderATL/{id}', 'AdminControllers\ServiceRequestController@selectOrderATL');
	Route::any('/addATL', 'AdminControllers\ServiceRequestController@addATL');
	Route::any('/createClientInvoice', 'AdminControllers\ServiceRequestController@getCompanyDetailsClientInvoice');
	Route::any('/getClientATLs', 'AdminControllers\ServiceRequestController@getClientATLs');
	Route::any('/createAdminInvoice', 'AdminControllers\ServiceRequestController@createAdminInvoice');



	Route::any('/atlSub', 'AdminControllers\ServiceRequestController@atlSub');
	Route::any('/viewOrderResources/{serviceIDNo}', 'AdminControllers\OrderDocumentation@viewOrderResources');//origin
	Route::any('/viewOrderResourcesDestination/{serviceIDNo}', 'AdminControllers\OrderDocumentation@viewOrderResourcesDestination');//dest
	Route::any('/uploadDocs', 'AdminControllers\OrderDocumentation@saveOrderDocumentOrigin');
	Route::any('/uploadDocsDest', 'AdminControllers\OrderDocumentation@saveOrderDocuments');
	Route::any('/viewOrderResources/{serviceIDNo}', 'AdminControllers\OrderDocumentation@viewOrderResources');
	Route::any('/haulageResourceRequest', 'AdminControllers\ServiceRequestController@sendHaulageRequest');

	Route::any('/userMods', 'AdminControllers\UserManagementController@testFunc');

});



/*Suupliers Routes*/

Route::middleware([/*'Supplier',*/ 'checkPassword'])->group(function () {
	
	Route::any('/all_suppliers_supply_orders','ClientControllers\SupplierController@allSupplierOrders');
	Route::any('/createQuoteForOrder/{id}','ClientControllers\SupplierController@createQuote');
	Route::any('/submitNewSupplyQuote','ClientControllers\SupplierController@submitQuote');
	Route::any('supplier/viewQuote/{id}','ClientControllers\SupplierController@supplierViewQuote');

	Route::any('/supply/downloadPurchaseOrder/{id}','ClientControllers\SupplierController@downloadPurchaseOrder');
	
});


/*CLient Controllers and Routes*/

Route::middleware(['checkPassword'/*,'Client', 'permissionChecker'*/])->group(function () {

	Route::any('/clientHome', 'ClientControllers\HomeController@index');
	Route::any('/create_order', 'ClientControllers\HomeController@create_order');
	// Route::any('/resetPassword', 'ServiceRequestController@resetPassword');
	Route::any('/createNewOrder', 'ClientControllers\ServiceRequestController@store');
	Route::any('/viewOrders', 'ClientControllers\ServiceRequestController@showAllForCustomer');
	Route::any('/viewInvoice/{serviceIDNo}', 'ClientControllers\ServiceRequestController@viewInvoice');
	Route::any('/approveInvoice/{serviceIDNo}', 'ClientControllers\ServiceRequestController@approveInvoice');
	Route::any('/finishTransaction/{serviceIDNo}', 'ClientControllers\ServiceRequestController@finishTransaction');
	Route::any('/openDispute/{serviceIDNo}', 'ClientControllers\ServiceRequestController@openDispute');
	Route::any('/carrierProceed/{serviceIDNo}', 'ClientControllers\ServiceRequestController@carrierProceed');
	Route::any('/viewOrderDetailsClient/{serviceIDNo}', 'ClientControllers\ServiceRequestController@viewOrderDetails');
	Route::any('/allInvoices', 'ClientControllers\ServiceRequestController@allInvoices');
	Route::any('/viewInvoiceDet/{invoice_no}', 'ClientControllers\ServiceRequestController@viewInvoiceDet');
	Route::any('/demurrage', 'ClientControllers\ServiceRequestController@allDemurrage');
	Route::any('/settlement', 'ClientControllers\ServiceRequestController@settlement');
	Route::any('/incidentManagement', 'ClientControllers\IncidentManagementController@viewAllIncidents');
	Route::any('/incidentThread/{id}', 'ClientControllers\IncidentManagementController@viewIncidentThread');
	
	Route::any('/allGPS', 'ClientControllers\TrackingController@fetchAllOrders');
	Route::any('/viewResourcesGPSDetails/{id}', 'ClientControllers\TrackingController@fetchAllGPSDetails');
	Route::any('/viewOnMap/{gpsid}', 'ClientControllers\TrackingController@viewOnMap');
	Route::any('/allOrders', 'ClientControllers\OrderDocumentationController@allOrders');
	Route::any('/viewOrderResources/{serviceIDNo}', 'ClientControllers\OrderDocumentationController@viewOrderResources');//origin
	Route::any('/viewOrderResourcesDest/{serviceIDNo}', 'ClientControllers\OrderDocumentationController@viewOrderResourcesDest');//Destination
	/*Route::any('/uploadDocs', 'ClientControllers\OrderDocumentationController@saveOrderDocuments');
	Route::any('/uploadDocsDest', 'ClientControllers\OrderDocumentationController@saveOrderDocumentsDest');*/
	Route::any('/generateSettlementSheet','ClientControllers\ServiceRequestController@export');
	Route::any('/downloadPDF/{invoice_no}','ClientControllers\ServiceRequestController@downloadInvoice');
	Route::any('/cancelOrder/{orderNo}','ClientControllers\ServiceRequestController@cancelOrder');
	Route::any('/getAll','ClientControllers\ServiceRequestController@showOrdersForSupervisor');
	
	Route::any('/getAllInvoices','ClientControllers\ServiceRequestController@showInvoicesSupervisor');

	Route::any('/invoicePayments', 'ClientControllers\ServiceRequestController@invoicePayments');

	Route::any('/finance/view_invoice_payments/{invoiceNo}', 'ClientControllers\ServiceRequestController@viewinvoicePayments');

	Route::any('/createConsolInvoice', 'ClientControllers\ServiceRequestController@createConsolInvoice');

	
	Route::any('/finance/download_waybills/{invoiceNo}', 'ClientControllers\ServiceRequestController@downloadWaybills');
    


	//only for suppliers

	Route::any('/supply/create','ClientControllers\SupplierController@createNewSupplyOrder');
	Route::any('/supplier/save','ClientControllers\SupplierController@createNewSupplier');
	Route::any('/suppliers/group/create','ClientControllers\SupplierController@createGroupView');
	Route::any('/suppliers/group/save','ClientControllers\SupplierController@createGroup');
	Route::any('/suppliers/groups','ClientControllers\SupplierController@viewGroups');
	Route::any('/suppliers/group/view/{id}','ClientControllers\SupplierController@viewGroupMembers');
	Route::any('/suppliers/group/{groupId}/member/remove/{id}','ClientControllers\SupplierController@removeGroupMember');
	Route::any('/suppliers','ClientControllers\SupplierController@allSuppliersView');
	Route::any('/supplier/create','ClientControllers\SupplierController@createSupplierView');
	Route::any('/suppliers/group/edit/{id}','ClientControllers\SupplierController@editGroupView');
	Route::any('suppliers/group/edit/save/{id}','ClientControllers\SupplierController@editGroup');
	Route::any('/submitNewSupplyOrder','ClientControllers\SupplierController@submitNewSupplyOrder');
	Route::any('/all_client_supply_orders','ClientControllers\SupplierController@allClientSupplyOrders');
	Route::any('/viewQuote/{id}','ClientControllers\SupplierController@viewQuote');
	Route::any('/markComplete/{id}','ClientControllers\SupplierController@markComplete');
	Route::any('/approveQuote/{id}','ClientControllers\SupplierController@approveQuote');
	Route::any('/supply/downloadInvoice/{id}','ClientControllers\SupplierController@downloadInvoice');


	
	
	Route::any('/user/new','ClientControllers\UserManagementController@createNewUserView');
	Route::any('/users/all','ClientControllers\UserManagementController@allUsersView');
	Route::any('/edit/permission/{userDetails}','ClientControllers\UserManagementController@edtUserPermissionView');
	Route::any('/role/edit/{roleId}','ClientControllers\UserManagementController@editRoleView');
	Route::any('/role/new','ClientControllers\UserManagementController@newRoleView');
	Route::any('/user/create','ClientControllers\UserManagementController@createNewUser');
	Route::any('/role/all','ClientControllers\UserManagementController@allRolesView');
	Route::any('/getpermissions','ClientControllers\UserManagementController@jsonRetrieveModules');
	Route::any('/changeUserPermission','ClientControllers\UserManagementController@changeUserPermission');
	Route::any('/saveNewRole','ClientControllers\UserManagementController@saveNewRole');
	Route::any('/updateRole','ClientControllers\UserManagementController@editRole');
});


/*Carrier Route*/
Route::middleware([/*'Carrier',*/ 'checkPassword'])->group(function () {

	Route::any('/transactions', 'CarrierControllers\HomeController@index')->name('transactions');
	Route::any('/viewOrderDetails/{serviceIDNo}', 'CarrierControllers\HomeController@viewOrderDetails');
	Route::any('/carrier', 'CarrierControllers\HomeController@index');
	Route::any('/wallet/transactions', 'CarrierControllers\HomeController@transactionWallet');
	Route::any('/invoice/view/{invoiceId}', 'CarrierControllers\HomeController@viewInvoice');

	Route::any('/all_haulage_requests', 'CarrierControllers\HomeController@allHaulageRequests');
	Route::any('/orderDetails/{serviceIDNo}', 'CarrierControllers\HomeController@orderDetails');

	Route::any('/carrier/resource/create', 'CarrierControllers\ResourceController@addresource');
	Route::any('/add-new-resource', 'CarrierControllers\ResourceController@addnewResource');
	Route::any('/carrier/resources', 'CarrierControllers\ResourceController@viewAllResource');
	Route::any('/edit-resources', 'CarrierControllers\ResourceController@editResource');
	Route::any('/edit-resources/{resourceId}', 'CarrierControllers\ResourceController@editResource');
	Route::any('/save-edit-resource', 'CarrierControllers\ResourceController@saveEditResource');
	Route::any('/deactivate-resource', 'CarrierControllers\ResourceController@deactivateResource');
	Route::any('/deactivate-resource/{resourceId}', 'CarrierControllers\ResourceController@deactivateResource');
	Route::any('/activate-resource', 'CarrierControllers\ResourceController@activateResource');
	Route::any('/all-orders', 'CarrierControllers\ordersController@allOrders');
	Route::any('/activate-resource/{resourceId}', 'CarrierControllers\ResourceController@activateResource');
	Route::any('/add-resource-to-order', 'CarrierControllers\ordersController@addResourceTo');
	Route::any('/add-resource-to-order/{orderId}', 'CarrierControllers\ordersController@addResourceTo');
	Route::any('/carrier/resources/attached', 'CarrierControllers\TrackingController@getAttachedResources');

	Route::any('/attach-resource', 'CarrierControllers\ordersController@attachResource');

});


Route::prefix('spotta')->group(function () {

    Route::any('home','Spotta\HomeController@index');//landing page

/*spotta client*/
    Route::any('client/home','Spotta\SpotOrders@clientHome');//client post-login page

    Route::any('checkOrigins','Spotta\HomeController@checkOrigins');//get destinations for origin

    Route::any('getQuote','Spotta\SpotOrders@getQuote');//generate quote for order

    Route::any('submitQuote','Spotta\SpotOrders@saveQuoteSession');//save quotes to session

    Route::any('client/finance/make_payment/{orderNumber}','Spotta\SpotOrders@make_payment');//payment page

    Route::any('client/finance/payments','Spotta\SpotOrders@my_payments');//payment page

    Route::any('client/finance/bank_payment','Spotta\SpotOrders@bank_payment');//payment form submit

    Route::any('client/finance/invoices','Spotta\SpotOrders@my_invoices');//display invoices
/*end spotta client*/

/*spotta carrier*/
	Route::any('carrier/home','Spotta\SpotOrders@carrierHome');//carrier default landing page
	Route::any('carrier/resources/new_resource','Spotta\SpotOrders@add_truck_view');//carrier default landing page
	Route::any('carrier/resources/add-new-resource','Spotta\SpotOrders@add_new_truck');//carrier new resource
});

//routes for truck-registry management and payments
Route::prefix('registry')->group(function () {

    /*administrator routes*/
    Route::any('administrator/home','TruckOrderRegistry\Administrator\UserManagementController@create_new_user_view');

    // create new user view/*
	/*Route::any('management/user_management/create_new_user','TruckOrderRegistry\Management\UserManagementController@create_new_user_view');

	Route::any('management/user_management/create_new_user','TruckOrderRegistry\Management\UserManagementController@create_new_user_view');*/

// create new user process
	Route::any('administrator/user_management/create_new_user_process','TruckOrderRegistry\Administrator\UserManagementController@create_new_user_process');
//end user management routes

    /*end administrator routes*/

// field ops routes

//landing page for field agents
	Route::any('fieldOps/home','TruckOrderRegistry\FieldOps\FieldOperationsController@index');

//part payment request view for field agents
	Route::any('fieldOps/request/request_payment','TruckOrderRegistry\FieldOps\FieldOperationsController@request_part_payment_view');

//balance payment request view for field agents
	//Route::any('fieldOps/request/request_balance_payment','TruckOrderRegistry\FieldOps\FieldOperationsController@balance_payment_request_view');

//process part payments
	Route::any('fieldOps/request/process_part_payment','TruckOrderRegistry\FieldOps\FieldOperationsController@process_part_payment');

//process balance payments
	Route::any('fieldOps/request/process_balance_payment','TruckOrderRegistry\FieldOps\FieldOperationsController@process_balance_payment');
	Route::any('fieldOps/request/all_requests','TruckOrderRegistry\FieldOps\FieldOperationsController@all_payment_requests_view');
	//all payment status view

    //change password view
    Route::any('fieldOps/change_password','TruckOrderRegistry\FieldOps\FieldOperationsController@change_password_view');
    Route::any('finance/change_password','TruckOrderRegistry\FinOps\FinancialOperationsController@change_password_view');
    Route::any('management/change_password','TruckOrderRegistry\Management\UserManagementController@change_password_view');

    //reset password
    Route::any('password_reset','TruckOrderRegistry\GeneralController@reset_password');



    ////////////transporter create and trucks
    //create transporter view
    Route::any('fieldOps/create_transporter','TruckOrderRegistry\FieldOps\FieldOperationsController@create_transporter_view');

    //create transporter process
    Route::any('fieldOps/create_transporter_process','TruckOrderRegistry\FieldOps\FieldOperationsController@create_transporter_process');

    //view all truck owners
    Route::any('fieldOps/truck_owners','TruckOrderRegistry\FieldOps\FieldOperationsController@truck_owners_view');

    Route::any('fieldOps/add-truck/{id}','TruckOrderRegistry\FieldOps\FieldOperationsController@add_new_truck_view');

    Route::any('fieldOps/add_truck_process','TruckOrderRegistry\FieldOps\FieldOperationsController@add_truck_process');
    //end transporter and trucks

    //view truck by owners and all trucks
    Route::any('fieldOps/view-truck-by-owners/{id}','TruckOrderRegistry\FieldOps\FieldOperationsController@truck_by_owners_view');
    Route::any('fieldOps/view-all-trucks','TruckOrderRegistry\FieldOps\FieldOperationsController@all_truck_view');
    //end of view truck
/*end field ops routes*/

//user management routes



//FINOPS//
Route::any('finops-home','TruckOrderRegistry\FinOps\FinancialOperationsController@index');//landing page for view FinOps

Route::any('approve-part-payment-finops','TruckOrderRegistry\FinOps\FinancialOperationsController@viewManagementApprovedPartPaymentRequest');//view  part payment requets landing page

Route::any('approve-balance-payment-finops','TruckOrderRegistry\FinOps\FinancialOperationsController@viewManagementApprovedBalancePaymentRequest');//view  balance payment request landing page

    /**/
    Route::any('/fin/bulkpayment','TruckOrderRegistry\FinOps\FinancialOperationsController@bulkPaymentPartPayment');

    Route::any('/fin/bulkpayment_balance','TruckOrderRegistry\FinOps\FinancialOperationsController@bulkPaymentBalancePayment');

    //view more part payment
Route::any('fin/view_more/{registry_number}','TruckOrderRegistry\FinOps\FinancialOperationsController@viewMorePartPayment');

    //view more balance payment
Route::any('fin/view_more_balance/{registry_number}','TruckOrderRegistry\FinOps\FinancialOperationsController@viewMoreBalancePayment');

Route::any('approving-part-payment-finops','TruckOrderRegistry\FinOps\FinancialOperationsController@finOpsPartPaymentRequest');//process part payment FinOps

Route::any('fins','TruckOrderRegistry\FinOps\FinancialOperationsController@finOpsBalancePaymentRequest')->name('approve_balance');
//approving balance payment FinOps

 //finalize part payment using OTP
Route::any('finalize_part_payment','TruckOrderRegistry\FinOps\FinancialOperationsController@finalizePartPayment');

Route::any('finalize_part_bulk_payment','TruckOrderRegistry\FinOps\FinancialOperationsController@finalize_part_bulk_payment');

//finalize balance using OTP
Route::any('finalize_balance_payment','TruckOrderRegistry\FinOps\FinancialOperationsController@finalizeBalancePayment');

Route::any('finalize_bulk_balance_payment','TruckOrderRegistry\FinOps\FinancialOperationsController@finalize_bulk_balance_payment');
//END OF FINOPS//

    //populate banks table with bank list from paystack use once
    //Route::get('getBanks','TruckOrderRegistry\FinOps\FinancialOperationsController@getBanks');



// OPERATIONS MGT//
    //landing page for view Mgt
Route::any('home1-mgt','TruckOrderRegistry\Management\FieldOpsManagementController@index');

//landing page for view Approve part payment Mgt
Route::any('view-approve-part-payment-mgt','TruckOrderRegistry\Management\FieldOpsManagementController@viewPartPaymentRequest');

//landing page for balance request
Route::any('view-approve-balance-payment-mgt','TruckOrderRegistry\Management\FieldOpsManagementController@viewBalancePaymentRequest');

//approving part payment Mgt
Route::any('ops/approve_part_payment/{registry_no}','TruckOrderRegistry\Management\FieldOpsManagementController@processPartPaymentRequest');

//approving part payment Mgt
Route::any('ops/approve_part_payment/{registry_no}','TruckOrderRegistry\Management\FieldOpsManagementController@processPartPaymentRequest');

//decline part payment Mgt
Route::any('/ops/decline_part_payment/{registry_no}','TruckOrderRegistry\Management\FieldOpsManagementController@partPaymentDeclinedOps');

    //process balance payment Mgt
Route::any('ops/approve_balance_payment/{registry_no}','TruckOrderRegistry\Management\FieldOpsManagementController@processBalancePaymentRequest');

    //view all payment request for ops
Route::any('ops/view_more/{registry_number}','TruckOrderRegistry\Management\FieldOpsManagementController@viewMore');

    //view all payment request for finance
Route::any('ops/view_more/{registry_number}','TruckOrderRegistry\FinOps\FinancialOperationsController@viewMore');

    //view more part payment
Route::any('ops/view_more_part_payment/{registry_number}','TruckOrderRegistry\Management\FieldOpsManagementController@viewMorePartPayment');

    //view more balance payment
Route::any('ops/view_more_balance/{registry_number}','TruckOrderRegistry\Management\FieldOpsManagementController@viewMoreBalancePayment');

Route::any('download_waybills','TruckOrderRegistry\Management\FieldOpsManagementController@downloadWaybillFile');//download waybill files

//END OF OPERATIONS MGT//

});

/*routes for exchange*/

Route::prefix('exchange')->group(function () {


    /*general routes*/
    Route::any('/checkOrigins','Exchange\GeneralController@checkOrigins');
    Route::any('getQuote','Exchange\GeneralController@getQuote');
    Route::any('/submitQuote','Exchange\GeneralController@saveQuote');
    Route::any('/register_supplier_capacity','Exchange\GeneralController@register_supplier_capacity');
    Route::any('/customer/sign_up_customer','Exchange\GeneralController@register_customer');
    Route::any('/customer/sign_up_customer_view','Exchange\GeneralController@register_customer_view');
    Route::any('/transporter/submit_request','Exchange\GeneralController@submitRequest');
    Route::any('/unregistered_user','Exchange\GeneralController@unregisteredUser');
    Route::any('/confirm_account/{confirmation_code}','Exchange\GeneralController@confirmAccount');

    Route::any('/supplier/sign_up_supplier_view','Exchange\GeneralController@register_supplier_view');
    Route::any('/supplier/sign_up_supplier','Exchange\GeneralController@register_new_supplier');
    /*end general routes*/

    /*customer routes*/
    Route::any('/customer/home','Exchange\Customer\OrderManagementController@index');
    Route::any('/customer/view_truck_details/{order_number}','Exchange\Customer\OrderManagementController@viewTruckDetails');
    Route::any('/customer/client-area/create_order','Exchange\Customer\OrderManagementController@createOrder');
    Route::any('/customer/client-area/submit_new_order','Exchange\Customer\OrderManagementController@submitOrder');
    Route::any('/customer/client-area/process-order','Exchange\Customer\OrderManagementController@processOrder');
    Route::any('/customer/client-area/confirm-delivery/{id}','Exchange\Customer\OrderManagementController@confirmDelivery');
    Route::any('/customer/client-area/invoices','Exchange\Customer\FinancialManagementController@viewAllInvoices');
    Route::any('/customer/client-area/unpaid_invoices','Exchange\Customer\FinancialManagementController@viewAllUnpaidInvoices');
    Route::any('/customer/client-area/pay_invoice/{reference_number}','Exchange\Customer\FinancialManagementController@payInvoice');
    Route::any('/customer/update_customer','Exchange\Customer\FinancialManagementController@updateCustomer');
    Route::any('/customer/payment/successful','Exchange\Customer\FinancialManagementController@successfulPayment');
    Route::any('/customer/client-area/complete_transaction/{reference}','Exchange\Customer\FinancialManagementController@completeOrder');
    Route::any('/customer/client-area/complete_order','Exchange\Customer\FinancialManagementController@finishOrder');
    Route::any('/customer/payment/webhook/','Exchange\Customer\FinancialManagementController@paystackWebhook');
    Route::any('/customer/invoice_details/{invoice_number}','Exchange\Customer\FinancialManagementController@invoiceDetail');
    /*end customer routes*/

    /*truck owner/driver controller*/
    Route::any('/truck_owner_driver/home','Exchange\OwnerDriver\OrderManagementController@home');
    Route::any('/truck_owner/commit_trucks','Exchange\OwnerDriver\OrderManagementController@commitTrucks');
    Route::any('/truck_supplier/add_trucks/{order_number}','Exchange\OwnerDriver\OrderManagementController@addTrucks');
    Route::any('/truck_supplier/view_order_details/{order_number}','Exchange\OwnerDriver\OrderManagementController@viewOrderDetails');
    Route::any('/truck_owner/update_account_details','Exchange\OwnerDriver\OrderManagementController@updateAccountDetailsView');
    Route::any('/truck_owner/save_account_details','Exchange\OwnerDriver\OrderManagementController@updateAccountDetails');
    Route::any('/truck_owner/save_account_details','Exchange\OwnerDriver\OrderManagementController@updateAccountDetails');
    /*end truck owner/driver controller*/

    /*exchange admin routes*/
    Route::any('/admin/home','Exchange\Admin\OrderManagementController@index');
    Route::any('/admin/confirm_order/{order_number}','Exchange\Admin\OrderManagementController@confirmOrder');
    Route::any('/admin/match_order/{order_number}','Exchange\Admin\OrderManagementController@matchOrder');
    Route::any('/admin/match_supplier_order','Exchange\Admin\OrderManagementController@matchSupplierOrder');
    Route::any('/admin/match_transporter/{order_number}','Exchange\Admin\OrderManagementController@matchTransporter');
    Route::any('/admin/add_response/{order_number}','Exchange\Admin\OrderManagementController@addResponse');
    Route::any('/admin/submit_response','Exchange\Admin\OrderManagementController@submitResponse');
    Route::any('/admin/transporter_requests','Exchange\Admin\OrderManagementController@transporterRequest');

    //manage routes
    Route::any('/admin/routes/all_routes','Exchange\Admin\ExchangeRouteController@allRoutes');
    Route::any('/admin/routes/suspend_route/{id}','Exchange\Admin\ExchangeRouteController@deactivateRoute');
    Route::any('/admin/routes/activate_route/{id}','Exchange\Admin\ExchangeRouteController@activateRoute');
    Route::any('/admin/route/add_new','Exchange\Admin\ExchangeRouteController@createNewRouteView');
    Route::any('admin/route/submit_new_route','Exchange\Admin\ExchangeRouteController@createNewRoute');
    Route::any('admin/routes/edit_route/{id}','Exchange\Admin\ExchangeRouteController@editRoute');
    Route::any('admin/routes/update_route','Exchange\Admin\ExchangeRouteController@updateRoute');

    Route::any('admin/orders/payment_requests','Exchange\Admin\ExchangePaymentRequestController@paymentRequestView');
    /*end exchange admin routes*/
});

/*routes for TMSW*/

Route::prefix('tmsw')->group(function () {



    /*@superadministrator routes*/
    Route::any('superAdministrator/orders','TMSW\System\TruckOrderController@orders');
    Route::any('superAdministrator/responses/{reference_number}','TMSW\System\TruckOrderController@truckDetails');
    Route::any('superAdministrator/add_Responses/{reference}','TMSW\System\TruckOrderController@addResponses');
    Route::any('superadministrator/order/add_response','TMSW\System\TruckOrderController@submitResponse');
    Route::any('superadministrator/order/update_responses','TMSW\System\TruckOrderController@updateResponse');
    /*@end superadministrator routes*/


	Route::any('/client/','TMSW\Client\UserController@getIndex');
	Route::any('/group/roles','TMSW\Client\UserController@getGroupRoles');
	Route::any('/admin/group/create','TMSW\Client\UserController@createGroupView');
	Route::any('/admin/group/save','TMSW\Client\UserController@createGroupSave');

	Route::any('/transporter/','TMSW\Transporter\UserController@getIndex');
	Route::any('/supply/order/participant/{id?}','TMSW\Client\SupplierController@getSupplyOrderParticipants');

    Route::any('/supply/order/view_purchase_order/{id}','TMSW\Client\SupplierController@viewPurchaseOrder');
    Route::any('/supply/order/view_quote/{id}','TMSW\Client\SupplierController@viewPurchaseOrder');

	Route::prefix('supplier')->group(function (){
        Route::any('/view_quote/{id}/{supply_id}','TMSW\Client\SupplierController@viewQuote');
		Route::any('/orders/quote/save/{id?}','TMSW\Supplier\UserController@uploadQuote');
		Route::any('/orders/','TMSW\Supplier\UserController@allSuppliersRequestsView');
		Route::any('/create/','TMSW\Client\SupplierController@createSupplierView');
		Route::any('/edit/{id?}','TMSW\Client\SupplierController@editSupplierView');
		Route::any('/edit/save/{id?}','TMSW\Client\SupplierController@editSupplier');
		Route::any('/save/','TMSW\Client\SupplierController@createNewSupplier');
		Route::any('/all/','TMSW\Client\SupplierController@allSuppliersView');
		Route::any('/supply/order/create','TMSW\Client\SupplierController@createSupplyView');
		Route::any('/supply/order/all','TMSW\Client\SupplierController@allClientSupplyOrders');
		Route::any('/supply/order/save','TMSW\Client\SupplierController@submitNewSupplyOrder');
		Route::any('/groups/{id?}','TMSW\Client\SupplierController@suppliersGroupView');
		Route::any('/group/create','TMSW\Client\SupplierController@createGroupView');
		Route::any('/group/edit/{id?}','TMSW\Client\SupplierController@editGroupView');
		Route::any('/group/edit/save/{id?}','TMSW\Client\SupplierController@editGroup');		
		Route::any('/group/create/save','TMSW\Client\SupplierController@createGroup');
		Route::any('/status/group/{id?}/{type?}','TMSW\Client\SupplierController@updateGroupStatus');
		Route::any('/status/{id?}/{type?}','TMSW\Client\SupplierController@updateSupplierStatus');

		Route::any('/status/supply/{id?}/{type?}','TMSW\Client\SupplierController@updateSupplieStatus');
		Route::any('/group','TMSW\Client\SupplierController@allGroupsView');
		Route::any('/status/quote/{id?}/{type?}','TMSW\Client\SupplierController@quoteStatus');

	});

	/* routes for client */
	Route::any('/client/home','TMSW\Client\UserController@getIndex');
	Route::any('/client/create-user', ['uses'=>'TMSW\Client\UserController@CreateUsers', 'as'=>'client.get.user']); ////client add user page
	Route::any('/client/store-user', ['uses'=>'TMSW\Client\UserController@StoreUsers', 'as'=>'client.store.user']); ////client store user
	Route::any('/client/manage-user', ['uses'=>'TMSW\Client\UserController@ViewUsers', 'as'=>'client.manage.user']); ////client manage user
	
	Route::any('/client/suspend-user', ['uses'=>'TMSW\Client\UserController@SuspendUsers', 'as'=>'client.suspend.user']); ////client suspend user
	Route::any('/client/unsuspend-user', ['uses'=>'TMSW\Client\UserController@UnsuspendUsers', 'as'=>'client.unsuspend.user']); ////client suspend user
	Route::any('/client/delete-user', ['uses'=>'TMSW\Client\UserController@DeleteUsers', 'as'=>'client.delete.user']); ////client delete user
	
	/* end routes for client */


	Route::any('/supplier/','TMSW\Supplier\UserController@getIndex');

	/* routes for transporter */
	Route::any('/transporter/home','TMSW\Transporter\UserController@getIndex');
	Route::any('/transporter/create-transporter', ['uses'=>'TMSW\Transporter\UserController@CreateTransporter', 'as'=>'transporter.create']); ////transporter add transporter page
	Route::any('/transporter/creating-transporter', ['uses'=>'TMSW\Transporter\UserController@create_transporter_process', 'as'=>'transporter.create.process']); ////transporter add transporter page
	Route::any('/transporter/view-transporter', ['uses'=>'TMSW\Transporter\UserController@ViewTransporter', 'as'=>'view.transporter']); ////transporter add transporter page
	Route::any('/transporter/deactivate-user', ['uses'=>'TMSW\Transporter\UserController@DeactivateTransporter', 'as'=>'transporter.deactivate']); ////client suspend user
	Route::any('/transporter/activate-user', ['uses'=>'TMSW\Transporter\UserController@ActivateTransporter', 'as'=>'transporter.activate']); ////client delete user
	
	
	/* end routes for  transporter */
	Route::any('administrator/home','TMSW\Client\UserController@getIndex');


/*@dealing with creation and maintenance of routes*/
	Route::any('manager/home','TMSW\Client\UserController@getIndex');
	Route::any('manager/manage_routes/','TMSW\Client\TruckOrderController@allRoutes');
	Route::any('manager/routes/suspend_route/{id}','TMSW\Client\TruckOrderController@deactivateRoute');
	Route::any('manager/routes/activate_route/{id}','TMSW\Client\TruckOrderController@activateRoute');
	Route::any('manager/routes/create_new_view','TMSW\Client\TruckOrderController@createRouteView');
	Route::any('manager/routes/create_new_route','TMSW\Client\TruckOrderController@createRoute');
	Route::any('manager/routes/edit_route/{id}','TMSW\Client\TruckOrderController@editRoute');
	Route::any('manager/routes/update_route','TMSW\Client\TruckOrderController@updateRoute');
/*@end*/

/*@ desk officer create and manage order routes*/
    Route::any('create_new_order','TMSW\Client\TruckOrderController@createOrderView');
    Route::any('checkOrigins','TMSW\Client\TruckOrderController@checkOrigins');
    Route::any('submit_new_order','TMSW\Client\TruckOrderController@submitOrder');
    Route::any('user_orders','TMSW\Client\TruckOrderController@userOrders');

    Route::any('/client/view_truck_details/{reference_number}','TMSW\Client\TruckOrderController@truckDetails');
/*@end create and manage order*/

/*@transporters routes*/
    Route::any('/transporter/home','TMSW\Transporter\OrderController@getRequest');
    Route::any('/transporter/add_trucks/{reference_number}','TMSW\Transporter\OrderController@addTrucks');
    Route::any('/transporter/add_truck_details','TMSW\Transporter\OrderController@addTruckDetails');
    Route::any('/transporter/active_orders','TMSW\Transporter\OrderController@activeOrders');
    Route::any('/transporter/upload_waybill/{id}','TMSW\Transporter\OrderController@uploadWaybill');
    Route::any('/transporter/submit_waybill','TMSW\Transporter\OrderController@submitWaybill');

    Route::any('/transporter/invoice_details/{id}','TMSW\Transporter\OrderController@invoiceDetail');
/*@end transporters*/


/*@finance routes*/
    Route::any('finance/home','TMSW\Client\FinanceController@allInvoices');
    Route::any('finance/confirm_truck_details/{invoice_reference_number}','TMSW\Client\FinanceController@truckOrderDetail');
    Route::any('finance/confirm_invoice/{invoice_reference_number}','TMSW\Client\FinanceController@confirmInvoice');

    Route::any('finance/view_invoice_details/{invoice_reference_number}','TMSW\Client\FinanceController@invoiceDetails');
/*@end finance routes*/

	Route::any('desk_officer/home','TMSW\Client\TruckOrderController@userOrders');
	Route::any('operator/manage_waybills','TMSW\Client\TruckOrderController@manageWaybills');
	Route::any('operator/upload_waybill','TMSW\Client\TruckOrderController@uploadWaybill');

	Route::any('/supplier/home','TMSW\Supplier\UserController@getIndex');

	//Route::any('/transporter/home','TMSW\Transporter\UserController@getIndex');

    Route::any('/performance','TMSW\Client\PerformanceController@performance');
    Route::any('/performance/analytics/truck_orders','TMSW\Client\PerformanceController@truck_orders');
    Route::any('/performance/analytics/supply','TMSW\Client\PerformanceController@supply');
    Route::any('/performance/analytics/finance','TMSW\Client\PerformanceController@finance');

    /*contact center*/
    Route::any('/tmsw/contact_center/home','TMSW\System\TruckOrderController@orders');
	/*end contact center*/
	

});

Route::prefix('map')->group(function () {
	Route::any('/home', ['uses'=>'TruckkaMap\GeneralController@index', 'as'=>'map.gethome']);

});

Route::any('/email/temp','Home@emailTemp');

