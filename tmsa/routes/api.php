<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('registry')->group(function () {

Route::apiResource('transporters','API\Registry\Transporter');
Route::apiResource('fieldOps','API\Registry\fieldOps\FieldOperationsController');
Route::apiResource('finops','API\Registry\finOps\FinancialOperationsController');
Route::apiResource('Management','API\Registry\Management\FieldOpsManagementController');

});


