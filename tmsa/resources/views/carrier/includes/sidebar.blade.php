<div class="page-sidebar-wrapper">
                    <!-- BEGIN SIDEBAR -->
                    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                    <div class="page-sidebar navbar-collapse collapse">
                        <!-- BEGIN SIDEBAR MENU -->
                        <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
                        <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
                        <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
                        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                        <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
                        <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                        <ul class="page-sidebar-menu  page-header-fixed page-sidebar-menu-light " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
                            <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
                            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                            <li class="sidebar-toggler-wrapper hide">
                                <div class="sidebar-toggler">
                                    <span></span>
                                </div>
                            </li>
                            <!-- END SIDEBAR TOGGLER BUTTON -->
                            <!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
                          <!-- <li class="nav-item truckka-back nav-item-head">
                                <a href="javascript:;" class="nav-link nav-toggle white bold ">
                                    <div class="flex-container flex-space-between flex-vertical-align">
                                      <span class="title">Orders</span>
                                      <span class=" "><i class="fas fa-plus-square"></i></span>
                                    </div>
                                </a>
                                <ul class="sub-menu box-shadow" > -->
                                    <!-- <li class="nav-item">
                                        <a href="javascript:;" class="nav-link nav-toggle">
                                            <i class="icon-settings"></i>New
                                            <span class="arrow"></span>
                                        </a>
                                        <ul class="sub-menu">
                                            <li class="nav-item">
                                                <a href="javascript:;" target="_blank" class="nav-link">
                                                    <i class="icon-user"></i> Arrow Toggle
                                                    <span class="arrow nav-toggle"></span>
                                                </a>
                                                <ul class="sub-menu">
                                                    <li class="nav-item">
                                                        <a href="#" class="nav-link">
                                                            <i class="icon-power"></i> Sample Link 1</a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a href="#" class="nav-link">
                                                            <i class="icon-paper-plane"></i> Sample Link 1</a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a href="#" class="nav-link">
                                                            <i class="icon-star"></i> Sample Link 1</a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li class="nav-item">
                                                <a href="#" class="nav-link">
                                                    <i class="icon-camera"></i> Sample Link 1</a>
                                            </li>
                                            <li class="nav-item">
                                                <a href="#" class="nav-link">
                                                    <i class="icon-link"></i> Sample Link 2</a>
                                            </li>
                                            <li class="nav-item">
                                                <a href="#" class="nav-link">
                                                    <i class="icon-pointer"></i> Sample Link 3</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="nav-item">
                                        <a href="javascript:;" target="_blank" class="nav-link">
                                            <i class="icon-globe"></i> Arrow Toggle
                                            <span class="arrow nav-toggle"></span>
                                        </a>
                                        <ul class="sub-menu">
                                            <li class="nav-item">
                                                <a href="#" class="nav-link">
                                                    <i class="icon-tag"></i> Sample Link 1</a>
                                            </li>
                                            <li class="nav-item">
                                                <a href="#" class="nav-link">
                                                    <i class="icon-pencil"></i> Sample Link 1</a>
                                            </li>
                                            <li class="nav-item">
                                                <a href="#" class="nav-link">
                                                    <i class="icon-graph"></i> Sample Link 1</a>
                                            </li>
                                        </ul>
                                    </li> -->
                                    <!-- <li class="nav-item inner-nav-item">
                                        <a href="{{url('/create_order')}}" class="nav-link f-parargraph black" style="font-size: 14px;">
                                          <div class="flex-container flex-space-between flex-vertical-align">
                                            <span>Incomplete</span>
                                            <span><i class="fas fa-cart-plus"></i></span>
                                          </div>
                                        </a>
                                    </li> -->
                                   <!-- <li class="nav-item inner-nav-item">
                                        <a href="{{url('/')}}" class="nav-link f-parargraph black" style="font-size: 14px;">
                                          <div class="flex-container flex-space-between flex-vertical-align">
                                            <span>All</span>
                                            <span><i class="fas fa-cart-arrow-down"></i></span>
                                          </div>
                                        </a>
                                    </li>
                                </ul>
                          </li> -->
                          <li class="nav-item truckka-back nav-item-head">
                                <a href="javascript:;" class="nav-link nav-toggle white bold ">
                                    <div class="flex-container flex-space-between flex-vertical-align">
                                      <span class="title">Resources</span>
                                      <span class=" "><i class="fas fa-plus-square"></i></span>
                                    </div>
                                </a>
                                <ul class="sub-menu box-shadow" >
                                   
                                    <!-- <li class="nav-item inner-nav-item">
                                        <a href="{{url('/')}}" class="nav-link f-parargraph black" style="font-size: 14px;">
                                          <div class="flex-container flex-space-between flex-vertical-align">
                                            <span>Invoices</span>
                                            <span><i class="fas fa-file-invoice-dollar"></i></span>
                                          </div>
                                        </a>
                                    </li> -->
                                   
                                    <li class="nav-item inner-nav-item">
                                        <a href="{{url('/carrier/resource/create')}}" class="nav-link f-parargraph black" style="font-size: 14px;">
                                          <div class="flex-container flex-space-between flex-vertical-align">
                                            <span>New</span>
                                            <span><i class="fas fa-file-invoice-dollar"></i></span>
                                          </div>
                                        </a>
                                    </li>
                                    <li class="nav-item inner-nav-item">
                                        <a href="{{url('/carrier/resources')}}" class="nav-link f-parargraph black" style="font-size: 14px;">
                                          <div class="flex-container flex-space-between flex-vertical-align">
                                            <span>All</span>
                                            <span><i class="fas fa-file-invoice-dollar"></i></span>
                                          </div>
                                        </a>
                                    </li>
                                    <li class="nav-item inner-nav-item">
                                        <a href="{{url('/carrier/resources/attached')}}" class="nav-link f-parargraph black" style="font-size: 14px;">
                                          <div class="flex-container flex-space-between flex-vertical-align">
                                            <span>Attached</span>
                                            <span><i class="fas fa-file-invoice-dollar"></i></span>
                                          </div>
                                        </a>
                                    </li>
                                   <!-- <li class="nav-item inner-nav-item">
                                        <a href="#" class="nav-link f-parargraph black" style="font-size: 14px;">
                                          <div class="flex-container flex-space-between flex-vertical-align">
                                            <span>Demurage</span>
                                            <span><i class="fas fa-fill-drip"></i></span>
                                          </div>
                                        </a>
                                    </li>
                                    <li class="nav-item inner-nav-item">
                                        <a href="#" class="nav-link f-parargraph black" style="font-size: 14px;">
                                          <div class="flex-container flex-space-between flex-vertical-align">
                                            <span>Settlements</span>
                                            <span><i class="fas fa-handshake"></i></span>
                                          </div>
                                        </a>
                                    </li> -->
                                </ul>
                          </li>
                          <li class="nav-item truckka-back nav-item-head">
                                <a href="javascript:;" class="nav-link nav-toggle white bold ">
                                    <div class="flex-container flex-space-between flex-vertical-align">
                                      <span class="title">Financials</span>
                                      <span class=" "><i class="fas fa-plus-square"></i></span>
                                    </div>
                                </a>
                                <ul class="sub-menu box-shadow" >
                                   
                                    <!-- <li class="nav-item inner-nav-item">
                                        <a href="{{url('/')}}" class="nav-link f-parargraph black" style="font-size: 14px;">
                                          <div class="flex-container flex-space-between flex-vertical-align">
                                            <span>Invoices</span>
                                            <span><i class="fas fa-file-invoice-dollar"></i></span>
                                          </div>
                                        </a>
                                    </li> -->
                                   
                                    <li class="nav-item inner-nav-item">
                                        <a href="{{url('/wallet/transactions')}}" class="nav-link f-parargraph black" style="font-size: 14px;">
                                          <div class="flex-container flex-space-between flex-vertical-align">
                                            <span>Statement</span>
                                            <span><i class="fas fa-file-invoice-dollar"></i></span>
                                          </div>
                                        </a>
                                    </li>
                                   <!-- <li class="nav-item inner-nav-item">
                                        <a href="#" class="nav-link f-parargraph black" style="font-size: 14px;">
                                          <div class="flex-container flex-space-between flex-vertical-align">
                                            <span>Demurage</span>
                                            <span><i class="fas fa-fill-drip"></i></span>
                                          </div>
                                        </a>
                                    </li>
                                    <li class="nav-item inner-nav-item">
                                        <a href="#" class="nav-link f-parargraph black" style="font-size: 14px;">
                                          <div class="flex-container flex-space-between flex-vertical-align">
                                            <span>Settlements</span>
                                            <span><i class="fas fa-handshake"></i></span>
                                          </div>
                                        </a>
                                    </li> -->
                                </ul>
                          </li>

                          <li class="nav-item truckka-back nav-item-head">
                                <a href="javascript:;" class="nav-link nav-toggle white bold ">
                                    <div class="flex-container flex-space-between flex-vertical-align">
                                      <span class="title">Haulage Requests</span>
                                      <span class=" "><i class="fas fa-plus-square"></i></span>
                                    </div>
                                </a>
                                <ul class="sub-menu box-shadow" >
                                   
                                    <!-- <li class="nav-item inner-nav-item">
                                        <a href="{{url('/')}}" class="nav-link f-parargraph black" style="font-size: 14px;">
                                          <div class="flex-container flex-space-between flex-vertical-align">
                                            <span>Invoices</span>
                                            <span><i class="fas fa-file-invoice-dollar"></i></span>
                                          </div>
                                        </a>
                                    </li> -->
                                   
                                    <li class="nav-item inner-nav-item">
                                        <a href="{{url('/all-orders')}}" class="nav-link f-parargraph black" style="font-size: 14px;">
                                          <div class="flex-container flex-space-between flex-vertical-align">
                                            <span>All Requests</span>
                                            <span><i class="fas fa-file-invoice-dollar"></i></span>
                                          </div>
                                        </a>
                                    </li>
                                    <li class="nav-item inner-nav-item">
                                        <a href="{{url('/carrier/map/view')}}" class="nav-link f-parargraph black" style="font-size: 14px;">
                                          <div class="flex-container flex-space-between flex-vertical-align">
                                            <span>Map</span>
                                            <span><i class="fas fa-file-invoice-dollar"></i></span>
                                          </div>
                                        </a>
                                    </li>
                                   <!-- <li class="nav-item inner-nav-item">
                                        <a href="#" class="nav-link f-parargraph black" style="font-size: 14px;">
                                          <div class="flex-container flex-space-between flex-vertical-align">
                                            <span>Demurage</span>
                                            <span><i class="fas fa-fill-drip"></i></span>
                                          </div>
                                        </a>
                                    </li>
                                    <li class="nav-item inner-nav-item">
                                        <a href="#" class="nav-link f-parargraph black" style="font-size: 14px;">
                                          <div class="flex-container flex-space-between flex-vertical-align">
                                            <span>Settlements</span>
                                            <span><i class="fas fa-handshake"></i></span>
                                          </div>
                                        </a>
                                    </li> -->
                                </ul>
                          </li>

                        </ul>
                        <!-- END SIDEBAR MENU -->
                        <!-- END SIDEBAR MENU -->
                    </div>
                    <!-- END SIDEBAR -->
                </div>