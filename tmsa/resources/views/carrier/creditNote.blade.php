<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>TMSHC</title>
    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css" href="{{ url('css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ url('css/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('css/indexupdate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('css/tmsa.css') }}">
    <link href="{{ url('https://fonts.googleapis.com/css?family=Titillium+Web|Concert+One|Josefin+Sans|Roboto') }}" rel="stylesheet">
    <script type="text/javascript" src="{{ url('js/admin_dashboard.js')}}"></script>
    <script type="text/javascript" src="{{ url('js/jquery-3.2.1.js')}}"></script>
    <script type="text/javascript" src="{{ url('js/bootstrap.js')}}"></script>
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web|Concert+One|Josefin+Sans|Roboto" rel="stylesheet">
    <script type="text/javascript" src="{{ url('js/confirmOrderDetails.js')}}"></script>
    <script type="text/javascript" src="{{ url('js/confirm_next.js')}}"></script>
    <script type="text/javascript" src="{{ url('js/loading-overlay.min.js')}}"></script>
     <script>
    // $(document).ready(function(){
    //     var target = $('#target');
    //     $(document).ajaxStart(function(){
    //         //$("#wait").css("display", "block");
    //         target.loadingOverlay();
    //     });
    //     $(document).ajaxComplete(function(){
    //         target.loadingOverlay('remove');
    //     });
    // });
    </script>

</head>
<body class="col-md-12-col-sm-6 col-xs-12" style="padding:0; background-color:#fff; height: auto;" id="target">
<div class="row" style="height:100%; margin-right:0; margin-left:0;">
<div class="side_bar col-md-3">
    <div class=" identifier hidden-xs hidden-sm" >
        <span class="navbar-brand type" >T</span>
        <span class="navbar-brand type">M</span>
        <span class="navbar-brand type">S</span>
        <span class="navbar-brand type">H</span>
        <span class="navbar-brand type">C</span>
    </div>
    <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed pull-left" data-toggle="collapse" data-target="#hidden_bar" aria-expanded="false" style="border: 2px solid #fff ; margin-left:10px;">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar" style="border:1px solid #fff"></span>
            <span class="icon-bar" style="border:1px solid #fff"></span>
            <span class="icon-bar" style="border:1px solid #fff"></span>
        </button>
        <a class="navbar-brand" href="#">
            <div class="brand">
                <span class="navbar-brand  logoT" style="border: 1px solid #ffffff; margin-left: 2px; border-radius: 10px 0 0 0; font-family: 'Concert One', cursive; color: #ffffff" >T</span>
                <span class="navbar-brand  logoR" style="border: 1px solid #ffffff;  margin-left: 2px; font-family: 'Concert One', cursive; background: #ffffff; color: #3baba0;">R</span>
                <span class="navbar-brand   logoU" style="border: 1px solid #ffffff;  margin-left: 2px; font-family: 'Concert One', cursive; color: #ffffff;">U</span>
                <span class="navbar-brand   logoC" style="border: 1px solid #ffffff;  margin-left: 2px;font-family: 'Concert One', cursive; background: #ffffff; color: #3baba0;">C</span>
                <span class="navbar-brand  logoK" style="border: 1px solid #ffffff;  margin-left: 2px;font-family: 'Concert One', cursive; color: #ffffff"; >K</span>
                <span class="navbar-brand   logoK" style="border: 1px solid #ffffff;  margin-left: 2px;font-family: 'Concert One', cursive; background: #ffffff; color: #3baba0;">K</span>
                <span class="navbar-brand   logoA" style="border: 1px solid #ffffff;  margin-left: 2px; border-radius: 0 0 10px 0;font-family: 'Concert One', cursive; color: #ffffff;">A</span>
            </div>
        </a>
    </div>
    <!--NAVBAR FOR MOBIE SCREENS-->
    <ul class="nav navbar-nav collapse" id="hidden_bar" style="margin-top: 50px;">
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage Haulage Requests<span class="glyphicon glyphicon-file pull-right"></span></a>
            <ul class="dropdown-menu">
                <li><a href="{{url('/viewRequests')}}">View All Requests <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                <li class="divider"></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Incident Management <span class="glyphicon glyphicon-file pull-right"></span></a>
            <ul class="dropdown-menu">
                <li><a href="{{url('/incidentManagement')}}">View All Incidents <span class="glyphicon glyphicon-open-file pull-right"></span></a></li>
                <li class="divider"></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage Resources <span class="glyphicon glyphicon-wrench pull-right"></span></a>
            <ul class="dropdown-menu">
                <li><a href='{{ url("/home") }}'>View All Resources <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                <li class="divider"></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="{{ route('logout') }}"
               onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();" class="dropdown-toggle" data-toggle="dropdown">Log Out <span class="glyphicon glyphicon-log-out pull-right"></span></a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </li>


    </ul>
    <!--NAVBAR FOR laptop SCREENS-->
    <ul class="nav navbar-nav hidden-xs hidden-sm">
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage Haulage Requests<span class="glyphicon glyphicon-file pull-right"></span></a>
            <ul class="dropdown-menu">
                <li><a href="{{url('/viewRequests')}}">View All Requests <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                <li class="divider"></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Incident Management <span class="glyphicon glyphicon-file pull-right"></span></a>
            <ul class="dropdown-menu">
                <li><a href="{{url('/incidentManagement')}}">View All Incidents <span class="glyphicon glyphicon-open-file pull-right"></span></a></li>
                <li class="divider"></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage Resources <span class="glyphicon glyphicon-wrench pull-right"></span></a>
            <ul class="dropdown-menu">
                <li><a href='{{ url("/home") }}'>View All Resources <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                <li class="divider"></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="{{ route('logout') }}"
               onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();" class="dropdown-toggle" data-toggle="dropdown">Log Out <span class="glyphicon glyphicon-log-out pull-right"></span></a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </li>


    </ul>
    <!--NAVBAR FOR laptop SCREENS-->
    <ul class="nav navbar-nav hidden-xs hidden-sm">
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage Haulage Requests<span class="glyphicon glyphicon-file pull-right"></span></a>
            <ul class="dropdown-menu">
                <li><a href="{{url('/viewRequests')}}">View All Requests <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                <li class="divider"></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Incident Management <span class="glyphicon glyphicon-file pull-right"></span></a>
            <ul class="dropdown-menu">
                <li><a href="{{url('/incidentManagement')}}">View All Incidents <span class="glyphicon glyphicon-open-file pull-right"></span></a></li>
                <li class="divider"></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage Resources <span class="glyphicon glyphicon-wrench pull-right"></span></a>
            <ul class="dropdown-menu">
                <li><a href='{{ url("/home") }}'>View All Resources <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                <li class="divider"></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="{{ route('logout') }}"
               onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();" class="dropdown-toggle" data-toggle="dropdown">Log Out <span class="glyphicon glyphicon-log-out pull-right"></span></a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </li>


    </ul>
    <!--NAVBAR FOR laptop SCREENS-->
    <ul class="nav navbar-nav hidden-xs hidden-sm">
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage Haulage Requests<span class="glyphicon glyphicon-file pull-right"></span></a>
            <ul class="dropdown-menu">
                <li><a href="{{url('/viewRequests')}}">View All Requests <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                <li class="divider"></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Incident Management <span class="glyphicon glyphicon-file pull-right"></span></a>
            <ul class="dropdown-menu">
                <li><a href="{{url('/incidentManagement')}}">View All Incidents <span class="glyphicon glyphicon-open-file pull-right"></span></a></li>
                <li class="divider"></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage Resources <span class="glyphicon glyphicon-wrench pull-right"></span></a>
            <ul class="dropdown-menu">
                <li><a href='{{ url("/home") }}'>View All Resources <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                <li class="divider"></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="{{ route('logout') }}"
               onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();" class="dropdown-toggle" data-toggle="dropdown">Log Out <span class="glyphicon glyphicon-log-out pull-right"></span></a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </li>


    </ul>
</div>
<!-- TSMA VIEW ORDERS PAGE -->
<div class="tmsa_main_body col-md-9" style=" padding-top:2%;padding-bottom:5%; ">


            <div style=" font-family: 'Josefin Sans', sans-serif; background: #ffffff;  height: 80vh">
<!--                    <h3 align ="center" class="panel-title">Order Details</h3>-->
                <div class="orderConfirm" style="height:10vh">
<!--PART TWO-->
                              <div class=" part2 col-md-12 col-lg-12" style="border: 2px solid #3baba0; background-color: #fff">
                                  <h4 align="center" class="btn-sm" style=" font-family: 'Josefin Sans', sans-serif; margin-left: 35%; margin-right: 35%; background: #ffffff; border: 2px solid #3baba0;">Order Details</h4>
                                     <table class="table table-user-information" style="padding-top:20%;padding-bottom: 20%;" >
                                        <tbody>
                                         @foreach($orderDetails as $orderDetail)
                                         
                                          <input type="hidden" name="serviceIDNo" value="{{ $orderDetail->id }}">
                                                    <tr>
                                                        <td>Order No:</td>
                                                        <td>{{ $orderDetail->serviceIDNo }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Amount Payable:</td>
                                                        <td>&#8358;{{ $price }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Value Of Haulage:</td>
                                                        <td> &#8358;{{ $orderDetail->valueOfHaulage }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Origin:</td>
                                                        <td>{{ $orderDetail->deliverFrom }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Destination:</td>
                                                        <td>{{$orderDetail->deliverTo}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Item Description:</td>
                                                        <td>{{$orderDetail->itemDescription}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Approximate Weight:</td>
                                                        <td>{{$orderDetail->estimatedWgt}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Contact Name:</td>
                                                        <td>{{$orderDetail->contactName}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Contact Phone:</td>
                                                        <td>{{$orderDetail->contactPhone}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Pick Up Date:</td>
                                                        <td>{{$orderDetail->pickUpDate}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Pick Up Time:</td>
                                                        <td>{{$orderDetail->pickUpTime}}</td>
                                                    </tr>
                                                     </div>

                                                    </tbody>
                                                </table>

                             @endforeach


                </div>
           </div>
  </div>

</form>




<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                <h4 class="modal-title custom_align" id="Heading">Delete this entry</h4>
            </div>
            <div class="modal-body">

                <div class="alert alert-danger"><span class="glyphicon glyphicon-warning-sign"></span> Are you sure you want to delete this Record?</div>

            </div>
            <div class="modal-footer ">
                <button type="button" class="btn btn-success" ><span class="glyphicon glyphicon-ok-sign"></span> Yes</button>
                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> No</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

</div>
<!--     ************************************************** -->
<!--BOTTOM NAV BAR-->

<script type="text/javascript" src="{{url('js/tmsa.js')}}"></script>
</body>
</html>

