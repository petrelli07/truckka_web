
    @include('carrier.includes.header')
    @include('carrier.includes.head')
    <!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-content-white page-md">
        <div class="page-wrapper">
            <!-- BEGIN HEADER -->
                @include('carrier.includes.menuBar')

            <!-- END HEADER -->
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
            <!-- END HEADER & CONTENT DIVIDER -->
            <!-- BEGIN CONTAINER -->
            <div class="page-container">
                <!-- BEGIN SIDEBAR -->
                    @include('carrier.includes.sidebar')
                <!-- END SIDEBAR -->
                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <!-- BEGIN PAGE HEADER-->
                        <!-- BEGIN THEME PANEL -->
                       
                        <!-- END THEME PANEL -->
                        <!-- BEGIN PAGE BAR -->
                        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    <a href="index.html">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Dashboard</span>
                                </li>
                            </ul>
                            <div class="page-toolbar">
                                <div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
                                    <i class="icon-calendar"></i>&nbsp;
                                    <span class="thin uppercase hidden-xs"></span>&nbsp;
                                    <i class="fa fa-angle-down"></i>
                                </div>
                            </div>
                        </div>
                        <!-- END PAGE BAR -->
                        <!-- BEGIN PAGE TITLE-->
                        <h1 class="page-title">
                        </h1>
                        <!-- END PAGE TITLE-->
                        <!-- END PAGE HEADER-->
                        


<div class="row" style="margin-top: 100px; margin-bottom: 80px;">
                            
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <div class="dashboard-stat2 ">
                                  <div class="flex-container flex-center summary-icon image-flex-fix">
                                    <img src="https://cdn0.iconfinder.com/data/icons/commerce-and-retail/512/delivery_shipping_truck_business_van_transport_car_service_moving_courier_logistics_freight_logistic_package_shipment_cargo_transportation_commerce_flat_design_icon-512.png">
                                  </div>
                                    <div class="display">
                                        <div class="number">
                                            <h3 class="font-red-haze align-center">
                                                <span data-counter="counterup" data-value="1349">{{$userSummary['allOrders']}}</span>
                                            </h3>
                                        </div>
                                    </div>
                                </div>
                                <div class="summary-tag truckka-back">
                                  <p class="f-paragraph align-center default-p">Orders</p>
                                </div>
                            </div>
                             <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <div class="dashboard-stat2 ">
                                  <div class="flex-container flex flex-center image-flex-fix summary-icon">
                                    <img src="https://cdn.iconscout.com/icon/premium/png-512-thumb/transport-service-758039.png">
                                  </div>
                                    <div class="display">
                                        <div class="number">
                                            <h3 class="font-red-haze align-center">
                                                <span data-counter="counterup" data-value="1349">{{$userSummary['completedOrders']}}</span>
                                            </h3>
                                        </div>
                                    </div>
                                </div>
                                <div class="summary-tag truckka-back">
                                  <p class="f-paragraph align-center default-p">Completed</p>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <div class="dashboard-stat2 ">
                                  <div class="flex-container image-flex-fix flex-center summary-icon">
                                    <img src="https://image.pbs.org/poster_images/assets/pfm4cdi0zkn8nk7der1j_1.png.resize.710x399.png">
                                  </div>
                                    <div class="display">
                                        <div class="number">
                                            <h3 class="font-red-haze align-center">
                                                <span data-counter="counterup" data-value="1349">{{$userSummary['incompletedOrders']}}</span>
                                            </h3>
                                        </div>
                                    </div>
                                    
                                </div>
                                <div class="summary-tag truckka-back">
                                  <p class="f-paragraph align-center default-p">Incompleted Orders</p>
                                </div>
                            </div>
                             <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <div class="dashboard-stat2 ">
                                  <div class="flex-container image-flex-fix flex-center summary-icon">
                                    <img src="http://icons.iconarchive.com/icons/flat-icons.com/flat/256/Wallet-icon.png">
                                  </div>
                                    <div class="display">
                                        <div class="number">
                                            <h3 class="font-red-haze align-center">
                                                <span data-counter="counterup" data-value="1349">&#8358;{{$userSummary['walletBalance']['balance']}}</span>
                                            </h3>
                                        </div>
                                        
                                    </div>
                                    
                                </div>
                                <div class="summary-tag truckka-back">
                                  <p class="f-paragraph align-center default-p">Wallet Balance</p>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12  col-xs-12 col-sm-12">
                                      <div class="portlet box green">
                                                    <div class="portlet-title">
                                                        <div class="caption">
                                                         Recent Transactions</div>
                                                        <div class="tools"> </div>
                                                    </div>
                                                    <div class="portlet-body">
                                                        <table class="table table-striped table-bordered table-hover" id="sample_2">
                                                            <thead>
                                                                <tr>
                                                                <th align="center">Order No</th>
                                                                    <th align="center">Destination</th>
                                                                    <th align="center">Origin</th>
                                                                    <th align="center">Status</th>
                                                                    <th align="center">Action</th>
                                                                    <!-- <th> OrderID </th>
                                                                    <th> Origin</th>
                                                                    <th> Destination </th>
                                                                    <th> Status</th>
                                                                    <th> Action </th> -->
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @forelse($orders as $m => $o)

                                                                <tr>
                                                                    <td><h5>{{$o->serviceIDNo}}</h5></td>
                                                                    <td><h5>{{$o->deliverTo}}</h5></td>
                                                                    <td><h5>{{$o->deliverFrom}}</h5></td>
                                                                    <td>
                                                                        <h5>
                                                                            @if($o->orderStatus <= 9)
                                                                                In Progress
                                                                            @elseif($o->orderStatus==10)
                                                                            Order Complete
                                                                            @endif
                                                                        </h5>
                                                                    </td>
                                                                    <td>
                                                                        <a class="btn btn-primary btn-md edit" href='{{ url("/viewOrderDetails/{$o->serviceIDNo}") }}' title="View Order Details">
                                                                                 Order Details   
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                                @empty
                                                                <tr>
                                                                    <td><h5>No Orders</h5></td>
                                                                    <td><h5>No Orders</h5></td>
                                                                    <td><h5>No Orders</h5></td>
                                                                    <td>
                                                                        <h5>
                                                                           No Orders
                                                                        </h5>
                                                                    </td>
                                                                    <td>
                                                                        <h5>
                                                                           No Orders
                                                                        </h5>
                                                                    </td>
                                                                </tr>
                                                                @endforelse
                                                              

                                                              <!-- <tr>
                                                                <td>1003</td>
                                                                <td>Tin Can A - Lagos</td>
                                                                <td>Owerri</td>
                                                                <td><span class="label label-sm label-success">Pending</span></td>
                                                                <td><button class="btn btn-primary">View</button> <button data-toggle="modal" href="#cancel" class="btn btn-danger">Cancel</button></td>
                                                              </tr> -->
                                                            </tbody>
                                                        </table>
                                                        
                                                    </div>
                                      </div>
                                      <!-- <div class="flex-container flex-center">
                                          <button class="btn btn-primary"> view all</button>
                                      </div> -->

                            </div>
                            
                        </div>

                        <div class="row">
                            <div class="col-lg-6 col-xs-12 col-sm-12">
                                <div class="portlet light ">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <span class="caption-subject bold uppercase font-dark">Revenue</span>
                                            <span class="caption-helper">distance stats...</span>
                                        </div>
                                        <div class="actions">
                                            <a class="btn btn-circle btn-icon-only btn-default" href="#">
                                                <i class="icon-cloud-upload"></i>
                                            </a>
                                            <a class="btn btn-circle btn-icon-only btn-default" href="#">
                                                <i class="icon-wrench"></i>
                                            </a>
                                            <a class="btn btn-circle btn-icon-only btn-default" href="#">
                                                <i class="icon-trash"></i>
                                            </a>
                                            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="#"> </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div id="dashboard_amchart_1" class="CSSAnimationChart"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-xs-12 col-sm-12">
                                <div class="portlet light ">
                                    <div class="portlet-title">
                                        <div class="caption ">
                                            <span class="caption-subject font-dark bold uppercase">Finance</span>
                                            <span class="caption-helper">distance stats...</span>
                                        </div>
                                        <div class="actions">
                                            <a href="#" class="btn btn-circle green btn-outline btn-sm">
                                                <i class="fa fa-pencil"></i> Export </a>
                                            <a href="#" class="btn btn-circle green btn-outline btn-sm">
                                                <i class="fa fa-print"></i> Print </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div id="dashboard_amchart_3" class="CSSAnimationChart"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <!-- END CONTENT BODY -->
                </div>
            </div>
            <!-- END CONTAINER -->
            <!-- BEGIN FOOTER -->
            <div class="page-footer">
                <div class="page-footer-inner">
                 2018 &copy; Truckka
                </div>
                <div class="scroll-to-top">
                    <i class="icon-arrow-up"></i>
                </div>
            </div>
            <!-- END FOOTER -->
        </div>


        
        

        <!-- BEGIN QUICK NAV -->
        
        <!-- END QUICK NAV -->
        <!--[if lt IE 9]>
<script src="../assets/global/plugins/respond.min.js"></script>
<script src="../assets/global/plugins/excanvas.min.js"></script> 
<script src="../assets/global/plugins/ie8.fix.min.js"></script> 
<![endif]-->
    @include('carrier.includes.footer')
