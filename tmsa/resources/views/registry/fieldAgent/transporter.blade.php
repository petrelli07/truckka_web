@extends('registry.fieldAgent.layout.content')
@section('content')


<div class="row">
    <div class="box col-md-12">
        <!--if @ session -->
        @if (session('message'))
        <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert">x</button>
            {{ session('message') }}
        </div>
        <p>Click <a href="{{url('/registry/fieldOps/truck_owners')}}">here</a> to add trucks</p>
        @endif
        <!--endif-->

        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-plus"></i> Add New Transporter</h2>

            </div>
            <div class="box-content row">
                 <form role="form" method="post" class="validate"  action="{{url('/registry/fieldOps/create_transporter_process')}}"  enctype="multipart/form-data">
                 {{csrf_field()}}

                 <div class="box col-md-10">
                    <div class="form-group">
                        <label class="control-label" for="name">Transporter Name</label>
                        <input type="text" name="name" class="form-control required" value="{{old('name')}}" placeholder="Enter Transporter name">
                        @if ($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                 <div class="box col-md-10">
                    <div class="form-group">
                        <label class="control-label" for="phone">Transporter Phone Number</label>
                        <input type="text" name="phone_number" value="{{old('phone_number')}}" class="form-control required" placeholder="Add phone number">
                        @if ($errors->has('phone_number'))
                        <span class="help-block">
                            <strong>{{ $errors->first('phone_number') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>


                 <div class="box col-md-10">
                    <div class="form-group">
                        <label class="control-label" for="address">Transporter Address</label>
                        <textarea type="text" name="address" class="form-control required" value="{{old('address')}}"  placeholder="Enter address"></textarea>
                        @if ($errors->has('address'))
                        <span class="help-block">
                            <strong>{{ $errors->first('address') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                 <div class="box col-md-4">
                     <div class="form-group">
                         <label class="control-label" for="destination">Select Bank for Payment</label>
                         <div class="controls">
                             <select id="selectError" class="col-md-12 form-control" name="bank">
                                 <option value=''>[-Select Bank-]</option>
                                 @foreach($banks as $bank)
                                 <option value='{{$bank->id}}'>{{$bank->bank_name}}</option>
                                 @endforeach
                             </select>
                         </div>
                         @if ($errors->has('bank'))
                        <span class="help-block">
                            <strong>{{ $errors->first('bank') }}</strong>
                        </span>
                         @endif
                     </div>
                 </div>

                 <div class="box col-md-4">
                     <div class="form-group">
                         <label class="control-label" for="weight">Account Number</label>
                         <input type="text" name="account_number" value="{{old('account_number')}}" class="form-control required" placeholder="Enter Account Number">
                         @if ($errors->has('account_number'))
                        <span class="help-block">
                            <strong>{{ $errors->first('account_number') }}</strong>
                        </span>
                         @endif
                     </div>
                 </div>

                 <div class="box col-md-4">
                     <div class="form-group">
                         <label class="control-label" for="weight">Account Name</label>
                         <input type="text" name="account_name" value="{{old('account_name')}}" class="form-control required" placeholder="Enter Account Name">
                         @if ($errors->has('account_name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('account_name') }}</strong>
                        </span>
                         @endif
                     </div>
                 </div>

            
                <div class="box col-md-6">
                    <div class="form-group">
                        <button type="submit" class="btn btn-custom">
                            <i class="glyphicon glyphicon-plus"></i>Submit
                        </button>
                    </div>
                </div>
            </form>

            </div>

        </div>

    </div>
    <!--/span-->

</div><!--/row-->






@endsection