@extends('registry.administrator.layout.content')
@section('content')


<div class="row">
    <div class="box col-md-12">
        <!--if @ session -->
        @if (session('message'))
            <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                {{ session('message') }}
            </div>
        @endif
        <!--endif-->

        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-plus"></i> CREATE NEW USER</h2>

            </div>
                 <form role="form" method="post" class="validate"  action="{{url('/registry/administrator/user_management/create_new_user_process')}}"  enctype="multipart/form-data">
                 {{csrf_field()}}


              <div class="box-content row">
                <div class="box col-md-4">
                    <div class="form-group">
                        <label class="control-label" for="destination">Choose User Type</label>
                        <div class="controls">
                            <select id="selectError" class="col-md-12" data-rel="chosen" name="userType">
                            <option value=''>[-Choose User Type-]</option>
                            <option value='7'>FIELD AGENT</option>
                            <option value='9'>OPERATIONS</option>
                            <option value='8'>FINANCE</option>
                            </select>
                        </div>
                        @if ($errors->has('userType'))
                            <span class="help-block">
                                <strong>{{ $errors->first('userType') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="box col-md-4">
                    <div class="form-group">
                        <label class="control-label" for="weight">Name</label>
                        <input type="text" name="name" class="form-control required" placeholder="Enter a Name">
                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="box col-md-4">
                    <div class="form-group">
                        <label class="control-label" for="weight">Email</label>
                        <input type="email" name="email" class="form-control required" placeholder="Enter Email Address">
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="box col-md-4">
                    <div class="form-group">
                        <label class="control-label" for="weight">Phone Number</label>
                        <input type="text" name="phone" class="form-control required" placeholder="Enter Phone Number">
                        @if ($errors->has('phone'))
                            <span class="help-block">
                                <strong>{{ $errors->first('phone') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

              </div>

              <div class="box-content row">


                  <div class="box col-md-4">
                      <div class="form-group">
                          <button type="submit" class="btn btn-custom">
                              <i class="glyphicon glyphicon-plus"></i>Submit
                          </button>
                      </div>
                  </div>
              </div>

                </form>
        </div>
    </div>
    <!--/span-->

</div><!--/row-->

@endsection