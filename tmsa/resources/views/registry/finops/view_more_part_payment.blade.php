@extends('registry.finops.layout.content')
@section('content')


<div class="row" xmlns="http://www.w3.org/1999/html">
    <div class="box col-md-12">


        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-plus"></i> View More</h2>

            </div>
            <div class="box-content row">

                <div class="box col-md-12">
                    @foreach($details as $detail)
                    <img src="{{url('/waybillfiles/'.$detail->waybill_number.$detail->waybill_file_origin)}}"  height="500" width="100%" class="img-responsive">
                </div>

                <div class="box col-md-12">
                    <div class="box-content">
                        <table class="table table-striped table-bordered  responsive">
                            <thead>
                            <tr>
                                <th>Waybill Number</th>
                                <th>Origin</th>
                                <th>Destination</th>
                                <th>Amount</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td class="center">{{$detail->waybill_number}}</td>
                                <td class="center">{{$detail->origin}}</td>
                                <td class="center">{{$detail->destination}}</td>
                                <td class="center">{{$detail->amount/2}}</td>
                                <td class="center">
                                    <form method="post" action="{{url('registry/approving-part-payment-finops')}}">
                                        {{csrf_field()}}
                                        @foreach($details as $detail)
                                        <input type="hidden" name="agent_id" value="{{$detail->agent_id}}">
                                        <input type="hidden" name="amount" value="{{$detail->amount}}">
                                        <input type="hidden" name="registry_number" value="{{$detail->registry_number}}">
                                        @endforeach
                                        <input type="hidden" name="account_number" value="{{$account_number}}">
                                        <button class="btn btn-success btn-sm view-shop">Approve</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>






            </div>
        </div>
    </div>
    <!--/span-->

</div><!--/row-->



@endsection