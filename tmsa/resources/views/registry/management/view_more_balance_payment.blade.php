@extends('registry.management.layout.content')
@section('content')


<div class="row">
    <div class="box col-md-12">


        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-plus"></i> View More</h2>

            </div>
            <div class="box-content row">


                <div class="box col-md-12">

                    @foreach($details as $detail)

                    @if($sanitized_filename != NULL)
                    <img src="{{url('/waybillfiles/'.$detail->waybill_number.$detail->waybill_file_origin)}}"  height="500" width="100%" class="img-responsive">
                    @endif

                </div>

                <div class="box col-md-12">
                    <div class="box-content">
                        <table class="table table-striped table-bordered  responsive">
                            <thead>
                            <tr>
                                <th>Waybill Number</th>
                                <th>Plate Number</th>
                                <th>Driver Name</th>
                                <th>Driver Phone</th>
                                <th>Field Agent Name</th>
                                <th>Recipient Phone Number</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>

                            <tr>
                                <td class="center">{{$detail->waybill_number}}</td>
                                <td class="center">{{$detail->plate_number}}</td>
                                <td class="center">{{$detail->driver_name}}</td>
                                <td class="center">{{$detail->driver_phone}}</td>
                                <td class="center">{{$detail->name}}</td>
                                <td class="center">{{$detail->recipient_phone_number}}</td>
                                <td class="center">
                                    <a class="btn btn-success btn-sm view-shop" href='{{url("registry/ops/approve_balance_payment/{$detail->registry_number}")}}'>Approve</a>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>






            </div>
        </div>
    </div>
    <!--/span-->

</div><!--/row-->



@endsection