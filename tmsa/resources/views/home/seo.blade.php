
<title>{{$title}}</title>
<meta name="keywords" content="truckka">
<meta name="title" content="{{$metatitle}}">
<meta name="description" content="{{$sitedescription}}" />
<meta name="Classification" content="">
<meta name="target" content="">
<meta name="copyright" content="Copyright by Truccka logistics, All Rights Reserved." />
<meta name="generator" content="." />

<link rel="apple-touch-icon-precomposed" href="" />
<meta name="msapplication-TileImage" content="" />