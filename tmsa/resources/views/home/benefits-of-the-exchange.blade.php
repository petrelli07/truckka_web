
@extends('home.layout.content')
@section('content')
<!--Benefits of the exchange-->
<div class="container-fluid" id="benefits">
    <div class="row about-al">
        <div class="container">
            <div class="row">
                <div class="work-title">
                    <h2>
                    BENEFITS OF THE EXCHANGE
                    </h2>
                </div>
                <div class="col-xs-12 no-padding work-block-al">
                    
                    

                    <div class="col-sm-9 col-md-6 col-lg-6">
                        <div class="work-block-horizontal2">
                        <div class="work-imm">
                            <div class="image-freight">

                                <img src="{{URL::to('home_assets/assets/images/map-with-truck-ful.png')}}"  >           
                               
                            </div>
                            <div class ="content-freight">
                                <p class="p-horizon">
                                <b>TRANSPARENT PRICING:</b></p>
                                <p class="p-horizon">
                                Our fee structure brings the freight and truck owner 
                                directly together removing the layers of middle men thus 
                                reducing your freight charges</p>
                        </div>
                        </div>
                    </div>
                    </div>

                

                    <div class="col-sm-9 col-md-6 col-lg-6">
                        <div class="work-block-horizontal">
                        <div class="work-imm">
                            <div class="image-freight">
                                <img src="{{URL::to('home_assets/assets/images/delivery-truck_with_freight2.png')}}">     
                            </div>      
                            <div class ="content-freight">
                                        <p class="p-horizon">
                                        <b>CONSISTENT FREIGHT / TRUCK ALWAYS MATCHING: </b></p>
                                        <p class="p-horizon">
                                        Worry no more about travelling empty or where to find trucks for the
                                        freight owner. The exchange is always here to meet your needs.

                                    </p>
                            </div>
                            </div>
                               
                        </div>
                    </div>

                    <div class="col-sm-9 col-md-6 col-lg-6">
                        <div class="work-block-horizontal2">
                        <div class="work-imm">
                            <div class="image-freight">
                                <img src="{{URL::to('home_assets/assets/images/lap-phone-white.png')}}"> 
                            </div>          
                            <div class ="content-freight">
                                        <p class="p-horizon">
                                        <b>EASY TO USE TECHNOLOGY: </b> </p>
                                        <p class="p-horizon">
                                        The exchange is easy to access via our online and mobile application. 
                                        You can with ease signup, request and pay for trucks or search for 
                                        freight online. All material movement is trackable in real-time for 
                                        your peace of mind.

                                    </p>
                            </div>
                            </div>
                               
                        </div>
                    </div>

                     <div class="col-sm-9 col-md-6 col-lg-6">
                     <div class="work-block-horizontal">
                            <div class="work-imm">
                            <div class="image-freight">
                                <img src="{{URL::to('home_assets/assets/images/customer_support_hover.png')}}" alt="No Hiden fee">
                            </div>
                            <div class ="content-freight">
                                <p class="p-horizon">
                                <b>HELPFUL / COURTEOUS CUSTOMER SUPPORT :</b>
                                </p>
                                <p class="p-horizon">
                                Our service channels are equipped with knowledgeable and polite support/sales teams
                                 who can attend to you via the phone, email and/or via our social media channels.
                                </p>
                            </div>
                            </div>
                               
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end of Benefits of exchange-->



@endsection