@extends('home.layout.content')
@section('content')

<div class="container-fluid">
    <div class="row forget-bg" >
        <div class="forget-overlay"></div>
        <div class="container">
            <div class="row">

                <form action="{{url('/exchange/customer/sign_up_customer')}}" method="post">
                <div class="forget-content ">
                    <div class="col-xs-12 forget-content-al">
                        <div class="forget-title col-xs-12 no-padding">
                            <h2>
                                Register
                            </h2>
                        </div>
                        {{csrf_field()}}
                        <div class="col-xs-12 no-padding">
                            <label class="forget-label">First Name</label>
                            <input type="text" class="text-forget" id="txtForgotEmail" value="{{old('first_name')}}" name="first_name" placeholder="First Name">
                            <span class="field-validation-valid c-red" data-valmsg-for="UserName" id="forgotEmailError" ></span>
                            @if ($errors->has('first_name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('first_name') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="col-xs-12 no-padding">
                            <label class="forget-label">Last Name</label>
                            <input type="text" class="text-forget" id="txtForgotEmail" value="{{old('last_name')}}" name="last_name" placeholder="Last Name">
                            <span class="field-validation-valid c-red" data-valmsg-for="UserName" id="forgotEmailError" ></span>
                            @if ($errors->has('last_name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('last_name') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="col-xs-12 no-padding">
                            <label class="forget-label">Email</label>
                            <input type="email" class="text-forget" id="txtForgotEmail" name="customer_email" placeholder="Email Address" value="{{old('customer_email')}}">
                            <span class="field-validation-valid c-red" data-valmsg-for="UserName" id="forgotEmailError" ></span>
                            @if ($errors->has('customer_email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('customer_email') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="col-xs-12 no-padding">
                            <label class="forget-label">Phone Number</label>
                            <input type="text" class="text-forget" value="{{old('phone')}}" id="yourphone" name="phone" placeholder="E.g 08xxxxxxxxx" maxlength="11">
                            <span class="field-validation-valid c-red" data-valmsg-for="UserName" id="forgotEmailError" ></span>
                            @if ($errors->has('phone'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('phone') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="col-xs-12 no-padding">
                            <label class="forget-label">Address</label>
                            <textarea class="text-forget" value="{{old('address')}}" name="address"></textarea>
                            <span class="field-validation-valid c-red" data-valmsg-for="UserName" id="forgotEmailError" ></span>
                            @if ($errors->has('address'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('address') }}</strong>
                                </span>
                            @endif
                        </div>


                        <div class="col-xs-12 no-padding">
                            <label class="forget-label">Password</label>
                            <input type="password" class="text-forget" id="txtForgotEmail" name="password" placeholder="Password">
                            <span class="field-validation-valid c-red" data-valmsg-for="Password"  ></span>
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="col-xs-12 no-padding">
                            <label class="forget-label">Confirm Password</label>
                            <input type="password" class="text-forget" id="txtForgotEmail" name="password_confirmation" placeholder="Password">
                            <span class="field-validation-valid c-red" data-valmsg-for="Password"  ></span>
                        </div>

                        <div class="col-xs-12">
                            <div class="row">
                                <div class="forget-btn">
                                    <button type="submit" id="btnForgotSend" class="">Submit</button>
                                    <button type="reset" id="backBtn" class="payment-cancel ">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>



@endsection