function initMap () {
  $(document).ready(function  (argument) {

    let pickupInput         = document.getElementById('pickup_address');
    let deliveryInput       = document.getElementById('delivery_address');
    let resultFields        = ['formatted_address','place_id'];

    let autoCompletePickup    = new google.maps.places.Autocomplete(pickupInput);
    let autoCompleteDelivery  = new google.maps.places.Autocomplete(deliveryInput);


    autoCompletePickup.setFields(resultFields);
    autoCompleteDelivery.setFields(resultFields);


    google.maps.event.addListener(autoCompletePickup, 'place_changed', function() {

      var place = autoCompletePickup.getPlace();
      $('#pickup_address').val(place.formatted_address);
      $('#pickup_placeId').val(place.place_id);

    });

    google.maps.event.addListener(autoCompleteDelivery, 'place_changed', function() {

      var place = autoCompleteDelivery.getPlace();
      $('#deliveryInput').value(place.formatted_address);
      $('#delivery_placeId').value.(place.place_id);

    });

  })

}
