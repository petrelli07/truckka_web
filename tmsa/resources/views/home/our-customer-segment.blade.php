

@extends('home.layout.content')
@section('content')

<!--Mission and vision-->
<div class="container-fluid" id="about">
    <div class="row about-al">
        <div class="container">
            <div class="row">
                <div class="about-title">
                    <h2>
                	OUR ECOSYSTEM
                    </h2>
                    
                    <img src="{{URL::to('home_assets/assets/images/truckka-segment.png')}}" style="width:100%;">
                    
                </div>
                
            </div>
        </div>
    </div>
</div>
<!--end of mission and vision-->
@endsection