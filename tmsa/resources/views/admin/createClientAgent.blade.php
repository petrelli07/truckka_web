<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>TMSA</title>
    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css" href="{{ url('css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ url('css/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('css/indexupdate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('css/tmsa.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('css/loader.css') }}">
    <link href="{{ url('https://fonts.googleapis.com/css?family=Titillium+Web|Concert+One|Josefin+Sans|Roboto') }}" rel="stylesheet">
    <script type="text/javascript" src="{{ url('js/admin_dashboard.js')}}"></script>
    <script type="text/javascript" src="{{ url('js/jquery-3.2.1.js')}}"></script>
    <script type="text/javascript" src="{{ url('js/bootstrap.js')}}"></script>
    <script type="text/javascript" src="{{ url('js/newClientAgent.js')}}"></script>
    <script>

        $(document).ready(function() {

            $('.createClient').hide();
            $('.createCarrier').hide();
            $('.createAdmin').show();
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function() {

            $(document).ajaxStart(function(){
                document.getElementById('modal').style.display = 'block';
                document.getElementById('fade').style.display = 'block';
            });

            $(document).ajaxComplete(function(){
                document.getElementById('modal').style.display = 'none';
                document.getElementById('fade').style.display = 'none';
            });
        });
        
    </script>

</head>
<body class="col-md-12-col-sm-6 col-xs-12" style="padding:0;background-color: #3baba0; height: auto">

</div>
<!--TMSA-->
<div class="tmsa" style="height:100vh">
    <div class="row" style="height:100vh; margin-right:0; margin-left:0;">
        <div class="side_bar col-md-3" >
            <div class=" identifier hidden-xs hidden-sm" >
                <span class="navbar-brand type" >T</span>
                <span class="navbar-brand type">M</span>
                <span class="navbar-brand type">S</span>
                <span class="navbar-brand type">A</span>
            </div>
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed pull-left" data-toggle="collapse" data-target="#hidden_bar" aria-expanded="false" style="border: 2px solid #fff ; margin-left:10px;">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar" style="border:1px solid #fff"></span>
                    <span class="icon-bar" style="border:1px solid #fff"></span>
                    <span class="icon-bar" style="border:1px solid #fff"></span>
                </button>
                <a class="navbar-brand" href="#">
                    <div class="brand">
                        <span class="navbar-brand  logoT" style="border: 1px solid #ffffff; margin-left: 2px; border-radius: 10px 0 0 0; font-family: 'Concert One', cursive; color: #ffffff" >T</span>
                        <span class="navbar-brand  logoR" style="border: 1px solid #ffffff;  margin-left: 2px; font-family: 'Concert One', cursive; background: #ffffff; color: #3baba0;">R</span>
                        <span class="navbar-brand   logoU" style="border: 1px solid #ffffff;  margin-left: 2px; font-family: 'Concert One', cursive; color: #ffffff;">U</span>
                        <span class="navbar-brand   logoC" style="border: 1px solid #ffffff;  margin-left: 2px;font-family: 'Concert One', cursive; background: #ffffff; color: #3baba0;">C</span>
                        <span class="navbar-brand  logoK" style="border: 1px solid #ffffff;  margin-left: 2px;font-family: 'Concert One', cursive; color: #ffffff"; >K</span>
                        <span class="navbar-brand   logoK" style="border: 1px solid #ffffff;  margin-left: 2px;font-family: 'Concert One', cursive; background: #ffffff; color: #3baba0;">K</span>
                        <span class="navbar-brand   logoA" style="border: 1px solid #ffffff;  margin-left: 2px; border-radius: 0 0 10px 0;font-family: 'Concert One', cursive; color: #ffffff;">A</span>
                    </div>
                </a>
            </div>
<!--NAVBAR FOR MOBIE SCREENS-->
            <ul class="nav navbar-nav collapse" id="hidden_bar" style="margin-top:50px;">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage User Groups<span class="glyphicon glyphicon-user pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{url('/home')}}">Create Users<span class="glyphicon glyphicon-plus-sign pull-right"></span></a></li>
                        <li class="divider"></li>
                        <li><a href="{{url('/viewAllUsers')}}">View All Users <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Client Agents<span class="glyphicon glyphicon-file pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{url('/viewAllClients')}}">Create Client Agents<span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage Client Orders <span class="glyphicon glyphicon-file pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{url('/viewOrders')}}">View All Orders <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Incident Management <span class="glyphicon glyphicon-file pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{url('/incidentManagement')}}">View All Incidents <span class="glyphicon glyphicon-open-file pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage Resources <span class="glyphicon glyphicon-wrench pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href='{{ url("/viewAllResources") }}'>View All Resources <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Track and Trace <span class="glyphicon glyphicon-map-marker pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href='{{ url("/allGPS") }}'>Track Orders <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();" class="dropdown-toggle" data-toggle="dropdown">Log Out <span class="glyphicon glyphicon-log-out pull-right"></span></a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>


            </ul>
            <!--NAVBAR FOR laptop SCREENS-->
            <ul class="nav navbar-nav hidden-xs hidden-sm" style="padding-top:0;">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage User Groups<span class="glyphicon glyphicon-user pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{url('/home')}}">Create Users<span class="glyphicon glyphicon-plus-sign pull-right"></span></a></li>
                        <li class="divider"></li>
                        <li><a href="{{url('/viewAllUsers')}}">View All Users <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Client Agents<span class="glyphicon glyphicon-file pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{url('/viewAllClients')}}">Create Client Agents<span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage Client Orders <span class="glyphicon glyphicon-file pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{url('/viewOrders')}}">View All Orders <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Incident Management <span class="glyphicon glyphicon-file pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{url('/incidentManagement')}}">View All Incidents <span class="glyphicon glyphicon-open-file pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage Resources <span class="glyphicon glyphicon-wrench pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href='{{ url("/viewAllResources") }}'>View All Resources <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Track and Trace <span class="glyphicon glyphicon-map-marker pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href='{{ url("/allGPS") }}'>Track Orders <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();" class="dropdown-toggle" data-toggle="dropdown">Log Out <span class="glyphicon glyphicon-log-out pull-right"></span></a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>


            </ul>
        </div>
        <!-- TSMA MAIN PAGE -->
        <div class="tmsa_main_body col-md-9" style="height:100vh">
            <div class="tmsa_user_page" >
                <!session message starts-->

                @if(session('message'))
                <p>
                    {{ session('message') }}
                </p>
                @endif

                <!session message ends-->
                <div class="alert alert-danger print-error-msg" style="display:none">

                    <ul></ul>
                </div>

                <div class="alert alert-success print-success-msg" style="display:none">

                    <ul></ul>
                </div>
                <!-- FORM -->
                    <form id="userDetails" class="form-horizontal" method="post">
                        {{csrf_field()}}
                    <fieldset style="background: #ffffff">

                        <!-- Form Name -->
                        <div class="form-group" >
                            <div class="input-group" style="margin-left:10%; margin-right: 10%; margin-bottom: 3%; border-radius:10px;">
                                <label for="addUser" class="input-group-addon" style=" border-radius:10px; height: 50px; font-family: Roboto; font-size:150%; font-weight: 1000;"> CREATE A CLIENT AGENT <span class="glyphicon glyphicon-plus-sign"></span></label>
                            </div>
                        </div>
                    </fieldset>
                    </form>
                        <!-- Email input-->
                    <div class="createClient">
                        <form id="clientDetails"  class="newTestCase">
                            <fieldset>
                                {{ csrf_field() }}
                                <div class="form-group " style="margin-left:10%; margin-right: 10%;  margin-bottom: 3%;">
                                    <label><h4>Agent's Email Address</h4></label>
                                    <div class="col-md-12">
                                        <label class="sr-only" for="form-first-name">Agent's Email Address</label>
                                        <input placeholder="Agent's Email Address" class="input-md form-control" id="email" name="email" required="required">

                                    </div>
                                </div>

                                <div class="form-group " style="margin-left:10%; margin-right: 10%;  margin-bottom: 3%;">
                                    <label><h4>Agent's Name</h4></label>
                                    <div class="col-md-12">
                                        <label class="sr-only" for="form-first-name">Agent's Name</label>
                                        <input placeholder="Agent's Name" class="input-md form-control"  id="companyName" name="companyName" required="required">
                                    </div>
                                </div>

                                <div align="center">
                                    <div align="center">
                                        <button id="btn_registrar" name="btn_registrar" class="btn button_login newClientDets" style="margin:2%;">CREATE USER</button>
                                    </div>
                                </div>

                            </fieldset>
                        </form>
                    </div>


                    <div class="createAdmin">
                        <form id="clientAgentForm"  class="newTestCase">
                            <fieldset>
                                {{ csrf_field() }}

                                <div class="form-group" style="margin-left:10%; margin-right: 10%; margin-bottom: 5%;">
                                    <label><h4>CLIENT COMPANY LIST</h4></label>
                                    <div class="col-md-12">
                                        <select name="companyID" required class="form-control input-md" id="userCategory">
                                            <option value="">[-Choose a Client Company-]</option>
                                            @foreach($allClients as $c)
                                            <option value="{{$c->id}}">{{$c->companyName}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group " style="margin-left:10%; margin-right: 10%;  margin-bottom: 3%;">
                                    <label><h4>Agent's Email Address</h4></label>
                                    <div class="col-md-12">
                                        <label class="sr-only" for="form-first-name">Agent's Email Address</label>
                                        <input placeholder="Agent's Email Address" class="input-md form-control" id="email" name="email" required="required">

                                    </div>
                                </div>

                                <div class="form-group " style="margin-left:10%; margin-right: 10%;  margin-bottom: 3%;">
                                    <label><h4>Agent's Name</h4></label>
                                    <div class="col-md-12">
                                        <label class="sr-only" for="form-first-name">Agent's Name</label>
                                        <input placeholder="Agent's Name" class="input-md form-control"  id="agentName" name="agentName" required="required">
                                    </div>
                                </div>

                                <div class="form-group" style="margin-left:10%; margin-right: 10%; margin-bottom: 5%;">
                                    <label><h4>CHOOSE AGENT ROLE</h4></label>
                                    <div class="col-md-12">
                                        <select name="roleID" required class="form-control input-md" id="userCategory">
                                            <option value="">[-Choose Agent Role-]</option>
                                            <option value="0">Supervisor</option>
                                            <option value="1">Operator</option>
                                            <option value="2">Accountant</option>
                                        </select>
                                    </div>
                                </div>

                            <div align="center">
                                <div align="center">
                                    <button id="btn_registrar" name="btn_registrar" class="btn button_login clientAgent" style="margin:2%;">CREATE AGENT</button>
                                </div>
                            </div>

                            </fieldset>
                        </form>
                    </div>

            </div>

        </div>
    </div>
    <!--BOTTOM NAV BAR-->
    <!-- <div class = "bottom footer" style="height: auto; width:100%; background-color: #3baba0; ">
        <p class="text-center" style="color: #232323; margin-top: 5px; background-color: #3baba0;" >Copyright © TRUCKKA 2018. All rights reserved.</p>
    </div> -->

<div id="fade"></div>
        <div id="modal">
            <img id="loader" src="{{url('images/69.gif')}}" />
        </div>
</div>

<!--     ************************************************** -->
</body>
</html>

