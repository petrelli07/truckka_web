<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>TMSA</title>
    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css" href="{{ url('css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ url('css/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('css/indexupdate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('css/tmsa.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('css/loader.css') }}">
    <link href="{{ url('https://fonts.googleapis.com/css?family=Titillium+Web|Concert+One|Josefin+Sans|Roboto') }}" rel="stylesheet">
    <script type="text/javascript" src="{{ url('js/admin_dashboard.js')}}"></script>
    <script type="text/javascript" src="{{ url('js/jquery-3.2.1.js')}}"></script>
    <script type="text/javascript" src="{{ url('js/bootstrap.js')}}"></script>
    <script type="text/javascript" src="{{ url('js/newUser.js')}}"></script>
    <script type="text/javascript" src="{{ url('js/newCarrier.js')}}"></script>
    <script type="text/javascript" src="{{ url('js/newClient.js')}}"></script>
    <script type="text/javascript" src="{{ url('js/loadSelectedCarrier.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function() {

            $(document).ajaxStart(function(){
                document.getElementById('modal').style.display = 'block';
                document.getElementById('fade').style.display = 'block';
            });

            $(document).ajaxComplete(function(){
                document.getElementById('modal').style.display = 'none';
                document.getElementById('fade').style.display = 'none';
            });
        });
        
    </script>

</head>


<body class="col-md-12-col-sm-6 col-xs-12" style="padding:0; height:auto">
<div class="row" style="height:100vh; margin-right:0; margin-left:0; background-color: #232323;">
    <div class="side_bar col-md-3" >
        <div class=" identifier hidden-xs hidden-sm" >
            <span class="navbar-brand type" >T</span>
            <span class="navbar-brand type">M</span>
            <span class="navbar-brand type">S</span>
            <span class="navbar-brand type">A</span>
        </div>
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed pull-left" data-toggle="collapse" data-target="#hidden_bar" aria-expanded="false" style="border: 2px solid #fff ; margin-left:10px;">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar" style="border:1px solid #fff"></span>
                <span class="icon-bar" style="border:1px solid #fff"></span>
                <span class="icon-bar" style="border:1px solid #fff"></span>
            </button>
            <a class="navbar-brand" href="#">
                <div class="brand">
                    <span class="navbar-brand  logoT" style="border: 1px solid #ffffff; margin-left: 2px; border-radius: 10px 0 0 0; font-family: 'Concert One', cursive; color: #ffffff" >T</span>
                    <span class="navbar-brand  logoR" style="border: 1px solid #ffffff;  margin-left: 2px; font-family: 'Concert One', cursive; background: #ffffff; color: #3baba0;">R</span>
                    <span class="navbar-brand   logoU" style="border: 1px solid #ffffff;  margin-left: 2px; font-family: 'Concert One', cursive; color: #ffffff;">U</span>
                    <span class="navbar-brand   logoC" style="border: 1px solid #ffffff;  margin-left: 2px;font-family: 'Concert One', cursive; background: #ffffff; color: #3baba0;">C</span>
                    <span class="navbar-brand  logoK" style="border: 1px solid #ffffff;  margin-left: 2px;font-family: 'Concert One', cursive; color: #ffffff"; >K</span>
                    <span class="navbar-brand   logoK" style="border: 1px solid #ffffff;  margin-left: 2px;font-family: 'Concert One', cursive; background: #ffffff; color: #3baba0;">K</span>
                    <span class="navbar-brand   logoA" style="border: 1px solid #ffffff;  margin-left: 2px; border-radius: 0 0 10px 0;font-family: 'Concert One', cursive; color: #ffffff;">A</span>
                </div>
            </a>
        </div>
        <!--NAVBAR FOR MOBIE SCREENS-->
            <ul class="nav navbar-nav collapse" id="hidden_bar" style="margin-top:50px;">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage User Groups<span class="glyphicon glyphicon-user pull-right"></span></a>
                    <ul class="dropdown-menu">

                            <li><a href="{{url('/home')}}">Create Users<span class="glyphicon glyphicon-plus-sign pull-right"></span></a></li>
                        <li class="divider"></li>
                        <li><a href="{{url('/viewAllUsers')}}">View All Users <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        
                        <li class="divider"></li>
                    </ul>
                </li>

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage Client Orders <span class="glyphicon glyphicon-file pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{url('/viewClientOrders')}}">View All Orders <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                        <li><a href="{{url('/createClientInvoice')}}">Client Invoices <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                        <li><a href="{{url('/manageATLs')}}">Manage Waybills<span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Track and Trace <span class="glyphicon glyphicon-map-marker pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href='{{ url("/allGPS") }}'>Track Orders <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>

                <li class="dropdown">
                    <a href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();" class="dropdown-toggle" data-toggle="dropdown">Log Out <span class="glyphicon glyphicon-log-out pull-right"></span></a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>


            </ul>
            <!--NAVBAR FOR laptop SCREENS-->
            <ul class="nav navbar-nav hidden-xs hidden-sm" style="margin-top:50px;">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage User Groups<span class="glyphicon glyphicon-user pull-right"></span></a>
                    <ul class="dropdown-menu">

                            <li><a href="{{url('/home')}}">Create Users<span class="glyphicon glyphicon-plus-sign pull-right"></span></a></li>
                        <li class="divider"></li>
                        <li><a href="{{url('/viewAllUsers')}}">View All Users <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        
                        <li class="divider"></li>
                    </ul>
                </li>

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage Client Orders <span class="glyphicon glyphicon-file pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{url('/viewClientOrders')}}">View All Orders <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                        <li><a href="{{url('/createClientInvoice')}}">Client Invoices <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                        <li><a href="{{url('/manageATLs')}}">Manage Waybills<span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Track and Trace <span class="glyphicon glyphicon-map-marker pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href='{{ url("/allGPS") }}'>Track Orders <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>

                <li class="dropdown">
                    <a href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();" class="dropdown-toggle" data-toggle="dropdown">Log Out <span class="glyphicon glyphicon-log-out pull-right"></span></a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>


            </ul>
</div>
<!-- TSMA VIEW ORDERS PAGE -->
<div class="tmsa_main_body col-md-9" style="height: 100vh;" >
    <div class="row" style="height: 100vh;">
        <div class="col-md-9 view-order" >
            <h4 align="center" class="btn-lg" style=" font-family: 'Josefin Sans', sans-serif; margin-left: 5%; margin-right: 5%; background: #ffffff; border: 2px solid #3baba0;">Search Results</h4>
            <div class="table-responsive">
            <form method="post" action="{{url('/loadSelectedCarriers')}}">
            {{csrf_field()}}
                <table id="mytable" class="table table-bordred table-striped" style=" font-family: 'Josefin Sans'; font-size: 80%;">

                    <thead>
                        <th>Carrier Name</th>
                        <th>RC Number</th>
                        <th>Select Carriers</th>
                        <!-- <th>Action</th> -->
                    </thead>
                    <tbody>


                    @foreach($res as $collection)

                        @foreach($collection as $item)

                        <tr>
                            <td>{{ $item->companyName }}</td>
                            <td>{{ $item->rcNumber }}</td>
                            <td><input type="checkbox" name="selectedCarriers[]" value="{{$item->user_id}}"></td>
                            <!-- 
                            <td><a class="label label-success"  href='{{ url("/carrierDetail/{$item->user_id}/{$serviceIDNo}") }}'>View Details</a> </td> -->
                        </tr>

                        @endforeach

                    @endforeach

                    </tbody>
                        <input type="hidden" name="serviceIDNo" value="{{$serviceIDNo}}">
                </table>
                <div>
                    <button type="submit" class="btn btn-md btn-success">Continue</button>
                </div>
            </form>

            </div>

        </div>
    </div>

</div>
<!--     ************************************************** -->
<!--BOTTOM NAV BAR-->


<div id="fade"></div>
        <div id="modal">
            <img id="loader" src="{{url('images/69.gif')}}" />
        </div>
    </div>

</body>

</html>