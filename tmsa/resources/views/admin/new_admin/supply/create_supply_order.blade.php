
    @include('client.includes.header')
    @include('client.includes.head')
    <!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-content-white page-md">
     <!--  <div class="modal fade bs-modal-sm alert-modal flex-container flex-center flex-vertical-align flex-responsive " id="cancel" tabindex="-1" role="dialog" aria-hidden="true" style="display:inherit;background:rgba(25, 24, 24, 0.48);opacity:1;"><div class="col44 loader-container"><img src="{{url('/assets/pages/img/loader.gif')}}"></div></div> -->
     
    </div>
        <div class="page-wrapper">
            <!-- BEGIN HEADER -->
                @include('client.includes.menuBar')

            <!-- END HEADER -->
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
            <!-- END HEADER & CONTENT DIVIDER -->
            <!-- BEGIN CONTAINER -->
            <div class="page-container">
                <!-- BEGIN SIDEBAR -->
                    @include('client.includes.sidebar')
                <!-- END SIDEBAR -->
                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <!-- BEGIN PAGE HEADER-->
                        <!-- BEGIN THEME PANEL -->
                       
                        <!-- END THEME PANEL -->
                        <!-- BEGIN PAGE BAR -->
                        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    <a href="index.html">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Dashboard</span>
                                </li>
                            </ul>
                            <div class="page-toolbar">
                                <div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
                                    <i class="icon-calendar"></i>&nbsp;
                                    <span class="thin uppercase hidden-xs"></span>&nbsp;
                                    <i class="fa fa-angle-down"></i>
                                </div>
                            </div>
                        </div>
                        <!-- END PAGE BAR -->
                        <!-- BEGIN PAGE TITLE-->
                        <h1 class="page-title">
                        </h1>
                        <!-- END PAGE TITLE-->
                        <!-- END PAGE HEADER-->
<!--  -->                        <div class="row">
                            <div class="col-lg-10 col-lg-offset-1 col-xs-12 col-sm-12">
                                <div class="portlet light ">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <span class="caption-subject font-dark bold uppercase">Create Supply Order</span>                                         
                                      </div>
                                        
                                    </div>
                                     <div class="portlet-body form">
                                        <form role="form" action="{{url('/submitNewSupplyOrder')}}" ajax-submit="true"  class="ajax-form-submit" enctype="multipart/form-data">
                                         {{csrf_field()}}
                                            <div class="form-body">
                                              <div class="row">
                                               
                                                <div class="col-md-6">
                                                  <div class="form-group form-md-line-input has-info">
                                                      <input class="form-control form-copy validate" name="itemDescription" id="contactName"  placeholder="Item you want to order" bind-to="summaryContactPerson">
                                                        
                                                      <label for="form_control_1">Item</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                  <div class="form-group form-md-line-input">
                                                    <input type="number" class="form-control form-copy validate" id="quantity"  bind-to="quantity" name="quantity">
                                                    <label for="form_control_1">Order Quantity</label>
                                                  </div>
                                                </div>
                                                
                                                <!-- <div class="col-md-6">
                                                   <div class="form-group form-md-line-input has-info">
                                                      <select class="form-control form-copy validate" id="pickupTime" name="pickupTime"  bind-to="summaryPickTimeWindow">
                                                          <option value="">[-Delivery Time-]</option>
                                                            <option value="0000Hrs - 0600Hrs">0000Hrs - 0600Hrs</option>
                                                            <option value="0600Hrs - 1200Hrs">0600Hrs - 1200Hrs</option>
                                                            <option value="1200Hrs - 1800Hrs">1200Hrs - 1800Hrs</option>
                                                            <option value="1800Hrs - 0600Hrs">1800Hrs - 0600Hrs</option>
                                                      </select>
                                                      <label for="form_control_1">Pickup Time Window</label>
                                                    </div>
                                                </div> -->
                                                <!-- <div class="col-md-6">
                                                  <div class="form-group form-md-line-input">
                                                    <textarea type="text" class="form-control form-copy validate" id="itemDescription" placeholder="Enter Description Here"  bind-to="itemDescription" name="itemDescription"></textarea>
                                                    <label for="form_control_1">What are you ordering</label>
                                                    <span class="help-block">write a short description about items ordere</span>
                                                  </div>
                                                </div> -->

                                                

                                                <!-- <div class="col-md-6">
                                                  <div class="form-group form-md-line-input">
                                                    <input type="file" class="form-control form-copy validate" id="purchaseOrder"  bind-to="purchaseOrder" name="purchaseOrder">
                                                    <label for="form_control_1">Purchase Order</label>
                                                  </div>
                                                </div> -->
                                                

                                                <div class="col-md-6">
                                                  <div class="form-group">
                                                    <label for="form_control_1">Expiry Date</label>
                                                    <div class="input-group input-medium date date-picker" data-date-format="dd-mm-yyyy" data-date-start-date="+1d" style="width: 100% !important">
                                                            <input type="text" name="expiryDate" id="expiryDate" class="form-control  form-copy validate" readonly  bind-to="expiryDate">
                                                            <span class="input-group-btn">
                                                                <button class="btn default" type="button">
                                                                    <i class="fa fa-calendar"></i>
                                                                </button>
                                                            </span>
                                                    </div>
                                                  </div>
                                                </div>
                                                <div class="col-md-6">
                                                  <div class="form-group form-md-line-input" style="padding-top: 0px;">
                                                    <label for="multiple1" class="control-label">Select Suppliers Group</label>
                                                    <select id="multiple1" class="form-control select2-multiple" name="groups[]" multiple placeholder='select groups'>
                                                        @forelse($companyGroups as $group)
                                                          <option value="{{$group->id}}">{{$group->groupName}}</option>
                                                         @empty
                                                           no group
                                                        @endforelse
                                                          

                                                    </select>
                                                  </div>

                                                </div>

                                                <div class="col-md-6">
                                                  <div class="form-group form-md-line-input has-info">
                                                      <input type="file" class="form-control form-copy validate" name="purchaseOrder" >
                                                        
                                                      <label for="form_control_1">Upload Purchase Order</label>
                                                    </div>
                                                </div>
                                                <!-- 
                                                <div class="col-md-6">
                                                  <div class="form-group form-md-line-input has-info">
                                                      <input class="form-control form-copy  validate" name="contactPhone"  bind-to="summaryContactnum">
                                                        
                                                      <label for="form_control_1">Contact Number</label>
                                                    </div>
                                                </div> -->
                                              </div>
                                            </div>
                                            <div class="form-actions noborder">
                                                <button  type="submit" class="btn btn-primary">Submit</button>
                                               
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        
                    </div>
                    <!-- END CONTENT BODY -->
                </div>
                <!-- END CONTENT -->
                <!-- BEGIN QUICK SIDEBAR -->
              
                <!-- END QUICK SIDEBAR -->
            </div>
            <!-- END CONTAINER -->
            <!-- BEGIN FOOTER -->
            <div class="page-footer">
                <div class="page-footer-inner"> 2018 &copy; Truckka  &nbsp;|&nbsp;
                   
                </div>
                <div class="scroll-to-top">
                    <i class="icon-arrow-up"></i>
                </div>
            </div>
            <!-- END FOOTER -->
        </div>
        <!-- BEGIN QUICK NAV -->
        
        <div class="quick-nav-overlay"></div>
 @include('client.includes.footer')
