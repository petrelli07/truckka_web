<div class="page-sidebar-wrapper">
										<!-- BEGIN SIDEBAR -->
										<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
										<!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
										<div class="page-sidebar navbar-collapse collapse">
												<!-- BEGIN SIDEBAR MENU -->
												<!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
												<!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
												<!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
												<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
												<!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
												<!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
												<ul class="page-sidebar-menu  page-header-fixed page-sidebar-menu-light " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
														<!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
														<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
														<li class="sidebar-toggler-wrapper hide">
																<div class="sidebar-toggler">
																		<span></span>
																</div>
														</li>
                            <li class="nav-item truckka-back nav-item-head">
                                    <a href="{{ url('/createNew') }}" class="nav-link nav-toggle white bold ">
                                        <div class="flex-container flex-space-between flex-vertical-align">
                                          <span class="title">Home</span>
                                          <span class=" "><i class="fas fa-home"></i></span>
                                        </div>
                                    </a>
                                </li>
														<!-- END SIDEBAR TOGGLER BUTTON -->
														<!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
    													<li class="nav-item truckka-back nav-item-head">
    																<a href="javascript:;" class="nav-link nav-toggle white bold ">
    																		<div class="flex-container flex-space-between flex-vertical-align">
    																			<span class="title">Manage User Groups</span>
    																			<span class=" "><i class="fas fa-plus-square"></i></span>
    																		</div>
    																</a>
    																<ul class="sub-menu box-shadow" >
    																		<li class="nav-item inner-nav-item">
    																				<a href="{{url('/createNew')}}" class="nav-link f-parargraph black mobile-white" style="font-size: 14px;">
    																					<div class="flex-container flex-space-between flex-vertical-align">
    																						<span>Create New</span>
    																						<span><i class="fas fa-cart-plus"></i></span>
    																					</div>
    																				</a>
    																		</li>

    																	   <li class="nav-item inner-nav-item">
    																				<a href="{{url('/viewAllUsers')}}" class="nav-link f-parargraph black mobile-white" style="font-size: 14px;">
    																					<div class="flex-container flex-space-between flex-vertical-align">
    																						<span>View All Users</span>
    																						<span><i class="fas fa-cart-arrow-down"></i></span>
    																					</div>
    																				</a>
    																		</li>
    																</ul>
    													</li>


    													<li class="nav-item truckka-back nav-item-head">
    																<a href="javascript:;" class="nav-link nav-toggle white bold ">
    																		<div class="flex-container flex-space-between flex-vertical-align">
    																			<span class="title">Manage Client Orders</span>
    																			<span class=" "><i class="fas fa-plus-square"></i></span>
    																		</div>
    																</a>
    																<ul class="sub-menu box-shadow" >
    																		<li class="nav-item inner-nav-item">
    																				<a href="{{url('/viewClientOrders')}}" class="nav-link f-parargraph black mobile-white" style="font-size: 14px;">
    																					<div class="flex-container flex-space-between flex-vertical-align">
    																						<span>View All Orders</span>
    																						<span><i class="fas fa-cart-plus"></i></span>
    																					</div>
    																				</a>
    																		</li>

    																	   <li class="nav-item inner-nav-item">
    																				<a href="{{url('/viewOrders')}}" class="nav-link f-parargraph black mobile-white" style="font-size: 14px;">
    																					<div class="flex-container flex-space-between flex-vertical-align">
    																						<span>Manage Client Invoices</span>
    																						<span><i class="fas fa-cart-arrow-down"></i></span>
    																					</div>
    																				</a>
    																		</li>
    																		<li class="nav-item inner-nav-item">
    																				<a href="{{url('/getAll')}}" class="nav-link f-parargraph black mobile-white" style="font-size: 14px;">
    																					<div class="flex-container flex-space-between flex-vertical-align">
    																						<span>Manage Waybills</span>
    																						<span><i class="fas fa-cart-arrow-down"></i></span>
    																					</div>
    																				</a>
    																		</li>
    																</ul>
    													</li>



													<li class="nav-item truckka-back nav-item-head">
																<a href="javascript:;" class="nav-link nav-toggle white bold ">
																		<div class="flex-container flex-space-between flex-vertical-align">
																			<span class="title">Performance</span>
																			<span class=" "><i class="fas fa-plus-square"></i></span>
																		</div>
																</a>
																<ul class="sub-menu box-shadow" >

																		<li class="nav-item inner-nav-item">
																				<a href="{{url('/performance')}}" class="nav-link f-parargraph black mobile-white" style="font-size: 14px;">
																					<div class="flex-container flex-space-between flex-vertical-align">
																						<span>KPIs</span>
																						<span><i class="fas fa-file-invoice-dollar"></i></span>
																					</div>
																				</a>
																		</li>
																</ul>
													</li>




                          <li class="nav-item truckka-back nav-item-head">
                                <a href="javascript:;" class="nav-link nav-toggle white bold ">
                                    <div class="flex-container flex-space-between flex-vertical-align">
                                      <span class="title">Track and Trace</span>
                                      <span class=" "><i class="fas fa-plus-square"></i></span>
                                    </div>
                                </a>
                                <ul class="sub-menu box-shadow" >

                                    <li class="nav-item inner-nav-item">

                                        <a href="{{url('/client/map/view')}}" class="nav-link f-parargraph black mobile-white" style="font-size: 14px;">
                                          <div class="flex-container flex-space-between flex-vertical-align">
                                            <span>Track</span>
                                            <span><i class="fas fa-file-invoice-dollar"></i></span>
                                          </div>
                                        </a>
                                    </li>

                                    <li class="nav-item inner-nav-item">

                                        <a href="{{url('/client/map/viewHistory')}}" class="nav-link f-parargraph black mobile-white" style="font-size: 14px;">
                                          <div class="flex-container flex-space-between flex-vertical-align">
                                            <span>View Resource History</span>
                                            <span><i class="fas fa-file-invoice-dollar"></i></span>
                                          </div>
                                        </a>
                                    </li>
                                </ul>
                          </li>


                                  <li class="nav-item truckka-back nav-item-head">
                                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
	                                  <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="nav-link nav-toggle white bold ">
	                                      <div class="flex-container flex-space-between flex-vertical-align">
	                                        <span class="title">Logout</span>
	                                        <span class=" "><i class="fas fa-plus-square"></i></span>
	                                      </div>
	                                  </a>
                              	</li>

												</ul>
												<!-- END SIDEBAR MENU -->
												<!-- END SIDEBAR MENU -->
										</div>
										<!-- END SIDEBAR -->
								</div>