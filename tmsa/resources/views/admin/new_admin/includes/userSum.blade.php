<div class="row user-summary-mob" style="margin-top: 100px; margin-bottom: 80px; ">
                            
                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <div class="dashboard-stat2 ">
                                  <div class="flex-container flex-center summary-icon image-flex-fix">
                                    <!-- <img src="https://cdn0.iconfinder.com/data/icons/commerce-and-retail/512/delivery_shipping_truck_business_van_transport_car_service_moving_courier_logistics_freight_logistic_package_shipment_cargo_transportation_commerce_flat_design_icon-512.png"> -->
                                  </div>
                                    <div class="display">
                                        <div class="number">
                                            <h3 class="font-red-haze align-center">
                                                <span data-counter="counterup" data-value="1349">{!! $chart->html() !!}</span>
                                            </h3>
                                        </div>
                                        <!-- <div class="icon">
                                            <i class="icon-like"></i>
                                        </div> -->
                                    </div>
                                    
                                </div>
                                <div class="summary-tag truckka-back">
                                  <p class="f-paragraph align-center default-p">Orders</p>
                                </div>
                            </div>
                             <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <div class="dashboard-stat2 ">
                                  <div class="flex-container flex flex-center image-flex-fix summary-icon">
                                    <!-- <img src="https://cdn.iconscout.com/icon/premium/png-512-thumb/transport-service-758039.png"> -->
                                  </div>
                                    <div class="display">
                                        <div class="number">
                                            <h3 class="font-red-haze align-center">
                                                <span data-counter="counterup" data-value="1349">{!! $pie_chart->html() !!}</span>
                                            </h3>
                                        </div>
                                        <!-- <div class="icon">
                                            <i class="icon-like"></i>
                                        </div> -->
                                    </div>
                                    
                                </div>
                                <div class="summary-tag truckka-back">
                                  <p class="f-paragraph align-center default-p">Invoices</p>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <div class="dashboard-stat2 ">
                                  <div class="flex-container image-flex-fix flex-center summary-icon">
                                    <!-- <img src="https://image.pbs.org/poster_images/assets/pfm4cdi0zkn8nk7der1j_1.png.resize.710x399.png"> -->
                                  </div>
                                    <div class="display">
                                        <div class="number">
                                            <h3 class="font-red-haze align-center">
                                                <span data-counter="counterup" data-value="1349">{!! $costPerDay->html() !!}</span>
                                            </h3>
                                        </div>
                                        <!-- <div class="icon">
                                            <i class="icon-like"></i>
                                        </div> -->
                                    </div>
                                    
                                </div>
                                <div class="summary-tag truckka-back">
                                  <p class="f-paragraph align-center default-p">Costs</p>
                                </div>
                            </div>
                             <!-- <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <div class="dashboard-stat2 ">
                                  <div class="flex-container image-flex-fix flex-center summary-icon">
                                    <img src="http://icons.iconarchive.com/icons/flat-icons.com/flat/256/Wallet-icon.png">
                                  </div>
                                    <div class="display">
                                        <div class="number">
                                            <h3 class="font-red-haze align-center">
                                                <span data-counter="counterup" data-value="1349">0</span>
                                            </h3>
                                        </div>
                                        
                                    </div>
                                    
                                </div>
                                <div class="summary-tag truckka-back">
                                  <p class="f-paragraph align-center default-p">Wallet Balance</p>
                                </div>
                            </div> -->
                        </div>