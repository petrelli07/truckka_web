<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>TMSA</title>
    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css" href="{{ url('css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ url('css/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('css/indexupdate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('css/tmsa.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('css/loader.css') }}">
    <link href="{{ url('https://fonts.googleapis.com/css?family=Titillium+Web|Concert+One|Josefin+Sans|Roboto') }}" rel="stylesheet">
    <script type="text/javascript" src="{{ url('js/admin_dashboard.js')}}"></script>
    <script type="text/javascript" src="{{ url('js/jquery-3.2.1.js')}}"></script>
    <script type="text/javascript" src="{{ url('js/bootstrap.js')}}"></script>

    <script type="text/javascript" src="{{ url('js/newUser.js')}}"></script>
    <script type="text/javascript" src="{{ url('js/newCarrier.js')}}"></script>
    <script type="text/javascript" src="{{ url('js/newClient.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function() {

            $(document).ajaxStart(function(){
                document.getElementById('modal').style.display = 'block';
                document.getElementById('fade').style.display = 'block';
            });

            $(document).ajaxComplete(function(){
                document.getElementById('modal').style.display = 'none';
                document.getElementById('fade').style.display = 'none';
            });
        });
        
    </script>

    <script type="text/javascript">
    //     $(document).ready(function() {
    //         $(".selectAllClient").click(function(){
    //             //$(".rtClient").attr("checked", "true");
    //             $('input.rtClient').prop('checked',this.checked)
    //         });

    //         $(".selectAllRoutes").click(function(){
    //             //$(".rtClient").attr("checked", "true");
    //             $('input.allRoutes').prop('checked',this.checked)
    //         });
    //     });
    // </script>


    <script>

        // $(document).ready(function() {

        //     $('.createClient').hide();
        //     $('.createCarrier').hide();
        //     $('.createAdmin').hide();

        //     $('#userCategory').on('change', function() {
        //         if ( this.value == '1')
        //         {

        //             $('.createAdmin').hide();
        //             $('.createCarrier').hide();
        //             $('.createClient').show();

        //         }
        //         else if ( this.value == '2'){

        //             $('.createAdmin').hide();
        //             $('.createCarrier').show();
        //             $('.createClient').hide();
        //         }else if (this.value == '0'){

        //             $('.createAdmin').show();
        //             $('.createCarrier').hide();
        //             $('.createClient').hide();
        //         }else if(this.value == ''){

        //             $('.createClient').hide();
        //             $('.createCarrier').hide();
        //             $('.createAdmin').hide();

        //         }
        //     });
        // });
    </script>

</head>
<body class="col-md-12-col-sm-6 col-xs-12" style="padding:0; ">

</div>
<!--TMSA-->
<div class="tmsa">
    <div class="row" style=" margin-right:0; margin-left:0;">
        <div class="side_bar col-md-3" >
            <div class=" identifier hidden-xs hidden-sm" >
                <span class="navbar-brand type" >T</span>
                <span class="navbar-brand type">M</span>
                <span class="navbar-brand type">S</span>
                <span class="navbar-brand type">A</span>
            </div>
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed pull-left" data-toggle="collapse" data-target="#hidden_bar" aria-expanded="false" style="border: 2px solid #fff ; margin-left:10px;">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar" style="border:1px solid #fff"></span>
                    <span class="icon-bar" style="border:1px solid #fff"></span>
                    <span class="icon-bar" style="border:1px solid #fff"></span>
                </button>
                <a class="navbar-brand" href="#">
                    <div class="brand">
                        <span class="navbar-brand  logoT" style="border: 1px solid #ffffff; margin-left: 2px; border-radius: 10px 0 0 0; font-family: 'Concert One', cursive; color: #ffffff" >T</span>
                        <span class="navbar-brand  logoR" style="border: 1px solid #ffffff;  margin-left: 2px; font-family: 'Concert One', cursive; background: #ffffff; color: #3baba0;">R</span>
                        <span class="navbar-brand   logoU" style="border: 1px solid #ffffff;  margin-left: 2px; font-family: 'Concert One', cursive; color: #ffffff;">U</span>
                        <span class="navbar-brand   logoC" style="border: 1px solid #ffffff;  margin-left: 2px;font-family: 'Concert One', cursive; background: #ffffff; color: #3baba0;">C</span>
                        <span class="navbar-brand  logoK" style="border: 1px solid #ffffff;  margin-left: 2px;font-family: 'Concert One', cursive; color: #ffffff"; >K</span>
                        <span class="navbar-brand   logoK" style="border: 1px solid #ffffff;  margin-left: 2px;font-family: 'Concert One', cursive; background: #ffffff; color: #3baba0;">K</span>
                        <span class="navbar-brand   logoA" style="border: 1px solid #ffffff;  margin-left: 2px; border-radius: 0 0 10px 0;font-family: 'Concert One', cursive; color: #ffffff;">A</span>
                    </div>
                </a>
            </div>
<!--NAVBAR FOR MOBIE SCREENS-->
            <ul class="nav navbar-nav collapse" id="hidden_bar" style="margin-top:50px;">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage User Groups<span class="glyphicon glyphicon-user pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{url('/home')}}">Create Users<span class="glyphicon glyphicon-plus-sign pull-right"></span></a></li>
                        <li class="divider"></li>
                        <li><a href="{{url('/viewAllUsers')}}">View All Users <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage Routes<span class="glyphicon glyphicon-map-marker pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{url('/newRoute')}}">Upload Route File<span class="glyphicon glyphicon-map-marker pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Client Agents<span class="glyphicon glyphicon-file pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{url('/viewAllClients')}}">Create Client Agents<span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage Client Orders <span class="glyphicon glyphicon-file pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{url('/viewOrders')}}">View All Orders <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Incident Management <span class="glyphicon glyphicon-file pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{url('/incidentManagement')}}">View All Incidents <span class="glyphicon glyphicon-open-file pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage Resources <span class="glyphicon glyphicon-wrench pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href='{{ url("/viewAllResources") }}'>View All Resources <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Track and Trace <span class="glyphicon glyphicon-map-marker pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href='{{ url("/allGPS") }}'>Track Orders <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();" class="dropdown-toggle" data-toggle="dropdown">Log Out <span class="glyphicon glyphicon-log-out pull-right"></span></a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>


            </ul>
            <!--NAVBAR FOR laptop SCREENS-->
            <ul class="nav navbar-nav hidden-xs hidden-sm" style="margin-top:50px;">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage User Groups<span class="glyphicon glyphicon-user pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{url('/home')}}">Create Users<span class="glyphicon glyphicon-plus-sign pull-right"></span></a></li>
                        <li class="divider"></li>
                        <li><a href="{{url('/viewAllUsers')}}">View All Users <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage Routes<span class="glyphicon glyphicon-map-marker pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{url('/newRoute')}}">Upload Route File<span class="glyphicon glyphicon-map-marker pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Client Agents<span class="glyphicon glyphicon-file pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{url('/viewAllClients')}}">Create Client Agents<span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage Client Orders <span class="glyphicon glyphicon-file pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{url('/viewOrders')}}">View All Orders <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Incident Management <span class="glyphicon glyphicon-file pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{url('/incidentManagement')}}">View All Incidents <span class="glyphicon glyphicon-open-file pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage Resources <span class="glyphicon glyphicon-wrench pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href='{{ url("/viewAllResources") }}'>View All Resources <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Track and Trace <span class="glyphicon glyphicon-map-marker pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href='{{ url("/allGPS") }}'>Track Orders <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();" class="dropdown-toggle" data-toggle="dropdown">Log Out <span class="glyphicon glyphicon-log-out pull-right"></span></a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>


            </ul>
        </div>
        <!-- TSMA MAIN PAGE -->
        <div class="tmsa_main_body col-md-9">
            <div class="tmsa_user_page" >
                <!session message starts-->

                @if(session('message'))
                <p>
                    {{ session('message') }}
                </p>
                @endif

                <!session message ends-->
                <div class="alert alert-danger print-error-msg" style="display:none">

                    <ul></ul>
                </div>

                <div class="alert alert-success print-success-msg" style="display:none">

                    <ul></ul>
                </div>
                <!-- FORM -->
                        <div class="form-group hidden-xs hidden-sm" >
                            <div class="input-group" style="margin-left:10%; margin-right: 10%; margin-bottom: 3%; border-radius:10px;">
                                <label for="addUser" class="input-group-addon" style=" border-radius:10px; height: 50px; font-family: Roboto; font-size:150%; font-weight: 1000;"> MANAGE ROUTES <span class="glyphicon glyphicon-map-marker"></span></label>
                            </div>
                        </div>
                        <div class="form-group hidden-md hidden-lg" >
                            <div class="input-group" style="margin-left:10%; margin-right: 10%; margin-bottom: 3%; border-radius:10px;">
                                <label for="addUser" class="input-group-addon" style=" border-radius:10px; height: 50px; font-family: Roboto; font-size:80%; font-weight: 1000;"> MANAGE ROUTES <span class="glyphicon glyphicon-map-marker"></span></label>
                            </div>
                        </div>
                        

                    <div class="createAdmin">
                        <form id="routeFileUpload">
                            <fieldset>
                                {{ csrf_field() }}
                                <div class="form-group " style="margin-left:10%; margin-right: 10%;  margin-bottom: 3%;">
                                    <label><h4>Upload Route File</h4></label>
                                    <div class="col-md-12">
                                        <label class="sr-only" for="form-first-name">Upload Route File</label>
                                        <input type="file" class="form-control" style="width:50%" id="routeFile" name="routeFile" required="required">

                                    </div>
                                </div>

                            <div class="pull-left" style="margin-left:118px; margin-top:10px;">
                                <div align="center">
                                    <button id="btn_registrar" name="btn_registrar" class="btn button_login uploadRouteFile">SUBMIT</button>
                                </div>
                            </div>

                            </fieldset>
                        </form>
                    </div>

            </div>

        </div>
    </div>
    <!--BOTTOM NAV BAR-->
    <!-- <div class = "bottom footer" style="height: auto; width:100%; background-color: #3baba0; ">
        <p class="text-center" style="color: #232323; margin-top: 5px; background-color: #3baba0;" >Copyright © TRUCKKA 2018. All rights reserved.</p>
    </div> -->
    <div id="fade"></div>
        <div id="modal">
            <img id="loader" src="{{url('images/69.gif')}}" />
        </div>
</div>

<!--     ************************************************** -->
</body>
</html>

