<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>TMSA</title>
    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css" href="{{ url('css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ url('css/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('css/indexupdate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('css/tmsa.css') }}">
    <link href="{{ url('https://fonts.googleapis.com/css?family=Titillium+Web|Concert+One|Josefin+Sans|Roboto') }}" rel="stylesheet">
    <script type="text/javascript" src="{{ url('js/admin_dashboard.js')}}"></script>
    <script type="text/javascript" src="{{ url('js/jquery-3.2.1.js')}}"></script>
    <script type="text/javascript" src="{{ url('js/bootstrap.js')}}"></script>

</head>


<body class="col-md-12-col-sm-6 col-xs-12" style="padding:0; height:auto">
<div class="row" style="height:100vh; margin-right:0; margin-left:0; background-color: #232323;">
<div class="side_bar col-md-3">
    <div class=" identifier hidden-xs hidden-sm" >
        <span class="navbar-brand type" >T</span>
        <span class="navbar-brand type">M</span>
        <span class="navbar-brand type">S</span>
        <span class="navbar-brand type">A</span>
    </div>
    <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed pull-left" data-toggle="collapse" data-target="#hidden_bar" aria-expanded="false" style="border: 2px solid #fff ; margin-left:10px;">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar" style="border:1px solid #fff"></span>
            <span class="icon-bar" style="border:1px solid #fff"></span>
            <span class="icon-bar" style="border:1px solid #fff"></span>
        </button>
        <a class="navbar-brand" href="#">
            <div class="brand">
                <span class="navbar-brand  logoT" style="border: 1px solid #ffffff; margin-left: 2px; border-radius: 10px 0 0 0; font-family: 'Concert One', cursive; color: #ffffff" >T</span>
                <span class="navbar-brand  logoR" style="border: 1px solid #ffffff;  margin-left: 2px; font-family: 'Concert One', cursive; background: #ffffff; color: #3baba0;">R</span>
                <span class="navbar-brand   logoU" style="border: 1px solid #ffffff;  margin-left: 2px; font-family: 'Concert One', cursive; color: #ffffff;">U</span>
                <span class="navbar-brand   logoC" style="border: 1px solid #ffffff;  margin-left: 2px;font-family: 'Concert One', cursive; background: #ffffff; color: #3baba0;">C</span>
                <span class="navbar-brand  logoK" style="border: 1px solid #ffffff;  margin-left: 2px;font-family: 'Concert One', cursive; color: #ffffff"; >K</span>
                <span class="navbar-brand   logoK" style="border: 1px solid #ffffff;  margin-left: 2px;font-family: 'Concert One', cursive; background: #ffffff; color: #3baba0;">K</span>
                <span class="navbar-brand   logoA" style="border: 1px solid #ffffff;  margin-left: 2px; border-radius: 0 0 10px 0;font-family: 'Concert One', cursive; color: #ffffff;">A</span>
            </div>
        </a>
    </div>
    <!--NAVBAR FOR mobile SCREENS-->
    <ul class="nav navbar-nav collapse" id="hidden_bar" style="margin-top:50px;">
       <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage User Groups<span class="glyphicon glyphicon-user pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{url('/home')}}">Create Users<span class="glyphicon glyphicon-plus-sign pull-right"></span></a></li>
                        <li class="divider"></li>
                        <li><a href="{{url('/viewAllUsers')}}">View All Users <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Client Agents<span class="glyphicon glyphicon-file pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{url('/viewAllClients')}}">Create Client Agents<span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage Client Orders <span class="glyphicon glyphicon-file pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{url('/viewOrders')}}">View All Orders <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Incident Management <span class="glyphicon glyphicon-file pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{url('/incidentManagement')}}">View All Incidents <span class="glyphicon glyphicon-open-file pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage Resources <span class="glyphicon glyphicon-wrench pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href='{{ url("/viewAllResources") }}'>View All Resources <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Track and Trace <span class="glyphicon glyphicon-map-marker pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href='{{ url("/allGPS") }}'>Track Orders <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();" class="dropdown-toggle" data-toggle="dropdown">Log Out <span class="glyphicon glyphicon-log-out pull-right"></span></a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>


    </ul>
    <!--NAVBAR FOR laptop SCREENS-->
    <ul class="nav navbar-nav hidden-xs hidden-sm" style="padding-top:0;">
        <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage User Groups<span class="glyphicon glyphicon-user pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{url('/home')}}">Create Users<span class="glyphicon glyphicon-plus-sign pull-right"></span></a></li>
                        <li class="divider"></li>
                        <li><a href="{{url('/viewAllUsers')}}">View All Users <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Client Agents<span class="glyphicon glyphicon-file pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{url('/viewAllClients')}}">Create Client Agents<span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage Client Orders <span class="glyphicon glyphicon-file pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{url('/viewOrders')}}">View All Orders <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Incident Management <span class="glyphicon glyphicon-file pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{url('/incidentManagement')}}">View All Incidents <span class="glyphicon glyphicon-open-file pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage Resources <span class="glyphicon glyphicon-wrench pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href='{{ url("/viewAllResources") }}'>View All Resources <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Track and Trace <span class="glyphicon glyphicon-map-marker pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href='{{ url("/allGPS") }}'>Track Orders <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();" class="dropdown-toggle" data-toggle="dropdown">Log Out <span class="glyphicon glyphicon-log-out pull-right"></span></a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>


    </ul>
</div>
<!-- TSMA VIEW ORDERS PAGE -->
<div class="tmsa_main_body col-md-9" style="height: 100vh">
    <div class="row" style="height: 100vh">
        <div class="col-md-9 view-order" >
            <h4 align="center" class="btn-lg" style=" font-family: 'Josefin Sans', sans-serif; margin-left: 5%; margin-right: 5%; background: #ffffff; border: 2px solid #3baba0;">All Resources</h4>
            
                <iframe src="http://afrk.co/traccar/map/devices.php" style="width:100%;height:550px;border:0"></iframe>


        </div>
    </div>

    <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                    <h4 class="modal-title custom_align" id="Heading">Edit Your Detail</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <input class="form-control " type="text" placeholder="Mohsin">
                    </div>
                    <div class="form-group">

                        <input class="form-control " type="text" placeholder="Irshad">
                    </div>
                    <div class="form-group">
                        <textarea rows="2" class="form-control" placeholder="CB 106/107 Street # 11 Wah Cantt Islamabad Pakistan"></textarea>


                    </div>
                </div>
                <div class="modal-footer ">
                    <button type="button" class="btn btn-warning btn-lg" style="width: 100%;"><span class="glyphicon glyphicon-ok-sign"></span> Update</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>



    <div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                    <h4 class="modal-title custom_align" id="Heading">Delete this entry</h4>
                </div>
                <div class="modal-body">

                    <div class="alert alert-danger"><span class="glyphicon glyphicon-warning-sign"></span> Are you sure you want to delete this Record?</div>

                </div>
                <div class="modal-footer ">
                    <button type="button" class="btn btn-success" ><span class="glyphicon glyphicon-ok-sign"></span> Yes</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> No</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

</div>
<!--     ************************************************** -->
<!--BOTTOM NAV BAR-->


</body>

</html>