<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>TMSA</title>
    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css" href="{{ url('css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ url('css/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('css/indexupdate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('css/tmsa.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('css/loader.css') }}">
    <link rel="stylesheet" href="{{ url('css/modal.css') }}">
    <link href="{{ url('https://fonts.googleapis.com/css?family=Titillium+Web|Concert+One|Josefin+Sans|Roboto') }}" rel="stylesheet">
    <script type="text/javascript" src="{{ url('js/admin_dashboard.js')}}"></script>
    <script type="text/javascript" src="{{ url('js/jquery-3.2.1.js')}}"></script>
    <script type="text/javascript" src="{{ url('js/bootstrap.js')}}"></script>
    <script type="text/javascript" src="{{ url('js/updateIncident.js')}}"></script>
    <link rel="stylesheet" href="{{ url('css/modal.css') }}">
    <script type="text/javascript">
        $(document).ready(function() {

            $(document).ajaxStart(function(){
                document.getElementById('modal').style.display = 'block';
                document.getElementById('fade').style.display = 'block';
            });

            $(document).ajaxComplete(function(){
                document.getElementById('modal').style.display = 'none';
                document.getElementById('fade').style.display = 'none';
            });
        });
        
    </script>


</head>
<body class="col-md-12-col-sm-6 col-xs-12" style="padding:0; height:auto">
<div class="row" style="height:100vh; margin-right:0; margin-left:0; background:#232323; ">
    <div class="side_bar col-md-3" >
    <div class=" identifier hidden-xs hidden-sm" >
        <span class="navbar-brand type" >T</span>
        <span class="navbar-brand type">M</span>
        <span class="navbar-brand type">S</span>
        <span class="navbar-brand type">A</span>
    </div>
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed pull-left" data-toggle="collapse" data-target="#hidden_bar" aria-expanded="false" style="border: 2px solid #fff ; margin-left:10px;">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar" style="border:1px solid #fff"></span>
                <span class="icon-bar" style="border:1px solid #fff"></span>
                <span class="icon-bar" style="border:1px solid #fff"></span>
            </button>
            <a class="navbar-brand" href="#" class="hidden-md hidden-lg">
                <div class="brand hidden-md hidden-lg">
                    <span class="navbar-brand  logoT" style="border: 1px solid #ffffff; margin-left: 2px; border-radius: 10px 0 0 0; font-family: 'Concert One', cursive; color: #ffffff" >T</span>
                    <span class="navbar-brand  logoR" style="border: 1px solid #ffffff;  margin-left: 2px; font-family: 'Concert One', cursive; background: #ffffff; color: #3baba0;">R</span>
                    <span class="navbar-brand   logoU" style="border: 1px solid #ffffff;  margin-left: 2px; font-family: 'Concert One', cursive; color: #ffffff;">U</span>
                    <span class="navbar-brand   logoC" style="border: 1px solid #ffffff;  margin-left: 2px;font-family: 'Concert One', cursive; background: #ffffff; color: #3baba0;">C</span>
                    <span class="navbar-brand  logoK" style="border: 1px solid #ffffff;  margin-left: 2px;font-family: 'Concert One', cursive; color: #ffffff"; >K</span>
                    <span class="navbar-brand   logoK" style="border: 1px solid #ffffff;  margin-left: 2px;font-family: 'Concert One', cursive; background: #ffffff; color: #3baba0;">K</span>
                    <span class="navbar-brand   logoA" style="border: 1px solid #ffffff;  margin-left: 2px; border-radius: 0 0 10px 0;font-family: 'Concert One', cursive; color: #ffffff;">A</span>
                </div>
            </a>
        </div>
        <!--NAVBAR FOR mobile SCREENS-->
        <ul class="nav navbar-nav collapse" id="hidden_bar" style="margin-top:50px; background:#3baba0;">
            <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage User Groups<span class="glyphicon glyphicon-user pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{url('/home')}}">Create Users<span class="glyphicon glyphicon-plus-sign pull-right"></span></a></li>
                        <li class="divider"></li>
                        <li><a href="{{url('/viewAllUsers')}}">View All Users <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Client Agents<span class="glyphicon glyphicon-file pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{url('/viewAllClients')}}">Create Client Agents<span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage Client Orders <span class="glyphicon glyphicon-file pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{url('/viewOrders')}}">View All Orders <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Incident Management <span class="glyphicon glyphicon-file pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{url('/incidentManagement')}}">View All Incidents <span class="glyphicon glyphicon-open-file pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage Resources <span class="glyphicon glyphicon-wrench pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href='{{ url("/viewAllResources") }}'>View All Resources <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Track and Trace <span class="glyphicon glyphicon-map-marker pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href='{{ url("/allGPS") }}'>Track Orders <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();" class="dropdown-toggle" data-toggle="dropdown">Log Out <span class="glyphicon glyphicon-log-out pull-right"></span></a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>


        </ul>
        <!--NAVBAR FOR laptop SCREENS-->
        <ul class="nav navbar-nav hidden-xs hidden-sm" style="padding-top:0;">
            <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage User Groups<span class="glyphicon glyphicon-user pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{url('/home')}}">Create Users<span class="glyphicon glyphicon-plus-sign pull-right"></span></a></li>
                        <li class="divider"></li>
                        <li><a href="{{url('/viewAllUsers')}}">View All Users <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Client Agents<span class="glyphicon glyphicon-file pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{url('/viewAllClients')}}">Create Client Agents<span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage Client Orders <span class="glyphicon glyphicon-file pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{url('/viewOrders')}}">View All Orders <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Incident Management <span class="glyphicon glyphicon-file pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{url('/incidentManagement')}}">View All Incidents <span class="glyphicon glyphicon-open-file pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage Resources <span class="glyphicon glyphicon-wrench pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href='{{ url("/viewAllResources") }}'>View All Resources <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Track and Trace <span class="glyphicon glyphicon-map-marker pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href='{{ url("/allGPS") }}'>Track Orders <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();" class="dropdown-toggle" data-toggle="dropdown">Log Out <span class="glyphicon glyphicon-log-out pull-right"></span></a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>


        </ul>
        <!--NAVBAR FOR laptop SCREENS-->
        <ul class="nav navbar-nav hidden-xs hidden-sm">
            <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage User Groups<span class="glyphicon glyphicon-user pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{url('/home')}}">Create Users<span class="glyphicon glyphicon-plus-sign pull-right"></span></a></li>
                        <li class="divider"></li>
                        <li><a href="{{url('/viewAllUsers')}}">View All Users <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Client Agents<span class="glyphicon glyphicon-file pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{url('/viewAllClients')}}">Create Client Agents<span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage Client Orders <span class="glyphicon glyphicon-file pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{url('/viewOrders')}}">View All Orders <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Incident Management <span class="glyphicon glyphicon-file pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{url('/incidentManagement')}}">View All Incidents <span class="glyphicon glyphicon-open-file pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage Resources <span class="glyphicon glyphicon-wrench pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href='{{ url("/viewAllResources") }}'>View All Resources <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Track and Trace <span class="glyphicon glyphicon-map-marker pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href='{{ url("/allGPS") }}'>Track Orders <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();" class="dropdown-toggle" data-toggle="dropdown">Log Out <span class="glyphicon glyphicon-log-out pull-right"></span></a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>

        </ul>
        <!--NAVBAR FOR laptop SCREENS-->
        <ul class="nav navbar-nav hidden-xs hidden-sm">
            <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage User Groups<span class="glyphicon glyphicon-user pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{url('/home')}}">Create Users<span class="glyphicon glyphicon-plus-sign pull-right"></span></a></li>
                        <li class="divider"></li>
                        <li><a href="{{url('/viewAllUsers')}}">View All Users <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Client Agents<span class="glyphicon glyphicon-file pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{url('/viewAllClients')}}">Create Client Agents<span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage Client Orders <span class="glyphicon glyphicon-file pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{url('/viewOrders')}}">View All Orders <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Incident Management <span class="glyphicon glyphicon-file pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{url('/incidentManagement')}}">View All Incidents <span class="glyphicon glyphicon-open-file pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage Resources <span class="glyphicon glyphicon-wrench pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href='{{ url("/viewAllResources") }}'>View All Resources <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Track and Trace <span class="glyphicon glyphicon-map-marker pull-right"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href='{{ url("/allGPS") }}'>Track Orders <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();" class="dropdown-toggle" data-toggle="dropdown">Log Out <span class="glyphicon glyphicon-log-out pull-right"></span></a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>


        </ul>
    </div>
    <!-- TSMA MAIN PAGE -->
    <div class="tmsa_main_body col-md-9 col-xs-12 col-sm-12" style=" background: rgba(59,171,160, 0.5); padding-top:5%; height:100vh">

        <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 text-center" style="height:100vh">
            <div class="well" >
                <div>
                    <a class="btn btn-success edit" href='{{ url("/incidentThread/{$incidentReport}") }}' title="View Details">
                        View Incident Thread <i class="fa fa-eye" aria-hidden="true"></i>
                    </a>
                </div>
                @foreach($incidentReportID as $irt)
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <p><h4>Incident ID:</h4> {{$irt->incidentReportIDNo}} </p>
                        <p><h4>Service ID:</h4> {{$irt->service_request_id}}  </p>
                        <p><h4>Service ID:</h4> {{$irt->incident_title}}  </p>
                    </div>
                </div>
                @endforeach

                <h4>Incident Log</h4>
                <form id="updateIncident">
                <div class="input-group">
                    <input type="text" id="userComment" name="incidentComment" class="form-control input-sm chat-input" placeholder="Log your incidence here..." />
                    <input type="hidden" name="reportID" value="{{$incidentReport}}">
                      <span class="input-group-btn">
                            <a href="#" class="btn btn-primary btn-sm incidentUpdateThread" id="save"><span class="glyphicon glyphicon-comment"></span> Save Incident</a>
                        </span>
                </div>
                </form>
                <hr data-brackets-id="12673">
                <!--<ul data-brackets-id="12674" id="sortable" class="list-unstyled ui-sortable hidden">
                    <strong class="pull-left primary-font">Admin</strong>
                    <small class="pull-right text-muted">
                        <span class="glyphicon glyphicon-time"></span>7 mins ago</small>
                    </br>
                    <li id ="ui-state-default"  class="text-justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </li>
                    </br>
                    <hr style="border: 2px solid #3baba0;">
                     <strong class="pull-left primary-font">Taylor</strong>
                   <small class="pull-right text-muted">
                      <span class="glyphicon glyphicon-time"></span>14 mins ago</small>
                   </br>
                   <li id="ui-state-default" class="text-justify">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</li>

                </ul>-->
            </div>
        </div>
    </div>
</div>
<!--BOTTOM NAV BAR-->

<div id="fade"></div>
<div id="modal">
    <img id="loader" src="{{url('images/69.gif')}}" />
</div>

</div>
<!--     ************************************************** -->
<div id="hello"></div>
<script type ="text/javascript">


    /*function addComment(){
        var userComment = document.getElementById("userComment").value;
        console.log(userComment)
        document.getElementById("ui-state-default").innerHTML= userComment;
    }
    function incidentLog(){
        $('#sortable').removeClass("hidden");

    }
*/
</script>
</body>
</html>

