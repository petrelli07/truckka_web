<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Confirmation email</title>
    <style type="text/css">
        /* Client-specific Styles */

        .ExternalClass {width:100%;} /* Force Hotmail to display emails at full width */
        .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;} /* Force Hotmail to display normal line spacing.  More on that: http://www.emailonacid.com/forum/viewthread/43/ */
        /*a {color: #e95353;text-decoration: none;text-decoration:none!important;}*/
        /*STYLES*/
        /*################################################*/
        /*IPAD STYLES*/
        /*################################################*/
        @media only screen and (max-width: 640px) {
            a[href^="tel"], a[href^="sms"] {
                text-decoration: none;
                color: #ffffff; /* or whatever your want */
                pointer-events: none;
                cursor: default;
            }
            .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
                text-decoration: default;
                color: #ffffff !important;
                pointer-events: auto;
                cursor: default;
            }
            table[class=devicewidth] {width: 440px!important;text-align:center!important;}
            table[class=devicewidthinner] {width: 420px!important;text-align:center!important;}
            table[class="sthide"]{display: none!important;}
            img[class="bigimage"]{width: 420px!important;height:219px!important;}
            img[class="col2img"]{width: 420px!important;height:258px!important;}
            img[class="image-banner"]{width: 440px!important;height:106px!important;}
            td[class="menu"]{text-align:center !important; padding: 0 0 10px 0 !important;}
            td[class="logo"]{padding:10px 0 5px 0!important;margin: 0 auto !important;}
            img[class="logo"]{padding:0!important;margin: 0 auto !important;}
        }
        /*##############################################*/
        /*IPHONE STYLES*/
        /*##############################################*/
        @media only screen and (max-width: 480px) {
            a[href^="tel"], a[href^="sms"] {
                text-decoration: none;
                color: #ffffff; /* or whatever your want */
                pointer-events: none;
                cursor: default;
            }
            .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
                text-decoration: default;
                color: #ffffff !important;
                pointer-events: auto;
                cursor: default;
            }
            table[class=devicewidth] {width: 280px!important;text-align:center!important;}
            table[class=devicewidthinner] {width: 260px!important;text-align:center!important;}
            table[class="sthide"]{display: none!important;}
            img[class="bigimage"]{width: 260px!important;height:136px!important;}
            img[class="col2img"]{width: 260px!important;height:160px!important;}
            img[class="image-banner"]{width: 280px!important;height:68px!important;}
        }
    </style>
</head>
<body style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;margin:0;padding:0;width:100% !important">
<div class="block">
    <!-- Start of preheader -->
    <table width="100%" bgcolor="#f6f4f5" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="preheader" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;margin:0;padding:0;width:100% !important;line-height:100% !important">
        <tbody>
        <tr>
            <td width="100%" style="border-collapse:collapse">
                <table width="580" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt">
                    <tbody>
                    <!-- Spacing -->
                    <tr>
                        <td width="100%" height="5" style="border-collapse:collapse"></td>
                    </tr>
                    <!-- Spacing -->
                    <!-- Spacing -->
                    <tr>
                        <td width="100%" height="5" style="border-collapse:collapse"></td>
                    </tr>
                    <!-- Spacing -->
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
    <!-- End of preheader -->
</div>
<div class="block">
    <!-- start of header -->
    <table width="100%" bgcolor="#f6f4f5" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="header" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;margin:0;padding:0;width:100% !important;line-height:100% !important">
        <tbody>
        <tr>
            <td style="border-collapse:collapse">
                <table width="580" bgcolor="#F9A81A" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" hlitebg="edit" shadow="edit" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt">
                    <tbody>
                    <tr>
                        <td style="border-collapse:collapse">
                            <!-- logo -->
                            <table width="280" cellpadding="0" cellspacing="0" border="0" align="left" class="devicewidth" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt">
                                <tbody>
                                <tr>
                                    <td valign="middle" width="270" style="border-collapse:collapse;padding: 10px 0 10px 20px;" class="logo">
                                        <div class="imgpop">
                                            <a href="{{URL::to('/')}}"><img src="{{URL::to('home_assets/assets/images/truckka-orange.png')}}" alt="truckka.ng" border="0" style="width:150px;outline:none;text-decoration:none;border:none;-ms-interpolation-mode:bicubic;border:none;display:block; border:none; outline:none; text-decoration:none;" st-image="edit" class="logo"></a>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <!-- End of logo -->
                            <!-- menu -->
                            <table width="280" cellpadding="0" cellspacing="0" border="0" align="right" class="devicewidth" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt">
                                <tbody>
                                <tr>
                                    <td width="270" valign="middle" style="border-collapse:collapse;font-family: Helvetica, Arial, sans-serif;font-size: 14px; color: #ffffff;line-height: 24px; padding: 10px 0;padding-top:40px;" align="right" class="menu" st-content="menu">
                                        <a href="http://www.truckka.ng/" style="text-decoration: none; color: #ffffff;">Home</a>
                                        &nbsp;|&nbsp;
                                        <a href="http://www.truckka.ng/about" style="text-decoration: none; color: #ffffff;">About us</a>
                                        &nbsp;|&nbsp;
                                        <a href="http://www.truckka.ng/our-services" style="text-decoration: none; color: #ffffff;">Our Service</a>
                                    </td>
                                    <td width="20" style="border-collapse:collapse"></td>
                                </tr>
                                </tbody>
                            </table>
                            <!-- End of Menu -->
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
    <!-- end of header -->
</div>
<div class="block">
    <!-- image + text -->
    <table width="100%" bgcolor="#f6f4f5" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="bigimage" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;margin:0;padding:0;width:100% !important;line-height:100% !important">
        <tbody>
        <tr>
            <td style="border-collapse:collapse">
                <table bgcolor="#ffffff" width="580" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth" modulebg="edit" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt">
                    <tbody>
                    <tr>
                        <td width="100%" height="20" style="border-collapse:collapse"></td>
                    </tr>

                    <tr>
                        <td style="border-collapse:collapse">
                            <table width="540" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidthinner" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt">
                                <tbody>
                                <tr>
                                    <td><h3 style="margin:.4em 0;font-size:25px; text-transform:capitalize;">Hello {{$details['name']}},</h3></td>
                                </tr>

                                <tr>
                                    <td>
                                        <p style="font-family:Verdana, Arial, Helvetica, sans-serif;padding: 15px 0 0;">
                                            We are pleased to inform you that your order, {{$details["order_number"]}}, has been received and is being processed. You can view the details of your order at anytime by clicking the link below.
                                        </p>
                                        <p style="font-family:Verdana, Arial, Helvetica, sans-serif;padding: 15px 0 0;">
                                            <a href='{{url("/exchange/customer/home")}}'>View  Order Details</a>
                                        </p>
                                        <p style="font-family:Verdana, Arial, Helvetica, sans-serif;padding: 15px 0 0;">
                                            Regards,
                                            <br><em>-The Truckka Team</em>
                                        </p>
                                    </td>
                                </tr>





                                <!-- Spacing -->
                                <tr>
                                    <td width="100%" height="10" style="border-collapse:collapse"></td>
                                </tr>
                                <!-- button -->
                                <!-- /button -->
                                <!-- Spacing -->
                                <tr>
                                    <td width="100%" height="20" style="border-collapse:collapse;text-align:center;"><a href="http://www.truckka.ng/" style="text-decoration: none; color: #0db9ea;font-family:arial;">www.truckka.ng</a></td>
                                </tr>
                                <!-- Spacing -->
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
    <br /><br /><br />
</div>
</body>
</html>