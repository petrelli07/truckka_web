
    @include('client.includes.header')
    @include('client.includes.head')
    <!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-content-white page-md">
     <!--  <div class="modal fade bs-modal-sm alert-modal flex-container flex-center flex-vertical-align flex-responsive " id="cancel" tabindex="-1" role="dialog" aria-hidden="true" style="display:inherit;background:rgba(25, 24, 24, 0.48);opacity:1;"><div class="col44 loader-container"><img src="{{url('/assets/pages/img/loader.gif')}}"></div></div> -->

    </div>
        <div class="page-wrapper">
            <!-- BEGIN HEADER -->
                @include('client.includes.menuBar')

            <!-- END HEADER -->
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
            <!-- END HEADER & CONTENT DIVIDER -->
            <!-- BEGIN CONTAINER -->
            <div class="page-container">
                <!-- BEGIN SIDEBAR -->
                    @include('client.includes.sidebar')
                <!-- END SIDEBAR -->
                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <!-- BEGIN PAGE HEADER-->
                        <!-- BEGIN THEME PANEL -->

                        <!-- END THEME PANEL -->
                        <!-- BEGIN PAGE BAR -->
                        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    <a href="index.html">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Dashboard</span>
                                </li>
                            </ul>
                            <div class="page-toolbar">
                                <div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
                                    <i class="icon-calendar"></i>&nbsp;
                                    <span class="thin uppercase hidden-xs"></span>&nbsp;
                                    <i class="fa fa-angle-down"></i>
                                </div>
                            </div>
                        </div>
                        <!-- END PAGE BAR -->
                        <!-- BEGIN PAGE TITLE-->
                        <h1 class="page-title">
                        </h1>
                        <!-- END PAGE TITLE-->
                        <!-- END PAGE HEADER-->
<!--  -->                 <div class="row">
                            
                            <div class="col-lg-10 col-lg-offset-1 col-xs-12 col-sm-12" style="position: relative;">
                                <div class="sidebar-menu window-open">
                                  
                                 <div class="" style="position: relative;">
                                  <div class="toggle-sidemenu truckka-back" data-target="sidebar-menu " >
                                    
                                  </div>
                                  <div class="portlet-title tabbable-line c-tab-control">
                      
                                      <ul class="nav nav-tabs">
                                          <li class="active">
                                              <a href="#tab_1_1" data-toggle="tab"><span><i class="fas fa-user-circle"></i></span> Drivers </a>
                                          </li>
                                          <li>
                                              <a href="#tab_1_2" data-toggle="tab"> <i class="fas fa-filter"></i> Filter </a>
                                          </li>
                                      </ul>
                                  </div>
                                  
                                  <div class="portlet-body">
                                      <!--BEGIN TABS-->
                                       <div class="tab-content">
                                          <div class="tab-pane active" id="tab_1_1">
                                           
                                             <div class="drivers-tabs truckka-back" style="color: white;" >
                                               <p class="f-paragraph bold padding-0 margin-0" style="font-size: 13px;font-weight: bold;"></p> <b>Orders</b>
                                             </div>
                                             <div class="active-tile tiles-map ">
                                               
                                             </div>
                                             <div class="drivers-tabs" >
                                               <p class="f-paragraph bold padding-0 margin-0" style="font-size: 13px;font-weight: bold;"></p> 
                                             </div>
                                             <div class="offlline_tiles tiles-map">
                                               
                                             </div>
                                          </div>
                                          <div class="tab-pane" id="tab_1_2" style="padding:12px;">
                                              <p class="f-paragraph padding-0 margin-0" style="font-weight: bold;margin-bottom: 15px;">Filter By:</p>
                                              <div class="row" >

                                                <div class="col-md-12" style="margin-bottom: 10px;">

                                                   <input type="number" class="form-control form-filter input-sm flat" placeholder="order Id" name="orderNumber"> </td>
                                                </div>
                                                <div class="col-md-12" style="margin-bottom: 10px;">
                                                  <input type="text" class="form-control form-filter input-sm flat" placeholder="Plate Number" name="truckdetail">
                                                </div>
                                                <div class="col-md-12" style="margin-bottom: 10px;">
                                                   <select name="orderOrigin" class="form-control form-filter input-sm flat">
                                                                  <option value="">Order Origin</option>
                                                                  @foreach($locations as $location)
                                                                    <option value="{{$location}}">{{$location}}</option>
                                                                  @endforeach
                                                  </select>
                                                </div>
                                                <div class="col-md-12" style="margin-bottom: 10px;">
                                                   <select name="orderDest" class="form-control form-filter input-sm">
                                                                  <option value="">Order Destination</option>
                                                                  @foreach($locations as $location)
                                                                    <option value="{{$location}}">{{$location}}</option>
                                                                  @endforeach
                                                              </select>
                                                </div>
                                              </div>
                                              <div class="flex-container flex-center">
                                                  <button class="btn btn-primary flat">Filter</button>
                                                </div>
                                          </div>
                                      </div>
                                      <!--END TABS-->
                                  </div>

                                 </div> 
                                </div>
                                                                
                                <div class="portlet light ">
                                    <div class="portlet-title">
                                      <div class="flex-container flex-space-between" id="btnHolder">
                                        <div class="caption">
                                            <span class="caption-subject font-dark bold uppercase">MAP</span>
                                        </div>
                                        
                                      </div>

                                    </div>
                                     <div class="portlet-body form">
                                            <div id="map"></div>
                                
                                  </div>
                                </div>
                                            <!-- <div id="content">
                                              Hello world!
                                            </div> -->
                                 


                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                    <!-- END CONTENT BODY -->
                </div>
                <!-- END CONTENT -->
                <!-- BEGIN QUICK SIDEBAR -->

                <!-- END QUICK SIDEBAR -->
            </div>
            <!-- END CONTAINER -->
            <!-- BEGIN FOOTER -->
            <div class="page-footer">
                <div class="page-footer-inner"> 2018 &copy; Truckka  &nbsp;|&nbsp;

                </div>
                <div class="scroll-to-top">
                    <i class="icon-arrow-up"></i>
                </div>
            </div>
            <!-- END FOOTER -->
        </div>
        <!-- BEGIN QUICK NAV -->

        <div class="quick-nav-overlay"></div>


 @include('client.includes.footer')
