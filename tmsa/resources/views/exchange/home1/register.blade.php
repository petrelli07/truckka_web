@extends('exchange.home.layout.content')
@section('content')

<div class="container-fluid">
    <div class="row forget-bg" >
        <div class="forget-overlay"></div>
        <div class="container">
            <div class="row">
                
                <div class="forget-content ">
                    <div class="col-xs-12 forget-content-al">
                        <div class="forget-title col-xs-12 no-padding">
                            <h2>
                                Register
                            </h2>
                        </div>
                        <form method="post" action="{{ route('register') }}">
                            {{csrf_field()}}
                            <div class="col-xs-12 no-padding">
                                <label class="forget-label">Name</label>
                                <input type="text" required class="text-forget" id="txtForgotEmail" name="name" placeholder="Name">
                                <span class="field-validation-valid c-red" data-valmsg-for="UserName" id="forgotEmailError" >
                                    @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                                </span>
                            </div>

                            <div class="col-xs-12 no-padding">
                                <label class="forget-label">Email</label>
                                <input type="email" required class="text-forget" id="txtForgotEmail" name="email" placeholder="Email">
                                <span class="field-validation-valid c-red" data-valmsg-for="UserName" id="forgotEmailError" >
                                    @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                                </span>
                            </div>

                            <div class="col-xs-12 no-padding">
                                <label class="forget-label">Phone Number</label>
                                <input type="text" required class="text-forget" id="txtForgotEmail" name="phone" placeholder="Phone Number">
                                <span class="field-validation-valid c-red" data-valmsg-for="UserName" id="forgotEmailError" >
                                    @if ($errors->has('phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                                </span>
                            </div>

                            <div class="col-xs-12 no-padding">
                                <label class="forget-label">I am</label>
                                <select class="text-forget" name="userType" required>
                                    <option value=""  class="text-forget"> ? </option>
                                    <option value="5"  class="text-forget"> A Truck owner / Driver</option>
                                    <option value="6"  class="text-forget"> A Truck Supplier</option>
                                    <option value="4"  class="text-forget"> Looking for Trucks</option>
                                </select>
                                @if ($errors->has('userType'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('userType') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="col-xs-12 no-padding">
                                <label class="forget-label">Password</label>
                                <input type="password" class="text-forget" id="txtForgotEmail" name="password" placeholder="Password">
                                <span class="field-validation-valid c-red" data-valmsg-for="Password"  >
                                    @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                                </span>
                            </div>

                            <div class="col-xs-12 no-padding">
                                <label class="forget-label">Confirm Password</label>
                                <input type="password" class="text-forget" id="txtForgotEmail" name="password_confirmation" placeholder="Retype Password">
                                <span class="field-validation-valid c-red" data-valmsg-for="Password"  >
                                    @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                                </span>
                            </div>

                            <div class="col-xs-12">
                                <div class="row">
                                    <div class="forget-btn">
                                        <button type="submit" id="btnForgotSend" class="">Submit</button>
                                        <button type="submit" id="backBtn" class="payment-cancel ">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>



@endsection