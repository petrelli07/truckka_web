@extends('exchange.home.layout.content')
@section('content')


<input type="hidden" id="returnUrl" />
<div class="container-fluid">
    <div class="row banner">
        <div class="location-center-align">
            <div class="location-center">
                <div class="location-title width-100 text-center">
                    <h1 class="text-center font-22 text-center">Instant Truckload Quotes;  <span> Affordable, Reliable and Hassle-free</span></h1>
                </div>


                    <div class="container-fluid" id="truckka-options">
                                <div class="row about-al">
                                    <div class="container">
                                        <div class="row">
                                        <div class="Ineedtrucks-btn">
                                            <a href="#" class="i_need_truck"> I NEED TRUCK</a>
                                            <a href="#" class="fill-btn-trucks i_can_give_truck">I CAN GIVE TRUCK</a>
                                        </div>                
                                        </div>
                                    </div>
                                </div>
                        </div>

                <!--Hidden for origin-destination form-->
                <form id="originForm" style="display:none;">
                    {{csrf_field()}}
                    <input type="hidden" id="origin2" name="originToServer">
                </form>
                <!--end Hidden for origin-destination form-->

                    <!---- THIS IS FOR I NEED TRUCK ------>

                       <div class="location-search" id="i_need_truck">

                           <form id="getQuoteForm">
                               {{csrf_field()}}

                               <div class="location-content white white-lbl">
                                   <div class="col-xs-12 no-padding">

                                       <div class="col-sm-6 col-xs-12 no-padding">
                                           <div class="col-xs-12">
                                               <div class="col-xs-12 no-padding">
                                                   <div class="col-xs-12 no-padding">
                                                       <div class="booking-label-change mtp-lbl">
                                                           <label>Origin</label>
                                                       </div>
                                                   </div>
                                                   <div class="col-xs-12 no-padding">
                                                       <select class="text-booking-change" name="origin" required id="standardOrigin">
                                                           <option class="text-booking-change">[-Select Origin-]</option>
                                                           @for ($i=0; $i < count($uniqueOrigins); $i++)
                                                           <option class="text-booking-change" value="{{$uniqueOrigins[$i]}}">{{$uniqueOrigins[$i]}}</option>
                                                           @endfor
                                                       </select>
                                                   </div>
                                               </div>
                                           </div>
                                       </div>

                                       <input id="number_of_truck" value="2" type="hidden">

                                       <div class="col-sm-6 col-xs-12 no-padding">
                                           <div class="col-xs-12">
                                               <div class="col-xs-12 no-padding">
                                                   <div class="col-xs-12 no-padding">
                                                       <div class="booking-labe-changel">
                                                           <label>Destination</label>
                                                       </div>
                                                   </div>
                                                   <div class="col-xs-12 no-padding">
                                                       <select class="text-booking-change" required id="destinationSelect" name="destination">

                                                       </select>
                                                   </div>
                                               </div>
                                           </div>
                                       </div>
                                   </div>

                                   <!--listing 2 -->
                                   <div class="col-xs-12 no-padding">
                                       <div class="col-sm-6 col-xs-12 no-padding">
                                           <div class="col-xs-12">
                                               <div class="col-xs-12 no-padding">
                                                   <div class="col-xs-12 no-padding">
                                                       <div class="booking-labe-changel">
                                                           <label>Choose Truck</label>
                                                       </div>
                                                   </div>
                                                   <div class="col-xs-12 no-padding">
                                                       <select class="text-booking-change" name="truckType">
                                                           <option class="text-booking-change" required value="">[-Select Truck Type-]</option>
                                                           @foreach($truckTypes as $truckType)
                                                           <option class="text-booking-change" value="{{$truckType->id}}">{{$truckType->truck_type_description}}</option>
                                                           @endforeach
                                                       </select>
                                                   </div>
                                               </div>
                                           </div>
                                       </div>

                                       <div class="col-sm-6 col-xs-12 no-padding">
                                           <div class="col-xs-12">
                                               <div class="col-xs-12 no-padding">
                                                   <div class="col-xs-12 no-padding">
                                                       <div class="booking-labe-changel">
                                                           <label>Pick Up Date</label>
                                                       </div>
                                                   </div>
                                                   <div class="col-xs-12 no-padding">
                                                       <input class="text-booking-change1"  data-val="true" data-val-date="The field PickUpDateReady must be a date." id="datepicker" name="pickupdate" placeholder="MM/DD/YYYY"  tabindex="5">
                                                       <div id="datetime"></div>
                                                       <div class="date-ic"><img src="{{URL::to('exchange/home_assets/assets/images/date-ic-trackka.png')}}" alt="TQDate" class="img-responsive"></div>
                                                       <span class="field-validation-valid col-xs-12 no-padding c-red" data-valmsg-for="UserName" id="txtDeliveryDateError" data-valmsg-replace="true"></span>
                                                   </div>
                                               </div>
                                           </div>
                                       </div>
                                   </div>
                                   <!--end it-->

                                   <!--listing 2 -->
                                   <div class="col-xs-12 no-padding">
                                       <div class="col-sm-6 col-xs-12 no-padding">
                                           <div class="col-xs-12">
                                               <div class="col-xs-12 no-padding">
                                                   <div class="col-xs-12 no-padding">
                                                       <div class="booking-labe-changel">
                                                           <label>Contact Phone Number</label>
                                                       </div>
                                                   </div>
                                                   <div class="col-xs-12 no-padding">
                                                       <input class="text-booking-change1"  data-val="true" data-val-date="Enter Contact Phone Number" name="contact_phone" id="contact_phone" placeholder="Enter Contact Phone Number"  tabindex="5">

                                                       <span class="field-validation-valid col-xs-12 no-padding c-red" data-valmsg-for="UserName" id="txtDeliveryDateError" data-valmsg-replace="true"></span>
                                                   </div>
                                               </div>
                                           </div>
                                       </div>

                                       <div class="col-sm-6 col-xs-12 no-padding">
                                           <div class="col-xs-12">
                                               <div class="col-xs-12 no-padding">
                                                   <div class="col-xs-12 no-padding">
                                                       <div class="booking-labe-changel">
                                                           <label>Pickup Address</label>
                                                       </div>
                                                   </div>
                                                   <div class="col-xs-12 col-lg-12 no-padding">
                                                       <textarea id="pickup_address" class="text-booking-change-textarea"  name="pickup_address" placeholder="Enter Description"  type="text"></textarea>
                                                       <span class="field-validation-valid c-red" data-valmsg-for="origin"  data-valmsg-replace="true"></span>
                                                   </div>
                                               </div>
                                           </div>
                                       </div>
                                   </div>
                                   <!--end it-->


                                   <div class="col-xs-12 no-padding">
                                       <div class="col-sm-12 col-xs-12 no-padding">
                                           <div class="col-xs-12">
                                               <div class="col-xs-12 no-padding">
                                                   <div class="col-xs-12 no-padding">
                                                       <div class="booking-label-change mtp-lbl">
                                                           <label>Description of items</label>
                                                       </div>
                                                   </div>
                                                   <div class="col-xs-12 col-lg-12 no-padding">
                                                       <textarea id="description" class="text-booking-change-textarea"  name="description" placeholder="Enter Description"  type="text"></textarea>
                                                       <span class="field-validation-valid c-red" data-valmsg-for="origin"  data-valmsg-replace="true"></span>
                                                   </div>
                                               </div>
                                           </div>
                                       </div>
                                   </div>
                                   <div class="col-xs-12 no-padding">


                                       <div class="col-xs-12">
                                           <div class="quote-btn col-xs-12 no-padding margin-top-55">
                                               <a id="btnGetQuote" class="cursor-pointer getQuoteButton" tabindex="8">Get Quote</a>
                                               <a id="backtooptions3" class="cursor-pointer" tabindex="8">Back to Options</a>
                                           </div>


                                       </div>
                                   </div>
                               </div>

                           </form>


           
                        </div>
                    
 <!------------------------------------------------------------- END OF IT --------------------------------------------->


 <!---- -------------------------------------------THIS IS FOR I CAN GIVE TRUCKS ----------------------------------->

                       <div class="location-search" id="i_can_give_truck">
                           <form method="post" action="{{url('/exchange/register_supplier_capacity')}}">

                               {{csrf_field()}}

                            <div class="location-content white white-lbl">
                            <div class="col-xs-12 no-padding">

                            <div class="col-sm-6 col-xs-12 no-padding">
                                    <div class="col-xs-12">
                                        <div class="col-xs-12 no-padding">
                                            <div class="col-xs-12 no-padding">
                                                <div class="booking-label-change mtp-lbl">
                                                    <label>Type of Truck</label>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 no-padding">
                                                <select class="text-booking-change" name="truckType">
                                                    <option class="text-booking-change" required value="">[-Select Truck Type-]</option>
                                                    @foreach($truckTypes as $truckType)
                                                    <option class="text-booking-change" value="{{$truckType->id}}">{{$truckType->truck_type_description}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            <div class="col-sm-6 col-xs-12 no-padding">
                                    <div class="col-xs-12">
                                        <div class="col-xs-12 no-padding">
                                            <div class="col-xs-12 no-padding">
                                                <div class="booking-label-change mtp-lbl">
                                                    <label>Number of Trucks</label>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 no-padding">
                                                <input type="text" class="text-booking-change" name="number_of_truck"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xs-12 no-padding">
                                    <div class="col-xs-12">
                                        <div class="col-xs-12 no-padding">
                                            <div class="col-xs-12 no-padding">
                                                <div class="booking-labe-changel">
                                                    <label>Location Covered</label>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 no-padding">

                                               <ul class="report-radio-buttons tax-radio-btn">
                                                   @foreach($area as $areas)
                                                <li class="">
                                                    <input type="checkbox"  class="text-booking-change2"id="defaultInline1" value="{{$areas->id}}" name="coverageArea[]">
                                                    <span class="lbl-chng-span22">{{$areas->coverage_area_description}}</span>
                                                </li>
                                                   @endforeach
                                                </ul>

                                                    


                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--listing 2 -->

                            <!--end it-->

                            <div class="col-sm-6 col-xs-12 no-padding">
                                <div class="col-xs-12">
                                    <div class="col-xs-12 no-padding">
                                        <div class="col-xs-12 no-padding">
                                            <div class="booking-labe-changel">
                                                <label>Relationship with the truck</label>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 no-padding">
                                            <ul class="report-radio-buttons tax-radio-btn">
                                                <li class="">
                                                    <input id="radio-1" class="radio-custom equipments" name="supplierType" type="radio" value="1" checked  tabindex="3">
                                                    <label for="radio-1" class="radio-custom-label lbl-chng-txt-change"><span class="lbl-chng-span">Truck Owner</span></label>
                                                </li>
                                                <li class="m-l-30">
                                                    <input id="radio-2" class="radio-custom equipments" value="2" name="supplierType" type="radio" tabindex="4">
                                                    <label for="radio-2" class="radio-custom-label lbl-chng-txt-change"><span class="lbl-chng-span">Aggregator</span></label>
                                                </li>
                                            </ul>
                                           
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12 no-padding">
                                
                                
                                <div class="col-xs-12">
                                    <div class="quote-btn col-xs-12 no-padding margin-top-55">
                                        <button id="btnGetQuote" class="cursor-pointer btn btn-sm btn-success" type="submit" tabindex="8">Get Quote</button>
                                        <a id="backtooptions2" class="cursor-pointer" tabindex="8">Back to Options</a>
                                    </div>

                                    
                                </div>
                            </div>
                            </div>

                           </form>
                            </div>


<!-------------------------------------- END OF IT ---------------------------------------------------->




<!---- -------------------------------------------THIS IS FOR GET QUOTE SUMMERY ----------------------------------->

                       <div class="location-search" id="get_quote">

<div class="location-content white white-lbl">
        <div class="col-xs-12 no-padding">
            <h3 style="color:#fff;"><u>Quote Summery</u><h3>

                    <div class="row">
                        <div class="col-sm-6 col-xs-12 no-padding">
                            <span style="color:#fff;margin-right:4px;">Origin:</span>
                            <span style="color:#ccc;" id="originSummary"></span>
                        </div>

                        <div class="col-sm-6 col-xs-12 no-padding">
                            <span style="color:#fff;margin-right:4px;">Destination:</span>
                            <span style="color:#ccc;" id="destinationSummary"> </span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6 col-xs-12 no-padding">
                            <span style="color:#fff;margin-right:4px;">Truck Type:</span>
                            <span style="color:#ccc;" id="truckTypeSummary"> </span>
                        </div>

                        <div class="col-sm-6 col-xs-12 no-padding">
                            <span style="color:#fff;margin-right:4px;">Number of Trucks:</span>
                            <span style="color:#ccc;" id="numberOfTruckSummary"> </span>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-sm-4 col-xs-12 no-padding">
                            <span style="color:#fff;margin-right:4px;">Price:</span>
                            <span style="color:#ccc;" id="priceSummary"> </span>
                        </div>

                        <div class="col-sm-4 col-xs-12 no-padding">
                            <span style="color:#fff;margin-right:4px;">VAT:</span>
                            <span style="color:#ccc;" id="vatSummary"> </span>
                        </div>

                        <div class="col-sm-4 col-xs-12 no-padding">
                            <span style="color:#fff;margin-right:4px;">Total Amount:</span>
                            <span style="color:#ccc;" id="totalSummary"> </span>
                        </div>
                    </div>

                    <form action="{{url('/exchange/submitQuote')}}" method="POST">
                        {{csrf_field()}}
                        <input id="routeIDForm" type="hidden" name="routeID">
                        <input id="truckNumberForm" type="hidden" name="truckNumberForm">
                        <input id="truckTypeIDForm" type="hidden" name="truckTypeIDForm">
                        <input id="itemDescriptionForm" type="hidden" name="itemDescriptionForm">
                        <input id="pickUpDateForm" type="hidden" name="pickUpDateForm">
                        <input id="totalAmtForm" type="hidden" name="totalAmtForm">
                        <input id="pickupAddressForm" type="hidden" name="pickupAddressForm">
                        <input id="contactPhoneForm" type="hidden" name="contactPhoneForm">

                        <div class="col-xs-12 no-padding">


                            <div class="col-xs-12">
                                <div class="quote-btn col-xs-12 no-padding margin-top-55">
                                    @if (Route::has('login'))
                                    @auth
                                    <button type="submit" class="btn-sm btn-success" tabindex="8" value="loggedInUser" name="submitQuoteButton">Continue</button>
                                    @else
                                    <button type="submit" class="btn-sm btn-success" tabindex="8" value="register" name="submitQuoteButton">Register to Continue</button>
                                    @endauth
                                    @endif
                                </div>


                            </div>
                        </div>

                    </form>



        </div>

</div>

                       </div>
<!-------------------------------------- END OF IT ---------------------------------------------------->











        </div>
    </div>
</div>

<!--DEMAND AND SUPPIED -->
<div class="container-fluid" >
    <div class="row about-al" >
        <div class="container" id="container-demand" >
            <div class="row">
                    <div class="">
                            <div class="col-sm-6 col-xs-12 no-padding">
                                <div class="col-xs-12">
                                    <div class="col-xs-12 no-padding">
                                        <div class="col-xs-12 no-padding">
                                            <div class="booking-label-change mtp-lbl">
                                                <label>DEMAND</label>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 no-padding">
                                        <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
                                                <thead>

                                                <tr>
                                                    
                                                    <th>Order Number</th>
                                                    <th>Origin</th>
                                                    <th>Destination</th>
                                                    <th>Pick-up date</th>
                                                    
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    
                                                    <td class="center">1</td>
                                                    <td class="center">Lagos</td>
                                                    <td class="center">Abuja</td>
                                                    <td class="center">21/11/2018</td>
                                                    
                                                </tr>
                                                </tbody>
                                                </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12 no-padding">
                                <div class="col-xs-12">
                                    <div class="col-xs-12 no-padding">
                                        <div class="col-xs-12 no-padding">
                                            <div class="booking-labe-changel">
                                                <label>SUPPIED</label>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 no-padding">
                                        <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
                                                <thead>

                                                <tr>
                                                    
                                                    <th>Order Number</th>
                                                    <th>Origin</th>
                                                    <th>Destination</th>
                                                    <th>Pick-up date</th>
                                                    
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    
                                                    <td class="center">1</td>
                                                    <td class="center">Lagos</td>
                                                    <td class="center">Abuja</td>
                                                    <td class="center">21/11/2018</td>
                                                    
                                                </tr>
                                                </tbody>
                                                </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
            </div>
        </div>
    </div>
</div>
<!-- END OF IT-->


<!--About-->
<div class="container-fluid" id="about">
    <div class="row about-al">
        <div class="container">
            <div class="row">
                <div class="about-title">
                    <h2>
                        ABOUT TRUCKKA
                    </h2>
                    <p class="about-p">
                        Truckka.ng is a based company that was founded by a group of individuals with decades of experience in the trucking industry.
                        Truckka.ng is a simple to use, full truck or LTL load quoting service that provides instant quotes, locates drivers and allows the user to
                        effortlessly book a load to be shipped. TQ’s tool is for everyone and was purposely designed to speed up the time it takes to get a quote,
                        easily find reliable trucking at a competitive price and allow the user to track the shipment along the route. The best part, no phone calls required!
                    </p>
                </div>
                <div class="about-btn">
                    <a href="#benefits">BENEFITS</a>
                    <a href="#howitworks" class="fill-btn">HOW IT WORKS</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!--About-->


<!--How it works-->
<div class="container-fluid" id="howitworks">
    <div class="row about-al p-relative">
        <div class="benifits-overlay"></div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ben-pad">
            <div class="benefits-title col-xs-12 no-padding">
                <h2>
                    how it works
                </h2>
            </div>
            <div class="benefits-info col-xs-12 no-padding">
                <div class="col-sm-2 col-xs-12 pad-left">
                    <div class="ben-im"><img src="{{URL::to('exchange/home_assets/assets/images/icon_incredibly_fast.png')}}" alt="Instant Quotes"></div>
                </div>
                <div class="col-sm-10 col-xs-12 pad-left">
                    <div class="benefits-info1">
                        <h3>Instant Quotes</h3>
                        <p>
                            Simply enter your pickup and delivery locations and instantly receive a free quote.
                        </p>
                    </div>
                </div>
            </div>
            <div class="benefits-info col-xs-12 no-padding">
                <div class="col-sm-2 col-xs-12 pad-left">
                    <div class="ben-im"><img src="{{URL::to('exchange/home_assets/assets/images/Booking.png')}}" alt="Quick Booking"></div>
                </div>
                <div class="col-sm-10 col-xs-12 pad-left">
                    <div class="benefits-info1">
                        <h3>Quick Booking</h3>
                        <p>
                            Input details of shipment and payment to easily confirm booking.
                        </p>
                    </div>
                </div>
            </div>
            <div class="benefits-info col-xs-12 no-padding">
                <div class="col-sm-2 col-xs-12 pad-left">
                    <div class="ben-im"><img src="{{URL::to('exchange/home_assets/assets/images/24-7.png')}}" alt="Superior Tracking"></div>
                </div>
                <div class="col-sm-10 col-xs-12 pad-left">
                    <div class="benefits-info1">
                        <h3>Superior Tracking </h3>
                        <p>
                            Track your shipment by logging into your account and viewing all order updates.
                        </p>
                    </div>
                </div>
            </div>
            <div class="benefits-info col-xs-12 no-padding">
                <div class="col-sm-2 col-xs-12 pad-left">
                    <div class="ben-im"><img src="{{URL::to('exchange/home_assets/assets/images/delivery-truck-with-circular-clock-1-512x400.png')}}" alt="Shippment Delivery"></div>
                </div>
                <div class="col-sm-10 col-xs-12 pad-left">
                    <div class="benefits-info1">
                        <h3>Shipment Delivery Satisfaction guaranteed</h3>
                        <p>
                            Receive signed Bill of Lading once your shipment has delivered.
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding">
            <div class="benifits-bg-im"></div>
        </div>
    </div>
</div>
<!--How it works-->


<!--Benefits-->
<div class="container-fluid" id="benefits">
    <div class="row about-al">
        <div class="container">
            <div class="row">
                <div class="work-title">
                    <h2>
                        BENEFITS
                    </h2>
                </div>
                <div class="col-xs-12 no-padding work-block-al">
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="col-xs-12 work-block">
                          
                            <div class="work-im">
                                <img src="{{URL::to('exchange/home_assets/assets/images/free_quote_hover.png')}}" alt="Free Quote">
                            </div>
                            <div class="work-im1">
                                <img src="{{URL::to('exchange/home_assets/assets/images/free_quote_hover.png')}}" alt="Free Quote">
                            </div>
                            <div class="work-info">
                                <h3>
                                    Free Quotes
                                </h3>
                                <p>
                                    Get instant pricing that we stand behind.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="col-xs-12 work-block">
                            <div class="work-im">
                                <img src="{{URL::to('exchange/home_assets/assets/images/No_hidden_fee_hover.png')}}" alt="No Hiden fee">
                            </div>
                            <div class="work-im1">
                                <img src="{{URL::to('exchange/home_assets/assets/images/No_hidden_fee_hover.png')}}" alt="No Hiden fee">
                            </div>
                            <div class="work-info">
                                <h3>
                                    No Hidden Fees
                                </h3>
                                <p>
                                    Free to setup your personal account. No hidden annual membership or booking fees. The only thing you pay for is the cost of your booked order.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-9 col-md-6 col-lg-6">
                        <div class="work-block-horizontal">
                        <div class="work-imm">
                                <img src="{{URL::to('exchange/home_assets/assets/images/easy_to_use_hover.png')}}" alt="No Hiden fee">
                                <span class="span-horizon">
                                        Easy to Use</span>
                                        <p class="p-horizon">
                                        Stress-free setup and booking. Simply create and log in to your account and easily get instant quotes, set up loads, track existing orders or browse all of your order history.
                                    </p>
                            </div>
                               
                        </div>
                    </div>

                     <div class="col-sm-9 col-md-6 col-lg-6">
                     <div class="work-block-horizontal">
                            <div class="work-imm">
                                <img src="{{URL::to('exchange/home_assets/assets/images/customer_support_hover.png')}}" alt="No Hiden fee">
                               
                                    <span class="span-horizon">Excellent Customer Support</span>
                               
                                <p class="p-horizon">
                                    With customer satisfaction being our main priority we make it easy for you to connect with us. You can call and speak to a live representative in the U.S.A, live chat on our website or send us an email. Your choice!
                                </p>
                            </div>
                               
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>
<!--Benefits-->


@endsection