
<!DOCTYPE html>
<html>

<meta http-equiv="content-type" content="text/html;charset=utf-8" />

<head>
    <title>Truckka</title>
    <meta charset="utf-8" />
    <meta name="keywords" content="" />

    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />
    <meta name="format-detection" content="telephone=no">
    <link rel="canonical" href="index.html" />

    <link rel="shortcut icon" type="image/x-icon" href="">
    <link href='http://fonts.googleapis.com/css?family=Nunito' rel='stylesheet'>
    <link rel="stylesheet" href="{{URL::to('home_assets/assets/css-font/v5.1.0/css/all.css')}}" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    <link href="{{URL::to('home_assets/assets/css/bootstrap.min.css')}}" rel="stylesheet" />
    <link href="{{URL::to('home_assets/assets/css/datepicker.css')}}" rel="stylesheet" />

    <link href="{{URL::to('home_assets/assets/css/media.css')}}" rel="stylesheet" />
    <!--THIS IS FOR TABLETS SIZES -->
    <link href="{{URL::to('home_assets/assets/css/ipad.css')}}" rel="stylesheet" />
    <link href="{{URL::to('home_assets/assets/css/galaxy-tab.css')}}" rel="stylesheet" />
    <!-- END OF TABLETS SIZES -->
    <link href="{{URL::to('home_assets/assets/css/bootstrap-multiselect.css')}}" rel="stylesheet" />
    <link rel="stylesheet" href="{{URL::to('home_assets/assets/ajax/libs/toastr.js/latest/css/toastr.min.css')}}" />
    <link href="{{URL::to('home_assets/assets/css/style.css')}}" rel="stylesheet" />
    <script src="{{URL::to('dashboard_assets/assets/js/jquery.min.js')}}"></script>

    <script>
        $( function() {
            $( "#datepicker" ).datepicker();
        } );
    </script>


    <link rel="stylesheet" href="{{URL::to('home_assets/assets/nav/css/normalize.min.css')}}">
    <link rel="stylesheet" href="{{URL::to('home_assets/assets/nav/css/defaults.min.css')}}">
    <link rel="stylesheet" href="{{URL::to('home_assets/assets/nav/css/nav-core.min.css')}}">
    <link rel="stylesheet" href="{{URL::to('home_assets/assets/nav/css/nav-layout.min.css')}}">
    <script src="{{URL::to('home_assets/assets/nav/js/rem.min.js')}}"></script>
    <script src="{{URL::to('dashboard_assets/assets/js/jquery.min.js')}}"></script>




</head>
<body>


<!-- THIS IS THE MOBILE VERSION -->
<div class="mobile-version-mv">
    <header>
        <div class="logo">
            <a href="{{url('/')}}"><img src="{{URL::to('dashboard_assets/assets/images/logo.png')}}" alt="Truckka"></a>

        </div>
    </header>

    <a href="#" class="nav-button"></a>

    <nav class="nav">
        <ul>
            <li><a href="{{url('nWBbsRfgFm5JKb5FNSyshBIERxakU2/home')}}">Home</a></li>
            <li><a href="{{url('nWBbsRfgFm5JKb5FNSyshBIERxakU2/home')}}">The Exchange</a></li>
            <li ><a href="{{url('nWBbsRfgFm5JKb5FNSyshBIERxakU2/about-the-exchange')}}"> About The Exchange</a></li>

            <li ><a href="{{url('nWBbsRfgFm5JKb5FNSyshBIERxakU2/our-services')}}">Our Services</a>
            <li><a href="{{url('nWBbsRfgFm5JKb5FNSyshBIERxakU2/our-customer-segment')}}">Our Customer Segment</a></li>
            <li ><a href="{{url('nWBbsRfgFm5JKb5FNSyshBIERxakU2/about-us')}}">About us</a>
            <li><a href="{{url('nWBbsRfgFm5JKb5FNSyshBIERxakU2/contact-us')}}">Contact Us</a></li>

            <li><p style="margin-left:10px;"><span><img class="phone-icon-right" src="{{URL::to('home_assets/assets/images/phone-icon-11-256.png')}}" alt="Phone"></span>070 300 533 09</p></li>
            <li><a href="{{url('nWBbsRfgFm5JKb5FNSyshBIERxakU2/login')}}" id="IndexLogin" class="right-info-link1 loginClr">LOGIN</a></li>
            <li><a href="{{url('nWBbsRfgFm5JKb5FNSyshBIERxakU2/register')}}"  id="signup" class="right-info-link1 loginClr">SIGN UP</a></li>

        </ul>
    </nav>

    <a href="#" class="nav-close">Close Menu</a>

</div>

<!-- END OF THE MOBILE VERSION -->


<div ng-app="truckQuote" >
    <header class="desktop-vs">
        <div class="container-fluid banner-header">
            <div class="row">

                <div class="col-sm-3 col-xs-12 logo-section">

                    <div class="logo">

                        <a href="{{url('nWBbsRfgFm5JKb5FNSyshBIERxakU2/home')}}"><img src="{{URL::to('dashboard_assets/assets/images/logo.png')}}" alt="Truckka"></a>
                    </div>
                </div>
                <div class="col-sm-9 col-xs-12">
                    <div class="right-info-section col-xs-12 no-padding">
                        <div class="right-info">
                            <nav class="nav">
                                <ul>
                                    <li><a href="{{url('nWBbsRfgFm5JKb5FNSyshBIERxakU2/home')}}">Home</a></li>
                                    <li><a href="{{url('nWBbsRfgFm5JKb5FNSyshBIERxakU2/home')}}">The Exchange</a>

                                    </li>


                                    <li class="nav-submenu"><a href="{{url('nWBbsRfgFm5JKb5FNSyshBIERxakU2/our-services')}}">Our Services</a>
                                        <ul>
                                            <li><a href="{{url('nWBbsRfgFm5JKb5FNSyshBIERxakU2/our-customer-segment')}}">Our Customer Segment</a></li>


                                        </ul>
                                    </li>
                                    <li class="nav-submenu"><a href="{{url('nWBbsRfgFm5JKb5FNSyshBIERxakU2/about-us')}}">About us</a>
                                        <ul>
                                            <li><a href="{{url('nWBbsRfgFm5JKb5FNSyshBIERxakU2/contact-us')}}">Contact Us</a></li>


                                        </ul>
                                    </li>
                                    <li><p><span><img class="phone-icon-right" src="{{URL::to('home_assets/assets/images/phone-icon-11-256.png')}}" alt="Phone"></span>+234 8142 330 001</p></li>
                                    <li><a href="{{url('nWBbsRfgFm5JKb5FNSyshBIERxakU2/login')}}" id="IndexLogin" class="right-info-link1 loginClr">LOGIN</a></li>
                                    <li><a href="{{url('nWBbsRfgFm5JKb5FNSyshBIERxakU2/register')}}"  id="signup" class="right-info-link2 signUpClr">SIGN UP</a></li>

                                </ul>
                            </nav>





                        </div>
                    </div>


                </div>



            </div>

        </div>
    </header>


    <script>
        $(document).ready(function() {


            $('#i_can_give_truck').hide();
            $('#i_need_truck').show();
            $('#get_quote').hide();

            $('.i_need_truck').click(function() {
                $('#i_need_truck').show();
                $('#i_can_give_truck').hide();
                $('#get_quote').hide();

            });

            $('.i_can_give_truck').click(function() {
                $('#i_can_give_truck').show();
                $('#i_need_truck').hide();
                $('#get_quote').hide();
            });


                            var origin                  = $('#standardOrigin').find(":selected").text();
                            var destination             = $('#destinationSelect').find(":selected").text();
                            var truck_number            = $('#number_of_truck').val();
                            var description             = $('#description').val();
                            var pickupDate              = $('#datepicker').val();
                            var contactPhone            = $('#contact_phone').val();
                            var pickupAddress           = $('#pickup_address').val();




                            $("#routeIDForm").val(data.success.route_id);
                            $("#truckTypeIDForm").val(data.success.truckTypeID);
                            $("#truckNumberForm").val(truck_number);
                            $("#itemDescriptionForm").val(description);
                            $("#pickUpDateForm").val(pickupDate);
                            $("#totalAmtForm").val(data.success.total);
                            $("#pickupAddressForm").val(pickupAddress);
                            $("#contactPhoneForm").val(contactPhone);


            $('.btnGetQuotenew2').click(function() {
                $('#i_need_truck').hide();
                $('#i_can_give_truck').hide();
                $('#get_quote').show();
            });





        });
    </script>


    <div id="cover-spin"></div>

    <style>
        #cover-spin {
            position:fixed;
            width:100%;
            left:0;right:0;top:0;bottom:0;
            background-color: rgba(255,255,255,0.7);
            z-index:9999;
            display:none;
        }

        @-webkit-keyframes spin {
            from {-webkit-transform:rotate(0deg);}
            to {-webkit-transform:rotate(360deg);}
        }

        @keyframes spin {
            from {transform:rotate(0deg);}
            to {transform:rotate(360deg);}
        }

        #cover-spin::after {
            content:'';
            display:block;
            position:absolute;
            left:48%;top:40%;
            width:40px;height:40px;
            border-style:solid;
            border-color:black;
            border-top-color:transparent;
            border-width: 4px;
            border-radius:50%;
            -webkit-animation: spin .8s linear infinite;
            animation: spin .8s linear infinite;
        }

    </style>


    @yield('content')

    @extends('home.layout.footer')
