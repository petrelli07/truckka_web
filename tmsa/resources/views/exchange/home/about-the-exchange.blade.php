
@extends('home.layout.content')
@section('content')


<!-- how the exchange works -->
<div class="container-fluid" id="about">
    <div class="row about-al">
        <div class="container">
            <div class="row">
                <div class="about-title">
                    <h2>
                    HOW THE EXCHANGE WORKS
                    </h2>
                    
                    <img   src="{{URL::to('home_assets/assets/images/how-the-exchange-work-new.png')}}" style="width:100%" > 
                   
                </div>
                <div class="about-btn">
                   
                </div>
            </div>
        </div>
    </div>
</div>
 <!-- end of how the exchange works -->


   


            <!--Benefits of the exchange-->
        <div class="container-fluid benifits-of-exchange" id="benefits">
            <div class="row about-al" >
                <div class="container">
                    <div class="row">
                        <div class="work-title">
                            <h2>
                            BENEFITS OF THE EXCHANGE
                            </h2>
                        </div>
                        <div class="col-xs-12 no-padding work-block-al">
                            
                            

                            <div class="col-sm-9 col-md-6 col-lg-4">
                                <div class="work-block-horizontal2">
                                <div class="work-imm">
                                    <div class="image-freight">

                                        <img src="{{URL::to('home_assets/assets/images/map-with-truck-ful.png')}}"  >           
                                    
                                    </div>
                                    <div class ="content-freight">
                                        <p class="p-horizon">
                                        Peace of knowing in real-time the location of your truck/freight</p>
                                </div>
                                </div>
                            </div>
                            </div>

                            <div class="col-sm-9 col-md-6 col-lg-4">
                                <div class="work-block-horizontal2">
                                <div class="work-imm">
                                    <div class="image-freight">
                                        <img src="{{URL::to('home_assets/assets/images/pig-white.png')}}" alt="No Hiden fee"> 
                                    </div>          
                                    <div class ="content-freight">
                                                <p class="p-horizon">
                                                Save funds through transparent/price efficiency.
                                            </p>
                                    </div>
                                    </div>
                                    
                                </div>
                            </div>

                            <div class="col-sm-9 col-md-6 col-lg-4">
                                <div class="work-block-horizontal">
                                <div class="work-imm">
                                    <div class="image-freight">
                                        <img src="{{URL::to('home_assets/assets/images/worrying-less-white.png')}}">     
                                    </div>      
                                    <div class ="content-freight">
                                                <p class="p-horizon">
                                                Worry less on finding freight/truck
                                            </p>
                                    </div>
                                    </div>
                                    
                                </div>
                            </div>

                            <div class="col-sm-9 col-md-6 col-lg-6">
                                <div class="work-block-horizontal2">
                                <div class="work-imm">
                                    <div class="image-freight">
                                        <img src="{{URL::to('home_assets/assets/images/lap-phone-white.png')}}"> 
                                    </div>          
                                    <div class ="content-freight">
                                                <p class="p-horizon">
                                                Easy to use web / mobile technology 
                                            </p>
                                    </div>
                                    </div>
                                    
                                </div>
                            </div>

                            <div class="col-sm-9 col-md-6 col-lg-6">
                            <div class="work-block-horizontal">
                                    <div class="work-imm">
                                    <div class="image-freight">
                                        <img src="{{URL::to('home_assets/assets/images/customer_support_hover.png')}}" alt="No Hiden fee">
                                    </div>
                                    <div class ="content-freight">
                                        <p class="p-horizon">
                                        Helpful / Courteous Customer Support
                                        </p>
                                    </div>
                                    </div>
                                    
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
<!-- end of Benefits of exchange-->


@endsection