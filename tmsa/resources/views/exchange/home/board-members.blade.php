<!--Board members-->
<div class="container-fluid margin-for-board-mem" id="board-members">
<div class="container">
            <div class="row">
                <div class="work-title">
                    <h2>
                    BOARD OF DIRECTORS
                    </h2>
                </div>
               
               <div class="section group">
					<div class="col span_1_of_4 col-determinate">
                        <div class="board-images2">
                        <img src="{{URL::to('home_assets/assets/images/abiodun_passport photo_2.jpg')}}">
                        </div>
                            <div class="board-content">
                                <div class="section-board-c">
                                    <h3>GMD/CEO</h3>
                                    <h4>Abiodun Adesanya</h4>
                                </div>
                                <p><b>Truckka Logistics Ltd </b> -Founder/CEO</p>
                                <p><b>Southern Streams Universal Services Ltd </b> - MD /CEO</p>
                                <p><b>FCMB</b> - Senior Manager</p>
                                <p><b>Sterling Bank Plc </b>- Manager</p>

                                <p><b>Magnum T. Bank Plc (Defunct)</b></p>
                                <p>- Deputy Manager</p>
                               
                                    <div class="social-media-board">
                                    <a href="https://www.linkedin.com/in/abiodun-adesanya-2922832/" >

                                    <img src="{{URL::to('home_assets/assets/images/linkedin-icon.png')}}" width="30px"> @Abiodun Adesanya

                                    </a>

                            </div>
                              
                        </div>
                    </div>
                    
					<div class="col span_1_of_4 col-determinate">
                        <div class="board-images2">
                        <img src="{{URL::to('home_assets/assets/images/OMG-profile-picture.png')}}">
                        </div>
                            <div class="board-content">
                                <div class="section-board-c">
                                    <h3>DIRECTOR</h3>
                                    <h4>Gbolahan, Mark-George</h4>
                                </div>
                                    <p> <b>Mark-George Consultants </b>-Partner</p>
                                    <p><b>Enclude Holding</b>- Senior Consultant,  </p>
                                    <p><b>Nigeria Infrastructure Advisory Facility</b> -Finance Sector, Specialist</p>

                                    <p><b>MicroCred, MFB Nig.</b> - CEO, </p>
                                    <p> <b>GroFin Nig</b> - Country Manager</p>
                                    <br />
                                

                                    <div class="social-media-board">
                                    <a href="https://www.linkedin.com/in/olugbolahan-mark-george-83396a29/?originalSubdomain=ng" >

                                    <img src="{{URL::to('home_assets/assets/images/linkedin-icon.png')}}" width="30px"> @Gbolahan, Mark-George

                                    </a>

                            </div>
                              
                        </div>
                    </div>
                    
                    
					<div class="col span_1_of_4 col-determinate">
                        <div class="board-images2">
                        <img src="{{URL::to('home_assets/assets/images/enitan-profile-picture.jpg')}}">
                        </div>
                            <div class="board-content">
                                <div class="section-board-c">
                                    <h3>DIRECTOR</h3>
                                    <h4>Enitan Adesanya</h4>
                                </div>
                                <p><b>Easterseals, Bay Area, Oakland, California, USA</b>
                                - Chief Administration & Finance Officer.</p>
                               
                                <p><b>KAISER PERMANENTE (KP), Oakland, CA, USA</b></p>
                                <p>- V.P, Enterprise Shared Services (ESS) (2016-Present)</p>
                                <p>- V.P, National Facilities Services </p>
                                <BR />
                                
                                <div class="social-media-board">
                                    <a href="https://www.linkedin.com/in/enitanadesanya/" >

                                    <img src="{{URL::to('home_assets/assets/images/linkedin-icon.png')}}" width="30px"> @Enitan Adesanya

                                    </a>

                            </div>
                              
                        </div>
                    </div>


					<div class="col span_1_of_4 col-determinate">
                        <div class="board-images2">

                        <img src="{{URL::to('home_assets/assets/images/anino-akperi-profile-picture.png')}}">

                        </div>
                            <div class="board-content">
                                <div class="section-board-c">
                                    <h3>DIRECTOR</h3>
                                    <h4>Anino Akperi</h4>
                                </div>
                                <p><b>Director Seal Towers Ltd </b>- MD / CEO</p>
                                <p><b>FCMB Plc </b> - V.P & Head,  Risk Management</p>
                                <p> <b> FINBANK Plc(Defunct)</b></p>
                                <p> - Chief Risk Officer</p>
                                <p> - Head, Risk Management , Commercial Banking</p>

                                <p><b>UBA  Ltd </b> - Head, Consumer Credits</p>
                                
                                    
                                    <div class="social-media-board">
                                    <a href="https://www.linkedin.com/in/anino-akperi-4b723215/" >

                                    <img src="{{URL::to('home_assets/assets/images/linkedin-icon.png')}}" width="30px"> @Anino Akperi

                                    </a>

                            </div>
                              
                        </div>
                    </div>
				</div>
            </div>
            </div>            
      
</div>
<!--End of Board members -->