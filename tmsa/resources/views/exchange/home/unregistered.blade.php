@extends('home.layout.content')
@section('content')

<div class="container-fluid">
    <div class="row forget-bg" >
        <div class="forget-overlay"></div>
        <div class="container">
            <div class="row">
                <div class="forget-content ">
                    <div class="col-xs-12 forget-content-al">
                        <div class="forget-title col-xs-12 no-padding">
                            <div class="row">
                                <div class="col-md-offset-1 text-center">
                                    <h4><p style="color: white"></p>
                                        <p  class="justify-center" style="color: white">Login to view quote or register if you don't have an account</p>
                                    </h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12" style="margin:20px;">
                            <a class="btn btn-md btn-block" href="{{url('/login')}}" style="background-color: #F9A81A; color: #000000;"><b>Login</b></a>
                        </div>

                        <div class="col-md-12 col-xs-12" style="margin:20px;">
                            <a class="btn btn-md btn-block" href="{{url('/exchange/customer/sign_up_customer_view')}}" style="background-color: #F9A81A; color: #000000;"><b>Register</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>



@endsection