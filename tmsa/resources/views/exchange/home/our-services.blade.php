@extends('home.layout.content')
@section('content')


<div style="margin-top:50px;">
</div>
<!--Our Services-->

<div class="container-fluid margin-for-deter-service" id="our-services-mv">
<div class="container">
            <div class="row">
    
                <div class="work-title">
                    <h2>
                        OUR SERVICES
                    </h2>
                </div>

				<div class="section group">
					<div class="col span_1_of_3 col-determinate-service">
					<div class="board-images">
                        <img src="{{URL::to('home_assets/assets/images/contract logistics.jpg')}}">
                    </div>
                    <div class="content-d-service">
                        <h3>CONTRACT LOGISTICS</h3>
                        <p>We provide Full Truck Load (FTL) on an outsourced contractual basis </p>
                    </div>
                    </div>

                    
					<div class="col span_1_of_3 col-determinate-service">
					<div class="board-images">
                        <img src="{{URL::to('home_assets/assets/images/roadrunner-truck-angle_0.jpg')}}">
                    </div>
                    <div class="content-d-service">
                        <h3>Freight/Transportation Exchange</h3>
                        <p>Our digital mobile & web enabled freight-truck matching platform </p>
                    </div>
                    </div>
                    

					<div class="col span_1_of_3 col-determinate-service">
					<div class="board-images">
                        <img src="{{URL::to('home_assets/assets/images/tms icon.jpg')}}">
                    </div>

                     <div class="content-d-service">
                        <h3>Transportation Management System (TMS)</h3>
                        <p>bespoke TMS delivers visibility, control and seamless communication across your enterprise </p>
                    </div>

					</div>
				</div>
              
                    
</div>
</div>
</div> 
<!--end services-->


@endsection

