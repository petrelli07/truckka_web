@extends('registry.fieldAgent.layout.content')
@section('content')



<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-eye-open"></i> Request Status</h2>
            </div>
            <div class="box-content">
                <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
                    <thead>

                    <tr>

                        <th>Owner Name</th>
                        <th>Owner Phone Number</th>
                        <th>Owner Address</th>
                        <th>Number of Truck</th>
                        <th>View Truck</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($truck_owners_details as $owner)
                    <tr>
                        <td class="center">{{$owner->truck_owner_name}}</td>
                        <td class="center">{{$owner->truck_owner_phone}}</td>
                        <td class="center">{{$owner->truck_owner_address}}</td>
                        <td class="center">{{$owner->total}}</td>
                        <td class="center">
                            <a href='{{url("registry/fieldOps/view-truck-by-owners/{$owner->id}")}}' class="btn btn-success btn-sm view-shop" type="submit">
                                <i class="glyphicon glyphicon-eye-open icon-white"></i> View Truck
                            </a>
                        </td>

                        <td class="center">
                            <a href='{{url("registry/fieldOps/add-truck/{$owner->id}")}}' class="btn btn-success btn-sm view-shop" type="submit">
                                <i class="glyphicon glyphicon-plus icon-white"></i> Add Trucks
                            </a>
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td class="center">No Requests Yet</td>
                        <td class="center">No Requests Yet</td>
                        <td class="center">No Requests Yet</td>
                        <td class="center">No Requests Yet</td>
                        <td class="center">No Requests Yet</td>
                    </tr>
                    @endforelse

                    <!---->

                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!--/span-->

</div>



@endsection