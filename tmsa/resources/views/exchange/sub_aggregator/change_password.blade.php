@extends('registry.fieldAgent.layout.content')
@section('content')


<div class="row">
    <div class="box col-md-12">
        <!--if @ session -->
            @if (session('message'))
                <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    {{ session('message') }}
                </div
            @endif
        <!--endif-->

        <div class="box-inner">
            <!--<div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-plus"></i> Add New Truck</h2>

            </div>-->
            <div class="box-content row">
                 <form role="form" method="post" class="validate"  action="{{url('registry/password_reset')}}"  enctype="multipart/form-data">
                    {{csrf_field()}}

                 <div class="box col-md-12">
                    <div class="form-group">
                        <label class="control-label" for="otp">Enter New Password</label>
                        <input type="password" style="width: 25%" name="password" class="form-control required" placeholder="Enter New Password">
                        @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                 <div class="box col-md-12">
                    <div class="form-group">
                        <label class="control-label" for="otp">Confirm New Password</label>
                        <input type="password" style="width: 25%" name="password_confirmation" class="form-control required" placeholder="Confirm New Password">
                        @if ($errors->has('password_confirmation'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <br /><br />
            
                <div class="box col-md-6">
                    <div class="form-group">
                        <button type="submit" class="btn btn-custom">
                            <i class="glyphicon glyphicon-plus"></i>Submit
                        </button>
                    </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
    <!--/span-->

</div><!--/row-->




@endsection