@extends('client.layout.content')
@section('content')

<div class="row">
    <div class="box col-md-12">
        <!--if @ session -->
            <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                   success
            </div>
        <!--endif-->

        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-plus"></i> Create New Orders</h2>

            </div>
            <div class="box-content row">
                 <form role="form" method="post" class="validate"  action="#"  enctype="multipart/form-data">
                 <div class="box col-md-6">
                    <div class="form-group">
                        <label class="control-label" for="origin">Origin</label>
                        <div class="controls">
                            <select id="selectError" class="col-md-12" data-rel="chosen" name="origin">
                            <option value='0'>?</option>
                            <option value=''>lagos</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="box col-md-6">
                    <div class="form-group">
                        <label class="control-label" for="destination">Destination</label>
                        <div class="controls">
                            <select id="selectError" class="col-md-12" data-rel="chosen" name="destination">
                            <option value='0'>?</option>
                            <option value=''>abuja</option>
                            </select>
                        </div>
                    </div>
                </div>


                 <div class="box col-md-6">
                    <div class="form-group">
                        <label class="control-label" for="Type of cargo">Type of cargo</label>
                        <div class="controls">
                            <select id="selectError" class="col-md-12" data-rel="chosen" name="type_cargo">
                            <option value='0'>?</option>
                            <option value=''>destin 1</option>
                            </select>
                        </div>
                    </div>
                </div>

                 <div class="box col-md-6">
                    <div class="form-group">
                        <label class="control-label" for="weight">Weight</label>
                        <input type="text" name="weight" class="form-control required" placeholder="Weight">
                    </div>
                </div>

            <div class="box col-md-6">
                <div class="form-group">
                        <label class="control-label" for="pick-up-date">Pick-up Date</label>
                        <input type="text" name="pickupdate" class="form-control required" data-field="date" placeholder="Enter Pick-up Date">
                </div>
            </div>
                    
                <div id="dtBox"></div>

                <div class="box col-md-6">
                    <div class="form-group">
                        <label class="control-label" for="type_truck">Type of truck</label>
                        <div class="controls">
                            <select id="selectError" class="col-md-12 required" data-rel="chosen" name="type_truck">
                            <option value='0'>?</option>
                            <option value=''>Truck</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="box col-md-12">
                    <div class="form-group">
                        <label class="control-label" for="additional_info">Addtional information</label>
                        <textarea type="text" name="additional_info" class="form-control required" placeholder="Enter addtional information"></textarea>
                    </div>
                </div>
                <div class="box col-md-6">
                    <div class="form-group">
                        <button type="submit" class="btn btn-custom">
                            <i class="glyphicon glyphicon-plus"></i>Submit
                        </button>
                    </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
    <!--/span-->

</div><!--/row-->



@endsection