@extends('exchange.client.layout.content')
@section('content')

<div class="row">
    <div class="box col-md-12">
        <!--if @ session -->
        <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert">x</button>
            success
        </div>
        <!--endif-->

        <!--origin to server form-->
        <form id="originForm" style="display:none;">
            {{csrf_field()}}
            <input type="hidden" id="origin2" name="originToServer">
        </form>
        <!--end origin to server form-->

        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-plus"></i> Complete Your Order</h2>

            </div>
            <div class="box-content row">
                <form role="form" method="post" class="validate"  action="{{url('/exchange/customer/client-area/complete_order')}}"  enctype="multipart/form-data">
                    {{csrf_field()}}

                    <input name="order_id" type="hidden" value="{{$order_id}}">
                    <div class="box col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="weight">Pickup Date</label>
                            <input type="date" name="pickupdate" id="datepicker" class="form-control" placeholder="PickUp Date">
                        </div>
                    </div>

                    <div class="box col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="pick-up-date">Pickup Address</label>
                            <textarea name="pickup_address" class="form-control required" placeholder="Enter addtional information"></textarea>
                        </div>
                    </div>

                    <div id="dtBox"></div>

                    <div class="box col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="type_truck">Contact Phone Number</label>
                            <input type="text" name="contact_phone" class="form-control required" placeholder="Enter Contact Phone">
                        </div>
                    </div>

                    <div class="box col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="additional_info">Item Description</label>
                            <textarea type="text" name="description" class="form-control required" placeholder="Enter addtional information"></textarea>
                        </div>
                    </div>
                    <div class="box col-md-6">
                        <div class="form-group">
                            <button type="submit" class="btn btn-custom">
                                <i class="glyphicon glyphicon-plus"></i>Complete Order
                            </button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
    <!--/span-->

</div><!--/row-->



@endsection