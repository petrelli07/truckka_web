@extends('exchange.client.layout.content')
@section('content')



<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-eye-open"></i> Truck Details</h2>
            </div>
            <div class="box-content">
                <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
                    <thead>

                    <tr>
                        <th>Plate Number</th>
                        <th>Driver Name</th>
                        <th>Driver Phone Number</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($truck_details as $details)
                    <tr>
                        <td class="center">{{$details->plate_number}}</td>
                        <td class="center">{{$details->driver_name}}</td>
                        <td class="center">{{$details->driver_phone_number}}</td>
                        <td class="center">
                            @if($details->exchange_status_id == 33)
                                <a href='{{url("/exchange/customer/client-area/confirm-delivery/{$details->id}")}}'>Confirm Delivery</a>
                            @else
                                No Action Required
                            @endif
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!--/span-->

</div>



@endsection