
    
    
    <hr>
    <footer class="row">
        <p class="col-md-9 col-sm-9 col-xs-12 copyright" style="margin-left:10px;">&copy;
         <a href="#" target="_blank"> Truckka</a> 2018</p>
 
    </footer>
<!--/.fluid-container-->

<script src="{{URL::to('exchange/dashboard_assets/assets/js/custom-script.js')}}"></script>

<!-- external javascript -->

<script src="{{URL::to('exchange/dashboard_assets/assets/js/bootstrap.min.js')}}"></script>
<!-- data table plugin -->
<script src="{{URL::to('exchange/dashboard_assets/assets/js/jquery.dataTables.min.js')}}"></script>
<!-- select or dropdown enhancer -->
<script src="{{URL::to('exchange/dashboard_assets/assets/js/chosen.jquery.min.js')}}"></script>
<!-- plugin for gallery image view -->
<script src="{{URL::to('exchange/dashboard_assets/assets/js/jquery.colorbox-min.js')}}"></script>
<!-- tour plugin -->
<script src="{{URL::to('exchange/dashboard_assets/assets/js/bootstrap-tour.min.js')}}"></script>
<!-- rating plugin -->
<script src="{{URL::to('exchange/dashboard_assets/assets/js/jquery.raty.min.js')}}"></script>
<!-- datepicker -->
<script src="{{URL::to('exchange/dashboard_assets/assets/js/DateTimePicker.js')}}"></script>
<!-- application script for Charisma demo -->
<script src="{{URL::to('exchange/dashboard_assets/assets/js/charisma.js')}}"></script>

  <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDz45dPojJCLeQZtPizla8CX2s4rOvoky0&callback=initMap&libraries=geometry,places">
    </script>
    <script type="text/javascript">

        function initMap () {
          $(document).ready(function  (argument) {
            
            let pickupInput          = document.getElementById('pickup_address');
            let deliveryInput        = document.getElementById('delivery_address');
            let resultFields         = ['formatted_address','place_id'];
            // let restriction          = google.maps.places.ComponentRestrictions;
            let restriction             =  {country:'NG'};
            
            let autoCompletePickup    = new google.maps.places.Autocomplete(pickupInput);
            let autoCompleteDelivery  = new google.maps.places.Autocomplete(deliveryInput);


            autoCompletePickup.setFields(resultFields);
            autoCompleteDelivery.setFields(resultFields);

            autoCompletePickup.setComponentRestrictions(restriction);
            autoCompleteDelivery.setComponentRestrictions(restriction);


            google.maps.event.addListener(autoCompletePickup, 'place_changed', function() {
                
                var place = autoCompletePickup.getPlace();
                $('#pickup_address').val(place.formatted_address);
                $('#pickup_placeId').val(place.place_id);

            });

            google.maps.event.addListener(autoCompleteDelivery, 'place_changed', function() {
              
                var place = autoCompleteDelivery.getPlace();
                $('#deliveryInput').val(place.formatted_address);
                $('#delivery_placeId').val(place.place_id);

            });

          })

        }
       


</script>