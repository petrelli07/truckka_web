@extends('exchange.client.layout.content')
@section('content')
<link href="{{URL::to('home_assets/assets/css/invoice.css')}}" rel="stylesheet" />


<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-eye-open"></i> Invoices</h2>
            </div>
            <div class="box-content">
                <table class="neworder">

                    <thead class="theadorder">
                    <tr class="tr_order bgtable">
                        <th class="th_order" colspan="6">INVOICE INFORMATION</th>

                    </tr>
                    <tr class="tr_order">
                        <td class="td_order" colspan="6">
                            <strong>Invoice No: </strong>{{$data['invoice_number']}}<br>
                            <strong>Order No: </strong>{{$data['order_number']}}<br>
                            <strong>Date: </strong> {{$data['date']}}<br>
                            <strong>Invoice Status: @if($data['status'] === 'Pending')
                                <p style="color:red;">Unpaid</p>
                                @else<p style="color:green;">Paid</p>@endif
                            </strong>
                        </td>

                    </tr>
                    </thead>
                    <tbody>
                    <tr class="tr_order bgtable">
                        <th class="th_order">Description</th>
                        <th class="th_order">Origin</th>
                        <th class="th_order">Destination</th>
                        <th class="th_order">Number of Trucks</th>
                        <th class="th_order">Unit Price</th>
                        <th class="th_order">Total Amount</th>
                    </tr>
                    <tr class="tr_order">
                        <td class="td_order">Payment for {{$data['number_of_trucks']}} {{$data['truck_type_description']}} Trucks from {{$data['origin']}} to {{$data['destination']}}</td>
                        <td class="td_order">{{$data['origin']}}</td>
                        <td class="td_order">{{$data['destination']}}</td>
                        <td class="td_order">{{$data['number_of_trucks']}}</td>
                        <td class="td_order">&#x20A6;{{$data['unit_price']}}</td>
                        <td class="td_order">&#x20A6;{{$data['total']}}</td>
                    </tr>
                    </tbody>
                    <tfoot>
                    <tr class="tr_order">
                        <th class="th_order" colspan="5">Total</th>
                        <td class="td_order"> &#x20A6;{{$data['total']}}</td>
                    </tr>
                    <tr class="tr_order">
                        <th class="th_order" colspan="4">Service Charge</th>
                        <td class="td_order"> 10% </td>
                        <td class="td_order">&#x20A6;{{$data['service_charge']}}</td>
                    </tr>
                    <tr class="tr_order">
                        <th class="th_order" colspan="5">Net Total Amount</th>
                        <td class="td_order">&#x20A6;{{$data['total_amount']}}</td>
                    </tr>

                    </tfoot>
                </table>

                <table class="neworder">

                    <tr class="tr_order bgtable">
                        <th class="th_order" colspan="6">MAKE PAYMENT TO:</th>

                    </tr>
                    <tr class="tr_order">
                        <td class="td_order" colspan="2">
                            <strong>Bank Name</strong><br>
                            <p>XXXXX Bank</p>
                        </td>

                        <td class="td_order" colspan="2">
                            <strong>ACCOUNT NAME</strong><br>
                            <p>Truckka Logistics Ltd</p>
                        </td>
                        <td  class="td_order" colspan="2">
                            <strong>ACCOUNT NUMBER:</strong><br>
                            <p>xxxxxxxxx</p>
                        </td>
                    </tr>



                </table>
                <div class="row">
                    <div class="col-md-6">
                        @if($data['status'] === 'Pending')
                        <a class="btn btn-block btn-success" href='{{url("/exchange/customer/client-area/pay_invoice/{$data["invoice_number"]}")}}'>Pay Now</a>
                        @endif
                    </div>
                    <div class="col-md-6">
                        <a class="btn btn-block btn-danger" href='{{url("exchange/customer/update_customer")}}'>Back</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--/span-->

</div>


@endsection