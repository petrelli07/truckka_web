@extends('exchange.truck_owners.layout.content')
@section('content')

<div class="row">
    <div class="box col-md-12">
    <div class="box-inner">
    <div class="box-header well" data-original-title="">
        <h2><i class="glyphicon glyphicon-eye-open"></i> Order Request</h2>
    </div>
    <div class="box-content">
    <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
    <thead>
    <!--if @ session -->
    @if (session('message'))
    <div class="alert alert-success" role="alert">
        <button type="button" class="close" data-dismiss="alert">x</button>
        {{ session('message') }}
    </div>
    @endif
    <!--endif-->
    <tr>

        <th>Origin</th>
        <th>Destination</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    @forelse($match_request as $users)
    <tr>
        <td class="center">{{$users->originStateName}}</td>
        <td class="center">{{$users->destStateName}}</td>
        <td class="center">
            <a href='{{url("/exchange/truck_supplier/add_trucks/{$users->order_number}")}}'>
                Add Trucks
            </a>|
            <a href='{{url("/exchange/truck_supplier/view_order_details/{$users->order_number}")}}'>
                View Details
            </a>
        </td>

    </tr>
    @empty
    <tr>
        <td class="center"></td>
        <td class="center"></td>
        <td class="center"></td>
    </tr>
    @endforelse
    </tbody>
    </table>
    </div>
    </div>
    </div>
    <!--/span-->

    </div>

@endsection