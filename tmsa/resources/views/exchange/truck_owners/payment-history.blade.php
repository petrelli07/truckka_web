@extends('truck_owners.layout.content')
@section('content')



<div class="row">
    <div class="box col-md-12">
    <div class="box-inner">
    <div class="box-header well" data-original-title="">
        <h2><i class="glyphicon glyphicon-eye-open"></i> Payment History</h2>
    </div>
    <div class="box-content">
    <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
    <thead>

    <tr>
        
        <th>Payment Id number</th>
        <th>Order Number</th>
        <th>Amount Paid</th>
        <th>Status</th>
        <th>Date Created</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        
        <td class="center">1</td>
        <td class="center">23j</td>
        <td class="center">23,000</td>
        <td class="center">success</td>
        <td class="center">21/11/2018</td>
        <td class="center">
           
            <a class="btn btn-info btn-sm" href="#">
                <i class="glyphicon glyphicon-eye-open icon-white"></i>
              Descriptions
            </a>
            
            
        </td>
    </tr>
    </tbody>
    </table>
    </div>
    </div>
    </div>
    <!--/span-->

    </div>



@endsection