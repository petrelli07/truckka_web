@extends('exchange.admin.layout.content')
@section('content')


<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">

            </div>
            <div class="box-content">

                <!--if has session message-->
                @if (session('message'))
                <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    {{ session('message') }}
                </div>
                @endif
                <!--endif-->
                <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
                    <thead>

                    <tr>
                        <th>Id</th>
                        <th>Reference Number</th>
                        <th>Plate Number</th>
                        <th>Amount</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>

                    </thead>
                    <tbody>
                    <?php $x =1; ?>
                    @forelse($truckPayment as $request)
                    <tr>
                        <td class="center">{{$x++}}</td>
                        <td class="center">{{$request->reference_number}}</td>
                        <td class="center">{{$request->plate_number}}</td>
                        <td class="center">
                            &#8358;{{number_format($request->transporter_base_price, 2, '.', ',')}}
                        </td>
                        <td class="center">{{$request->status_description}}</td>

                        <td class="center">
                            @if($request->status_description==="Truck Part Payment Pending")
                            <a href='{{url("/exchange/admin/payments/part_payment/{$request->reference_number}")}}' class="btn-danger btn-sm">
                                Make Part Payment
                            </a>
                            @elseif($request->status_description==="Truck Part Payment Confirmed")
                                {{$request->status_description}}
                            @elseif($request->status_description==="Truck Balance Payment Pending")
                            <a href='{{url("exchange/admin/payments/part_payment/{$request->reference_number}")}}' class="btn-success btn-sm">
                                Make Balance Payment
                            </a>
                            @elseif($request->status_description==="Truck Balance Payment Confirmed")
                                {{$request->status_description}}
                            @endif
                        </td>

                    </tr>
                    @empty

                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!--/span-->




    @endsection