@extends('exchange.admin.layout.content')
@section('content')

<div class="row">
    <div class="box col-md-12">
        <!--if @ session -->
        <!--<div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert">x</button>
               success
        </div>-->
        <!--endif-->

        <!--origin to server form-->
        <form id="originForm" style="display:none;">
            {{csrf_field()}}
            <input type="hidden" id="origin2" name="originToServer">
        </form>
        <!--end origin to server form-->

        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-plus"></i> Create New Orders</h2>

            </div>
            <div class="box-content row">
                <!--if has session message-->
                @if (session('message'))
                <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    {{ session('message') }}
                </div>
                @endif
                <!--endif-->
                <form role="form" method="post" class="validate"  action="{{url('/exchange/admin/routes/update_route')}}"  enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="box col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="origin">Origin</label>
                            <div class="controls">
                                <select  class="form-control col-md-12" disabled>
                                    <option value=''>{{$data['origin']}}</option>
                                </select>
                            </div>

                        </div>
                    </div>


                    <div class="box col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="destination">Destination</label>
                            <div class="controls">
                                <select  class="col-md-12 form-control"  disabled>
                                    <option value=''>{{$data['destination']}}</option>
                                </select>
                            </div>

                        </div>
                    </div>

                    <input name="route_id" value="{{$data['id']}}" type="hidden">

                    <div class="box col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="Type of cargo">Customer Base Price</label>
                            <div class="controls">
                                <input type="text" class="col-md-12 form-control price" value="{{$data['customer_base_price']}}" id="price" name="customer_base_price" required/>
                            </div>
                            @if ($errors->has('customer_base_price'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('customer_base_price') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>


                    <div class="box col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="Type of cargo">Customer Service Charge</label>
                            <div class="controls">
                                <input type="text" class="col-md-12 form-control price" id="price" value="{{$data['customer_service_charge']}}" name="customer_service_charge" required/>
                            </div>
                            @if ($errors->has('customer_service_charge'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('customer_service_charge') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>


                    <div class="box col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="Type of cargo">Transporter Base Fare</label>
                            <div class="controls">
                                <input type="text" class="col-md-12 form-control price" value="{{$data['transporter_base_price']}}"  id="price" name="transporter_base_price" required/>
                            </div>
                            @if ($errors->has('transporter_base_price'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('transporter_base_price') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>


                    <div class="box col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="Type of cargo">Transporter Service Charge</label>
                            <div class="controls">
                                <input type="text" class="col-md-12 form-control price" value="{{$data['transporter_service_charge']}}" id="price" name="transporter_service_charge" required/>
                            </div>
                            @if ($errors->has('transporter_service_charge'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('transporter_service_charge') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>


                    <div class="box col-md-6">

                    </div>


                    <div class="box col-md-6">
                        <div class="form-group">
                            <button type="submit" class="btn btn-custom">
                                <i class="glyphicon glyphicon-plus"></i>Submit
                            </button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
    <!--/span-->

</div><!--/row-->



@endsection