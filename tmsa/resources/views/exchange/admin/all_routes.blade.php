@extends('exchange.admin.layout.content')
@section('content')


<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-plus"></i> <a href="{{url('/exchange/admin/route/add_new')}}" class="btn btn-success btn-sm">Add New Route</a></h2>
            </div>
            <div class="box-content">

                <!--if has session message-->
                @if (session('message'))
                <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    {{ session('message') }}
                </div>
                @endif
                <!--endif-->
                <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
                    <thead>

                    <tr>
                        <th>Id</th>
                        <th>Origin</th>
                        <th>Destination</th>
                        <th>Transporter Price</th>
                        <th>Action</th>
                    </tr>

                    </thead>
                    <tbody>
                    <?php $x =1; ?>
                    @forelse($route as $request)
                    <tr>
                        <td class="center">{{$x++}}</td>
                        <td class="center">{{$request->originStateName}}</td>
                        <td class="center">{{$request->destStateName}}</td>
                        <td class="center">&#8358;{{number_format($request->transporter_base_price, 2, '.', ',')}}</td>

                        <td class="center">
                        @if($request->exchange_status_id==1)
                            <a href='{{url("/exchange/admin/routes/suspend_route/{$request->id}")}}' class="btn-danger btn-sm">
                                Suspend Route
                            </a>
                        @elseif($request->exchange_status_id==2)
                            <a href='{{url("/exchange/admin/routes/activate_route/{$request->id}")}}' class="btn-success btn-sm">
                                Activate Route
                            </a>

                        @endif
                            |
                            <a href='{{url("/exchange/admin/routes/edit_route/{$request->id}")}}' class="btn-success btn-sm">
                                Edit Route
                            </a>
                        </td>

                    </tr>
                    @empty

                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!--/span-->




    @endsection