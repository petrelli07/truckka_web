@extends('exchange.admin.layout.content')
@section('content')
<link href="{{URL::to('home_assets/assets/css/invoice.css')}}" rel="stylesheet" />





<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-eye-open"></i> Invoices</h2>
            </div>
            <div class="box-content">
                <table class="neworder">

                    <thead class="theadorder">
                    <tr class="tr_order bgtable">
                        <th class="th_order" colspan="6">INVOICE INFORMATION</th>

                    </tr>
                    <tr class="tr_order">
                        <td class="td_order" colspan="6">
                            <strong>Invoice No: </strong>#123456789<br>
                            <strong>Date: </strong> 14 January 2025<br>

                        </td>

                    </tr>
                    </thead>
                    <tbody>
                    <tr class="tr_order bgtable">
                        <th class="th_order">Description</th>
                        <th class="th_order">Origin</th>
                        <th class="th_order">Destination</th>
                        <th class="th_order">Number of truck</th>
                        <th class="th_order">Unit Price</th>
                        <th class="th_order">Amount</th>
                    </tr>
                    <tr class="tr_order">
                        <td class="td_order">Goods,on hhm hms </td>
                        <td class="td_order">Lagos</td>
                        <td class="td_order">Abuja</td>
                        <td class="td_order">5</td>
                        <td class="td_order">200</td>
                        <td class="td_order">300,000</td>
                    </tr>
                    <tr class="tr_order">
                        <td class="td_order">Goods,on hhm hms </td>
                        <td class="td_order">Lagos</td>
                        <td class="td_order">Abuja</td>
                        <td class="td_order">5</td>
                        <td class="td_order">200</td>
                        <td class="td_order">300,000</td>
                    </tr>
                    </tbody>
                    <tfoot>
                    <tr class="tr_order">
                        <th class="th_order" colspan="5">Total</th>
                        <td class="td_order"> 110.00</td>
                    </tr>
                    <tr class="tr_order">
                        <th class="th_order" colspan="4">Charges</th>
                        <td class="td_order"> 8% </td>
                        <td class="td_order">8.80</td>
                    </tr>
                    <tr class="tr_order">
                        <th class="th_order" colspan="5">Net Total Amount</th>
                        <td class="td_order">₦118.80</td>
                    </tr>

                    </tfoot>
                </table>

                <table class="neworder">

                    <tr class="tr_order bgtable">
                        <th class="th_order" colspan="6">MAKE PAYMENT TO:</th>

                    </tr>
                    <tr class="tr_order">
                        <td class="td_order" colspan="2">
                            <strong>Bank Name</strong><br>
                            <p>Uba bank</p>
                        </td>

                        <td class="td_order" colspan="2">
                            <strong>ACCOUNT NAME</strong><br>
                            <p>james paul</p>
                        </td>
                        <td  class="td_order" colspan="2">
                            <strong>ACCOUNT NUMBER:</strong><br>
                            <p>202028678 </p>
                        </td>
                    </tr>



                </table>
            </div>
        </div>
    </div>
    <!--/span-->

</div>

    
@endsection