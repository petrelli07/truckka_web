@extends('exchange.admin.layout.content')
@section('content')




<div class="row">
    <div class="box col-md-12">
    <div class="box-inner">
    <div class="box-header well" data-original-title="">
        <h2><i class="glyphicon glyphicon-eye-open"></i> Assign Orders</h2>
    </div>
    <div class="box-content">
        @if (session('message'))
        <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert">x</button>
            {{ session('message') }}
        </div>
        @endif
        <!--endif-->
    <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
    <thead>

    <tr>
        
        <th>Supplier Name</th>
        <th>Supplier Email</th>
        <th>Supplier Phone</th>
        <th>Capacity</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    @foreach($supplier_match as $match)
    <tr>
        
        <td class="center">{{$match->name}}</td>
        <td class="center">{{$match->email}}</td>
        <td class="center">{{$match->phone}}</td>
        <td class="center">{{$match->number_of_truck}}</td>
        <td class="center">
            <form action="{{url('exchange/admin/match_supplier_order')}}" method="post">
                <input type="hidden" name="supplier_id" value="{{$match->truck_supplier_id}}">
                <input type="hidden" name="order_id" value="{{$order_id}}">
                <button class="btn btn-success btn-sm" type="submit">Match Order</button>
                {{csrf_field()}}
            </form>

        </td>
    </tr>
    @endforeach
    </tbody>
    </table>
    </div>
    </div>
    </div>
    <!--/span-->

    </div>


@endsection