<!DOCTYPE html>
<html lang="en">
<body>
<div align="center"><h3>Southern Streams Universal Services Invoice</h3></div>

<div align="left">
            <div class="col-sm-6">
                <address>
                    <strong>Order No:</strong>
                    {{$serviceId}}
                    <br><strong>Invoice No:</strong>
                    {{$invoice}}
                </address>
            </div>
    </div>

    <div align="right">
                <address>
                    <strong>Billed To:</strong><br>
                    {{$billedTo}}<br>{{$billedToRc}}
                </address>
            </div>
            <div align="left">
                <address>
                    <strong>Origin:</strong><br>

                    {{$shippedFrom}}<br>
                    <strong>Destination:</strong><br>

                    {{$shippedTo}}
                </address>
            </div>

    <div align="center"><h4>Resource Details</h4></div>

    <div align="center">
        <table align="center">
                <thead>
                <tr>
                    <td><strong>Plate Number</strong></td>
                    <td><strong>Driver Name</strong></td>
                </tr>
                </thead>
                <tbody>
                <!-- foreach ($order->lineItems as $line) or some such thing here -->
                @foreach($resGPSDet as $r)
                <tr>
                    <td>{{ $r->plateNumber }}</td>
                    <td>{{ $r->driverName }}</td>
                </tr>
                @endforeach
                </tbody>
        </table>
    </div>
<br/>   
    <div align="center"><strong>Bank Details</strong></div>
    <br/>
    <div align="center">
    <div><strong>Bank: FCMB PLC</strong></div><br/>
    <div><strong>AC Name: SOUTHERN STREAMS UNIVERSAL SERVICES</strong></div><br/>
    <div><strong>AC NO: 2146 319 032</strong></div>
    </div>

    <div align="center">
                        <h3 ><strong>Order summary</strong></h3>
                    </div>
                    <div>
                        <div>
                            <table>
                                <thead>
                                <tr>
                                    <td><strong>Resource Type</strong></td>
                                    <td class="text-center"><strong>Description</strong></td>
                                    <td class="text-center"><strong>Quantity</strong></td>
                                </tr>
                                </thead>
                                <tbody>
                                <!-- foreach ($order->lineItems as $line) or some such thing here -->
                                @foreach($resourceTypeNumber as $carrResDet)
                                <tr>
                                    <td>{{ $carrResDet->resourceType }}</td>
                                    <td class="text-center">{{ $description }}</td>
                                    <td class="text-center">{{ $carrResDet->resourceNumber }}</td>
                                </tr>
                                @endforeach
                                </tbody>
                             </table>
                        </div>
                        <div>
                        <div align="right">
                        
                        <h4><b>Gross</b>: #{{$totalAmount}}</h4>
                        <h4><b>VAT(5%)</b>: #{{$vatVal}}</h4>
                        <div ></div>
                        <h4><b>Total</b>:#{{$amt}}</h4>
                        </div>
                        
                        </div>
                    </div>
            
    

</body>
</html>

