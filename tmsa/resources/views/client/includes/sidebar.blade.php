<div class="page-sidebar-wrapper">
										<!-- BEGIN SIDEBAR -->
										<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
										<!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
										<div class="page-sidebar navbar-collapse collapse">
												<!-- BEGIN SIDEBAR MENU -->
												<!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
												<!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
												<!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
												<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
												<!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
												<!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
												<ul class="page-sidebar-menu  page-header-fixed page-sidebar-menu-light " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
														<!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
														<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
														<li class="sidebar-toggler-wrapper hide">
																<div class="sidebar-toggler">
																		<span></span>
																</div>
														</li>
                            <li class="nav-item truckka-back nav-item-head">
                                    <a href="{{ url('/clientHome') }}" class="nav-link nav-toggle white bold ">
                                        <div class="flex-container flex-space-between flex-vertical-align">
                                          <span class="title">Home</span>
                                          <span class=" "><i class="fas fa-home"></i></span>
                                        </div>
                                    </a>
                                </li>
														<!-- END SIDEBAR TOGGLER BUTTON -->
														<!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
                            @if(Helper::hasPermissionUrl(array('create_order','viewOrders','getAll')))
    													<li class="nav-item truckka-back nav-item-head">
    																<a href="javascript:;" class="nav-link nav-toggle white bold ">
    																		<div class="flex-container flex-space-between flex-vertical-align">
    																			<span class="title">Orders</span>
    																			<span class=" "><i class="fas fa-plus-square"></i></span>
    																		</div>
    																</a>
    																<ul class="sub-menu box-shadow" >
    																	 @if(Helper::hasPermissionUrl(array('create_order')))
    																		<li class="nav-item inner-nav-item">
    																				<a href="{{url('/create_order')}}" class="nav-link f-parargraph black mobile-white" style="font-size: 14px;">
    																					<div class="flex-container flex-space-between flex-vertical-align">
    																						<span>Create New</span>
    																						<span><i class="fas fa-cart-plus"></i></span>
    																					</div>
    																				</a>
    																		</li>
                                        @endif
                                       @if(Helper::hasPermissionUrl(array('viewOrders')))
    																	   <li class="nav-item inner-nav-item">
    																				<a href="{{url('/viewOrders')}}" class="nav-link f-parargraph black mobile-white" style="font-size: 14px;">
    																					<div class="flex-container flex-space-between flex-vertical-align">
    																						<span>Order Status</span>
    																						<span><i class="fas fa-cart-arrow-down"></i></span>
    																					</div>
    																				</a>
    																		</li>
                                      @endif
                                       @if(Helper::hasPermissionUrl(array('getAll')))

    																		<li class="nav-item inner-nav-item">
    																				<a href="{{url('/getAll')}}" class="nav-link f-parargraph black mobile-white" style="font-size: 14px;">
    																					<div class="flex-container flex-space-between flex-vertical-align">
    																						<span>All Company Orders</span>
    																						<span><i class="fas fa-cart-arrow-down"></i></span>
    																					</div>
    																				</a>
    																		</li>
                                      @endif
    																</ul>
    													</li>
													 @endif

                           @if(Helper::hasPermissionUrl(array('getAllInvoices','invoicePayments')))

                          <li class="nav-item truckka-back nav-item-head">
                                <a href="javascript:;" class="nav-link nav-toggle white bold ">
                                    <div class="flex-container flex-space-between flex-vertical-align">
                                      <span class="title">Invoices</span>
                                      <span class=" "><i class="fas fa-plus-square"></i></span>
                                    </div>
                                </a>
                                <ul class="sub-menu box-shadow" >
                                  @if(Helper::hasPermissionUrl(array('getAllInvoices')))
                                    <li class="nav-item inner-nav-item">
                                        <a href="{{url('/getAllInvoices')}}" class="nav-link f-parargraph black mobile-white" style="font-size: 14px;">
                                          <div class="flex-container flex-space-between flex-vertical-align">
                                            <span>Invoices</span>
                                            <span><i class="fas fa-file-invoice-dollar"></i></span>
                                          </div>
                                        </a>
                                    </li>

                                  @endif
                                  @if(Helper::hasPermissionUrl(array('invoicePayments')))
                                 
                                    <li class="nav-item inner-nav-item">
                                        <a href="{{url('/invoicePayments')}}" class="nav-link f-parargraph black mobile-white" style="font-size: 14px;">
                                          <div class="flex-container flex-space-between flex-vertical-align">
                                            <span>Payments</span>

                                            <span><i class="fas fa-file-invoice-dollar"></i></span>
                                          </div>
                                        </a>
                                    </li>
                                  @endif
                                   <!-- <li class="nav-item inner-nav-item">
                                        <a href="#" class="nav-link f-parargraph black" style="font-size: 14px;">
                                          <div class="flex-container flex-space-between flex-vertical-align">
                                            <span>Demurage</span>
                                            <span><i class="fas fa-fill-drip"></i></span>
                                          </div>
                                        </a>
                                    </li>
                                    <li class="nav-item inner-nav-item">
                                        <a href="#" class="nav-link f-parargraph black" style="font-size: 14px;">
                                          <div class="flex-container flex-space-between flex-vertical-align">
                                            <span>Settlements</span>
                                            <span><i class="fas fa-handshake"></i></span>
                                          </div>
                                        </a>
                                    </li> -->
                                </ul>
                          </li>
                          @endif
                           

                          @if(Helper::hasPermissionUrl(array('createNewSupply','all_client_supply_orders')))

													<li class="nav-item truckka-back nav-item-head">
																<a href="javascript:;" class="nav-link nav-toggle white bold ">
																		<div class="flex-container flex-space-between flex-vertical-align">
																			<span class="title">RFQs</span>
																			<span class=" "><i class="fas fa-plus-square"></i></span>
																		</div>
																</a>
																<ul class="sub-menu box-shadow" >
																	
																	

																	@if(Helper::hasPermissionUrl(array('supply/create')))
																		<li class="nav-item inner-nav-item">
																				<a href="{{url('/supply/create')}}" class="nav-link f-parargraph black mobile-white" style="font-size: 14px;">
																					<div class="flex-container flex-space-between flex-vertical-align">
																						<span>Create New RFQ</span>
																						<span><i class="fas fa-file-invoice-dollar"></i></span>
																					</div>
																				</a>
																		</li>
                                  									@endif


                                  									
                                  									@if(Helper::hasPermissionUrl(array('all_client_supply_orders')))
																		<li class="nav-item inner-nav-item">
																				<a href="{{url('/all_client_supply_orders')}}" class="nav-link f-parargraph black mobile-white" style="font-size: 14px;">
																					<div class="flex-container flex-space-between flex-vertical-align">
																						<span>All RFQs</span>
																						<span><i class="fas fa-file-invoice-dollar"></i></span>
																					</div>
																				</a>
																		</li>
                                  									@endif
																	 <!-- <li class="nav-item inner-nav-item">
																				<a href="#" class="nav-link f-parargraph black" style="font-size: 14px;">
																					<div class="flex-container flex-space-between flex-vertical-align">
																						<span>Demurage</span>
																						<span><i class="fas fa-fill-drip"></i></span>
																					</div>
																				</a>
																		</li>
																		<li class="nav-item inner-nav-item">
																				<a href="#" class="nav-link f-parargraph black" style="font-size: 14px;">
																					<div class="flex-container flex-space-between flex-vertical-align">
																						<span>Settlements</span>
																						<span><i class="fas fa-handshake"></i></span>
																					</div>
																				</a>
																		</li> -->
																</ul>
													</li>
                          @endif
                      

													<li class="nav-item truckka-back nav-item-head">
																<a href="javascript:;" class="nav-link nav-toggle white bold ">
																		<div class="flex-container flex-space-between flex-vertical-align">
																			<span class="title">Performance</span>
																			<span class=" "><i class="fas fa-plus-square"></i></span>
																		</div>
																</a>
																<ul class="sub-menu box-shadow" >

																		<li class="nav-item inner-nav-item">
																				<a href="{{url('/performance')}}" class="nav-link f-parargraph black mobile-white" style="font-size: 14px;">
																					<div class="flex-container flex-space-between flex-vertical-align">
																						<span>KPIs</span>
																						<span><i class="fas fa-file-invoice-dollar"></i></span>
																					</div>
																				</a>
																		</li>
																	 <!-- <li class="nav-item inner-nav-item">
																				<a href="#" class="nav-link f-parargraph black" style="font-size: 14px;">
																					<div class="flex-container flex-space-between flex-vertical-align">
																						<span>Demurage</span>
																						<span><i class="fas fa-fill-drip"></i></span>
																					</div>
																				</a>
																		</li>
																		<li class="nav-item inner-nav-item">
																				<a href="#" class="nav-link f-parargraph black" style="font-size: 14px;">
																					<div class="flex-container flex-space-between flex-vertical-align">
																						<span>Settlements</span>
																						<span><i class="fas fa-handshake"></i></span>
																					</div>
																				</a>
																		</li> -->
																</ul>
													</li>




                          <li class="nav-item truckka-back nav-item-head">
                                <a href="javascript:;" class="nav-link nav-toggle white bold ">
                                    <div class="flex-container flex-space-between flex-vertical-align">
                                      <span class="title">Track and Trace</span>
                                      <span class=" "><i class="fas fa-plus-square"></i></span>
                                    </div>
                                </a>
                                <ul class="sub-menu box-shadow" >

                                    <li class="nav-item inner-nav-item">

                                        <a href="{{url('/client/map/view')}}" class="nav-link f-parargraph black mobile-white" style="font-size: 14px;">
                                          <div class="flex-container flex-space-between flex-vertical-align">
                                            <span>Track</span>
                                            <span><i class="fas fa-file-invoice-dollar"></i></span>
                                          </div>
                                        </a>
                                    </li>

                                    <li class="nav-item inner-nav-item">

                                        <a href="{{url('/client/map/viewHistory')}}" class="nav-link f-parargraph black mobile-white" style="font-size: 14px;">
                                          <div class="flex-container flex-space-between flex-vertical-align">
                                            <span>View Resource History</span>
                                            <span><i class="fas fa-file-invoice-dollar"></i></span>
                                          </div>
                                        </a>
                                    </li>
                                   <!-- <li class="nav-item inner-nav-item">
                                        <a href="#" class="nav-link f-parargraph black" style="font-size: 14px;">
                                          <div class="flex-container flex-space-between flex-vertical-align">
                                            <span>Demurage</span>
                                            <span><i class="fas fa-fill-drip"></i></span>
                                          </div>
                                        </a>
                                    </li>
                                    <li class="nav-item inner-nav-item">
                                        <a href="#" class="nav-link f-parargraph black" style="font-size: 14px;">
                                          <div class="flex-container flex-space-between flex-vertical-align">
                                            <span>Settlements</span>
                                            <span><i class="fas fa-handshake"></i></span>
                                          </div>
                                        </a>
                                    </li> -->
                                </ul>
                          </li>


                          @if(Helper::hasPermissionUrl(array('user/new','users/all','role/all','role/new')))
                            <li class="nav-item truckka-back nav-item-head">
                                  <a href="javascript:;" class="nav-link nav-toggle white bold ">
                                      <div class="flex-container flex-space-between flex-vertical-align">
                                        <span class="title">Admin Tasks</span>
                                        <span class=" "><i class="fas fa-plus-square"></i></span>
                                      </div>
                                  </a>
                                  <ul class="sub-menu box-shadow" >
                                   @if(Helper::hasPermissionUrl(array('user/new','users/all')))
                                     
                                      <li class="nav-item truckka-back inner-nav-item nav-item-head">
                                          <a href="javascript:;" class="nav-link white nav-toggle f-parargraph black mobile-white" style="font-size: 14px;">
                                            <div class="flex-container flex-space-between flex-vertical-align">
                                              <span>Users</span>
                                              <span class=" "><i class="fas fa-plus-square"></i></span>
                                            </div>
                                          </a>
                                          <ul class="sub-menu">
                                      @if(Helper::hasPermissionUrl(array('user/new')))
                                            <li class="nav-item inner-nav-item">
                                              <a href="{{url('/user/new')}}" class="nav-link f-parargraph black mobile-white" style="font-size: 14px;">
                                                <div class="flex-container flex-space-between flex-vertical-align">
                                                  <span>Create</span>
                                                  <span><i class="fas fa-file-invoice-dollar"></i></span>
                                                </div>
                                              </a>
                                            </li>
                                      @endif
                                      @if(Helper::hasPermissionUrl(array('users/all')))

                                            <li class="nav-item inner-nav-item">
                                                  <a href="{{url('/users/all')}}" class="nav-link f-parargraph black mobile-white" style="font-size: 14px;">
                                                    <div class="flex-container flex-space-between flex-vertical-align">
                                                      <span>All</span>
                                                      <span><i class="fas fa-file-invoice-dollar"></i></span>
                                                    </div>
                                                  </a>
                                            </li>
                                      @endif
                                          </ul>
                                      </li>
                                  @endif



                                  @if(Helper::hasPermissionUrl(array('suppliers')))
                                  
                                  <li class="nav-item truckka-back inner-nav-item nav-item-head">
                                          <a href="javascript:;" class="nav-link white nav-toggle f-parargraph black mobile-white" style="font-size: 14px;">
                                            <div class="flex-container flex-space-between flex-vertical-align">
                                              <span>Manage Suppliers</span>
                                              <span class=" "><i class="fas fa-plus-square"></i></span>
                                            </div>
                                          </a>
                                          <ul class="sub-menu">

											<li class="nav-item inner-nav-item">
													<a href="{{url('/suppliers')}}" class="nav-link f-parargraph black mobile-white" style="font-size: 14px;">
														<div class="flex-container flex-space-between flex-vertical-align">
															<span>View All Suppliers</span>
															<span><i class="fas fa-file-invoice-dollar"></i></span>
														</div>
													</a>
											</li>
      									@endif

										@if(Helper::hasPermissionUrl(array('supplier/create')))
											<li class="nav-item inner-nav-item">
													<a href="{{url('/supplier/create')}}" class="nav-link f-parargraph black mobile-white" style="font-size: 14px;">
														<div class="flex-container flex-space-between flex-vertical-align">
															<span>Create New Supplier</span>
															<span><i class="fas fa-file-invoice-dollar"></i></span>
														</div>
													</a>
											</li>
      									@endif

										@if(Helper::hasPermissionUrl(array('suppliers/group/create')))
											<li class="nav-item inner-nav-item">
													<a href="{{url('/suppliers/group/create')}}" class="nav-link f-parargraph black mobile-white" style="font-size: 14px;">
														<div class="flex-container flex-space-between flex-vertical-align">
															<span>Create New Supply Group</span>
															<span><i class="fas fa-file-invoice-dollar"></i></span>
														</div>
													</a>
											</li>
      									@endif

      									@if(Helper::hasPermissionUrl(array('suppliers/groups')))
											<li class="nav-item inner-nav-item">
													<a href="{{url('/suppliers/groups')}}" class="nav-link f-parargraph black mobile-white" style="font-size: 14px;">
														<div class="flex-container flex-space-between flex-vertical-align">
															<span>View All Supply Groups</span>
															<span><i class="fas fa-file-invoice-dollar"></i></span>
														</div>
													</a>
											</li>
      									@endif
      								</ul>
      								</li>

      									


                                    <!-- @if(Helper::hasPermissionUrl(array('role/all','role/new')))

                                      <li class="nav-item truckka-back inner-nav-item nav-item-head">
                                          <a href="javascript:;" class="nav-link white nav-toggle f-parargraph black mobile-white" style="font-size: 14px;">
                                            <div class="flex-container flex-space-between flex-vertical-align">
                                              <span>Advance</span>
                                              <span class=" "><i class="fas fa-plus-square"></i></span>
                                            </div>
                                          </a>
                                          <ul class="sub-menu">
                                            @if(Helper::hasPermissionUrl(array('role/all')))

                                            <li class="nav-item inner-nav-item">
                                              <a href="{{url('/role/all')}}" class="nav-link f-parargraph black mobile-white" style="font-size: 14px;">
                                                <div class="flex-container flex-space-between flex-vertical-align">
                                                  <span>My Roles</span>
                                                  <span><i class="fas fa-file-invoice-dollar"></i></span>
                                                </div>
                                              </a>
                                            </li>
                                            @endif
                                             @if(Helper::hasPermissionUrl(array('role/new')))

                                            <li class="nav-item inner-nav-item">
                                                  <a href="{{url('/role/new')}}" class="nav-link f-parargraph black mobile-white" style="font-size: 14px;">
                                                    <div class="flex-container flex-space-between flex-vertical-align">
                                                      <span>New Role</span>
                                                      <span><i class="fas fa-file-invoice-dollar"></i></span>
                                                    </div>
                                                  </a>
                                              </li>
                                            @endif
                                          </ul>
                                      </li>
                                    @endif -->

                                      <!-- <li class="nav-item inner-nav-item">
                                          <a href="{{url('/')}}" class="nav-link f-parargraph black mobile-white" style="font-size: 14px;">
                                            <div class="flex-container flex-space-between flex-vertical-align">
                                              <span>Profile</span>
                                              <span><i class="fas fa-file-invoice-dollar"></i></span>
                                            </div>
                                          </a>
                                      </li> -->
                                     <!-- <li class="nav-item inner-nav-item">
                                          <a href="#" class="nav-link f-parargraph black" style="font-size: 14px;">
                                            <div class="flex-container flex-space-between flex-vertical-align">
                                              <span>Demurage</span>
                                              <span><i class="fas fa-fill-drip"></i></span>
                                            </div>
                                          </a>
                                      </li>
                                      <li class="nav-item inner-nav-item">
                                          <a href="#" class="nav-link f-parargraph black" style="font-size: 14px;">
                                            <div class="flex-container flex-space-between flex-vertical-align">
                                              <span>Settlements</span>
                                              <span><i class="fas fa-handshake"></i></span>
                                            </div>
                                          </a>
                                      </li> -->
                                  </ul>

                                  

                                  <li class="nav-item truckka-back nav-item-head">
                                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
	                                  <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="nav-link nav-toggle white bold ">
	                                      <div class="flex-container flex-space-between flex-vertical-align">
	                                        <span class="title">Logout</span>
	                                        <span class=" "><i class="fas fa-plus-square"></i></span>
	                                      </div>
	                                  </a>
                              	</li>

                            </li>
                          @endif

												</ul>
												<!-- END SIDEBAR MENU -->
												<!-- END SIDEBAR MENU -->
										</div>
										<!-- END SIDEBAR -->
								</div>