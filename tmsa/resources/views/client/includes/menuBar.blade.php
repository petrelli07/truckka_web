<div class="page-header navbar navbar-fixed-top" id="custom-page-head">
                <!-- BEGIN HEADER INNER -->
                <div class="page-header-inner ">
                    <!-- BEGIN LOGO -->
                    <div class="page-logo logo-on-mobile">
                        <a href="{{url('/home')}}">
                            <img src="{{ url('assets/layouts/layout/img/logo2.jpg')}}" alt="truckka logo" class="logo-default" /> </a>
                        <div class="menu-toggler sidebar-toggler">
                            <span></span>
                        </div>
                    </div>
                    <!-- END LOGO -->
                    <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                    <a href="javascript:;" class="menu-toggler responsive-toggler nav-on-mobile" data-toggle="collapse" data-target=".navbar-collapse" style="color: grey;font-weight: bold;font-size: 15px">
                        <i class="fas fa-bars"></i>
                    </a>
                    <!-- END RESPONSIVE MENU TOGGLER -->
                    <!-- BEGIN TOP NAVIGATION MENU -->
                    @include('client.includes.topmenu')
                    <!-- END TOP NAVIGATION MENU -->
                </div>
                <!-- END HEADER INNER -->
            </div>