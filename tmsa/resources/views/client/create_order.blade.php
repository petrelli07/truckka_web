
    @include('client.includes.header')
    @include('client.includes.head')
    <!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-content-white page-md">
     <!--  <div class="modal fade bs-modal-sm alert-modal flex-container flex-center flex-vertical-align flex-responsive " id="cancel" tabindex="-1" role="dialog" aria-hidden="true" style="display:inherit;background:rgba(25, 24, 24, 0.48);opacity:1;"><div class="col44 loader-container"><img src="{{url('/assets/pages/img/loader.gif')}}"></div></div> -->
      <div class="modal fade" id="order_summary" tabindex="-1" role="basic" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header" style="background: #e87a25;">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                  <h4 class="modal-title" style="color: white;">Your Order Summary</h4>
              </div>
         <!--      Order Summary container -->
              <div class="modal-body order-summary-container">
                <p class="f-paragraph default-p">Here is a summary of your order</p>
                <div class="flex-container flex-responsive flex-vertical-align flex-space-between">
                  <div class="summary-type">
                    <p class="f-paragraph bold default-p">Truck Type </p>
                  </div>
                  <div class="summary-details">
                    <p class="f-paragraph default-p" id="summaryRes"></p>
                  </div>
                </div>
                <div class="flex-container flex-responsive flex-vertical-align flex-space-between">
                  <div class="summary-type">
                    <p class="f-paragraph bold default-p">Number of Truck -</p>
                  </div>
                  <div class="summary-details">
                    <p class="f-paragraph default-p" id="summaryNum"></p>
                  </div>
                </div>
                <div class="flex-container flex-responsive flex-vertical-align flex-space-between">
                    <div class="summary-type">
                      <p class="f-paragraph bold default-p">Pickup Point -</p>
                    </div>
                    <div class="summary-details">
                      <p class="f-paragraph default-p" id="summaryOrig"> </p>
                    </div>
                </div>
                <div class="flex-container flex-responsive flex-vertical-align flex-space-between">
                    <div class="summary-type">
                      <p class="f-paragraph bold default-p">Order Destination -</p>
                    </div>
                    <div class="summary-details">
                      <p class="f-paragraph default-p" id="summaryDes"> </p>
                    </div>
                </div>
                <div class="flex-container flex-responsive flex-vertical-align flex-space-between">
                    <div class="summary-type">
                      <p class="f-paragraph bold default-p" >Pickup Date -</p>
                    </div>
                    <div class="summary-details">
                      <p class="f-paragraph default-p" id="summaryPickupDate" ></p>
                    </div>
                </div>
                <div class="flex-container flex-responsive flex-vertical-align flex-space-between">
                    <div class="summary-type">
                      <p class="f-paragraph bold default-p" >Expiry Date -</p>
                    </div>
                    <div class="summary-details">
                      <p class="f-paragraph default-p" id="expiryDate" ></p>
                    </div>
                </div>
                <div class="flex-container flex-responsive flex-vertical-align flex-space-between">
                    <div class="summary-type">
                      <p class="f-paragraph bold default-p" >Pickup Window -</p>
                    </div>
                    <div class="summary-details">
                      <p class="f-paragraph default-p" id="summaryPickTimeWindow"></p>
                    </div>
                </div>
                <div class="flex-container flex-responsive flex-vertical-align flex-space-between">
                    <div class="summary-type">
                      <p class="f-paragraph bold default-p">Haulage Desc -</p>
                    </div>
                    <div class="summary-details">
                      <p class="f-paragraph default-p" id="summaryHaulageDec"> </p>
                    </div>
                </div>
                <div class="flex-container flex-responsive flex-vertical-align flex-space-between">
                    <div class="summary-type">
                      <p class="f-paragraph bold default-p">Contact Person -</p>
                    </div>
                    <div class="summary-details">
                      <p class="f-paragraph default-p" id="summaryContactPerson"></p>
                    </div>
                </div>
                <div class="flex-container flex-responsive flex-vertical-align flex-space-between">
                    <div class="summary-type">
                      <p class="f-paragraph bold default-p">Contact Number -</p>
                    </div>
                    <div class="summary-details">
                      <p class="f-paragraph default-p" id="summaryContactnum"></p>
                    </div>
                </div>
              </div>
          <!--      -->
              <input type="hidden" id="region" name="region">
              <div class="modal-footer" style="background: #d3cece;">
                <div>               
                  <button class="btn btn-primary submit-form artificial-submit" form-target="order-form">Continue</button>
                </div>
              </div>
          </div>
          <!-- /.modal-content -->
      </div>
    </div>
        <div class="page-wrapper">
            <!-- BEGIN HEADER -->
                @include('client.includes.menuBar')

            <!-- END HEADER -->
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
            <!-- END HEADER & CONTENT DIVIDER -->
            <!-- BEGIN CONTAINER -->
            <div class="page-container">
                <!-- BEGIN SIDEBAR -->
                    @include('client.includes.sidebar')
                <!-- END SIDEBAR -->
                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <!-- BEGIN PAGE HEADER-->
                        <!-- BEGIN THEME PANEL -->
                       
                        <!-- END THEME PANEL -->
                        <!-- BEGIN PAGE BAR -->
                        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    <a href="index.html">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Dashboard</span>
                                </li>
                            </ul>
                            <div class="page-toolbar">
                                <div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
                                    <i class="icon-calendar"></i>&nbsp;
                                    <span class="thin uppercase hidden-xs"></span>&nbsp;
                                    <i class="fa fa-angle-down"></i>
                                </div>
                            </div>
                        </div>
                        <!-- END PAGE BAR -->
                        <!-- BEGIN PAGE TITLE-->
                        <h1 class="page-title">
                        </h1>
                        <!-- END PAGE TITLE-->
                        <!-- END PAGE HEADER-->
<!--  -->                        <div class="row">
                            <div class="col-lg-10 col-lg-offset-1 col-xs-12 col-sm-12">
                                <div class="portlet light ">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <span class="caption-subject font-dark bold uppercase">Create New Order</span>                                         
                                      </div>
                                        
                                    </div>
                                     <div class="portlet-body form">
                                        <form role="form" ajax-submit="true"  redirect-to="{{url('/clientHome')}}"" class="ajax-form-submit order-form" action="{{url('/createNewOrder')}}">
                                         {{csrf_field()}}
                                            <div class="form-body">
                                            	<div class="row">


                                                <div class="col-md-6">
                                                  <div class="form-group form-md-line-input">
                                                    <input type="text" class="form-control form-copy validate" id="itemDescription" placeholder="Enter Description Here"  bind-to="summaryHaulageDec" name="itemDescription">
                                                    <label for="form_control_1">What are you moving</label>
                                                    <span class="help-block">write a short description about items to be moved</span>
                                                  </div>
                                                </div>


                                                <div class="col-md-6">
                                                   <div class="form-group form-md-line-input has-info">
                                                      <select class="form-control form-copy validate" name="standardOrigin" id="standardOrigin"  bind-to="summaryOrig">
                                                          <option value=""></option>
                                                          @for ($i=0; $i < count($uniqueOrigins); $i++) 
                                                            <option value="{{$uniqueOrigins[$i]}}">{{$uniqueOrigins[$i]}}</option>
                                                           @endfor
                                                      </select>
                                                      <label for="form_control_1">Pickup Point</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                   <div class="form-group form-md-line-input has-info">
                                                        <select name="standardDestination" class="form-control form-copy validate" id="standardDestination" disabled="disabled"  bind-to="summaryDes">
                                                          <option></option>
                                                        </select>
                                                      <label for="form_control_1">Order Destination</label>
                                                    </div>
                                                </div>

                                            		<div class="col-md-6">
                                            			 <div class="form-group form-md-line-input has-info">
	                                                   
                                                      <select class="form-control form-copy validate " id="form_control_1 "  name="resourceType[]"  bind-to="summaryRes" >
                                                            <option value="">[-Choose Truck Type-]</option>
                                                            <option value="30ton">30Ton</option>
                                                      </select>
	                                                    <label for="form_control_1">Truck Type</label>
                                                		</div>
                                            		</div>
                                                <div class="col-md-6">
                                                  <div class="form-group">
                                                    <label for="form_control_1">Expiry Date</label>
                                                    <div class="input-group input-medium date date-picker" data-date-format="dd-mm-yyyy" data-date-start-date="+1d" style="width: 100% !important">
                                                            <input type="text" name="expiryDate" id="expiryDate" class="form-control  form-copy validate" readonly  bind-to="expiryDate">
                                                            <span class="input-group-btn">
                                                                <button class="btn default" type="button">
                                                                    <i class="fa fa-calendar"></i>
                                                                </button>
                                                            </span>
                                                    </div>
                                                  </div>
                                                </div>
                                            		<div class="col-md-6">
                                            			 <div class="form-group form-md-line-input has-info">
	                                                    <select class="form-control form-copy validate" id="form_control_1 " name="resourceNumber[]"  bind-to="summaryNum">

                                                          <option value="">[-Choose Number of Trucks-]</option>
                                                          <option value="1">1</option>
                                                          <option value="2">2</option>
                                                          <option value="3">3</option>
                                                          <option value="5">5</option>
                                                          <option value="6">6</option>
                                                          <option value="7">7</option>
                                                          <option value="8">8</option>
                                                          <option value="9">9</option>
                                                          <option value="10">10</option>

                                                      </select>
	                                                    <label for="form_control_1">Number of Trucks</label>
                                                		</div>
                                            		</div>
                                                
                                                <div class="col-md-6">
                                                  <div class="form-group">
                                                    <label for="form_control_1">Pickup Date</label>
                                                    <div class="input-group input-medium date date-picker" data-date-format="dd-mm-yyyy" data-date-start-date="+0d" style="width: 100% !important">
                                                            <input type="text" name="pickupDate" id="pickupDate" class="form-control  form-copy validate" readonly  bind-to="summaryPickupDate">
                                                            <span class="input-group-btn">
                                                                <button class="btn default" type="button">
                                                                    <i class="fa fa-calendar"></i>
                                                                </button>
                                                            </span>
                                                    </div>
                                                  </div>
                                                </div>


                                                <div class="col-md-6">
                                                   <div class="form-group form-md-line-input has-info">
                                                      <select class="form-control form-copy validate" id="pickupTime" name="pickupTime"  bind-to="summaryPickTimeWindow">
                                                          <option value="">[-Pick Up Time-]</option>
                                                            <option value="0000Hrs - 0600Hrs">0000Hrs - 0600Hrs</option>
                                                            <option value="0600Hrs - 1200Hrs">0600Hrs - 1200Hrs</option>
                                                            <option value="1200Hrs - 1800Hrs">1200Hrs - 1800Hrs</option>
                                                            <option value="1800Hrs - 0600Hrs">1800Hrs - 0600Hrs</option>
                                                      </select>
                                                      <label for="form_control_1">Pickup Time Window</label>
                                                    </div>
                                                </div>
                                                
                                                <div class="col-md-6">
                                                  <div class="form-group form-md-line-input has-info">
                                                      <input class="form-control form-copy validate" name="contactName" id="contactName"  bind-to="summaryContactPerson">
                                                        
                                                      <label for="form_control_1">Contact Person</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                  <div class="form-group form-md-line-input has-info">
                                                      <input class="form-control form-copy  validate" name="contactPhone"  bind-to="summaryContactnum">
                                                        
                                                      <label for="form_control_1">Contact Number</label>
                                                    </div>
                                                </div>


                                            		


                                            	</div>
                                            <input class="haulageType" type="hidden" id="haulageType" value="0">
                                            <input class="haulageType" type="hidden" id="requestType" value="0">
                                            </div>
                                            <div class="form-actions noborder">
                                                <button type="button" class="btn btn-primary validate-form-button" form-target="order-form" data-toggle="modal" href="#order_summary">Save and Continue</button>
                                                <!-- <button type="button" class="btn btn-danger">Back</button> -->
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        
                    </div>
                    <!-- END CONTENT BODY -->
                </div>
                <!-- END CONTENT -->
                <!-- BEGIN QUICK SIDEBAR -->
              
                <!-- END QUICK SIDEBAR -->
            </div>
            <!-- END CONTAINER -->
            <!-- BEGIN FOOTER -->
            <div class="page-footer">
                <div class="page-footer-inner"> 2018 &copy; Truckka  &nbsp;|&nbsp;
                   
                </div>
                <div class="scroll-to-top">
                    <i class="icon-arrow-up"></i>
                </div>
            </div>
            <!-- END FOOTER -->
        </div>
        <!-- BEGIN QUICK NAV -->
        
        <div class="quick-nav-overlay"></div>
 @include('client.includes.footer')
