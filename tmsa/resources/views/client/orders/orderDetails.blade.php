<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>TMSC</title>
    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css" href="{{ url('css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ url('css/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('css/indexupdate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('css/tmsa.css') }}">
    <link href="{{ url('https://fonts.googleapis.com/css?family=Titillium+Web|Concert+One|Josefin+Sans|Roboto') }}" rel="stylesheet">
    <script type="text/javascript" src="{{ url('js/admin_dashboard.js')}}"></script>
    <script type="text/javascript" src="{{ url('js/jquery-3.2.1.js')}}"></script>
    <script type="text/javascript" src="{{ url('js/bootstrap.js')}}"></script>
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web|Concert+One|Josefin+Sans|Roboto" rel="stylesheet">


</head>
<body class="col-md-12-col-sm-6 col-xs-12" style="padding:0; height: auto;">
<div class="row" style="height:100vh; margin-right:0; margin-left:0;">
<div class="side_bar col-md-3">
    <div class=" identifier hidden-xs hidden-sm" >
        <span class="navbar-brand type" >T</span>
        <span class="navbar-brand type">M</span>
        <span class="navbar-brand type">S</span>
        <span class="navbar-brand type">C</span>
    </div>
    <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed pull-left" data-toggle="collapse" data-target="#hidden_bar" aria-expanded="false" style="border: 2px solid #fff ; margin-left:10px;">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar" style="border:1px solid #fff"></span>
            <span class="icon-bar" style="border:1px solid #fff"></span>
            <span class="icon-bar" style="border:1px solid #fff"></span>
        </button>
        <a class="navbar-brand" href="#">
            <div class="brand">
                <span class="navbar-brand  logoT" style="border: 1px solid #ffffff; margin-left: 2px; border-radius: 10px 0 0 0; font-family: 'Concert One', cursive; color: #ffffff" >T</span>
                <span class="navbar-brand  logoR" style="border: 1px solid #ffffff;  margin-left: 2px; font-family: 'Concert One', cursive; background: #ffffff; color: #3baba0;">R</span>
                <span class="navbar-brand   logoU" style="border: 1px solid #ffffff;  margin-left: 2px; font-family: 'Concert One', cursive; color: #ffffff;">U</span>
                <span class="navbar-brand   logoC" style="border: 1px solid #ffffff;  margin-left: 2px;font-family: 'Concert One', cursive; background: #ffffff; color: #3baba0;">C</span>
                <span class="navbar-brand  logoK" style="border: 1px solid #ffffff;  margin-left: 2px;font-family: 'Concert One', cursive; color: #ffffff"; >K</span>
                <span class="navbar-brand   logoK" style="border: 1px solid #ffffff;  margin-left: 2px;font-family: 'Concert One', cursive; background: #ffffff; color: #3baba0;">K</span>
                <span class="navbar-brand   logoA" style="border: 1px solid #ffffff;  margin-left: 2px; border-radius: 0 0 10px 0;font-family: 'Concert One', cursive; color: #ffffff;">A</span>
            </div>
        </a>
    </div>
    <!--NAVBAR FOR MOBIE SCREENS-->
    <ul class="nav navbar-nav collapse" id="hidden_bar">
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">My Orders <span class="glyphicon glyphicon-file pull-right"></span></a>
            <ul class="dropdown-menu">
                <li>
                    <a href="{{url('/home')}}">Create New Order <span class="glyphicon glyphicon-plus-sign pull-right"></span></a>
                </li>
                <li class="divider"></li>
                <li><a href="{{ url('/viewOrders') }}">View My Orders <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                <li class="divider"></li>
            </ul>
        </li>

        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Financial Management <span class="fa fa-dollar pull-right"></span></a>
            <ul class="dropdown-menu">
                <li>
                    <a href="{{url('/allInvoices')}}">My Invoices <span class="glyphicon glyphicon-plus-sign pull-right"></span></a>
                </li>
                <li class="divider"></li>
                <li><a href="{{url('/demurrage')}}">Demurrage <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                <li class="divider"></li>
                <li><a href="{{url('/settlement')}}">Settlement <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                <li class="divider"></li>
            </ul>
        </li>

        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Documentation <span class="fa fa-file-open pull-right"></span></a>
            <ul class="dropdown-menu">
                <li>
                    <a href="{{url('/allOrders')}}">All Orders<span class="glyphicon glyphicon-plus-sign pull-right"></span></a>
                </li>
                <li class="divider"></li>

            </ul>
        </li>



        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Performance Management <span class="fa fa-line-chart pull-right"></span></a>
            <ul class="dropdown-menu">
                <li>
                    <a href="{{url('/performance')}}">Finance<span class="fa fa-dollar pull-right"></span></a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="{{url('/performance')}}">Haulage<span class="glyphicon glyphicon-shopping-cart pull-right"></span></a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="{{url('/performance')}}">Time<span class="glyphicon glyphicon-time pull-right"></span></a>
                </li>
                <li class="divider"></li>

            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Track and Trace <span class="glyphicon glyphicon-map-marker pull-right"></span></a>
            <ul class="dropdown-menu">
                <li><a href='{{ url("/allGPS") }}'>Track Orders <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                <li class="divider"></li>
            </ul>
        </li>



        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Documentation <span class="fa fa-file-open pull-right"></span></a>
            <ul class="dropdown-menu">
                <li>
                    <a href="{{url('/allOrders')}}">All Orders<span class="glyphicon glyphicon-plus-sign pull-right"></span></a>
                </li>
                <li class="divider"></li>

            </ul>
        </li>

        <li class="dropdown">
            <a href="{{ route('logout') }}"
               onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();" class="dropdown-toggle" data-toggle="dropdown">Log Out <span class="glyphicon glyphicon-log-out pull-right"></span></a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </li>



    </ul>
    <!--NAVBAR FOR laptop SCREENS-->
    <ul class="nav navbar-nav hidden-xs hidden-sm">

        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">My Orders <span class="glyphicon glyphicon-file pull-right"></span></a>
            <ul class="dropdown-menu">
                <li>
                    <a href="{{url('/home')}}">Create New Order <span class="glyphicon glyphicon-plus-sign pull-right"></span></a>
                </li>
                <li class="divider"></li>
                <li><a href="{{ url('/viewOrders') }}">View My Orders <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                <li class="divider"></li>
            </ul>
        </li>

        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Financial Management <span class="fa fa-dollar pull-right"></span></a>
            <ul class="dropdown-menu">
                <li>
                    <a href="{{url('/allInvoices')}}">My Invoices <span class="glyphicon glyphicon-plus-sign pull-right"></span></a>
                </li>
                <li class="divider"></li>
                <li><a href="{{url('/demurrage')}}">Demurrage <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                <li class="divider"></li>
                <li><a href="{{url('/settlement')}}">Settlement <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                <li class="divider"></li>
            </ul>
        </li>

        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Documentation <span class="fa fa-file-open pull-right"></span></a>
            <ul class="dropdown-menu">
                <li>
                    <a href="{{url('/allOrders')}}">All Orders<span class="glyphicon glyphicon-plus-sign pull-right"></span></a>
                </li>
                <li class="divider"></li>

            </ul>
        </li>



        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Performance Management <span class="fa fa-line-chart pull-right"></span></a>
            <ul class="dropdown-menu">
                <li>
                    <a href="{{url('/performance')}}">Finance<span class="fa fa-dollar pull-right"></span></a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="{{url('/performance')}}">Haulage<span class="glyphicon glyphicon-shopping-cart pull-right"></span></a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="{{url('/performance')}}">Time<span class="glyphicon glyphicon-time pull-right"></span></a>
                </li>
                <li class="divider"></li>

            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Track and Trace <span class="glyphicon glyphicon-map-marker pull-right"></span></a>
            <ul class="dropdown-menu">
                <li><a href='{{ url("/allGPS") }}'>Track Orders <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                <li class="divider"></li>
            </ul>
        </li>



        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Documentation <span class="fa fa-file-open pull-right"></span></a>
            <ul class="dropdown-menu">
                <li>
                    <a href="{{url('/allOrders')}}">All Orders<span class="glyphicon glyphicon-plus-sign pull-right"></span></a>
                </li>
                <li class="divider"></li>

            </ul>
        </li>

        <li class="dropdown">
            <a href="{{ route('logout') }}"
               onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();" class="dropdown-toggle" data-toggle="dropdown">Log Out <span class="glyphicon glyphicon-log-out pull-right"></span></a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </li>

    </ul>
</div>
<!-- TSMA VIEW ORDERS PAGE -->
<div class="tmsa_main_body col-md-9" style="height: 100vh; background-color: transparent;" >
    <div class="row" style="height: 100vh; background-color: #fff;">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  toppad" >
            <div class="panel" style=" font-family: 'Josefin Sans', sans-serif; background: #ffffff; border: 2px solid #3baba0; margin-top:50px;">
                <div class="panel-heading" style="background: #3baba0; color:#000;">
                    <h3 class="panel-title">Order Details</h3>
                    <!session message starts-->

                    @if(session('message'))
                    <p>
                        {{ session('message') }}
                    </p>
                    @endif

                    <!session message ends-->
                    <div class="description">
                        <div class="alert alert-danger print-error-msg" style="display:none">

                            <ul></ul>
                        </div>

                        <div class="alert alert-success print-success-msg" style="display:none">

                            <ul></ul>
                        </div>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="row">

                        <div class=" col-md-12 col-lg-12">
                            <h4 align="center" class="btn-lg" style=" font-family: 'Josefin Sans', sans-serif; margin-left: 5%; margin-right: 5%; background: #ffffff; border: 2px solid #3baba0;">Order Details</h4>
                            <table class="table table-user-information">

                                <tbody>
                                @foreach($orderDetails as $orderDetail)
                                <tr>
                                    <td>Service ID No:</td>
                                    <td>{{ $orderDetail->serviceIDNo }}</td>
                                </tr>
                                <tr>
                                    <td>Value Of Haulage:</td>
                                    <td> {{ $orderDetail->valueOfHaulage }}</td>
                                </tr>
                                <tr>
                                    <td>Origin:</td>
                                    <td>{{ $orderDetail->deliverFrom }}</td>
                                </tr>
                                <tr>
                                    <td>Destination:</td>
                                    <td>{{$orderDetail->deliverTo}}</td>
                                </tr>
                                <tr>
                                    <td>Item Description:</td>
                                    <td>{{$orderDetail->itemDescription}}</td>
                                </tr>
                                <tr>
                                    <td>Approximate Weight:</td>
                                    <td>{{$orderDetail->estimatedWgt}}</td>
                                </tr>
                                <tr>
                                    <td>Contact Name:</td>
                                    <td>{{$orderDetail->contactName}}</td>
                                </tr>
                                <tr>
                                    <td>Contact Name:</td>
                                    <td>{{$orderDetail->contactPhone}}</td>
                                </tr>
                                <tr>
                                    <td>Pick Up Date:</td>
                                    <td>{{$orderDetail->pickUpDate}}</td>
                                </tr>
                                <tr>
                                    <td>Pick Up Time:</td>
                                    <td>{{$orderDetail->pickUpTime}}</td>
                                </tr>
                                <tr>
                                    <td>Request Type:</td>
                                    <td>
                                        @if($orderDetail->typeOfHaulage == 0)
                                        Standard
                                        @elseif($orderDetail->typeOfHaulage == 1)
                                        Non Standard
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td>Order Status:</td>
                                    <td>
                                        @if($orderDetail->orderStatus == 0)
                                        Pending(Under Review)
                                        @elseif($orderDetail->orderStatus == 1)
                                        Review Passed (Pending)
                                        @elseif($orderDetail->orderStatus == 2)
                                        Processing
                                        @elseif($orderDetail->orderStatus==3)
                                        Processing
                                        @elseif($orderDetail->orderStatus==4)
                                        Processing
                                        @elseif($orderDetail->orderStatus == 5)
                                        Invoice Created
                                        @elseIf($orderDetail->orderStatus == 6)
                                        Invoice Approved
                                        @elseif($orderDetail->orderStatus == 7)
                                        Carrier Mobilized
                                        @elseif($orderDetail->orderStatus == 8)
                                        Carrier Clocked In
                                        @elseif($orderDetail->orderStatus == 9)
                                        Carrier Clocked Out
                                        @elseif($orderDetail->orderStatus == 10)
                                        Transaction Closed
                                        @endif
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            @endforeach
                        </div>

                    </div>
                </div>
                <div class="panel-footer" style="background: #3baba0; color:#000;">
                    <!--  <a data-original-title="Broadcast Message" data-toggle="tooltip" type="button" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-envelope"></i></a>
                     <span class="pull-right">
                         <a href="edit.html" data-original-title="Edit this user" data-toggle="tooltip" type="button" class="btn btn-sm btn-warning"><i class="glyphicon glyphicon-edit"></i></a>
                         <a data-original-title="Remove this user" data-toggle="tooltip" type="button" class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-remove"></i></a>
                     </span> -->

                    <!--<span class=" text-info panel_foot" style=" color:#000;" > <a style=" color:#000;" href="edit.html" >Edit Profile</a>&nbsp; &nbsp;&nbsp;May 05,2014,03:00 pm <a style=" color:#000;" href="edit.html" style="margin-left: 90%;">Logout</a></span>-->


                </div>

            </div>
        </div>

        </div>
</div>



    <div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                    <h4 class="modal-title custom_align" id="Heading">Delete this entry</h4>
                </div>
                <div class="modal-body">

                    <div class="alert alert-danger"><span class="glyphicon glyphicon-warning-sign"></span> Are you sure you want to delete this Record?</div>

                </div>
                <div class="modal-footer ">
                    <button type="button" class="btn btn-success" ><span class="glyphicon glyphicon-ok-sign"></span> Yes</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> No</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

</div>
<!--     ************************************************** -->
<!--BOTTOM NAV BAR-->

<script type="text/javascript" src="{{url('js/tmsa.js')}}"></script>
</body>
</html>

