<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>TMSC</title>
    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css" href="{{ url('css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ url('css/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('css/indexupdate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('css/tmsa.css') }}">
    <link href="{{ url('https://fonts.googleapis.com/css?family=Titillium+Web|Concert+One|Josefin+Sans|Roboto') }}" rel="stylesheet">
    <script type="text/javascript" src="{{ url('js/admin_dashboard.js')}}"></script>
    <script type="text/javascript" src="{{ url('js/jquery-3.2.1.js')}}"></script>
    <script type="text/javascript" src="{{ url('js/bootstrap.js')}}"></script>
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web|Concert+One|Josefin+Sans|Roboto" rel="stylesheet">


</head>
<body class="col-md-12-col-sm-6 col-xs-12" style="padding:0; background-color: #3baba0;">
<div class="row" style="height:100vh; margin-right:0; margin-left:0;">
<div class="side_bar col-md-3">
    <div class=" identifier hidden-xs hidden-sm" >
        <span class="navbar-brand type" >T</span>
        <span class="navbar-brand type">M</span>
        <span class="navbar-brand type">S</span>
        <span class="navbar-brand type">C</span>
    </div>
    <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed pull-left" data-toggle="collapse" data-target="#hidden_bar" aria-expanded="false" style="border: 2px solid #fff ; margin-left:10px;">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar" style="border:1px solid #fff"></span>
            <span class="icon-bar" style="border:1px solid #fff"></span>
            <span class="icon-bar" style="border:1px solid #fff"></span>
        </button>
        <a class="navbar-brand" href="#">
            <div class="brand">
                <span class="navbar-brand  logoT" style="border: 1px solid #ffffff; margin-left: 2px; border-radius: 10px 0 0 0; font-family: 'Concert One', cursive; color: #ffffff" >T</span>
                <span class="navbar-brand  logoR" style="border: 1px solid #ffffff;  margin-left: 2px; font-family: 'Concert One', cursive; background: #ffffff; color: #3baba0;">R</span>
                <span class="navbar-brand   logoU" style="border: 1px solid #ffffff;  margin-left: 2px; font-family: 'Concert One', cursive; color: #ffffff;">U</span>
                <span class="navbar-brand   logoC" style="border: 1px solid #ffffff;  margin-left: 2px;font-family: 'Concert One', cursive; background: #ffffff; color: #3baba0;">C</span>
                <span class="navbar-brand  logoK" style="border: 1px solid #ffffff;  margin-left: 2px;font-family: 'Concert One', cursive; color: #ffffff"; >K</span>
                <span class="navbar-brand   logoK" style="border: 1px solid #ffffff;  margin-left: 2px;font-family: 'Concert One', cursive; background: #ffffff; color: #3baba0;">K</span>
                <span class="navbar-brand   logoA" style="border: 1px solid #ffffff;  margin-left: 2px; border-radius: 0 0 10px 0;font-family: 'Concert One', cursive; color: #ffffff;">A</span>
            </div>
        </a>
    </div>
    <!--NAVBAR FOR MOBIE SCREENS-->
    <ul class="nav navbar-nav collapse" id="hidden_bar">
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">My Orders <span class="glyphicon glyphicon-file pull-right"></span></a>
            <ul class="dropdown-menu">
                <li>
                    <a href="{{url('/home')}}">Create New Order <span class="glyphicon glyphicon-plus-sign pull-right"></span></a>
                </li>
                <li class="divider"></li>
                <li><a href="{{ url('/viewOrders') }}">View My Orders <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                <li class="divider"></li>
            </ul>
        </li>

        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Financial Management <span class="fa fa-dollar pull-right"></span></a>
            <ul class="dropdown-menu">
                <li>
                    <a href="{{url('/allInvoices')}}">My Invoices <span class="glyphicon glyphicon-plus-sign pull-right"></span></a>
                </li>
                <li class="divider"></li>
                <li><a href="{{url('/demurrage')}}">Demurrage <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                <li class="divider"></li>
                <li><a href="{{url('/settlement')}}">Settlement <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                <li class="divider"></li>
            </ul>
        </li>

        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Documentation <span class="fa fa-file-open pull-right"></span></a>
            <ul class="dropdown-menu">
                <li>
                    <a href="{{url('/allOrders')}}">All Orders<span class="glyphicon glyphicon-plus-sign pull-right"></span></a>
                </li>
                <li class="divider"></li>

            </ul>
        </li>



        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Performance Management <span class="fa fa-line-chart pull-right"></span></a>
            <ul class="dropdown-menu">
                <li>
                    <a href="{{url('/performance')}}">Finance<span class="fa fa-dollar pull-right"></span></a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="{{url('/performance')}}">Haulage<span class="glyphicon glyphicon-shopping-cart pull-right"></span></a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="{{url('/performance')}}">Time<span class="glyphicon glyphicon-time pull-right"></span></a>
                </li>
                <li class="divider"></li>

            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Track and Trace <span class="glyphicon glyphicon-map-marker pull-right"></span></a>
            <ul class="dropdown-menu">
                <li><a href='{{ url("/allGPS") }}'>Track Orders <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                <li class="divider"></li>
            </ul>
        </li>



        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Documentation <span class="fa fa-file-open pull-right"></span></a>
            <ul class="dropdown-menu">
                <li>
                    <a href="{{url('/allOrders')}}">All Orders<span class="glyphicon glyphicon-plus-sign pull-right"></span></a>
                </li>
                <li class="divider"></li>

            </ul>
        </li>

        <li class="dropdown">
            <a href="{{ route('logout') }}"
               onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();" class="dropdown-toggle" data-toggle="dropdown">Log Out <span class="glyphicon glyphicon-log-out pull-right"></span></a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </li>



    </ul>
    <!--NAVBAR FOR laptop SCREENS-->
    <ul class="nav navbar-nav hidden-xs hidden-sm">
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">My Orders <span class="glyphicon glyphicon-file pull-right"></span></a>
            <ul class="dropdown-menu">
                <li>
                    <a href="{{url('/home')}}">Create New Order <span class="glyphicon glyphicon-plus-sign pull-right"></span></a>
                </li>
                <li class="divider"></li>
                <li><a href="{{ url('/viewOrders') }}">View My Orders <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                <li class="divider"></li>
            </ul>
        </li>

        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Financial Management <span class="fa fa-dollar pull-right"></span></a>
            <ul class="dropdown-menu">
                <li>
                    <a href="{{url('/allInvoices')}}">My Invoices <span class="glyphicon glyphicon-plus-sign pull-right"></span></a>
                </li>
                <li class="divider"></li>
                <li><a href="{{url('/demurrage')}}">Demurrage <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                <li class="divider"></li>
                <li><a href="{{url('/settlement')}}">Settlement <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                <li class="divider"></li>
            </ul>
        </li>

        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Documentation <span class="fa fa-file-open pull-right"></span></a>
            <ul class="dropdown-menu">
                <li>
                    <a href="{{url('/allOrders')}}">All Orders<span class="glyphicon glyphicon-plus-sign pull-right"></span></a>
                </li>
                <li class="divider"></li>

            </ul>
        </li>



        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Performance Management <span class="fa fa-line-chart pull-right"></span></a>
            <ul class="dropdown-menu">
                <li>
                    <a href="{{url('/performance')}}">Finance<span class="fa fa-dollar pull-right"></span></a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="{{url('/performance')}}">Haulage<span class="glyphicon glyphicon-shopping-cart pull-right"></span></a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="{{url('/performance')}}">Time<span class="glyphicon glyphicon-time pull-right"></span></a>
                </li>
                <li class="divider"></li>

            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Track and Trace <span class="glyphicon glyphicon-map-marker pull-right"></span></a>
            <ul class="dropdown-menu">
                <li><a href='{{ url("/allGPS") }}'>Track Orders <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
                <li class="divider"></li>
            </ul>
        </li>



        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Documentation <span class="fa fa-file-open pull-right"></span></a>
            <ul class="dropdown-menu">
                <li>
                    <a href="{{url('/allOrders')}}">All Orders<span class="glyphicon glyphicon-plus-sign pull-right"></span></a>
                </li>
                <li class="divider"></li>

            </ul>
        </li>

        <li class="dropdown">
            <a href="{{ route('logout') }}"
               onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();" class="dropdown-toggle" data-toggle="dropdown">Log Out <span class="glyphicon glyphicon-log-out pull-right"></span></a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </li>

    </ul>
</div>
<!-- TSMA VIEW ORDERS PAGE -->
    <div class="tmsa_main_body col-md-9 col-xs-12 col-sm-12" style="padding-top:0%; height:100vh">
        <!session message starts-->

        @if(session('message'))
        <p>
            {{ session('message') }}
        </p>
        @endif

        <!session message ends-->

        <div style="margin:5% 0% 5% 0%;float:left; width:100%; box-shadow:0 0 3px #aaa; height:auto;color:#666;">
            <div style="width:100%; padding:10px; float:left; background:#3baba0; color:#232323; font-size:20px; text-align:center;">
                <h2 class="invoice_header">Southern Streams Universal Services Invoice</h2>
            </div>
        </div>
        <div class="row invoice_body text-uppercase">
            <div class="col-sm-6">
                <div class="table-responsive">
                    <table class="table table-condensed">
                    <legend>Resource Details</legend>
                        <thead>
                        <tr>
                            <td><strong>Plate Number</strong></td>
                            <td class="text-center"><strong>Driver Name</strong></td>
                        </tr>
                        </thead>
                        <tbody>
                        <!-- foreach ($order->lineItems as $line) or some such thing here -->
                        @foreach($resGPSDet as $r)
                        <tr>
                            <td>{{ $r->plateNumber }}</td>
                            <td class="text-center">{{ $r->driverName }}</td>
                        </tr>
                        @endforeach
                        </tbody>
                     </table>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="table-responsive">
                    <table class="table table-condensed">
                    <legend>Make Payments To:</legend>
                        <tbody>
                            <tr class="text-center text-capitalize"><strong>Bank: FCMB PLC</strong></tr><br/>
                            <tr class="text-center text-capitalize"><strong>AC Name: SOUTHERN STREAMS UNIVERSAL SERVICES</strong></tr><br/>
                            <tr class="text-center text-capitalize"><strong>AC NO: 2146 319 032</strong></tr>
                        </tbody>
                     </table>
                </div>
            </div>
        </div>

        <div class="row invoice_body text-uppercase">
            <div class="col-sm-6">
                <address>
                    <strong>Order No:</strong>
                    {{$serviceId}}
                    <br><strong>Invoice No:</strong>
                    {{$invoice}}
                </address>
            </div>
        </div>
        <div class="row invoice_body text-uppercase">
            <div class="col-sm-6">
                <address>
                    <strong>Billed To:</strong><br>
                    {{$billedTo}}<br>{{$billedToRc}}
                </address>
            </div>
            <div class="col-sm-6 text-right">
                <address>
                    <strong>Origin:</strong><br>

                    {{$shippedFrom}}<br>
                    <strong>Destination:</strong><br>

                    {{$shippedTo}}
                </address>
            </div>
        </div>
        <div class="row invoice_body text-uppercase">
            <div class="col-sm-6">
                <address>
                    <strong>Weight(Tonnes):</strong><br>
                    {{$wgt}}
                </address>
                <address>
                    <strong>Item Description:</strong><br>
                    {{$desc}}
                </address>
            </div>
            <div class="col-sm-6 text-right">
                <address>
                    <strong>Order Date:</strong><br>
                   {{$dateCreated}}<br><br>
                </address>
            </div>
        </div>

        <div class="row invoice_body text-uppercase">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><strong>Order summary</strong></h3>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-condensed">
                                <thead>
                                <tr>
                                    <td><strong>Resource Type</strong></td>
                                    <td class="text-center"><strong>Description</strong></td>
                                    <td class="text-center"><strong>Quantity</strong></td>
                                </tr>
                                </thead>
                                <tbody>
                                <!-- foreach ($order->lineItems as $line) or some such thing here -->
                                @foreach($resourceTypeNumber as $carrResDet)
                                <tr>
                                    <td>{{ $carrResDet->resourceType }}</td>
                                    <td class="text-center">{{ $description }}</td>
                                    <td class="text-center">{{ $carrResDet->resourceNumber }}</td>
                                </tr>
                                @endforeach
                                </tbody>
                             </table>
                        </div>
                        <div class="row">
                        <div style="padding-left:950px;">
                        
                        <h4><b>Gross</b>: &#8358;{{$totalAmount}}</h4>
                        <h4><b>VAT(5%)</b>: &#8358;{{$vatVal}}</h4>
                        <div style="height:1px;background-color:black;width:200px;"></div>
                        <h4><b>Total</b>: &#8358;{{$amt}}</h4>
                        </div>
                        
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


</div>
<!--     ************************************************** -->
<!--BOTTOM NAV BAR-->

<script type="text/javascript" src="{{url('js/tmsa.js')}}"></script>
</body>
</html>

