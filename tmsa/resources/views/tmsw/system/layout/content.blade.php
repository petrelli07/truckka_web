<head>
 
    <meta charset="utf-8">
    <title>Client Area</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="title" content="">
    <meta name="author" content="">
    <link id="bs-css" href="{{URL::to('dashboard_assets/assets/css/bootstrap-cerulean.min.css')}}" rel="stylesheet">
    <link href="{{URL::to('dashboard_assets/assets/css/charisma-app.css')}}" rel="stylesheet">
    <link href="{{URL::to('dashboard_assets/assets/css/colorbox.css')}}" rel="stylesheet">
    
    <link href="{{URL::to('dashboard_assets/assets/css/custom-style.css')}}" rel="stylesheet">
    
    <link href="{{URL::to('dashboard_assets/assets/css/chosen.min.css')}}" rel="stylesheet">
     <link href="{{URL::to('dashboard_assets/assets/css/DateTimePicker.min.css')}}" rel="stylesheet">
     
      <!-- jQuery -->
      <script src="{{URL::to('dashboard_assets/assets/js/jquery.min.js')}}"></script>
    <script src="{{URL::to('exchange/js/currency_formatter.js')}}"></script>
    <script type = "text/javascript" src = "https://www.gstatic.com/charts/loader.js">
    </script>


    <script>
        $(document).ready(function() {
            $('#standardOrigin').on('change', function() {

                var val = this.value;
                var _token = $("input[name='_token']").val();

                document.getElementById('origin2').value = val;
                var original = document.getElementById('origin2').value;

                if (original === '') {
                    alert('Select a location');
                }else if(original !== '')
                {


                    var APP_URL = {!! json_encode(url('/tmsw/')) !!}

                $.ajax({

                    url: APP_URL+"/checkOrigins",

                    type:'POST',

                    data: {_token:_token, originToServer:original},

                    success: function(data) {

                        if($.isEmptyObject(data.error)){

                            var length = data.success;
                            var trueLength = length.length;

                            if(trueLength > 0){
                                var catOptions = "";
                                for(var i = 0; i < trueLength; i++) {

                                    var company = data.success[i];

                                    catOptions += "<option value="+ company +">" + company + "</option>";
                                }

                                document.getElementById("destinationSelect").innerHTML = catOptions;

                            }else{
                                alert("nothing");
                            }



                        }else{

                            alert('error');

                        }

                    }

                });
            }
        })
        });
    </script>

    <script type = "text/javascript">
        google.charts.load('current', {packages: ['corechart']});
    </script>

  
    <!-- The fav icon -->
    <link rel="shortcut icon" href="">

</head>



<!--//////////////////////// BEGINING OF HEADER MENU ///////////////////////////////// -->

<div class="navbar navbar-default" role="navigation">

<div class="navbar-inner">
    <button type="button" class="navbar-toggle pull-left animated flip">
    <span class="sr-only">Toggle navigation</span> 
  <img src="{{URL::to('dashboard_assets/assets/images/logo-transparent.png')}}"  class="pull-left" style="width:120px;margin-left:30px;" alt="Truckka"/> </a>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href=""> 
    <img src="{{URL::to('dashboard_assets/assets/images/logo-transparent.png')}}" class="hidden-xs" style="" alt="Truckka"/> </a>

    <!-- user dropdown starts -->
    <!--<div class="btn-group pull-right">
        <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
            <i class="glyphicon glyphicon-user"></i><span class="hidden-sm hidden-xs"> 
            </span>
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu">
        
            <li><a href="#">Profile</a></li>
            <li class="divider"></li>
            <li><a href="#">Edit Profile</a></li>
            <li class="divider"></li>
            <li><a href="#">Change password</a></li>
            <li class="divider"></li>
            <li><a href="logout"> Logout</a></li>
        </ul>
    </div>-->
    <!-- user dropdown ends -->

    <!-- theme selector starts -->

    <!-- theme selector ends -->

    <!--<ul class="collapse navbar-collapse nav navbar-nav top-menu">
        <li><a href="#" target="_blank"><i class="glyphicon glyphicon-star"></i> Visit Site</a></li>
        <li class="dropdown">
            <a href="#" data-toggle="dropdown"><i class="glyphicon glyphicon-cog"></i> Quick Links <span
                    class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
                <li><a href="#">Dashboard</a></li>
                <li><a href="#">View Users</a></li>
                <li class="divider"></li>
                <li><a href="logout">Logout</a></li>
            </ul>
        </li>
        <li>
            <form class="navbar-search pull-left">
                <input placeholder="Search" class="search-query form-control col-md-10" name="query"
                       type="text">
            </form>
        </li>
    </ul>-->

</div>
</div>
<!--////////////////////////// END OF HEADER MENU //////////////////////////////////////////////-->





<div class="ch-container">
    <div class="row">
    <!--/////////////////////////////////// BEGIGNING OF LEFT MENU //////////////////////////////// -->
<div class="col-sm-2 col-lg-2 admin-menu">
            <div class="sidebar-nav">
                <div class="nav-canvas">
                    <div class="nav-sm nav nav-stacked">

                    </div>
                    <ul class="nav nav-pills nav-stacked main-menu">
                       
                        <li><a class="ajax-link" href="#"><i class="glyphicon glyphicon-dashboard"></i><span> Dashboard</span></a> </li>
                        
                       <!-- <div class="admin-menu">-->
                       <li class="accordion">
                            <a href="#"><i class="glyphicon glyphicon-briefcase"></i><span> Orders</span></a>
                            <ul class="nav nav-pills nav-stacked">
                                <li><a href="{{url('/tmsw/superAdministrator/orders')}}">All Orders</a></li>
                                <li><a href="{{url('/tmsw/superAdministrator/completed_orders')}}">Completed Orders</a></li>
                            </ul>
                        </li>

                        <li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"  class="ajax-link" href="#"><i class="glyphicon glyphicon-off"></i><span> Logout </span></a> </li>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>

                      <!--</div>-->
 
                    </ul>
                      
                </div>
            </div>
        </div>
        

    <!--/////////////////////////////////////// END OF IT .//////////////////////////////////////////-->
   
   
   
   
    <div id="content" class="col-lg-10 col-sm-10">
                <!-- content starts -->
        <div>
          <ul class="breadcrumb">
              <li>
                  <a href="#">Dashboard</a>
              </li>
              <li>
                  <a href="#" class="breadcrumb-child">{{$breadcrumb}}</a>
              </li>
            
          </ul>
        </div>
        @yield('content')	
	</div>
	</div>
    @extends('tmsw.client.layout.footer')
</div>