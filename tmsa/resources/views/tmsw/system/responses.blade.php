@extends('tmsw.system.layout.content')
@section('content')


<div class="row" xmlns="http://www.w3.org/1999/html">
    <div class="box col-md-12">

        <!--if @ session -->
        @if (session('message'))
        <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert">x</button>
            {{ session('message') }}
        </div>
        @endif

        <!--endif-->

        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-plus"></i> View Truck Details</h2>

            </div>
            <div class="box-content row">


                <div class="box col-md-12">
                    <div>
                        <a href='{{url("/tmsw/superAdministrator/add_Responses/{$reference}")}}' class="btn btn-sm btn-info">Add Responses</a>
                    </div>
                    <div class="box-content">

                        <table class="table table-striped table-bordered  responsive">
                            <thead>
                            <tr>
                                <th>Select</th>
                                <th>Plate Number</th>
                                <th>Driver Name</th>
                                <th>Driver Phone</th>
                                <th>Company Name</th>
                            </tr>
                            </thead>
                            <form method="post" action="{{url('/tmsw/superadministrator/order/update_responses')}}">
                                {{csrf_field()}}
                                <input name="reference_number" type="hidden" value="{{$reference}}">
                            <tbody>
                            @forelse($trucks as $detail)
                            <tr>
                                
                                <td>
                                        <input name="response_id[]" value="{{$detail->id}}" type="checkbox">
                                </td>

                                <td class="center">{{$detail->plateNumber}}</td>
                                <td class="center">{{$detail->driverName}}</td>
                                <td class="center">{{$detail->driverPhone}}</td>
                                <td class="center">{{$detail->company_name}}</td>
                            </tr>
                            @empty
                            @endforelse
                            </tbody>
                        </table>
                        <button class="btn btn-sm btn-success" type="submit">Approve</button>
                        </form>
                    </div>

                </div>






            </div>
        </div>
    </div>
    <!--/span-->

</div><!--/row-->



@endsection