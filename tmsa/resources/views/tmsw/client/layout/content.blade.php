<head>
 
    <meta charset="utf-8">
    <title>Client Area</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="title" content="">
    <meta name="author" content="">
    <link id="bs-css" href="{{URL::to('dashboard_assets/assets/css/bootstrap-cerulean.min.css')}}" rel="stylesheet">
    <link href="{{URL::to('dashboard_assets/assets/css/charisma-app.css')}}" rel="stylesheet">
    <link href="{{URL::to('dashboard_assets/assets/css/colorbox.css')}}" rel="stylesheet">
    
    <link href="{{URL::to('dashboard_assets/assets/css/custom-style.css')}}" rel="stylesheet">
    
    <link href="{{URL::to('dashboard_assets/assets/css/chosen.min.css')}}" rel="stylesheet">
     <link href="{{URL::to('dashboard_assets/assets/css/DateTimePicker.min.css')}}" rel="stylesheet">
     
      <!-- jQuery -->
      <script src="{{URL::to('dashboard_assets/assets/js/jquery.min.js')}}"></script>
    <script src="{{URL::to('exchange/js/currency_formatter.js')}}"></script>
    <script type = "text/javascript" src = "https://www.gstatic.com/charts/loader.js">
    </script>

    <script>
        $(document).ready(function() {

            /*$('#freight_expenses').hide();
            $('#average_freight_expenses').hide();
            $('#number_of_trucks').hide();
            $('#freight_month').hide();*/

            $('#performance').on('change', function() {
                var val = this.value;
                if(val === "transporters"){
                    $('#freight_expenses').show();
                    $('#average_freight_expenses').hide();
                    $('#number_of_trucks').hide();
                    $('#freight_month').hide();
                }else if(val === "freight"){
                    $('#freight_expenses').hide();
                    $('#average_freight_expenses').show();
                    $('#number_of_trucks').hide();
                    $('#freight_month').hide();
                }else if(val === "ops"){
                    $('#freight_expenses').hide();
                    $('#average_freight_expenses').hide();
                    $('#number_of_trucks').show();
                    $('#freight_month').hide();
                }else if(val === "cost"){
                    $('#freight_expenses').hide();
                    $('#average_freight_expenses').hide();
                    $('#number_of_trucks').hide();
                    $('#freight_month').show();
                }else if(val === ''){
                    $('#freight_expenses').hide();
                    $('#average_freight_expenses').hide();
                    $('#number_of_trucks').hide();
                    $('#freight_month').hide();
                }
            });
        });
     </script>

    <script>
        $(document).ready(function() {
            $('#standardOrigin').on('change', function() {

                var val = this.value;
                var _token = $("input[name='_token']").val();

                document.getElementById('origin2').value = val;
                var original = document.getElementById('origin2').value;

                if (original === '') {
                    alert('Select a location');
                }else if(original !== '')
                {


                    var APP_URL = {!! json_encode(url('/tmsw/')) !!}

                $.ajax({

                    url: APP_URL+"/checkOrigins",

                    type:'POST',

                    data: {_token:_token, originToServer:original},

                    success: function(data) {

                        if($.isEmptyObject(data.error)){

                            var length = data.success;
                            var trueLength = length.length;

                            if(trueLength > 0){
                                var catOptions = "";
                                for(var i = 0; i < trueLength; i++) {

                                    var company = data.success[i];

                                    catOptions += "<option value="+ company +">" + company + "</option>";
                                }

                                document.getElementById("destinationSelect").innerHTML = catOptions;

                            }else{
                                alert("nothing");
                            }



                        }else{

                            alert('error');

                        }

                    }

                });
            }
        })
            $('#getRoles').on('change', function() {
                var val = this.value;
                var _token = $("input[name='_token']").val();

                var original = document.getElementById('getRoles').value;

                if (original === '') {
                    alert('Select a Group');
                }else if(original !== '')
                {


                    var APP_URL = {!! json_encode(url('/tmsw/')) !!}

                $.ajax({

                    url: APP_URL+"/group/roles",

                    type:'POST',

                    data: {_token:_token, groupId:original},

                    success: function(data) {

                    var catOptions="";

                    for(var i = 0; i < data.length; i++) {


                                    catOptions += "<option value="+ data[i].role_id +">" + data[i].role_name + "</option>";

                    }
                                    document.getElementById('destinationSelect2').innerHTML = catOptions;
                                // document.getElementById("roles").innerHTML = catOptions;

                    }

                });
            }
        })
        });
    </script>

    <script type = "text/javascript">
        google.charts.load('current', {packages: ['corechart']});
    </script>

  
    <!-- The fav icon -->
    <link rel="shortcut icon" href="">

</head>



<!--//////////////////////// BEGINING OF HEADER MENU ///////////////////////////////// -->

<div class="navbar navbar-default" role="navigation">

<div class="navbar-inner">
    <button type="button" class="navbar-toggle pull-left animated flip">
    <span class="sr-only">Toggle navigation</span> 
  <img src="{{URL::to('dashboard_assets/assets/images/logo-transparent.png')}}"  class="pull-left" style="width:120px;margin-left:30px;" alt="Truckka"/> </a>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href=""> 
    <img src="{{URL::to('dashboard_assets/assets/images/logo-transparent.png')}}" class="hidden-xs" style="" alt="Truckka"/> </a>

    <!-- user dropdown starts -->
    <!--<div class="btn-group pull-right">
        <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
            <i class="glyphicon glyphicon-user"></i><span class="hidden-sm hidden-xs"> 
            </span>
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu">
        
            <li><a href="#">Profile</a></li>
            <li class="divider"></li>
            <li><a href="#">Edit Profile</a></li>
            <li class="divider"></li>
            <li><a href="#">Change password</a></li>
            <li class="divider"></li>
            <li><a href="logout"> Logout</a></li>
        </ul>
    </div>-->
    <!-- user dropdown ends -->

    <!-- theme selector starts -->

    <!-- theme selector ends -->

    <!--<ul class="collapse navbar-collapse nav navbar-nav top-menu">
        <li><a href="#" target="_blank"><i class="glyphicon glyphicon-star"></i> Visit Site</a></li>
        <li class="dropdown">
            <a href="#" data-toggle="dropdown"><i class="glyphicon glyphicon-cog"></i> Quick Links <span
                    class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
                <li><a href="#">Dashboard</a></li>
                <li><a href="#">View Users</a></li>
                <li class="divider"></li>
                <li><a href="logout">Logout</a></li>
            </ul>
        </li>
        <li>
            <form class="navbar-search pull-left">
                <input placeholder="Search" class="search-query form-control col-md-10" name="query"
                       type="text">
            </form>
        </li>
    </ul>-->

</div>
</div>
<!--////////////////////////// END OF HEADER MENU //////////////////////////////////////////////-->





<div class="ch-container">
    <div class="row">
    <!--/////////////////////////////////// BEGIGNING OF LEFT MENU //////////////////////////////// -->
<div class="col-sm-2 col-lg-2 admin-menu">
            <div class="sidebar-nav">
                <div class="nav-canvas">
                    <div class="nav-sm nav nav-stacked">

                    </div>
                    <ul class="nav nav-pills nav-stacked main-menu">
                       
                        <li><a class="ajax-link" href="#"><i class="glyphicon glyphicon-dashboard"></i><span> Dashboard</span></a> </li>
                        
                       <!-- <div class="admin-menu">-->



                        @if($accessLevel === "administrator")
                       <li class="accordion">
                            <a href="#"><i class="glyphicon glyphicon-briefcase"></i><span> Users</span></a>
                            <ul class="nav nav-pills nav-stacked">

                                <li><a href="{{route('client.get.user')}}">Create User</a></li>
                                <li><a href="{{route('client.manage.user')}}">Manage users</a></li>
                                <li><a href="{{url('/tmsw/admin/group/create')}}">Create Department</a></li>


                            </ul>
                        </li>
                        @endif


                        @if($accessLevel === "manager" || $accessLevel === "desk_officer")
                       <li class="accordion">
                            <a href="#"><i class="glyphicon glyphicon-briefcase"></i><span> Orders</span></a>
                            <ul class="nav nav-pills nav-stacked">
                                <li><a href="{{url('/tmsw/user_orders')}}">My Orders</a></li>
                                <li><a href="{{url('/tmsw/create_new_order')}}">Create New Order</a></li>
                                <li><a href="{{url('/tmsw/operator/manage_waybills')}}">Upload Waybill</a></li>
                            </ul>
                        </li>
                        @endif


                        @if($accessLevel === "manager")
                        <li><a href="{{url('/tmsw/performance')}}"><i class="glyphicon glyphicon-file"></i><span> Performance</span></a> </li>
                        @endif

                        @if($accessLevel === "finance")
                        <li class="accordion">
                            <a href="#"><i class="glyphicon glyphicon-briefcase"></i><span> Invoices</span></a>
                            <ul class="nav nav-pills nav-stacked">
                                <li><a href="{{url('/tmsw/finance/home')}}">Manage Invoices</a></li>

                            </ul>
                        </li>
                        @endif


                        @if($accessLevel === "administrator")
                        <li class="accordion">
                            <a href="#"><i class="glyphicon glyphicon-flag"></i><span>Manage Transporters</span></a>
                            <ul class="nav nav-pills nav-stacked">

                                <li><a href="{{route('transporter.create')}}">Create Transporter </a></li>
                                <li><a href="{{route('view.transporter')}}">All Transporters </a></li>

                                <li><a href="{{url('/tmsw/transporter/active_orders')}}">View Active Orders</a></li><!--
                                <li><a href="{{url('/tmsw/transporter/invoices')}}">View Invoice Status</a></li>-->

                            </ul>
                        </li>
                        @endif

                        @if($accessLevel === "administrator")
                        <li><a href="{{url('/tmsw/manager/manage_routes')}}"><i class="glyphicon glyphicon-file"></i><span> Manage Routes</span></a> </li>
                        @endif


                        @if($accessLevel === "administrator")
                        <li class="accordion">
                            <a href="#"><i class="glyphicon glyphicon-flag"></i><span>Manage Suppliers</span></a>
                            <ul class="nav nav-pills nav-stacked">

                                <li><a href="{{url('/tmsw/supplier/create')}}">Create  Supplier</a></li>
                                <li><a href="{{url('/tmsw/supplier/all')}}">All Suppliers </a></li>

                                <li><a href="{{url('/tmsw/supplier/group/create/')}}">Create Group</a></li>
                                <li><a href="{{url('/tmsw/supplier/group')}}">All Groups</a></li>

                            </ul>
                        </li>
                        @endif

                        @if($accessLevel === "manager")
                            <li class="accordion">
                                <a href="#"><i class="glyphicon glyphicon-flag"></i><span>Manage Supply Orders</span></a>
                                <ul class="nav nav-pills nav-stacked">

                                    <li><a href="{{url('/tmsw/supplier/supply/order/create')}}">Create  Supply Order</a></li>
                                    <li><a href="{{url('/tmsw/supplier/supply/order/all')}}">All Supply Order </a></li>

                                </ul>
                            </li>
                        @endif
                        <li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"  class="ajax-link" href="#"><i class="glyphicon glyphicon-off"></i><span> Logout </span></a> </li>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>

                      <!--</div>-->
 
                    </ul>
                      
                </div>
            </div>
        </div>
        

    <!--/////////////////////////////////////// END OF IT .//////////////////////////////////////////-->
   
   
   
   
    <div id="content" class="col-lg-10 col-sm-10">
                <!-- content starts -->
        <div>
          <ul class="breadcrumb">
              <li>
                  <a href="#">Dashboard</a>
              </li>
              <li>
                  <a href="#" class="breadcrumb-child">{{$breadcrumb}}</a>
              </li>
            
          </ul>
        </div>
        @yield('content')	
	</div>
	</div>
    @extends('tmsw.client.layout.footer')
</div>