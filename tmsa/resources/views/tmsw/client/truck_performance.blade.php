@extends('tmsw.client.layout.content')
@section('content')
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {

        var data = google.visualization.arrayToDataTable([
            ['Task', 'Percentage of Completed Orders'],
            ['Complete',     11],
            ['Incomplete',      2],
        ]);

        var options = {
            title: 'Order Completion Rate'
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
    }
</script>

<script type="text/javascript">
    google.charts.load('current', {packages: ['corechart', 'bar']});
    google.charts.setOnLoadCallback(drawBasic);

    function drawBasic() {

        var data = google.visualization.arrayToDataTable([
            ['Month', 'Tonnage',],
            ['Jan', 817],
            ['Feb', 600],
            ['Mar', 0],
            ['Apr', 0],
            ['May', 0],
            ['Jun', 0],
            ['Jul', 0],
            ['Aug', 0],
            ['Sep', 0],
            ['Oct', 0],
            ['Nov', 0],
            ['Dec', 0],
        ]);

        var options = {
            title: 'Tonnage Moved per Month',
            chartArea: {width: '50%'},
            hAxis: {
                title: 'Tonnage',
                minValue: 0
            },
            vAxis: {
                title: 'Month'
            }
        };

        var chart = new google.visualization.BarChart(document.getElementById('chart_div'));

        chart.draw(data, options);
    }
</script>

<div class="row" xmlns="http://www.w3.org/1999/html">
    <div class="box col-md-12">

        <!--if @ session -->
        @if (session('message'))
        <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert">x</button>
            {{ session('message') }}
        </div>
        @endif

        <!--endif-->

        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-plus"></i> Reports &amp; Analytics</h2>

            </div>
            <div class="box-content row">


                <div class="box col-md-6">

                    <div class="box-content">
                        <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
                            <thead>
                            <tr>
                                <th></th>
                                <th>Origin</th>
                                <th>Destination</th>
                                <th>Number of Trucks</th>
                                <th>Type of Truck</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td class="center"></td>
                                <td class="center">Lagos</td>
                                <td class="center">Abuja</td>
                                <td class="center">5</td>
                                <td class="center">30 Ton Flatbed</td>
                            </tr>
                            <tr>
                                <td class="center"></td>
                                <td class="center">Benin</td>
                                <td class="center">Kano</td>
                                <td class="center">5</td>
                                <td class="center">30 Ton Flatbed</td>
                            </tr>
                            <tr>
                                <td class="center"></td>
                                <td class="center">Lagos</td>
                                <td class="center">Kano</td>
                                <td class="center">5</td>
                                <td class="center">30 Ton Flatbed</td>
                            </tr>
                            <tr>
                                <td class="center"></td>
                                <td class="center">Kaduna</td>
                                <td class="center">Lagos</td>
                                <td class="center">5</td>
                                <td class="center">30 Ton Flatbed</td>
                            </tr>
                            <tr>
                                <td class="center"></td>
                                <td class="center">Lagos</td>
                                <td class="center">Gombe</td>
                                <td class="center">2</td>
                                <td class="center">30 Ton Covered</td>
                            </tr>
                            <tr>
                                <td class="center"></td>
                                <td class="center">Oshogbo</td>
                                <td class="center">Kano</td>
                                <td class="center">5</td>
                                <td class="center">30 Ton Flatbed</td>
                            </tr>
                            <tr>
                                <td class="center"></td>
                                <td class="center">Awka</td>
                                <td class="center">Benin</td>
                                <td class="center">1</td>
                                <td class="center">30 Ton Covered</td>
                            </tr>
                            <tr>
                                <td class="center"></td>
                                <td class="center">Lagos</td>
                                <td class="center">Niger</td>
                                <td class="center">5</td>
                                <td class="center">30 Ton Flatbed</td>
                            </tr>
                            <tr>
                                <td class="center"></td>
                                <td class="center">Lagos</td>
                                <td class="center">PortHarcourt</td>
                                <td class="center">4</td>
                                <td class="center">30 Ton Covered</td>
                            </tr>
                            <tr>
                                <td class="center"></td>
                                <td class="center">Benin</td>
                                <td class="center">Kano</td>
                                <td class="center">3</td>
                                <td class="center">30 Ton Flatbed</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                </div>


                <div class="box col-md-6">

                    <div class="box-content">
                        <div id="piechart">

                        </div>

                        <div id="chart_div">

                        </div>
                    </div>

                </div>






            </div>
        </div>
    </div>
    <!--/span-->

</div><!--/row-->



@endsection