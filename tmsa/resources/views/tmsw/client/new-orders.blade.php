@extends('tmsw.client.layout.content')
@section('content')

<div class="row">
    <div class="box col-md-12">
        <!--if @ session -->
        <!--<div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert">x</button>
               success
        </div>-->
        <!--endif-->

        <!--origin to server form-->
        <form id="originForm" style="display:none;">
            {{csrf_field()}}
            <input type="hidden" id="origin2" name="originToServer">
        </form>
        <!--end origin to server form-->

        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-plus"></i> Create New Orders</h2>

            </div>
            <div class="box-content row">
                <!--if has session message-->
                @if (session('message'))
                <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    {{ session('message') }}
                </div>
                @endif
                <!--endif-->
                <form role="form" method="post" class="validate"  action="{{url('/tmsw/submit_new_order')}}"  enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="box col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="origin">Origin</label>
                            <div class="controls">
                                <select id="standardOrigin" class="form-control col-md-12" name="origin">
                                    <option value=''>[-Origin-]</option>
                                    @for($x=0; $x < count($uniqueOrigins); $x++)
                                    <option>{{$uniqueOrigins[$x]}}</option>
                                    @endfor
                                </select>
                            </div>
                            @if ($errors->has('origin'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('origin') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>


                    <div class="box col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="destination">Destination</label>
                            <div class="controls">
                                <select id="destinationSelect" class="col-md-12 form-control" name="destination">

                                </select>
                            </div>
                            @if ($errors->has('destination'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('destination') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="box col-md-4">
                        <div class="form-group">
                            <label class="control-label" for="Type of cargo">Type of Cargo</label>
                            <div class="controls">
                                <select id="selectError" class="col-md-12 form-control" name="cargoType" required>
                                    <option value="">[-Select Cargo Type-]</option>
                                    <option value="1">Wet</option>
                                    <option value="2">Dry</option>

                                </select>
                            </div>
                            @if ($errors->has('cargoType'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('cargoType') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>


                    <div class="box col-md-4">
                        <div class="form-group">
                            <label class="control-label" for="Type of cargo">Type of Truck</label>
                            <div class="controls">
                                <select id="selectError" class="col-md-12 form-control" name="truckType" required>
                                    <option value="">[-Select Truck Type-]</option>
                                    @foreach($truckTypes as $truckType)
                                    <option value="{{$truckType->id}}">{{$truckType->truck_type_description}}</option>
                                    @endforeach
                                </select>
                            </div>
                            @if ($errors->has('truckType'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('truckType') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="box col-md-4">
                        <div class="form-group">
                            <label class="control-label" for="Type of cargo">Number of Truck</label>
                            <div class="controls">
                                <select id="selectError" class="col-md-12 form-control" name="number_of_truck" required>
                                    <option value="1">1</option>
                                    <option value="1">2</option>
                                    <option value="1">3</option>
                                    <option value="1">4</option>
                                    <option value="1">5</option>
                                    <option value="1">6</option>
                                </select>
                            </div>
                            @if ($errors->has('number_of_truck'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('number_of_truck') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>


                    <div class="box col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="Type of cargo">PickUp Address</label>
                            <div class="controls">
                                <textarea class="col-md-12 form-control" name="pickup_address" required></textarea>
                            </div>
                            @if ($errors->has('pickup_address'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('pickup_address') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>


                    <div class="box col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="Type of cargo">Delivery Address</label>
                            <div class="controls">
                                <textarea class="col-md-12 form-control" name="delivery_address" required></textarea>
                            </div>
                            @if ($errors->has('delivery_address'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('delivery_address') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>


                    <div class="box col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="Type of cargo">Contact Phone</label>
                            <div class="controls">
                                <input type="text" class="col-md-12 form-control" name="contact_phone" required/>
                            </div>
                            @if ($errors->has('contact_phone'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('contact_phone') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>


                    <div class="box col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="Type of cargo">Contact Name</label>
                            <div class="controls">
                                <input type="text" class="col-md-12 form-control" name="contact_name" required/>
                            </div>
                            @if ($errors->has('contact_name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('contact_name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>


                    <div class="box col-md-6">
                        <div class="form-group">
                            <button type="submit" class="btn btn-custom">
                                <i class="glyphicon glyphicon-plus"></i>Submit
                            </button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
    <!--/span-->

</div><!--/row-->



@endsection