@extends('tmsw.client.layout.content')
@section('content')


<div class="row" xmlns="http://www.w3.org/1999/html">
    <div class="box col-md-12">


        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-plus"></i> View Truck Detals</h2>

            </div>
            <div class="box-content row">


                <div class="box col-md-12">
                    <div class="box-content">
                        <table class="table table-striped table-bordered  responsive">
                            <thead>
                            <tr>
                                <th>Plate Number</th>
                                <th>Driver Name</th>
                                <th>Driver Phone</th>
                                <th>Company Name</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($truck_details as $detail)
                            <tr>
                                <td class="center">{{$detail->plateNumber}}</td>
                                <td class="center">{{$detail->driverName}}</td>
                                <td class="center">{{$detail->driverPhone}}</td>
                                <td class="center">{{$detail->company_name}}</td>
                            </tr>
                            @empty
                            @endforelse
                            </tbody>
                        </table>
                    </div>

                </div>






            </div>
        </div>
    </div>
    <!--/span-->

</div><!--/row-->



@endsection