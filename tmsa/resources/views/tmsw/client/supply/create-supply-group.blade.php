@extends('tmsw.client.layout.content')
@section('content')

<div class="row">
    <div class="box col-md-12">
        @if(session('message'))
        <!--if @ session -->
            <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                  {{session('message')}}
            </div>
        <!--endif-->
        @endif
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-plus"></i> Create New Supply Group</h2>

            </div>
            <div class="box-content row">
                 <form role="form" method="post" class="validate"  action="@if($group == null){{url('tmsw/supplier/group/create/save')}}@else{{url('tmsw/supplier/group/edit/save/'.$group->id)}}@endif"  enctype="multipart/form-data">
                    @csrf
                    <div class="box col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="weight">Group Name</label>
                            <input type="text" name="group_name" class="form-control required"  placeholder="Name" value="@if(!is_null($group)){{$group->groupName}}@endif">
                        </div>
                    </div>
                    

                 <div class="box col-md-6">
                    <div class="form-group">
                        <label class="control-label" for="origin">Add to group</label>
                        <div class="controls">
                            <select id="selectError" class="col-md-12" multiple="multiple" data-rel="chosen" name="suppliers[]">
                           @foreach($suppliers as $supplier)
                            <option value='{{$supplier->id}}'
                            @if(!is_null($group) && in_array($supplier->id,$selectedSuppliers))
                                selected="selected"
                            @endif
                            >{{$supplier->supplierName}}</option>
                            @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="box col-md-6">
                    <div class="form-group">
                        <button type="submit" class="btn btn-custom">
                            <i class="glyphicon glyphicon-plus"></i>Submit
                        </button>
                    </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
    <!--/span-->

</div><!--/row-->



@endsection