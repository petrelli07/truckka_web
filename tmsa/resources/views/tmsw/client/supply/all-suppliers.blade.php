@extends('tmsw.client.layout.content')
@section('content')



<div class="row">
    <div class="box col-md-12">
    <div class="box-inner">
    <div class="box-header well" data-original-title="">
        <h2><i class="glyphicon glyphicon-eye-open"></i> My Suppliers</h2>
    </div>
    <div class="box-content">
    <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
    <thead>

    <tr>
        
        <th>S/N</th>
        <th> Name</th>
        <th>Address</th>
        <th>Email</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
        @foreach($suppliers as $supplier)
    <tr>
        
        <td class="center">1</td>
        <td class="center">{{$supplier->supplierName}}</td>
        <td class="center">{{$supplier->supplierAddress}}</td>
        <td class="center">{{$supplier->email}}</td>
        <td class="center">
          @if($supplier->status == 1)
            <a class="btn btn-info btn-sm" href="{{url('tmsw/supplier/status/'.$supplier->id.'/deactivate')}}">
                <i class="glyphicon glyphicon-eye-open icon-white"></i>
                Deactivate
            </a>
          @elseif($supplier->status == 2)
            <a class="btn btn-info btn-sm"  href="{{url('tmsw/supplier/status/'.$supplier->id.'/activate')}}">
                <i class="glyphicon glyphicon-eye-open icon-white"></i>
                Activate
            </a>
          @endif
            
            <a class="btn btn-danger btn-sm"href="{{url('tmsw/supplier/groups/'.$supplier->id)}}">
                <i class="glyphicon glyphicon-eye-open icon-white"></i>
               Groups
            </a>
             <a class="btn btn-danger btn-sm"  href="{{url('tmsw/supplier/edit/'.$supplier->id)}}">
                <i class="glyphicon glyphicon-eye-open icon-white"></i>
               Edit
            </a>
        </td>
    </tr>
    @endforeach
    </tbody>
    </table>
    </div>
    </div>
    </div>
    <!--/span-->

    </div>



@endsection