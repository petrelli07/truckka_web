@extends('tmsw.client.layout.content')
@section('content')



<div class="row">
    <div class="box col-md-12">
    <div class="box-inner">
    <div class="box-header well" data-original-title="">
        <h2><i class="glyphicon glyphicon-eye-open"></i> Order Participants</h2>
    </div>
    <div class="box-content">
    <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
    <thead>

    <tr>
        
        <th>S/N</th>
        <th> Supplier Name</th>
        <th>status</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
      <?php  $counter = 1;?>
        @foreach($participants as $participant)
    <tr>
        
        <td class="center">{{$counter++}}</td>
        <td class="center">{{$participant->supplierName}}</td>
        
        <td class="center">
            @if(is_null($participant->supply_quotes))
                {{'Quote not sent'}}
            @elseif(!is_null($participant->supply_quotes) && $participant->request_status == 3)
                {{'rejected'}}
            @elseif(!is_null($participant->supply_quotes) && $participant->request_status == 4)
                {{'Approved'}}
            @else
                {{'Quote sent'}}
            @endif

        </td>
  
     
        <td class="center">
           @if(!is_null($participant->supply_quotes) && $participant->request_status == 3 && strtotime(date("Y-m-d h:i:sa")) < strtotime($participant->expires_at))
            <a class="btn btn-info btn-sm" href="{{url('/tmsw/supplier/status/quote/'.$participant->request_id.'/approve')}}">
                <i class="glyphicon glyphicon-eye-open icon-white"></i>
                Approve
            </a>
            @elseif(!is_null($participant->supply_quotes) && $participant->request_status == 4 && strtotime(date("Y-m-d h:i:sa")) < strtotime($participant->expires_at))
              <a class="btn btn-info btn-sm" href="{{url('/tmsw/supplier/status/quote/'.$participant->request_id.'/reject')}}">
                <i class="glyphicon glyphicon-eye-open icon-white"></i>
                Reject
              </a>
            @elseif(!is_null($participant->supply_quotes) && $participant->request_status == 1 && strtotime(date("Y-m-d h:i:sa")) < strtotime($participant->expires_at))
                <a class="btn btn-info btn-sm" href="{{url('/tmsw/supplier/status/quote/'.$participant->request_id.'/approve')}}">
                    <i class="glyphicon glyphicon-eye-open icon-white"></i>
                    Approve
                </a>
                |
                <a class="btn btn-info btn-sm" href="{{url('/tmsw/supplier/status/quote/'.$participant->request_id.'/reject')}}">
                    <i class="glyphicon glyphicon-eye-open icon-white"></i>
                    Reject
                </a>

            @endif
            
           
           @if(!is_null($participant->supply_quotes))

             <a class="btn btn-danger btn-sm" href='{{url("/tmsw/supplier/view_quote/$participant->id/$participant->client_supply_order_id")}}'>
                <i class="glyphicon glyphicon-eye-open icon-white"></i>
               Download Quote
            </a>
            @endif
        </td>
    </tr>
    @endforeach
    </tbody>
    </table>
    </div>
    </div>
    </div>
    <!--/span-->

    </div>



@endsection