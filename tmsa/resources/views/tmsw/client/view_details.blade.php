@extends('tmsw.client.layout.content')
@section('content')


<div class="row" xmlns="http://www.w3.org/1999/html">
    <div class="box col-md-12">


        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-plus"></i> View More</h2>

            </div>
            <div class="box-content row">
                <!--if @ session -->
                @if (session('message'))
                <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    {{ session('message') }}
                </div>
                @endif
                <!--endif-->
                @foreach($details as $detail)
                <div class="box col-md-12">
                    @if($detail->waybillFile != NULL)
                    <div class="box col-md-12">

                        <img src="{{url('/tmsw/waybills/'.$detail->waybillNumber.$detail->waybillFile)}}"  height="500" width="100%" class="img-responsive">

                    </div>
                    @endif
                </div>

                <div class="box col-md-12">
                    <div class="box-content">
                        <table class="table table-striped table-bordered  responsive">
                            <thead>
                            <tr>
                                <th>Waybill Number</th>
                                <th>Origin</th>
                                <th>Destination</th>
                                <th>Amount</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td class="center">{{$detail->waybillNumber}}</td>
                                <td class="center">{{$detail->originStateName}}</td>
                                <td class="center">{{$detail->destStateName}}</td>
                                <td class="center">{{$detail->price}}</td>
                                <td class="center">
                                @if($detail->tmsw_status_id == 13)
                                    <a class="btn btn-success btn-md" href='#'>Invoice Processed</a>
                                    @else
                                    <a class="btn btn-success btn-md" href='{{url("/tmsw/finance/confirm_invoice/$detail->invoice_reference")}}'>Confirm Invoice</a>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>






            </div>
        </div>
    </div>
    <!--/span-->

</div><!--/row-->



@endsection