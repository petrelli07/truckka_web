@extends('tmsw.transporter.layout.content')
@section('content')



<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-eye-open"></i> My Orders</h2>
            </div>
            <div class="box-content">
                <!--if @ session -->
                @if (session('message'))
                <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    {{ session('message') }}
                </div>
                @endif
                <!--endif-->
                <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
                    <thead>

                    <tr>

                        <th>Order ID</th>
                        <th>Origin</th>
                        <th>Destination</th>
                        <th>Number of Trucks</th>
                        <th>Amount</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($user as $users)
                    <tr>
                        <td class="center">{{$users->reference_number}}</td>
                        <td class="center">{{$users->originStateName}}</td>
                        <td class="center">{{$users->destStateName}}</td>
                        <td class="center">{{$users->truck_number}}</td>
                        <td class="center">&#x20A6;{{number_format($users->price , 2, '.', ',')}}/Truck</td>
                        <td class="center">
                            {{$users->description}}
                        </td>
                        <td class="center">
                            @if($users->description === "Order Created:Awaiting Transporter Response")
                            <a class="btn btn-info btn-sm" href='{{url("tmsw/transporter/add_trucks/{$users->reference_number}")}}'>
                                Add Trucks
                            </a>
                            @endif
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td class="center"></td>
                        <td class="center"></td>
                        <td class="center"></td>
                        <td class="center"></td>
                        <td class="center"></td>
                        <td class="center"></td>
                        <td class="center"></td>
                    </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!--/span-->

</div>



@endsection