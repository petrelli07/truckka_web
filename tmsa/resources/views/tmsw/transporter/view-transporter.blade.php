@extends('tmsw.client.layout.content')
@section('content')



<div class="row">
    <div class="box col-md-12">
     <!--if @ session -->
     @if(session('message'))
            <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    {{session('message')}}
            </div>
        @endif
        <!--endif-->
        
    <div class="box-inner">
    <div class="box-header well" data-original-title="">
        <h2><i class="glyphicon glyphicon-eye-open"></i> All Transporters</h2>
    </div>
    <div class="box-content">
    <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
    <thead>

    <tr>
        
        <th>ID</th>
        <th>Name</th>
        <th>Phone Number</th>
        <th>Account Name</th>
        <th>Account Number</th>
        <th>Company Name</th>
        <th>Status</th>
        <th>Action</th>
        
    </tr>
    </thead>
    <tbody>
    <?php $x =1; ?>
        @forelse($transporterinfo as $row)
        
    <tr>
    
        <td class="center">{{$x++}}</td>
        <td class="center">{{$row->contact_name}}</td>
        <td class="center">{{$row->phone}}</td>
        <td class="center">{{$row->account_name}}</td>
        <td class="center">{{$row->account_number}}</td>
        <td class="center">{{$row->company_name}}</td>
        @if($row->status == 1)
        <td>Active</td>
        @else
        <td>Deactived</td>
        @endif
        <td class="center">

        @if($row->status == 1)
        <!-- THIS IS TO SUSPEND -->

        <form action="{{ route('transporter.deactivate') }}" method="post" class="login"> 
        {{ csrf_field() }}
            <input type="hidden" name="id" value="{{$row->user_id}}"  />
            <button class="btn btn-success btn-sm view-shop" type="submit">
            <i class="glyphicon glyphicon-edit icon-white"></i> Deactivate
            </button>
        </form>
         <!-- END OF THIS IS TO SUSPEND -->
         @else
        
        <!-- THIS IS TO SUSPEND -->

        <form action="{{ route('transporter.activate') }}" method="post" class="login"> 
        {{ csrf_field() }}
            <input type="hidden" name="id" value="{{$row->user_id}}"  />
            <button class="btn btn-info btn-sm view-shop" type="submit">
            <i class="glyphicon glyphicon-edit icon-white"></i> Activate
            </button>
        </form>
         <!-- END OF THIS IS TO SUSPEND -->

        @endif

        </td>
       
        @empty

    
    </tr>
    @endforelse
    </tbody>
    </table>
    </div>
    </div>
    </div>
    <!--/span-->

    </div>



@endsection