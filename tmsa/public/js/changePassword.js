$(document).ready(function() {

    $(".resetPassword").click(function(e){
        e.preventDefault();


        var _token = $("input[name='_token']").val();/*
        var name = $("input[name='adminName']").val();*/

        $.ajax({

            async:false,

            processData: false,

            contentType: false,

             //url: "http://truckka.com.ng:8082/resetPassword",
            url: baseURI+"/resetPassword",

            type:'POST',

            data: new FormData($("#resetPass")[0]),

            success: function(data) {

                if($.isEmptyObject(data.error)){

                    printSuccessMsg(data.success);
                    document.getElementById('logout-form').submit();
                    //window.location.replace("http://localhost/tmsa/public/home1");
                    //window.location.replace("http://truckka.com.ng:8082/home1");

                }else{

                    alert(data.error);

                }

            }

        });


    });
    function printErrorMsg (msg) {

        $(".print-error-msg").find("ul").html('');

        $(".print-error-msg").css('display','block');

        $.each( msg, function( key, value ) {

            $(".print-error-msg").find("ul").append('<li>'+value+'</li>');
            $(".print-error-msg").fadeOut(5000);
        });

    }

    function printSuccessMsg (success) {

        $(".print-success-msg").find("ul").html('');

        $(".print-success-msg").css('display','block');


        $(".print-success-msg").find("ul").append(success);
        $(".print-success-msg").fadeOut(5000);

    }

});


