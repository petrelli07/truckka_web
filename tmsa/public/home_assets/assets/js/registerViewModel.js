﻿function RegisterViewModel() {
    var self = this;
    var txtUserName = $('#txtUserName');
    var txtEmail = $('#txtEmail');
    var txtPassword = $('#txtLoginPassword');
    var txtPhone = $('#txtPhone');
    var txtConfirmPassword = $("#txtConfirmPassword");
    var btnRegister = $("#btnRegister");
    var btnUnRegister = $("#btnUnRegister");
    var indexsignUp = $("#IndexSignUp");

    function Intialize() {

        Events();

    };

    function homeScroll() {
        $(document).ready(function () {
            $('#IndexLoader').fadeOut(500);
            $(".dwn-arw a,.list-unstyled li .slide").click(function (evn) {
                evn.preventDefault();
                $('html, body').animate({ scrollTop: $($(this).attr('href')).offset().top }, 500, 'linear');
            });

            $('.carousel').carousel({
                interval: 12000
            });
        });

        $(".about-btn a").click(function (evn) {
            evn.preventDefault();
            $('html, body').animate({ scrollTop: $($(this).attr('href')).offset().top - 40 }, 500, 'linear');
        });

        $('.close-register-message').click(function () {
                    $(".register-message").hide();
        });
        $(document).ready(function () {
            $("#aboutSlide").click(function (evn) {
                evn.preventDefault();
                if (window.location.pathname == "/home/faq" || window.location.pathname == "/home/contact" || window.location.pathname == "/faq" || window.location.pathname == "/contact" || window.location.pathname == "truck/rates/index.html")
                    window.open("home/index/index.html#about", '_blank');// window.location.href = '/Home/Index/#about';
                $('html, body').animate({ scrollTop: $($(this).attr('href')).offset().top - 40 }, 500, 'linear');
            });
            $("#goToAbout").click(function () {
                window.open("home/index/index.html#about", '_blank');
            });
        });
    }

    function Events() {
        $("#txtLoginPassword,#txtConfirmPassword").keypress(function(evt) {

            var key = (evt.which) ? evt.which : evt.keyCode;
            if (key == 32) {
                return false;
            }
        });

        indexsignUp.click(function () {
            txtUserName.val("");
            txtUserName.next().text("");
            txtEmail.val("");
            txtEmail.next().text("")
            txtPassword.val("");
            txtPhone.val("");
            txtConfirmPassword.val("");
            txtPassword.val("");
        });

        btnRegister.on("click", function (e) {

            var isRequired = false;
            var isPasswordVal = false;

            var ctrl = $(".field-validation-valid");
            if (txtUserName.val().trim() == "") {
                txtUserName.next().text("Name is required.");
                $('.text4').css({ 'margin-bottom': '0px' });
                isRequired = true;
            }

            else {
                txtUserName.next().text("");
                $('.text4').css({ 'margin-bottom': '20px' })
                $('#txtUserName').css({ 'margin-bottom': '20px' })
            }

            if (txtEmail.val() == "") {
                txtEmail.next().text("Email is required.");
                $('.text4').css({ 'margin-bottom': '0px' });
                isRequired = true;
            }

            if (txtEmail.val() != "") {
                var email = DataBase.ExecuteDataset("SELECT EMAIL FROM AspNetUsers WHERE EMAIL='" + txtEmail.val().trim() + "'")
                if (email.length > 0) {
                    txtEmail.next().text("Email already exist");
                    $('.text4').css({ 'margin-bottom': '0px' });
                    isRequired = true;
                }


                else {
                    txtEmail.next().text("");
                    $('.text4').css({ 'margin-bottom': '20px' })
                }
            }
            if (txtPhone.val() == "") {
                txtPhone.next().text("Phone Number is required.");
                $('.text4').css({ 'margin-bottom': '0px' });
                isRequired = true;
            }
            else if (txtPhone.val() != "") {

                var phonenumber = DataBase.ExecuteDataset("SELECT PhoneNumber FROM AspNetUsers WHERE PHONENUMBER='" +
                    txtPhone.val().trim() +
                    "'");
                if (phonenumber.length > 0) {
                    txtPhone.next().text("Phone Number already exist");
                    $('.text4').css({ 'margin-bottom': '0px' });
                    isRequired = true;
                }
                else {
                    txtPhone.next().text("");
                    $('.text4').css({ 'margin-bottom': '20px' });
                }
            }
            else {
                txtPhone.next().text("");
                $('.text4').css({ 'margin-bottom': '20px' });

            }

            if (txtPassword.val() == "") {
                txtPassword.next().text("Password is required.");
                $('.text4').css({ 'margin-bottom': '0px' });
                isRequired = true;
            } else {
                txtPassword.next().text("");
                $('.text4').css({ 'margin-bottom': '20px' });
            }
            if (txtConfirmPassword.val() == "") {
                txtConfirmPassword.next().text("Confirm Password is required.");
                $('.text4').css({ 'margin-bottom': '0px' });
                isRequired = true;
                isPasswordVal = true;
            } else {
                txtConfirmPassword.next().text("");
                $('.text4').css({ 'margin-bottom': '20px' });
            }
            if (txtConfirmPassword.val().trim() != txtPassword.val().trim()) {
                txtConfirmPassword.next().text("Password and Confirm Password are doesn't match.");
                $('.text4').css({ 'margin-bottom': '0px' });
                isRequired = true;
                isPasswordVal = true;
            }
            if (txtEmail.val() != "" && !Common.ValidateEmail(txtEmail.val())) {
                txtEmail.next().text("Invalid email.");
                $('.text4').css({ 'margin-bottom': '0px' });
                isRequired = true;
                isPasswordVal = true;
            }
            if (isPasswordVal == false) {
                if (txtPassword.val().length < 6 && txtConfirmPassword.val().length < 6) {
                    txtConfirmPassword.next().text("Passwords must be at least 6 characters.");
                    $('.text4').css({ 'margin-bottom': '0px' });
                    isRequired = true;
                }
            }

            if (isRequired) {
                return false;
            }
            else {
                Common.IsBusy(true);
                var date = new Date();
                var time = date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();

                $('#regPickupAddress').val($("#txtPickupAddress1").val());
                $('#regDestAddress').val($("#txtDeliveryAddress1").val());
                $('#regDatePickup').val($("#txtPickUpDate").val() + " " + time);
                $('#regEquipmentType').val($(".equipments:checked").val());
                $('#regTruckWeight').val($("#SelectedWeightVal").val());
                $('#regTruckFeet').val(parseInt($("#SelectedFeetVal").val()));
                $('#pickupLat').val($("#pickUpLat").val());
                $('#pickupLng').val($("#pickUpLong").val());
                $('#destLat').val($("#deliveryLat").val());
                $('#destLng').val($("#deliveryLong").val());

                var data = {
                    Name: $("#txtUserName").val(), Email: $("#txtEmail").val(), PhoneNumber: $("#txtPhone").val(),
                    Password: $("#txtLoginPassword").val(), ConfirmPassword: $("#txtConfirmPassword").val()
                };
                $.ajax({
                    url: "/Account/RegisterUser",
                    type: "POST",
                    data: data,
                    success: function (result) {
                        if (result.Status == true && result.Result != null) {
                            if ($("#txtPickupAddress1").val() != null && $("#txtPickupAddress1").val() != "") {
                                var date = new Date();
                                var time = date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
                                var data = {
                                    Origin: $("#txtPickupAddress1").val(), Destination: $("#txtDeliveryAddress1").val(), Equipment_Type: $(".equipments:checked").val().toString(),
                                    Date: $("#txtPickUpDate").val() + " " + time, Weight: $("#SelectedWeightVal").val().trim(), Feet: parseInt($("#SelectedFeetVal").val()),
                                    PickUpLng: $("#pickUpLong").val(), PickUpLat: $("#pickUpLat").val(), DeliveryLng: $("#deliveryLong").val(), DeliveryLat: $("#deliveryLat").val(),
                                    SkipUserCheck: true, UserId: result.Result
                                };
                                $.ajax({
                                    url: "/api/quotes",
                                    type: "POST",
                                    data: data,
                                    success: function (result) {
                                        $(".registerpoppup").modal('hide');
                                        //$(".registerpoppup").hide();
                                        $(".register-message").show();
                                        Common.IsBusy(false);
                                    }
                                });
                            } else {
                                $(".registerpoppup").modal('hide');
                                $(".register-message").show();
                                Common.IsBusy(false);
                            }
                        } else {
                            $(".registerpoppup").modal('hide');
                            Common.IsBusy(false);
                        }

                        homeScroll();
                    }
                });
            }
        });

        btnUnRegister.on("click", function (e) {
            var isRequired = false;
            var isPasswordVal = false;
            var ctrl = $(".field-validation-valid");
            if (txtUserName.val().trim() == "") {
                txtUserName.next().text("Name is required.");
                $('.text4').css({ 'margin-bottom': '0px' });
                isRequired = true;
            }
            else {
                txtUserName.next().text("");
                $('.text4').css({ 'margin-bottom': '20px' })
                $('#txtUserName').css({ 'margin-bottom': '20px' })
            }
            if (txtEmail.val() == "") {
                txtEmail.next().text("Email is required.");
                $('.text4').css({ 'margin-bottom': '0px' });
                isRequired = true;
            }
            else {
                txtEmail.next().text("");
                $('.text4').css({ 'margin-bottom': '20px' })
            }
            if (txtEmail.val() != "") {
                var email = DataBase.ExecuteDataset("SELECT EMAIL FROM AspNetUsers WHERE EMAIL='" + txtEmail.val().trim() + "'")
                if (email.length > 0) {
                    txtEmail.next().text("Email already exist");
                    $('.text4').css({ 'margin-bottom': '0px' });
                    isRequired = true;
                }


                else {
                    txtEmail.next().text("");
                    $('.text4').css({ 'margin-bottom': '20px' })
                }
            }

            if (txtPhone.val() == "") {
                txtPhone.next().text("Phone Number is required.");
                $('.text4').css({ 'margin-bottom': '0px' });
                isRequired = true;
            }
            else if (txtPhone.val() != "") {

                var phonenumber = DataBase.ExecuteDataset("SELECT PhoneNumber FROM AspNetUsers WHERE PHONENUMBER='" + txtPhone.val().trim() + "'")
                if (phonenumber.length > 0) {
                    txtPhone.next().text("Phone Number already exist");
                    $('.text4').css({ 'margin-bottom': '0px' });
                    isRequired = true;
                }
                else {
                    txtPhone.next().text("");
                    $('.text4').css({ 'margin-bottom': '20px' });
                }
            }
            else {
                txtPhone.next().text("");
                $('.text4').css({ 'margin-bottom': '20px' });

            }

            if (txtPassword.val() == "") {
                txtPassword.next().text("Password is required.");
                $('.text4').css({ 'margin-bottom': '0px' });
                isRequired = true;
            } else {
                txtPassword.next().text("");
                $('.text4').css({ 'margin-bottom': '20px' })
            }

            if (txtConfirmPassword.val() == "") {
                txtConfirmPassword.next().text("Confirm Password is required.");
                $('.text4').css({ 'margin-bottom': '0px' });
                isRequired = true;
                isPasswordVal = true;
            }
            else if (txtConfirmPassword.val() != "") {
                if (txtPassword.val() != txtConfirmPassword.val()) {
                    txtConfirmPassword.next().text("Password and Confirm Password are doesn't match.");
                    $('.text4').css({ 'margin-bottom': '0px' });
                    isRequired = true;
                    isPasswordVal = true;
                }
                else {
                    txtConfirmPassword.next().text("");
                    $('.text4').css({ 'margin-bottom': '20px' })
                }
            }
            else {
                txtConfirmPassword.next().text("");
                $('.text4').css({ 'margin-bottom': '20px' })
            }


            if (txtEmail.val() != "" && !Common.ValidateEmail(txtEmail.val())) {
                txtEmail.next().text("Invalid email.");
                $('.text4').css({ 'margin-bottom': '0px' });
                isRequired = true;
                isPasswordVal = true;
            }

            if (isPasswordVal == false) {
                if (txtPassword.val().length < 6 && txtConfirmPassword.val().length < 6) {
                    txtConfirmPassword.next().text("Passwords must be at least 6 characters.");
                    $('.text4').css({ 'margin-bottom': '0px' });
                    isRequired = true;
                }
            }

            if (isRequired) {

                return false;
            }
            else {
                var WeightNum = $('#SelectedWeightVal').val();
                firstName = "";
                lastName = "";

                if (!(txtUserName.val().indexOf(' ') == -1)) {
                    var NameCount = txtUserName.val().split(' ').length;
                    firstName = txtUserName.val().split(' ')[0];

                    for (var i = 1; i < NameCount; i++) {
                        lastName = lastName + " " + txtUserName.val().split(' ')[i];
                    }
                }
                else {
                    firstName = txtUserName.val();
                    lastName = "";
                }
                Common.IsBusy(true);
                var feet = $("#SelectedFeetVal").val().split(' ');
                $.ajax({
                    url: "/Account/RegisterUserDetails",
                    type: "POST",
                    async: false,
                    data: {
                        FirstName: firstName.trim(), LastName: lastName.trim(), UserName: $('#txtEmail').val().trim(), Email: $('#txtEmail').val().trim(), Password: $('#txtLoginPassword').val(), ConfirmPassword: $('#txtConfirmPassword').val(), PhoneNumber: $('#txtPhone').val(), CompanyName: $('#txtCompanyName').val(),
                        Pickup: $('#txtPickupAddress1').val(), Dest: $('#txtDeliveryAddress1').val(), PickUpDate: $('#txtPickUpDate').val(), EstimateTime: $('.spnPickUpAvgDays').text(), EquipMent: $('.equipments:checked').val(), Weight: WeightNum, Feet: feet[0], Miles: $(".spnPickUpMiles").text(), Amount: $('.spanTotalAmount').text()
                    },
                    success: function (data) {
                        Common.IsBusy(false);
                        if (data.Status == true) {
                            if (path.includes("/user/quote")) {
                                location.href = "/user/quote?id=" + $('#id').val();
                                $('.loginpoppup').hide();
                            }
                        }
                        else {
                            location.href = "home/index.html";
                        }
                    }
                })
            }
        });

        txtPhone.keypress(function (evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        });

    };

    Intialize();
}

Register = new RegisterViewModel();

