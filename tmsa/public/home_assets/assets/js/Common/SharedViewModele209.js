﻿//common view model codes
function SettingsViewModel() {
    var self = this;
    self.common = {};
    self.common.isBusy = function (isBusy) {
        if (isBusy) {
            $("#isLoaderBusy").css("cssText", "display: inline-block !important;");
        } else {
            $("#isLoaderBusy").css("display", "none");
        }
    }
    self.common.validateEmail = function (email) {
        var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        return expr.test(email);
    }
    self.common.getGoogleMapLocation = function (picklocation, deliverylocation) {
        function init() {
            var directionsService = new google.maps.DirectionsService;
            var directionsDisplay = new google.maps.DirectionsRenderer;
            if (document.getElementById('map') != undefined && document.getElementById('map') != null) {
                var map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 7,
                    center: new google.maps.LatLng(41.85, -87.65)
                });
                directionsDisplay.setMap(map);
                calculateAndDisplayRoute(directionsService, directionsDisplay);
            }
        }
        function calculateAndDisplayRoute(directionsService, directionsDisplay) {
            directionsService.route({
                origin: picklocation.val(),
                destination: deliverylocation.val(),
                travelMode: 'DRIVING'
            }, function (response, status) {
                if (status === 'OK') {
                    directionsDisplay.setDirections(response);
                } else {
                }
            });
        }
        init();
    }
    self.common.redirectToRoute = function (url, type, data) {
        var result = null;
        $.ajax({
            url: url,
            type: type,
            contenttype: "application/json",
            async: false,
            data: data,
            success: function (data) {
                result = data;
                if (result.errorMessage == "Session Expired") {
                    tq.common.redirectToUrl("Common/Common/SessionExpire");
                    return;
                }
                tq.common.isBusy(false);
            }

        });
        return result;
    }
    self.common.isNumberKey = function (evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }
    self.common.isNumber = function (evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        if (charCode == 67 && charCode == 88 && charCode == 86) {
            return true;
        }
        return true;
    }
    self.common.isDecimalNumber = function (evt, element) {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        if (
            (charCode != 45 || $(element).val().indexOf('-') != -1) &&      // “-” CHECK MINUS, AND ONLY ONE.
                (charCode != 46 || $(element).val().indexOf('.') != -1) &&      // “.” CHECK DOT, AND ONLY ONE.
                (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
    self.common.getCardType = function (number) {

        // Visa;
        var re = new RegExp("^4[0-9]{12}(?:[0-9]{3})?$");
        if (number.match(re) != null)
            return "Visa";
        // Mastercard
        re = new RegExp("^(?:5[1-5][0-9]{2}|222[1-9]|22[3-9][0-9]|2[3-6][0-9]{2}|27[01][0-9]|2720)[0-9]{12}$");
        if (number.match(re) != null)
            return "MasterCard";
        // AMEX
        re = new RegExp("^3[47][0-9]{13}$");
        if (number.match(re) != null)
            return "AMEX";
        // Discover
        re = new RegExp("^6(?:011|5[0-9]{2})[0-9]{12}$");
        if (number.match(re) != null)
            return "Discover";
        // Diners
        re = new RegExp("^3(?:0[0-5]|[68][0-9])[0-9]{11}$");
        if (number.match(re) != null)
            return "Diners";
        // Diners - Carte Blanche
        re = new RegExp("^30[0-5]");
        if (number.match(re) != null)
            return "Diners - Carte Blanche";
        // JCB
        re = new RegExp("^(?:2131|1800|35[0-9]{3})[0-9]{11}$");
        if (number.match(re) != null)
            return "JCB";
        // Visa Electron
        re = new RegExp("^(4026|417500|4508|4844|491(3|7))");
        if (number.match(re) != null)
            return "Visa Electron";
        // Maestro
        re = new RegExp("^(5018|5020|5038|6220|6304|6759|6761|6763)");
        if (number.match(re) != null)
            return "Maestro";
        return "";
    };
    self.common.redirectToUrl = function (url) {
        window.location.href = url;
    };
    self.urls = {};

    self.urls.quoteApi = "/api/quotes";
    self.urls.quotePayment = "user/quote/payment/index.html";
    self.urls.getOrderShipmentInfo = "/OrderApi/GetOrderShipmentInfo";
    self.urls.bookQuoteByPhoneNo = "/User/Booking/BookQuoteByPhoneNo";
    self.urls.orderPickUpInfo = "/OrderApi/OrderPickUpInfo";
    self.urls.orderDeliveryInfo = "/OrderApi/OrderDeliveryInfo";
    self.urls.paymentInvoice = "OrderApi/PaymentInvoice/index.html";
    self.urls.orderCreate = "/OrderApi/Create";
    self.urls.saveQuestionForQus = "/User/Order/SaveQuestionForQus";
    self.urls.trackOrderPartial = "/User/Order/TrackOrderPartial";
    self.urls.saveReplyNotification = "/Admin/Notification/SaveReplyNotification";
    self.urls.replyNotification = "/Admin/Notification/ReplyNotification";
    self.urls.sendContact = "/Home/SendContact";
    self.urls.quotes = "/quotes";
    self.urls.getAllOrderStatusHistoriesByOrderId = "/User/Order/GetAllOrderStatusHistoriesByOrderId";
    self.urls.updateOrderInfo = "/OrderApi/UpdateOrderInfo";
    self.urls.getOrderValue = "/OrderApi/GetOrderValue";
    self.urls.isAdminAuthorized = "/User/Order/IsAdminAuthorized";
    self.urls.updateQuoteAmount = "/OrderApi/UpdateQuoteAmount";
    self.urls.processPayment = "/OrderApi/ProcessPayment";
    self.urls.removerOrderDetails = "/OrderApi/RemoverOrderDetails";
    self.urls.orderUpload = "/OrderApi/UploadFile";
    self.urls.jsonResultForOrder = "/Common/Common/JsonResultForOrder";
    self.urls.orders = "/orders";
    self.urls.removerOrder = "/User/Order/RemoverOrder";
    self.urls.trackOrdersById = "/User/Order/TrackOrdersById";
    self.urls.getUserById = "/Admin/Admin/GetUserByID?searchText=";
    self.urls.adminQuotes = "/Admin/Quotes/Quotes?userId=";
    self.urls.userInfo = "/Admin/Admin/UserInfo?userId=";
    self.urls.createUser = "/Account/CreateUser";
    self.urls.editAdminProfile = "/Account/EditAdminProfile";
    self.urls.verifyEmail = '/Admin/Admin/VerifyEmail';
    self.urls.resetPasswordByAdmin = "/Account/ResetPasswordByAdmin";
    self.urls.resendActivationLinkByAdmin = "/Account/ResendActivationLinkByAdmin";
    self.urls.saveEmailNotificationStatus = "/User/User/SaveEmailNotificationStatus";
    self.urls.deleteUserCard = "/User/User/DeleteUserCard";
    self.urls.userChangePassword = "/Account/UserChangePassword";
    self.urls.saveweightcalculation = "/admin/admin/saveweightcalculation";
    self.urls.saveFootage = "/Admin/Admin/SaveFootage";
    self.urls.marketCitySaveall = "/MarketRate/MarketCitySaveall";
    self.urls.saveCitiesAssociateWithMarketCities = "/MarketRate/SaveCitiesAssociateWithMarketCities";
    self.urls.marketrates = "/marketrates?source=";
    self.urls.saveTruck = "/MarketRate/SaveTruck";
    self.urls.truckSaveall = "/MarketRate/TruckSaveall";
    self.urls.marketRateFileUpload = "/Admin/MarketRate/FileUpload";
    self.urls.getReportsByDate = "/Admin/Admin/GetReportsByDate";
    self.urls.getAllActiveUsers = "/Api/User/GetAllActiveUsers";
    self.urls.loginUser = "/Account/LoginUser";
    self.urls.registerUser = "/Account/RegisterUser";
    self.urls.userVerified = "/Home/UserVerified";
    self.urls.generateAccessToken = "/User/User/GenerateAccessToken";
    self.urls.userProfile = "/Account/UserProfile";
    self.urls.forgotPassword = "/Account/ForgotPassword";
    self.urls.deleteAccessToken = "/User/User/DeleteAccessToken";
    self.urls.placeOrderByAdmin = "/OrderApi/PlaceOrderByAdmin";

    self.dataManager = {};
    self.dataManager.ErrorMessage = "Bad Server, something has gone wrong.";

    self.dataManager.ajaxTransport = function (url, options) {

        var baseUrl = '' + url;
        var deferred = new $.Deferred();

        var defaults = {
            statusCode: {
                500: function (xhr, status, error) {
                    toastr.error(error);
                },
                404: function (xhr, status, error) {
                    toastr.error(error);
                },
                403: function (xhr, status, error) {
                    toastr.info("Session Expired, Login again.", function () {
                        tq.common.redirectToUrl('/logoff');
                    });
                }
            },
            url: baseUrl,
            contentType: 'application/json; charset=utf-8',
            success: function (result) {

                deferred.resolve(result);
            },
            error: function (xhr, status, error) {
                if (error == "Forbidden") {
                    deferred.reject(this, arguments);
                } else {
                    var result = { Status: false, Message: self.dataManager.ErrorMessage, Result: null };
                    deferred.resolve(result);
                }
            }
        };

        var o = $.extend({}, defaults, options);
        $.ajax(o);
        return deferred.promise();
    };

    self.dataManager.getData = function (url, tryCache) {
        if (tryCache) {
            var cached = localStorage.getItem(url);
            if (cached) {
                var deferred = new $.Deferred();
                var deserialized = JSON.parse(cached);
                return deferred.resolve(deserialized);
            } else {
                var promise = self.dataManager.ajaxTransport(url, { type: 'GET' });
                promise.done(function (result) {
                    var serialized = JSON.stringify(result);
                    localStorage.setItem(url, serialized);
                });
                return promise;
            }
        } else {
            return self.dataManager.ajaxTransport(url, { type: 'GET' });
        }
    };

    self.dataManager.getDataWithParams = function (url, data) {
        return self.dataManager.ajaxTransport(url, { type: 'GET', data: data });
    };

    self.dataManager.postData = function (url, data) {
        return self.dataManager.ajaxTransport(url, { type: 'POST', data: JSON.stringify(data) });
    };

    self.dataManager.postDeleteData = function (url, data) {
        return self.dataManager.ajaxTransport(url, { type: 'POST', data: JSON.stringify(data) });
    };

    self.dataManager.postFile = function (url, formData) {
        var deferred = new $.Deferred();
        $.ajax({
            type: "POST",
            url: url,
            data: formData,
            dataType: 'json',
            contentType: false,
            processData: false,
            success: function (result) {
                deferred.resolve(result);
            },
            error: function () {
                deferred.reject(this, arguments);
            }
        });
        return deferred.promise();
    }

}
//end of common view model codes

//Quote result common code

function QuoteResultViewModel() {
    var self = this;
    var pathname = window.location.pathname;
    var path = pathname.toLowerCase();
    var txtPickUpAddress = null;
    var txtDelivery = null;
    var equipmentType = null;
    var btnGetQuote = null;
    var PickupError = null;
    var DelieryError = null;
    var bookpopup = null;
    var btnLogOut = null;
    var btnBookPopUp = null;
    var txtPickUpDate = $("#txtPickUpDate");

    self.calculateQuote = function () {

        PickupError.text("");
        DelieryError.text("");
        txttrucktypeError.text("");
        txtSelectWeightError.text("");
        txttruckCategoryError.text("");

        //for slider
        txtSelectFeetError.text("");

        var isCheck = true;
        if (txtPickUpAddress.val() == "") {
            isCheck = false;
            PickupError.text("Please enter the PickUp City and State");
        }
        else {
            PickupError.text("");
        }
        if (txtDelivery.val() == "") {
            isCheck = false;
            DelieryError.text("Please enter the Delivery City And State");
        }
        else {
            DelieryError.text("");
        }
        if (equipmentType.is(':checked') == false) {
            txttrucktypeError.text("Please select truck type");
            isCheck = false;
        }

        if (SelectedWeightVal.val() == "0LBS") {
            isCheck = false;
            txtSelectWeightError.text("Please select weight");
        }

        //for feet slider
        if (SelectedFeetVal.val() == "") {
            isCheck = false;
            txtSelectFeetError.text("Please select Feet");
        }
        if (path.includes("/orders/new") || path.includes("/Orders/New")) {
            if ($("#adminQuoteUserId").val() == "") {
                $("#txtUserError").text("Please select user");
                isCheck = false;
            } else {
                $("#txtUserError").text("");
            }
        }
        if (isCheck) {
            tq.common.isBusy(true);
            var date = new Date();
            var time = date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
            var data;
            if (!path.includes("/orders/new") && !path.includes("/Orders/New")) {
                data = {
                    Equipment_Type: $(".equipments:checked").val().toString(),
                    Date: $("#txtPickUpDate").val() + " " + time, Weight: $("#SelectedWeightVal").val().trim(), Feet: parseInt($("#SelectedFeetVal").val()), IsWeb: true,
                    stops: [{
                        Address: $("#txtPickupAddress1").val(), Longitude: $("#pickUpLong").val(), Latitude: $("#pickUpLat").val(), city: $("#pickUpCity").val(),
                        state: $("#pickUpState").val(), ZipCode: $("#pickUpZip").val()
                    },
                    {
                        Address: $("#txtDeliveryAddress1").val(), Longitude: $("#deliveryLong").val(), Latitude: $("#deliveryLat").val(), city: $("#deliveryCity").val(),
                        state: $("#deliveryState").val(), ZipCode: $("#deliveryZip").val()
                    }]
                };
            } else {
                data = {
                    Equipment_Type: $(".equipments:checked").val().toString(),
                    Date: $("#txtPickUpDate").val() + " " + time, Weight: $("#SelectedWeightVal").val().trim(), Feet: parseInt($("#SelectedFeetVal").val()),
                    UserId: $("#adminQuoteUserId").val(), IsWeb: true, stops: [{
                        Address: $("#txtPickupAddress1").val(), Longitude: $("#pickUpLong").val(), Latitude: $("#pickUpLat").val(), city: $("#pickUpCity").val(),
                        state: $("#pickUpState").val(), ZipCode: $("#pickUpZip").val()
                    },
                    {
                        Address: $("#txtDeliveryAddress1").val(), Longitude: $("#deliveryLong").val(), Latitude: $("#deliveryLat").val(), city: $("#deliveryCity").val(),
                        state: $("#deliveryState").val(), ZipCode: $("#deliveryZip").val()
                    }]
                };
            }

            var response = tq.dataManager.postData(tq.urls.quoteApi, data);
            response.done(function (result) {
                tq.common.isBusy(false);
                if (result.status == true && result.result != null) {
                    if (!path.includes("/user/quote") && !path.includes("/orders/new") && !path.includes("/Orders/New")) {
                        location.href = "/user/quote?id=" + result.result.id;
                    }
                    else {
                        var amount = self.format2(result.result.quoteAmount, "$");
                        $('.spanTotalAmount').text(amount);
                        $('#id').val(result.result.id);
                        $('.spnPickUpMiles').text(result.result.miles);
                        $('.spnPickUpAvgDays').text(result.result.estimatedTime);
                    }
                } else {
                    if (result.message == "Daily Limit Exceed") {
                        $('.freeQuoteMsg').show();
                        $('.spanTotalAmount').text(0);
                    } else if (result.message == "Data Not Available") {
                        $('.dataNotAvailable').show();
                        $('.spanTotalAmount').text(0);
                    } else {
                        $('.dataNotAvailable').show();
                        $('#errorQuoteText').val(result.message);
                        $('.spanTotalAmount').text(0);
                    }
                    $('#id').val("");
                }
            });
        } else {
            tq.common.isBusy(false);
        }
    };

    self.events = function () {
        txtPickUpAddress.on("keypress", function (e) {
            if (e.which === 13)
                e.preventDefault();
        });
        txtDelivery.on("keypress", function (e) {
            if (e.which === 13)
                e.preventDefault();
        });
        equipmentType.on("keypress", function (e) {
            if (e.which === 13 && (txtPickUpAddress.val() == "" || txtDelivery.val() == ""))
                e.preventDefault();
        });
        btnGetQuote.on("keypress", function (e) {
            if (e.which === 13 && (txtPickUpAddress.val() == "" || txtDelivery.val() == ""))
                e.preventDefault();
        });
        btnGetQuote.on("click", function (e) {
            self.calculateQuote();
        });
        $('#GetTruckBook').unbind().click(function (e) {
            $('#txtPickupAddress1Error').text("");
            $('#txtDeliveryAddress1Error').text("");
            $('#txtDeliveryDateError').text("");
            $('#SelectedWeightError').text("");
            $('#txttrucktypeError').text("");
            $('#txttruckCategoryError').text("");
            $("#txtUserName").val("");
            $("#txtEmail").val("");
            $("#txtPhone").val("");
            $("#txtLoginPassword").val("");
            $("#txtConfirmPassword").val("");
            $("#txtErrUserName").text("");
            $("#txtErrEmail").text("");
            $("#txtErrPhone").text("");
            $("#PasswordError").text("");
            $("#ConfirmPasswordError").text("");
            //feet
            $('#SelectFeetError').text("");

            var result = false;
            $(".field-validation-valid").text("");
            if ($('#txtPickupAddress1').val().trim() == "") {
                $('#txtPickupAddress1Error').text("Pickup City and State is required");
                result = true;
            }
            //if ($('#txtPickupAddress1').val().trim() != "") {
            //    if ($('#txtPickupAddress1').val().split(",").length < 3) {
            //        $('#txtPickupAddress1Error').text("Enter the pickup City and State");
            //        result = true;
            //    }
            //}

            if ($('#txtDeliveryAddress1').val().trim() == "") {
                $('#txtDeliveryAddress1Error').text("Delivery City and State is required");
                result = true;
            }

            //if ($('#txtDeliveryAddress1').val().trim() != "") {
            //    if ($('#txtDeliveryAddress1').val().split(",").length < 3) {
            //        $('#txtDeliveryAddress1Error').text("Enter the delivery City and State");
            //        result = true;
            //    }
            //}

            if ($("#txtPickUpDate").val().trim() == "") {
                $('#txtDeliveryDateError').text("Pickup date is required field");
                result = true;
            }

            if ($("#SelectedWeightVal").val().trim() == "0LBS") {
                $("#SelectedWeightError").text("Please select weight.");
                result = true;
            }

            //feet
            if ($("#SelectedFeetVal").val().trim() == "0") {
                $("#SelectFeetError").text("Please select Feet.");
                result = true;
            }
            if ($('.equipments:checked').val() == undefined) {
                $("#txttrucktypeError").text("Please select truck type.");
                result = true;
            }
            if ($("#IsTruckNotAvailable").val() == "1") {
                $("#pickUpPhoneNumber").val("");
                $("#spnPhoneNumber").text("");
                result = true;
                $(".quoteNotAvailable").show();
            }
            if (path.includes("/orders/new") || path.includes("/Orders/New")) {
                if ($("#adminQuoteUserId").val() == "") {
                    $("#txtUserError").text("Please select user");
                    result = true;
                }
            }

            if (result == true) {
                return false;
            }
            else {
                if ($("#GetTruckBook").hasClass("paymentPage")) {
                    //Assign PickUp,Delivery City and State Info....
                    $("#txtPickUpCityInfo").val($('#txtPickupAddress1').val().split(',')[0]);
                    $("#txtPickUpStateInfo").val($("#txtPickupAddress1").val().split(',')[1]);
                    $("#txtDeliveryCityInfo").val($('#txtDeliveryAddress1').val().split(',')[0]);
                    $("#txtDeliveryStateInfo").val($('#txtDeliveryAddress1').val().split(',')[1]);
                    tq.common.isBusy(true);

                    if ($("#SelectedFeetVal").val().split(' ')[0] != $("#TruckLoad").val())
                        $("#TruckLoad").val($("#SelectedFeetVal").val().split(' ')[0]);
                    var jsonData = {
                        PickUpAddress1: $("#txtPickupAddress1").val(), DeliveryAddress1: $("#txtDeliveryAddress1").val(), TotalMiles: $(".spnPickUpMiles").text(), TotalPaymentAmount: $('.spanTotalAmount').text(), IsSuccess: true, PickUpCity: $('#txtPickupAddress1').val().split(',')[0], PickUpState: $("#txtPickupAddress1").val().split(',')[1],
                        Deliverycity: $('#txtDeliveryAddress1').val().split(',')[0], DeliveryState: $('#txtDeliveryAddress1').val().split(',')[1], EquipmentType: $('.equipments:checked').val(), IsEdit: "N",
                        Weigth: $("#SelectedWeightVal").val().trim(), AverageDays: $('.spnPickUpAvgDays').text(), PickUpDate: $("#txtPickUpDate").val().trim(),
                        TruckLoadType: $("#TruckLoadType").val(), TruckLoad: $("#TruckLoad").val(), OrderId: $('#hOrderId').val(), QuoteId: $('#id').val()
                    };

                    if ($('#id').val() != null && $('#id').val() != "" && $('#id').val() != undefined) {
                        var response = tq.dataManager.postData(tq.urls.orderCreate, jsonData);
                        response.done(function (data) {
                            var bookht = $('#step2-pickupinfo').height();
                            $(".map-ht").css({ "height": bookht });
                            $('#step1-bookingpage').hide();
                            $('#step2-pickupinfo').show();
                            $('#orderId').val(data.OrderId);

                            if (path.includes("/orders/new") || path.includes("/Orders/New")) {
                                $('.map-ht').hide();
                                $('#textPickupAddress').text($("#txtPickupAddress1").val());
                                $('#textDeliveryAddress').text($("#txtDeliveryAddress1").val());
                                $('#textType').text($('.equipments:checked').val());
                                $('#textDate').text($("#txtPickUpDate").val());
                                $('#textWeight').text($("#SelectedWeightVal").val());
                                $('#textFeet').text($("#SelectedFeetVal").val());
                            }
                            tq.common.isBusy(false);
                        });
                    } else {
                        tq.common.isBusy(false);
                        toastr.info("Please check your quote");
                    }
                }
            }
        });
    };

    self.format2 = function (n, currency) {
        return currency + " " + n.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
    }

    self.resetPickUpAddress = function () {
        $('#pickUpCity').val("");
        $('#pickUpState').val("");
        $('#pickUpLong').val("");
        $('#pickUpLat').val("");
        $('#pickUpZip').val("");
    }

    self.resetDeliveryAddress = function () {
        $('#deliveryCity').val("");
        $('#deliveryState').val("");
        $('#deliveryLong').val("");
        $('#deliveryLat').val("");
        $('#deliveryZip').val("");
    }

    self.init = function () {
        $('#btnSignUpForFreeQuote ,.quoteMsgClose').click(function () {
            $('.freeQuoteMsg').hide();
        });

        $('.quoteMsg ,.dataMsgClose').click(function () {
            $('.dataNotAvailable').hide();
        })

        if ($('#hid_verification').val() != "" && $('#hid_verification').val() != undefined) {
            $('.emailverification').show();
            $('#hid_verification').val("");
        }

        $('.close-popup').click(function () {
            $('.emailverification').hide();
        });

        var pathname = window.location.pathname;
        var path = pathname.toLowerCase();

        txtPickUpAddress = $("#txtPickupAddress1");
        txtDelivery = $("#txtDeliveryAddress1");
        txttrucktypeError = $("#txttrucktypeError");
        equipmentType = $(".equipments");
        PickupError = $("#txtPickupAddress1Error");
        DelieryError = $("#txtDeliveryAddress1Error");
        btnGetQuote = $("#btnGetQuote");
        bookpopup = $("#myModal");
        btnLogOut = $("#btnLogOut");
        txtSelectWeightError = $("#SelectedWeightError");
        txttruckCategoryError = $("#SelectFeetError");
        SelectedWeightVal = $("#SelectedWeightVal");
        ddlFootageSize = $("#ddlSize");

        //for slider
        SelectedFeetVal = $("#SelectedFeetVal");
        txtSelectFeetError = $("#SelectFeetError");

        $(document).on('click',
            '#equipmentType,#equipmentType1',
            function (e) {
                if (document.getElementById('equipmentType').checked) {
                    $('.spnPickUpTruck').text(document.getElementById('equipmentType').value);
                } else {
                    if (document.getElementById('equipmentType1').checked) {
                        $('.spnPickUpTruck').text(document.getElementById('equipmentType1').value);
                    }
                }
                if (e.handled !== true) // This will prevent event triggering more then once
                {
                    if (path.includes("/user/quote") ||
                        path.includes("/orders/new") ||
                        path.includes("/Orders/New")) {
                        self.calculateQuote();
                        e.handled = true;
                    }
                }
            });

        $(document).on('changeDate', '#txtPickUpDate', function (e, ui) {
            e.preventDefault();
            if (path.includes("/user/quote") || path.includes("/orders/new") || path.includes("/Orders/New"))
                self.calculateQuote();
        });

        $(document).on('slidechange', '#weightSlider', function (e, ui) {
            if (e.handled !== true) // This will prevent event triggering more then once
            {
                if (path.includes("/user/quote") || path.includes("/orders/new") || path.includes("/Orders/New")) {
                    self.calculateQuote();
                    e.handled = true;
                }
            }
        });

        $(document).on('slidechange', '#ddlSize', function (e, ui) {
            if (e.handled !== true) // This will prevent event triggering more then once
            {
                if (path.includes("/user/quote") || path.includes("/orders/new") || path.includes("/Orders/New")) {
                    self.calculateQuote();
                    e.handled = true;
                }
            }
        });

        self.events();

        google.maps.event.addDomListener(window, 'load', function () {
            var options = {
                //types: ['(cities)'],
                componentRestrictions: { country: "us" }
            };
            var places = new google.maps.places.Autocomplete(document.getElementById('txtPickupAddress1'), options);
            google.maps.event.addListener(places, 'place_changed', function (e) {
                self.resetPickUpAddress();
                var placeData = places.getPlace();
                if (placeData != undefined) {
                    $('#pickUpLong').val(placeData.geometry.location.lng());
                    $('#pickUpLat').val(placeData.geometry.location.lat());

                    $.each(placeData.address_components,
                        function (k, v) {
                            if (v.types[0] == "locality") {
                                $('#pickUpCity').val(v.long_name);
                            }
                            if (v.types[0] == "administrative_area_level_1") {
                                $('#pickUpState').val(v.short_name);
                            }
                            if (v.types[0] == "postal_code") {
                                $('#pickUpZip').val(v.long_name);
                            }
                        });
                }
                tq.common.getGoogleMapLocation($('#txtPickupAddress1'), $('#txtDeliveryAddress1'));
                if (path.includes("/user/quote") || path.includes("/orders/new") || path.includes("/Orders/New"))
                    self.calculateQuote(e);
            });
        });
        google.maps.event.addDomListener(window, 'load', function () {
            $('#IndexLoader').fadeOut(1000);
            var options = {
                //types: ['(cities)'],
                componentRestrictions: { country: "us" }
            };
            var places = new google.maps.places.Autocomplete(document.getElementById('txtDeliveryAddress1'), options);
            google.maps.event.addListener(places, 'place_changed', function (e) {
                self.resetDeliveryAddress();
                var placeData = places.getPlace();
                if (placeData != undefined) {
                    $('#deliveryLong').val(placeData.geometry.location.lng());
                    $('#deliveryLat').val(placeData.geometry.location.lat());

                    $.each(placeData.address_components,
                        function (k, v) {
                            if (v.types[0] == "locality") {
                                $('#deliveryCity').val(v.long_name);
                            }
                            if (v.types[0] == "administrative_area_level_1") {
                                $('#deliveryState').val(v.short_name);
                            }
                            if (v.types[0] == "postal_code") {
                                $('#deliveryZip').val(v.long_name);
                            }
                        });
                }
                tq.common.getGoogleMapLocation($('#txtPickupAddress1'), $('#txtDeliveryAddress1'));
                if (path.includes("/user/quote") || path.includes("/orders/new") || path.includes("/Orders/New"))
                    self.calculateQuote(e);
            });
        });


        var pac_pickup = document.getElementById('txtPickupAddress1');
        if (pac_pickup != null && pac_pickup != undefined && pac_pickup != "") {
            document.getElementById('txtPickupAddress1').onfocusout = function () {
                google.maps.event.trigger(this, 'keydown', {
                    keyCode: 40
                });
            };
            (function pacSelectFirst(input) {
                // store the original event binding function
                var _addEventListener = (input.addEventListener) ? input.addEventListener : input.attachEvent;

                function addEventListenerWrapper(type, listener) {
                    // Simulate a 'down arrow' keypress on hitting 'return' when no pac suggestion is selected,
                    // and then trigger the original listener.

                    if (type == "keydown") {

                        var orig_listener = listener;
                        listener = function (event) {
                            var suggestion_selected = $(".pac-item-selected").length > 0;
                            var keyCode = event.keyCode || event.which;
                            if ((keyCode === 13 || keyCode === 9) && !suggestion_selected) {

                                var simulated_downarrow = $.Event("keydown", {
                                    keyCode: 40,
                                    which: 40
                                })
                                orig_listener.apply(input, [simulated_downarrow]);
                            } else if (event.type === 'blur') {
                                pac_pickup.value = $('.pac-container .pac-item:first-child').text();
                            }
                            orig_listener.apply(input, [event]);
                        };
                    }

                    // add the modified listener
                    _addEventListener.apply(input, [type, listener]);
                }

                if (input.addEventListener)
                    input.addEventListener = addEventListenerWrapper;
                else if (input.attachEvent)
                    input.attachEvent = addEventListenerWrapper;

            })(pac_pickup);
        }

        var pac_input = document.getElementById('txtDeliveryAddress1');
        if (pac_input != null && pac_input != undefined && pac_input != "") {
            document.getElementById('txtDeliveryAddress1').onfocusout = function () {
                google.maps.event.trigger(this, 'keydown', {
                    keyCode: 40
                });
            };
            (function pacSelectFirst(input) {
                // store the original event binding function
                var _addEventListener = (input.addEventListener) ? input.addEventListener : input.attachEvent;

                function addEventListenerWrapper(type, listener) {
                    // Simulate a 'down arrow' keypress on hitting 'return' when no pac suggestion is selected,
                    // and then trigger the original listener.
                    if (type == "keydown") {
                        var orig_listener = listener;
                        listener = function (event) {
                            var suggestion_selected = $(".pac-item-selected").length > 0;
                            var keyCode = event.keyCode || event.which;
                            if ((keyCode === 13 || keyCode === 9) && !suggestion_selected) {
                                var simulated_downarrow = $.Event("keydown", {
                                    keyCode: 40,
                                    which: 40
                                })
                                orig_listener.apply(input, [simulated_downarrow]);
                            } else if (event.type === 'blur') {
                                pac_input.value = $('.pac-container .pac-item:first-child').text();
                            }
                            orig_listener.apply(input, [event]);
                        };
                    }

                    // add the modified listener
                    _addEventListener.apply(input, [type, listener]);
                }

                if (input.addEventListener)
                    input.addEventListener = addEventListenerWrapper;
                else if (input.attachEvent)
                    input.attachEvent = addEventListenerWrapper;

            })(pac_input);
        }
        //map-script
        var httop = $(".top_dec").height();
        $(".cn-map1").css({
            height: httop
        });

        $('#adminDashboardLoad,#adminOrderDetailsLoad,#adminMarketRateLoad,#adminMarketCityLoad,#adminUserListLoad,#adminCurrentNotificationLoad,#adminReplyNotificationLoad,#adminProfileLoad,#adminLogOutLoad,#reports,#adminFootagLoad,#adminWeightLoad').click(function () {
            $("#isAdminBusy").css("display", "inline-block");
        })

        //*Step-1 Booking Info Start*
        var sliderCreate = false;
        $(function () {
            function refreshSliderIcon() {
                var weightSlider = $("#weightSlider").slider("value");
            }

            $("#weightSlider").slider({
                value: 0,
                min: 0,
                orientation: "horizontal",
                range: "min",
                max: 45000,
                step: 1000,
                slide: function (event, ui) {
                    refreshSliderIcon();
                    $("#SelectedWeightVal").val(ui.value + "LBS");
                }, create: function (event, ui) {
                    sliderCreate = true;
                },
            }).unbind('slidechange', function (event, ui) { });

            if (!path.includes("/user/quote") && !path.includes("/orders/new") && !path.includes("/Orders/New")) {
                var weight = $("#weightSlider").slider("value");
                $("#SelectedWeightVal").val(weight + "LBS");
            }
            else {
                var weight = $('#SelectedWeightVal').val().split('L');
                var slideLength = weight[0] / 1000;
                var slideLeftWidth = "2.22222" * slideLength;
                slideLeftWidth = slideLeftWidth + "%";
                $('#weightSlider .ui-slider-handle').css('left', slideLeftWidth)
                $("#weightSlider .ui-slider-range").css("width", slideLeftWidth)
                $("#weightSlider .ui-slider-range").css("background", "#1a74bc")
            }
        });
        //Feet
        var sliderFeetCreate = false;
        $(function () {
            function refreshSliderFeetIcon() {
                var weightSlider = $("#ddlSize").slider("value");
            }
            $("#ddlSize").slider({
                value: 0,
                min: 4,
                orientation: "horizontal",
                range: "min",
                max: 53,
                step: 1,
                slide: function (event, ui) {
                    refreshSliderFeetIcon();
                    $("#SelectedFeetVal").val(ui.value + " FEET");
                }, create: function (event, ui) {
                    sliderFeetCreate = true;
                },
            }).unbind('slidechange', function (event, ui) { });
            if (!path.includes("/user/quote") && !path.includes("/orders/new") && !path.includes("/Orders/New"))
                $("#SelectedFeetVal").val($("#ddlSize").slider("value") + " FEET");
            else {
                var feet = $('#SelectedFeetVal').val().split(' ')[0];
                if (feet > 4) {
                    var slideFeetLeftWidth = (feet - 4) * 100 / (53 - 4);
                    slideFeetLeftWidth = slideFeetLeftWidth + "%";
                    $('#ddlSize .ui-slider-handle').css('left', slideFeetLeftWidth)
                    $("#ddlSize .ui-slider-range").css("width", slideFeetLeftWidth)
                    $("#ddlSize .ui-slider-range").css("background", "#1a74bc")
                } else {
                    $("#ddlSize .ui-slider-range").css("background", "#1a74bc")
                }
            }
        });



        var nowTemp = new Date();
        var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

        var date = $("#txtPickUpDate").datepicker({      //For Pickup Date
            onRender: function (date) {
                return date.valueOf() < now.valueOf() ? 'disabled' : '';
            },

        }).on('changeDate', function (ev) {
            date.hide();
            $('#txtDeliveryDateError').text("");
            var TextDate = new Date($(this).val());
            var DateVar = TextDate.getDate();
            locale = "en-us",
               month = TextDate.toLocaleString(locale, { month: "short" });
            var YearVar = TextDate.getFullYear();
            $(".spnPickUpDate").text('' + month + ' ' + DateVar + ', ' + YearVar + '' + " (Shipment takes 1-3 business days to ship depending on truck availability)");
        }).data('datepicker');

        if ($("#calculateQuote").val()) {
            self.calculateQuote();
        }
    };
}

//End of Quote result

//quote View Model code

function QuoteViewModel() {
    var self = this;

    var txtPickupAddress1 = null;
    var txtDeliveryAddress1 = null;
    var txtPickUpDate = null;
    var equipmentType = null;
    var SelectedWeightVal = null;
    var btnQuote = null;
    var quotePricePanel = null;
    var Login = null;
    var SelectedFeetVal = null;

    //*Step-4 Payment Info Start*
    self.loadPayment = function () {
        var response = tq.dataManager.getData(tq.urls.quotePayment + $('#orderId').val());
        response.done(function (data) {
            $('#divOrderPaymentInfo').empty();
            $('#divOrderPaymentInfo').append(data);
            $('#step1-bookingpage').hide();
            $('#step2-pickupinfo').hide();
            $('#step3-deliveryinfo').hide();
            $('#step4-paymentinfo').show();

            var bookht = $('#step4-paymentinfo').height();
            $(".map-ht").css({ "height": bookht });

            tq.common.isBusy(false);
        });
    }

    //*Step-5 Shipping Info Start*
    self.bindShippingDetails = function () {
        var data = { OrderId: $('#orderId').val() };
        var response = tq.dataManager.postData(tq.urls.getOrderShipmentInfo, data);
        response.done(function (data) {
            var FirstName = data.PickUpFirstName == null ? "" : data.PickUpFirstName;
            var LastName = data.PickUpLastName == null ? "" : data.PickUpLastName;
            var LoadType = data.TQ_Equipment_Id == 1 ? "Dry Vans" : "Flat beds";
            $('.spPickupName').text(FirstName + " " + LastName);
            $('.spPickupCompanyName').text(data.PickUpCompanyName == null ? "" : data.PickUpCompanyName);
            $('.spPickupAddress').text(data.PickUpExactAddr == null ? "" : data.PickUpExactAddr);
            $('.spPickupState').text(data.PickUpState);
            $('.spPickupCity').text(data.PickUpCity);
            $('.spPickupDate').text(data.PickUpApptNote);
            $('.spCommodity').text(data.ShippingName);
            $('.spPickupType').text(LoadType);
            $('.spPickupWeight').text(data.Weigth);
            $('.spDeliveryName').text(data.DeliveryFirstName + " " + data.DeliveryLastName);
            $('.spDeliveryCompanyName').text(data.DeliveryCompanyName == null ? "" : data.DeliveryCompanyName);
            $('.spDeliveryAddress').text(data.DeliveryExactAddr);
            $('.spDeliveryState').text(data.DeliveryState);
            $('.spDeliveryCity').text(data.DeliveryCity);
            $('.spDeliveryDate').text("");
            $('.spDeliveryType').text(LoadType);
            $('.spDeliveryWeight').text(data.Weigth);

            var trackOrder = "/User/Order/GetTrackOrdersByID?OrderId=" + $('#orderId').val();
            $('.trackyourorder_').html("<a href=" + trackOrder + ">" + "Track your Order" + "</a>");

            tq.common.isBusy(false);
        });
    }
    //*Step-5 Shipping Info End*

    //*Valditation Start*
    self.isValidEmail = function () {
        var isValid = true;
        if ($('#snEmail').text() == "") {
            if ($('#txtDeliveryEmail').val().trim() != "") {
                isValid = tq.common.validateEmail($('#txtDeliveryEmail').val().trim());
                if (!isValid) {
                    $('#snEmail').text('Please enter a valid email address');
                    $('#snEmail').show();
                    isValid = false;
                }
            }
        }
        return isValid;
    }

    self.isValidPhone = function () {
        var isValid = true;
        if ($('#snPhoneNumber').text() == "") {
            isValid = self.validPhoneNumber($('#txtDeliveryPhone').val().trim());
            if (!isValid) {
                $('#snPhoneNumber').text('Please enter a valid phone number');
                $('#snPhoneNumber').show();
                isValid = false;
            }
        }
        return isValid;
    }

    self.isValidPickUpPhone = function () {
        var isValid = true;
        if ($('#spnPickUpContactno').text() == "") {
            isValid = self.validPhoneNumber($('#txtPickUpContactNo').val().trim());
            if (!isValid) {
                $('#spnPickUpContactno').text('Please enter a valid phone number');
                $('#spnPickUpContactno').show();
                isValid = false;
            }
        }
        return isValid;
    }

    self.checkMandatory = function () {
        var isAllowed = true;
        $('.field-validation-valid').hide();
        $('.field-validation-valid').text("");
        $('.txt-mandatory').each(function () {
            if ($(this).val() == "") {
                $(this).closest('div').find('span').text($(this).closest('div').find('span').attr('data-valmsg') + ' is required');
                $(this).closest('div').find('span').show();
                isAllowed = false;
            }

        });
        return isAllowed;
    }

    self.checkMandatoryForPickUpInfo = function () {
        var isAllowed = true;
        $('.field-validation-valid').hide();
        $('.field-validation-valid').text("");
        $('.txt-mandatory-pickup').each(function () {
            if ($(this).val() == "") {
                $(this).closest('div').find('span').text($(this).closest('div').find('span').attr('data-valmsg') + ' is required');
                $(this).closest('div').find('span').show();
                isAllowed = false;
            }
        });
        return isAllowed;
    }

    self.validPhoneNumber = function (value) {
        var expr = /^[0-9]{1,10}$/;
        return expr.test(value);
    }

    self.init = function () {
        txtPickupAddress1 = $("#txtPickupAddress1");
        txtDeliveryAddress1 = $("#txtDeliveryAddress1");
        txtPickUpDate = $("#txtPickUpDate");
        equipmentType = $("#equipmentType");
        SelectedWeightVal = $("#SelectedWeightVal");
        btnQuote = $("#btnQuote");
        Login = $("#login");
        SelectedFeetVal = $("SelectedFeetVal");
        $('#txtShippingName').val('');
        $('#txtPickUpCompanyName').val('');
        $('#txtPickUpFirstName').val('');
        $('#txtPickUpLastName').val('');
        $('#txtPickUpContactNo').val('');
        $('#txtPickUpExt').val('');
        $('#txtPickUpContactEmail').val('');
        $('#txtPickUpExactAddr').val('');
        $('#txtPickUpSpecialReq').val('');
        $('#txtDeliveryCompanyName').val('');
        $('#txtDeliveryFirstName').val('');
        $('#txtDeliveryLastName').val('');
        $('#txtDeliveryPhone').val('');
        $('#txtDeliveryExt').val('');
        $('#txtDeliveryEmail').val('');
        $('#txtDeliveryExactAddr').val('');
        $('#txtDeliverySpecialRequirements').val('');
        $("#txtPickUpDate").keypress(false);
        $("#IsTruckNotAvailable").val(0);
        var truckFeet = $("#SelectedFeetVal").val().split(" ")[0];
        var weightLbs = $('#SelectedWeightVal').val().split("L")[0];

        //code for slider-feet

        $('.spnPickUpMilesTxt').show();

        $("#TruckLoadType").val("FL");
        $("#TruckLoad").val(truckFeet);

        if (weightLbs < 20000) {
            $('#TruckLoadType').val("LTL");
        }

        $('.quoteMsg ,.quoteMsgClose').click(function () {
            $('.quoteMsg').hide();
        });

        $('.quoteMsg ,.dataMsgClose').click(function () {
            $('.dataNotAvailable').hide();
        });

        $('#btnClickPhoneNo').click(function () {
            var result = false;
            if ($("#pickUpPhoneNumber").val().trim() == "") {
                $('#spnPhoneNumber').text("Phone number is required");
                result = true;
            }
            if (result == false) {

                tq.common.isBusy(true);
                var data = {
                    Orgin: $('#txtPickupAddress1').val().trim(),
                    Destination: $('#txtDeliveryAddress1').val().trim(),
                    PhoneNumber: $('#pickUpPhoneNumber').val()
                };
                var response = tq.dataManager.postData(tq.urls.bookQuoteByPhoneNo, data);
                response.done(function (data) {
                    $("#pickUpPhoneNumber").val("");
                    $("#spnPhoneNumber").text("");
                    $('.quoteNotAvailable').hide();
                    tq.common.isBusy(false);
                });
                $('.quoteNotAvailable').hide();
            }
        });

        $('.quoteMsgClose').click(function () {
            $('.quoteNotAvailable').hide();
        });

        $("#pickUpPhoneNumber").keypress(function (evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        });

        $(".view-password").click(function () {
            if ($('#LoginPassword').val().trim() != "") {
                if ($(".view-password").hasClass("view")) {
                    $('.no-view').hide();
                    $('#LoginPassword').prop("type", "text");
                    $('.view-password').removeClass('view');
                } else {
                    $('.no-view').show();
                    $('#LoginPassword').prop("type", "password");
                    $('.view-password').addClass('view');
                }
            }
        });

        //*Step-1 Booking Info End*

        //*Step-2 PickUp Info Start*
        $('.orderPickUpSaveAndContinue').click(function () {

            if ($('#txtPickUpContactEmail').val().trim() != "") {

                var isPickUpEmailValid = tq.common.validateEmail($('#txtPickUpContactEmail').val().trim());
                if (!isPickUpEmailValid) {
                    $('#spnPickUpEmailId').text('Please enter a valid email address');
                    $('#spnPickUpEmailId').show();
                    return false;
                }
            }

            if ((self.checkMandatoryForPickUpInfo()) && (self.isValidPickUpPhone())) {
                tq.common.isBusy(true);
                var data = {
                    OrderId: $('#orderId').val(),
                    ShippingName: $('#txtShippingName').val(),
                    PickUpCompanyName: $('#txtPickUpCompanyName').val(),
                    PickUpFirstName: $('#txtPickUpFirstName').val(),
                    PickUpLastName: $('#txtPickUpLastName').val(),
                    PickUpPhone: $('#txtPickUpContactNo').val(),
                    PickUpExt: $('#txtPickUpExt').val(),
                    PickUpEmail: $('#txtPickUpContactEmail').val(),
                    PickUpExactAddr: $('#txtPickUpExactAddr').val(),
                    PickUpSpecialRequirements: $('#txtPickUpSpecialReq').val()
                };
                var response = tq.dataManager.postData(tq.urls.orderPickUpInfo, data);
                response.done(function (data) {
                    $('#step1-bookingpage').hide();
                    $('#step2-pickupinfo').hide();
                    $('#step3-deliveryinfo').show();
                    var bookht = $('#step3-deliveryinfo').height();
                    $(".map-ht").css({ "height": bookht });
                    tq.common.isBusy(false);
                });
            }
        });

        $('.orderPickUpBack').click(function () {
            tq.common.isBusy(true);
            $('#step1-bookingpage').show();

            var bookht = $("#step1-bookingpage").height();
            $(".map-ht").css({ "height": bookht });

            $('#step2-pickupinfo').hide();
            tq.common.isBusy(false);
        });

        //*Step-2 PickUp Info End*

        //*Step-3 Delivery Info Start*

        $('.orderDeliveryBack').click(function () {
            tq.common.isBusy(true);
            $('#step1-bookingpage').hide();
            $('#step3-deliveryinfo').hide();
            $('#step2-pickupinfo').show();
            var bookht = $('#step2-pickupinfo').height();
            $(".map-ht").css({ "height": bookht });
            tq.common.isBusy(false);
        })

        $('.orderPickUp_Delivery_Back').click(function () {
            tq.common.isBusy(true);
            $('#step3-deliveryinfo').hide();
            $('#step2-pickupinfo').hide();
            $('#step1-bookingpage').show();
            var bookht = $("#step1-bookingpage").height();
            $(".map-ht").css({ "height": bookht });
            tq.common.isBusy(false);
        })

        $('.txt-mandatory').on("keyup", function () {
            if ($(this).closest('div').find('span').text() != "")
                $(this).closest('div').find('span').text("");
        });

        $('.number-only').on("keydown", function (e) {
            if ((e.which >= 48 && e.which <= 57) || e.which == 8 || e.which == 9 || e.which == 46 || e.which == 39 | e.which == 37 || (e.which == 16 && e.which == 9))
                return true;
            else
                return false;
        });

        $('.orderDeliverySaveAndCont').click(function () {
            if (self.checkMandatory() && self.isValidPhone() && self.isValidEmail()) {
                tq.common.isBusy(true);
                var model = {};
                model.OrderId = $('#orderId').val(),
                model.DeliveryCompanyName = $('#txtDeliveryCompanyName').val();
                model.DeliveryFirstName = $('#txtDeliveryFirstName').val();
                model.DeliveryLastName = $('#txtDeliveryLastName').val();
                model.DeliveryPhone = $('#txtDeliveryPhone').val();
                model.DeliveryExt = $('#txtDeliveryExt').val();
                model.DeliveryEmail = $('#txtDeliveryEmail').val();
                model.DeliveryExactAddr = $('#txtDeliveryExactAddr').val();
                model.DeliverySpecialRequirements = $('#txtDeliverySpecialRequirements').val();
                var data = { UserOrderInfo: model };
                var response = tq.dataManager.postData(tq.urls.orderDeliveryInfo, data);
                response.done(function (data) {
                    if (!data) {
                        tq.common.isBusy(false);
                    }
                    else {
                        self.loadPayment();
                    }
                });
            }
        });

        //*Step-3 Delivery Info End*

        //*Step-4 Payment Info Start*

        $(document).on('click', '#btnPayCancel', function () {
            tq.common.isBusy(true);
            tq.common.redirectToUrl("/user/quote");
        });

        $(document).on('keyup', '#Number', function () {
            var number = $('#Number').val();
            var cardType = tq.common.getCardType(number);
            $('#selectedCardType_').val(cardType);
        });

        $(document).on('click', '#btnPayNow', function () {
            $('.paymentFailureMsg').hide();
            $('.paymentSuccessMsg').hide();
            var errorSummary = false;
            if ($('#Number').val() == "") {
                $('#spnNumber').show();
                $('#spnNumber').html("Card number is requied.\n");
                errorSummary = true;
                return false;
            }
            else {
                $('#spnNumber').hide();
            }
            if ($('#Name').val() == "") {
                $('#spnName').show();
                $('#spnName').html("Card name is requied.\n");
                errorSummary = true;
                return false;
            }
            else {
                $('#spnName').hide();
            }
            if ($('#SelectedMonth').val() == "0") {
                $('#cardExpireMonth').show();
                $('#cardExpireMonth').html("Expire month is requied.\n");
                errorSummary = true;
                return false;
            }
            else {
                $('#cardExpireMonth').hide();
            }
            if ($('#SelectedYear').val() == "0") {
                $('#cardExpireYear').show();
                $('#cardExpireYear').html("Expire year is requied.\n");
                errorSummary = true;
                return false;
            }
            else {
                $('#cardExpireYear').hide();
            }

            if ($('#Cvc').val() == "") {
                $('#cardCVV').show();
                $('#cardCVV').html("Card number is requied.\n");
                errorSummary = true;
                return false;
            }
            else {
                $('#cardCVV').hide();
            }

            if (!errorSummary) {
                var Model = {};
                Model.Name = $('#Name').val();
                Model.Number = $('#Number').val();
                Model.SelectedMonth = $('#SelectedMonth').val();
                Model.SelectedYear = $('#SelectedYear').val();
                Model.Cvc = $('#Cvc').val();
                Model.isSave = $('#payCheckbox').is(":checked");
                Model.SelectedCardType = $('#selectedCardType_').val();
                Model.PayAmount = $('#PayAmount').val();
                Model.CardExist = "N";
                Model.OrderId = $("#orderId").val();
                tq.common.isBusy(true);
                window.setTimeout(function (event) {
                    $.post(tq.urls.paymentInvoice, Model, function (data) {
                        if (data.Status == true) {
                            tq.common.isBusy(false);
                            if (data.IsPayment == true) {
                                $('#hOrderNo_').val(data.OrderNo);
                                $('.paymentSuccessMsg').show();
                            }
                            else if (data.IsPayment == false) {
                                $('#FailureMsgInfo').html("<p>" + "Reson for failure:" + "</p>");
                                $('#FailureMsgInfo').html("<p>" + data.FailureInfo + "</p>");
                                $('.paymentFailureMsg').show();
                            }
                        }
                        else {
                            window.location = "home.html";
                        }
                    });
                }, 100);
            }
        });

        $('#btnFailureTryAgain,.paymentPopUpClose').click(function () {
            $('.paymentFailureMsg').hide();
        })

        $(document).on('click', '.paySavedCardDetails', function () {
            $('.paymentFailureMsg').hide();
            $('.paymentSuccessMsg').hide();

            var errorSummary = false;

            var id = $(this).attr('cardvalue');
            var cardId = $(this).attr('data-cardid');

            if ($("#savedcardcvv_" + id).val() == "") {
                $('#txtSavedcardCvv_' + id).show();
                $('#txtSavedcardCvv_' + id).html("CVV number is requied.\n");
                errorSummary = true;
                return false;
            }
            else {
                $('#txtSavedcardCvv_' + id).hide();
            }

            if (!errorSummary) {

                var Model = {};
                Model.Cvc = $("#savedcardcvv_" + id).val();
                Model.PayAmount = $('#PayAmount').val();
                Model.CardExist = "Y";
                Model.ExistingCardId = cardId;
                Model.OrderId = $("#orderId").val();
                tq.common.isBusy(true);
                window.setTimeout(function (event) {
                    $.post(tq.urls.paymentInvoice, Model, function (data) {
                        if (data.Status == true) {
                            tq.common.isBusy(false);
                            if (data.IsPayment == true) {
                                $('#hOrderNo_').val(data.OrderNo);
                                $('.paymentSuccessMsg').show();
                            }
                            else if (data.IsPayment == false) {
                                $('#FailureMsgInfo').html("<p>" + "Reson for failure:" + "</p>");
                                $('#FailureMsgInfo').html("<p>" + data.FailureInfo + "</p>");
                                $('.paymentFailureMsg').show();
                            }
                        }
                        else {
                            window.location = "home.html";
                        }
                    });
                }, 100);
            }

        });

        $('.paymentSuccessYes').click(function () {
            tq.common.isBusy(true);
        });

        $('#PaymentSuccess_Order').click(function () {
            tq.common.isBusy(true);
            $('.paymentSuccessMsg').hide();
            $('#step1-bookingpage').hide();
            $('#step2-pickupinfo').hide();
            $('#step3-deliveryinfo').hide();
            $('#step4-paymentinfo').hide();
            $('#step5-shipmentinfo').show();

            var shipheight = $('.all-ship-ht').height();
            $(".ship-to-delivery").css({ "height": shipheight });
            var bookht = $('#step5-shipmentinfo').height();
            $(".map-ht").css({ "height": bookht });
            self.bindShippingDetails();
        });
        //*Step-4 Payment Info End*  
    };
};

//end quote view model code

//end quote and order code

//Display google map

function MapViewModel() {
    var self = this;

    self.size = function () {
        var win = $(window).height();
        $(".banner").css({ "height": win - 122 });
    }

    self.mapht = function () {
        var bookht = $('.book-ht').height();
        $(".map-ht").css({ "height": bookht });
        $(".map-loader").css({ "height": bookht });

    }

    self.initMap = function () {
        self.size();
        self.mapht();
    }

    self.init = function () {
        var directionsService = new google.maps.DirectionsService;
        var directionsDisplay = new google.maps.DirectionsRenderer;
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 7,
            center: new google.maps.LatLng(41.85, -87.65)
        });
        directionsDisplay.setMap(map);
        self.calculateAndDisplayRoute(directionsService, directionsDisplay);
    }

    self.calculateAndDisplayRoute = function (directionsService, directionsDisplay) {
        directionsService.route({
            origin: $("#txtPickupAddress1").val(),
            destination: $("#txtDeliveryAddress1").val(),
            travelMode: 'DRIVING'
        }, function (response, status) {
            if (status === 'OK') {
                directionsDisplay.setDirections(response);
            } else {
            }
        });
    }

    // When the window has finished loading create our google map below
    google.maps.event.addDomListener(window, 'load', self.init());
}

//end of display google map code

//Track Order page
function TrackOrderViewModel() {
    var self = this;

    self.init = function () {
        $('#IndexLoader').fadeOut(500);
        $('#collapse_hradmin_0').addClass("in");
        $('#trackOrderFirstTag').removeClass("collapsed");

        $('.nav a').click(function (e) {
            $(this).tab('show');
            var tabContent = '#tabContent_' + this.id;
            $('#tabContent_personal').hide();
            $('#tabContent_financial').hide();
            $(tabContent).show();
        });

        var count = 0;
        $(".arrow").click(function () {
            if (count > 0) {
                if ($(this).hasClass("arrow1")) {
                    $(this).removeClass("arrow1");
                    $(this).parent().parent().parent().find('.order-additional-col').css({ 'display': 'block' });
                    $(this).find('.order-arrow').css({ 'display': 'none' });
                    $(this).find('.order-arrow1').css({ 'display': 'inline-block' });
                } else {
                    $(this).addClass("arrow1");
                    $(this).parent().parent().parent().find('.order-additional-col').css({ 'display': 'none' });
                    $(this).find('.order-arrow').css({ 'display': 'inline-block' });
                    $(this).find('.order-arrow1').css({ 'display': 'none' });
                }
            } else {
                $(this).removeClass("arrow1");
                $(this).parent().parent().parent().find('.order-additional-col').css({ 'display': 'block' });
                $(this).find('.order-arrow').css({ 'display': 'none' });
                $(this).find('.order-arrow1').css({ 'display': 'inline-block' });
                count = count + 1;
            }

        });

        $("#arrowChange").click(function () {
            if (arrowChange == "arrow") {
                $("#arrowChange").removeClass("arrow");
                $("#arrowChange").addClass("arrow1");
                arrowChangeCount = +arrowChangeCount + 1;
                if (arrowChangeCount == 2) {
                    arrowChange = "arrow1";
                    arrowChangeCount = 0;
                }
            }

            if (arrowChange == "arrow1") {
                $("#arrowChange").removeClass("arrow1");
                $("#arrowChange").addClass("arrow");
                arrowChangeCount = +arrowChangeCount + 1;
                if (arrowChangeCount == 2) {
                    arrowChange = "arrow";
                    arrowChangeCount = 0;
                }
            }

        });

        $('#btnTrackOrder-Search').click(function () {
            self.searchTrackOrderEvent();

        });

        $(document).on("keypress", "#txtTrackOrder-Search", function (event) {
            if (event.keyCode == 13) {
                self.searchTrackOrderEvent();
            }
        });

        $('.shipmentclose-popup').click(function () {
            $('.shipmentclosepopup').hide();
        });

        $('.save-Order-Ques').click(function () {

            var incId = $(this).attr('data-subqus-IncreNo');
            var orderNo = $(this).attr('data-subqus-orderno');
            $('.shipmentclosepopup').hide();

            tq.common.isBusy(true);
            var data = { subject: $('#ddlQuesSub_' + incId).val(), message: $('#querySubject_' + incId).val(), orderNo: orderNo };
            var response = tq.dataManager.postData(tq.urls.saveQuestionForQus, data);
            response.done(function (data) {
                if (data.Status == true) {
                }
                tq.common.isBusy(false);

                $('.trackOrderCancelPopUpClose').click(function () {
                    $('.trackOrderDelete-PopUp').hide();
                });
            });
            return false;

        });

        $('.deleteThis').click(function () {
            id = $(this).attr('data-id');
            $(this).attr('data-attr')
            $(this).attr('data-orderno')
            $('.trackOrderCancelPopUp_' + id).show();
        });

        $('.trackOrderCancelPopUpClose').click(function () {
            $('.trackOrderDelete-PopUp').hide();
        });

        $('.shipment-qns').click(function () {
            var id = $(this).attr('data-shipincno');
            $('textarea').val("");
            $('.shipMentQusPopup_' + id).show();

        });
    }

    self.searchTrackOrderEvent = function () {
        tq.common.isBusy(true);
        var data = { orderNo: $('#txtTrackOrder-Search').val().trim() };
        var response = tq.dataManager.postData(tq.urls.trackOrderPartial, data);
        response.done(function (result) {
            $('#divSearchTrackOrder').html("");
            $('#divSearchTrackOrder').html(result);

            $('.deleteThis').click(function () {
                var id = $(this).attr('data-id');
                $(this).attr('data-attr')
                $(this).attr('data-orderno')
                $('.trackOrderCancelPopUp_' + id).show();
            });

            $('.shipment-qns').click(function () {
                var id = $(this).attr('data-shipincno');
                $('.shipMentQusPopup_' + id).show();
            });

            $('.shipmentclose-popup').click(function () {
                $('.shipmentclosepopup').hide();
            });
            var count = 0;
            $(".arrow").click(function () {
                if (count > 0) {
                    if ($(this).hasClass("arrow1")) {
                        $(this).removeClass("arrow1");
                        $(this).parent().parent().parent().find('.order-additional-col').css({ 'display': 'block' });
                        $(this).find('.order-arrow').css({ 'display': 'none' });
                        $(this).find('.order-arrow1').css({ 'display': 'inline-block' });
                    } else {
                        $(this).addClass("arrow1");
                        $(this).parent().parent().parent().find('.order-additional-col').css({ 'display': 'none' });
                        $(this).find('.order-arrow').css({ 'display': 'inline-block' });
                        $(this).find('.order-arrow1').css({ 'display': 'none' });
                    }
                } else {
                    $(this).removeClass("arrow1");
                    $(this).parent().parent().parent().find('.order-additional-col').css({ 'display': 'block' });
                    $(this).find('.order-arrow').css({ 'display': 'none' });
                    $(this).find('.order-arrow1').css({ 'display': 'inline-block' });
                    count = count + 1;
                }

            });

            $("#arrowChange").click(function () {
                if (arrowChange == "arrow") {
                    $("#arrowChange").removeClass("arrow");
                    $("#arrowChange").addClass("arrow1");
                    arrowChangeCount = +arrowChangeCount + 1;
                    if (arrowChangeCount == 2) {
                        arrowChange = "arrow1";
                        arrowChangeCount = 0;
                    }
                }

                if (arrowChange == "arrow1") {
                    $("#arrowChange").removeClass("arrow1");
                    $("#arrowChange").addClass("arrow");
                    arrowChangeCount = +arrowChangeCount + 1;
                    if (arrowChangeCount == 2) {
                        arrowChange = "arrow";
                        arrowChangeCount = 0;
                    }
                }

            });

            $('.trackOrderCancelPopUpClose').click(function () {
                $('.trackOrderDelete-PopUp').hide();
            });

            $('.save-Order-Ques').click(function () {

                var incId = $(this).attr('data-subqus-IncreNo');
                var orderNo = $(this).attr('data-subqus-orderno');
                $('.shipmentclosepopup').hide();

                tq.common.isBusy(true);

                var data = { subject: $('#ddlQuesSub_' + incId).val(), message: $('#querySubject_' + incId).val(), orderNo: orderNo };
                var response = tq.dataManager.postData(tq.urls.saveQuestionForQus, data);
                response.done(function (data) {
                    if (data.Status == true) {
                    }
                    tq.common.isBusy(false);

                    $('.trackOrderCancelPopUpClose').click(function () {
                        $('.trackOrderDelete-PopUp').hide();
                    });
                });
                return false;

            });

            $('.deleteThis').click(function () {
                id = $(this).attr('data-id');
                $(this).attr('data-attr')
                $(this).attr('data-orderno')
                $('.trackOrderCancelPopUp_' + id).show();
            });

            tq.common.isBusy(false);
        });

        return false;
    }
}
//End Track Order


function NotificationViewModel() {
    var self = this;

    var replyNotification = null;
    var showReply = null;
    var btnSave = null;
    var txtQuerySubjectError = null;
    var replyToUserContainer = null;
    var ulAdminPanel = null;

    self.init = function () {
        replyNotification = $(".reply-notification");
        showReply = $(".view-all-repy");
        btnSave = $(".btn");
        ulAdminPanel = $("#ulAdminPanel");

        $('.query-qns').unbind("click").click(function () {
            var id = $(this).attr('data-reply');
            $('textarea').val("");
            $('.adminReplyPopup_' + id).show();
        });

        $('.queryclose-popup').click(function () {
            $('.queryclosepopup').hide();
        });

        $('.query-Order-Reply').click(function () {
            var incId = $(this).attr('data-subqusreply-increno');
            var orderNo = $(this).attr('data-subreply-orderno');
            var aspnetUserId = $(this).attr('data-aspnetuser_id');
            var userId = $(this).attr('data-user_id');
            var userName = $(this).attr('data-user-Name');
            var UserQuery = $(this).attr('data-user-query');
            $('.queryclosepopup').hide();
            tq.common.isBusy(true);
            var data = { email: $('#userEmail' + incId).val(), message: $('#querySubject' + incId).val(), orderNo: orderNo, aspnetUserId: aspnetUserId, userId: userId, userName: userName, userQues: UserQuery };

            var response = tq.dataManager.postData(tq.urls.saveReplyNotification, data);
            response.done(function (data) {
                if (data.Status == true) {
                }
                tq.common.isBusy(false);
            });

            return false;
        });

        $(".notifclose-popup").click(function () {
            $(".notification-pop-up").modal('hide');
        });

        replyNotification.on("click", "a", function () {
            var control = $(this).parent().parent().next();
            if (control.css("display") == "block") {
                control.css("display", "none");
            }
            else {
                control.css("display", "block");
            }
        });

        showReply.on("click", "a", function (e) {
            var userId = $(this).attr("data-attrid");
            $(".showNotification-popup_" + userId).modal('show');
        });

        btnSave.on("click", function (e) {
            var ctrl = $(this).parent().parent().find("textarea");
            var email = $(this).parent().parent().find("input[type='text']");
            replyToUserContainer = $(this).parent().parent().parent().parent().parent().parent();
            txtQuerySubjectError = $(this).parent().parent().find(".field-validation-valid");
            txtQuerySubjectError.text("");
            if (ctrl.val().trim() == "") {
                txtQuerySubjectError.text("Field is mandatory");
                e.stopPropagation();
                e.preventDefault();
                return;
            }

            var hiddenFields = replyToUserContainer.find("input[type='hidden']");
            var orderNo = hiddenFields.eq(0).val();
            var aspUserID = hiddenFields.eq(1).val();
            var id = hiddenFields.eq(2).val();
            var data = {};
            data.Notifications = {};
            data.LstNotifications = [];
            var obj = {};
            obj.Email = email.val();
            obj.Id = id;
            obj.AspNetUser_Id = aspUserID;
            obj.OrderNo = orderNo;

            data.LstNotifications.push(obj)
            data.Notifications.Subject = "";
            data.Notifications.Message = ctrl.val();
            data.Notifications.Id = 0;

            var model = { model: data };

            ulAdminPanel.children().removeClass("active");
            ulAdminPanel.find("#btnAdminUserNotification").addClass("active");

            tq.common.isBusy(true);
            window.setTimeout(function (e) {
                var result = tq.common.redirectToRoute(tq.urls.replyNotification, "POST", model);
                $("#userPanelRenderBody").children().not(".tr-user-details-outer").remove();
                $("#userPanelRenderBody").children().not(".tr-user-details-outer").remove();
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();
                $("#userPanelRenderBody").append(result);
                window.location.href = "index.html";
                tq.common.isBusy(false);
            }, 100);

        });
    };
};

//Contact page view
function ContactUsViewModel() {
    var self = this;
    var txtContactName = $('#contactName');
    var txtContactEmail = $('#contactEmail');
    var txtPhoneNumber = $('#PhoneNumber');
    var textContactMessage = $('#Message');
    var btnContact = $('#btnContact');

    self.init = function () {

        btnContact.click(function (event) {
            var isRequired = false;
            if (txtContactName.val() == "") {
                txtContactName.next().text("Name is required");
                isRequired = true;
            }
            else {
                txtContactName.next().text("");
            }
            if (txtContactEmail.val() == "") {
                txtContactEmail.next().text("Email is required");
                isRequired = true;
            }
            else {
                txtContactEmail.next().text("");
            }

            if (txtContactEmail.val() != "" && !tq.common.validateEmail(txtContactEmail.val())) {
                txtContactEmail.next().text("");
                txtContactEmail.next().text("Invalid email.");
                isRequired = true;
            }

            if (txtPhoneNumber.val() == "") {
                txtPhoneNumber.next().text("Phone number is required");
                isRequired = true;
            }
            else {
                txtPhoneNumber.next().text("");
            }
            if (textContactMessage.val() == "") {
                textContactMessage.next().text("Message is required");
                isRequired = true;
            }
            else {
                textContactMessage.next().text("");
            }

            if (isRequired) {
                return false;
            }
            else {
                tq.common.isBusy(true);
                textContactMessage.next().text("");
                var data = { Name: $('#contactName').val().trim(), Email: $('#contactEmail').val().trim(), Phone: $('#PhoneNumber').val().trim(), Message: $('#Message').val().trim() };
                var response = tq.dataManager.postData(tq.urls.sendContact, data);
                response.done(function (data) {
                    $('#contactName').val("");
                    $('#contactEmail').val("");
                    $('#PhoneNumber').val("");
                    $('#Message').val("");
                    tq.common.isBusy(false);
                    $('.contactUsMsg').show();
                });
            }
        });

        txtPhoneNumber.keypress(function (evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        });

        $('.contactUsMsgClose,.contactUsMsgYes').click(function () {
            $('.contactUsMsg').hide();
        });
    };
}
//end contact page view
