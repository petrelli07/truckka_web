﻿function LoginViewModel() {
    var pathname = window.location.pathname;
    var path = pathname.toLowerCase()
    var txtLoginEmail = $("#txtLoginEmail");
    var txtLoginPassword = $("#txtPassword");
    var btnLogin = $("#btnLogin");
    var EmailError = $("#EmailError");
    var PasswordError = $("#PwdError");
    var EmailExpChk = false;
    var Email_Phone = "";
    var btnLoginUser = $("#btnLoginUser");
    var password_hash = "";
    var Email = $("#txtLoginEmail");
    var Password = $("#txtPassword");
    var Remember = false;
    var RememberMe = $("#rememberMe");
    var Equipments = $(".equipments");
    var LoginEmailError = $("#EmailError")
    var LoginPwdError = $("#PwdError");
    var trucksize = $("#sizeSelection");
    var slider = $("#SelectedWeightVal");
    var InActiveAccount = $(".InactiveAccount");
    var txtPickUpAddress = null;
    var txtDelivery = null;
    var equipmentType = null;
    var btnGetQuote = null;
    var PickupError = null;
    var DelieryError = null;
    var bookpopup = null;
    var btnLogOut = null;
    var btnBookPopUp = null;
    var txtPickUpDate = $("#txtPickUpDate");
    var txtPickUpAddress = $("#txtPickupAddress1");
    var txtDelivery = $("#txtDeliveryAddress1");
    var txttrucktypeError = $("#txttrucktypeError");
    var equipmentType = $(".equipments");
    var PickupError = $("#txtPickupAddress1Error");
    var DelieryError = $("#txtDeliveryAddress1Error");
    var btnGetQuote = $("#btnGetQuote");
    var bookpopup = $("#myModal");
    var btnLogOut = $("#btnLogOut");
    var txtSelectWeightError = $("#SelectedWeightError");
    var txttruckCategoryError = $("#SelectFeetError");
    var SelectedWeightVal = $("#SelectedWeightVal");
    var ddlFootageSize = $("#ddlSize");
    function Intialize() {

        Events();

    };

    function Events() {

        $("#txtLoginEmail,#txtPassword").keypress(function (evt) {
            var key = (evt.which) ? evt.which : evt.keyCode;
            if (key == 13) {
                userLogin();
            }
            if (key == 32) {
                return false;
            }
        });

        //btnLogin.on("click", function (event) {
        //    var isRequired = false;
        //    if (txtLoginEmail.val().trim() == "") {      //Email is Empty
        //        EmailError.text("Email/Phone No is required");
        //        isRequired = true;
        //    }
        //    if (txtLoginPassword.val().trim() == "") {   //Password is Empty

        //        PasswordError.text("Password is required");
        //        isRequired = true;
        //    }
        //    else {
        //        PasswordError.text("");
        //    }
        //    if (txtLoginEmail.val().trim() != "")     //Checking Expression of Email 
        //    {

        //        if (!$.isNumeric(txtLoginEmail.val().trim())) {

        //            EmailExpChk = Common.ValidateEmail(txtLoginEmail.val().trim()); //Checking expression 
        //            if (!EmailExpChk) //expression is false
        //            {
        //                EmailError.text("Email is invalid");
        //                isRequired = true;  //isrequired is true
        //            }

        //            else {
        //                //Getting email and password from the database
        //                Email_Phone = DataBase.ExecuteDataset("SELECT IsActive,EMAIL,PasswordHash FROM AspNetUsers WHERE EMAIL='" + txtLoginEmail.val().trim() + "'");
        //                if (Email_Phone.length < 1) {
        //                    EmailError.text("Email is not found");
        //                    isRequired = true;
        //                }
        //                else {
        //                    EmailError.text("");
        //                    if (Email_Phone[0].IsActive == "N") {
        //                        EmailError.text("Email is inactive.");
        //                        isRequired = true;
        //                        InActiveAccount.show();
        //                    }
        //                }
        //            }
        //        }
        //        else {
        //            Email_Phone = DataBase.ExecuteDataset("SELECT IsActive,PhoneNumber,PasswordHash FROM AspNetUsers WHERE PhoneNumber='" + txtLoginEmail.val().trim() + "'");

        //            if (Email_Phone.length < 1) {
        //                EmailError.text("Phone Number is not found");
        //                isRequired = true;
        //            }
        //            else {

        //                EmailError.text("");
        //                if (Email_Phone[0].IsActive == "N") {
        //                    EmailError.text("Email is inactive.");
        //                    isRequired = true;
        //                    InActiveAccount.show();
        //                }
        //            }

        //        }
        //    }

        //    if (txtLoginPassword.val().trim() != "" && Email_Phone != "") {
        //        var passwordhash = Email_Phone[0].PasswordHash;
        //        var result = "";
        //        $.ajax({
        //            url: "/Account/PasswordChecker",
        //            type: "GET",
        //            contentType: "application/json",
        //            async: false,

        //            data: { Password: txtLoginPassword.val(), passwordHash: Email_Phone[0].PasswordHash },
        //            success: function (data) {
        //                if (data != "Success") {
        //                    EmailError.text("");
        //                    PasswordError.text("Password is invalid");
        //                    isRequired = true;
        //                }
        //            }
        //        });
        //    }

        //    if (isRequired) {
        //        event.stopPropagation();
        //        event.preventDefault();
        //        return false;
        //    }
        //    else {
        //        var date = new Date();
        //        var time = date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
        //        $('#pickupAddress').val($("#txtPickupAddress1").val());
        //        $('#deliveryAddress').val($("#txtDeliveryAddress1").val());
        //        $('#pickupDate').val($("#txtPickUpDate").val() + " " + time);
        //        $('#truckType').val($(".equipments:checked").val());
        //        $('#truckLoad').val($("#SelectedWeightVal").val());
        //        var feet = $("#SelectedFeetVal").val().split(' ')[0];
        //        $('#truckFootage').val(parseInt(feet));
        //        $('#pickupLat1').val($("#pickUpLat").val());
        //        $('#pickupLng1').val($("#pickUpLong").val());
        //        $('#destLat1').val($("#deliveryLat").val());
        //        $('#destLng1').val($("#deliveryLong").val());

        //        var data = {
        //            Name: $("#txtLoginEmail").val(), Password: $("#txtPassword").val()
        //        };
        //        $.ajax({
        //            url: "/Account/LoginUser",
        //            type: "POST",
        //            data: data,
        //            success: function (result) {
        //                if (result.Status == true && result.Result != null) {
        //                    if ($("#txtPickupAddress1").val() != null && $("#txtPickupAddress1").val() != "" && $("#txtPickupAddress1").val() != undefined) {
        //                        var date = new Date();
        //                        var time = date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
        //                        var data = {
        //                            Origin: $("#txtPickupAddress1").val(), Destination: $("#txtDeliveryAddress1").val(), Equipment_Type: $(".equipments:checked").val().toString(),
        //                            Date: $("#txtPickUpDate").val() + " " + time, Weight: $("#SelectedWeightVal").val().trim(), Feet: parseInt($("#SelectedFeetVal").val()),
        //                            PickUpLng: $("#pickUpLong").val(), PickUpLat: $("#pickUpLat").val(), DeliveryLng: $("#deliveryLong").val(), DeliveryLat: $("#deliveryLat").val(),
        //                            SkipUserCheck: true, UserId: result.Result
        //                        };
        //                        $.ajax({
        //                            url: "/api/quotes",
        //                            type: "POST",
        //                            data: data,
        //                            success: function (result) {
        //                                $(".registerpoppup").hide();
        //                                $(".register-message").show();
        //                                Common.IsBusy(false);
        //                            }
        //                        });
        //                    } else {
        //                        $(".registerpoppup").hide();
        //                        $(".register-message").show();
        //                        Common.IsBusy(false);
        //                    }
        //                } else {
        //                    $(".registerpoppup").hide();
        //                    Common.IsBusy(false);
        //                }
        //            }
        //        });

        //    }
        //});
        InActiveAccount.click(function () {
            $(' #InactiveAccount-pop').hide();
        });

        $('.InActiveClose').click(function () {
            $('.Inactive').hide();
        })
        $('.PopupInactive').click(function () {
            $('.Inactive').hide();
        })
        $(".view-password").click(function () {
            if (txtLoginPassword.val().trim() != "") {
                if ($(".view-password").hasClass("view")) {
                    $('.no-view').hide();
                    txtLoginPassword.prop("type", "text");
                    $('.view-password').removeClass('view');
                }
                else {
                    $('.no-view').show();
                    txtLoginPassword.prop("type", "password");
                    $('.view-password').addClass('view');

                }
            }
        })

        btnLoginUser.on("click",
            function (event) {
                userLogin();
            });

        function userLogin() {
            if (RememberMe.prop("checked") == true) {
                Remember = true;
            }
           // Common.IsBusy(true);
            var isRequired = false;

            if (Email.val().trim() == "") { //Email is Empty
                LoginEmailError.text("Email/Phone No is required");
                isRequired = true;
            }
            if (Password.val().trim() == "") { //Password is Empty
                LoginPwdError.text("Password is required");
                isRequired = true;
            }
            if (isRequired) {
               // Common.IsBusy(false);
                event.stopPropagation();
                event.preventDefault();
                return false;
            } else {
                var WeightNum = $('#SelectedWeightVal').val();
                Common.IsBusy(true);
                var feet = 4;
                if ($("#SelectedFeetVal").val() != undefined) {
                    feet = $("#SelectedFeetVal").val().split(' ')[0];
                }
                $.ajax({
                    url: "/Account/LoginUser",
                    type: "POST",
                    data: {
                        UserName: Email.val(),
                        Password: Password.val(),
                        RememberMe: Remember,
                        Pickup: $('#txtPickupAddress1').val(),
                        Dest: $('#txtDeliveryAddress1').val(),
                        PickUpDate: $('#txtPickUpDate').val(),
                        EstimateTime: $('.spnPickUpAvgDays').text(),
                        EquipMent: $('.equipments:checked').val(),
                        Weight: WeightNum,
                        Feet: feet,
                        Miles: $(".spnPickUpMiles").text(),
                        Amount: $('.spanTotalAmount').text()
                    },
                    success: function (data) {
                        if (data.Status == true) {
                            if (data.Role == "Admin" || data.Role == "Super Admin") {
                                location.href = "index.html";
                            } else {
                                if (!path.includes("/user/quote")) {
                                    if ($('#txtPickupAddress1').val() != null && $('#txtPickupAddress1').val() != "" && $("#txtPickupAddress1").val() != undefined &&
                                        $('#txtDeliveryAddress1').val() != null && $('#txtDeliveryAddress1').val() != "" && $("#txtDeliveryAddress1").val() != undefined) {
                                        CalculateQuote(data.UserID, true);
                                    } else {
                                        location.href = "/user/quote?userId=" + data.UserID;
                                    }
                                } else {
                                    Common.IsBusy(false);
                                    location.href = "/user/quote?id=" + $('#id').val();
                                    $('.loginpoppup').hide();
                                    $("#usererror").text("");
                                }
                            }

                        } else {
                            if (data.Message == "Invalid Username") {
                                LoginEmailError.text("Invalid Email/Phone No");
                                LoginPwdError.text("");
                            } else if (data.Message == "Invalid Password") {
                                LoginPwdError.text(data.Message);
                                LoginEmailError.text("");
                            }
                            else {
                                LoginEmailError.text("");
                                LoginPwdError.text("");
                                $("#usererror").text(data.Message);
                            }
                            Common.IsBusy(false);
                        }
                    }
                });
            }
        }

        $("#loginClose").click(function () {
            $("#usererror").text("");
        });

        function CalculateQuote(userId, skipUserCheck) {
            Common.IsBusy(false);
            PickupError.text("");
            DelieryError.text("");
            txttrucktypeError.text("");
            txtSelectWeightError.text("");
            txttruckCategoryError.text("");
            if (skipUserCheck == "" || skipUserCheck == undefined) {
                skipUserCheck = false;
            }
            //for slider
            txtSelectFeetError.text("");

            var isCheck = true;
            if (txtPickUpAddress.val() == "") {
                isCheck = false;
                PickupError.text("Please enter the PickUp City and State");
            }
            else if (txtPickUpAddress.val().split(",").length < 3) {

                PickupError.text("Please enter the PickUp City and State");
                isCheck = false;
            }
            else {
                PickupError.text("");
            }
            if (txtDelivery.val() == "") {
                isCheck = false;
                DelieryError.text("Please enter the Delivery City And State");
            }
            else if (txtDelivery.val().split(",").length < 3) {
                DelieryError.text("Please enter the Delivery City And State");
                isCheck = false;
            }
            else {
                DelieryError.text("");
            }
            if (equipmentType.is(':checked') == false) {
                txttrucktypeError.text("Please select truck type");
                isCheck = false;
            }

            if (SelectedWeightVal.val() == "0LBS") {
                isCheck = false;
                txtSelectWeightError.text("Please select weight");
            }

            //for feet slider
            if (SelectedFeetVal.val() == "") {
                isCheck = false;
                txtSelectFeetError.text("Please select Feet");
            }
            if (!isCheck) {
                Common.IsBusy(false);
                return false;
            }
            var date = new Date();
            var time = date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
            Common.IsBusy(true);
            var data = {
                Origin: $("#txtPickupAddress1").val(), Destination: $("#txtDeliveryAddress1").val(), Equipment_Type: $(".equipments:checked").val().toString(),
                Date: $("#txtPickUpDate").val() + " " + time, Weight: $("#SelectedWeightVal").val().trim(), Feet: parseInt($("#SelectedFeetVal").val()),
                PickUpLng: $("#pickUpLong").val(), PickUpLat: $("#pickUpLat").val(), DeliveryLng: $("#deliveryLong").val(), DeliveryLat: $("#deliveryLat").val(),
                UserId: userId, SkipUserCheck: skipUserCheck
            };
            $.ajax({
                url: "/api/quotes",
                type: "POST",
                data: data,
                success: function (result) {
                    Common.IsBusy(false);
                    if (result.status == true && result.result != null) {
                        Common.IsBusy(false);
                        $('#quoteId').val(result.result.id);
                        $('#quoteId1').val(result.result.id);
                        $('#id').val(result.result.id);

                        if (skipUserCheck) {
                            if ($('#quoteId').val() != "" &&
                                $('#quoteId').val() != undefined &&
                                $('#quoteId').val() != null)
                                location.href = "/user/quote?id=" + $('#quoteId').val();
                        }

                        return true;
                    } else {
                        if (result.message == "Data Not Available") {
                            $('.dataNotAvailable').show();
                            $('.spanTotalAmount').text(0);
                            Common.IsBusy(false);
                            return false;
                        } else {
                            $('.freeQuoteMsg').show();
                            $('.spanTotalAmount').text(0);
                            Common.IsBusy(false);
                            return false;
                        }
                    }
                }
            });
        };
        $('.verifyLoginBtn').click(function () {
            $('.emailverification').hide();
            $("#modal-1").removeClass('fade');
            $('.loginpoppup').show();
        });
    }
    Intialize();
};

Login = new LoginViewModel();