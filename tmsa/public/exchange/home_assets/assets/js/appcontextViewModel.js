﻿function Context() {
    var object = Common.GetSessionContext();
    this.FirstName = object != null ? object.FirstName : "";
    this.LastName = object != null ? object.LastName : "";
    this.Email = object != null ? object.Email : "";
    this.UserId = object != null ? object.UserId : "";
    this.Role = object != null ? object.Role : "";
    this.ImageUrl = object != null ? object.ImageUrl : "";
    return this;
};
Context.prototype.GetFirstName = function () {
    return this.FirstName;
};
Context.prototype.GetLastName = function () {
    return this.LastName;
};
Context.prototype.GetEmail = function () {
    return this.Email;
};
Context.prototype.GetImageUrl = function () {
    return this.ImageUrl;
};
Context.prototype.GetRole = function () {
    return this.Role;
};
Context.prototype.GetUserId = function () {
    return this.UserId;
};
Context.prototype.DisposeCtx = function () {
    appCtx.FirstName = "";
    appCtx.LastName = "";
    appCtx.Email = "";
    appCtx.UserId = "";
    appCtx.Role = "";
    appCtx.ImageUrl = "";
};
appCtx = new Context();