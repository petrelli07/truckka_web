﻿// Layout page codes
function LoginViewModel() {
    var self = this;
    var pathname = window.location.pathname;
    var path = pathname.toLowerCase()
    var txtLoginEmail = $("#txtLoginEmail");
    var txtLoginPassword = $("#txtPassword");
    var btnLogin = $("#btnLogin");
    var EmailError = $("#EmailError");
    var PasswordError = $("#PwdError");
    var EmailExpChk = false;
    var Email_Phone = "";
    var btnLoginUser = $("#btnLoginUser");
    var password_hash = "";
    var Email = $("#txtLoginEmail");
    var Password = $("#txtPassword");
    var Remember = false;
    var RememberMe = $("#rememberMe");
    var Equipments = $(".equipments");
    var LoginEmailError = $("#EmailError")
    var LoginPwdError = $("#PwdError");
    var trucksize = $("#sizeSelection");
    var slider = $("#SelectedWeightVal");
    var InActiveAccount = $(".InactiveAccount");
    var txtPickUpAddress = null;
    var txtDelivery = null;
    var equipmentType = null;
    var btnGetQuote = null;
    var PickupError = null;
    var DelieryError = null;
    var bookpopup = null;
    var btnLogOut = null;
    var btnBookPopUp = null;
    var txtPickUpDate = $("#txtPickUpDate");
    var txtPickUpAddress = $("#txtPickupAddress1");
    var txtDelivery = $("#txtDeliveryAddress1");
    var txttrucktypeError = $("#txttrucktypeError");
    var equipmentType = $(".equipments");
    var PickupError = $("#txtPickupAddress1Error");
    var DelieryError = $("#txtDeliveryAddress1Error");
    var btnGetQuote = $("#btnGetQuote");
    var bookpopup = $("#myModal");
    var btnLogOut = $("#btnLogOut");
    var txtSelectWeightError = $("#SelectedWeightError");
    var txttruckCategoryError = $("#SelectFeetError");
    var SelectedWeightVal = $("#SelectedWeightVal");
    var ddlFootageSize = $("#ddlSize");

    self.calculateLoginQuote = function (userId, skipUserCheck) {
        PickupError.text("");
        DelieryError.text("");
        txttrucktypeError.text("");
        txtSelectWeightError.text("");
        txttruckCategoryError.text("");
        if (skipUserCheck == "" || skipUserCheck == undefined) {
            skipUserCheck = false;
        }
        //for slider
        txtSelectFeetError.text("");

        var isCheck = true;
        if (txtPickUpAddress.val() == "") {
            isCheck = false;
            PickupError.text("Please enter the PickUp City and State");
        }
        else if (txtPickUpAddress.val().split(",").length < 3) {

            PickupError.text("Please enter the PickUp City and State");
            isCheck = false;
        }
        else {
            PickupError.text("");
        }
        if (txtDelivery.val() == "") {
            isCheck = false;
            DelieryError.text("Please enter the Delivery City And State");
        }
        else if (txtDelivery.val().split(",").length < 3) {
            DelieryError.text("Please enter the Delivery City And State");
            isCheck = false;
        }
        else {
            DelieryError.text("");
        }
        if (equipmentType.is(':checked') == false) {
            txttrucktypeError.text("Please select truck type");
            isCheck = false;
        }

        if (SelectedWeightVal.val() == "0LBS") {
            isCheck = false;
            txtSelectWeightError.text("Please select weight");
        }

        //for feet slider
        if (SelectedFeetVal.val() == "") {
            isCheck = false;
            txtSelectFeetError.text("Please select Feet");
        }
        if (!isCheck) {
            tq.common.isBusy(false);
            return false;
        }
        var date = new Date();
        var time = date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
        tq.common.isBusy(true);
        var data = {
            Equipment_Type: $(".equipments:checked").val().toString(),
            Date: $("#txtPickUpDate").val() + " " + time, Weight: $("#SelectedWeightVal").val().trim(), Feet: parseInt($("#SelectedFeetVal").val()),
            UserId: userId, SkipUserCheck: skipUserCheck, IsWeb: true,
            stops: [{
                Address: $("#txtPickupAddress1").val(), Longitude: $("#pickUpLong").val(), Latitude: $("#pickUpLat").val(), city: $("#pickUpCity").val(),
                state: $("#pickUpState").val(), ZipCode: $("#pickUpZip").val()
            },
            {
                Address: $("#txtDeliveryAddress1").val(), Longitude: $("#deliveryLong").val(), Latitude: $("#deliveryLat").val(), city: $("#deliveryCity").val(),
                state: $("#deliveryState").val(), ZipCode: $("#deliveryZip").val()
            }]
        };
        var response = tq.dataManager.postData(tq.urls.quoteApi, data);
        response.done(function (result) {
            tq.common.isBusy(false);
            if (result.status == true && result.result != null) {
                tq.common.isBusy(false);
                $('#quoteId').val(result.result.id);
                $('#quoteId1').val(result.result.id);
                $('#id').val(result.result.id);

                if (skipUserCheck) {
                    if ($('#quoteId').val() != "" &&
                        $('#quoteId').val() != undefined &&
                        $('#quoteId').val() != null)
                        location.href = "/user/quote?id=" + $('#quoteId').val();
                }

                return true;
            } else {
                if (result.message == "Data Not Available") {
                    $('.dataNotAvailable').show();
                    $('.spanTotalAmount').text(0);
                    tq.common.isBusy(false);
                    return false;
                } else {
                    $('.freeQuoteMsg').show();
                    $('.spanTotalAmount').text(0);
                    tq.common.isBusy(false);
                    return false;
                }
            }
        });
    };

    self.userLogin = function () {
        if (RememberMe.prop("checked") == true) {
            Remember = true;
        }
        var isRequired = false;

        if (Email.val().trim() == "") { //Email is Empty
            LoginEmailError.text("Email/Phone No is required");
            isRequired = true;
        }

        if (Password.val().trim() == "") { //Password is Empty
            LoginPwdError.text("Password is required");
            isRequired = true;
        }

        if (isRequired) {
            event.stopPropagation();
            event.preventDefault();
            return false;
        } else {
            var WeightNum = $('#SelectedWeightVal').val();
            tq.common.isBusy(true);
            var feet = 4;
            if ($("#SelectedFeetVal").val() != undefined) {
                feet = $("#SelectedFeetVal").val().split(' ')[0];
            }

            var data = {
                UserName: Email.val(),
                Password: Password.val(),
                RememberMe: Remember,
                Pickup: $('#txtPickupAddress1').val(),
                Dest: $('#txtDeliveryAddress1').val(),
                PickUpDate: $('#txtPickUpDate').val(),
                EstimateTime: $('.spnPickUpAvgDays').text(),
                EquipMent: $('.equipments:checked').val(),
                Weight: WeightNum,
                Feet: feet,
                Miles: $(".spnPickUpMiles").text(),
                Amount: $('.spanTotalAmount').text()
            };

            var response = tq.dataManager.postData(tq.urls.loginUser, data);
            response.done(function (data) {
                if (data.Status == true) {
                    if ($("#returnUrl").val() != "" && $("#returnUrl").val() != null) {
                        location.href = $("#returnUrl").val();
                    } else {
                        if (data.Role == "Admin" || data.Role == "Super Admin") {
                            location.href = "index.html";
                        } else {
                            if (!path.includes("/user/quote")) {
                                if ($('#txtPickupAddress1').val() != null && $('#txtPickupAddress1').val() != "" && $("#txtPickupAddress1").val() != undefined &&
                                    $('#txtDeliveryAddress1').val() != null && $('#txtDeliveryAddress1').val() != "" && $("#txtDeliveryAddress1").val() != undefined) {
                                    self.calculateLoginQuote(data.UserID, true);
                                } else {
                                    location.href = "/user/quote?userId=" + data.UserID;
                                }
                            } else {
                                tq.common.isBusy(false);
                                location.href = "/user/quote?id=" + $('#id').val();
                                $('.loginpoppup').hide();
                                $("#usererror").text("");
                            }
                        }
                    }

                } else {
                    if (data.Message == "Invalid Username") {
                        LoginEmailError.text("Invalid Email/Phone No");
                        LoginPwdError.text("");
                    } else if (data.Message == "Invalid Password") {
                        LoginPwdError.text(data.Message);
                        LoginEmailError.text("");
                    }
                    else {
                        LoginEmailError.text("");
                        LoginPwdError.text("");
                        $("#usererror").text(data.Message);
                    }
                    tq.common.isBusy(false);
                }
            });
        }
    }

    self.init = function () {

        $("#txtLoginEmail,#txtPassword").keypress(function (evt) {
            var key = (evt.which) ? evt.which : evt.keyCode;
            if (key == 13) {
                self.userLogin();
            }
            if (key == 32) {
                return false;
            }
        });

        InActiveAccount.click(function () {
            $(' #InactiveAccount-pop').hide();
        });

        $('.InActiveClose').click(function () {
            $('.Inactive').hide();
        })
        $('.PopupInactive').click(function () {
            $('.Inactive').hide();
        })
        $(".view-password").click(function () {
            if (txtLoginPassword.val().trim() != "") {
                if ($(".view-password").hasClass("view")) {
                    $('.no-view').hide();
                    txtLoginPassword.prop("type", "text");
                    $('.view-password').removeClass('view');
                }
                else {
                    $('.no-view').show();
                    txtLoginPassword.prop("type", "password");
                    $('.view-password').addClass('view');
                }
            }
        })

        btnLoginUser.on("click",
            function (event) {
                self.userLogin();
            });

        $("#loginClose").click(function () {
            $("#usererror").text("");
        });

        $('.verifyLoginBtn').click(function () {
            $('.emailverification').hide();
            $("#modal-1").removeClass('fade');
            $('.loginpoppup').show();
        });
    }
}

function RegisterViewModel() {
    var self = this;

    var txtUserName = $('#txtUserName');
    var txtEmail = $('#txtEmail');
    var txtPassword = $('#txtLoginPassword');
    var txtPhone = $('#txtPhone');
    var txtConfirmPassword = $("#txtConfirmPassword");
    var btnRegister = $("#btnRegister");
    var indexsignUp = $("#signup");

    self.init = function () {

        $("#txtLoginPassword,#txtConfirmPassword").keypress(function (evt) {
            var key = (evt.which) ? evt.which : evt.keyCode;
            if (key == 32) {
                return false;
            }
        });

        indexsignUp.click(function () {
            txtUserName.val("");
            txtUserName.next().text("");
            txtEmail.val("");
            txtEmail.next().text("")
            txtPassword.val("");
            txtPhone.val("");
            txtConfirmPassword.val("");
            txtPassword.val("");
            txtEmail.next().text("");
            txtPhone.next().text("");
        });

        btnRegister.on("click", function (e) {

            var isRequired = false;
            var isPasswordVal = false;

            var ctrl = $(".field-validation-valid");
            if (txtUserName.val().trim() == "") {
                txtUserName.next().text("Name is required.");
                $('.text4').css({ 'margin-bottom': '0px' });
                isRequired = true;
            }
            else {
                txtUserName.next().text("");
                $('.text4').css({ 'margin-bottom': '20px' })
                $('#txtUserName').css({ 'margin-bottom': '20px' })
            }

            if (txtEmail.val() == "") {
                txtEmail.next().text("Email is required.");
                $('.text4').css({ 'margin-bottom': '0px' });
                isRequired = true;
            }
            if (txtPhone.val() == "") {
                txtPhone.next().text("Phone Number is required.");
                $('.text4').css({ 'margin-bottom': '0px' });
                isRequired = true;
            }
            if (txtPassword.val() == "") {
                txtPassword.next().text("Password is required.");
                $('.text4').css({ 'margin-bottom': '0px' });
                isRequired = true;
            } else {
                txtPassword.next().text("");
                $('.text4').css({ 'margin-bottom': '20px' });
            }
            if (txtConfirmPassword.val() == "") {
                txtConfirmPassword.next().text("Confirm Password is required.");
                $('.text4').css({ 'margin-bottom': '0px' });
                isRequired = true;
                isPasswordVal = true;
            } else {
                txtConfirmPassword.next().text("");
                $('.text4').css({ 'margin-bottom': '20px' });
            }
            if (txtConfirmPassword.val().trim() != txtPassword.val().trim()) {
                txtConfirmPassword.next().text("Password and Confirm Password are doesn't match.");
                $('.text4').css({ 'margin-bottom': '0px' });
                isRequired = true;
                isPasswordVal = true;
            }
            if (txtEmail.val() != "" && !tq.common.validateEmail(txtEmail.val())) {
                txtEmail.next().text("Invalid email.");
                $('.text4').css({ 'margin-bottom': '0px' });
                isRequired = true;
                isPasswordVal = true;
            }
            if (isPasswordVal == false) {
                if (txtPassword.val().length < 6 && txtConfirmPassword.val().length < 6) {
                    txtConfirmPassword.next().text("Passwords must be at least 6 characters.");
                    $('.text4').css({ 'margin-bottom': '0px' });
                    isRequired = true;
                }
            }
            if (isRequired) {
                return false;
            }
            else {
                tq.common.isBusy(true);
                var date = new Date();
                var time = date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
                $('#regPickupAddress').val($("#txtPickupAddress1").val());
                $('#regDestAddress').val($("#txtDeliveryAddress1").val());
                $('#regDatePickup').val($("#txtPickUpDate").val() + " " + time);
                $('#regEquipmentType').val($(".equipments:checked").val());
                $('#regTruckWeight').val($("#SelectedWeightVal").val());
                $('#regTruckFeet').val(parseInt($("#SelectedFeetVal").val()));
                $('#pickupLat').val($("#pickUpLat").val());
                $('#pickupLng').val($("#pickUpLong").val());
                $('#destLat').val($("#deliveryLat").val());
                $('#destLng').val($("#deliveryLong").val());

                var data = {
                    Name: $("#txtUserName").val(), Email: $("#txtEmail").val(), PhoneNumber: $("#txtPhone").val(),
                    Password: $("#txtLoginPassword").val(), ConfirmPassword: $("#txtConfirmPassword").val()
                };
                var response = tq.dataManager.postData(tq.urls.registerUser, data);
                response.done(function (result) {
                    if (result.Status == true && result.Result != null) {
                        if ($("#txtPickupAddress1").val() != null && $("#txtPickupAddress1").val() != "") {
                            var date = new Date();
                            var time = date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
                            var data = {
                                Equipment_Type: $(".equipments:checked").val().toString(),
                                Date: $("#txtPickUpDate").val() + " " + time, Weight: $("#SelectedWeightVal").val().trim(), Feet: parseInt($("#SelectedFeetVal").val()),
                                SkipUserCheck: true, UserId: result.Result, IsWeb: true,
                                stops: [{
                                    Address: $("#txtPickupAddress1").val(), Longitude: $("#pickUpLong").val(), Latitude: $("#pickUpLat").val(), city: $("#pickUpCity").val(),
                                    state: $("#pickUpState").val(), ZipCode: $("#pickUpZip").val()
                                },
                                    {
                                        Address: $("#txtDeliveryAddress1").val(), Longitude: $("#deliveryLong").val(), Latitude: $("#deliveryLat").val(), city: $("#deliveryCity").val(),
                                        state: $("#deliveryState").val(), ZipCode: $("#deliveryZip").val()
                                    }]
                            };
                            var response = tq.dataManager.postData(tq.urls.quoteApi, data);
                            response.done(function (result) {
                                $(".registerpoppup").modal('hide');
                                //$(".registerpoppup").hide();
                                $(".register-message").show();
                                tq.common.isBusy(false);
                            });
                        } else {
                            $(".registerpoppup").modal('hide');
                            $(".register-message").show();
                            tq.common.isBusy(false);
                        }
                    } else {
                        if (result.Message == "Email already exist") {
                            txtEmail.next().text("Email already exist");
                            txtPhone.next().text("");
                        }
                        else if (result.Message == "Phone No already exist") {
                            txtEmail.next().text("");
                            txtPhone.next().text("Phone Number already exist");
                        }
                        tq.common.isBusy(false);
                    }
                });
            }
        });

        txtPhone.keypress(function (evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        });

        $('.close-register-message').click(function () {
            var response = tq.dataManager.getData(tq.urls.userVerified);
            response.done(function (data) {
                $(".register-message").hide();
            });
        });
    }
}

//User verified session clear 
$('.close-change').click(function () {
    var response = tq.dataManager.getData(tq.urls.userVerified);
    response.done(function (data) {
        $(".loginpoppup").hide();
        $("#usererror").text("");
    });
});

//End User verified session clear 

//State to state view code
function StateToStateViewModel() {
    var self = this;

    self.displayStateToState = function () {
        $('#dynamicStateUrl').html("");
        var originText = $("#origin-location :selected").text();
        var size = $("#destination-location option").length
        if (size > 0) {
            for (var i = 1; i < size; i++) {
                if (originText != $('#destination-location option:eq(' + i + ')').text()) {
                    $('#dynamicStateUrl').append('<p><a href=/truck/rates/' + $("#origin-location :selected").val() + '' + "-to-" + '' + $('#destination-location option:eq(' + i + ')').val() + '>' + originText + '&nbsp;' + 'to' + '&nbsp;' + $('#destination-location option:eq(' + i + ')').text() + '</a></p>');
                }
            }
        }
    }

    self.populateState = function (origin, dest) {
        var CityList = $(".Options");
        if (origin != 0 && dest == 0) {
            var originText = $("#origin-location :selected").text();
            var size = $("select#destination-location option").length;
            if (size > 0) {
                for (var i = 1; i < size; i++) {
                    CityList.append('<div style="padding-bottom:15px">' + originText + '&nbsp;' + 'to' + '&nbsp;' + $('#destination-location option:eq(' + i + ')').text() + '</div>');
                }
            }
        }
        else if (origin != 0 && dest != 0) {
            var CityList = $(".Options");
            var originText = $("#origin-location :selected").text();
            var destText = $("#destination-location :selected").text();
            CityList.append('<div>' + originText + '&nbsp;' + 'to' + '&nbsp;' + destText + '</div>');

        }
        else if (origin == 0 && dest != 0) {
            var destText = $("#destination-location :selected").text();
            var size = $("select#origin-location option").length;
            if (size > 0) {
                for (var i = 1; i < size; i++) {
                    CityList.append('<div style="padding-bottom:15px">' + destText + '&nbsp;' + 'to' + '&nbsp;' + $('#origin-location option:eq(' + i + ')').text() + '</div>');
                }
            }

        }
    }

    self.redirectStateToStatePage = function (origin, dest) {
        location.href = "/truck/rates/" + origin + "-to-" + dest;
    }

    self.init = function () {
        $('#ddlFootageSize').val(0);
        $('#origin-location').removeClass('orderEditPopupVal');
        $('#destination-location').removeClass('orderEditPopupVal');
        $("#destination-location option[value*='" + '@(stateName.ToLower().Trim())' + "']").prop('disabled', true);
        if ($("#HiddenSearch").val() != "" && $("#HiddenSearch").val() != undefined) {
            var searchText = $("#HiddenSearch").val().toLowerCase();
            var textCount = $('#origin-location option[value=' + searchText + ']').length;
            if (textCount > 0) {
                $('#origin-location option[value=' + searchText + ']').attr('selected', 'selected');
                var origin = $("#origin-location").val();
                var dest = 0;
                self.populateState(origin, dest);
            }
        }
        self.displayStateToState();

        $("#origin-location").change(function () {
            $("#destination-location option").prop("disabled", false);
            $("#destination-location option[value*='" + $("#origin-location").val().toString().toLowerCase().trim() + "']")
                .prop('disabled', true);
        });

        $("#destination-location").change(function () {
            $("#origin-location option").prop("disabled", false);
            $("#origin-location option[value*='" + $("#destination-location").val().toString().toLowerCase().trim() + "']")
                .prop('disabled', true);
        });

        $("#AreaSubmit").click(function () {
            var origin = $("#origin-location").val();
            var dest = $("#destination-location").val()
            $('#origin-location').removeClass('orderEditPopupVal');
            $('#destination-location').removeClass('orderEditPopupVal');
            if (origin == "0") {
                $('#origin-location').addClass('orderEditPopupVal');
                return false;
            }

            if (dest == "1") {
                $('#destination-location').addClass('orderEditPopupVal');
                return false;
            }
            self.redirectStateToStatePage(origin, dest);
        });
    }
}
//end state to state code

//Access token codes
function AccessTokenViewModel() {
    var self = this;

    self.init = function () {
        $("#generateToken").click(function () {
            var isValid = true;
            if ($('#expiryDate').val().trim() == "") {
                $('#dateError').text("Date is required");
                isValid = false;
            }
            if ($('#tokenName').val().trim() == "") {
                $('#nameError').text("Name is required");
                isValid = false;
            }
            if (isValid) {
                tq.common.isBusy(true);
                var date = new Date($("#expiryDate").val()).toLocaleString();
                var data = { Name: $("#tokenName").val(), UserId: $("#userId").val(), ExpiryDate: date, ExpiryDateString: $("#expiryDate").val() };
                var response = tq.dataManager.postData(tq.urls.generateAccessToken, data);
                response.done(function (data) {
                    if (data != null) {
                        var str = '<div class="col-sm-6 col-xs-12" id="accesscard_' + data.Id + '"><div class="col-xs-12 payment-sec border-px"><div class="row"> <div class="col-xs-12  payment-al2 b-bottom">'
                        str = str + '<div class="col-xs-6"><span>' + data.Name + '</span></div><div class="col-xs-6"><div class="payment-btn2">'
                        str = str + '<a href="#" class="remove_access_token" id="removeAccessToken" data-id="' + data.Id + '">Remove Access Token</a></div></div></div><div class="col-xs-12  payment-al3">'
                        str = str + 'Key : <span>' + data.Token + '</span> <br />Expiry Date : <span>' + data.ExpiryDateString + '</span></div></div></div></div>'

                        $("#tokenName").val("");

                        $("#AccessToken").prepend($(str).fadeIn('slow'));
                        $('.remove_access_token').click(function () {
                            accessTokenId = $(this).attr('data-id');
                            $('#confirmRemoveAccessToken').show();
                        });

                    }
                    tq.common.isBusy(false);
                });
            }

        });

        $('.remove_access_token').click(function () {
            accessTokenId = $(this).attr('data-id');
            $('#confirmRemoveAccessToken').show();
        });

        $('.accessTokenDeletePopUpClose').click(function () {
            $('#confirmRemoveAccessToken').hide();
        });

        $('.accessTokenDeletePopUpYes').click(function () {
            var id = accessTokenId;
            var card_Id = accessTokenId;

            $('#confirmRemoveAccessToken').hide();
            tq.common.isBusy(true);

            var data = { UserId: $("#userId").val(), Id: card_Id };

            var response = tq.dataManager.postData(tq.urls.deleteAccessToken, data);
            response.done(function (data) {
                if (data == true) {
                    $('#accesscard_' + id).remove();
                }
                tq.common.isBusy(false);
                $('#removeAccessToken').click(function () {
                    accessTokenId = $(this).attr('data-id');
                    $('#confirmRemoveAccessToken').show();
                });
            });

            return false;
        });
    }
}
//End access token codes

//Profile code
function ProfileViewModel() {
    var self = this;
    var UserName = null;
    var txtLastName = null;
    var txtCompanyName = null;
    var txtCompanyWebsite = null;
    var txtPhoneNumber = null;
    var txtEmail = null;
    var txtAddress = null;
    var txtState = null;
    var txtCity = null;
    var txtZip = null;
    var btnSave = null;
    var txtId = 0;
    var txtFirstNameError = null;
    var txtLastNameError = null;
    var txtEmailError = null;
    var UserImage = null;

    self.init = function () {
        UserName = $("#Name");
        txtPhoneNumber = $("#PhoneNumber");
        txtEmail = $("#Email");
        txtId = $("#Id");
        btnSaveUser = $("#SaveProfile");

        $('.shippingStatus').click(function () {
            tq.common.isBusy(true);
            var data = { TruckStatus: $(this).attr('attr'), UserId: $('#userId').val(), IsActive: $(this).val() };
            var response = tq.dataManager.postData(tq.urls.saveEmailNotificationStatus, data);
            response.done(function (data) {
                tq.common.isBusy(false);
            });
        });

        self.events();
    }

    self.events = function () {

        $(".view-password1").click(function () {
            if ($(".view-password1").hasClass("view")) {
                $("#ProfPwd").prop("type", "text");
                $('.view-password1').removeClass('view');
            }
            else {
                $("#ProfPwd").prop("type", "password");
                $('.view-password').addClass('view');

            }
        });

        UserName.keypress(function (e) {
            var key = (e.which) ? e.which : e.keyCode;
            if (!((key == 8) || (key == 9) || (key == 14) || (key == 15) || (key == 32) || (key == 46) || (key >= 65 && key <= 90) || (key >= 48 && key <= 57) || (key >= 97 && key <= 122))) {
                return false;
            }
            else {
                return true;
            }
        });

        btnSaveUser.click(function () {
            var isRequired = false;
            $("#Prof_UserName").text("");

            $("#Prof_PhoneNoError").text("");

            $("#Prof_EmailError").text("");


            if (UserName.val().trim() == "") {
                $("#Prof_UserName").text("Name is Required");
                isRequired = true;

            }
            if (txtEmail.val().trim() == "") {
                $("#Prof_EmailError").text("Email is Required");
                isRequired = true;
            }
            if (txtEmail.val().trim() != "") {
                var EmailExpChk = tq.common.validateEmail(txtEmail.val().trim()); //Checking expression 
                if (!EmailExpChk) //expression is false
                {
                    $("#Prof_EmailError").text("Email is Invalid");
                    isRequired = true;  //isrequired is true
                }
            }
            if (txtPhoneNumber.val().trim() == "") {
                $("#Prof_PhoneNoError").text("Phone number is required");
                isRequired = true;
            }

            if (isRequired) {
                return false;
            }
            else {

                var ProfUserName = $("#Name").val().trim();
                var firstName = "";
                var lastName = "";
                if (!(ProfUserName.indexOf(' ') == -1)) {
                    var NameCount = ProfUserName.split(' ').length;
                    firstName = ProfUserName.split(' ')[0];
                    for (var i = 1; i < NameCount; i++) {
                        lastName = lastName + " " + ProfUserName.split(' ')[i];
                    }
                }
                else {
                    firstName = ProfUserName;
                    lastName = "";
                }

                var data1 = {};
                data1.ProfileDetails = {};

                data1.FirstName = firstName;
                data1.LastName = lastName.trim();
                data1.Email = txtEmail.val();
                data1.PhoneNumber = txtPhoneNumber.val();
                data1.Id = txtId.val();
                window.setTimeout(function () {
                    var data = { model: data1 };
                    var response = tq.dataManager.postData(tq.urls.userProfile, data);
                    response.done(function (result) {
                        if (result.Status == true && result.Result != null) {
                            toastr.success("Details Updated Successfully.");
                            $("#Prof_PhoneNoError").text("");
                            tq.common.isBusy(false);
                            location.reload();
                        } else {
                            if (result.Message == "Phone No already exist") {
                                $("#Prof_PhoneNoError").text("Phone Number already exist");
                            } else {
                                toastr.error("Something went wrong please try again.");
                            }
                            tq.common.isBusy(false);
                        }
                        Reset();
                    });
                }, 100);
            }
        });

        txtPhoneNumber.keypress(function (evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        });

        //Remove Card

        $('.removeCard').click(function () {
            var id = $(this).attr('data-id');
            $('.saveCardDeletePopUp_' + id).show();
        });

        $('.saveCardDeletePopUpYesClose').click(function () {
            $('.saveCardDelete-PopUp').hide();
        });

        $('.saveCardDeletePopUpYes').click(function () {
            var id = $(this).attr('data-incrno');
            var card_Id = $(this).attr('data-cardNo');

            $('.saveCardDelete-PopUp').hide();
            tq.common.isBusy(true);
            var data = { UserId: $("#userId").val(), pageNo: 1, pageSize: 100, id: card_Id };
            var response = tq.dataManager.postData(tq.urls.deleteUserCard, data);
            response.done(function (data) {
                if (data.Status == true) {
                    $('#cardSection_' + id).remove();
                }
                tq.common.isBusy(false);
            });
            return false;
        });
    };
}
//end profile code

//Change password

function ChangePasswordViewModel() {

    var self = this;

    self.init = function () {
        $("#ChangePasswordBtn").click(function () {
            self.changePassword();
        });
    };

    self.changePassword = function () {
        var result = true;
        var isPasswordVal = false;
        if ($("#currentPassword").val() == "" || $("#currentPassword").val() == null || $("#currentPassword").val() == undefined) {
            result = false;
            $("#currentPasswordError").text("Please enter current password");
        }
        if ($("#ConfirmPassword").val() == "" || $("#ConfirmPassword").val() == null || $("#ConfirmPassword").val() == undefined) {
            result = false;
            isPasswordVal = true;
            $("#ConfirmPasswordError").text("Please enter confirm password");
        }
        if ($("#NewPassword").val() == "" || $("#NewPassword").val() == null || $("#NewPassword").val() == undefined) {
            result = false;
            isPasswordVal = true;
            $("#newPasswordError").text("Please enter new password");
        }
        if ($("#ConfirmPassword").val() != "" && $("#NewPassword").val() != "") {
            if ($("#ConfirmPassword").val() != $("#NewPassword").val()) {
                result = false;
                $("#ConfirmPasswordError").text("Password Mismatch");
                isPasswordVal = true;
            }
        }
        if (isPasswordVal == false) {
            if ($("#NewPassword").val().length < 6 && $("#ConfirmPassword").val().length < 6) {
                $("#ConfirmPasswordError").text("Passwords must be at least 6 characters.");
                result = false;
            }
        }
        if (result) {
            tq.common.isBusy(true);
            var data = {
                OldPassword: $("#currentPassword").val(),
                NewPassword: $("#NewPassword").val(),
                ConfirmPassword: $("#ConfirmPassword").val(),
                UserId: $("#Id").val()
            };
            var response = tq.dataManager.postData(tq.urls.userChangePassword, data);
            response.done(function (data) {
                if (data.Status == true) {
                    $("#currentPassword").val("");
                    $("#NewPassword").val("");
                    $("#ConfirmPassword").val("");
                    toastr.success("Password changed successfully");
                    tq.common.isBusy(false);
                } else {
                    if (data.Message == "Incorrect password.") {
                        toastr.error("Incorrect Current Password.");
                        tq.common.isBusy(false);
                    } else {
                        toastr.error(data.Message);
                    }
                    tq.common.isBusy(false);
                }
            });
        } else {
            return false;
            tq.common.isBusy(false);
        }
    }
};

//End Change password

function ForgotpasswordViewModel() {
    var self = this;

    self.init = function () {
        txtForgotEmail = $("#txtForgotEmail");
        btnForgotSend = $("#btnForgotSend");
        btnBack = $("#backBtn");
        txtForgotEmail.next().text("");
        btnForgotSend.on("click", function (event) {
            $("#EmailSuccessfulMessage").text("");
            if (txtForgotEmail.val().trim() == "") {
                txtForgotEmail.next().text("Email is required field");
                return;
            }

            if (!tq.common.validateEmail(txtForgotEmail.val().trim())) {
                txtForgotEmail.next().text("Email format is not correct");
                return;
            }

            btnForgotSend.prop("disabled", true);
            $("#isLoaderBusy").css("display", "inline-block");
            window.setTimeout(function (e) {
                var data = { email: txtForgotEmail.val() };
                var response = tq.dataManager.postData(tq.urls.userChangePassword, data);
                response.done(function (result) {
                    if (result.Status == true) {
                        $("#EmailSuccessfulMessage").css("display", "inline-block");
                        $("#EmailSuccessfulMessage").text(result.Message);
                        $("#txtForgotEmail").val("");
                        $("#forgotEmailError").text("");
                        btnForgotSend.prop("disabled", false);
                        $("#isLoaderBusy").css("display", "none");
                        $('.quoteMsg').show();
                    }
                    else {
                        txtForgotEmail.next().text("Email is not available");
                        $("#isLoaderBusy").css("display", "none");
                    }
                });
            }, 100);
        });

        $('.quoteMsgClose,.quoteMsgYes').click(function () {
            location.href = "home1/index.html";
        });

        btnBack.on("click", function (event) {
            location.href = "home1/index.html";
        });

        $(document).bind('keypress', function (e) {
            if (e.keyCode == 13) {      //KeyCode for the Enter key=13
                if (txtForgotEmail.val().trim() != "") {
                    txtForgotEmail.next().text("");
                    btnForgotSend.trigger('click');
                }
                else {
                    $("#EmailSuccessfulMessage").text("");
                    txtForgotEmail.next().text("Email is required field");
                }
            }
        });
    }
}

function ResetPasswordViewModel() {
    var self = this;
    var OldPassword = null;
    var NewPassword = null;
    var ConfirmPassword = null;
    var btnResetPassword = null;
    var Email = null;

    self.init = function () {
        NewPassword = $("#NewPassword");
        ConfirmPassword = $("#ConfirmPassword");
        btnResetPassword = $("#btnResetPassword");
        Email = $("#email");
        btnBack = $("#backBtn");

        $("#NewPassword,#ConfirmPassword").keypress(function (evt) {
            var key = (evt.which) ? evt.which : evt.keyCode;
            if (key == 32) {
                return false;
            }
        })

        btnResetPassword.on("click", function (event) {
            NewPassword.next().text("");
            ConfirmPassword.next().text("");

            var isRequired = false;
            var isPasswordVal = false;

            if (NewPassword.val().trim() == "") {

                NewPassword.next().text("New Password is Required");
                isRequired = true;
                isPasswordVal = true;
            }
            if (ConfirmPassword.val().trim() == "") {
                ConfirmPassword.next().text("Confirm Password is Required");
                isRequired = true;
            }
            if (NewPassword.val().trim() != "" && ConfirmPassword.val().trim() != "") {
                if (NewPassword.val().trim() != ConfirmPassword.val().trim()) {
                    ConfirmPassword.next().text("Password and Confirm Password is not match");
                    isRequired = true;
                    isPasswordVal = true;
                }

            }
            if (isPasswordVal == false) {
                if (NewPassword.val().trim().length < 6 && ConfirmPassword.val().trim().length < 6) {
                    ConfirmPassword.next().text("Passwords must be at least 6 characters.");
                    isRequired = true;
                }
            }

            if (isRequired) {

                return false;
            }
            else {
                $("#isBusyLoad").css("display", "inline-block");
                var UserData = {};
                UserData.NewPassword = NewPassword.val();
                UserData.ConfirmPassword = ConfirmPassword.val();
                var data1 = { model: UserData, forgotEmail: Email.val().trim() };
            }
        });

        btnBack.on("click", function (event) {
            location.href = "home1/index.html";
        });
    };
};

function UserPaymentViewModel() {
    var self = this;
    self.bindShippingDetails = function () {
        $.ajax({
            url: "/OrderApi/GetOrderShipmentInfo",
            type: "POST",
            data: { OrderId: $('#orderId').val() },
            success: function (data) {
                var FirstName = data.PickUpFirstName == null ? "" : data.PickUpFirstName;
                var LastName = data.PickUpLastName == null ? "" : data.PickUpLastName;
                var LoadType = data.TQ_Equipment_Id == 1 ? "Dry Vans" : "Flat beds";
                $('.spPickupName').text(FirstName + " " + LastName);
                $('.spPickupCompanyName').text(data.PickUpCompanyName == null ? "" : data.PickUpCompanyName);
                $('.spPickupAddress').text(data.PickUpExactAddr == null ? "" : data.PickUpExactAddr);
                $('.spPickupState').text(data.PickUpState);
                $('.spPickupCity').text(data.PickUpCity);
                $('.spPickupDate').text(data.PickUpApptNote);
                $('.spCommodity').text(data.ShippingName);
                $('.spPickupType').text(LoadType);
                $('.spPickupWeight').text(data.Weigth);
                $('.spDeliveryName').text(data.DeliveryFirstName + " " + data.DeliveryLastName);
                $('.spDeliveryCompanyName').text(data.DeliveryCompanyName == null ? "" : data.DeliveryCompanyName);
                $('.spDeliveryAddress').text(data.DeliveryExactAddr);
                $('.spDeliveryState').text(data.DeliveryState);
                $('.spDeliveryCity').text(data.DeliveryCity);
                $('.spDeliveryDate').text("");
                $('.spDeliveryType').text(LoadType);
                $('.spDeliveryWeight').text(data.Weigth);

                var trackOrder = "/User/Order/GetTrackOrdersByID?OrderId=" + $('#orderId').val();
                $('.trackyourorder_').html("<a href=" + trackOrder + ">" + "Track your Order" + "</a>");

                tq.common.isBusy(false);
            }
        });
    }
    self.init = function () {
        $(document).on('click', '#btnPayCancel', function () {
            tq.common.isBusy(true);
            tq.common.redirectToUrl("/user/order/trackorders");
        });

        $(document).on('click', '#btnPayNow', function () {
            $('.paymentFailureMsg').hide();
            $('.paymentSuccessMsg').hide();
            var errorSummary = false;
            if ($('#Number').val() == "") {
                $('#spnNumber').show();
                $('#spnNumber').html("Card number is requied.\n");
                errorSummary = true;
                return false;
            }
            else {
                $('#spnNumber').hide();
            }
            if ($('#Name').val() == "") {
                $('#spnName').show();
                $('#spnName').html("Card name is requied.\n");
                errorSummary = true;
                return false;
            }
            else {
                $('#spnName').hide();
            }
            if ($('#SelectedMonth').val() == "0") {
                $('#cardExpireMonth').show();
                $('#cardExpireMonth').html("Expire month is requied.\n");
                errorSummary = true;
                return false;
            }
            else {
                $('#cardExpireMonth').hide();
            }
            if ($('#SelectedYear').val() == "0") {
                $('#cardExpireYear').show();
                $('#cardExpireYear').html("Expire year is requied.\n");
                errorSummary = true;
                return false;
            }
            else {
                $('#cardExpireYear').hide();
            }

            if ($('#Cvc').val() == "") {
                $('#cardCVV').show();
                $('#cardCVV').html("Card number is requied.\n");
                errorSummary = true;
                return false;
            }
            else {
                $('#cardCVV').hide();
            }

            if (!errorSummary) {
                var Model = {};
                Model.Name = $('#Name').val();
                Model.Number = $('#Number').val();
                Model.SelectedMonth = $('#SelectedMonth').val();
                Model.SelectedYear = $('#SelectedYear').val();
                Model.Cvc = $('#Cvc').val();
                Model.isSave = $('#payCheckbox').is(":checked");
                Model.SelectedCardType = $('#selectedCardType_').val();
                Model.PayAmount = $('#PayAmount').val();
                Model.CardExist = "N";
                Model.OrderId = $("#orderId").val();
                tq.common.isBusy(true);
                window.setTimeout(function (event) {
                    $.post('OrderApi/PaymentInvoice/index.html', Model, function (data) {
                        if (data.Status == true) {
                            tq.common.isBusy(false);

                            if (data.IsPayment == true) {
                                $('#hOrderNo_').val(data.OrderNo);
                                $('.paymentSuccessMsg').show();
                            }
                            else if (data.IsPayment == false) {
                                $('#FailureMsgInfo').html("<p>" + "Reson for failure:" + "</p>");
                                $('#FailureMsgInfo').html("<p>" + data.FailureInfo + "</p>");
                                $('.paymentFailureMsg').show();
                            }
                        }
                        else {

                        }
                    });
                }, 100);
            }
        });

        $('#PaymentSuccess_Order').click(function () {
            tq.common.isBusy(true);
            $('.paymentSuccessMsg').hide();
            $('#step1-bookingpage').hide();
            $('#step2-pickupinfo').hide();
            $('#step3-deliveryinfo').hide();
            $('#divOrderPaymentInfo').hide();
            $('#step5-shipmentinfo').show();

            var shipheight = $('.all-ship-ht').height();
            $(".ship-to-delivery").css({ "height": shipheight });
            var bookht = $('#step5-shipmentinfo').height();
            $(".map-ht").css({ "height": bookht });
            self.bindShippingDetails();
        });
        $('#btnFailureTryAgain,.paymentPopUpClose').click(function () {
            $('.paymentFailureMsg').hide();
        })

        $(document).on('click', '.paySavedCardDetails', function () {
            $('.paymentFailureMsg').hide();
            $('.paymentSuccessMsg').hide();

            var errorSummary = false;

            var id = $(this).attr('cardvalue');
            var cardId = $(this).attr('data-cardid');

            if ($("#savedcardcvv_" + id).val() == "") {
                $('#txtSavedcardCvv_' + id).show();
                $('#txtSavedcardCvv_' + id).html("CVV number is requied.\n");
                errorSummary = true;
                return false;
            }
            else {
                $('#txtSavedcardCvv_' + id).hide();
            }

            if (!errorSummary) {

                var Model = {};
                Model.Cvc = $("#savedcardcvv_" + id).val();
                Model.PayAmount = $('#PayAmount').val();
                Model.CardExist = "Y";
                Model.ExistingCardId = cardId;
                Model.OrderId = $("#orderId").val();
                tq.common.isBusy(true);
                window.setTimeout(function (event) {
                    $.post(tq.urls.paymentInvoice, Model, function (data) {
                        if (data.Status == true) {
                            tq.common.isBusy(false);
                            if (data.IsPayment == true) {
                                $('#hOrderNo_').val(data.OrderNo);
                                $('.paymentSuccessMsg').show();
                            }
                            else if (data.IsPayment == false) {
                                $('#FailureMsgInfo').html("<p>" + "Reson for failure:" + "</p>");
                                $('#FailureMsgInfo').html("<p>" + data.FailureInfo + "</p>");
                                $('.paymentFailureMsg').show();
                            }
                        }
                        else {
                            window.location = "account/login2eea.html";
                        }
                    });
                }, 100);
            }

        });
    }
}