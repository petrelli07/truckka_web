﻿$(document).ready(function () {
    $('#IndexLoader').fadeOut(500);
    $(".dwn-arw a,.list-unstyled li .slide").click(function (evn) {
        evn.preventDefault();
        $('html, body').animate({ scrollTop: $($(this).attr('href')).offset().top }, 500, 'linear');
    });

    $('.carousel').carousel({
        interval: 12000
    });
});

$(".about-btn a").click(function (evn) {
    evn.preventDefault();
    $('html, body').animate({ scrollTop: $($(this).attr('href')).offset().top - 40 }, 500, 'linear');
});

$('.close-register-message').click(function () {
    $.ajax({
        type: "GET",
        url: "/Home/UserVerified",
        contenttype: "application/json",
        async: true,
        success: function (data) {
            $(".register-message").hide();
        }
    });
});
$(document).ready(function () {
    $("#aboutSlide").click(function (evn) {
        evn.preventDefault();
        if (window.location.pathname == "/home1/faq" || window.location.pathname == "/home1/contact"|| window.location.pathname == "/faq" || window.location.pathname == "/contact"  || window.location.pathname == "truck/rates/index.html")
            window.open("home1/index/index.html#about", '_blank');//window.location.href = '/Home/Index/#about';
        $('html, body').animate({ scrollTop: $($(this).attr('href')).offset().top - 40 }, 500, 'linear');
    });
    $("#goToAbout").click(function () {
        window.location.href = "home1/index/index.html#about";
    });
});