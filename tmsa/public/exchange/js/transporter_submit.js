$(document).ready(function() {

    $(".carrier_form_sumbit").unbind('click').click(function(e){

        loaderOpen();
        e.preventDefault();

    $.ajax({

        async:false,

        processData: false,

        contentType: false,

        url: APP_URL+"/transporter/submit_request",

        type:'POST',

        data: new FormData($("#carrier_form")[0]),

        success: function(data) {

            if($.isEmptyObject(data.error)){
                loaderClose();
                printSuccessMsg(data.success);
            }else{

                loaderClose();
                printErrorMsg(data.error);


            }

        }

    });

    loaderClose()
});
    function printErrorMsg (msg) {

        $(".print-error-msg").find("ul").html('');

        $(".print-error-msg").css('display','block');

        $.each( msg, function( key, value ) {

            $(".print-error-msg").find("ul").append('<li>'+value+'</li>');
            $(".print-error-msg").fadeOut(5000);
        });

    }

    function printSuccessMsg (success) {

        $(".print-success-msg").find("ul").html('');

        $(".print-success-msg").css('display','block');


        $(".print-success-msg").find("ul").append(success);
        $(".print-success-msg").fadeOut(20000);

    }
});