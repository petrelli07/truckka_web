<?php


function find_gps_route($uniqueId,$from=null,$to=null) {
  if(is_array($uniqueId)) {
    $response=[];
    foreach($uniqueId as $device) {
      $response[$device]=find_gps_route($device);
    }
    return $response;
  }

  if($from==null) {$from=strtotime("-1 week");}
  if($to==null) {$to=time();}

  $from=str2time($from);
  $to=str2time($to);


  $result=array();

  //fetch matching device
  $device=trace_get('devices',array('uniqueId'=>$uniqueId));
  if($device==null) {return $result;}


  $result['id']=$device[0]->id;
  $result['name']=$device[0]->name;
  $result['status']=$device[0]->status;
  $result['disabled']=$device[0]->disabled;
  $result['positionId']=$device[0]->positionId;
  $result['routes']=array();

  //var_dump($result);

  //try find routes
  $routes=trace_get('reports/route',['deviceId'=>$result['id'],'from'=>$from,'to'=>$to]);
  if($routes==null) {return $result;}

  foreach($routes as $route) {
    $result['routes'][]=array(
      'lat'=>$route->latitude,
      'lng'=>$route->longitude,
  );
  }

  return $result;
}


function find_gps_location($uniqueId) {
  if(is_array($uniqueId)) {
    $response=[];
    foreach($uniqueId as $device) {
      $response[$device]=find_gps_location($device);
    }
    return $response;
  }
  $result=array(
    'name'=>'',
    'uniqueId'=>$uniqueId,
    'status'=>'',
    'lng'=>0,
    'lat'=>0,
    'positionId'=>null,
    'address'=>null,
    'disabled'=>true,
    'success'=>false,
  );

  //fetch matching device
  $device=trace_get('devices',array('uniqueId'=>$uniqueId));
  if($device==null) {return $result;}

  $result['name']=$device[0]->name;
  $result['status']=$device[0]->status;
  $result['disabled']=$device[0]->disabled;
  $result['positionId']=$device[0]->positionId;

  //try find location
  $location=trace_get('positions',['id'=>$result['positionId']]);
  if($location==null) {return $result;}

  $result['lat']=$location[0]->latitude;
  $result['lng']=$location[0]->longitude;
  $result['address']=$location[0]->address;
  $result['success']=true;

  return $result;

}




function str2time($time=null, $timeZone='Africa/Lagos') {
  if($time==null) {$time=time();}
  if(!is_int($time)) {
    $time = strtotime($time);
  }
  $dateInLocal = date("Y-m-d H:i:s", $time);
  $dt = new DateTime($dateInLocal, new DateTimeZone($timeZone));
  $dt->setTimezone(new DateTimeZone('UTC'));
  $utcTime = $dt->format('Y-m-d H:i:s');
  $returnObject = new DateTime($utcTime);
  $returnIso = substr($returnObject->format(DateTime::ATOM), 0, -6) . '.000Z';
  return $returnIso;
}




/**
 * Converts an array to json, prints output to browser and exits
 *
 * @param array $arr The array of directives
 * @param int   $status The HTTP Status (default is 200)
 * @param boolean $pretty Should output be in pretty_print
 *
 * @return void
 */
function respond($arr = array(), $status = 200, $pretty = true)
{
    http_response_code($status);

    header("Content-type:text/javaScript");

    header('Access-Control-Allow-Origin: *');

    //$arr = anything_to_utf8($arr, true);

    if ($pretty) {
        $resp = json_encode($arr, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
    } else {
        $resp = json_encode($arr);
    }

    echo $resp;
    exit();
}
