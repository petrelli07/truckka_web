$(function() {


$('#animate').on('click',function() {
  reset_markers();
});

fetch_markers();

});



function fetch_markers() {
  var request = $.ajax({
    url: "../resources.php",
    type: "GET",
    data: {params : params},
    dataType: "json"
  });

  request.done(function(json) {
    markers=json;

    console.log(json);
    initialize();

    reset_markers();

    setmarkers();
  });
  request.fail(function(jqXHR, textStatus) {
    //alert( "Request failed: " + textStatus );
  });
  request.complete(function() {

    //fetch again after sometime
    setTimeout(function() {
      fetch_markers();
    },60000);
  });

}

var markers;
var map;
var jmarkers=[];
var initialized=false;

function initialize() {
  if(initialized) {return;}

  initialized=true;


  map = new google.maps.Map(document.getElementById('map_canvas'), {
    zoom: 10,
    center: new google.maps.LatLng(markers[0][1], markers[0][2]),
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });

  /*
  var infowindow = new google.maps.InfoWindow();
  var marker, i;

  for (i = 0; i < markers.length; i++) {
    marker = new google.maps.Marker({
      position: new google.maps.LatLng(markers[i][1], markers[i][2]),
      map: map
    });

    google.maps.event.addListener(marker, 'click', (function(marker, i) {
      return function() {
        infowindow.setContent(markers[i][0]);
        infowindow.open(map, marker);
      }
    })(marker, i));
  }
  */

}

function setmarkers() {  var infowindow = new google.maps.InfoWindow();
  var marker, i;

  for (i = 0; i < markers.length; i++) {
    marker = new google.maps.Marker({
      position: new google.maps.LatLng(markers[i][1], markers[i][2]),
      map: map
    });

    jmarkers.push(marker);

    google.maps.event.addListener(marker, 'click', (function(marker, i) {
      return function() {
        infowindow.setContent(markers[i][0]);
        infowindow.open(map, marker);
      }
    })(marker, i));
  }
}



function reset_markers() {
  try {
  //attempt resetting the map
  if(jmarkers) {
    for(var i=0; i< jmarkers.length; i++)
    {
      jmarkers[i].setMap(null);
    }
  }

  } catch(e) {}

  //initialize();
}
