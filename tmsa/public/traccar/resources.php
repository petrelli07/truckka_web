<?php
include __DIR__.'/api/vendor.php';

$params='W3siaWQiOjEsIm5hbWUiOiJkZmdoIiwibGFiZWwiOiJnaGtsIiwiZ3BzaWQiOiI4NjQ4OTUwMzAxNTk2NDIifSx7ImlkIjoyLCJuYW1lIjoiSm9obiBMb2JpIiwibGFiZWwiOiJCYWdzIG9mIFN1Z2FyIiwiZ3BzaWQiOiI4NjQ4OTUwMzAxNTk3NjYifSx7ImlkIjozLCJuYW1lIjoiZGZnaCIsImxhYmVsIjoiZ2hrbCIsImdwc2lkIjoiODY0ODk1MDMwMTU5MzUyIn1d';

if(isset($_GET['params'])) {
$params=$_GET['params'];
}

$params=json_decode(base64_decode($params),1);

$uniqueId=array();
foreach($params as $param) {
$uniqueId[]=$param['gpsid'];
}

$locations=find_gps_location($uniqueId);

//['Bondi Beach', -33.890542, 151.274856]

foreach($params as $param) {
$key=$param['gpsid'];
$locations[$key]['_id']=$param['id'];
$locations[$key]['_label']=$param['label'];
$locations[$key]['_name']=$param['name'];
}

//build final output
$result=array();
foreach($locations as $info) {
$result[]=array($info['_name'],$info['lat'],$info['lng']);
}

header("Content-type:text/javascript");
echo(json_encode($result,JSON_PRETTY_PRINT));
