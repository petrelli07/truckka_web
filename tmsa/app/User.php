<?php

namespace App;

use App\TruckRegistry\TruckOwner;
use App\Exchange\TruckSupplier;
use App\Exchange\ExchangeOrder;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens,Notifiable;



    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'default_password','userAccessLevel','phone'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function ServiceRequest() {
        return $this->hasMany('App\serviceRequest');
    }

    public function CarrierResource() {
        return $this->hasMany('App\carrierResource');
    }

    public function ClientOrder() {
        return $this->hasOne('App\ClientOrder');
    }

    public function TruckOwner(){
        return $this->hasMany(TruckOwner::class);
    }

    public function TruckSupplier(){
        return $this->hasOne(TruckSupplier::class);
    }

    public function ExchangeOrder(){
        return $this->hasMany(ExchangeOrder::class);
    }
}
