<?php

class TraceAPI
{
    //api username
    public $username = 'olaitan.adesanya@gmail.com';

    //api password
    public $password = '4HLZcaKD';

    //api baseurl
    public $base_url = 'http://server.traccar.org/api/';

    //curl connection
    private $ch;

    public function __construct()
    {

        //open connection
        $this->ch = curl_init();

        curl_setopt($this->ch, CURLOPT_HTTPHEADER,
            ["Authorization: Basic " . base64_encode($this->username . ":" . $this->password),
            ]);

    }

    //send request
    public function get($method, $params = array())
    {
        $url = $this->base_url . $method;

        $_result = $this->_http_get($url, $params);

        $result = json_decode($_result);
        return is_null($result) ? $_result : $result;
    }

    public function delt($method, $params = array())
    {
        $url = $this->base_url . $method;

        $_result = $this->_http_del($url, $params);

        $result = json_decode($_result);
        return is_null($result) ? $_result : $result;
    }

    //send request
    public function post($method, $params = array())
    {
        $url = $this->base_url . $method;

        $_result = $this->_http_post($url, $params);

        $result = json_decode($_result);
        return is_null($result) ? $_result : $result;
    }


    //send curl request
    public function curl($method, $params = '')
    {
        $url = $this->base_url . $method;

        $_result = $this->_http_curl($url, $params);

        $result = json_decode($_result);
        return is_null($result) ? $_result : $result;
    }


    /** preprocess parameters e.g.
     * array('uniqueId'=>['864895030159642','864895030159766','864895030159352'])
     */
    public function cross_merge($key, $value)
    {
        $data = [];
        foreach ($value as $lbl) {
            $data[] = "$key=$lbl";
        }
        return implode('&', $data);
    }

    /**
     * _http_post
     *
     * post a remote url and returns the response
     *
     * @param  string  $url    The target url
     * @param  array   $field  (optional) the post parameters
     *
     * @return string
     */
    public function _http_post($url, $fields = array())
    {

        //url-ify the data for the POST
        $fields_string = '';
        if (!empty($fields)) {
            foreach ($fields as $key => $value) {

                  if (is_array($value)) {
                      $fields_string .= $this->cross_merge($key, $value) . '&';
                  } else {
                    $fields_string .= $key . '=' . $value . '&';
                  }
            }
            $fields_string = rtrim($fields_string, '&');
        }

        curl_setopt($this->ch, CURLOPT_URL, $url);
        curl_setopt($this->ch, CURLOPT_POST, count($fields));
        curl_setopt($this->ch, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');

        //execute post
        $result = curl_exec($this->ch);



        //close connection
        curl_close($this->ch);

        return $result;
    }

    public function _http_curl($url, $data = array())
    {

      //var_dump($url);
      //var_dump($fields);
      //die();

      $data_string = json_encode($data);



      curl_setopt_array($this->ch, array(
          CURLOPT_URL => $url,
          CURLOPT_CUSTOMREQUEST=> 'POST',
          CURLOPT_POST => true,
          CURLOPT_POSTFIELDS => $data_string,
          CURLOPT_HEADER => false,
          CURLOPT_RETURNTRANSFER=>true,
          CURLOPT_HTTPHEADER => array(
            "Authorization: Basic " . base64_encode($this->username . ":" . $this->password),
            'Content-Type:application/json', 'Content-Length: ' . strlen($data_string)
          )
        )
      );

      $result = curl_exec($this->ch);

      return $result;
    }


    /**
     * _http_get
     *
     * gets a remote url and returns the response
     *
     * @param  string  $url    The target url
     * @param  array   $field  (optional) the get parameters
     *
     * @return string
     */
    public function _http_get($url, $fields = array())
    {
        $fields_string = '';
        if (!empty($fields)) {
            foreach ($fields as $key => $value) {

                  if (is_array($value)) {
                      $fields_string .= $this->cross_merge($key, $value) . '&';
                  } else {
                    $fields_string .= $key . '=' . $value . '&';
                  }
            }
            $fields_string = rtrim($fields_string, '&');
            $url .= '?' . $fields_string;
        }


        //set the url, number of POST vars, POST data
        curl_setopt($this->ch, CURLOPT_URL, $url);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');

        //execute post
        $result = curl_exec($this->ch);

        //close connection
        curl_close($this->ch);

        return $result;
    }

    public function _http_del($url, $fields = array())
    {
        $fields_string = '';
        if (!empty($fields)) {
            foreach ($fields as $key => $value) {

                  if (is_array($value)) {
                      $fields_string .= $this->cross_merge($key, $value) . '&';
                  } else {
                    $fields_string .= $key . '=' . $value . '&';
                  }
            }
            $fields_string = rtrim($fields_string, '&');
            $url .= '?' . $fields_string;
        }


        //set the url, number of POST vars, POST data
        curl_setopt($this->ch, CURLOPT_URL, $url);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->ch,CURLOPT_CUSTOMREQUEST,'DELETE');
        curl_setopt($this->ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');

        //execute post
        $result = curl_exec($this->ch);

        //close connection
        curl_close($this->ch);

        return $result;
    }

}
