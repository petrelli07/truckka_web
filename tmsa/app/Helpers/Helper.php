<?php
/*include __DIR__.'/api/vendor.php';*/
include __DIR__.'/../../public/traccar/api/vendor.php';

use DB 		as db;
use Auth 	as auth;

class Helper{
	public static function checker($value){
		$userId             = Auth::user()->id;
		$namespace					=	$value;
		$query              = DB::table('user_permissions')
                            ->join('modules','modules.id','=','user_permissions.module_id')
                            ->where('user_permissions.user_id', '=', $userId);

    for ($i=0; $i < sizeof($namespace); $i++) {
    	if ($i == 0 ) {
    		$query->where('modules.moduleMethod','=',$namespace[$i]);
    	}else{
    		$query->orWhere('modules.moduleMethod','=',$namespace[$i]);
    	}
    }

    $result =	$query->count();

		if ($result > 0) {
        return true;
    }else{
    	return false;
    }
	}

	public static function  hasPermissionController($namespace){
			return	Helper::checker($namespace);
	}

	public static function hasPermissionUrl( $passedUrl){
    $getRouteCollection = Route::getRoutes();
    $array 							=	array();

     //get and returns all returns route collection

		foreach ($getRouteCollection as $route) {
			if (in_array($route->uri(),  $passedUrl)) {
				$namespace					=	str_replace("@",'\\', $route->getActionName());
				array_push($array, $namespace);
			}
		}

	return	Helper::checker($array);
	}

}
