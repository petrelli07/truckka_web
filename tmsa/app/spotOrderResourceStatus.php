<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class spotOrderResourceStatus extends Model
{
    public function spotOrderResource()
    {
    	return $this->hasMany('App\spotOrderResource');
    }
}
