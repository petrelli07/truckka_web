<?php

namespace App\Notifications\Exchange;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NewQuote extends Notification
{
    use Queueable;

    protected $reference_number;


    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($reference_number)
    {
        $this->reference_number = $reference_number;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('Wee are pleased to inform you that a quote for your order '.$this->reference_number.' has been generated.')
                    ->action('Click the link to view', url('/exchange/customer/invoice_details/'.$this->reference_number))
                    ->line('We hope you enjoy using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
