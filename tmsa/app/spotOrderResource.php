<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class spotOrderResource extends Model
{
    
    public function spotOrderResource()
    {
    	return $this->belongsTo('App\spotOrder');
    }

    public function spotOrderResourceStatus()
    {
    	return $this->belongsTo('App\spotOrderResourceStatus');
    }
}
