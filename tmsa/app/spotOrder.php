<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class spotOrder extends Model
{
    public function spotOrderStatus()
    {
        return $this->belongsTo('App\spotOrderStatus');
    }

    public function spotOrderInvoice()
    {
    	return $this->hasMany('App\spotOrderInvoice');
    }

    public function spotOrderPayment()
    {
    	return $this->hasMany('App\spotOrderPayment');
    }

    public function spotOrderResource()
    {
    	return $this->hasMany('App\spotOrderResource');
    }

    public function spotOrderRoutePicing()
    {
    	return $this->belongsTo('App\spotOrderRoutePicing');
    }

    public function spotOrderReview()
    {
    	return $this->hasOne('App\spotOrderReview');
    }

}
