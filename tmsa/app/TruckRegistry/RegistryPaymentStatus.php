<?php

namespace App\TruckRegistry;
use App\TruckRegistry\RegistryPayment;

use Illuminate\Database\Eloquent\Model;

class RegistryPaymentStatus extends Model
{
    public function RegistryPayment(){
        return $this->hasMany(RegistryPayment::class);
    }
}
