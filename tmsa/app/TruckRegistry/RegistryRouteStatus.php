<?php

namespace App\TruckRegistry;
use App\TruckRegistry\RegistryRoutes;

use Illuminate\Database\Eloquent\Model;

class RegistryRouteStatus extends Model
{
    public function RegistryRoutes(){
        return $this->hasMany(RegistryRoutes::class);
    }
}
