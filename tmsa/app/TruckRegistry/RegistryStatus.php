<?php

namespace App\TruckRegistry;
use App\TruckRegistry\TruckOrderRegistry;

use Illuminate\Database\Eloquent\Model;

class RegistryStatus extends Model
{
    public function TruckOrderRegistry(){
        return $this->hasMany(TruckOrderRegistry::class);
    }
}
