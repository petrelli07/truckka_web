<?php

namespace App\TruckRegistry;
use App\TruckRegistry\TruckOwnerStatus;
use App\TruckRegistry\Truck;
use App\User;

use Illuminate\Database\Eloquent\Model;

class TruckOwner extends Model
{
    public function TruckOwnerStatus(){
        return $this->belongsTo(TruckOwnerStatus::class);
    }

    public function Truck(){
        return $this->hasMany(Truck::class);
    }

    public function FieldAgent(){
        return $this->belongsTo(User::class);
    }
}
