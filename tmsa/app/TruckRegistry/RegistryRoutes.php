<?php

namespace App\TruckRegistry;
use App\TruckRegistry\RegistryRouteStatus;

use Illuminate\Database\Eloquent\Model;

class RegistryRoutes extends Model
{
    public function RegistryRouteStatus(){
        return $this->belongsTo(RegistryRouteStatus::class);
    }
}
