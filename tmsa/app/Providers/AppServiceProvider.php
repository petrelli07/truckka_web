<?php

namespace App\Providers;

use DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        //
        //return;

        //check for devices that have not been registered to the api
         $resources = DB::table('carrier_resource_details')->where('installer', '0')->get();
         foreach ($resources as $resource) {

             //retrieve info of device
             $plateNumber = $resource->plateNumber;
             $imei = $resource->GPSID;

             //register device to api
             $device = device_add($imei, $plateNumber);
             if ($device->status == 200 || $device->status == 201) {

                 //update the installed flag to 1
                 DB::table('carrier_resource_details')
                     ->where('id', $resource->id)
                     ->update(['installer' => 1]);
             }

         }

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
