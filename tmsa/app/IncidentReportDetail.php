<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IncidentReportDetail extends Model
{
    public function IncidentReport() {
        return $this->belongsTo('App\IncidentReport');
    }
}
