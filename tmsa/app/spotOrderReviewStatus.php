<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class spotOrderReviewStatus extends Model
{
    public function spotOrderReview()
    {
    	return $this->hasMany('App\spotOrderReview');
    }
}
