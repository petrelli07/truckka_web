<?php

namespace App\Http\Controllers\TMSW\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class PerformanceController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');
    }

    public function userAccessLevel(){
        $role  =   Auth::user()->userAccessLevel;
        switch ($role) {
            case 12:
                return 'administrator';
                break;
            case 13:
                return 'manager';
                break;
            case 14:
                return 'desk_officer';
                break;
            case 15:
                return 'finance';
                break;
            case 16:
                return 'transporter';
                break;
            case 17:
                return 'supplier';
                break;
            default:
                return '/';
                break;
        }
    }

    public function company_id(){
        $user_id    =   Auth::user()->id;
        $company_id =   CompanyUser::where('user_id',$user_id)->value('tmsw_company_detail_id');
        return $company_id;
    }

    public function performance(){
        $accessLevel    =   $this->userAccessLevel();
        $breadcrumb     =   "Performance";
        return view('tmsw.client.performance',compact('accessLevel','breadcrumb'));
    }

    public function truck_orders(){
        $accessLevel    =   $this->userAccessLevel();
        $breadcrumb     =   "All Truck Orders";
        return view('tmsw.client.truck_performance',compact('accessLevel','breadcrumb'));
    }

    public function supply(){
        $accessLevel    =   $this->userAccessLevel();
        $breadcrumb     =   "All Truck Orders";
        return view('tmsw.client.supply_performance',compact('accessLevel','breadcrumb'));
    }

    public function finance(){
        $accessLevel    =   $this->userAccessLevel();
        $breadcrumb     =   "All Truck Orders";
        return view('tmsw.client.finance_performance',compact('accessLevel','breadcrumb'));
    }
}
