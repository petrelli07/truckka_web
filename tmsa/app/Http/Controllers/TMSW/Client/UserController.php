<?php

namespace App\Http\Controllers\TMSW\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\TMSW\tmswCompanyDetail;
use App\TMSW\tmswRole;

use App\TMSW\tmswCompanyUser;
use App\TMSW\tmswOrder as Order;
use App\TMSW\tmswCompanyUser as CompanyUser;
use Notification;
use App\Notifications\tmsw\newAccount;
use Auth;
use DB;

class UserController extends Controller
{

    public $active      =   1; 
    public $inactive    =   2;



    public function __construct()
    {
        $this->middleware('auth');
    }

    public function userAccessLevel(){
        $role  =   Auth::user()->userAccessLevel;
        switch ($role) {
            case 12:
                return 'administrator';
                break;
            case 13:
                return 'manager';
                break;
            case 14:
                return 'desk_officer';
                break;
            case 15:
                return 'finance';
                break;
            case 16:
                return 'transporter';
                break;
            case 17:
                return 'supplier';
                break;
            default:
            return '/';
            break;
        }
    }

    public function company_id(){
        $user_id    =   Auth::user()->id;
        $company_id =   CompanyUser::where('user_id',$user_id)->value('tmsw_company_detail_id');
        return $company_id;
    }

    public function createGroupView(){
        $breadcrumb = 'Client dashboard';
        $accessLevel    =   $this->userAccessLevel();

       $roles =  DB::table('tmsw_roles')->get();


       $data   =     array('roles'          =>$roles,
                            'breadcrumb'    =>$breadcrumb,
                            'accessLevel'   =>$accessLevel
                            );

        return view('tmsw.group.create-group',$data);

    }

    public function createGroupSave(Request $request){
       //TODO put validation here // validate form fields
        $this->validate($request, [
            'name'=>'required|unique:tmsw_roles_group,name',
        ]);

        $groupName  =    $request->name;
        $groupRoles =    $request->roles;
        $data       =    array('name'=>$groupName);
        DB::beginTransaction();

        $groupId    =   DB::table('tmsw_roles_group')->insertGetId($data);

        foreach ($groupRoles as $role) {
            $data   =    array('group_id'   =>  $groupId,
                                'role_id'   =>  $role
                                );

            DB::table('tmsw_group_roles_matching')->insert($data);           
        }
         DB::commit();    // Commiting  ==> There is no problem whatsoever

        return back()->withMessage('successful');
    }



    public function getIndex(){
        $breadcrumb = 'Client dashboard';
        $accessLevel    =   $this->userAccessLevel();

        $user   =  Order::join('tmsw_routes','tmsw_orders.tmsw_route_id','=',
            'tmsw_routes.id')
            ->join('tmsw_states as origin_state','tmsw_routes.origin_state_id','origin_state.id')
            ->join('tmsw_states as destination_state','tmsw_routes.destination_state_id','destination_state.id')
            ->join('tmsw_truck_types','tmsw_orders.tmsw_truck_type_id',
                '=','tmsw_truck_types.id')
            ->join('tmsw_statuses','tmsw_orders.tmsw_status_id',
                '=','tmsw_statuses.id')
            ->select('tmsw_orders.*','tmsw_truck_types.*','origin_state.state_description as originStateName','destination_state.state_description as destStateName','tmsw_statuses.*','tmsw_routes.*')
            ->where('tmsw_orders.tmsw_company_detail_id',$this->company_id())
            ->get();

        return view('tmsw.client.index',compact('breadcrumb','accessLevel','user'));
    }


    public function CreateUsers(){
        $breadcrumb = 'Client dashboard';
        $company = tmswCompanyDetail::get();
        $groups     =   DB::table('tmsw_roles_group')
                            ->get();
        $accessLevel    =   $this->userAccessLevel();
        return view('tmsw.client.create-user')
        ->withBreadcrumb($breadcrumb)
        ->withCompany($company)
        ->withAccessLevel($accessLevel)
        ->withGroups( $groups);

    }

    private function mt_rand_str_password ($l, $c = 'abcdefghijklmnopqrstuvwxyz1234567890') {
		for ($s = '', $cl = strlen($c)-1, $i = 0; $i < $l; $s .= $c[mt_rand(0, $cl)], ++$i);
		return $s;
    }

    public function getGroupRoles(Request $request){
        $groupId    =   $request->groupId;
        $roles = DB::table('tmsw_group_roles_matching')->where('tmsw_group_roles_matching.group_id',$groupId)
                                                     ->join('tmsw_roles','tmsw_roles.id','=','tmsw_group_roles_matching.role_id')
                                                        ->select('*','tmsw_roles.id as role_id','tmsw_roles.name as role_name')
                                                        ->get();
        return response()->json($roles);
    }

    public function StoreUsers(Request $request) {

          // validate form fields
          $this->validate($request, [
            'name'=>'required',
            'phone'=>'required|unique:users,phone',
            'email'=>'required|email|unique:users,email',
            
        ]);

        ////  STARTING DB TRANSACTIONS ///
        DB::beginTransaction();

        try {

        //// INSERTING TO USERS TABLE ////
        $name   =   $request->name;
        $email   =   $request->email;
        $phone   =   $request->phone;
        $password   =   $this->mt_rand_str_password(8);
        $encrypt_pass   =   bcrypt($password);

        $user                       =   new User;
        $user->name                 =   $name;
        $user->email                =   $email;
        $user->phone                =   $phone;
        $user->password             =   $encrypt_pass;
        $user->default_password     =   0;
        $user->userAccessLevel      =   $request->userlevel;
        $user->save();
        $userID = $user->id;

        ///// THIS AREAS IS FOR NOTIFICATIONS///
        $notification = $user->notify(new newAccount($password));

        /// END OF IT////

        ///// END OF IT //////////////////

        //// INSERTING TO COMPANIES_USERS TABLE ////
        $user                           =   new tmswCompanyUser;
        $user->user_id                  =   $userID;
        $user->tmsw_company_detail_id   =   $this->company_id();
        $user->tmsw_status_id           =   1;
        $user->save();
         ///// END OF IT //////////////////


         DB::commit();    // Commiting  ==> There is no problem whatsoever
         return back()->withMessage('User Created');

        } catch (\Exception $e) {
            DB::rollback();   // rollbacking  ==> Something went wrong
            return back()->withMessage('Something went wrong');
        }

    }


    public function ViewUsers() {
        $breadcrumb = 'Client dashboard';
        $groups     =   DB::table('tmsw_roles_group')->get();

        $usersinfo = tmswCompanyUser::get();
        $users = array();
        foreach($usersinfo as $rows => $row) {
        $row['company_name'] = tmswCompanyDetail::where('id',$row->tmsw_company_detail_id)->value('company_name');
        $row['name'] = user::where('id',$row->user_id)->value('name');
        $row['email'] = user::where('id',$row->user_id)->value('email');
        $row['status'] = user::where('id',$row->user_id)->value('status');
        $users[] = $row;
        }

        $accessLevel    =   $this->userAccessLevel();
        return view('tmsw.client.view-users')
        ->withBreadcrumb($breadcrumb)
        ->withUsers($users)
        ->withAccessLevel($accessLevel)
        ->withGroups($groups);

    }

    public function SuspendUsers(Request $request){
        
        $id = $request->id;
        $post = User::findOrFail($id);
        $post->status  = $this->inactive;
        $post->save();
        return back()->withMessage('User Suspended');

    }

    public function UnsuspendUsers(Request $request){
         $id = $request->id;
         $post = User::findOrFail($id);
         $post->status  = $this->active;
         $post->save();
         return back()->withMessage('User Unsuspended');

    }

    public function DeleteUsers(Request $request){
        $id = $request->id;
        $post = User::find($id);
        $post->delete();
        return back()->withMessage('User Deleted');

    }


    
}
