<?php

namespace App\Http\Controllers\TMSW\Client;

use App\Notifications\tmsw\newOrder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\TMSW\tmswRoute as Route;
use App\TMSW\tmswState as State;
use App\TMSW\tmswTruckType as TruckType;
use App\TMSW\tmswOrder as Order;
use App\TMSW\tmswCompanyUser as CompanyUser;
use App\TMSW\tmswTruckOrder as TruckDetail;
use App\TMSW\tmswTransporter;
use App\TMSW\tmswInvoice as Invoice;
use App\User;
use Image;
use Auth;
use Notification;

class TruckOrderController extends Controller
{
    public $active_route        =   1;
    public $inactive_route      =   2;
    public $transporters        =   16;

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function userAccessLevel(){
        $role  =   Auth::user()->userAccessLevel;
        switch ($role) {
            case 12:
                return 'administrator';
                break;
            case 13:
                return 'manager';
                break;
            case 14:
                return 'desk_officer';
                break;
            case 15:
                return 'finance';
                break;
            case 16:
                return 'transporter';
                break;
            case 17:
                return 'supplier';
                break;
            default:
                return '/';
                break;
        }
    }

    public function user_id(){
        $user_id    =   Auth::user()->id;
        return $user_id;
    }

    public function company_id(){
        $user_id    =   Auth::user()->id;
        $company_id =   CompanyUser::where('user_id',$user_id)->value('tmsw_company_detail_id');
        return $company_id;
    }

/*@manage routes*/
    public function allRoutes(){
        $breadcrumb =   "All Routes";
        $route  =   Route::join('tmsw_statuses','tmsw_routes.tmsw_status_id','tmsw_statuses.id')
            ->join('tmsw_states as origin_state','tmsw_routes.origin_state_id','origin_state.id')
            ->join('tmsw_states as destination_state','tmsw_routes.destination_state_id','destination_state.id')
            ->select('origin_state.state_description as originStateName','destination_state.state_description as destStateName','tmsw_statuses.*','tmsw_routes.*')
            ->get();
        $accessLevel    =   $this->userAccessLevel();
        return view('tmsw.client.all_routes',compact('route','breadcrumb','accessLevel'));
    }

    public function deactivateRoute($route_id){
        Route::where('id',$route_id)
            ->update(
                array(
                    'tmsw_status_id' => $this->inactive_route
                )
            );
        return back()->with('message','Route Deactivated Successfully');
    }

    public function activateRoute($route_id){
        Route::where('id',$route_id)
            ->update(
                array(
                    'tmsw_status_id' => $this->active_route
                )
            );
        return back()->with('message','Route activated Successfully');
    }

    public function createRouteView(){
        $states =  State::all();
        $breadcrumb =   "All Routes";
        $accessLevel    =   $this->userAccessLevel();
        return view('tmsw.client.create_new_route',compact('states','breadcrumb','accessLevel'));
    }

    public function createRoute(Request $request){

        $this->validate($request, [
            'origin'=>'required',
            'destination'=>'required',
            'price'=>'required',
        ]);
        //return $request->all();
        $origin         =   $request->origin;
        $destination    =   $request->destination;
        $price          =   $request->price;


        if($origin == $destination){
            return back()->with('message','Origin and Destination can not have same values');
        }

        if($this->checkRouteExist($origin, $destination)){
            return back()->with('message','this route already exists');
        }


        $sanitized_price    =    $this->clean($price);//removed all commas in price

        $route  =   new Route;
        $route->origin_state_id         =   $origin;
        $route->destination_state_id    =   $destination;
        $route->price                   =   $sanitized_price;
        $route->tmsw_status_id          =   $this->active_route;
        $route->save();

        return back()->with('message','New Route Created Successfully');
    }


    public function editRoute($route_id){
        $route_detail   =   Route::where('tmsw_routes.id',$route_id)
            ->join('tmsw_states as origin_state','tmsw_routes.origin_state_id','origin_state.id')
            ->join('tmsw_states as destination_state','tmsw_routes.destination_state_id','destination_state.id')
            ->select('origin_state.state_description as originStateName','destination_state.state_description as destStateName','tmsw_routes.*')
            ->get();

        $accessLevel    =   $this->userAccessLevel();
        $breadcrumb =   "Update Route";

        foreach($route_detail as $detail){
            $origin         =   $detail->originStateName;
            $destination    =   $detail->destStateName;
            $price          =   $detail->price;
            $id             =   $detail->id;
        }

        $data   =   [
            'id'             => $id,
            'origin'         => $origin,
            'destination'    => $destination,
            'price'          => $price,
        ];

        return view('tmsw.client.edit_route',compact('data','breadcrumb','accessLevel'));
    }

    public function updateRoute(Request $request){
        $this->validate($request, [
            'route_id'=>'required|numeric',
            'price'=>'required',
        ]);

        $route_id   =   $request->route_id;
        $price      =   $request->price;

        $sanitized_price    =    $this->clean($price);//removed all commas in price

        Route::where('id',$route_id)
            ->update(
                array(
                    'price'=>$sanitized_price
                )
            );
        return redirect('/tmsw/manager/manage_routes/')->with('message','Route updated Successfully');
    }

    public function checkRouteExist($origin,$destination){

        $route_exist  =   Route::where('origin_state_id',$origin)
            ->where('destination_state_id',$destination)
            ->count();

        if($route_exist >= 1){
            return true;
        }
    }

    public function clean($string) {
        $string = str_replace(',', '', $string); // Replaces all spaces with hyphens.

        return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }
/*@end*/

    /*order management*/
    public function createOrderView(){
        $breadcrumb     =   "Create New Order";
        $truckTypes =	TruckType::where('tmsw_status_id',7)->get();


        $route 	    =	Route::where('tmsw_status_id',1)->pluck('origin_state_id');

        $state_description  =   [];

        for($i=0;$i<count($route);$i++){
            $state_description[]    =   State::where('id',$route[$i])->value('state_description');
        }

        $array_unique  =   array_unique($state_description);

        $uniqueOrigins = array_values($array_unique);
        $accessLevel    =   $this->userAccessLevel();
        return view('tmsw.client.new-orders',compact('breadcrumb','truckTypes','uniqueOrigins','accessLevel'));
    }




    public function checkOrigins(Request $request)
    {
        $originToServer     = $request->originToServer;

        $origin_id          =   State::where('state_description',$originToServer)->value('id');

        $destination_id 	= Route::where('origin_state_id',$origin_id)->pluck('destination_state_id');

        $destination_description    =   [];

        for($i=0;$i<count($destination_id);$i++){
            $destination_description[]  =   State::where('id',$destination_id[$i])->pluck('state_description');
        }

        return response()->json(['success'=>$destination_description]);

    }


    public function submitOrder(Request $request){
        //return $request->all();
        $this->validate($request, [
            'origin'=>'required',
            'destination'=>'required',
            'truckType'=>'required',
            'number_of_truck'=>'required',
            'pickup_address'=>'required',
            'delivery_address'=>'required',
            'contact_name'=>'required',
            'contact_phone'=>'required',
        ]);

        $origin 			    = $request->origin;
        $destination 		    = $request->destination;
        $resourceType 		    = $request->truckType;
        $truckNumber 		    = $request->number_of_truck;
        $delivery_address 		= $request->delivery_address;
        $pickup_address 		= $request->pickup_address;
        $contact_name 		    = $request->contact_name;
        $contact_phone 		    = $request->contact_phone;


        $or            =   State::where('state_description',$origin)->get();

        foreach($or as $origins){
            $origin_state_id    =  $origins->id;
        }

        $dest       =   State::where('state_description',$destination)->get();

        foreach($dest as $destinations){
            $destination_state_id   =   $destinations->id;
        }

        $route  =   Route::where('origin_state_id',$origin_state_id)
            ->where('destination_state_id',$destination_state_id)
            ->get();

        foreach($route as $routes){
            $route_id       =   $routes->id;
        }

        $reference_number   =   $this->orderNumber();


        $TruckOrder  =   new Order;

        $TruckOrder->reference_number                       =   $reference_number;
        $TruckOrder->truck_number                           =   $truckNumber;
        $TruckOrder->tmsw_truck_type_id                     =   $resourceType;
        $TruckOrder->pickup_address                         =   $pickup_address;
        $TruckOrder->delivery_address                       =   $delivery_address;
        $TruckOrder->tmsw_route_id                          =   $route_id;
        $TruckOrder->contact_name                           =   $contact_name;
        $TruckOrder->contact_phone                          =   $contact_phone;
        $TruckOrder->tmsw_company_detail_id                 =   $this->company_id();
        $TruckOrder->user_id                                =   $this->user_id();
        $TruckOrder->tmsw_status_id                         =   9;
        $TruckOrder->save();

        $transporters   =   User::where('userAccessLevel',$this->transporters)->get();

        if(count($transporters) > 0){
            for($x=0;$x<count($transporters);$x++){
                $user    =    User::find($transporters[$x]['id']);

                $notification = $user->notify(new newOrder($reference_number));
            }
        }

        return back()->with('message','Order Created Successfully. Awaiting Transporter Response');
    }

    public function userOrders(){
        $breadcrumb =   "Home";
        $user   =  Order::join('tmsw_routes','tmsw_orders.tmsw_route_id','=',
            'tmsw_routes.id')
            ->join('tmsw_states as origin_state','tmsw_routes.origin_state_id','origin_state.id')
            ->join('tmsw_states as destination_state','tmsw_routes.destination_state_id','destination_state.id')
            ->join('tmsw_truck_types','tmsw_orders.tmsw_truck_type_id',
                '=','tmsw_truck_types.id')
            ->join('tmsw_statuses','tmsw_orders.tmsw_status_id',
                '=','tmsw_statuses.id')
            ->select('tmsw_orders.*','tmsw_truck_types.*','origin_state.state_description as originStateName','destination_state.state_description as destStateName','tmsw_statuses.*','tmsw_routes.*')
            ->where('tmsw_orders.user_id',$this->user_id())
            ->get();
        $accessLevel    =   $this->userAccessLevel();
        return view('tmsw.client.index',compact('breadcrumb','user','accessLevel'));
    }

    public function truckDetails($reference_number){
        $breadcrumb =   "Truck Details";
        $order_id   =   Order::where('reference_number',$reference_number)->value('id');
        $truck_details  =   TruckDetail::where('tmsw_order_id',$order_id)
                                    ->join('tmsw_transporters','tmsw_truck_orders.tmsw_transporter_id','tmsw_transporters.id')
                                    ->get();
        $accessLevel    =   $this->userAccessLevel();
        return view('tmsw.client.truck_details',compact('breadcrumb','truck_details','accessLevel'));
    }

    public function orderNumber(){
        $i = 1;
        for($j=0; $j < $i; $j++){
            $result = '';
            $length = 7;
            for($x = 0; $x < $length; $x++) {
                $result .= mt_rand(0, 9);
            }

            $checkAvailable = Order::where('reference_number', $result);
            if( $checkAvailable->count() < 1 ){
                return $result;
                break;
            }

            $i++;
        }
    }

    public function manageWaybills(){

        $breadcrumb     =   "Create New Order";
        $accessLevel    =   $this->userAccessLevel();
        $transporters   =   tmswTransporter::get();
        return view('tmsw.client.upload_wabill',compact('transporters','accessLevel','breadcrumb'));
    }


    public function check_waybill_duplicate($waybill_number){
        $waybill    =   TruckDetail::where('waybillNumber',$waybill_number)->count();
        if($waybill > 0){
            return true;
        }
    }

    public function uploadWaybill(Request $request){
        $order  = Order::where('reference_number',$request->order_number)->get();
        if(count($order) < 1){
            return back()->with('message',"Order Number incorrect");
        }else{

            foreach($order as $orders){
                $order_id =   $orders->id;
            }

            $clean_plates = preg_replace('/\s+/', '', $request->truck_plate_number);

            $truck_order_details =  TruckDetail::where('plateNumber',$clean_plates)
                        ->where('tmsw_order_id',$order_id)
                        ->where('tmsw_transporter_id',$request->transport_company)
                        ->get();

            if(count($truck_order_details) < 1){
                return back()->with('message', 'Truck not attached to Order');
            }else{
                foreach($truck_order_details as $detail){
                    $truck_order_detail_id   = $detail->id;
                }

                //TODO check for null waybill file

                $waybillFile        =   $request->file('waybill_file');
                $waybillnumber      =   $request->waybill_number;

                $check_duplicate    =   $this->check_waybill_duplicate($request->waybill_number);
                if(!$check_duplicate){

                    $originalImage_waybillfile  = $waybillFile;
                    $thumbnailImage_waybillfile  = Image::make($originalImage_waybillfile);
                    $originalPath_waybillnumber    = public_path().'/tmsw/waybills/';
                    $thumbnailImage_waybillfile->save($originalPath_waybillnumber.$waybillnumber.$originalImage_waybillfile->getClientOriginalName());

                    $waybillupload  =   $originalImage_waybillfile->getClientOriginalName();

                    TruckDetail::where('id',$truck_order_detail_id)
                        ->update(
                            array(
                                'waybillFile'               => $waybillupload,
                                'waybillNumber'             => $waybillnumber,
                                'tmsw_status_id'            => 12,
                            )
                        );

                    $invoice    =   new Invoice;
                    $invoice->invoice_reference     =   $this->invoice_reference();
                    $invoice->tmsw_truck_order_id   =   $truck_order_detail_id;
                    $invoice->tmsw_status_id        =   14;
                    $invoice->save();

                    return back()->with('message','An Invoice for Payment has been Created');
                }else{
                    return back()->with('message','Duplicate Waybill Number');
                }
            }

        }

    }

    public function invoice_reference(){
        $i = 1;
        for($j=0; $j < $i; $j++){
            $result = '';
            $length = 7;
            for($x = 0; $x < $length; $x++) {
                $result .= mt_rand(0, 9);
            }

            $checkAvailable = Invoice::where('invoice_reference', $result);
            if( $checkAvailable->count() < 1 ){
                return $result;
                break;
            }

            $i++;
        }
    }

    /*@end order management*/


}
