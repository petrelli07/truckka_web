<?php

namespace App\Http\Controllers\TMSW\Client;

use App\TMSW\tmswTruckOrder as TruckOrder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\TMSW\tmswInvoice as Invoice;
use App\TMSW\tmswOrder as Order;
use Auth;

class FinanceController extends Controller
{

    public function userAccessLevel(){
        $role  =   Auth::user()->userAccessLevel;
        switch ($role) {
            case 12:
                return 'administrator';
                break;
            case 13:
                return 'manager';
                break;
            case 14:
                return 'desk_officer';
                break;
            case 15:
                return 'finance';
                break;
            case 16:
                return 'transporter';
                break;
            case 17:
                return 'supplier';
                break;
            default:
                return '/';
                break;
        }
    }

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function user_id(){
        $user_id    =   Auth::user()->id;
        return $user_id;
    }

    public function company_id(){
        $user_id    =   Auth::user()->id;
        $company_id =   CompanyUser::where('user_id',$user_id)->value('tmsw_company_detail_id');
        return $company_id;
    }

    public function allInvoices(){
        $breadcrumb =   "Invoices";

        $accessLevel = $this->userAccessLevel();
        $invoice    =   Order::join('tmsw_routes','tmsw_orders.tmsw_route_id','=',
            'tmsw_routes.id')
            ->join('tmsw_states as origin_state','tmsw_routes.origin_state_id','origin_state.id')
            ->join('tmsw_states as destination_state','tmsw_routes.destination_state_id','destination_state.id')
            ->join('tmsw_truck_types','tmsw_orders.tmsw_truck_type_id',
                '=','tmsw_truck_types.id')
            ->join('tmsw_statuses','tmsw_orders.tmsw_status_id',
                '=','tmsw_statuses.id')
            ->join('tmsw_truck_orders','tmsw_truck_orders.tmsw_order_id','tmsw_orders.id')
            ->join('tmsw_invoices','tmsw_invoices.tmsw_truck_order_id','tmsw_truck_orders.id')
            ->select('tmsw_orders.*','tmsw_truck_types.*','origin_state.state_description as originStateName','destination_state.state_description as destStateName','tmsw_statuses.*','tmsw_routes.*','tmsw_invoices.*','tmsw_truck_orders.*')
            ->where('tmsw_invoices.tmsw_status_id',14)
            ->get();

        return view('tmsw.client.invoices',compact('breadcrumb','invoice','accessLevel'));
    }

    public function truckOrderDetail($invoice_reference_number){
        $accessLevel = $this->userAccessLevel();
        $breadcrumb =   "Order Details";
        $details = Invoice::where('invoice_reference',$invoice_reference_number)
                        ->join('tmsw_truck_orders','tmsw_invoices.tmsw_truck_order_id','tmsw_truck_orders.id')
                        ->join('tmsw_orders','tmsw_truck_orders.tmsw_order_id','tmsw_orders.id')
                        ->join('tmsw_routes','tmsw_orders.tmsw_route_id','=',
                        'tmsw_routes.id')
                            ->join('tmsw_states as origin_state','tmsw_routes.origin_state_id','origin_state.id')
                            ->join('tmsw_states as destination_state','tmsw_routes.destination_state_id','destination_state.id')
                            ->join('tmsw_truck_types','tmsw_orders.tmsw_truck_type_id',
                                '=','tmsw_truck_types.id')
                            ->join('tmsw_statuses','tmsw_orders.tmsw_status_id',
                                '=','tmsw_statuses.id')
            ->select('tmsw_orders.*','tmsw_truck_types.*','origin_state.state_description as originStateName','destination_state.state_description as destStateName','tmsw_statuses.*','tmsw_routes.*','tmsw_invoices.*','tmsw_truck_orders.*')
                            ->get();
        return view('tmsw.client.view_details',compact('details','breadcrumb','accessLevel'));
    }

    public function confirmInvoice($invoice_reference_number){
        $details    =    Invoice::where('invoice_reference',$invoice_reference_number)
                            ->get();
        foreach($details as $detail){
            $status   =   $detail->tmsw_status_id;
            $truck_order_id   =   $detail->tmsw_truck_order_id;
        }
        if($status != 14){
            return back()->with('message','Invoice already Processed');
        }
        Invoice::where('invoice_reference',$invoice_reference_number)
            ->update(
                array(
                    'tmsw_status_id'            => 13,
                )
            );
        TruckOrder::where('id',$truck_order_id)
            ->update(
                array(
                    'tmsw_status_id'            => 13,
                )
            );
        return back()->with('message','Invoice Processed Successfully');
    }

    public function invoiceDetails($id){
        $invoice    = Invoice::where('tmsw_invoices.invoice_reference',$id)
            ->join('tmsw_truck_orders as orders','tmsw_invoices.tmsw_truck_order_id','orders.id')
            ->join('tmsw_transporters','orders.tmsw_transporter_id','tmsw_transporters.id')
            ->join('banks','tmsw_transporters.bank_id','banks.id')
            ->join('tmsw_orders','orders.tmsw_order_id','tmsw_orders.id')
            ->join('tmsw_routes','tmsw_orders.tmsw_route_id','=',
                'tmsw_routes.id')
            ->join('tmsw_states as origin_state','tmsw_routes.origin_state_id','origin_state.id')
            ->join('tmsw_states as destination_state','tmsw_routes.destination_state_id','destination_state.id')
            ->join('tmsw_truck_types','tmsw_orders.tmsw_truck_type_id',
                '=','tmsw_truck_types.id')
            ->join('tmsw_statuses','tmsw_invoices.tmsw_status_id',
                '=','tmsw_statuses.id')
            ->select('tmsw_orders.*','tmsw_truck_types.*','origin_state.state_description as originStateName','destination_state.state_description as destStateName','tmsw_statuses.*','tmsw_routes.*','orders.*','tmsw_invoices.*','tmsw_transporters.*','banks.*')
            ->get();
        $breadcrumb =   "Invoice Details";

        $accessLevel = $this->userAccessLevel();
        return view('tmsw.client.invoice_details',compact('breadcrumb','invoice','accessLevel'));
    }
}
