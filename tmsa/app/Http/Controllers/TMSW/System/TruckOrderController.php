<?php

namespace App\Http\Controllers\TMSW\System;

use App\TMSW\tmswTransporter;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\TMSW\tmswRoute as Route;
use App\TMSW\tmswState as State;
use App\TMSW\tmswTruckType as TruckType;
use App\TMSW\tmswOrder as Order;
use App\TMSW\tmswTruckOrder as TruckOrder;
use App\TMSW\tmswCompanyUser as CompanyUser;
use App\TMSW\tmswTruckOrder as TruckDetail;
use App\User;
use Auth;
use Notification;

class TruckOrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function orders(){
        $breadcrumb =   "Home";
        $user   =  Order::join('tmsw_routes','tmsw_orders.tmsw_route_id','=',
            'tmsw_routes.id')
            ->join('tmsw_states as origin_state','tmsw_routes.origin_state_id','origin_state.id')
            ->join('tmsw_states as destination_state','tmsw_routes.destination_state_id','destination_state.id')
            ->join('tmsw_truck_types','tmsw_orders.tmsw_truck_type_id',
                '=','tmsw_truck_types.id')
            ->join('tmsw_statuses','tmsw_orders.tmsw_status_id',
                '=','tmsw_statuses.id')
            ->select('tmsw_orders.*','tmsw_truck_types.*','origin_state.state_description as originStateName','destination_state.state_description as destStateName','tmsw_statuses.*','tmsw_routes.*')
            //->where('tmsw_orders.tmsw_status_id',9)
            ->get();
        return view('tmsw.system.index',compact('breadcrumb','user'));
    }

    public function truckDetails($reference_number){
        $reference  =   $reference_number;
        $order_id   =   Order::where('reference_number',$reference)->value('id');
        $trucks =   TruckDetail::where('tmsw_order_id',$order_id)
            ->join('tmsw_transporters as transporters','tmsw_truck_orders.tmsw_transporter_id','transporters.id')
            //->where('tmsw_truck_orders.tmsw_status_id',16)
            ->select('transporters.*','tmsw_truck_orders.*')
            ->get();
        $breadcrumb =   "Truck Details";
        return view('tmsw.system.responses',compact('breadcrumb','trucks','reference'));
    }

    public function addResponses($reference_number){

        $breadcrumb =   "Add Truck Details";
        $reference  =   $reference_number;
        $transporters   =   tmswTransporter::get();
        return view('tmsw.system.choose_response',compact('reference','transporters','breadcrumb'));
    }

    public function submitResponse(Request $request){

        $this->validate($request, [
            'truck_plate_number.*'=>'required',
            'driver_name.*'=>'required',
            'driver_phone.*'=>'required',
            'order_id.*'=>'required',
            'transport_company'=>'required',
        ]);

            $transporter_id =   $request->transport_company;

            $plate_number               =   $request->truck_plate_number;
            $driver_name                =   $request->driver_name;
            $driver_phone               =   $request->driver_phone;
            $reference_number           =    $request->order_id;

            $order_id   =   Order::where('reference_number',$reference_number)->value('id');

        for($x=0;$x<=count($plate_number);$x++){
            $truck_detail                                               =   new TruckOrder;
            $truck_detail->plateNumber                                  =   $plate_number[$x];
            $truck_detail->driverName                                   =   $driver_name[$x];
            $truck_detail->driverPhone                                  =   $driver_phone;
            $truck_detail->tmsw_order_id                                =   $order_id;
            $truck_detail->tmsw_transporter_id                          =   $transporter_id;
            $truck_detail->tmsw_status_id                               =   16;
            $truck_detail->save();
        }


        return redirect('/tmsw/superAdministrator/responses/'.$reference_number)->with('message','Truck Details Entered Successfully');
    }

    public function updateResponse(Request $request){

        $this->validate($request, [
            'response_id.*'=>'required',
            'reference_number'=>'required',
        ]);

        $transporters   =   $request->response_id;

        $reference_number   =   $request->reference_number;

        $order_id   =   Order::where('reference_number',$reference_number)->value('id');

        $transporter_id_count   =   count($transporters);

        for($x=0;$x<$transporter_id_count;$x++){
            TruckOrder::where('id',$transporters[$x])
                        ->where('tmsw_order_id',$order_id)
                        ->update(
                            array(
                                'tmsw_status_id'=>15
                            )
                        );
        }

        return redirect('/tmsw/superAdministrator/responses/'.$reference_number)->with('message','Successful');
    }
}
