<?php

namespace App\Http\Controllers\TMSW\Supplier;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\ManageClientSupplier;
use App\TMSW\tmswCompanyDetail;
use App\TMSW\tmswCompanyUser as UserComapany;
use App\TMSW\tmswClientSupplyOrder as ClientSupplyOrder ;
use App\ClientSupplyOrderQuote;
use App\TMSW\tmswSuppliersGroup as SuppliersGroup;
use App\TMSW\tmswSuppliersGroupList as SuppliersGroupList;
use App\TMSW\tmswSupplyRequest as SupplyRequest;
use App\TMSW\tmswClientSupplierDetail as ClientSupplierDetail;
use App;
use DB;
use Validator;
use Image;
use App\User;
use App\Notifications\newClientAccount;
use \App\Http\Controllers\AdminControllers\UserManagementController as Admin;
use \App\Http\Controllers\ClientControllers\UserManagementController as Client;

class UserController extends Controller{
	public $userId;
    public $companyID;

    public function userInit(){
      
      //TODO get userId from table

      $userID           =   Auth::user()->id;
      
      $companyID        =   UserComapany::where('user_id',$userID)
                                        ->value('tmsw_company_detail_id');
      
      $this->userId     =   $userID;
      $this->companyID  =   $companyID;

    }

    public function activate(array $where,string $table){
    	DB::table($table)->where($where)->update(['status'=> '1']);
    }

    public function deactivate(array $where,string $table){
      DB::table($table)->where($where)->update(['status'=> '2']);
    }

    public function reject(array $where,string $table){
    	DB::table($table)->where($where)->update(['status'=> '3']);
    }

    public function approve(array $where,string $table){
    	DB::table($table)->where($where)->update(['status'=> '4']);
    }

    public function getIndex(){
        
        $breadcrumb = 'Supplier dashboard';

        return view('tmsw.supplier.index')
        			->withBreadcrumb($breadcrumb);
    }



	public function uploadQuote(Request $request,$quoteId){
		
		$this->userInit();

		$validator = Validator::make($request->all(), [
                'file' => 'required'
            		]);

		if ($validator->passes()) {

			$purchaseOrderFilePath            =  $request->file('file');
            $originalImage_purchaseOrderFile  = $purchaseOrderFilePath;
            $thumbnailImage_purchaseOrderFile  = Image::make($originalImage_purchaseOrderFile);
            $originalPath_purchase_order    = public_path().'/tmsw/purchaseOrderQuote/';
            $thumbnailImage_purchaseOrderFile->save($originalPath_purchase_order.$originalImage_purchaseOrderFile->getClientOriginalName());

            $quote_filename          =   $originalImage_purchaseOrderFile->getClientOriginalName();

			$data 				=	array('supply_quotes'                    =>	$quote_filename,
														'tmsw_supply_requests.status'			 =>	'5'
													);

			DB::table('tmsw_supply_requests')->where('client_supply_order_id',$quoteId)
										->where('tmsw_client_supplier_details.user_id',$this->userId)
										->join('tmsw_client_supplier_details','tmsw_supply_requests.client_id','=','tmsw_client_supplier_details.company_id')
										->update($data);
      return back();

		}

	}

  public function allSuppliersRequestsView(){

    $this->userInit();

    $breadcrumb   = 'my orders';

    $tableTitle   = 'My Orders';

    $orders      =  SupplyRequest::join('tmsw_client_supplier_details','tmsw_client_supplier_details.id','=','tmsw_supply_requests.supplier_id')
                                      ->join('tmsw_client_supply_orders','tmsw_client_supply_orders.id','=','tmsw_supply_requests.client_supply_order_id')
                                      ->where('tmsw_client_supplier_details.user_id',$this->userId)
                                      ->select('*','tmsw_client_supply_orders.id as supply_order_id','tmsw_client_supply_orders.created_at as order_create_date','tmsw_client_supply_orders.expires_at as order_expire_date','tmsw_supply_requests.status as request_status','tmsw_client_supply_orders.status as order_status')
                                      ->get();
    $data        =  array('orders'      =>  $orders,
                              'tableTitle'  =>  $tableTitle  
                            );

     return view('tmsw.supplier.supplier-view-supply-request',$data)
              ->withBreadcrumb($breadcrumb);

  }
}

