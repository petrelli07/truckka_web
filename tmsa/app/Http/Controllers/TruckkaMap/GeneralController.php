<?php

namespace App\Http\Controllers\TruckkaMap;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GeneralController extends Controller
{
    public function index(){
    	
        $breadcrumb = 'map';
        return view('TruckkaMap.home.index')
        ->withBreadcrumb($breadcrumb);

    }
}
