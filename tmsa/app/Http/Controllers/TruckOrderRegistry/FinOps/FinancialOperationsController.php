<?php

namespace App\Http\Controllers\TruckOrderRegistry\FinOps;

use App\TruckRegistry\TruckOwner;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\TruckRegistry\TruckOrderRegistry;
use App\TruckRegistry\Truck;
use App\TruckRegistry\RegistryPayment;
use App\TruckRegistry\TruckOrderRegistryAudit;
use App\Bank;
use App\User;
use Auth;
use App\Notifications\TruckOrderRegistry\NewPartPaymentApproval;
use App\Notifications\TruckOrderRegistry\NewBalancePaymentApproval;

class FinancialOperationsController extends Controller
{

///////////////// process statuses
    public $partPaymentRequestStatus            = 1;

    public $partPaymentOpApprovedStatus         = 2;

    public $partPaymentFinApprovedtatus         = 3;

    public $balancePaymentRequestStatus         = 4;

    public $balancePaymentOpApprovedStatus      = 5;

    public $balancePaymentFinApprovedStatus     = 6;

    public $paymentComplete                     = 7;

    public $partPaymentDeclinedOps                          = 8;
    public $partPaymentDeclinedFin                          = 9;
    public $balancePaymentDeclinedOps                       = 10;
    public $balancePaymentDeclinedFin                       = 11;

// process statuses



// payment statuses

    public $confirmed       =   1;
    public $unconfirmed     =   2;
//  end payment statuses

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function audit($message){
        $audit  =   new TruckOrderRegistryAudit;
        $audit->user_id     =   Auth::user()->id;
        $audit->description =   $message;
        $audit->save();
    }
    //send sms
    public function sendSms($phoneNumber,$message){
        $encoded_message    =   urlencode($message);
        $result = array();
        $url = 'https://www.bulksmsnigeria.com/api/v1/sms/create?api_token=Lk6hs0SzNhQJfVFOisboSBcq2VSX0CJehOdtC5diccvPyzUlUE7qE2a0Co82&from=Truckka&to='.$phoneNumber.'&body='.$encoded_message.'&dnd=2';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $request = curl_exec($ch);

        curl_close($ch);
        if ($request) {
            $result = json_decode($request, true);
        }
    }

//landing page view for finOps
    public function index() {

        $auditmessage    =   "logged in";
        $this->audit($auditmessage);

        $results        =      TruckOrderRegistry::join('registry_statuses','registry_statuses.id','=',
                                                    'truck_order_registries.registry_status_id')
                                                    ->where('truck_order_registries.registry_status_id',
                                                        $this->partPaymentOpApprovedStatus)
                                                    ->orWhere('truck_order_registries.registry_status_id',
                                                        $this->balancePaymentOpApprovedStatus)
                                                    ->get();

        $breadcrumb ='Finance Operations';
        return view('registry.finops.index',compact('breadcrumb','results'));
    }

//get Management approved Part payment requests
    private function getManagentApprovedPartPaymentRequest(){
        $tds = TruckOrderRegistry::join('registry_statuses','registry_statuses.id','=',
                                    'truck_order_registries.registry_status_id')
                                    ->where('truck_order_registries.registry_status_id',$this->partPaymentOpApprovedStatus)
                                    ->get();
        return $tds;
    }
    
//view Management approved Part payment requests
    public function viewManagementApprovedPartPaymentRequest() {
        $breadcrumb ='All Approved Part Payment';
        $results = $this->getManagentApprovedPartPaymentRequest();
        return view('registry.finops.view-approve-part-payment',compact('breadcrumb','results'));
    }

    //get waybillfile for origin
    public function getWaybillFile($registry_number){
        $get_waybill_file   =   TruckOrderRegistry::join('trucks','trucks.id','=',
                                'truck_order_registries.truck_id')
                                ->join('users','users.id','=','truck_order_registries.agent_id')
                                ->where('registry_number',$registry_number)->get();
        foreach($get_waybill_file as $file){
            $waybillFile    =   $file->waybill_file_origin;
            $truck_id       =   $file->truck_id;
        }
        $data   =   ['filename'=>$waybillFile,'details'=>$get_waybill_file,'truck_id'=>$truck_id];
        return $data;
    }

    //get waybillfile for destination
    public function getWaybillFileDestination($registry_number){
        $get_waybill_file   =   TruckOrderRegistry::join('trucks','trucks.id','=',
                                'truck_order_registries.truck_id')
                                ->join('users','users.id','=','truck_order_registries.agent_id')
                                ->where('registry_number',$registry_number)->get();
        foreach($get_waybill_file as $file){
            $waybillFile    =   $file->waybill_file_destination;
            $truck_id       =   $file->truck_id;
        }
        $data   =   ['filename'=>$waybillFile,'details'=>$get_waybill_file,'truck_id'=>$truck_id];
        return $data;
    }

    //view more part payment
    public function viewMorePartPayment($registry_number){
        $data       =   $this->getWaybillFile($registry_number);

        //transporter details
        $transporter_details    =   Truck::join('truck_owners','truck_owners.id','=','trucks.id')
            ->where('trucks.id',$data['truck_id'])->get();

        foreach($transporter_details as $transporter){
            $account_number =   $transporter->account_number;
        }
        //end transporter details

        //sanitize string
        //$sanitized_filename     =   trim(explode('/', $data['filename'])[2]);
        //end sanitizer

        /*$file_path  =   storage_path().'\\app\\public\\registry_waybillFiles\\'.$sanitized_filename;
        $ext = pathinfo(storage_path().$file_path, PATHINFO_EXTENSION);*/
        $details    =   $data['details'];
        $breadcrumb =   'Approve Part Payment';
        return view('registry.finops.view_more_part_payment',compact('breadcrumb','details','account_number'));
    }

    //view more balance payment
    public function viewMoreBalancePayment($registry_number){
        $data       =   $this->getWaybillFileDestination($registry_number);

        //transporter details
        $transporter_details    =   Truck::join('truck_owners','truck_owners.id','=','trucks.id')
            ->where('trucks.id',$data['truck_id'])->get();

        foreach($transporter_details as $transporter){
            $account_number =   $transporter->account_number;
        }
        //end transporter details

        if($data['filename'] == NULL){
            $sanitized_filename = NULL;
            $details    =   $data['details'];
        }else{
            /////sanitize string
            //$sanitized_filename     =   trim(explode('/', $data['filename'])[2]);
            /////end sanitizer/*
            /*$file_path  =   storage_path().'\\app\\public\\registry_waybillFiles\\'.$sanitized_filename;
            $ext = pathinfo(storage_path().$file_path, PATHINFO_EXTENSION);*/
            $sanitized_filename =   $data['filename'];
            $details    =   $data['details'];
        }

        $breadcrumb =   'Approve Balance Payment';
        return view('registry.finops.view_more_balance_payment',compact('breadcrumb','sanitized_filename','details','account_number'));
    }

    // process part payment request
    public function finOpsPartPaymentRequest(Request $request){

        $registry_dets = TruckOrderRegistry::where('registry_number',$request->registry_number)->get();

        $registry_number    =   $request->registry_number;

        foreach($registry_dets as $reg){
            $registry_id    =   $reg->id;
            $truck_id    =   $reg->truck_id;
            $waybill_number =   $reg->waybill_number;
        }

        $truck    =   Truck::findOrFail($truck_id);
        $truck_owner_id    =   $truck->truck_owner_id;
        $truck_owner        =   TruckOwner::findOrFail($truck_owner_id);
        $recipient_code     =   $truck_owner->recipient_code;

        $amount_to_pay          =   $request->amount / 2;
        $full_amount_to_pay     =   $request->amount;
        $agent_id               =   $request->agent_id;

        $amount_converted_to_kobo    =   $amount_to_pay * 100;

        //initiate transfer via paystack api
        $result = array();

        //Set other parameters as keys in the $postdata array
        $postdata = array(
            'source' => 'balance',
            'reason' => 'Part Payment for loading',
            'amount' => $amount_converted_to_kobo,
            "recipient" => $recipient_code,
        );

        $url = "https://api.paystack.co/transfer";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postdata));  //Post Fields
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $headers = [
            //'Authorization: Bearer sk_live_c491d1a0c5b7c8273dad7798c58f727085ec835f',
            'Authorization: Bearer sk_test_28a937c41ad4dda08b5bb33c71fba1b2d5357b3d',
            'Content-Type: application/json'
        ];
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $request = curl_exec($ch);

        curl_close($ch);

        if ($request) {
            $result = json_decode($request, true);
            $transfer_code  = $result['data']['transfer_code'];
        }

        if($result['status'] == true){

            $details    =   [
                        'registry_id'               =>  $registry_id,
                        'agent_id'                  =>  $agent_id,
                        'full_amount_to_pay'        =>  $full_amount_to_pay,
                        'amount_to_pay'             =>  $amount_to_pay,
                        'transfer_code'             =>  $transfer_code
            ];

            $breadcrumb =   'Enter OTP';

            return view('registry.finops.otp_part_payment',compact('details','breadcrumb'));

        }else{
            return redirect('registry/approve-part-payment-finops')->withInput()->with('response','Transaction Failed');
        }

    }

    public function finalizePartPayment(Request $request){
            // validate form fields
                    $this->validate($request, [
                        'otp'=>'required'
                    ]);

            $registry_id            =   $request->registry_id;
            $agent_id               =   $request->agent_id;
            $amount_to_pay          =   $request->amount_to_pay;
            $full_amount_to_pay     =   $request->full_amount_to_pay;
            $transfer_code          =   $request->transfer_code;
            $otp                    =   $request->otp;
            //complete transfer via paystack api
            $result = array();
            //Set other parameters as keys in the $postdata array
            $postdata = array(
                'transfer_code' => $transfer_code,
                'otp'           => $otp,
            );
            $url = "https://api.paystack.co/transfer/finalize_transfer";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postdata));  //Post Fields
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $headers = [
                //'Authorization: Bearer sk_live_c491d1a0c5b7c8273dad7798c58f727085ec835f',
                'Authorization: Bearer sk_test_28a937c41ad4dda08b5bb33c71fba1b2d5357b3d',
                'Content-Type: application/json'
            ];
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            $request = curl_exec($ch);
            curl_close($ch);
            if ($request) {
                $result = json_decode($request, true);
            }
            if($result['status'] == true){
                $registry_number    =   TruckOrderRegistry::where('id',$registry_id)->value('registry_number');
                TruckOrderRegistry::where('id',$registry_id)
                    ->update(
                        array(
                            'registry_status_id' => $this->partPaymentFinApprovedtatus,
                        )
                    );
                //inserting to register payment table
                $post = new RegistryPayment;
                $post->truck_order_registry_id          = $registry_id;
                $post->registry_payment_status_id       = $this->confirmed;
                $post->user_id                          = Auth::user()->id;
                $post->agent_id                         = $agent_id;
                $post->total_amount                     = $full_amount_to_pay;
                $post->part_payment_amount              = $amount_to_pay;
                $post->balance_payment_amount           = null;
                $post->save();
                //get agent phone number
                $agent_details  =   User::findOrFail($agent_id);
                //send ops message
                $message        =   'Part Payment for order '.$registry_number.' has been made by finance';
                $agentPhoneNumber =   $agent_details->phone;
                //call send sms method
                $this->sendSms($agentPhoneNumber,$message);
                $auditmessage    =   "Part Payment for ".$registry_number." completed";
                $this->audit($auditmessage);

                $registry   =   TruckOrderRegistry::where('registry_number',$registry_number)->get();
                foreach($registry as $reg){
                    $waybill_number   =   $reg->waybill_number;
                }
                $user_id    =   Auth::user()->id;

                $user = User::find($user_id);
                $notification = $user->notify(new NewPartPaymentApproval($waybill_number));

                return redirect('/registry/approve-part-payment-finops')->withInput()->with('response','Transfer successful');
            }else{
                return back()->withInput()->with('response','An error occurred');
            }
    }

/////////////////////// get operations approved balance payments
    private function getManagementApprovedBalancePayment(){
        $tds =  TruckOrderRegistry::join('registry_statuses','registry_statuses.id','=',
                                        'truck_order_registries.registry_status_id')
                                        ->where('truck_order_registries.registry_status_id',$this->balancePaymentOpApprovedStatus)
                                        ->get();
        return $tds;
    }

    public function viewManagementApprovedBalancePaymentRequest() {
        $breadcrumb ='All Approve Balance Payment';
        $results = $this->getManagementApprovedBalancePayment();
        return view('registry.finops.view-approve-balance-payment',compact('breadcrumb','results'));
    }

    public function getRegistryPaymentID($id) {
        $parent = RegistryPayment::where('truck_order_registry_id',$id)->get();
        foreach($parent as $row) {
        return $row['id'];
      }
    }

    public function getRegistryPaymentAmount($id) {
        $parent = RegistryPayment::select('total_amount')->where('truck_order_registry_id',$id)->get();
        foreach($parent as $row) {
        return $row['total_amount'];
      }
    }


    public function finOpsBalancePaymentRequest(Request $request){
        $registry_dets = TruckOrderRegistry::where('registry_number',$request->registry_number)->get();
        $registry_number    =   $request->registry_number;
        foreach($registry_dets as $reg){
            $registry_id    =   $reg->id;
            $truck_id       =   $reg->truck_id;
        }
        $truck              =   Truck::findOrFail($truck_id);
        $truck_owner_id     =   $truck->truck_owner_id;
        $truck_owner        =   TruckOwner::findOrFail($truck_owner_id);
        $recipient_code     =   $truck_owner->recipient_code;

            //checking if half payment has been made
                $checking_for_half_payment = RegistryPayment::where('truck_order_registry_id', $registry_id)
                                                            ->where('part_payment_amount',$request->amount / 2)
                                                            ->get();
                       //updating the balance payment//
                    if(!empty($checking_for_half_payment)):
                        foreach ($checking_for_half_payment as $payment_id) {
                             $id                        =   $payment_id->id;
                             $part_payment_amount       =   $payment_id->part_payment_amount;
                             $agent_id                  =   $payment_id->agent_id;
                         }
                        $amount_converted_to_kobo   =   $part_payment_amount * 100;
                        //make balance payment transfer via the paystack api
                        $result = array();
                        //Set other parameters as keys in the $postdata array
                        $postdata = array(
                            'source' => 'balance',
                            'reason' => 'Balance Payment for loading',
                            'amount' => $amount_converted_to_kobo,
                            "recipient" => $recipient_code,
                        );
                        $url = "https://api.paystack.co/transfer";
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL, $url);
                        curl_setopt($ch, CURLOPT_POST, 1);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postdata));  //Post Fields
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        $headers = [
                            //'Authorization: Bearer sk_live_c491d1a0c5b7c8273dad7798c58f727085ec835f',
                            'Authorization: Bearer sk_test_28a937c41ad4dda08b5bb33c71fba1b2d5357b3d',
                            'Content-Type: application/json'
                        ];
                        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                        $request = curl_exec($ch);
                        curl_close($ch);
                        //end balance payment via api
                        if ($request) {
                            $result = json_decode($request, true);
                            $transfer_code  = $result['data']['transfer_code'];
                        }
                        //check if payment is successful
                    if($result['status'] == true){
                        $details    =   [
                            'id'                        =>  $id,
                            'registry_id'               =>  $registry_id,
                            'agent_id'                  =>  $agent_id,
                            'transfer_code'             =>  $transfer_code,
                            'registry_number'           =>  $registry_number,
                            'amount'                    =>  $part_payment_amount
                        ];
                        $breadcrumb =   'Enter OTP';
                        return view('registry.finops.otp_balance_payment',compact('details','breadcrumb'));
                    }else{
                        return redirect('registry/approve-part-payment-finops')->withInput()->with('response','Transaction Failed');
                    }
                    else:
                        return back()->withInput()->with('response','Invalid Payment');   
                    endif;
    }

    //finalize balance payment
    public function finalizeBalancePayment(Request $request){
        // validate form fields
        $this->validate($request, [
            'otp'=>'required'
        ]);
        $registry_number                            =   $request->registry_number;
        $agent_id                                   =   $request->agent_id;
        $id                                         =   $request->id;
        $registry_id                                =   $request->registry_id;
        $transfer_code                              =   $request->transfer_code;
        $part_payment_amount                        =   $request->amount;
        $otp                                        =   $request->otp;
        //complete transfer via paystack api
        $result = array();
        //Set other parameters as keys in the $postdata array
        $postdata = array(
            'transfer_code' => $transfer_code,
            'otp'           => $otp,
        );
        $url = "https://api.paystack.co/transfer/finalize_transfer";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postdata));  //Post Fields
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $headers = [
            //'Authorization: Bearer sk_live_c491d1a0c5b7c8273dad7798c58f727085ec835f',
            'Authorization: Bearer sk_test_28a937c41ad4dda08b5bb33c71fba1b2d5357b3d',
            'Content-Type: application/json'
        ];
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $request = curl_exec($ch);

        curl_close($ch);

        if ($request) {
            $result = json_decode($request, true);
        }

        RegistryPayment::where('id',$id)
            ->update(
                array(
                    'registry_payment_status_id' => $this->confirmed,
                    'balance_payment_amount'     => $part_payment_amount,
                )
            );
        //get agent phone number
        $agent_details  =   User::findOrFail($agent_id);
        //send ops message
        $message        =   'Balance Payment for order '.$registry_number.' has been made by finance';
        $agentPhoneNumber =   $agent_details->phone;
        //call send sms method
        $this->sendSms($agentPhoneNumber,$message);
        //Updating fields in truck Order registry//
        if($registry_id):
            $id     = $registry_id;
            $post   = TruckOrderRegistry::find($id);
            $post->registry_status_id = $this->paymentComplete;
            $post->save();
        endif;
        $auditmessage    =   "Balance Payment for ".$registry_number." completed";
        $this->audit($auditmessage);

        $registry   =   TruckOrderRegistry::where('registry_number',$registry_number)->get();
        foreach($registry as $reg){
            $waybill_number   =   $reg->waybill_number;
        }
        $user_id    =   Auth::user()->id;

        $user = User::find($user_id);
        $notification = $user->notify(new NewPartPaymentApproval($waybill_number));

        return redirect('registry/approve-balance-payment-finops')->withInput()->with('response','Payment Complete');

    }

    function genReference($qtd){
//Under the string $Caracteres you write all the characters you want to be used to randomly generate the code.
        $Caracteres = 'ABCDEFGHIJKLMOPQRSTUVXWYZ0123456789';
        $QuantidadeCaracteres = strlen($Caracteres);
        $QuantidadeCaracteres--;

        $Hash=NULL;

        for($x=1;$x<=$qtd;$x++){
            $Posicao = rand(0,$QuantidadeCaracteres);
            $Hash .= substr($Caracteres,$Posicao,1);
        }

        return $Hash;
    }


    public function change_password_view(){
        $breadcrumb     =   'Change Your Password';
        return view('registry.finops.change_password',compact('breadcrumb'));
    }

    public function getBanks(){
        $result = array();
        //$url = "https://api.paystack.co/bank/?perPage=100";
        $url = "https://api.paystack.co/bank/?perPage=100";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $headers = [
            //'Authorization: sk_live_c491d1a0c5b7c8273dad7798c58f727085ec835f',
            'Authorization: sk_test_28a937c41ad4dda08b5bb33c71fba1b2d5357b3d',
            'Content-Type: application/json',
        ];
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $request = curl_exec($ch);
        curl_close($ch);
        if ($request) {
            $result = json_decode($request, true);
            //return $result['data'];
            $count  =   count($result['data']);
            $res    =   [];
            for($i=0; $i < $count; $i++){
                $bank   =   new Bank;
                $bank->bank_name    =   $result['data'][$i]['name'];
                $bank->bank_code    =   $result['data'][$i]['code'];
                $bank->currency     =   $result['data'][$i]['currency'];
                $bank->type         =   $result['data'][$i]['type'];
                $bank->save();
            }
            return "Done";
            //header('Location: ' . $result['data']['authorization_url']);
        }
    }

    public function bulkPaymentPartPayment(Request $request){
        $registry_number            =   $request->id;
        $amount_array           =   [];
        $truck_owner_id_array         =   [];
        $registry_id            =     [];
        $agent_id               =     [];
        $registry_payment_id    =     [];

        for($i=0;$i<count($registry_number);$i++){
             $truck_id              =   TruckOrderRegistry::where('registry_number',$registry_number[$i])->value('truck_id');
             $registry_id[]         =   TruckOrderRegistry::where('registry_number',$registry_number[$i])->value('id');
             $agent_id[]            =   TruckOrderRegistry::where('registry_number',$registry_number[$i])->value('agent_id');
             $truckowner            =   Truck::find($truck_id)->TruckOwner;
             $truck_owner_id_array[]      =   $truckowner->id;
            if(count(array_unique($truck_owner_id_array)) == 1){
                $amount_array[]     =   TruckOrderRegistry::where('registry_number',$registry_number[$i])->value('amount');
            }else{
                return back()->withInput()->with('response','Trucks Do not belong to same owner!');
            }
        }

        $recipient_code        =   $truckowner->recipient_code;


        $sum_of_array  =   array_sum($amount_array);
        $part_payment_amount    =   $sum_of_array * 0.5;
        $amount_converted_to_kobo   =   $part_payment_amount * 100;


        //initiate transfer via paystack api
        $result = array();

        //Set other parameters as keys in the $postdata array
        $postdata = array(
            'source' => 'balance',
            'reason' => 'Part Payment for loading',
            'amount' => $amount_converted_to_kobo,
            "recipient" => $recipient_code,
        );

        $url = "https://api.paystack.co/transfer";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postdata));  //Post Fields
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $headers = [
            //'Authorization: Bearer sk_live_c491d1a0c5b7c8273dad7798c58f727085ec835f',
            'Authorization: Bearer sk_test_28a937c41ad4dda08b5bb33c71fba1b2d5357b3d',
            'Content-Type: application/json'
        ];
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $request = curl_exec($ch);

        curl_close($ch);

        if ($request) {
            $result = json_decode($request, true);
            $transfer_code  = $result['data']['transfer_code'];
        }

        if($result['status'] == true){

            $details    =   [
                'full_amount_to_pay'        =>  $sum_of_array,
                'amount_to_pay'             =>  $part_payment_amount,
                'transfer_code'             =>  $transfer_code
            ];

            $breadcrumb =   'Enter OTP';

            return view('registry.finops.otp_part_bulk_payment',compact('details','agent_id','registry_id','breadcrumb'));

        }else{
            return redirect('registry/approve-part-payment-finops')->withInput()->with('response','Transaction Failed');
        }


    }

     public function finalize_part_bulk_payment(Request $request){
         // validate form fields
         $this->validate($request, [
             'otp'=>'required'
         ]);

         $registry_id            =   $request->registry_id;
         $agent_id               =   $request->agent_id;
         $amount_to_pay          =   $request->amount_to_pay;
         $full_amount_to_pay     =   $request->full_amount_to_pay;
         $transfer_code          =   $request->transfer_code;
         $otp                    =   $request->otp;

         //complete transfer via paystack api
         $result = array();
         //Set other parameters as keys in the $postdata array
         $postdata = array(
             'transfer_code' => $transfer_code,
             'otp'           => $otp,
         );

         $url = "https://api.paystack.co/transfer/finalize_transfer";
         $ch = curl_init();
         curl_setopt($ch, CURLOPT_URL, $url);
         curl_setopt($ch, CURLOPT_POST, 1);
         curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postdata));  //Post Fields
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
         $headers = [
             //'Authorization: Bearer sk_live_c491d1a0c5b7c8273dad7798c58f727085ec835f',
             'Authorization: Bearer sk_test_28a937c41ad4dda08b5bb33c71fba1b2d5357b3d',
             'Content-Type: application/json'
         ];
         curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
         $request = curl_exec($ch);
         curl_close($ch);
         if ($request) {
             $result = json_decode($request, true);
         }
         if($result['status'] == true){
             for($x=0;$x<count($agent_id);$x++){

             $registry_number    =   TruckOrderRegistry::where('id',$registry_id[$x])->value('registry_number');
                 TruckOrderRegistry::where('id',$registry_id[$x])
                     ->update(
                         array(
                             'registry_status_id' => $this->partPaymentFinApprovedtatus,
                         )
                     );
                 //inserting to register payment table
                 $post = new RegistryPayment;
                 $post->truck_order_registry_id          = $registry_id[$x];
                 $post->registry_payment_status_id       = $this->confirmed;
                 $post->user_id                          = Auth::user()->id;
                 $post->agent_id                         = $agent_id[$x];
                 $post->total_amount                     = $full_amount_to_pay/2;
                 $post->part_payment_amount              = $amount_to_pay/2;
                 $post->balance_payment_amount           = null;
                 $post->save();
                 //get agent phone number
                 $agent_details  =   User::findOrFail($agent_id[$x]);
                 //send ops message
                 $message        =   'Part Payment for order '.$registry_number[$x].' has been made by finance';
                 $agentPhoneNumber =   $agent_details->phone;
                 //call send sms method
                 $this->sendSms($agentPhoneNumber,$message);
                 $auditmessage    =   "Part Payment for ".$registry_number[$x]." completed";
                 $this->audit($auditmessage);

             }

             return redirect('/registry/approve-part-payment-finops')->withInput()->with('response','Transfer successful');
         }else{
             return redirect('registry/finops-home1')->with('response','An error occurred');
         }
     }

    public function bulkPaymentBalancePayment(Request $request){

        $registry_number                =   $request->id;
        $amount_array                   =   [];
        $truck_owner_id_array           =   [];
        $registry_id                    =     [];
        $agent_id                       =     [];
        $registry_payment_id            =       [];
        for($i=0;$i<count($registry_number);$i++){
            $truck_id              =   TruckOrderRegistry::where('registry_number',$registry_number[$i])->value('truck_id');
            $registry_id[]         =   TruckOrderRegistry::where('registry_number',$registry_number[$i])->value('id');
            $agent_id[]            =   TruckOrderRegistry::where('registry_number',$registry_number[$i])->value('agent_id');
            $registry_payment_id[]    =   RegistryPayment::where('truck_order_registry_id',$registry_id[$i])->value('id');
            $truckowner            =   Truck::find($truck_id)->TruckOwner;
            $truck_owner_id_array[]      =   $truckowner->id;
            if(count(array_unique($truck_owner_id_array)) == 1){
                $amount_array[]     =   TruckOrderRegistry::where('registry_number',$registry_number[$i])->value('amount');
            }else{
                return back()->withInput()->with('response','Trucks Do not belong to same owner!');
            }
        }

        $recipient_code        =   $truckowner->recipient_code;

        $sum_of_array  =   array_sum($amount_array);
        $part_payment_amount    =   $sum_of_array * 0.5;
        $amount_converted_to_kobo   =   $part_payment_amount * 100;


        //updating the balance payment//

            //make balance payment transfer via the paystack api
            $result = array();
            //Set other parameters as keys in the $postdata array
            $postdata = array(
                'source' => 'balance',
                'reason' => 'Balance Payment for loading',
                'amount' => $amount_converted_to_kobo,
                "recipient" => $recipient_code,
            );
            $url = "https://api.paystack.co/transfer";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postdata));  //Post Fields
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $headers = [
                //'Authorization: Bearer sk_live_c491d1a0c5b7c8273dad7798c58f727085ec835f',
                'Authorization: Bearer sk_test_28a937c41ad4dda08b5bb33c71fba1b2d5357b3d',
                'Content-Type: application/json'
            ];
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            $request = curl_exec($ch);
            curl_close($ch);
            //end balance payment via api
            if ($request) {
                $result = json_decode($request, true);
                $transfer_code  = $result['data']['transfer_code'];
            }
            //check if payment is successful
            if($result['status'] == true){
                $details    =   [
                    'transfer_code'             =>  $transfer_code,
                    'amount'                    =>  $part_payment_amount
                ];
                $breadcrumb =   'Enter OTP';
                return view('registry.finops.otp_balance_bulk_payment',compact('details','registry_id','registry_number','agent_id','registry_payment_id','breadcrumb'));
            }else{
                return redirect('registry/approve-part-payment-finops')->withInput()->with('response','Transaction Failed');
            }
    }

    public function finalize_bulk_balance_payment(Request $request){

        // validate form fields
        $this->validate($request, [
            'otp'=>'required'
        ]);

        $registry_number                            =   $request->registry_number;
        $agent_id                                   =   $request->agent_id;
        $registry_payment_id                        =   $request->registry_payment_id;
        $registry_id                                =   $request->registry_id;
        $transfer_code                              =   $request->transfer_code;
        $part_payment_amount                        =   $request->amount;
        $otp                                        =   $request->otp;
        //complete transfer via paystack api
        $result = array();
        //Set other parameters as keys in the $postdata array
        $postdata = array(
            'transfer_code' => $transfer_code,
            'otp'           => $otp,
        );
        $url = "https://api.paystack.co/transfer/finalize_transfer";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postdata));  //Post Fields
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $headers = [
            'Authorization: Bearer sk_test_28a937c41ad4dda08b5bb33c71fba1b2d5357b3d',
            //'Authorization: Bearer sk_live_c491d1a0c5b7c8273dad7798c58f727085ec835f',
            'Content-Type: application/json'
        ];
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $request = curl_exec($ch);

        curl_close($ch);

        if ($request) {

            $result = json_decode($request, true);
        }
            if($result['status'] == true){
            for($x=0;$x<count($registry_payment_id);$x++){

                RegistryPayment::where('id',$registry_payment_id[$x])
                    ->update(
                        array(
                            'registry_payment_status_id' => $this->confirmed,
                            'balance_payment_amount'     => $part_payment_amount/2,
                        )
                    );
                //get agent phone number
                $agent_details  =   User::findOrFail($agent_id[$x]);
                //send ops message
                $message        =   'Balance Payment for order '.$registry_number[$x].' has been made by finance';
                $agentPhoneNumber =   $agent_details->phone;
                //call send sms method
                $this->sendSms($agentPhoneNumber,$message);
                //Updating fields in truck Order registry//
                if($registry_id):
                    $id     = $registry_id[$x];
                    $post   = TruckOrderRegistry::find($id);
                    $post->registry_status_id = $this->paymentComplete;
                    $post->save();
                endif;
                $auditmessage    =   "Balance Payment for ".$registry_number[$x]." completed";
                $this->audit($auditmessage);
            }
                return redirect('registry/approve-balance-payment-finops')->withInput()->with('response','Payment Complete');

        }else{
                return redirect('registry/finops-home1')->with('response','An error occurred');
            }

    }

    //processing page for view more
    public function viewMore($registry_number){
        $registry_detail   =   TruckOrderRegistry::where('registry_number',$registry_number)->get();

        foreach($registry_detail as $detail){
            $registry_number        =   $detail->registry_number;
            $registry_status_id     =   $detail->registry_status_id;
        }

        if($registry_status_id === $this->partPaymentRequestStatus){
            return redirect('/registry/ops/view_more_part_payment/'.$registry_number);
        }elseif($registry_status_id === $this->balancePaymentRequestStatus){
            return redirect('/registry/ops/view_more_balance/'.$registry_number);
        }

    }
}
