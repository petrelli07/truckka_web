<?php

namespace App\Http\Controllers\TruckOrderRegistry\Administrator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Auth;
use App\Notifications\TruckOrderRegistry\NewUser;

class UserManagementController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create_new_user_view()
    {

        $breadcrumb = 'Create user';

        return view('registry.administrator.create_new_registry_user',compact('breadcrumb'));
    }

    public function change_password_view(){
        $breadcrumb     =   'Change Your Password';
        return view('registry.management.change_password',compact('breadcrumb'));
    }

    public function create_new_user_process(Request $request)
    {
        ///////////// validate form fields ///////////////
        $this->validate($request, [
            'email'=>'required|unique:users,email',
            'name'=>'required',
            'userType'=>'required',
        ]);

        $email 		= $request->email;
        $name 		= $request->name;
        $userType 	= $request->userType;
        $phone  	= $request->phone;

        $user 	=	new User;

        $defaultPassword            =   $this->randomNumberForPassword();

        $user->name 				=	$name;
        $user->email 				=	$email;
        $user->userAccessLevel 		=	$userType;
        $user->password 			=	bcrypt($defaultPassword);
        $user->phone 			    =	$phone;
        $user->default_password 	=	0;
        $user->status 				=	1;

        $user->save();

        $user_id    =   $user->id;

        $user = User::find($user_id);
        $notification = $user->notify(new NewUser($defaultPassword));

            return back()->withMessage('User Created Successfully');


    }


    public function randomNumberForPassword(){

        $i = 1;

        for($j=0; $j < $i; $j++){
//            return random_int(1,10000);

            $result = '';

            $length = 7;

            for($x = 0; $x < $length; $x++) {
                $result .= mt_rand(0, 9);
            }

            $checkAvailable = User::where('password', bcrypt($result));

            if( $checkAvailable->count() < 1 ){

                return $result;

                break;
            }
            $i++;

        }
    }
}
