<?php

namespace App\Http\Controllers\AdminControllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\carrierResource;
use App\User;

class ReportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function viewAllResources(){
        $myResources = carrierResource::all();
        return view('admin.resources.allResources', ['myResources'=>$myResources]);
    }

    public function allUsers(){
        $resource = User::paginate(10);
        return view('admin.new_admin.users.view_all_users', ['resource'=>$resource]);
    }
}
