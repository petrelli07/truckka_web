<?php

namespace App\Http\Controllers\AdminControllers;

define( 'APPPATH',dirname(dirname(__FILE__)));

use Illuminate\Http\Request;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use \App\Http\Controllers\ClientControllers\UserManagementController as userManagement;
use Validator;



class modulesCompiler extends Controller{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
  
    public function __construct(){
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public static function has_permission(){
     // $sdf = new \ReflectionClass();
      // return debug_backtrace();

    }

    public function getDeployedModules($userType = 1){
      $getDeployedModules   = DB::table('modules')
                              ->where('userType','=',$userType)                          
                              ->get();
      return $getDeployedModules->toArray();
    }

    public function getCompanyManagers($companyId,$userType){
      switch ($userType) {
        case 1:
          $table = 'user_comapanies';
          break;
        case 2:
          $table =  'carrier_users';
          break;
      }
          $managers             =   DB::table('unique_users')
                                              ->join($table,$table.'.user_id','=','unique_users.user_id')
                                              ->where($table.'.company_id','=',$companyId)
                                              ->select('unique_users.user_id')
                                              ->get();
      return $managers;
    }

    public function deployModules(Request $request){
     
      $validator = Validator::make($request->all(), [
            'details'       =>  'required|unique:modules,moduleMethod',
            'userType'      =>  'required'
      ]);

      $sortedArray          =  array();

      for ($m=0; $m < sizeof($request->details); $m++) { 
        if (isset($request->details[$m]['method'])) {
          array_push($sortedArray, $request->details[$m]);
        }
      }
     if ($validator->passes()) {

      for ($i=0; $i < sizeof($sortedArray) ; $i++) {
        $moduleName       =   $request->modules[$i];
        $successfull      =   array();
        $updateUserArray  =   array();

        $moduleDetails    =   array(
                                    'moduleMethod'      =>  $sortedArray[$i]['method'],
                                    'moduleName'        =>  $sortedArray[$i]['name'],
                                    'moduleDescription' =>  $sortedArray[$i]['desc'],
                                    'byDefault'         =>  1,
                                    'userType'          =>  $request->userType
                                  );
        $deployModule     =   DB::table('modules')
                                ->insertGetId($moduleDetails);

          if (isset($request->deployTo)) {

            $companies          =   $request->deployTo;
            $updateComData      =   array();

            for ($k=0; $k < sizeof($companies); $k++) { 


              $data              =   array('module_id'       =>  $deployModule,
                                          'company_id'       =>  $companies[$k]
                                          );

              array_push($updateComData, $data);
              //Get Company Managers ///////////////////////////////
              $managers             =   $this->getCompanyManagers($companies[$k],$request->userType);

              foreach ($managers as $user) {
                $data               =     array('user_id'         =>  $user->user_id,
                                                'module_id'       =>  $deployModule
                                                );

                array_push($updateUserArray,$data);
              }

            }

              $updateCompanyModules =   DB::table('client_company_modules')
                                          ->insert($updateComData);
          } 

          if ($request->userType == 0) {

            $superAdmins  =   DB::table('unique_users')
                                ->where('type_id','=',2)
                                ->select('user_id')
                                ->get();

            foreach ($superAdmins as $user) {
              $data   = array('user_id'   => $user->user_id,
                              'module_id' => $deployModule
              );
                array_push($updateUserArray,$data);
            }
          }

          $updateUserModules    =   DB::table('user_permissions')
                                        ->insert($updateUserArray);
          if ($deployModule) {
            array_push($successfull, $request->modules[$i]);
          }
        }
          return redirect()->back();
      }
    }


    public function creatRoleView($userType){
      switch ($userType){
          case 'client':
            $Dir      = 'ClientControllers';
            $userType = 1;
            $table    = 'companies';
            break;
          case 'admin':
            $Dir      = 'AdminControllers';
            $userType = 0;
            break;
          case 'carrier':
            $Dir      = 'CarrierControllers';
            $userType = 2;
            break;
          default:
            $Dir = 'AdminControllers';
            $userType = 0;
            break;
      }

      $deployedModules  = $this->getDeployedModules($userType);

      $data             = array('allModules' => $deployedModules,
                                'userType'   => $userType
                                );

      return view('admin.adminCreateRole',$data);
    }

    public function saveNewRole(Request $request,$userType){
      
      $validator             =    Validator::make($request->all(),[
            'name'           =>   'required',
            ]);
      if ($validator->passes()) {
        
        DB::transaction(function() use($request,$userType){

          $roleName             = $request->name;
          $permissions          = $request->modules;
          $userId               = 1;
          $is_default           = 'true';
          $roleName             = $request->name;
          $permissions          = $request->modules;
          $userManagement       = new UserManagement();
          $newRoleId            = $userManagement->createNewRole($userId, $roleName, $userType, $is_default);
          $saveRolePermission   = $userManagement->saveRolePermission($newRoleId, $permissions);
          
        },2);
      }

    }

    public function undeployModules(Request $request){
      $validator = Validator::make($request->all(), [
            'modules'       =>  'required',
            'userType'      =>  'required'
      ]);

     if ($validator->passes()) {
      for ($i=0; $i < sizeof($request->modules) ; $i++) {
        $moduleName       =   $request->modules[$i];
        $successfull      =   array(); 
        $deleteModule     =   DB::table('modules')
                              ->where('id', '=', $request->modules[$i])
                              ->where('userType','=',$request->userType)
                              ->delete();
      }
          return redirect()->back();
     }
    }


    public function viewAllModules(){
        $currentUrl     =   explode('/', URL::current());
       
        switch (end($currentUrl)){
          case 'client':
            $Dir      = 'ClientControllers';
            $userType = 1;
            $table    = 'companies';
            break;
          case 'admin':
            $Dir      = 'AdminControllers';
            $userType = 0;
            break;
          case 'carrier':
            $Dir      = 'CarrierControllers';
            $userType = 2;
            $table    = 'carrier_details';
            break;
          default:
            $Dir = 'AdminControllers';
            $userType = 0;
            break;
        }

        $controllers    =   array_diff(scandir(APPPATH.'/'.$Dir), array('..', '.'));
        $classArray     =   array();
        $data           =   array();
        ///Get all Companies/////////////////////////////////////////////
        if (isset($table)) {
           $allCompanies     = DB::table($table)
                                  ->get();

          $data['companies'] =  $allCompanies;
        }
        //Get the methods in each controller with thier namespaces
        
        foreach ($controllers as $controller ) {

           if (file_exists(APPPATH.'/'.$Dir.'/'.$controller)) {

             $controllerName        = explode('.php', $controller)[0];

             $Object                =  '\App\Http\Controllers\\'.$Dir.'\\'.$controllerName;

             $reflectionClass       = new \ReflectionClass(new $Object());
             $reflectionMethod      = $reflectionClass->getMethods(\ReflectionMethod::IS_PUBLIC);
             $sievedMethods         = array();

             for ($i=0; $i < sizeof($reflectionMethod) ; $i++) { 
               if ($reflectionMethod[$i]->class == 'App\Http\Controllers\\'.$Dir.'\\'.$controllerName) {
                array_push($sievedMethods, array('name'         =>  $reflectionClass->getNamespaceName().'\\'.$controllerName.'\\'.$reflectionMethod[$i]->name,
                                                )
                          );
               }
             }
            $classArray[$controllerName]  = $sievedMethods;
           }
        }
        // var_dump($classArray);
        $data['classes']              =   $classArray;
        $data['deployedModules']      =   $this->getDeployedModules($userType);
        $data['userType']             =   $userType;
        return view('admin.allModules',$data);
    }

    public function viewdeployedModules(){
        $currentUrl     =   explode('/', URL::current());
       
        switch (end($currentUrl)){
          case 'client':
            $Dir      = 'ClientControllers';
            $userType = 1;
            break;

          case 'admin':
            $Dir      = 'AdminControllers';
            $userType = 0;
            break;

          case 'carrier':
            $Dir      = 'CarrierControllers';
            $userType = 2;
            break;

          default:
            $Dir = 'AdminControllers';
            $userType = 0;
            break;
        }

        $controllers    =   array_diff(scandir(APPPATH.'/'.$Dir), array('..', '.'));
        $classArray     =   array();
        $data           =   array();
        
        //Get the methods in each controller with thier namespaces
        
        foreach ($controllers as $controller ) {

           if (file_exists(APPPATH.'/'.$Dir.'/'.$controller)) {

             $controllerName        = explode('.php', $controller)[0];

             $Object                =  '\App\Http\Controllers\\'.$Dir.'\\'.$controllerName;

             $reflectionClass       = new \ReflectionClass(new $Object());
             $reflectionMethod      = $reflectionClass->getMethods(\ReflectionMethod::IS_PUBLIC);
             $sievedMethods         = array();

             for ($i=0; $i < sizeof($reflectionMethod) ; $i++) { 
               if ($reflectionMethod[$i]->class == 'App\Http\Controllers\\'.$Dir.'\\'.$controllerName) {
                array_push($classArray,  $reflectionClass->getNamespaceName().'\\'.$controllerName.'\\'.$reflectionMethod[$i]->name
                                                
                          );
               }
             }

           }
        }


        // var_dump($classArray);
        $data['allModules']               =   $classArray;
        $data['deployedModules']          =   $this->getDeployedModules($userType);
        $data['userType']                 =   $userType;

        return view('admin.deployedModules',$data);
    }

  //Manage Individual Company Modules
    public function CompanyModuleView($userType,$companyId){

      switch ($userType){
          case 'client':
            $userType = 1;
            break;
          case 'carrier':
            $userType = 2;
            break;
      }
      
      $userManagement       = new UserManagement();
      $getCompanyModules    = $userManagement->getCompanysModules($companyId,$userType);

      // var_dump((array) $getCompanyModules);
      // exit;


      // var_dump( array_column((array) $getCompanyModules, 'module_id'));
      // exit;

      $data                 = array(

                                    'allModules'             => (array) $this->getDeployedModules($userType),
                                    'permissionModules'      => array_column((array) $getCompanyModules, 'module_id'),
                                    'userType'               => $userType
                                  );

      return view('admin.companyModules',$data);
    }
}
