<?php

namespace App\Http\Controllers\AdminControllers;
use App\Http\Controllers\Controller;
use App\OrderResource;
use App\CarrierInvoice;
use App\CarrierResourceDetail;
use App\ClientRequiredResource;
use App\ClientInvoice;
use App\ClientOrder;
use App\simpleInvoice;
use App\CarrierCompanyWallet;
use App\UserComapany;
use App\Waybill;
use App\Company;
use Auth;
use App\ServiceRequest;
use App\carrierResource;
use App\CarrierDetail;
use App\OrderDocument;
use App\Invoice;
use App\User;
use App\HaulageResourceRequest;
use App\ResourceTypeNumber;
use App\ResourceGpsDetail;
use App\OrderResourceGpsDetail;
use App\carrierRouteMap;
use App\CarrierCompanyDetail;
use App\CarrierCompanyRoutePrice;
use App\CarrierCompanyInvoice;
use Illuminate\Http\Request;
use DB;
use Validator;
use Pagination;
use Notification;
use Notify;
use App\Notifications\newHaulageRequest;
use App\Notifications\newCarrierInvoice;
use App\Notifications\newClientInvoice;
use App\Notifications\Mobilized;
use App\carrierRegion;
use App\tempWaybillDetail;
use File;
use Excel;
use Illuminate\Support\Facades\Storage;
use PDF;
use Illuminate\Support\Facades\Mail;
use App\Mail\ALODocument;
use DataTables;


class ServiceRequestController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

	

	public function randomNumber(){

		$i = 1;

		for($j=0; $j < $i; $j++){
//            return random_int(1,10000);

			$result = '';

			$length = 7;

			for($x = 0; $x < $length; $x++) {
				$result .= mt_rand(0, 9);
			}

			$checkAvailable = serviceRequest::where('serviceIDNo', $result);

			if( $checkAvailable->count() < 1 ){

				return $result;

				break;
			}
			$i++;

		}
	}

	public function randomNumberClientInvoice(){

		$i = 1;

		for($j=0; $j < $i; $j++){
//            return random_int(1,10000);

			$result = '';

			$length = 7;

			for($x = 0; $x < $length; $x++) {
				$result .= mt_rand(0, 9);
			}

			$checkAvailable = Invoice::where('invoice_id', $result);

			if( $checkAvailable->count() < 1 ){

				return $result;

				break;
			}
			$i++;

		}
	}

	public function simpleInvoice(){

		$i = 1;

		for($j=0; $j < $i; $j++){
//            return random_int(1,10000);

			$result = '';

			$length = 7;

			for($x = 0; $x < $length; $x++) {
				$result .= mt_rand(0, 9);
			}

			$checkAvailable = simpleInvoice::where('invoiceNo', $result);

			if( $checkAvailable->count() < 1 ){

				return $result;

				break;
			}
			$i++;

		}
	}

	public function carrierCompanyInvoice(){

		$i = 1;

		for($j=0; $j < $i; $j++){
//            return random_int(1,10000);

			$result = '';

			$length = 7;

			for($x = 0; $x < $length; $x++) {
				$result .= mt_rand(0, 9);
			}

			$checkAvailable = CarrierCompanyInvoice::where('invoice_id', $result);

			if( $checkAvailable->count() < 1 ){

				return $result;

				break;
			}
			$i++;

		}
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function showAllForCustomer()
	{
		$allRequests = ServiceRequest::all();
		$countRequests = ServiceRequest::count();
		//return view('admin.orders.allOrders',['allRequests'=>$allRequests,'countRequests'=>$countRequests]);
		return view('admin.new_admin.orders.allOrders',['allRequests'=>$allRequests,'countRequests'=>$countRequests]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$createdByUser = Auth::user()->id;
		$createdContact = Auth::user()->email;

		$deliverFrom = $request->deliverFrom;
		$deliverTo = $request->deliverTo;
		$resourceType = $request->resourceType;
		$itemDescription = $request->itemDescription;
		$numberOfResources = $request->numberOfResources;
		$haulageType = $request->haulageType;
		$estimatedWgt = $request->estimatedWgt;
		$dateRequired = $request->dateRequired;
		$dateReturned = $request->dateReturned;
		$git = $request->git;
		$orderStatus = 0;

		$validator = Validator::make($request->all(), [
			'deliverFrom'=>'required',
			'deliverTo'=>'required',
			'resourceType'=>'required',
			'itemDescription'=>'required',
			'numberOfResources'=>'required',
			'haulageType'=>'required',
			'estimatedWgt'=>'required',
			'dateRequired'=>'required|date',
			'dateReturned'=>'required|date',
			'git'=>'required',
		]);

		if ($validator->passes()) {

			$rand = $this->randomNumber();

			$serviceReq = DB::table("service_requests")->insert([
				'serviceIDNo' => $rand,
				'deliverFrom' => $deliverFrom,
				'deliverTo' => $deliverTo,
				'estimatedWgt' => $estimatedWgt,
				'orderStatus' => $orderStatus,
				'itemDescription' => $itemDescription,
				'dateRequired' => $dateRequired,
				'dateReturned' => $dateReturned,
				'requiredResourceType' => $resourceType,
				'typeOfHaulage' => $haulageType,
				'numberOfResources' => $numberOfResources,
				'git' => $git,
				'createdBy' => $createdByUser,
				'contactDetails' => $createdContact,
				'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
				'updated_at' => \Carbon\Carbon::now()->toDateTimeString(),
			]);

			if($serviceReq){
				return response()->json(['success'=>'New Order Created Successfully']);
			}else{
				return response()->json(['error'=>'Something Went Wrong']);
			}

		}else{
			return response()->json(['error'=>$validator->errors()->all()]);
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */



	public function getCarrierDetails()
	{
		$collectDetails = array();
		$carrierDetails = CarrierCompanyDetail::all();
		/*for ($i=0; $i < count($carrierDetails); $i++) { 
			$collectDetails[] = CarrierDetail::where('user_id',$carrierDetails[$i]['id'])->get();
		}*/
		return $carrierDetails;
	}

	public function viewOrderDetails($id)
	{
		$orderDetails = serviceRequest::where('serviceIDNo',$id)->get();
		$requestId = serviceRequest::where('serviceIDNo',$id)->value('id');
		
		$resourceTypeNumber = ResourceTypeNumber::where('service_request_id',$requestId)->get();

		$carrierDetails = CarrierCompanyDetail::all();

		if($orderDetails){
			return view('admin.new_admin.orders.order_detail', ['orderDetails'=>$orderDetails, 'resourceTypeNumber'=>$resourceTypeNumber,'carrierDetails'=>$carrierDetails]);/*
			return view('admin.orders.orderDetails', ['orderDetails'=>$orderDetails, 'resourceTypeNumber'=>$resourceTypeNumber,'carrierDetails'=>$carrierDetails]);*/
		}else{
			return redirect('/viewOrders')->with('message', 'Order Not Found');
		}
	}

	public function loadSelected(Request $request)
	{
		$serviceIDNo = $request->orderID;
		$emptyArray = array();
		if(is_array($request->selectedCarrier)){
			for ($i=0; $i < count($request->selectedCarrier); $i++) { 
				$emptyArray[] = $request->selectedCarrier[$i];
			}
		}else{
			return "no carrier selected";
		}
		$res = $this->fetchCarrierRDetails($emptyArray);

		return view('admin.resources.carrierAssign',compact('serviceIDNo','res'));
		
	}


	public function fetchCarrierRDetails($selectedCarrierID)
	{
		$carrierID = $selectedCarrierID;
		$carrierResourceDetails = array();
		for ($i=0; $i < count($carrierID); $i++) { 
			$carrierResourceDetails[] = carrierDetail::where('user_id',$carrierID[$i])->get();
		}
		return $carrierResourceDetails;
	}

	public function assignResourceToOrder(Request $request)
	{

		$orderIDNo = $request->serviceIDNo;                                                
		$serviceID = ServiceRequest::where('serviceIDNo',$orderIDNo)->value('id');
		$invoiceID = Invoice::where('service_id', $serviceID)->value('id');


		$data = array(
			'status' => 1
		);

		Invoice::where(
			'id',$invoiceID)
			->update($data);

		$dataStatus = array(
			'orderStatus' => 4
		);

		serviceRequest::where(
			'id',$serviceID)
			->update($dataStatus);

		for ($i=0; $i < count($request->carrierID); $i++) {

			$path = $request->file('clientForm')[$i]->getRealPath();
			$data = Excel::load($path)->get();

			if($data->count()){

				foreach ($data as $value) {
					$arr[] = [
							'carrier_id' => $request->carrierID[$i],
							'service_requests_id' => $serviceID,
							'carrierName' => $request->companyName[$i],
							'driverName' => $value->driver,
							'plateNumber' => $value->plate,
							'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
							'updated_at' => \Carbon\Carbon::now()->toDateTimeString(),
					];
				}

				/*if(!empty($arr)){

					DB::table('order_resource_gps_details')->insert($arr);

					//$this->createInvoiceForClient($serviceID);

					//$this->export_pdf($serviceID);
					
				}else{
					return response()->json(['error'=>'Unable To Complete']);
				}*/

			}else{
				return response()->json(['error'=>'File Path Not Found']);
			}
		}   

		if(!empty($arr)){

			DB::table('order_resource_gps_details')->insert($arr);

			//$this->createInvoiceForClient($serviceID);

			//$this->export_pdf($serviceID);
			
		}else{
			return response()->json(['error'=>'Unable To Complete']);
		}

		return response()->json(['success'=>'Resources assigned successfully!']);

	}




	  public function export_pdf($serviceID)
	  {

		$serviceRequest = serviceRequest::where('id',$serviceID)->get();
		$ORDERGPS = OrderResourceGpsDetail::where('service_requests_id',$serviceID)->get();
		
		for ($i=0; $i < count($ORDERGPS); $i++) { 



			$carrierID = $ORDERGPS[$i]['carrier_id'];
			$plateNumber = $ORDERGPS[$i]['plateNumber'];
			$drivername = $ORDERGPS[$i]['driverName'];

			$carrierEmail = User::where('id',$carrierID)->value('email');


			$path = base_path('public/alo/');
			$pdf_name = $plateNumber.'.pdf';

			$fullPath = $path.$pdf_name;

			PDF::loadView('token', compact('plateNumber','drivername','serviceRequest'))->save($fullPath);

			$data = array(
				'email_address'=> $carrierEmail,
				'subject'=>"ALO Docs for ".$plateNumber,
				'filePath'=> $fullPath,
			);

			Mail::send('emails.alo', $data, function($message) use($data) {
				$message->from('no_reply.trucka@noreply', 'ALO Document');
				$message->to($data['email_address']);
				$message->subject($data['subject']);
				//Full path with the pdf name
				$message->attach($data['filePath']);
			});
		}
		
	  }



	public function createInvoiceForClient($id){

		$serviceID = $id;

		//$serviceIDCreatedBy = serviceRequest::where('serviceIDNo',$id)->value('createdBy');
		$serviceIDNo = serviceRequest::where('id',$id)->value('serviceIDNo');

		$user_id = serviceRequest::where('id',$serviceID)->value('createdBy');

		$rand = $this->randomNumberClientInvoice();

		$serviceReq = DB::table("invoices")->insert([
			'invoice_id' => $rand,
			'service_id' => $serviceID,
			'client_id' => $user_id,
			'status' => 0,
			'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
			'updated_at' => \Carbon\Carbon::now()->toDateTimeString(),
		]);

		$data = array(
			'orderStatus' => 3
		);

		$updateStatus = serviceRequest::where(
			'id',$serviceID)
			->update($data);

		/*if($serviceReq && $updateStatus){*/

			$user = User::find($user_id);

			$notification = $user->notify(new newClientInvoice($serviceIDNo));

			/*return back()->with('message', 'Invoice sent successfully');
		}else{
			return back()->with('message', 'Something went wrong');
		}*/

	}


	public function testfn($test){

		$joinRes = array();
		for($i = 0; $i < count($test); ++$i){//get resource details

			$joinRes[$i] = DB::table('carrier_details')->where('user_id', '=', $test[$i])
				->get();
		}
		return $joinRes;
	}

	public function searchResources($serviceIDNo)
	{
		$service = serviceRequest::where('serviceIDNo', $serviceIDNo)->get();

		foreach($service as $serv){
			$originRegion = $serv->originRegion;
			$destinationRegion = $serv->destinationRegion;
		}

		$carrierQuery = carrierRegion::where('regions_covered', '=', $originRegion)/*->orWhere('regions_covered',$destinationRegion)*/->get();

		if (count($carrierQuery) > 0) {

			foreach($carrierQuery as $carr){//get carrier ids from origin destination map
				$arr[] = [ $carr->user_id ];
			}

			if(empty($arr)){
				return back()->with('message', 'No Carrier Found!');
			}else{


			$res =  $this->testfn($arr);

				return view('admin.resources.searchResults',['res'=>$res, 'serviceIDNo'=>$serviceIDNo]);
			}
			
		}elseif (count($carrierQuery) < 1) {

			$carrierQueryDest = carrierRegion::where('regions_covered',$destinationRegion)->get();

			foreach($carrierQueryDest as $carr){//get carrier ids from origin destination map
				$arr[] = [ $carr->user_id ];
			}

			if(empty($arr)){
				return back()->with('message', 'No Carrier Found!');
			}else{

			$res =  $this->testfn($arr);

				return view('admin.resources.searchResults',['res'=>$res, 'serviceIDNo'=>$serviceIDNo]);
			}            
		}
	}

	public function carrierDetail($userID, $serviceIDNo){

		$carrierResource =  carrierResource::where('user_id', $userID)->get();

		$serviceRequestDetails = serviceRequest::where('serviceIDNo',$serviceIDNo)->get();

		foreach($serviceRequestDetails as $service){
			$id = $service->id;
		}

		$findID = serviceRequest::find($id);

		$orderResourceRequest = $findID->ResourceTypeNumber()->get();

		return view('admin.resources.carrierDetails', ['orderResourceRequest'=>$orderResourceRequest, 'carrierResource'=>$carrierResource, 'serviceRequestDetails'=>$serviceRequestDetails]);


	}

	public function sendHaulageRequest(Request $request){

	$serviceRequestID   =   $request->orderID;

	$carrier_id         =   $request->selectedCarrier;
	$resourceType       = 	$request->resourceType;
	$resourceNumber     = 	$request->resourceNumber;
	$carriers 					=		$request->carriers;

	$validatorHaulageRequest = Validator::make($request->all(), [
									'orderID'				=> 'required',
									'carriers.*'			=> 'required',
								]);

	if($validatorHaulageRequest->passes()){

				foreach ($carriers as $carrier) {

					if (isset($carrier['selected'])) {
						
						$sendHaulageRequest         = DB::table("carrier_haulage_resource_requests")->insert([

							'carrier_id'            => $carrier['selected'],
							'service_request_id'    => $serviceRequestID,

							//Index set to '0' because there is only one resource type
							'resourceType'          => $resourceType[0],
							'resourceNumber'        => $carrier['resourceNumber'],
							'resource_limit'        => $carrier['resourceNumber'],
							'created_at'            => \Carbon\Carbon::now()->toDateTimeString(),
							'updated_at'            => \Carbon\Carbon::now()->toDateTimeString(),

						]);

					}

				}
						
						$data = array(
							'orderStatus' => 1
						);

						$updateStatus = serviceRequest::where(
							'id',$serviceRequestID)
							->update($data);



				//$user = User::find($carrier_id);
				//$notification = Notification::send($user, new newHaulageRequest($serviceIDNo));
				$allRequests = ServiceRequest::all();
				$countRequests = ServiceRequest::count();
				return view('admin.orders.allOrders',['allRequests'=>$allRequests,'countRequests'=>$countRequests])->with('message','Haulage Request Sent Successfully');
				

		}else{
			return response()->json(['error'=>$validatorHaulageRequest->errors()->all()]);
		}

	}


	public function viewResource($id)
	{
		$resource = carrierResource::where('resource_id',$id)->get();
		$carrierDetailUserID = carrierResource::where('resource_id',$id)->value('user_id');
		$carrierDetail = carrierDetail::where('user_id',$carrierDetailUserID)->get();
		return view ('admin.resources.resourceDetails', ['resource'=>$resource, 'carrierDetail'=>$carrierDetail]);
	}


	public function carrierProceed($id){

		$serviceID = serviceRequest::where('serviceIDNo',$id)->value('id');

		$dataHC = array(
			'status' => 2
		);

		$carrierID = HaulageResourceRequest::where(
			'service_request_id',$serviceID)->value('carrier_id');

			$user = User::find($carrierID);

			$notification = $user->notify(new Mobilized($id));

		$updateHCStatus = HaulageResourceRequest::where(
			'service_request_id',$serviceID)
			->update($dataHC);

		$servReq = array(
			'orderStatus' => 5
		);

		$updateServiceRequest = serviceRequest::where(
			'serviceIDNo',$id)
			->update($servReq);

		if($updateHCStatus && $updateServiceRequest){
			return back()->with('message', 'Action Successful');
		}else{
			return back()->with('message', 'Something went wrong');
		}
	}


	public function closedOrders()
	{
		$closedOrders = serviceRequest::where('orderStatus',9)->get();
		return view('admin.orders.closedOrders',['closedOrders'=>$closedOrders]);
	}




	public function manageATLS()
	{
		return view('admin.orders.atls');
	}


	function getResourceOrders(Request $request)
	{
		$plateNumber    =   $request->plateNumber;

		$resourceID     =   CarrierResourceDetail::where('plateNumber',$plateNumber)->value('id');

		$orderResources =   DB::table('order_resources')
								  ->select('service_requests.serviceIDNo','service_requests.deliverTo',
											'service_requests.deliverFrom','service_requests.pickUpDate', 'order_resources.*')
								  ->join('service_requests','service_requests.id','=','order_resources.order_id')
								  ->where('resource_id',$resourceID)->where('status',0)
								  ->get();

		return view('admin.orders.selectOrder',compact('orderResources'));
		
	}


	function selectOrderATL($id)
	{
		$order_resources_id    =   $id;
		return view('admin.orders.addATL', compact('order_resources_id'));
	}


	public function addATL(Request $request)
	{
		
		$waybillNo              =   $request->waybill;
		$order_resources_id     =   $request->order_resource_id;
		$numberOfBags           =   $request->numberOfBags;
		
		$waybillFileName        =   $request->file('waybillFile')->store('waybillFiles');

		/*
			@get company id
		*/
		$order_id       =   OrderResource::where('id',$order_resources_id)->value('order_id');

		$resource_id    =   OrderResource::where('id',$order_resources_id)->value('resource_id');

		$created_by     =   ServiceRequest::where('id',$order_id)->value('createdBy');

		$companyID      =   UserComapany::where('user_id',$created_by)->value('company_id');
		/*
			@end get company id
		*/
		$carrierCompanyID   =   CarrierResourceDetail::where('id',$resource_id)->value('company_id');

		$origin             =   ServiceRequest::where('id',$order_id)->value('deliverFrom');
		$destination        =   ServiceRequest::where('id',$order_id)->value('deliverTo');


		$carrierPrice       =   CarrierCompanyRoutePrice::where('carrier_id',$carrierCompanyID)
									->where('origin',$origin)
									->where('destination',$destination)
									->value('price');

		$description        =   "Payment for movement of ".$numberOfBags." from ".$origin." to ".$destination;

		DB::transaction(function () use($order_id,$carrierCompanyID,$carrierPrice,$numberOfBags,$description,$companyID,$resource_id,$waybillFileName,$waybillNo) {


			//insert into carrier company wallet and invoice                          
			$carrierInvoiceID     = DB::table("carrier_company_invoices")->insertGetId([
						'invoice_id'    =>          $this->carrierCompanyInvoice(),
						'service_id'    =>          $order_id,
						'carrier_id'    =>          $carrierCompanyID,
						'status'        =>          1
					]);
		
				$benchmark                          =   600;        

				$pricePerBag                        =   $carrierPrice/$benchmark;

				$prices                             =   $pricePerBag * $numberOfBags;

				$balance                            =   CarrierCompanyWallet::where('company_id',$carrierCompanyID)
														->orderBy('created_at','desc')->value('balance');
				$newBalance                         =   $balance + $prices;

				$carrierWallet =  DB::table("carrier_company_wallets")->insert([
					'company_id'            => $carrierCompanyID,
					'admin_id'              => Auth::user()->id,
					'invoice_id'            => $carrierInvoiceID,
					'description'           => $description,
					'cr'                    => $prices,
					'balance'               => $newBalance,
					'created_at'            => \Carbon\Carbon::now()->toDateTimeString(),
					'updated_at'            => \Carbon\Carbon::now()->toDateTimeString(),
				]);



				$waybill                    =   new Waybill;

				$waybill->company_id        =   $companyID;
				$waybill->order_id          =   $order_id;
				$waybill->resource_id       =   $resource_id;
				$waybill->waybillFile       =   $waybillFileName;
				$waybill->waybillNo         =   $waybillNo;
				$waybill->numberOfBags      =   $numberOfBags;

				$waybill->save();

				$resourceStatus = array(
					'status' => 1
				);

				$updateServiceRequest = OrderResource::where(
					'resource_id',$resource_id)->where('order_id',$order_id)
					->update($resourceStatus);

		}, 5);

		return redirect('/manageATLs')->withMessage('Waybill Uploaded Succesfully');

	}

	public function getCompanyDetailsClientInvoice()
	{
		$clientCompany   =   Company::all();

		return view('admin.waybills.createInvoice',compact('clientCompany'));
	}

	public function getClientATLs(Request $request){

		$data           =       DB::table('waybills')
								  ->select('waybills.*','service_requests.deliverTo','service_requests.deliverFrom','carrier_resource_details.plateNumber','carrier_resource_details.driverName')
								  ->join('carrier_resource_details','carrier_resource_details.id','=','waybills.resource_id')
								  ->join('service_requests','service_requests.id','=','waybills.order_id')
								  ->where('waybills.company_id',$request->companyID)->where('status',0)
								  ->get();

		$clientCompany   =   Company::all();

		return view('admin.waybills.clientCompanyOrders',compact('data','clientCompany'));
	}


	public function createAdminInvoice(Request $request)
	{
	   $company_id      =   $request->company_id;
	   $waybillID       =   $request->id;
	   $waybillNo       =   $request->waybillNo;
	   $deliverFrom     =   $request->deliverFrom;
	   $deliverTo       =   $request->deliverTo;
	   $plateNumber     =   $request->plateNumber;
	   $driverName      =   $request->driverName;
	   $waybillFile     =   $request->waybillFile;
	   $order_id        =   $request->order_id;

	   //return $waybillID;

	   //getPricePerResourcePerOrder
			$pricePerTruck  =   array();

			//getPrices
			$prices         =   array();

			//getResourceDetails
			$resourceDetails = array();

			//orderIDS
			$orderIDs       =   array();

			//waybillFiles
			$waybill_file   =   array();

			

			for ($i=0; $i < count($waybillID); $i++) { 


				$waybill                                =   Waybill::where('id',$waybillID[$i])->get();

				foreach ($waybill as $way) {

					$service_request_id                 =   $way->order_id;

					$waybill_resource_id                =   $way->resource_id;

					$waybillFile                        =   $way->waybillFile;

					$waybillNo                          =   $way->waybillNo;

					$numberOfBags                       =   $way->numberOfBags;

				}

				$origin                             =   ServiceRequest::where('id',$service_request_id)->value('deliverFrom');

				$destination                        =   ServiceRequest::where('id',$service_request_id)->value('deliverTo');

				$pricePerTruck                      =   ClientRequiredResource::where('company_id',$company_id)
														->where('origin',$origin)
														->where('destination',$destination)
														->value('price');

				$benchmark                          =   600;

				$pricePerBag                        =   $pricePerTruck/$benchmark;

				$prices[]                           =   $pricePerBag * $numberOfBags;

				$resourceDet                        =   CarrierResourceDetail::where('id',$waybill_resource_id)->get();

				foreach ($resourceDet as $resource) {
					$plateNumber    =   $resource->plateNumber;
					$driverName    =    $resource->driverName;
				}

				$resourceDetails[]                  = [
														'plateNumber'       =>  $plateNumber,
														'driverName'        =>  $driverName,
														'waybillFile'       =>  $waybillFile,
														'numberOfBags'      =>  $numberOfBags,
														'pricePerBag'       =>  $pricePerBag,
														'price'             =>  $prices[$i],
														'origin'            =>  $origin,
														'deliverTo'         =>  $destination,
														'waybillNo'         =>  $waybillNo,
													];

				$orderIDs[]         =   ['order_id'=>$service_request_id];

				$waybill_file[]     =   $waybillFile;

				$totalPrice         =   array_sum($prices);

				$data = array(
						'status' => 1
					);

				$updateStatus = Waybill::where(
					 'id',$waybillID[$i])
					->update($data);

			}

			//return $pricePerTruck;


			$invoice                    =   new simpleInvoice;

			$invoice->invoiceNo         =   $this->simpleInvoice();

			$invoice->resourceDetails   =   json_encode($resourceDetails);

			$invoice->status            =   0;

			$invoice->price             =   json_encode($totalPrice);

			$invoice->waybillFiles      =   json_encode($waybill_file);

			$invoice->order_ids         =   json_encode($orderIDs);

			$invoice->company_id        =   $company_id;

			$invoice->save();

			$clientCompany   =   Company::all();

		return view('admin.waybills.createInvoice',compact('clientCompany'));
		
	}



}
