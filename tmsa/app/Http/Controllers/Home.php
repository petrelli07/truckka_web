<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Home extends Controller
{
    public function getHome(){
        return view('home.index');
    }

    public function getAboutTheExchange(){
        return view('home.about-the-exchange');
    }
    public function getAboutUS(){
        return view('home.about-us');
    }
    public function getCustomerSegment(){
        return view('home.our-customer-segment');
    }

    public function getBenefistOfTheExchange(){
        return view('home.benefits-of-the-exchange');
    }

    public function getOurServices(){

        return view('home.our-services');
    }

    public function getLogin(){
        return view('home.login');
    }

    public function getRegister(){
        return view('home.register');
    }

    public function ContactUS(){
        return view('home.contact-us');
    }

    public function Invoice(){
        $breadcrumb     =   'invoice';
        return view('exchange.admin.invoices', compact('breadcrumb'));
    }

    public function emailTemp(){
        return view('EmailTemp.welcome');
    }


}
