<?php

namespace App\Http\Controllers\ClientControllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\ManageClientSupplier;
use App\UserComapany;
use App\ClientSupplierDetail;
use App\ClientSupplyOrder;
use App\ClientSupplyOrderQuote;
use App\SuppliersGroup;
use App\SuppliersGroupList;
use App\SupplyRequest;
use App;
use DB;
use Validator;
use App\User;
use App\Notifications\newClientAccount;
use \App\Http\Controllers\AdminControllers\UserManagementController as Admin;
use \App\Http\Controllers\ClientControllers\UserManagementController as Client;


class SupplierController extends Controller{

    public $userId;
    public $companyID;

    public function userInit(){
      
      $userID           =   Auth::user()->id;
      
      $companyID        =   UserComapany::where('user_id',$userID)
                                        ->value('company_id');
      $this->userId     =   $userID;
      $this->companyID  =   $companyID;

    }

    public function createSupplierView(){
      $getGroups              =   $this->getGroups();
      $data                   =   array('companyGroups' => $getGroups);

      return view('client.supply.createSupplier',$data);
    }

    public function allSuppliersView(){

      $suppliersDetails   = $this->allSuppiers();

      $data               = array('suppliersDetail' => $suppliersDetails
                                );
      return view('client.supply.allSuppliers',$data);

    }
    
    public function createNewSupplier(Request $request){
          
        $this->userInit();

        $validator             =    Validator::make($request->all(),[
            'email'            =>   'required|unique:users',
            'name'             =>   'required',
            'address'          =>   'required'
            ]);

        if ($validator->passes()) {

//////////////////Start Database Transaction/////////////////////////////////////////////

            DB::transaction(function() use($request){
                
                $admin                 =   new Admin();
                $companyID             =   $this->companyID;
                $userType              =   3;
                $email                 =   $request->email;
                $agentName             =   $request->name;
                $address               =   $request->address;
                $defaultPassword       =   $admin->randomNumberForPassword();
                $roleId                =   3;
                $groupId               =   $request->groupg;
                $bcryptPass            =   bcrypt($defaultPassword); 
                $client                =   new Client();

//////////////////////////Create New User///////////////////////////////////////////////
                
                $insertUserDetailsID    = DB::table("users")->insertGetId([
                    'name'              => $agentName,
                    'email'             => $email,
                    'password'          => $bcryptPass,
                    'default_password'  => $bcryptPass,
                    'userAccessLevel'   => 3,
                    'created_at'        => \Carbon\Carbon::now()->toDateTimeString(),
                    'updated_at'        => \Carbon\Carbon::now()->toDateTimeString(),
                ]);


////////////////Insert Modules into user's permission table/////////////////////////////////////////////////
                // $permissions            = $client->getRolesPermissionList($roleId);


                // $insertUserRoles        = $client->insertPermission($insertUserDetailsID, $permissions);

/////////////////Add user to Appropriate Company Table/////////////////////////////////////////////////////////////

                $insertUserCompany      = $client->newUserCompany($insertUserDetailsID, $companyID,$userType);

/////////////////////////////////Save user into supplier's table and get the id////////////////////////////////////////////
                $supplier               = $this->createSupplier($insertUserDetailsID,$agentName,$address);

////////////////////////////////Add Supplier to Group /////////////////////////////////////////////////////

                if (is_numeric($groupId)) {
                  $supplierGroup        =  $this->addSupplierToGroup($groupId, $supplier);
                }

////////////////Send Mail to the User/////////////////////////////////////////////////////////////
               

                $user         = User::find($insertUserDetailsID);
                $notification = $user->notify(new newClientAccount($defaultPassword));
                
            },2);

            return response()->json(['success'=>'New Agent Created Successfully']);
        }else{
            return response()->json(['error'=>$validator->errors()->all()]);
        }
    }

    public function createSupplier($userId,$name,$address){
      
      $this->userInit(); 

      $data           = array('user_id'         =>  $userId,
                              'company_id'      =>  $this->companyID,
                              'supplierAddress' =>  $address,
                              'supplierName'    =>  $name
                              );

      $saveSupplier   = ClientSupplierDetail::insertGetId($data);

      return $saveSupplier;
    }
    public function allSuppiers(){
      
      $this->userInit();

      $suppliersDetail =   ClientSupplierDetail::where('company_id',$this->companyID)
                                                ->get();
      return $suppliersDetail;
    }
    public function addSupplierToGroup($groupId,$supplier){
      
      $supplierInsert = array('supplier_id'  => $supplier,
                              'group_id'     => $groupId
                              );

      $saveGroupList  = SuppliersGroupList::insert($supplierInsert);

    }
    
    public function index(){
      $this->userInit();

		  $supplierDetail =	ClientSupplierDetail::where('company_id',$this->companyID)
                                            ->join('users','users.id','=','client_supplier_details.user_id')
                                            ->get();
    	return view('client.supply.view_suppliers',compact('supplierDetail'));
    }

    // Create Group for Suppliers
    public function createGroupView(){
        $userID         =   Auth::user()->id;
        
        $companyID      =   UserComapany::where('user_id',$userID)
                                        ->value('company_id');

        $supplierDetail =   ClientSupplierDetail::where('company_id',$companyID)
                                                ->get();

        $data           =   array('supplierDetail'  => $supplierDetail,
                                  'groupMembers'    => array()
                                  );

        return view('client.supply.createSuppliersGroup',$data);

    }

    public function editGroupView($groupId){
        
        $userID         =   Auth::user()->id;
        
        $companyID      =   UserComapany::where('user_id',$userID)
                                        ->value('company_id');

        $supplierDetail =   ClientSupplierDetail::where('company_id',$companyID)
                                                ->get();

        $groupMembers   =   SuppliersGroupList::where('group_id','=',$groupId)
                                              ->select('supplier_id')
                                              ->get();

        $groupDetails   =   SuppliersGroup::where('id','=',$groupId)
                                          ->where('company_id','=',$companyID)
                                          ->get();

        if (sizeof($groupDetails) < 1) {
          return redirect('/suppliers/groups');
        }

        $data           =   array('supplierDetail' => $supplierDetail,
                                  'groupMembers'   => array_column($groupMembers->toArray(), 'supplier_id'),
                                  'editField'      => $groupDetails[0]
                                );


        return view('client.supply.createSuppliersGroup',$data);

    }

    public function CreateGroup(Request $request){
        $validator = Validator::make($request->all(), [
                'groupName' => 'required'
            ]);
        if ($validator->passes()) {
          
          $userID           =   Auth::user()->id;
      
          $companyID        =   UserComapany::where('user_id',$userID)
                                      ->value('company_id');

          $groupInsert      =  array( 'groupName' => $request->groupName,
                                      'company_id' =>$companyID
                                    );

          $suppliers        =  $request->suppliers;
         
          // Start Transaction
         
          DB::transaction(function()use($groupInsert,$request,$suppliers){
            

  
            $supplierInsert   =  array();
            
            // Save group and get its Id
            
            $groupId =  SuppliersGroup::insertGetId($groupInsert);
            
            //restructure array to a multidimensional array for mass assignment
            
            if (is_array($suppliers)) {
              foreach ($suppliers as $supplier ) {
                array_push($supplierInsert, array('supplier_id'  => $supplier,
                                                      'group_id' => $groupId
                                                  ));
              }
              $saveGroupList  = SuppliersGroupList::insert($supplierInsert);
            }
          });
        }
    }

    public function editGroup(Request $request,$groupId){
     $validator = Validator::make($request->all(), [
                'groupName' => 'required'
            ]);

      if ($validator->passes()) {

          $this->userInit();


          $groupInsert      =  array( 'groupName' => $request->groupName,
                                    );

          $suppliers        =  $request->suppliers;
         
          // Start Transaction
         
          DB::transaction(function()use($groupInsert,$request,$suppliers,$groupId){
            
            $suppliersGroupList   =  new SuppliersGroupList();
  
            $supplierInsert   =  array();
            
            // Save group and get its Id
            
            $editGroup =  SuppliersGroup::where('id','=',$groupId)
                                        ->where('company_id','=',$this->companyID)
                                        ->update($groupInsert);
            
           $deleteGroupMemebers         =  $suppliersGroupList->join('suppliers_groups','suppliers_groups.id','=','suppliers_group_lists.group_id')
                                                        //Get Suppliers name and details
                                                              ->join('client_supplier_details','client_supplier_details.id','=','suppliers_group_lists.supplier_id')
                                                              ->where('group_id','=',$groupId)
                                                              ->where('client_supplier_details.company_id',$this->companyID)
                                                              ->delete();
            
            //restructure array to a multidimensional array for mass assignment

            
            if (is_array($suppliers)) {
              foreach ($suppliers as $supplier ) {
                array_push($supplierInsert, array('supplier_id'  => $supplier,
                                                      'group_id' => $groupId
                                                  ));
              }

              $saveGroupList  = SuppliersGroupList::insert($supplierInsert);
            }
          });
        }  
    }
    public function getGroups(){
  
     $this->userInit();
     
     $companyGroups   =  SuppliersGroup::where('company_id','=',$this->companyID)
                                        ->get();
     return $companyGroups;
    }

    public function viewGroups(){

     // Initialize User Details e.g UserId and CompanyId

     $this->userInit();
     
     $companyGroups   =  $this->getGroups();
     
     $data            =  array('groups' => $companyGroups);

     return view('client.supply.viewGroups',$data);

    }

    public function getGroupMembers($groupId,$companyID){
    
      $suppliersGroupList    =  new SuppliersGroupList();

      $groupMembers          =  $suppliersGroupList->join('suppliers_groups','suppliers_groups.id','=','suppliers_group_lists.group_id')
                                                        //Get Suppliers name and details
                                                  ->join('client_supplier_details','client_supplier_details.id','=','suppliers_group_lists.supplier_id')
                                                  ->where('group_id','=',$groupId)
                                                  ->where('client_supplier_details.company_id',$companyID)
                                                  ->get();
      return $groupMembers;
    }

    public function viewGroupMembers($groupId = Null){
      
      $this->userInit(); 

      $groupMembers = $this->getGroupMembers($groupId, $this->companyID);   

      $data         = array('members' => $groupMembers);

      
      return view('client.supply.viewGroupMembers',$data);

    }

    public function removeGroupMember($groupId,$supplierId){
      $this->userInit();

      $suppliersGroupList   =  new SuppliersGroupList();

      $suppliersGroupList->join('client_supplier_details','client_supplier_details.id','=', 'suppliers_group_lists.supplier_id')
                         ->where('company_id','=',$this->companyID)
                         ->where('group_id','=',$groupId)
                         ->where('supplier_id','=',$supplierId)
                         ->delete();

      return redirect('/suppliers/group/view/'.$groupId);

    }

    public function createNewSupplyOrder(){
      $this->userInit();

    	$companyGroups   =  $this->getGroups();
      $data            =  array('companyGroups' => $companyGroups);
      
      return view('client.supply.create_supply_order',$data);

    }

    public function submitNewSupplyOrder(Request $request){


      $validator = Validator::make($request->all(), [
              
                'itemDescription' => 'required',
                'expiryDate'      => 'required',
                'quantity'        => 'required',
                'groups'          => 'required'
            ]);

      if($validator->passes()){
           
        $this->userInit(); 


        $purchaseOrderFilePath          =  $request->file('purchaseOrder')->store('clientPurchaseOrders');

        
        DB::transaction(function()use($request){
          

          $groupMembersList                 =  [];
          $purchaseOrderFilePath            =  $request->file('purchaseOrder')->store('clientPurchaseOrders');
         //$purchaseOrderFilePath = '/';
          $expiryDate                       =  date("Y-m-d", strtotime($request->expiryDate));
          $quantity                         =  $request->quantity;
          $itemDescription                  =  $request->itemDescription;
          $groups                           =  $request->groups;

            // Get group members of individual group and merge it  
          foreach ($groups as $groupId) {
            
            $groupMembers       = $this->getGroupMembers($groupId, $this->companyID)->toArray();
            $groupMembersList   = array_merge($groupMembersList, array_column($groupMembers, 'user_id'));

          }

          //Create new Supply Order
          $supplyOrderId         =  DB::table("client_supply_orders")->insertGetId(
                [
                    'orderDescription'    => $itemDescription,
                    'orderQuantity'       => $quantity,
                    'purchaseOrder'       => $purchaseOrderFilePath,
                    'expires_at'          => $expiryDate,
                    'client_user_id'      => $this->userId,
                    'client_company_id'   => $this->companyID,
                    'created_at'          => \Carbon\Carbon::now()->toDateTimeString(),
                    'updated_at'          => \Carbon\Carbon::now()->toDateTimeString(),
                ]);

          //Remove Duplicates and run a foreach loop 
          foreach (array_unique($groupMembersList) as $groupMember) {
          
            

            DB::table("supply_requests")->insert(
                [
                    'client_supply_order_id'=>$supplyOrderId,
                    'company_id'            => $this->companyID,
                    'user_id'               => $groupMember,
                    'created_at'            => \Carbon\Carbon::now()->toDateTimeString(),
                    'updated_at'            => \Carbon\Carbon::now()->toDateTimeString(),
                ]);


          }
          	
            return response()->json(['success'=>'New Supply Order Created Successfully']);         

        });

	    }else{
			return response()->json(['error'=>$validator->errors()->all()]);
	    }

    }

    public function allClientSupplyOrders()
    {
    	$userID 		       =	Auth::user()->id;
      $companyID         =  UserComapany::where('user_id',$userID)->value('company_id');

    	$supplierDetail    =	ClientSupplyOrder::where('client_company_id',$companyID)->get();

    	return view('client.supply.viewSupplyOrders',compact('supplierDetail'));
    }

    public function allSupplierOrders(){
    	$userID 		     =	Auth::user()->id;
    	//$supplyOrder 	=	ClientSupplyOrder::where('client_user_id',$userID)->get();

    	 $supplierDetail =	SupplyRequest::where('user_id',$userID)
                          ->join('client_supply_orders','client_supply_orders.id','=','supply_requests.client_supply_order_id'
                                 )
                          ->select('client_supply_orders.*','supply_requests.*')
                          ->get();

    	return view('client.supply.supplier.viewSupplyOrders',compact('supplierDetail'));
    }

    public function createQuote($supplyOrderID)
    {
    	$supplyOrderID	=	$supplyOrderID;

    	//$supplyOrder 	=	ClientSupplyOrder::where('id',$supplyOrderID)->get();

    	return view('client.supply.supplier.newSupplierQuote',compact('supplyOrderID'));
    }

    public function submitQuote(Request $request)
    {


    	$validator = Validator::make($request->all(), [
                'deliveryDate'  => 'required',
                'units'         => 'required',
                'unitPrice'     => 'required',
                'invoice'       => 'required|file'
            ]);

    	if($validator->passes()){
    	
        	$invoice = $request->file('invoice')->store('supplierQuotes');

        	$deliveryDate 		 = 	date("Y-m-d", strtotime($request->deliveryDate));
        	$units 				     = 	$request->units;
        	$unitPrice 			   = 	$request->unitPrice;
        	$supplierID   		 =	Auth::user()->id;
        	$supplyOrderID 		 =	$request->supplyOrderID;

        	$supplyQuote = DB::table("client_supply_order_quotes")->insert([
                'deliveryDate' 			=> $deliveryDate,
                'unit' 					    => $units,
                'unitPrice' 			  => $unitPrice,
                'supplier_id' 			=> $supplierID,
                'invoice' 				  => $invoice,
                'supply_order_id'   => $supplyOrderID,
                'created_at' 	      => \Carbon\Carbon::now()->toDateTimeString(),
                'updated_at' 	      => \Carbon\Carbon::now()->toDateTimeString(),
            ]);



            $data = array(
		            'status' => 1
		        	);

		    $updateOrderStatus	=	SupplyRequest::where(
	            'client_supply_order_id',$supplyOrderID)
	            ->update($data);


            if ($supplyQuote) {
            	return response()->json(['success'=>'New Supply Quote Created Successfully']);
            }else{
            	return response()->json(['error'=>'An error Occurred!']);
            }

	    }else{
			return response()->json(['error'=>$validator->errors()->all()]);
	    }

    }

    public function viewQuote($id)
    {
    	$quoteDetails =	DB::table('client_supply_order_quotes')->where('supply_order_id',$id)
                                ->join('users','users.id','=','client_supply_order_quotes.supplier_id')
                                ->select('users.name','client_supply_order_quotes.*')
						                    ->get();
    	 return view('client.supply.quoteDetails',compact('quoteDetails'));
    }

    public function supplierViewQuote($id){

      $quoteDetails = DB::table('client_supply_order_quotes')->where('supply_order_id',$id)
                                ->join('users','users.id','=','client_supply_order_quotes.supplier_id')
                                ->select('users.name','client_supply_order_quotes.*')
                                ->get();

       return view('client.supply.supplier.supplierQuoteDetails',compact('quoteDetails'));
    }

    public function approveQuote($id)
    {
    	$orderID =	$id;

    	$data = array(
		            'status' => 1
		        	);

	    $updateOrderStatus	=	ClientSupplyOrder::where(
            'id',$orderID)
            ->update($data);

        return back()->withMessage('Quote Approved');
    }

    public function markComplete($id)
    {
    	$clientSupplyOrderID =	$id;

    	$data = array(
		            'status' => 3
		        	);

	    $updateOrderStatus	=	ClientSupplyOrder::where(
            'id',$clientSupplyOrderID)
            ->update($data);

        return back()->withMessage('Order Complete');

    }

    public function downloadInvoice($id)
    {

      $filePath   = ClientSupplyOrderQuote::where('id',$id)->value('invoice');

      $file = storage_path()."/app/".$filePath;

      $filename   = trim( explode('/', $filePath)[1] );

      $headers = array(
                'Content-Type: application/pdf',
              );

      return response()->download($file, $filename, $headers);
    }

    public function downloadPurchaseOrder($id)
    {

      $filePath   = ClientSupplyOrder::where('id',$id)->value('purchaseOrder');

      $file = storage_path()."/app/".$filePath;


      $filename   = trim( explode('/', $filePath)[1] );

      $headers = array(
                        'Content-Type: application/pdf',
                      );

      if (file_exists($file)) {
        return response()->download($file, $filename, $headers);
      }else{
        return "doesnt exist";
      }

    }


}
