<?php

namespace App\Http\Controllers\ClientControllers;
use App\Http\Controllers\Controller;
use App\carrierResource;
use App\simpleInvoice;
use App\UserRole;
use App\ClientOrder;
use App\Company;
use Auth;
use App\ServiceRequest;
use App\ConsolidatedInvoice;
use App\OrderResource;
use Illuminate\Http\Request;
use DB;
use Validator;
use App\haulageResourceRequest;
use App\User;
use App\ResourceTypeNumber;
use App\OrderResourceGpsDetail;
use App\ResourceGpsDetail;
use App\InvoiceDetail;
use File;
use App\Invoice;
use Notification;
use Notify;
use PDF;
use App\Notifications\newOrder;
use App\ClientRequiredResource;
use App\UserComapany;
use App\FinalInvoice;
use App\OrderDocumentDestination;
use Excel;
use Illuminate\Support\Facades\Mail;
use App\Mail\ALODocument;
use Storage;
use ZipArchive;

//Note: wrap try-catch blocks around actions posted to server
class ServiceRequestController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function changePass(){
        return view('passwords.changePassword');
    }

    public function getUserRole($userID)
    {
        $UserRole = UserRole::where('user_id',$userID)->value('role');
        return view('client.includes.sidebar',['UserRole'=>$UserRole]);
    }

    public function resetPassword(Request $request){

        $userPassword = Auth::user()->password;
        $id = Auth::user()->id;
        $newPassword = bcrypt($request->password);

        $validator = Validator::make($request->all(), [
            'password' => 'required|string|min:6|confirmed',
        ]); 

        if($validator->passes()){

        $changeDefaultPassword = array(
            'default_password' => "0",
            'password'=>$newPassword,
        );
        User::where('id',$id)->update($changeDefaultPassword);
        return response()->json(['success'=>'Password Reset Successful']);
        }else{
            return response()->json(['error'=>$validator->errors()->all()]);
        }

    }

    public function randomNumber(){

        $i = 1;

        for($j=0; $j < $i; $j++){
//            return random_int(1,10000);

            $result = '';

            $length = 7;

            for($x = 0; $x < $length; $x++) {
                $result .= mt_rand(0, 9);
            }

            $checkAvailable = ServiceRequest::where('serviceIDNo', $result);

            if( $checkAvailable->count() < 1 ){

                return $result;

                break;
            }
            $i++;

        }
    }

    public function finalInvoiceNumber(){

        $i = 1;

        for($j=0; $j < $i; $j++){
//            return random_int(1,10000);

            $result = '';

            $length = 7;

            for($x = 0; $x < $length; $x++) {
                $result .= mt_rand(0, 9);
            }

            //$checkAvailable = FinalInvoice::where('invoice_no', $result);
            $checkAvailable = ConsolidatedInvoice::where('invoiceNo', $result);

            if( $checkAvailable->count() < 1 ){

                return $result;

                break;
            }

            $i++;

        }
    }

    /*public function finalInvoiceNumber()
    {
        $checkInvoiceExist = FinalInvoice::count();
        if ($checkInvoiceExist>0) {
            $invVal = FinalInvoice::orderBy('created_at', 'desc')->first()->value('invoice_no');
            $finalInvVal = $inv + 1;
            return $finalInvVal;
        }elseif($checkInvoiceExist==0) {
            $invVal = "00001";
            return $invVal + "00001";
        }
                
    }*/



    public function checkOrigins(Request $request){
        $userID = Auth::user()->id;
        $originToServer = $request->originalServer;
        $companyID = UserComapany::where('user_id',$userID)->value('company_id');
        $destination = ClientOrder::where('origin',$originToServer)->where('company_id',$companyID)->get();
    
        return response()->json(['success'=>$destination]);
    }


     public function viewOrderDetails($id)
    {
        $userID = Auth::user()->id;
        $orderDetails = ServiceRequest::where('serviceIDNo',$id)
           ->join('user_comapanies', 'user_comapanies.user_id', '=', 'service_requests.createdBy')
           ->join('resource_type_numbers', 'resource_type_numbers.service_request_id', '=', 'service_requests.id')
           ->join('companies','companies.id' ,'=' ,'user_comapanies.company_id')
           ->get();
            
        $serviceId = ServiceRequest::where('serviceIDNo',$id)->value('id');

        $finalResDets = DB::table('order_resources')
                        ->where('order_id',$serviceId)
                        ->join('carrier_resource_details', 'carrier_resource_details.id', '=', 'order_resources.resource_id')
                        ->get();

        //$UserRole = $this->getUserRole($userID);

        if($orderDetails){
            return view('client.orders.singleOrder', ['orderDetails'=>$orderDetails,'finalResDets'=>$finalResDets]);
        }else{
            return redirect('/viewOrders')->with('message', 'Order Not Found');
        }
    }

    public function showOrdersForSupervisor()
    {
        $userID = Auth::user()->id;
        $companyID = UserComapany::where('user_id',$userID)->value('company_id');
        $orderDetails = DB::table('service_requests')
                        ->join('user_comapanies', 'user_comapanies.user_id', '=', 'service_requests.createdBy')
                        ->where('user_comapanies.company_id','=',$companyID)
                        // ->select('haulage_resource_requests.*', 'service_requests.*')
                        ->get();

        //$UserRole = $this->getUserRole($userID);

        return view('client.orders.companyOrders',compact('orderDetails'));
    }

    


    public function showAllForCustomer()
    {
        $createdByID = Auth::user()->id;
        $allRequests = ServiceRequest::where('createdBy',$createdByID)
         ->paginate(10);
         
        $countRequests = ServiceRequest::where('createdBy',$createdByID)->count();
        // $UserRole = $this->getUserRole($createdByID);
        
        return view('client.orders.myorders',['allRequests'=>$allRequests,'countRequests'=>$countRequests]);
        //return view('orders.allOrders',['allRequests'=>$allRequests,'countRequests'=>$countRequests]);
    }

    public function store(Request $request)
    {
        $createdByUser = Auth::user()->id;
        $createdContact = Auth::user()->email;

        $deliverFrom = $request->deliverFrom;
        $deliverTo = $request->deliverTo;
        $weight = $request->weight;
        $itemDescription = $request->itemDescription;
        //$packagingType = $request->packagingType;
        $haulageType = 0;
        $confirmpaymentType = 0;
        $contactName = $request->contactName;
        $contactPhone = $request->contactPhone;
        $value = $request->amount;
        $pickupDate = $request->pickupDate;
        $pickupTime = $request->pickupTime;
        $haulageVal = $request->haulageVal;
        $standardOrigin = $request->standardOrigin;
        $standardDestination = $request->standardDestination;
        $orderStatus = 0;

        $validator = Validator::make($request->all(), [
            'deliverFrom'=>'required',
            'deliverTo'=>'required',
            'resourceType'=>'required',
            'itemDescription'=>'required',/*
            'haulageType'=>'required',*/
        ]);

                $rand = $this->randomNumber();

                if($request->haulageType == 1){

                    if ($validator->passes()) {

                        $serviceReq = DB::table("service_requests")->insert([
                            'serviceIDNo' => $rand,
                            'deliverFrom' => $deliverFrom,
                            'deliverTo' => $deliverTo,
                            'orderStatus' => $orderStatus,
                            'itemDescription' => $itemDescription,/*
                            'requiredResourceType' => $resourceType,
                            'packagingType' => $packagingType,*/
                            'typeOfHaulage' => $haulageType,
                            'estimatedWgt' => $weight,
                            'valueOfHaulage' => $haulageVal,
                            'amountForHaulage' => $value,
                            'pickupDate' => $pickupDate,
                            'pickUpTime' => $pickupTime,
                            'createdBy' => $createdByUser,
                            'contactDetails' => $createdContact,
                            'contactName' => $contactName,
                            'contactPhone' => $contactPhone,
                            'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
                            'updated_at' => \Carbon\Carbon::now()->toDateTimeString(),
                        ]);

                        if($serviceReq){
                            return response()->json(['success'=>'New Order Created Successfully']);
                        }else{
                            return response()->json(['error'=>'Something Went Wrong']);
                        }

                    }else{
                        return response()->json(['error'=>$validator->errors()->all()]);
                    }

                }else{

                    //return $request->resourceType;

                    $validatorStandard = Validator::make($request->all(), [
                        'standardOrigin'=>'required',
                        'standardDestination'=>'required',
                        'resourceType.*'=>'required',
                        'resourceNumber.*'=>'required|numeric',
                        'itemDescription'=>'required',/*
                        'haulageType'=>'required',*/
                        'pickupTime'=>'required',
                        'pickupDate'=>'required',
                        'contactName'=>'required',
                        'contactPhone'=>'required'
                        // 'confirmpaymentType'=>'required',
                    ]);


                $standardOrigin = $request->standardOrigin;
                $standardDestination = $request->standardDestination;

                $userID = Auth::user()->id;
                $originToServer = $request->originToServer;
                $companyID = UserComapany::where('user_id',$userID)->value('company_id');

                $originRegion = ClientOrder::where('company_id',$companyID)->where('origin',$standardOrigin)->value('originRegion');
                $destinationRegion = ClientOrder::where('company_id',$companyID)->where('id',$standardDestination)->value('destinationRegion');

                //return $originRegion.' '.$destinationRegion;

                if($validatorStandard->passes()){
                    $testArray = array();
                    for($rtu=0;$rtu<count($request->resourceType);$rtu++){
                        if(in_array($request->resourceType[$rtu], $testArray)){
                            return response()->json(['error'=>'You cannot pick a Resource Type more than once']);
                        }
                        $testArray[] = $request->resourceType[$rtu];
                    }

                   /* print_r($testArray);
                    die();*/

                    $userID = Auth::user()->id;
                    $companyID = UserComapany::where('user_id',$userID)->value('company_id');
                    //$destination = ClientOrder::where('origin',$originToServer)->where('company_id',$companyID)->get();

                    $destinationValue = ClientOrder::where('id',$standardDestination)->where('company_id',$companyID)->value('destination');

                    //return $request->resourceType;

                    $res = array();

                    //$region = ClientOrder::where('destination',$destinationValue)->where('company_id',$companyID)->value('region');

                        $serviceReq = DB::table("service_requests")->insertGetId([
                            'serviceIDNo' => $rand,
                            'deliverFrom' => $standardOrigin,
                            'deliverTo' => $destinationValue,
                            'orderStatus' => $orderStatus,
                            'clientOrderID' => $standardDestination,
                            'itemDescription' => $itemDescription,/*
                            'itemDescription' => $itemDescription,/*
                            'requiredResourceType' => $resourceType,
                            'packagingType' => $packagingType,*/
                            'typeOfHaulage' => $haulageType,
                            'estimatedWgt' => $weight,
                            //'region' => $region,
                            'valueOfHaulage' => $haulageVal,
                            'originRegion' => $originRegion,
                            'destinationRegion' => $destinationRegion,
                            'pickupDate' => date("Y-m-d", strtotime($pickupDate)),
                            'expires_at' => date("Y-m-d", strtotime($pickupDate)),
                            'is_reverse' => 0,
                            'pickUpTime' => $pickupTime,
                            'createdBy' => $createdByUser,
                            'contactDetails' => $createdContact,
                            'estimatedWgt' => 0,
                            'valueOfHaulage'   => 0,
                            'contactName'=>$contactName,
                            'contactPhone'=>$contactPhone,
                            'paymentType' => $confirmpaymentType,
                            'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
                            'updated_at' => \Carbon\Carbon::now()->toDateTimeString(),
                        ]);

                    $hold = array();


                    for($i = 0; $i < count($request->resourceNumber); $i++){

                        $res[] = ClientRequiredResource::where('company_id',$companyID)->where('origin',$standardOrigin)->where('destination',$destinationValue)->where('resourceType',$request->resourceType[$i])->value('price');


                        DB::table("resource_type_numbers")->insert([
                            'service_request_id' => $serviceReq,
                            'resourceType' => $request->resourceType[$i],
                            'resourceNumber' => $request->resourceNumber[$i],
                            'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
                            'updated_at' => \Carbon\Carbon::now()->toDateTimeString(),
                        ]);
                        $hold[$i] = $res[$i] * $request->resourceNumber[$i];
                    }


                    $arraySum = array_sum($hold);

                    $vat = 0.05 * $arraySum;
                    $amtPlusVat = $vat + $arraySum;

                    $vatUpfront = 0.5 * $vat;
                    $amtPlusVatUpfront = $vatUpfront + (0.5 * $arraySum);

                    if($confirmpaymentType == 0){
                        $orderInfo = ['upfrontAmt'=>$amtPlusVatUpfront, 'upfrontVat'=> $vatUpfront,'serviceOrderNo'=>$rand, 'totalAmount'=>$arraySum, 'amountPlusVat'=>$amtPlusVat];
                    }elseif($confirmpaymentType == 1){
                        $orderInfo = ['serviceOrderNo'=>$rand, 'totalAmount'=>$arraySum, 'amountPlusVat'=>$amtPlusVat];
                    }

                    $encode = json_encode($orderInfo);

                    $countInvDet = InvoiceDetail::where('service_requests_id',$serviceReq)->count();

                    /*$file = $rand.'.json';
                    $destinationPath = public_path()."/invoiceInfoFiles/";
                    $fileNamePlusDirectory = $destinationPath.'/'.$file;*/

                    if($countInvDet < 1){
                        $invoiceDetails = new InvoiceDetail;
                        $invoiceDetails->orderDetail = $encode;
                        $invoiceDetails->service_requests_id = $serviceReq;
                        $invoiceSave = $invoiceDetails->save();
                        if(!$invoiceSave){
                            return response()->json(['error'=>'something went wrong creating the temporary invoice file']);
                        }

                    }else{
                        return response()->json(['error'=>'A similar order exists. Please cancel it']);
                    }

                    if($serviceReq){

                        $userID =Auth::user()->id;
                        $user = User::find($userID);

                         $notification = $user->notify(new newOrder($rand));

                         $name           = Auth::user()->name;

                        $activity = new ClientActivityLog;

                        $activity->company_id   = $companyID;
                        $activity->user_id      = $userID;
                        $activity->description  = $name." created an order with ID: ".$rand;
                        $activity->save();

                        return response()->json(['success'=>'New Order Created Successfully']);
                    }else{
                        return response()->json(['error'=>'Something Went Wrong']);
                    }
                }else{
                        return response()->json(['error'=>$validatorStandard->errors()->all()]);
                }

            }
    }

    public function viewInvoice($id){

            $serviceIDNo = $id;

            $userID = Auth::user()->id;
            $companyID = UserComapany::where('user_id',$userID)->value('company_id');

            $userDetails = ClientOrder::where('company_id',$companyID)->get();

            $serviceId = ServiceRequest::where('serviceIDNo',$serviceIDNo)->value('id');

            $orderGPSDetails = OrderResourceGpsDetail::where('service_requests_id',$serviceId)->get();

            $fileContent = InvoiceDetail::where('service_requests_id',$serviceId)->value('orderDetail');

            $decodedFileContent = json_decode($fileContent, 1);

            $amt = number_format($decodedFileContent['amountPlusVat'], 2, '.', ',');

            $totalAmount = number_format($decodedFileContent['totalAmount'], 2, '.', ',');

            $totalAmountForVAT = $decodedFileContent['totalAmount'];

            $vat = $totalAmountForVAT * 0.05;

            $vatVal = number_format($vat, 2, '.', ','); 

            $resourceTypeNumber = ResourceTypeNumber::where('service_request_id', $serviceId)->get();

            $billedTo = ClientOrder::where('company_id',$companyID)->value('companyName');
            $billedToRc = ClientOrder::where('company_id',$companyID)->value('rcNumber');

            $shippedTo = ServiceRequest::where('serviceIDNo', $serviceIDNo)->value('deliverTo');
            $shippedFrom = ServiceRequest::where('serviceIDNo', $serviceIDNo)->value('deliverFrom');
            $contactName = ServiceRequest::where('serviceIDNo', $serviceIDNo)->value('contactName');
            $createdAt = ServiceRequest::where('serviceIDNo', $serviceIDNo)->value('created_at');

            $desc = ServiceRequest::where('serviceIDNo', $serviceIDNo)->value('itemDescription');
            $wgt = ServiceRequest::where('serviceIDNo', $serviceIDNo)->value('estimatedWgt');

            $dateCreated = date('F d, Y', strtotime($createdAt));

            $invoice = Invoice::where('service_id',$serviceId)->value('invoice_id');

            $invoiceStatus = Invoice::where('service_id',$serviceId)->value('status');

            $resGPSDet = array();

            $resources = OrderResourceGpsDetail::where('service_requests_id',$serviceId)->get();

            for ($i=0; $i < count($resources); $i++) { 
                $resGPSDet[] = $resources[$i]['resource_gps_details_id'];
            }

            $finalResDets = array();

            for ($x=0; $x < count($resGPSDet); $x++) { 
                $finalResDets[] = ResourceGpsDetail::where('id',$resGPSDet[$x])->get();
            }

            return view('client.invoice.invoiceDetails',['amt'=>$amt, 'resourceTypeNumber'=>$resourceTypeNumber, 'billedTo'=>$billedTo, 'invoice'=>$invoice, 'invoiceStatus'=>$invoiceStatus, 'userDetails'=>$userDetails, 'serviceIDNo'=>$serviceIDNo, 'billedToRc'=>$billedToRc, 'contactName'=>$contactName, 'shippedTo'=>$shippedTo, 'shippedFrom'=>$shippedFrom, 'dateCreated'=>$dateCreated,'totalAmount'=>$totalAmount,'vatVal'=>$vatVal,'desc'=>$desc,'wgt'=>$wgt,'finalResDets'=>$finalResDets,'orderGPSDetails'=>$orderGPSDetails]);
    }



    public function approveInvoice($id){

        $serviceID = ServiceRequest::where('serviceIDNo',$id)->value('id');

        $invoiceID = Invoice::where('service_id', $serviceID)->value('id');


        $data = array(
            'status' => 1
        );

        Invoice::where(
            'id',$invoiceID)
            ->update($data);

        $dataStatus = array(
            'orderStatus' => 4
        );

        ServiceRequest::where(
            'id',$serviceID)
            ->update($dataStatus);

           $this->export_pdf($serviceID);

        return redirect('/viewOrders')->with('message', 'Invoice Approval Confirmed');

    }

      public function export_pdf($serviceID)
      {

        $ServiceRequest = ServiceRequest::where('id',$serviceID)->get();
        $ORDERGPS = OrderResourceGpsDetail::where('service_requests_id',$serviceID)->get();
        
        for ($i=0; $i < count($ORDERGPS); $i++) { 



            $carrierID = $ORDERGPS[$i]['carrier_id'];
            $plateNumber = $ORDERGPS[$i]['plateNumber'];
            $drivername = $ORDERGPS[$i]['driverName'];

            $carrierEmail = User::where('id',$carrierID)->value('email');


            $path = base_path('public/alo/');
            $pdf_name = $plateNumber.'.pdf';

            $fullPath = $path.$pdf_name;

            PDF::loadView('token', compact('plateNumber','drivername','ServiceRequest'))->save($path.$pdf_name);

            $data = array(
                'email_address'=> $carrierEmail,
                'subject'=>"ALO Docs for ".$plateNumber,
                'filePath'=> $fullPath,
            );

            Mail::send('emails.alo', $data, function($message) use($data) {
                $message->from('no_reply.trucka@noreply', 'ALO Document');
                $message->to($data['email_address']);
                $message->subject($data['subject']);
                //Full path with the pdf name
                $message->attach($data['filePath']);
            });
        }
        
      }

    public function UpdateResourceStat($array){
        $collect = array();
        for($i=0;$i<count($array);$i++){
            $collect[] = ResourceGpsDetail::where(
            'id',$array[$i])->value('id');
        }
        return $collect;
    }

    public function finishTransaction($id)
    {

        
        $serviceID = ServiceRequest::where('serviceIDNo', $id)->value('id');
        $orderDate = ServiceRequest::where('serviceIDNo', $id)->value('created_at');
        $weight = ServiceRequest::where('serviceIDNo', $id)->value('estimatedWgt');

        $dataStatus = array(
            'orderStatus' => 9
        );

        $array = array();
        $orderResQuery = OrderResourceGpsDetail::where('service_requests_id',$serviceID)->get();
        foreach ($orderResQuery as $query) {
            $array[] = $query->resource_gps_details_id;
        }

        $resID = $this->UpdateResourceStat($array);

        $stat = array(
            'resourceStatus' => 0
        );
        
        for($x=0;$x<count($resID);$x++){
            ResourceGpsDetail::where(
            'id',$resID[$x])
            ->update($stat);
        }

        $invoiceNo = $this->finalInvoiceNumber();

        $fileContent = InvoiceDetail::where('service_requests_id',$serviceID)->value('orderDetail');

        $decodedFileContent = json_decode($fileContent, 1);

        $amt = $decodedFileContent['totalAmount'];

        $userID = Auth::user()->id;

        DB::table("final_invoices")->insert([
            'user_id'=>$userID,
            'invoice_no'=>$invoiceNo,
            'service_id_no' => $id,
            'orderDate' => $orderDate,
            'weight' => $weight,
            'status' => 0,
            'totalAmount' => $amt,
            'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
            'updated_at' => \Carbon\Carbon::now()->toDateTimeString(),
        ]);

        ServiceRequest::where(
            'id',$serviceID)
            ->update($dataStatus);

        return back()->with('message', 'Transaction Confirmed');
    }

    public function allInvoices(){
        $userID = Auth::user()->id;
        // $companyID = UserComapany::where('user_id',$userID)->value('company_id');
        // $invoice = Invoice::where('client_id',$userID)->get();
        $invoice = FinalInvoice::where('user_id',$userID)->where('status',0)->get();
        //$UserRole = $this->getUserRole($userID);
        return view('client.finance.allFinalInvoice', ['invoice'=>$invoice]);
        //return view('finance.invoice', ['invoice'=>$invoice]);
    }

    public function viewAllInvoices($id){


            $inv = Invoice::where('invoice_id',$id)->value('service_id');

            $serviceIDNo = ServiceRequest::where('id',$inv)->value('serviceIDNo');

            $userID = Auth::user()->id;
            $companyID = UserComapany::where('user_id',$userID)->value('company_id');

            $userDetails = ClientOrder::where('company_id',$companyID)->get();

            $serviceId = ServiceRequest::where('serviceIDNo',$serviceIDNo)->value('id');/*

        $fileDirectory = public_path()."/invoiceInfoFiles/".$serviceIDNo.".json";*/


            $fileContent = InvoiceDetail::where('service_requests_id',$serviceId)->value('orderDetail');

        /*if(file_exists($fileDirectory)){

            $fileContent = File::get($fileDirectory);
*/
            $decodedFileContent = json_decode($fileContent, 1);

            $amt = number_format($decodedFileContent['amountPlusVat'], 2, '.', ',');

            $totalAmount = number_format($decodedFileContent['totalAmount'], 2, '.', ',');

            $totalAmountForVAT = $decodedFileContent['totalAmount'];

            $vat = $totalAmountForVAT * 0.05;

            $vatVal = number_format($vat, 2, '.', ','); 

            $resourceTypeNumber = ResourceTypeNumber::where('service_request_id', $serviceId)->get();

            $billedTo = ClientOrder::where('company_id',$companyID)->value('companyName');
            $billedToRc = ClientOrder::where('company_id',$companyID)->value('rcNumber');

            $shippedTo = ServiceRequest::where('serviceIDNo', $serviceIDNo)->value('deliverTo');
            $shippedFrom = ServiceRequest::where('serviceIDNo', $serviceIDNo)->value('deliverFrom');
            $contactName = ServiceRequest::where('serviceIDNo', $serviceIDNo)->value('contactName');
            $createdAt = ServiceRequest::where('serviceIDNo', $serviceIDNo)->value('created_at');

            $desc = ServiceRequest::where('serviceIDNo', $serviceIDNo)->value('itemDescription');
            $wgt = ServiceRequest::where('serviceIDNo', $serviceIDNo)->value('estimatedWgt');

            $dateCreated = date('F d, Y', strtotime($createdAt));

            $invoice = Invoice::where('service_id',$serviceId)->value('invoice_id');

            $invoiceStatus = Invoice::where('service_id',$serviceId)->value('status');

            return view('invoice.invoiceDetails',['amt'=>$amt, 'resourceTypeNumber'=>$resourceTypeNumber, 'billedTo'=>$billedTo, 'invoice'=>$invoice, 'invoiceStatus'=>$invoiceStatus, 'userDetails'=>$userDetails, 'serviceIDNo'=>$serviceIDNo, 'billedToRc'=>$billedToRc, 'contactName'=>$contactName, 'shippedTo'=>$shippedTo, 'shippedFrom'=>$shippedFrom, 'dateCreated'=>$dateCreated,'totalAmount'=>$totalAmount,'vatVal'=>$vatVal,'desc'=>$desc,'wgt'=>$wgt]);
        /*}else{
            return "Not Found";
        }*/
    }

    public function allDemurrage(){
        return view('finance.demurrage');
    }

    public function settlement(){
        return view('finance.settlement');
    }




    public function openDispute($id)
    {
        $serviceId = ServiceRequest::where('serviceIDNo',$id)->value('id');

        $resGPSDet = array();

        $resources = OrderResourceGpsDetail::where('service_requests_id',$serviceId)->get();

        for ($i=0; $i < count($resources); $i++) { 
            $resGPSDet[] = $resources[$i]['resource_gps_details_id'];
        }

        $finalResDets = array();

        for ($x=0; $x < count($resGPSDet); $x++) { 
            $finalResDets[] = ResourceGpsDetail::where('id',$resGPSDet[$x])->get();
        }

        return view('orders.dispute.openDispute',compact('id','finalResDets'));
    }

    public function downloadInvoice($invoiceID)
    {

        /*$adminFiles = Storage::get('');

        if (file_exists($adminFiles)) {
            return "ok";
        }else{
            return "not found";
        }*/
        
        $invNo = $invoiceID;
        $serviceId = FinalInvoice::where('invoice_no',$invNo)->value('service_id_no');
        
        $userID = Auth::user()->id;
            $companyID = UserComapany::where('user_id',$userID)->value('company_id');

            $userDetails = ClientOrder::where('company_id',$companyID)->get();

            $serviceIDNo = ServiceRequest::where('serviceIDNo',$serviceId)->value('id');

            /*$fileContent = InvoiceDetail::where('service_requests_id',$serviceId)->value('orderDetail');

            $decodedFileContent = json_decode($fileContent, 1);*/
            $totalAmount = FinalInvoice::where('invoice_no',$invNo)->value('totalAmount');

            $vat = $totalAmount * 0.05;

            $amtPlusVat = $vat + $totalAmount;


            $amt = number_format($amtPlusVat, 2, '.', ',');

            $totalAmount = number_format($totalAmount, 2, '.', ',');

            /*$totalAmountForVAT = $totalAmount;

            $vat = $totalAmountForVAT * 0.05;*/

            $vatVal = number_format($vat, 2, '.', ','); 

            $resourceTypeNumber = ResourceTypeNumber::where('service_request_id', $serviceIDNo)->get();

            $billedTo = ClientOrder::where('company_id',$companyID)->value('companyName');
            $billedToRc = ClientOrder::where('company_id',$companyID)->value('rcNumber');

            $shippedTo = ServiceRequest::where('id', $serviceIDNo)->value('deliverTo');
            $shippedFrom = ServiceRequest::where('id', $serviceIDNo)->value('deliverFrom');
            $contactName = ServiceRequest::where('id', $serviceIDNo)->value('contactName');
            $createdAt = ServiceRequest::where('id', $serviceIDNo)->value('created_at');

            $desc = ServiceRequest::where('id', $serviceIDNo)->value('itemDescription');
            $wgt = ServiceRequest::where('id', $serviceIDNo)->value('estimatedWgt');

            $dateCreated = date('F d, Y', strtotime($createdAt));

            $invoice = FinalInvoice::where('service_id_no',$serviceId)->value('invoice_no');

            $resGPSDet = array();

            $resources = OrderResourceGpsDetail::where('service_requests_id',$serviceIDNo)->get();

            for ($i=0; $i < count($resources); $i++) { 
                $resGPSDet[] = $resources[$i];
            }

            $path = base_path('public/invoices/');
            $pdf_name = $invoice.'.pdf';

            $fullPath = $path.$pdf_name;

            $query = OrderDocumentDestination::where('service_id',$serviceIDNo)->get();

           for($m=0;$m<count($query);$m++){

            $files = Storage::disk('dropbox')->get($query[$m]['fileName']);

            $str = $query[$m]['fileName'];

            $cleanfile = trim(explode('/', $str)[1]);

            Storage::disk('public_uploads')->put($cleanfile, $files);
            
           }

            $description = FinalInvoice::where('service_id_no',$serviceId)->value('description');

            $pdf = PDF::loadView('finance.finalInvoicePDF', compact('amt', 'resourceTypeNumber', 'billedTo', 'invoice', 'userDetails', 'serviceId', 'billedToRc', 'contactName', 'shippedTo', 'shippedFrom', 'dateCreated','totalAmount','vatVal','desc','wgt','resGPSDet','description'))->save($path.$pdf_name); 

            $waybillPath = base_path('/public/destDocs/');
            $public_dir=public_path();

            $zipper = new \Chumper\Zipper\Zipper;

            $arr = array();

            for ($i=0; $i < count($query); $i++) { 
                $str = $query[$i]['fileName'];

                $cleanfile = trim(explode('/', $str)[1]);
              $arr[] = $waybillPath.$cleanfile;
            }
                $arr[] = $fullPath;

                $zip_filename = $invoice.'.zip';

                $zipper->make(public_path().'/zips/'.$zip_filename)->add($arr);
                $zipper->close();

                $pathToFile = public_path().'/zips/'.$invoice.'.zip';

                // Set Header
                $headers = array(
                    'Content-Type' => 'application/octet-stream',
                );
                $filetopath=$public_dir.'/'.$zip_filename;
                // Create Download Response
                if(file_exists($filetopath)){
                    return response()->download($filetopath,$zip_filename,$headers);
                }

                return response()->download($pathToFile);

                return response()->download(public_path('zips/'.$invoice.'.zip'));

                // if (!file_exists(base_path('/public/zips/'.$invoice.'.zip'))) {

                // return response()->download(base_path('/public/zips/'.$invoice.'.zip'));
                // }else{
                    
                // return response()->download(base_path('/public/zips/'.$invoice.'.zip'));
                // }


    } 

    public function cancelOrder($orderNo){
   $userId        = Auth::user()->id;
   $baseUrl       = url('/');  
   $edtiRequest   = ServiceRequest::where('createdBy', $userId)
                                  ->where ('serviceIDNo', $orderNo)
                                  ->update(['orderStatus' => -1]);
       return redirect($baseUrl."/viewOrders");
  }

  public function viewResourceDetails($id)
  {
      $serviceId = ServiceRequest::where('serviceIDNo',$id)->value('id');

      $finalResDets = DB::table('order_resources')
                        ->where('order_id',$serviceId)
                        ->join('carrier_resource_details', 'carrier_resource_details.id', '=', 'order_resources.resource_id')
                        ->get();
  }

  public function showInvoicesSupervisor()
    {
        $userID = Auth::user()->id;
        $companyID = UserComapany::where('user_id',$userID)->value('company_id');
        $invoice =   simpleInvoice::where('company_id',$companyID)
                        ->where('status',0)->get();
        
        return view('client.finance.allFinalInvoice',compact('invoice'));
    }


    public function viewInvoiceDet($invoiceNo)
    {
        $invNo                      = $invoiceNo;

        $userID                     =   Auth::user()->id;
        $companyID                  =   UserComapany::where('user_id',$userID)->value('company_id');

        $companyDetail              =   Company::where('id',$companyID)->get()[0];

        $invoiceDetails             =   simpleInvoice::where('invoiceNo',$invNo)->get();

        $price                      =   simpleInvoice::where('invoiceNo',$invNo)->value('price');

        $vat                        =   0.05 * $price;

        $pricePlusVat               =   $price + $vat;

        $resDets                    =   array();
        $resDetsResult              =   array();

        for ($i=0; $i < count($invoiceDetails); $i++) {

            $resDets[]              =   $invoiceDetails[$i]['resourceDetails'];
            $resDetsResult[]        =   json_decode($resDets[$i],true);

        }


        $totalInWords              =   $this->currencyIntegerToWord(round( $pricePlusVat, 2, PHP_ROUND_HALF_UP));

        return view('client.finance.invoice_new',compact('resDetsResult','invoiceDetails','companyDetail','vat','pricePlusVat','totalInWords'));
        
        
    }


    public function currencyIntegerToWord($integer){
        

        $number                    =   new \NumberFormatter("en", \NumberFormatter::SPELLOUT);
        $kobo                      =   '';
       //Explode integer to seprate the Naira and the kobo 
        $explodedInteger           =   explode('.', $integer);
        //Convert Naira to words
        $naira                     =   $number->format($explodedInteger[0]).' Naira, ';
        //convert Kobo to words
        if (isset($explodedInteger[1])) {
            $kobo                      =   $number->format($explodedInteger[1]).' Kobo';
        }
        $totalInWords              =   $naira.$kobo;

        return $totalInWords;

    }

    public function createConsolInvoice(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'invoice_id.*'=>'required'
        ]);

        $invoiceID  =   $request->invoice_id;

        $invoiceIDCount  =   count($invoiceID);

        $waybills        =    array();

        $resourceDetails =    array();

        $price           =     array();

        $order_id        =      array();

        if ($validator->passes()) {

            /*if ($invoiceIDCount > 1) {*/

                $result     =   array();

                for ($i=0; $i < $invoiceIDCount; $i++) { 
                    
                    $result[]           =   simpleInvoice::where('id',$invoiceID[$i])->get();

                    $waybills[]         =   $result[$i][0]['waybillFiles'];

                    $price[]            =   $result[$i][0]['price'];  

                    $resourceDetails[]  =   $result[$i][0]['resourceDetails'];

                    $order_id[]           =  $result[$i][0]['order_ids'];

                    $data = array(
                        'status' => 1
                    );

                    simpleInvoice::where('id',$invoiceID[$i])
                                    ->update($data);

                }


                $userID                     = Auth::user()->id;
                
                $companyID                  = UserComapany::where('user_id',$userID)->value('company_id');

                $finalPrice                 =   array_sum($price);

                $invoice                    =  new ConsolidatedInvoice;

                $invoice->waybillFiles      =       json_encode($waybills);
                $invoice->company_id        =       $companyID;
                $invoice->invoiceNo         =       $this->finalInvoiceNumber();
                $invoice->resourceDetails   =       json_encode($resourceDetails);
                $invoice->price             =       $finalPrice;
                $invoice->order_ids         =       json_encode($order_id);

                $invoice->save();

                return back();

            /*}elseif($invoiceIDCount <= 1){
                return "okay";
            }*/

        }

    }


    public function invoicePayments()
    {
        $userID         = Auth::user()->id;
                
        $companyID      = UserComapany::where('user_id',$userID)->value('company_id');

        $payments    =   ConsolidatedInvoice::where('company_id',$companyID)->get();

        return view('client.finance.unpaidInvoices',compact('payments'));

    }

    public function viewinvoicePayments($invoiceNo)
    {


        $invNo                      = $invoiceNo;

        $userID                     =   Auth::user()->id;
        $companyID                  =   UserComapany::where('user_id',$userID)->value('company_id');

        $companyDetail              =   Company::where('id',$companyID)->get()[0];

        $invoiceDetails             =   ConsolidatedInvoice::where('invoiceNo',$invNo)->get();

        $price                      =   ConsolidatedInvoice::where('invoiceNo',$invNo)->value('price');

        $vat                        =   0.05 * $price;

        $pricePlusVat               =   $price + $vat;

        $resDets                    =   array();
        $resDetsResult              =   array();

        for ($i=0; $i < count($invoiceDetails); $i++) { 
            $in     =   json_decode($invoiceDetails[$i]['resourceDetails']);

            for ($x=0; $x < count($in); $x++) { 
            $finalResDetails    =    json_decode($in[$x],true);
            }
        }


        $totalInWords              =   $this->currencyIntegerToWord(round( $pricePlusVat, 2, PHP_ROUND_HALF_UP));

        // var_dump($finalResDetails);
        // exit;
       
        return view('client.finance.invoice_payments',compact('finalResDetails','invoiceDetails','companyDetail','vat','pricePlusVat','totalInWords','resDetsResult'));
        
        
    }

    public function downloadWaybills($invoiceNo)
    {
        $invoiceNumber  =   $invoiceNo;

        $zipFileName    =   $invoiceNumber.'.zip';

        $public_dir     =    public_path().'/ways';

        $filetopath     =    $public_dir.'/'.$zipFileName;

        //check if file already exists


        if (file_exists($filetopath)) {


            // Set Header
            $headers = array(
                'Content-Type' => 'application/octet-stream',
            );

            $zipFileName    =   $invoiceNumber.'.zip';

            $public_dir     =    public_path().'/ways';

            $filetopath     =    $public_dir.'/'.$zipFileName;

            
            return response()->download($filetopath,$zipFileName,$headers);
            
            

        }else{

            $firstJson = json_decode(ConsolidatedInvoice::where('invoiceNo',$invoiceNumber)->value('waybillFiles'),true);

            for ($i=0; $i < count($firstJson); $i++) { 
                $waybills   =   json_decode($firstJson[$i],true);
            }


            $public_dir     =    public_path().'/ways';
            // Zip File Name
            $zipFileName = $invoiceNumber.'.zip';

            // Create ZipArchive Obj
            $zip = new ZipArchive;

            $base_path =    base_path('storage\app\\');


            if ($zip->open($public_dir . '/' . $zipFileName, ZipArchive::CREATE) === TRUE) {
                // Add File in ZipArchive
                for ($i=0; $i < count($waybills); $i++) {

                    $zip->addFile( $base_path.$waybills[$i] , trim( explode('/', $waybills[$i])[1] ) ) ;

                } 

                // Close ZipArchive     
                $zip->close();
            }

            // Set Header
            $headers = array(
                'Content-Type' => 'application/octet-stream',
            );

            $filetopath = $public_dir.'/'.$zipFileName;
            // Create Download Response
            if(file_exists($filetopath)){
                return response()->download($filetopath,$zipFileName,$headers);
            }

        }



    }



    
    
}
