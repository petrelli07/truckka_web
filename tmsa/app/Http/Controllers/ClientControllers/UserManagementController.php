<?php

namespace App\Http\Controllers\ClientControllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use App\User;
use Validator;
use App\Notifications\newClientAccount;
use \App\Http\Controllers\AdminControllers\UserManagementController  as Admin;


class UserManagementController extends Controller{
    // private $companyID     =    session('companyId');
    public function __construct(){
        $this->middleware('auth');
    }
    public function getRoles(int $userID,$useStrict = false){
      if ($useStrict) {
        $getRoles                 =   DB::table('roles')
                                          ->where('is_default', '=', 'false')
                                          ->orwhere('created_by', '=', $userID)
                                          ->get();
      }else{
        $getRoles                 =   DB::table('roles')
                                          ->where('is_default', '=', 'true')
                                          ->orwhere('created_by', '=', $userID)
                                          ->get();
      }
      return $getRoles;
    }
    public function isUserRole($roleId){
      $userID                   =   Auth::user()->id;
      $check                    =   DB::table('roles')
                                        ->where('id', '=', $roleId)
                                        ->where('created_by', '=', $userID)
                                        ->get();
      if (sizeof($check) == 0) {
        return false;
      }
        return true;
    }

    public function getSingleRole($roleId){
      $userID                   =   Auth::user()->id;
      $detials                  =   DB::table('roles')
                                        ->where('id', '=', $roleId)
                                        ->where('created_by', '=', $userID)
                                        ->get();
    
      return  $detials;
    }
    public function updateRole($roleId,$roleName){
      $userID                   =   Auth::user()->id;
      $data                     =   array('role_name'=> $roleName);
      $detials                  =   DB::table('roles')
                                        ->where('id', '=', $roleId)
                                        ->where('created_by', '=', $userID)
                                        ->update($data);
    }
//////////////////////////////User Management Views/////////////////////////////////////////////////////////////////
    
    public function createNewUserView(){
      $userID                   =   Auth::user()->id;

      $getRoles                 =   $this->getRoles($userID);

      $data                     =   array('roles' => $getRoles); 

      return view('client.userManagement.createNewUser',$data);
    }

    public function allUsersView(){
      $userType     =   1;
      $allUsers     =   $this->getAllUsers(1);
      $data         =   array('users' => $allUsers);
      return view('client.userManagement.allUsers',$data);
    }

    public function edtUserPermissionView($editUserId){
      $companyID            =   session('companyId');
      $userType             =   1;
      $userID               =   Auth::user()->id;
      $companyModules       =   $this->getCompanysModules($companyID, $userType);
      $userModules          =   $this->getUserModules($editUserId,$companyID);
      $userModules          =   array_column((array)$userModules->toArray(), 'module_id');
      $getRoles             =   $this->getRoles($userID);
      $userDetails          =   $this->checkUserAuth($editUserId, $userType);
      ///////////////////////Security Check//////////////////////////////////////////////////////////
      $getSingleUser        =   $this->checkUserAuth($editUserId,$userType);
       if (!sizeof($getSingleUser) == 1) {
          return redirect('/unauthorized');
      }
      ///////////////////////////////////////////////////////////////////////////////////////////////
      $data                 =   array('companyModules' => $companyModules,
                                      'getUserModules' => $userModules,
                                      'roles'          => $getRoles,
                                      'user'           => $userDetails
                                      );
      return view('client.userManagement.editUserPermission',$data);

    }

    public function newRoleView(){
      $userType             =   1;
      $companyID            =   session('companyId');
      $companyModules       =   $this->getCompanysModules($companyID, $userType);
      $data                 =   array('companyModules' => $companyModules);
      return view('client.userManagement.createRole',$data);
     
    }
    public function allRolesView(){
      $userID     = Auth::user()->id;
      $useStrict  = true;
      $userRoles  = $this->getRoles($userID, $useStrict);
      $data       = array('userRoles' => $userRoles);
      return view('client.userManagement.allRoles',$data);
    }
    public function editRoleView($roleId){
      ///////////////////////Security Check to make Sure the User can only edit his own role//////////////////////////////
      if (!$this->isUserRole($roleId)) {
        return redirect('/unauthorized');
      }
      ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
      $userType             =   1;
      $companyID            =   session('companyId');
      $companyModules       =   $this->getCompanysModules($companyID, $userType);
      $getRolesPermission   =   $this->getRolesPermission($roleId,true)->toArray();
      $permissionModules    =   array_column($getRolesPermission, 'permission_id');
      $roleName             =   array_column($this->getSingleRole($roleId)->toArray(), 'role_name')[0];
      $data                 =   array('companyModules'    => $companyModules,
                                      'permissionModules' => $permissionModules,
                                      'roleName'          => $roleName,
                                      'roleId'            => $roleId
                                      );

      return view('client.userManagement.createRole',$data);

    }
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function jsonRetrieveModules (Request $request){

      $companyId          = session('companyId');
      $roleId             = $request->roleId;
      $userType           = 1;
      $companyModules     = $this->getCompanysModules($companyId, $userType,array('moduleName','moduleDescription','modules.id'));
      $permissionModules  = array_column($this->getRolesPermission($roleId)->toArray(), 'permission_id');
      $data               = array("permissionModules" => $permissionModules,
                                    "companyModules"    => $companyModules);
      return response()->json($data);

    }

    public function saveNewRole(Request $request){
      $validator             =    Validator::make($request->all(),[
            'name'           =>   'required',
            ]);
      if ($validator->passes()) {
        
        DB::transaction(function() use($request){
          $roleName             = $request->name;
          $permissions          = $request->modules;
          $userId               = Auth::user()->id;
          $is_default           = 'false';
          $roleName             = $request->name;
          $permissions          = $request->modules;
          $userId               = Auth::user()->id;
          $userType             = 1;
          $newRoleId            = $this->createNewRole($userId, $roleName, $userType, $is_default);
          $saveRolePermission   = $this->saveRolePermission($newRoleId, $permissions);
        },2);
      }

    }

    public function editRole(Request $request){
      $validator             =    Validator::make($request->all(),[
            'name'           =>   'required',
            ]);
      if ($validator->passes()) {
        DB::transaction(function() use($request){
          $roleName             = $request->name;
          $permissions          = $request->modules;
          $userId               = Auth::user()->id;
          $roleId               = $request->id;
          $is_default           = 'false';
          $roleName             = $request->name;
          $permissions          = $request->modules;
          $userId               = Auth::user()->id;
          $updateRole           = $this->updateRole($roleId, $roleName);
          $saveRolePermission   = $this->updateRolePermission($roleId, $permissions);
        },2);
      }
    }
    public function  newUserCompany(int $userId, int $companyId,int $userType){
    
     switch ($userType) {
      case 1:
        $table = 'user_comapanies';
        break;
       case 3:
        $table = 'user_comapanies';
        break;
      case 2:
        $table = 'carrier_users';
        break;
      }

      $data       =   array('user_id'=>$userId,'company_id'=>$companyId);
      $save       =   DB::table($table)
                        ->insert($data);
    }

    public function updateRolePermission($roleId,$permissions){
      $data                     = array();
      $userId                   = Auth::user()->id;

      $deletePermission         = DB::table('roles_permission')
                                    ->join('roles','roles.id','=','roles_permission.role_id')
                                    ->where('role_id', '=', $roleId)
                                    ->where('created_by','=',$userId)
                                    ->delete();

      for ($i=0; $i < sizeof($permissions) ; $i++) { 
            array_push($data, array( 'role_id'         => $roleId,
                                     'permission_id'   => $permissions[$i]
                                  ));
          }

      $saveRolePermission       =   DB::table('roles_permission')
                                      ->insert($data);
      return $saveRolePermission;
    }
    public function saveRolePermission($roleId,$permissions){
      
      $data   = array();

      for ($i=0; $i < sizeof($permissions) ; $i++) { 
            array_push($data, array( 'role_id'         => $roleId,
                                     'permission_id'   => $permissions[$i]
                                  ));
          }

      $saveRolePermission       =   DB::table('roles_permission')
                                      ->insert($data);
      return $saveRolePermission;
    }

    public function createNewRole(int $userId, $roleName,$userType, $is_default){
      $roleSettings             =   array('created_by' => $userId,
                                          'role_name'  => $roleName,
                                          'is_default' => $is_default,
                                          'user_type'  => $userType
                                        );
      $saveNewRole              =   DB::table('roles')
                                      ->insertGetId($roleSettings);
      return $saveNewRole;
    }
    public function getUserModules($userId,$companyId){

     $userModules               =   DB::table('user_permissions')
                                      ->join('user_comapanies', 'user_comapanies.user_id', '=','user_permissions.user_id')
                                      ->where('user_permissions.user_id',$userId)
                                      ->where('user_comapanies.company_id','=',$companyId)
                                      ->select('module_id')
                                      ->get();
      return $userModules;
    }
    public function getCompanysModulesList(int $companyId, int $userType){
      
      switch ($userType) {
        case 1:
          $table = 'client_company_modules';
          break;
        case 2:
          $table = 'carrier_company_modules';
          break;
      }

      $companyPermission        = DB::table($table)
                                    ->where('company_id', '=', $companyId)
                                    ->select('module_id')
                                    ->get();
      return $companyPermission->toArray();
    }
    public function getRolesPermission($roleId,$useAuth = false){
      
      $getRolesPermission       =   DB::table('roles_permission')
                                              ->join('roles','roles.id','=','roles_permission.role_id')
                                              ->join('modules','modules.id','=','roles_permission.permission_id')
                                              ->where('role_id', '=', $roleId);
                                              
      if ($useAuth) {
        $userId                 =   Auth::user()->id;
        $getRolesPermission     =   $getRolesPermission->where('created_by',$userId);
      }

      $getRolesPermission = $getRolesPermission->get();
      
      return $getRolesPermission;
    }
    public function getRolesPermissionList(int $roleId){
     
      $getRolesPermissionList       =   DB::table('roles_permission')
                                              ->where('role_id', '=', $roleId)
                                              ->select('permission_id')
                                              ->get();
      return $getRolesPermissionList;
    }

    public function insertPermission(int $userId,  $permissions){

      $deletePermission         = DB::table('user_permissions')
                                    ->where('user_id', '=', $userId)
                                    ->delete();

      $data                     = array();

      if (is_object($permissions)) {

        foreach ($permissions as $permission ) {
/*<<<<<<< HEAD
          // var_dump($permission);
=======
>>>>>>> 2caab16aebaa458e0c663e2f7fc020c65e2083ec*/
         array_push($data, array( 'user_id'    => $userId,
                                  'module_id'  => $permission->permission_id
                                ));
        }
      }else{
          
          for ($i=0; $i < sizeof($permissions) ; $i++) { 

            array_push($data, array('user_id'     => $userId,
                                    'module_id'   => $permissions[$i]->module_id
                                  ));
          }
        }

      $updateNewPermission      = DB::table('user_permissions')
                                    ->insert($data);

      return $updateNewPermission;
    }

    public function getCompanysModules($companyId,$userType,array $select = Null){
      switch ($userType) {
        case 1:
          $table = 'client_company_modules';
          break;
        case 2:
          $table = 'carrier_company_modules';
          break;
      }
      if (is_null($select)) {

        $companyPermission        = DB::table($table)
                                      ->where('company_id', '=', $companyId)
                                      ->join('modules','modules.id','=',$table.'.module_id')
                                      ->get();
      }else{
        $companyPermission        = DB::table($table)
                                      ->where('company_id', '=', $companyId)
                                      ->join('modules','modules.id','=',$table.'.module_id');
        for ($i=0; $i < sizeof($select); $i++) { 
                                      $companyPermission->addselect($select[$i]);
        }
            $companyPermission    =   $companyPermission->get();  
      }
          return $companyPermission->toArray();
      }
    
    public function createNewUser(Request $request){
        
        $validator             =    Validator::make($request->all(),[
            'email'            =>   'required|unique:users',
            'name'             =>   'required',
            'role'             =>   'required'
            ]);

        if ($validator->passes()) {

//////////////////Start Database Transaction/////////////////////////////////////////////

            DB::transaction(function() use($request){
                
                $admin                 =  new Admin();
                $companyID             =   session('companyId');
                $userType              =   1;
                $email                 =   $request->email;
                $agentName             =   $request->name;
                $defaultPassword       =   $admin->randomNumberForPassword();
                $roleId                =   $request->role;
                $bcryptPass            =   bcrypt($defaultPassword); 

//////////////////////////Create New User///////////////////////////////////////////////
                
                $insertUserDetailsID    = DB::table("users")->insertGetId([
                    'name'              => $agentName,
                    'email'             => $email,
                    'password'          => $bcryptPass,
                    'default_password'  => $bcryptPass,
                    'userAccessLevel'   => 1,
                    'created_at'        => \Carbon\Carbon::now()->toDateTimeString(),
                    'updated_at'        => \Carbon\Carbon::now()->toDateTimeString(),
                ]);


/////////////////Get Modules Based on The User Type  (unique user is 0) ////////////////////////////
               
                switch ($roleId) {
                  case 0:
                    $permissions        = $this->getCompanysModulesList($companyID, $userType);
                    break;
                  
                  default:
                    $permissions        = $this->getRolesPermissionList($roleId);
                    break;
                }

                

////////////////Insert Modules into user's permission table/////////////////////////////////////////////////
               

                $insertUserRoles        = $this->insertPermission($insertUserDetailsID, $permissions);

/////////////////Add user to Appropriate Company Table/////////////////////////////////////////////////////////////

                $insertUserCompany      = $this->newUserCompany($insertUserDetailsID, $companyID,$userType);

////////////////Send Mail to the User/////////////////////////////////////////////////////////////
               

                $user = User::find($insertUserDetailsID);
                $notification = $user->notify(new newClientAccount($defaultPassword));
                
            },2);

            return response()->json(['success'=>'New Agent Created Successfully']);
        }else{
            return response()->json(['error'=>$validator->errors()->all()]);
        }
    }

  public function changeUserPermission(Request $request){
   $userId                =   Auth::user()->id; 
   $companyId             =   session('companyId');
   $userType              =   1;
   $userPermissionId      =   $request->id;
   if (is_array($request->modules)) {
    $newModules           =   $request->modules;
   }else{
    $newModules           =   array();
   }
   /////////////////////////////Security Check To make sure the user has all the permissions he wants to insert/////////////////////////
   $getCompanysModules    =   $this->getCompanysModulesList($companyId, $userType);
   $filteredModules       =   array_intersect($newModules,array_column($getCompanysModules->toArray(), 'module_id'));
   /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   ////////////////////////////Security Check To make Sure The User Is In the Company///////////////////////////////////////////////////
   $getSingleUser         =   $this->checkUserAuth($userPermissionId,$userType);
   if (sizeof($getSingleUser) == 1) {
    $insertPermission     =   $this->insertPermission($userPermissionId, $filteredModules); 
   }
   ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  }

  public function checkUserAuth($userId,$userType){
    switch ($userType) {
        case 1:
          $table = 'user_comapanies';
          break;
        case 2:
          $table = 'carrier_users';
          break;
    }

      $companyID            =   session('companyId');
      $userID               =   Auth::user()->id;
      $getUsers             =   DB::table($table)
                                  ->join('users','users.id','=',$table.'.user_id')
                                  ->where($table.'.company_id','=',$companyID)
                                  ->where($table.'.user_id','=',$userId)
                                  ->get();
    return $getUsers;
  }

  public function getAllUsers(int $userType){
    
    switch ($userType) {
        case 1:
          $table = 'user_comapanies';
          break;
        case 2:
          $table = 'carrier_users';
          break;
    }

    $companyID            =   session('companyId');
    $userID               =   Auth::user()->id;
    $getUsers             =   DB::table($table)
                                ->join('users','users.id','=',$table.'.user_id')
                                // ->join('user_permissions','user_permissions.user_id','=','users.id')
                                // ->join('modules','modules.id','=','user_permissions.module_id')
                                ->join('status','status.id', '=', 'users.status')
                                ->where($table.'.company_id','=',$companyID)
                                // ->select('*',DB::raw('(select Group_concat( moduleName) ) as permissions'))
                                // ->groupBy('moduleName')    
                                ->get();
  return $getUsers;
  }

  
}
