<?php

namespace App\Http\Controllers\ClientControllers;

use App\Http\Controllers\Controller;
use App\CarrierDetail;
use App\HaulageResourceRequest;
use App\User;
use Illuminate\Http\Request;
use App\serviceRequest; 
use App\ResourceGpsDetail; 
use App\OrderResourceGpsDetail; 
use Auth;
use App\OrderDocument;
use App\OrderDocDest;
use App\InvoiceDetail;
use App\CarrierWallet;
use App\FinalInvoice;
use Validator;
use DB;

class OrderDocumentationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
  /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

	public function __construct()
    {
        $this->middleware('auth');
    }

    public function finalInvoiceNumber(){

        $i = 1;

        for($j=0; $j < $i; $j++){
//            return random_int(1,10000);

            $result = '';

            $length = 7;

            for($x = 0; $x < $length; $x++) {
                $result .= mt_rand(0, 9);
            }

            $checkAvailable = FinalInvoice::where('invoice_no', $result);

            if( $checkAvailable->count() < 1 ){

                return $result;

                break;
            }

            $i++;

        }
    }

    public function allOrders(){
    	$createdByID = Auth::user()->id;
        $allRequests = serviceRequest::where('createdBy',$createdByID)->paginate(10);
        $countRequests = serviceRequest::where('createdBy',$createdByID)->count();
        return view('documents.allOrdersDocs',['allRequests'=>$allRequests,'countRequests'=>$countRequests]);
    }

    public function getResourceDetails($res){
    	$resourceDetails = array();
    	for($i=0; $i<count($res); $i++){
    		$resourceDetails[] = ResourceGpsDetail::where('id',$res[$i])->get();
    	}
    	return $resourceDetails;
    }

    public function viewOrderResources($serviceIDNo){
    	$service_id = serviceRequest::where('serviceIDNo', $serviceIDNo)->value('id'); 
    	$orderResources =  OrderResourceGpsDetail::where('service_requests_id', $service_id)->get();
    	$res = array();
    	foreach ($orderResources as $order) {
    		$res[] =$order->resource_gps_details_id;
    	}
    	$resourceDetail = $this->getResourceDetails($res);
    	return view('documents.OrderResources', ['resourceDetail'=>$resourceDetail, 'service_id'=>$service_id]);
    }

    public function viewOrderResourcesDest($serviceIDNo){
    	$service_id = serviceRequest::where('serviceIDNo', $serviceIDNo)->value('id'); 
    	$orderResources =  OrderResourceGpsDetail::where('service_requests_id', $service_id)->get();
    	$res = array();
    	foreach ($orderResources as $order) {
    		$res[] =$order->resource_gps_details_id;
    	}
    	$resourceDetail = $this->getResourceDetails($res);
    	return view('documents.OrderResourcesDest', ['resourceDetail'=>$resourceDetail, 'service_id'=>$service_id]);
    }



    public function UpdateResourceStat($array){
        $collect = array();
        for($i=0;$i<count($array);$i++){
            $collect[] = ResourceGpsDetail::where(
            'id',$array[$i])->value('id');
        }
        return $collect;
    }

    public function getCarrierID($service_id){
        $carriers = HaulageResourceRequest::where('service_request_id',$service_id)->get();
        $arr = array();
        for($i=0;$i<count($carriers);$i++){
            $arr[] = $carriers[$i]['carrier_id'];
        }
        return $arr;
    }

    public function saveOrderDocuments(Request $request){
    	
    	$orderDocument = $request->file('orderDocument');
    	$service_id = $request->service_id;
    	$timeIn = $request->timeIn;
        $timeOut = $request->timeOut;
    	$atlNo = $request->atlNo;
    	$cargoWeight = $request->cargoWeight;
    	$cargoDescription = $request->cargoDescription;
    	$comments = $request->comments;
    	$gpsid = $request->gpsid;
    	$user_id = Auth::user()->id;

    	if($request->hasFile('orderDocument')){

    		$validator = Validator::make($request->all(), [
	            'timeIn.*'=>'required',
	            'timeOut.*'=>'required',
	            'cargoWeight.*'=>'required',
	            'cargoDescription.*'=>'required',
                'gpsid.*'=>'required',
	            'atlNo.*'=>'required',
	            'orderDocument.*'=>'required'
        	]);

        	if($validator->passes()){
                
		    	for($i=0; $i<count($orderDocument); $i++){
		    		$path = $request->file('orderDocument')[$i]->store('ordersDocs');
		    		$orderDoc = new OrderDocument;
		    		$orderDoc->service_id = $service_id;
		    		$orderDoc->user_id = $user_id;
		    		$orderDoc->fileName = $path;
                    $orderDoc->timeIn = $timeIn[$i];
		    		$orderDoc->atlNo = $atlNo[$i];
		    		$orderDoc->timeOut = $timeOut[$i];
		    		$orderDoc->cargoWeight = $cargoWeight[$i];
		    		$orderDoc->cargoDescription = $cargoDescription[$i];
		    		$orderDoc->comments = $comments[$i];
		    		$orderDoc->GPSID = $gpsid[$i];
		    		$orderDoc->mode = 0;
		    		$saveUpload = $orderDoc->save();
		    	}

                $paymentType = serviceRequest::where('id',$service_id)->value('paymentType');
                $serviceIDNo = serviceRequest::where('id',$service_id)->value('serviceIDNo');
                $weight = serviceRequest::where('id',$service_id)->value('estimatedWgt');
                $orderDate = serviceRequest::where('id',$service_id)->value('created_at');

	    	
		    	if ($saveUpload) {
		    	
                    $dataStatus = array(
                        'orderStatus' => 9
                    );                   

                    serviceRequest::where(
                        'id',$service_id)
                        ->update($dataStatus);


                        $invoiceNo = $this->finalInvoiceNumber();

                        $fileContent = InvoiceDetail::where('service_requests_id',          $service_id)->value('orderDetail');

                        $decodedFileContent = json_decode($fileContent, 1);

                        $amt = $decodedFileContent['upfrontAmt'];

                        $userID = Auth::user()->id;

                        DB::table("final_invoices")->insert([
                            'user_id'=>$userID,
                            'invoice_no'=>$invoiceNo,
                            'service_id_no' => $serviceIDNo,
                            'orderDate' => $orderDate,
                            'weight' => $weight,
                            'status' => 0,
                            'description' =>  "Part Payment for Order No:".$serviceIDNo.".",
                            'totalAmount' => $amt,
                            'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
                            'updated_at' => \Carbon\Carbon::now()->toDateTimeString(),
                        ]);

                    if($paymentType == 0){

                        $carriers = $this->getCarrierID($service_id);

                        for($z=0;$z<count($carriers);$z++){
                            $setType = CarrierDetail::where('id',$carriers[$z])->value('settlementType');
                            $curBalWallet = CarrierWallet::where('user_id',$carriers[$z])->value('balance');
                            $finBal = $amt + $curBalWallet;
                            if($setType == 0){
                                $cwallet = new CarrierWallet;
                                $cwallet->user_id = $carriers[$z];
                                $cwallet->created_by_user_id = 1;
                                $cwallet->balance = $finBal;
                                $cwallet->invoice_id = $this->finalInvoiceNumber();
                                $cwallet->description = "Part Payment for Order No:".$serviceIDNo.".";
                                $cwallet->save();
                            }
                        }

                    }

                    /*$array = array();
                    $orderResQuery = OrderResourceGpsDetail::where('service_requests_id',$service_id)->get();
                    foreach ($orderResQuery as $query) {
                        $array[] = $query->resource_gps_details_id;
                    }

                    $resID = $this->UpdateResourceStat($array);

                    $stat = array(
                        'resourceStatus' => 0
                    );
                    
                    for($x=0;$x<count($resID);$x++){
                        ResourceGpsDetail::where(
                        'id',$resID[$x])
                        ->update($stat);
                    }*/

		    		return response()->json(['success'=>"Upload Successful"]);
		    	}else{
		    		return response()->json(['error'=>"Something went wrong with the upload"]);
		    	}

        	}else{
        		return response()->json(['error'=>$validator->errors()->all()]);
        	}

	    }else{
	    		return response()->json(['error'=>"Please Upload a file for each order"]);

	    }       
    }

    public function saveOrderDocumentsDest(Request $request){
    	
    	$orderDocument = $request->file('orderDocument');
    	$service_id = $request->service_id;
    	$timeIn = $request->timeIn;
    	$timeOut = $request->timeOut;
    	$cargoWeight = $request->cargoWeight;
    	$cargoDescription = $request->cargoDescription;
    	$comments = $request->comments;
    	$gpsid = $request->gpsid;
    	$user_id = Auth::user()->id;

    	if($request->hasFile('orderDocument')){

    		$validator = Validator::make($request->all(), [
	            'timeIn.*'=>'required',
	            'timeOut.*'=>'required',
	            'cargoWeight.*'=>'required',
	            'cargoDescription.*'=>'required',
	            'gpsid.*'=>'required',
	            'orderDocument.*'=>'required|file|mimes:jpeg,jpg,pdf'
        	]);

        	if($validator->passes()){


		    	for($i=0; $i<count($orderDocument); $i++){
		    		$path = $request->file('orderDocument')[$i]->store('ordersDocumentDest');
		    		$orderDoc = new OrderDocDest;
		    		$orderDoc->service_id = $service_id;
		    		$orderDoc->user_id = $user_id;
		    		$orderDoc->fileName = $path;
		    		$orderDoc->timeIn = $timeIn[$i];
		    		$orderDoc->timeOut = $timeOut[$i];
		    		$orderDoc->cargoWeight = $cargoWeight[$i];
		    		$orderDoc->cargoDescription = $cargoDescription[$i];
		    		$orderDoc->comments = $comments[$i];
		    		$orderDoc->GPSID = $gpsid[$i];
		    		$orderDoc->mode = 0;
		    		$saveUpload = $orderDoc->save();
		    	}
	    	
		    	if ($saveUpload) {
		    	$data = array(
		            'orderStatus' => 7
		        );

		        serviceRequest::where(
		            'id',$service_id)
		            ->update($data);

		    		return response()->json(['success'=>"Upload Successful"]);
		    	}else{
		    		return response()->json(['error'=>"Something went wrong with the upload"]);
		    	}

        	}else{
        		return response()->json(['error'=>$validator->errors()->all()]);
        	}

	    }else{
	    		return response()->json(['error'=>"Please Upload a file for each order"]);

	    }

        
    }

}
