<?php

namespace App\Http\Controllers\ClientControllers;

use App\ClientOrder;
use App\Http\Controllers\Controller;
use App\OrderResource, App\ServiceRequest;
use App\UserComapany;
use Auth;
use Illuminate\Support\Facades\Input;
use App\ExchangeOrderTruckDetail as ExchangeOrderResource;
use Mapper;

class TrackingController extends Controller
{

    public $userId;
    public $companyID;
    public $truckDetail;
    public $orderNumber;
    public $orderOrigin;
    public $orderDest;
    public $resourceId;
    public $showHistory;
     //api username
    public $username = 'olaitan.adesanya@gmail.com';

    //api password
    public $password = '4HLZcaKD';

    //api baseurl
    public $base_url = 'http://server.traccar.org/api/';

    //curl connection
    private $ch;

    public function __construct()
    {

        //open connection
        $this->ch = curl_init();

        curl_setopt($this->ch, CURLOPT_HTTPHEADER,
            ["Authorization: Basic " . base64_encode($this->username . ":" . $this->password),
              'Content-Type:application/json', 
            ]);
        $this->middleware('auth');

    }

 

    public function userInit()
    {

        $userID = Auth::user()->id;

        $companyID = UserComapany::where('user_id', $userID)
            ->value('company_id');
        $this->userId = $userID;
        $this->companyID = $companyID;

    }

    public function allExchangeResources(){

         return ExchangeOrderResource::join('exchange_orders', 'exchange_orders.id', '=', 'exchange_order_truck_details.exchange_order_id')
                                     ->join('users','users.id','=','exchange_orders.user_id');

    }

    public function allResources()
    {

        $allResources = OrderResource::join('service_requests', 'service_requests.id', '=', 'order_resources.order_id')
            ->join('carrier_resource_details', 'carrier_resource_details.id', '=', 'order_resources.resource_id')
            ->join('user_comapanies', 'user_comapanies.user_id', '=', 'service_requests.createdBy');

        return $allResources;

    }

    public function getClientResources($userId)
    {

        $resourceDetails = $this->allExchangeResources()
            ->where('users.id', '=', $userId)
            ->where('exchange_order_truck_details.exchange_status_id', '=', '33')
            ->where('tracker_imei','!=','null')
            ->select('*','exchange_order_truck_details.created_at AS order_start_date')
            ->get();

        return $resourceDetails;
    }

    public function getSingleResource($truckdetail, $companyID)
    {

        $resourceDetail = $this->allResources()
            ->where('user_comapanies.company_id', '=', $companyID)
            ->where('order_resources.status', '=', '0')
            ->where(function ($query) use ($truckdetail) {

                $query->where('GPSID', '=', $truckdetail)
                    ->orWhere('plateNumber', '=', $truckdetail);
            })

            ->get();

        return $resourceDetail;

    }

    public function sortCompanyResources($companyID){

        $resourceDetail = $this->allResources()
            ->where('user_comapanies.company_id', '=', $companyID)
            ->where('order_resources.status', '=', '0');

        // Run query if user sorts by IMEI or PlateNUmber

        if (!is_null($this->truckdetail)) {
            $resourceDetail = $resourceDetail->where(function ($query) {

                $query->where('GPSID', '=', $this->truckdetail)
                    ->orWhere('plateNumber', '=', $this->truckdetail);
            });
        }

        // Run Query if user sorts based on Order Number

        if (!is_null($this->orderNumber)) {

            $resourceDetail = $resourceDetail->where('serviceIDNo', '=', $this->orderNumber);
        }

        // Run Query if user sets orderOrigin

        if (!is_null($this->orderOrigin)) {

            $resourceDetail = $resourceDetail->where('deliverFrom', '=', $this->orderOrigin);

        }

        // Run Query if user sets orderDestination

        if (!is_null($this->orderDest)) {

            $resourceDetail = $resourceDetail->where('deliverTo', '=', $this->orderDest);

        }

        //Sort by resourceId

        if (!is_null($this->resourceId)) {

            $resourceDetail = $resourceDetail->where('order_resources.resource_id', '=', $this->resourceId);

        }

        //Get resource history

        if (!is_null($this->showHistory)) {

          $resourceDetail   =  $resourceDetail->select("*",'order_resources.created_at as startDate');

        }

        return $resourceDetail
            ->select('*','service_requests.created_at as order_start_date','service_requests.id as order_id')
            ->groupBy('resource_id')
            ->get();}

    public function getCompanyResources($companyID){
      $resourceDetail = $this->allResources()
            ->where('user_comapanies.company_id', '=', $companyID)
            ->where('order_resources.status', '=', '0');

      return $resourceDetail
            ->select('*','service_requests.created_at as order_start_date','service_requests.id as order_id')
            ->groupBy('resource_id')
            ->get();
    }


    public function getClientResourceView($resourceId = null,$history = null){

        $this->userInit();

        //check to make sure the keyword is history
        if ($history != 'history') {
          $history    =   null;
        }

        $this->truckdetail = Input::get('truckdetail');
        $this->orderNumber = Input::get('orderNumber');
        $this->orderOrigin = Input::get('orderOrigin');
        $this->orderDest   = Input::get('orderDest');
        $this->resourceId  = $resourceId;
        $this->showHistory = $history;

        $locations = ClientOrder::where('company_id', $this->companyID)
            ->get();

        $origins = array_unique(array_column($locations->toArray(), 'origin'));

        $destinations = array_unique(array_column($locations->toArray(), 'destination'));

         
        $resourcesDetails = $this->sortCompanyResources($this->companyID);

        $data = array(
            'data'      => $resourcesDetails,
            'locations' => array_unique(array_merge($origins, $destinations)),
        );


        //send current date if the user is viewing with history
        $data['endDate']    =   date('Y-m-d');

        return view('clienttracker', $data);
    }

    public function getResources(){
        
        $this->userInit();

        $resourcesDetails = $this->getClientResources($this->userId);

        //define map info
        $mapinfo = [];

        //extra info
        foreach ($resourcesDetails as $info) {
            $mapinfo[] = array(
                'resource_Id'       => $info->resource_id,
                'contactName'       => $info->contactName,
                'label'             => $info->itemDescription,
                'imei'              => $info->tracker_imei,
                'driverName'        => $info->driver_name,
                'orderStartDate'    => $info->order_start_date,
                'profilePics'       => 'https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png',
                'orderId'           => $info->order_number,
                'plateNumber'       => $info->plate_number,
                'orderDeliveryPoint'=> $info->destination_place_id,
                'orderPickUpPoint'  => $info->origin_place_id,
            );
        }
        return response()->json($mapinfo);
    }

    public function viewOnMap($gpsid){

        $u = 'http://80.241.215.74/api/api.php?api=user&ver=1.0&key=2EC94CFEC58F96F44CD6F3D9CA587C9F&cmd=OBJECT_GET_LOCATIONS,' . $gpsid;

        $firstURL = file_get_contents($u);

        $json = json_decode($firstURL, 1);

        foreach ($json as $first) {
            $lat = $first['lat'];
            $lng = $first['lng'];
        }

        $location = file_get_contents("http://80.241.215.74/api/api.php?api=user&ver=1.0&key=2EC94CFEC58F96F44CD6F3D9CA587C9F&cmd=GET_ADDRESS," . $lat . "," . $lng);

        //return $location;

        Mapper::map($lat, $lng, ['zoom' => 18, 'center' => true, 'marker' => false]);
        Mapper::informationWindow($lat, $lng, $location);

        /*Mapper::map();*/

        return view('tracker.location');

    }

    public function getClientResourceHistory()
    {

        $this->userInit();
        $resourceDetail = $this->allResources()
                                ->where('user_comapanies.company_id', '=', $this->companyID)
                                ->where('order_resources.status', '=', '0')
                                ->get();

        return view('client.tracker.allOrderResources',compact('resourceDetail'));
    }

    public function getResourceHistory($gpsid,$orderID){
      $serviceRequest   =   ServiceRequest::where('serviceIDNo',$orderID)->get();

      foreach ($serviceRequest as $service) {
        $order_id     =   $service->id;
        $created_at   =   $service->created_at;
      }

      $from   =   $created_at;
      $to     =   date("Y-m-d");
      $imei   =   $gpsid;

    }

    public function get($value=''){
    


        //set the url, number of POST vars, POST data
        curl_setopt($this->ch, CURLOPT_URL, 'http://server.traccar.org/api/reports/route?deviceId=832&from=2018-11-01T14:48:00.000Z&to=2018-11-07T14:48:00.000Z');
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');

        //execute post
        $result = curl_exec($this->ch);

        //close connection
        curl_close($this->ch);

         var_dump($result);
         exit;
    }

}
