<?php

namespace App\Http\Controllers\ClientControllers;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\ClientOrder;
use Auth;
use App\ClientRequiredResource;
use App\UserComapany;
use App\ClientWallet;
use App\ServiceRequest;
use App\clientsContacts;
use App\UserRole;
use DB, App\User, App\simpleInvoice, Charts; 

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

   

    public function getUserRole($userID)
    {
        // $UserRole = UserRole::where('user_id',$userID)->value('role');
        // return view('client.includes.sidebar',['UserRole'=>$UserRole]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$userID = Auth::user()->id;
        //$standardOrder = ClientOrder::where('user_id',$userID)->paginate(10);
       $companyID = session('companyId');
       
        $userID = Auth::user()->id;
        $companyID = UserComapany::where('user_id',$userID)->value('company_id');
        //$originToServer = $request->originToServer;
        $orgins = ClientOrder::where('company_id',$companyID)->get();
        $originArray = array();

        for ($i=0; $i < count($orgins) ; $i++) { 
            $originArray[] = $orgins[$i]['origin'];
        }

        $uniqueOrigins = array_unique($originArray);

        //return $uniqueOrigins;


        $standardOrigin = ClientOrder::where('company_id',$companyID)->get();

        //$walletBalance = ClientWallet::where('company_id',$companyID)->orderBy('created_at', 'asc')->first()->value('balance');

        //$standardOrigin = ClientOrder::where('user_id',$userID)->get();
        $ClientRequiredResource = ClientRequiredResource::where('company_id',$companyID)->get();


        // return view('index', ['standardOrigin'=>$standardOrigin, 'ClientRequiredResource'=>$ClientRequiredResource,'uniqueOrigins'=>$uniqueOrigins,'walletBalance'=>$walletBalance]);

        $completed = count(ServiceRequest::where('createdBy',$userID)->where('orderStatus',9)->get());
        $orders = count(ServiceRequest::where('createdBy',$userID)->get());


        $orderDetails       = DB::table('service_requests')
                                    ->join('user_comapanies', 'user_comapanies.user_id', '=', 'service_requests.createdBy')
                                    ->where('user_comapanies.company_id','=',$companyID)
                                    // ->select('haulage_resource_requests.*', 'service_requests.*')
                                    ->get();

        $averageCostPerDay  =  DB::table('simple_invoices')
                                    ->where('company_id', $companyID)
                                    ->get();


        $paid               =  simpleInvoice::where('company_id',$companyID)
                                    ->where('status',1)->get();

        $unpaid             =  simpleInvoice::where('company_id',$companyID)
                                    ->where('status',0)->get();

        $pie_chart          = Charts::create('pie', 'highcharts')
                                    ->title('Invoices')
                                    ->labels(['Paid Invoices', 'Unpaid Invoices'])
                                    ->values([count($paid),count($unpaid)])
                                    ->dimensions(1000,500)
                                    ->responsive(true);

        $chart              = Charts::database($orderDetails, 'bar', 'highcharts')
                                        ->title('Orders')
                                        ->elementLabel("Total Orders")
                                        ->GroupByMonth();

        $costPerDay         = Charts::database($averageCostPerDay, 'line', 'highcharts')
                                        ->title('Average Cost Per Day')
                                        ->elementLabel("Average Cost Per Day")
                                        ->GroupByMonth();

        return view('client.index', compact('chart','pie_chart','costPerDay'));
    }


    public function create_order()
    {
       
        //$userID = Auth::user()->id;
        //$standardOrder = ClientOrder::where('user_id',$userID)->paginate(10);
        $userID = Auth::user()->id;
        $companyID = UserComapany::where('user_id',$userID)->value('company_id');
        //$originToServer = $request->originToServer;
        $orgins = ClientOrder::where('company_id',$companyID)->get();
        $originArray = array();

        for ($i=0; $i < count($orgins) ; $i++) { 
            $originArray[] = $orgins[$i]['origin'];
        }

        $uniqueOrigins = array_unique($originArray);

        //return $uniqueOrigins;
        
        $standardOrigin = ClientOrder::where('company_id',$companyID)->get();

        $walletBalance = ClientWallet::where('company_id',$companyID)->orderBy('created_at', 'asc')->first()->value('balance');

        //$standardOrigin = ClientOrder::where('user_id',$userID)->get();
        $ClientRequiredResource = ClientRequiredResource::where('company_id',$companyID)->get();
        //$UserRole = $this->getUserRole($userID);
        
        $data = array('standardOrigin'          =>$standardOrigin, 
                      'ClientRequiredResource'  =>$ClientRequiredResource,
                      'uniqueOrigins'           =>$uniqueOrigins,
                      'walletBalance'           =>$walletBalance,
                      //'UserRole'                =>$UserRole,
                    );


     return view('client.create_order', $data);

        
    }
}
