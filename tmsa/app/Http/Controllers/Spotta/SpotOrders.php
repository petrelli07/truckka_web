<?php

namespace App\Http\Controllers\spotta;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator, Auth, Mail, App\Mail\Invoice;
use Session,DB;
use App\SpotOrder,App\SpotOrderInvoice, App\SpotOrderRoutePricing, App\Bank, App\SpotOrderPayment, App\SpotOrderResourceType;

class SpotOrders extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');
    }
/*asing unique order number*/
    public function spotOrderNumber(){
        $i = 1;
        for($j=0; $j < $i; $j++){
            $result = '';
            $length = 7;
            for($x = 0; $x < $length; $x++) {
                $result .= mt_rand(0, 9);
            }

            $checkAvailable = SpotOrder::where('order_number', $result);
            if( $checkAvailable->count() < 1 ){
                return $result;
                break;
            }

            $i++;
        }
    }

/*assign unique Invoice number*/
    public function invoiceNumber(){
        $i = 1;
        for($j=0; $j < $i; $j++){
            $result = '';
            $length = 7;
            for($x = 0; $x < $length; $x++) {
                $result .= mt_rand(0, 9);
            }
            //$checkAvailable = FinalInvoice::where('invoice_no', $result);
            $checkAvailable = SpotOrderInvoice::where('invoice_number', $result);
            if( $checkAvailable->count() < 1 ){
                return $result;
                break;
            }

            $i++;
        }
    }

/*return quote for pricing*/
    public function getQuote(Request $request)
   	{   		
   		$amount			=	250000;
   		$vat 			=	$amount * 0.05;
   		$totalAmount	=	$amount + $vat;

   		$validator = Validator::make($request->all(), [
            'description'=>'required',
            'origin'=>'required',
            'destination'=>'required',
            'weight'=>'required'
        ]);

   		if ($validator->passes()) {
   			$description 		= $request->description;
   			$origin 			= $request->origin;
   			$destination 		= $request->destination;
   			$weight 			= $request->weight;
   			$resourceType 		= $request->truckType;

   			$array = [
   					'description'				=>	$request->description,
   					'origin'					=>	$request->origin,
   					'destination'				=>	$request->destination,
   					'weight'					=>	$request->weight,
   					'pickupDate'				=>	$request->pickupdate,
   					'amount'					=>	$amount,
   					'vat'						=>	$vat,
   					'total'						=>	$totalAmount,
   					'resourceType'				=>	$resourceType
   				];
   			return response()->json(['success'=>$array]);
   		}else{
   			return response()->json(['error'=>$validator->errors()->all()]);
   		}	
   	}

/*save quote to session*/
   	public function saveQuoteSession(Request $request)
   	{

   		if ($request->submitQuoteButton === 'loggedInUser') {

   			$route_id 				= SpotOrderRoutePricing::where('origin',$request->origin)
						   									->where('destination',$request->destination)
						   									->value('id');
   			$data =	new SpotOrder;

   			$data->order_number 				          =	$this->spotOrderNumber();
   			$data->item_description 			        =	$request->description;
   			$data->pickup_date 					          =	$request->pickupDate;
   			$data->weight 						            =	$request->weight;
   			$data->spot_order_resource_type_id	  =	$request->resourceType;
   			$data->user_id 						            =	Auth::user()->id;
   			$data->spot_order_route_pricing_id    =	$route_id;
   			$data->spot_order_status_id  		      =	1;

   			$data->save();

   			$spot_order_id 	=	$data->id;

   			$invoice = new SpotOrderInvoice;

   			$invoice->invoice_number 						          = $this->invoiceNumber();
   			$invoice->amount 								              =	$request->amount;	
   			$invoice->spot_order_id 						          =	$spot_order_id;	
            $invoice->spot_order_invoice_status_id        = 1;
   			$invoice->user_id 			                      =	Auth::user()->id;	
   			$invoice->save();

        $spotOrder  = DB::table('spot_orders')
              ->join('spot_order_route_pricings','spot_order_route_pricings.id','=',
                      'spot_orders.spot_order_route_pricing_id')
              ->join('spot_order_invoices','spot_order_invoices.spot_order_id','=',
                      'spot_orders.id')
              ->where('spot_orders.id',$spot_order_id)
              ->get();

        $user = Auth::user()->email;

        Mail::to($user)->send(new Invoice($spotOrder));

        return "done";

   		}elseif($request->submitQuoteButton === 'register'){

   			Session::put('quote', $request->all());
   			
   			return redirect('/register');
   		}	
   	}

   	
    /*client area*/

    /*client landing page*/
     public function clientHome()
     {
        $user_id    = Auth::user()->id;
        $spotOrders  = DB::table('spot_orders')
                  ->join('spot_order_route_pricings','spot_order_route_pricings.id','=','spot_orders.spot_order_route_pricing_id')
                  ->join('spot_order_invoices','spot_order_invoices.id','=','spot_orders.id')
                  ->join('spot_order_statuses','spot_order_statuses.id','=','spot_orders.spot_order_status_id')
                  ->where('spot_orders.user_id',$user_id)
                  ->get();

        return view('spotta.client.orders.myorders',compact('spotOrders'));
     }
     /*end client landing page*/

     /*client invoices*/
     public function my_invoices()
     {
        $user_id    = Auth::user()->id;
        $invoice   = DB::table('spot_order_invoices')
                          ->join('spot_orders','spot_orders.id','=','spot_order_invoices.spot_order_id')
                          ->join('spot_order_invoice_statuses','spot_order_invoice_statuses.id','=','spot_order_invoices.spot_order_invoice_status_id')
                          ->where('spot_order_invoices.user_id',$user_id)
                          ->get();

        return view('spotta.client.finance.myinvoices',compact('invoice'));
     }
     /*end client invoices*/

     /*enter payment details page*/
    public function make_payment($invoice_number)
    {

        $invoice = DB::table('spot_order_invoices')
                          ->join('spot_orders','spot_orders.id','=','spot_order_invoices.spot_order_id')
                          ->where('invoice_number',$invoice_number)
                          ->get();

        $banks  = Bank::all();
        return view('spotta.client.finance.payments',compact('banks','invoice'));
    }
     /*end enter payment details page*/

/*payment details form processing*/
    public function bank_payment(Request $request)
    {
          //return $request->all();
          $this->validate($request, [
              'tellerNumber'      =>'required',
              'depositorName'     =>'required',
              'contactNumber'     =>'required',
              'bank_id'           =>'required',
              'invoice_number'    =>'required',
              'amount'            =>'required'
          ]);

          $tellerNumber         =   $request->tellerNumber;
          $depositorName        =   $request->depositorName;
          $contactNumber        =   $request->contactNumber;
          $bank_id              =   $request->bank_id;
          $invoice_number       =   $request->invoice_number;
          $amount               =   $request->amount;
          $order_id             =   $request->order_id;

          $invoice_id           = SpotOrderInvoice::where('invoice_number',$invoice_number)
                                  ->value('id');

          $data                 =   new SpotOrderPayment;

          $data->reference_number                       = $tellerNumber;
          $data->amount                                 = $amount;
          $data->spot_order_invoice_id                  = $invoice_id;
          $data->spot_order_id                          = $order_id;
          $data->spot_order_payment_status_id           = 1;
          $data->bank_id                                = $bank_id;
          $data->depositor_name                         = $depositorName;
          $data->contact_number                         = $contactNumber;
          $data->user_id                                = Auth::user()->id;

          $data->save();

          return "successful";
    }
/*end payment details form processing*/

/*all payments made by client*/
    public function my_payments()
    {
      $user_id    = Auth::user()->id;
      $payments   = DB::table('spot_order_payments')
                        ->join('spot_orders','spot_orders.id','=','spot_order_payments.spot_order_id')
                        ->join('spot_order_invoices','spot_order_invoices.id','=','spot_order_payments.spot_order_invoice_id')
                        ->where('spot_order_payments.user_id',$user_id)
                        ->get();
      return $payments;
        return view('spotta.client.finance.myinvoices',compact('invoice'));
      
    }
/*end all payments made by client*/
/*end spot client area*/

/*spot carrier area*/
/*spot carrier landing page*/
    public function carrierHome()
    {
      $user_id    = Auth::user()->id;
      $requests = DB::table('spot_carrier_orders')
                        ->join('spot_orders','spot_orders.id','=','spot_carrier_orders.spot_order_id')
                        ->join('spot_carrier_order_statuses','spot_carrier_order_statuses.id','=',
                                'spot_carrier_orders.spot_carrier_order_status_id')
                        ->where('spot_carrier_orders.user_id',$user_id)
                        ->get();
      return view('spotta.carrier.orders.myorders');
    }
/*end spot carrier landing page*/

/*spot carrier add truck supply*/
    public function add_truck_view()
    {

      $truckTypes = SpotOrderResourceType::where('sort_statuses_id',1)->get();
      return view('spotta.carrier.resources.new_resources',compact('truckTypes'));
    }
/*end spot carrier add truck supply view*/

/*add new resources process*/
    public function add_new_truck(Request $request)
    {

      return $request->all();
      $validator = Validator::make($request->all(), [
            'platenumber'     =>  'required',
            'capacity'        =>  'required',
            'drivername'      =>  'required',
            'drivernumber'    =>  'required',            
            'imei'            =>  'required'
      ]);

      if ($validator->passes()) {
        
         $userID     = Auth::user()->id;

         /*$resourceDetails = array(
                                  'plateNumber'      => $details->platenumber,
                                  'driverName'       => $details->drivername,
                                  'driverPhoneNumber'=> $details->drivernumber,
                                  'capacity'         => $details->capacity,
                                  'company_id'       => $carrierId,
                                  'GPSID'            => $details->imei
                                );*/

        //$saveResource     = DB::table('carrier_resource_details')->insert($resourceDetails);
      }
    }
/*end add new resources process*/

/*end spot carrier area*/
}
