<?php

namespace App\Http\Controllers\Spotta;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\SpotOrderResourceType;
use App\SpotOrderRoutePricing;
use Validator,DB,Auth;

class HomeController extends Controller
{
   public function index()
   {
   		$truckTypes =	SpotOrderResourceType::where('sort_statuses_id',1)->get();

   		$pricing 	=	SpotOrderRoutePricing::where('sorp_status_id',1)->get();


   		$orgins = SpotOrderRoutePricing::where('sorp_status_id',1)->get();
        $originArray = array();

        for ($i=0; $i < count($orgins) ; $i++) { 
            $originArray[] = $orgins[$i]['origin'];
        }

        $uniqueOrigins = array_unique($originArray);

   		return view('spotta.index',compact('truckTypes','uniqueOrigins'));
   }

   public function checkOrigins(Request $request)
   {
        $originToServer = $request->originToServer;

        $destination 	= SpotOrderRoutePricing::where('origin',$originToServer)->get();
    
        return response()->json(['success'=>$destination]);
   }
   	
}
