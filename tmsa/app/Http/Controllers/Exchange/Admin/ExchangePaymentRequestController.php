<?php

namespace App\Http\Controllers\Exchange\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Exchange\ExchangeOrderTruckPayment as TruckPayment;

class ExchangePaymentRequestController extends Controller
{
    public function paymentRequestView(){
        $breadcrumb =   'Payment Requests';
        $truckPayment   =   TruckPayment::join('exchange_orders','exchange_order_truck_payments.exchange_order_id','exchange_orders.id')
        ->join('exchange_statuses','exchange_order_truck_payments.exchange_status_id','exchange_statuses.id')
        ->join('exchange_order_truck_details','exchange_order_truck_payments.truck_detail_id','exchange_order_truck_details.id')
        ->join('exchange_routes','exchange_orders.exchange_route_id','exchange_routes.id')
        ->where('exchange_order_truck_payments.exchange_status_id',33)
                                            ->get();

        return view('exchange.admin.payment_requests',compact('truckPayment','breadcrumb'));
    }
}
