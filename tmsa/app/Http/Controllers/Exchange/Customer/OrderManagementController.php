<?php

namespace App\Http\Controllers\Exchange\Customer;

use App\Exchange\ExchangeOrder as Order;
use App\Exchange\ExchangeRoute as Route;
use App\Exchange\ExchangeOrderTruckDetail as TruckDetail;
use App\Exchange\State as State;
use App\Exchange\ExchangeTruckType as TruckType;
use App\Exchange\ExchangePayment as Payment;
use App\Exchange\ExchangeOrderTruckPayment as TruckPayment;
use App\Exchange\TruckSupplier as TruckSupplier;
use App\Exchange\ExchangeWallet as Wallet;
use App\Exchange\ExchangeWalletHistory as WalletHistory;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Mail;
use App\Mail\Exchange\newOrder;

class OrderManagementController extends Controller
{

     public function __construct()
     {
         $this->middleware('auth');
     }

    public function user_id(){
        $user_id    =   Auth::user()->id;
        return $user_id;
    }

    public function checkFistLogin(){
        $user_id    =   $this->user_id();
        $check  =   User::where('id',$user_id)->value('is_first_login');
        if($check === 1){
            return true;
        }
    }

    public function getInvoiceReferenceNumber(){
        $user_id    =   $this->user_id();
        $payment    =   Payment::where('user_id',$user_id)->latest()->first();
        $invoice_number =   $payment->reference_number;
        return $invoice_number;
    }

    public function index(){

        $check_for_order   = $this->checkFistLogin();

        if($check_for_order){
            $invoice_number =   $this->getInvoiceReferenceNumber();
            return redirect("/exchange/customer/invoice_details/{$invoice_number}");
        }else{
            $breadcrumb =   "Home";
            $user   =   Payment::join('exchange_orders','exchange_payments.exchange_order_id','exchange_orders.id')
                                ->join('exchange_routes','exchange_orders.exchange_route_id','=',
                                'exchange_routes.id')
                                ->join('states as origin_state','exchange_routes.origin_state_id','origin_state.id')
                                ->join('states as destination_state','exchange_routes.destination_state_id','destination_state.id')
                                ->join('exchange_truck_types','exchange_orders.exchange_truck_type_id',
                                    '=','exchange_truck_types.id')
                                ->join('exchange_statuses','exchange_orders.exchange_status_id',
                                    '=','exchange_statuses.id')
                                ->select('exchange_orders.*','exchange_truck_types.*','origin_state.state_description as originStateName','destination_state.state_description as destStateName','exchange_statuses.*','exchange_routes.*','exchange_payments.*')
                                ->where('exchange_orders.user_id',$this->user_id())
                                ->get();
            return view('exchange.client.index',compact('breadcrumb','user'));
        }

    }

    public function viewTruckDetails($order_number){
        $breadcrumb =   "Truck Details";
        $order_id   =   Order::where('order_number',$order_number)->value('id');
        $truck_details  =   TruckDetail::where('exchange_order_id',$order_id)
                                        ->get();
        return view('exchange.client.truck_details',compact('truck_details','breadcrumb'));
    }

    public function createOrder(){

        $breadcrumb     =   "Create New Order";
        $truckTypes =	TruckType::where('exchange_status_id',7)->get();


        $route 	    =	Route::where('exchange_status_id',1)->pluck('origin_state_id');

        $state_description  =   [];

        for($i=0;$i<count($route);$i++){
            $state_description[]    =   State::where('id',$route[$i])->value('state_description');
        }

        $final = array_unique($state_description);
        $uniqueOrigins  =   array_values( $final);
        return view('exchange.client.new-orders',compact('breadcrumb','truckTypes','uniqueOrigins'));
    }

    public function submitOrder(Request $request){

        $this->validate($request, [
            'origin'=>'required',
            'destination'=>'required',
            'truckType'=>'required',
            'number_of_truck'=>'required',
            'cargo_description'=>'required',
            'pickup_date'=>'required|date',
            'pickup_address'=>'required',
            'delivery_address'=>'required',
            'truckCapacity'=>'required',
        ]);


        ///$breadcrumb         =   "Order Summary";
        $origin 			        = $request->origin;
        $destination 		        = $request->destination;
        $resourceType 		        = $request->truckType;
        $truckNumber 		        = $request->number_of_truck;
        $delivery_address 		    = $request->delivery_address;
        $pickup_address 		    = $request->pickup_address;
        $pickup_date     		    = $request->pickup_date;
        $cargo_description 		    = $request->cargo_description;
        $pickup_placeID             = $request->pickup_placeId;
        $delivery_placeID           = $request->delivery_placeId;
        $truckCapacity              = $request->truckCapacity;

        $or            =   State::where('state_description',$origin)->get();

        foreach($or as $origins){
            $origin_state_id    =  $origins->id;
        }

        $dest       =   State::where('state_description',$destination)->get();

        foreach($dest as $destinations){
            $destination_state_id   =   $destinations->id;
        }

        $route  =   Route::where('origin_state_id',$origin_state_id)
            ->where('destination_state_id',$destination_state_id)
            ->get();

        foreach($route as $routes){
            $route_id       =   $routes->id;
            $base_amount   =   $routes->customer_base_price;
            $service_charge   =   $routes->customer_service_charge;
        }

        $amount     =   $base_amount + $service_charge;

        $total_amount   =   $amount * $truckNumber;

        $request_amount     =   $total_amount;

        $order_number   =   $this->orderNumber();

        $ExchangeOrder  =   new Order;
        $ExchangeOrder->order_number                                =   $order_number;
        $ExchangeOrder->number_of_trucks                            =   $truckNumber;
        $ExchangeOrder->user_id                                     =   $this->user_id();
        $ExchangeOrder->exchange_route_id                           =   $route_id;
        $ExchangeOrder->exchange_truck_type_id                      =   $resourceType;
        $ExchangeOrder->item_description                            =   $cargo_description;
        $ExchangeOrder->pickup_address                              =   $pickup_address;
        $ExchangeOrder->delivery_address                            =   $delivery_address;
        $ExchangeOrder->pickup_date                                 =   $pickup_date;
        $ExchangeOrder->origin_place_id                             =   $pickup_placeID;
        $ExchangeOrder->destination_place_id                        =   $delivery_placeID;
        $ExchangeOrder->truck_capacity                              =   $truckCapacity;
        $ExchangeOrder->exchange_status_id                          =   11;
        $ExchangeOrder->save();

        //return back()->with('message','Order Created Successfully. You will get a Quote Shortly');

        $order_id   =   $ExchangeOrder->id;

        /*
        $amount             =   $amount  *  $truckNumber;
        $service_charge     =   $amount * 0.1;
        $total_amount       =   $amount + $service_charge;
        $request_amount     =   $total_amount;
        */

        $reference_number   =   $this->paymentIDNumber();
        /*record payment*/
        $payment    =   new Payment;
        $payment->reference_number               =  $reference_number;
        $payment->amount                         =   $request_amount;
        $payment->user_id                        =   Auth::user()->id;
        $payment->exchange_order_id              =   $order_id;
        $payment->exchange_status_id             =   16;
        $payment->save();

        $details    =   [
            'name'=>Auth::user()->name,
            'order_number'=>$order_number
        ];

        Mail::to(Auth::user()->email)->send(new newOrder($details));

        return redirect("/exchange/customer/invoice_details/{$order_number}");
    }

    public function processOrder(Request $request){
        /*Payment Gateway here*/
        /*End Payment Gateway*/

        $ExchangeOrder  =   new Order;

        $ExchangeOrder->order_number                        =   $this->orderNumber();
        $ExchangeOrder->number_of_trucks                    =   $request->truckNumber;
        $ExchangeOrder->pickup_date                         =   $request->pickupDate;
        $ExchangeOrder->item_description                    =   $request->itemDescription;
        $ExchangeOrder->user_id                             =   Auth::user()->id;
        $ExchangeOrder->exchange_route_id                   =   $request->exchangeRouteID;
        $ExchangeOrder->exchange_truck_type_id              =   $request->truckTypeID;
        $ExchangeOrder->contact_phone                       =   $request->contactPhone;
        $ExchangeOrder->pickup_address                      =   $request->pickupAddress;
        $ExchangeOrder->pickup_address                      =   $request->pickupAddress;
        $ExchangeOrder->exchange_status_id                  =   9;
        $ExchangeOrder->save();

        $order_id   =   $ExchangeOrder->id;

        $request_amount =   $request->totalAmount;
        $reference_number   =   $this->paymentIDNumber();

        /*record payment*/
        $payment    =   new Payment;
        $payment->reference_number               =  $reference_number;
        $payment->amount                         =   $request_amount;
        $payment->user_id                        =   Auth::user()->id;
        $payment->exchange_order_id              =   $order_id;
        $payment->exchange_status_id             =   16;
        $payment->save();
        /*end record payment*/

        $user_email         =   Auth::user()->email;

        $amount_in_kobo =   $request_amount * 100;

        $paystack = new \Yabacon\Paystack('sk_test_28a937c41ad4dda08b5bb33c71fba1b2d5357b3d');
        try
        {
            $tranx = $paystack->transaction->initialize([
                'amount'    =>  $amount_in_kobo,       // in kobo
                'email'     =>   $user_email,         // unique to customers
                'reference' =>$reference_number, // unique to transactions
            ]);
        } catch(\Yabacon\Paystack\Exception\ApiException $e){
            print_r($e->getResponseObject());
            die($e->getMessage());
        }

        //save_last_transaction_reference($tranx->data->reference);

        // redirect to page so User can pay
        return redirect()->to($tranx->data->authorization_url);

        //return redirect('/exchange/customer/home')->with('message','Order Completed Successfully');
    }

    public function confirmDelivery($id){
        $truck_detail       =    TruckDetail::find($id);
        $order_id           =   $truck_detail->exchange_order_id;
        $supplier_id        =   $truck_detail->truck_supplier_id;



        $amount  = Order::where('exchange_orders.id',$order_id)->join('exchange_routes','exchange_orders.exchange_route_id','=',
            'exchange_routes.id')->value('transporter_base_price');

        $supplier_details   =   TruckSupplier::find($supplier_id);
        $supplier_type_id   =   $supplier_details->truck_supplier_type_id;

        if($supplier_type_id == 1){
            TruckDetail::where('id',$id)
                ->update(
                    array(
                        'exchange_status_id'    => 34,
                    )
                );


            $truckPayment   =   new TruckPayment;
            $truckPayment->reference_number         =   $this->truckPaymentReference();
            $truckPayment->amount                   =   $amount;
            $truckPayment->exchange_order_id        =   $order_id;
            $truckPayment->truck_detail_id          =   $id;
            $truckPayment->exchange_status_id       =   33;
            $truckPayment->save();

            return back()->with('message','Delivery Confirmed');
        }elseif($supplier_type_id == 2){

            $supplier_user_id   =   $supplier_details->user_id;

            TruckDetail::where('id',$id)
                ->update(
                    array(
                        'exchange_status_id'    => 33,
                    )
                );

            $truckPayment   =   new TruckPayment;
            $truckPayment->reference_number         =   $this->truckPaymentReference();
            $truckPayment->exchange_order_id        =   $order_id;
            $truckPayment->truck_detail_id          =   $id;
            $truckPayment->exchange_status_id       =   33;
            $truckPayment->save();

            $amount =   5000.00;

            $wallet_history                     =   new WalletHistory;
            $wallet_history->amount             =   $amount;
            $wallet_history->user_id            =   $supplier_user_id;
            $wallet_history->exchange_order_id  =   $order_id;
            $wallet_history->save();

            $wallet_balance =   Wallet::where('user_id',$supplier_user_id)->get();

            if(count($wallet_balance) < 1){

                $wallet =   new Wallet;
                $wallet->amount                     =   $amount;
                $wallet->user_id                    =   $supplier_user_id;
                $wallet->exchange_status_id         =   23;

            }else{
                foreach($wallet_balance as $wallet_bal){
                    $current_wallet_balance =   $wallet_bal->amount;
                }
                $new_wallet_balance =   $current_wallet_balance + $amount;
                Wallet::where('user_id',$supplier_user_id)
                    ->update(
                        array(
                            'amount'    => $new_wallet_balance,
                        )
                    );
            }

            return back()->with('message','Delivery Confirmed');

        }

    }

    public function orderNumber(){
        $i = 1;
        for($j=0; $j < $i; $j++){
            $result = '';
            $length = 7;
            for($x = 0; $x < $length; $x++) {
                $result .= mt_rand(0, 9);
            }

            $checkAvailable = Order::where('order_number', $result);
            if( $checkAvailable->count() < 1 ){
                return $result;
                break;
            }

            $i++;
        }
    }

    public function truckPaymentReference(){
        $i = 1;
        for($j=0; $j < $i; $j++){
            $result = '';
            $length = 7;
            for($x = 0; $x < $length; $x++) {
                $result .= mt_rand(0, 9);
            }

            $checkAvailable = TruckPayment::where('reference_number', $result);
            if( $checkAvailable->count() < 1 ){
                return $result;
                break;
            }

            $i++;
        }
    }

    public function paymentIDNumber(){
        $i = 1;
        for($j=0; $j < $i; $j++){
            $result = '';
            $length = 7;
            for($x = 0; $x < $length; $x++) {
                $result .= mt_rand(0, 9);
            }

            $checkAvailable = Payment::where('reference_number', $result);
            if( $checkAvailable->count() < 1 ){
                return $result;
                break;
            }

            $i++;
        }
    }

}
