<?php

namespace App\Http\Controllers\Exchange\Customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Exchange\ExchangePayment as Payment;
use App\Exchange\ExchangeOrder as Order;
use App\User;
use Auth;

class FinancialManagementController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function referenceNumber(){
        $i = 1;
        for($j=0; $j < $i; $j++){
            $result = '';
            $length = 7;
            for($x = 0; $x < $length; $x++) {
                $result .= mt_rand(0, 9);
            }
            $checkAvailable = Payment::where('reference_number', $result);
            if( $checkAvailable->count() < 1 ){
                return $result;
                break;
            }

            $i++;
        }
    }

    public function user_id(){
        $user_id    =   Auth::user()->id;
        return $user_id;
    }

    public function checkFistLogin(){
        $user_id    =   $this->user_id();
        $check  =   User::where('id',$user_id)->value('is_first_login');
        if($check === 1){
            return true;
        }
    }
    

    public function viewAllInvoices(){
        $breadcrumb =   "All Invoices";
        $invoices   =  Payment::join('exchange_orders','exchange_payments.exchange_order_id','exchange_orders.id')
                                            ->join('exchange_routes','exchange_orders.exchange_route_id','=',
                                                'exchange_routes.id')
                                            ->join('states as origin_state','exchange_routes.origin_state_id','origin_state.id')
                                            ->join('states as destination_state','exchange_routes.destination_state_id','destination_state.id')
                                            ->join('exchange_truck_types','exchange_orders.exchange_truck_type_id',
                                                '=','exchange_truck_types.id')
                                            ->join('exchange_statuses','exchange_orders.exchange_status_id',
                                                '=','exchange_statuses.id')
                                            ->select('exchange_orders.*','exchange_truck_types.*','origin_state.state_description as originStateName','destination_state.state_description as destStateName','exchange_statuses.*','exchange_routes.*','exchange_payments.*')
                                            ->where('exchange_orders.user_id',$this->user_id())
                                            ->get();

        return view('exchange.client.invoice',compact('invoices','breadcrumb'));
    }

    public function viewAllUnpaidInvoices(){
        $unpaid_invoices    =   Payment::where('user_id',Auth::user()->id)
                                            ->where('exchange_status_id',16)
                                            ->join('exchange_statuses','exchange_payments.exchange_status_id',
                                                'exchange_statuses.id')
                                            ->join('exchange_orders','exchange_payments.exchange_order_id',
                                                'exchange_orders.id')
                                            ->get();
        return $unpaid_invoices;
    }

    public function payInvoice($reference_number){
        //$invoice_details   =   Payment::where('reference_number',$reference_number)->get();

        $invoice_details    =   Payment::where('reference_number',$reference_number)
                                ->join('exchange_orders','exchange_orders.id','exchange_payments.exchange_order_id')
                                ->join('exchange_routes','exchange_orders.exchange_route_id','=',
                                    'exchange_routes.id')
                                ->join('exchange_statuses','exchange_payments.exchange_status_id',
                                    'exchange_statuses.id')
                                ->select('exchange_routes.*','exchange_orders.*','exchange_statuses.*','exchange_payments.*')->get();

        $user_email         =   Auth::user()->email;

        foreach($invoice_details as $details){
             //$price                     =   $details->price;
             $number_or_trucks          =   $details->number_of_trucks;
            $base_amount                =   $details->customer_base_price;
            $service_charge             =   $details->customer_service_charge;
        }

        $amount     =   $base_amount + $service_charge;

        $total_amount   =   $amount * $number_or_trucks;

//        $request_amount     =   $total_amount;

        $amount_in_kobo =   $total_amount * 100;

        $paystack = new \Yabacon\Paystack('sk_test_28a937c41ad4dda08b5bb33c71fba1b2d5357b3d');
        try
        {
            $tranx = $paystack->transaction->initialize([
                'amount'=>$amount_in_kobo,       // in kobo
                'email'=>$user_email,         // unique to customers
                'reference'=>$reference_number, // unique to transactions
            ]);
        } catch(\Yabacon\Paystack\Exception\ApiException $e){
            /*print_r($e->getResponseObject());
            die($e->getMessage());*/
            Payment::where('reference_number',$reference_number)
                ->update(
                    array(
                        'reference_number'    => $this->referenceNumber(),
                    )
                );
            return redirect('/exchange/customer/home')->with('message','An error occurred. Please try again');
        }

        //save_last_transaction_reference($tranx->data->reference);

        // redirect to page so User can pay
        return redirect()->to($tranx->data->authorization_url);
        //header('Location: ' . $tranx->data->authorization_url);

    }

    public function successfulPayment(){
        $reference = isset($_GET['reference']) ? $_GET['reference'] : '';
        if(!$reference){
            die('No reference supplied');
        }
        // initiate the Library's Paystack Object
        $paystack = new \Yabacon\Paystack('sk_test_28a937c41ad4dda08b5bb33c71fba1b2d5357b3d');
        try
        {
            // verify using the library
            $tranx = $paystack->transaction->verify([
                'reference'=>$reference, // unique to transactions
            ]);
        } catch(\Yabacon\Paystack\Exception\ApiException $e){
            /*print_r($e->getResponseObject());
            die($e->getMessage());*/
            Payment::where('reference_number',$reference)
                ->update(
                    array(
                        'reference_number'    => $this->referenceNumber(),
                    )
                );
            return redirect('/exchange/customer/home')->with('message','An error occurred. Please try again');
        }

        if ('success' === $tranx->data->status) {
            // transaction was successful...
            // please check other things like whether you already gave value for this ref
            // if the email matches the customer who owns the product etc
            Payment::where('reference_number',$reference)
                ->update(
                    array(
                        'exchange_status_id'    => 15,
                    )
            );

            $order_id    =   Payment::where('reference_number',$reference)->value('exchange_order_id');
            Order::where('id',$order_id)
                ->update(
                    array(
                        'exchange_status_id'    => 10,
                    )
                );
            User::where('id',$this->user_id())
                ->update(
                    array(
                        'is_first_login'    => 0,
                    )
                );
        }else{
            return redirect('/exchange/customer/home')->with('message','An error occurred. Please try again');
        }

        return redirect("/exchange/customer/home")->with('message','Payment Received. Truck will be sent within 72 Hours');
        //return redirect("/exchange/customer/client-area/complete_transaction/{$reference}")->with('message','Payment Received');
    }

    public function paystackWebhook(){
        $event = \Yabacon\Paystack\Event::capture();
        http_response_code(200);

        /* It is a important to log all events received. Add code *
         * here to log the signature and body to db or file       */
        openlog('MyPaystackEvents', LOG_CONS | LOG_NDELAY | LOG_PID, LOG_USER | LOG_PERROR);
        syslog(LOG_INFO, $event->raw);
        closelog();

        /* Verify that the signature matches one of your keys*/
        $my_keys = [
            'live'=>'sk_live_c491d1a0c5b7c8273dad7798c58f727085ec835f',
            'test'=>'sk_test_28a937c41ad4dda08b5bb33c71fba1b2d5357b3d',
        ];
        $owner = $event->discoverOwner($my_keys);
        if(!$owner){
            // None of the keys matched the event's signature
            die();
        }

        // Do something with $event->obj
        // Give value to your customer but don't give any output
        // Remember that this is a call from Paystack's servers and
        // Your customer is not seeing the response here at all
        switch($event->obj->event){
            // charge.success
            case 'charge.success':
                if('success' === $event->obj->data->status){
                    // TIP: you may still verify the transaction
                    // via an API call before giving value.
                    Payment::where('reference_number',$event->obj->data->reference)
                        ->update(
                            array(
                                'exchange_status_id'    => 15,
                            )
                        );

                    $order_id    =   Payment::where('reference_number',$event->obj->data->reference)->value('exchange_order_id');
                    Order::where('id',$order_id)
                        ->update(
                            array(
                                'exchange_status_id'    => 10,
                            )
                        );
                }
                break;
        }
    }

    public function invoiceDetail($reference_number){

        //$order_id   =  Payment::where('reference_number',$reference_number)->value('exchange_order_id');

        $order_id   =   Order::where('order_number',$reference_number)->value('id');
        $invoice_details    =   Payment::where('exchange_order_id',$order_id)
                                        ->join('exchange_orders','exchange_orders.id','exchange_payments.exchange_order_id')
                                        ->join('exchange_routes','exchange_orders.exchange_route_id','=',
                'exchange_routes.id')
                                        ->join('states as origin_state','exchange_routes.origin_state_id','origin_state.id')
                                        ->join('states as destination_state','exchange_routes.destination_state_id','destination_state.id')
                                        ->join('exchange_truck_types','exchange_orders.exchange_truck_type_id',
                                            '=','exchange_truck_types.id')
                                        ->join('exchange_statuses','exchange_orders.exchange_status_id',
                                            '=','exchange_statuses.id')
                                        ->select('exchange_routes.*','exchange_orders.*','exchange_truck_types.*','origin_state.state_description as originStateName','destination_state.state_description as destStateName','exchange_statuses.*','exchange_payments.*')->get();

        foreach($invoice_details as $invoice_detail){
            $order_number   =   $invoice_detail->order_number;
            $invoice_number   =   $invoice_detail->reference_number;
            $date                   =   $invoice_detail->created_at;
            $origin                 =   $invoice_detail->originStateName;
            $destination            =   $invoice_detail->destStateName;
            $number_or_trucks           =   $invoice_detail->number_of_trucks;
            $truck_type_description           =   $invoice_detail->truck_type_description;
            $unit_price           =   $invoice_detail->customer_base_price ;
            $service_charge     =  $invoice_detail->customer_service_charge;
            $status           =   $invoice_detail->status_description;
            $user_id           =   $invoice_detail->user_id;
        }

        $user   =   User::where('id',$user_id)->get();
        foreach($user as $users){
            $user_name  =   $users->name;
            $address  =   $users->address;
        }
        //$user_name  =   User::where('id',$user_id)->value('name');
        //$addre  =   User::where('id',$user_id)->value('name');


        $data   =   [
                        'bill_to'                       => $user_name,
                        'address'                       => $address,
                        'order_number'                  => $order_number,
                        'invoice_number'                => $invoice_number,
                        'date'                          => date("d-m-Y", strtotime($date)),
                        'origin'                        => $origin,
                        'destination'                   => $destination,
                        'number_of_trucks'              => $number_or_trucks,
                        'unit_price'                    => $unit_price,
                        'service_charge'                => $service_charge,
                        'truck_type_description'        => $truck_type_description,
                        'status'                        => $status,
        ];
        $breadcrumb =   'Invoice Details';

        return view('exchange.client.invoice_new',compact('breadcrumb','data'));
    }

    public function updateCustomer(){
        if($this->checkFistLogin()){
            $user_id    =   Auth::user()->id;
            User::where('id',$user_id)
                ->update(
                    array(
                        'is_first_login'    => 0,
                    )
                );
        }
        return redirect('/exchange/customer/home');
    }

    public function completeOrder($reference){
        $order_id    =   Payment::where('reference_number',$reference)->value('exchange_order_id');
        $breadcrumb     =   'Complete Order';
        return view('exchange.client.completeOrder',compact('order_id','breadcrumb'));
    }

    public function finishOrder(Request $request){

        $this->validate($request, [
            'order_id'=>'required',
            'pickupdate'=>'required',
            'pickup_address'=>'required',
            'contact_phone'=>'required',
            'description'=>'required',
        ]);

       $order_id            =   $request->order_id;
       $pickupdate          =   $request->pickupdate;
       $pickup_address      =   $request->pickup_address;
       $contact_phone       =   $request->contact_phone;
       $description         =   $request->description;
        Order::where('id',$order_id)
            ->update(
                array(
                    'pickup_date'    => date("Y-m-d", strtotime($pickupdate)),
                    'item_description'    => $description,
                    'pickup_address'    => $pickup_address,
                    'contact_phone'    => $contact_phone,
                )
            );
        return redirect('/exchange/customer/home')->with('message','Order Complete. We will match you shortly');
    }
}
