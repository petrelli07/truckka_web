<?php

namespace App\Http\Controllers\Exchange;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Exchange\State;
use App\Exchange\ExchangeRoute as Route;
use App\Exchange\ExchangeCoverageArea as Area;
use App\Exchange\ExchangeOrder as Order;
use App\Exchange\ExchangeTruckType as TruckType;
use App\Exchange\ExchangePayment as Payment;
use App\Exchange\TruckSupplierType as SupplierType;
use App\Exchange\TruckSupplier as TruckSupplier;
use App\Exchange\IdleCapacity as IdleCapacity;
use App\Exchange\ExchangeTransporterRequest as TransporterRequest;
use Illuminate\Support\Facades\Auth;
use Validator;
use Session;
use Illuminate\Support\Facades\Mail;
use App\Mail\Exchange\confirmRegistration;
use App\Mail\Exchange\newOrder;

class GeneralController extends Controller
{

    public function exchangeOrderNumber(){
        $i = 1;
        for($j=0; $j < $i; $j++){
            $result = '';
            $length = 7;
            for($x = 0; $x < $length; $x++) {
                $result .= mt_rand(0, 9);
            }

            $checkAvailable = Order::where('order_number', $result);
            if( $checkAvailable->count() < 1 ){
                return $result;
                break;
            }

            $i++;
        }
    }


    public function referenceNumber(){
        $i = 1;
        for($j=0; $j < $i; $j++){
            $result = '';
            $length = 7;
            for($x = 0; $x < $length; $x++) {
                $result .= mt_rand(0, 9);
            }
            $checkAvailable = Payment::where('reference_number', $result);
            if( $checkAvailable->count() < 1 ){
                return $result;
                break;
            }

            $i++;
        }
    }

    public function home(){

        $truckTypes =	TruckType::where('exchange_status_id',7)->get();


        $route 	    =	Route::where('exchange_status_id',1)->pluck('origin_state_id');

        $area 	    =	Area::where('exchange_status_id',5)->get();

        $state      =   State::where('exchange_status_id',5)->get();

        $state_description  =   [];

        for($i=0;$i<count($route);$i++){
            $state_description[]    =   State::where('id',$route[$i])->value('state_description');
        }

        //return $state_description;

        $supplier   =   SupplierType::all();

        $final = array_unique($state_description);
        $uniqueOrigins  =   array_values( $final);

        return view('home.index',compact('truckTypes','uniqueOrigins','area','supplier','state'));

    }

    public function checkOrigins(Request $request)
    {
        $originToServer     = $request->originToServer;

        $origin_id          =   State::where('state_description',$originToServer)->value('id');

        $destination_id 	= Route::where('origin_state_id',$origin_id)->pluck('destination_state_id');

        $destination_description    =   [];

        for($i=0;$i<count($destination_id);$i++){
            $destination_description[]  =   State::where('id',$destination_id[$i])->pluck('state_description');
        }

        return response()->json(['success'=>$destination_description]);

    }

    /*return quote for pricing*/
    public function getQuote(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'origin'=>'required',
            'destination'=>'required',
            'number_of_truck'=>'required|numeric',
            'truckType'=>'required',
        ]);

        if ($validator->passes()) {
            //return $request->all();
            $origin 			= $request->origin;
            $destination 		= $request->destination;
            $resourceType 		= $request->truckType;
            $truckNumberForm 	= $request->number_of_truck;

            $origin_state_id            =   State::where('state_description',$origin)->value('id');
            $destination_state_id       =   State::where('state_description',$destination)->value('id');

            $route  =   Route::where('origin_state_id',$origin_state_id)
                                ->where('destination_state_id',$destination_state_id)
                                ->get();

            $typeOfTruck    =   TruckType::where('id',$resourceType)
                                        ->get();

            foreach($typeOfTruck as $truckType){
                $truckTypeDescription   =   $truckType->truck_type_description;
                $truckTypeID            =   $truckType->id;
            }


           /* $route          =   State::where('origin',$origin)
                                        ->where('destination',$destination)
                                        ->get();*/

            foreach($route as $routes){
                $route_id       =   $routes->id;
                $amount         =   $routes->price;
            }

            $true_amt       =   $amount * $truckNumberForm;

            $vat 			=	$true_amt * 0.05;
            $totalAmount	=	$true_amt + $vat;

            $array = [
                'route_id'					        =>	$route_id,
                'amount'					        =>	$amount,
                'vat'						        =>	$vat,
                'total'						        =>	$totalAmount,
                'truckTypeDescription'				=>	$truckTypeDescription,
                'truckTypeID'				        =>	$truckTypeID
            ];
            return response()->json(['success'=>$array]);
        }else{
            return response()->json(['error'=>$validator->errors()->all()]);
        }
    }

    public function saveQuote(Request $request){
        Session::put('quote', $request->all());
        return redirect('/exchange/unregistered_user')->with('message','Please login or register to view Quote');
    }

    public function register_supplier_capacity(Request $request){

        /*check if user is logged in*/
        if (Auth::check()) {
            // validate form fields
            $this->validate($request, [
                'truckType'=>'required',
                'number_of_truck'=>'required',
                'coverageArea'=>'required',
            ]);

            $truckType          =   $request->truckType;
            $number_of_truck    =   $request->number_of_truck;
            $coverage_area      =   $request->coverageArea;
            $supplier           =   User::find(Auth::user()->id)->TruckSupplier;
            $supplier_id        =   $supplier->id;

            /*add to idle capacity*/
            for($x=0; $x<count($coverage_area); $x++){
                $idleCapacity                               =   new IdleCapacity;
                $idleCapacity->number_of_truck              =   $number_of_truck;
                $idleCapacity->truck_supplier_id            =   $supplier_id;
                $idleCapacity->exchange_truck_type_id       =   $truckType;
                $idleCapacity->exchange_coverage_area_id    =   $coverage_area[$x];
                $idleCapacity->exchange_status_id           =   27;
                $idleCapacity->save();
            }
            /*end insert into idle capacities*/

            $breadcrumb =   'Home';
            /*check the type of supplier and redirect appropriately*/
            if(Auth::user()->userAccessLevel    ==  5){
                return view('exchange.truck_owners.index',compact('breadcrumb'));
            }elseif(Auth::user()->userAccessLevel    ==  6){
                return view('exchange.truck_owners.index',compact('breadcrumb'));
            }
            /*end supplier check*/
        }else{
                Session::put('supplier_details', $request->all());
                return redirect('/exchange/supplier/sign_up_supplier_view');
        }/*end user login check*/

    }

    public function register_customer_view(){
        return view('home.customer_signup');
    }

    public function register_customer(Request $request){

        // validate form fields
        $this->validate($request, [
            'first_name'=>'required',
            'last_name'=>'required',
            'address'=>'required',
            'customer_email'=>'required|email|unique:users,email',
            'password'=>'required|string|min:6|confirmed',
            'phone'=>'required|unique:users,phone',
        ]);
        $first_name =   $request->first_name;
        $last_name  =   $request->last_name;

        $fullname   =   $first_name.' '.$last_name;


        $name               =   $fullname;
        $email              =   $request->customer_email;
        $phone              =   $request->phone;
        $address            =   $request->address;
        $password           =   $request->password;
        $encrypt_pass       =   bcrypt($password);
        $confirmation_code  =   $this->confirmationCode();

        $user               =   new User;
        $user->name                             =   $name;
        $user->email                            =   $email;
        $user->phone                            =   $phone;
        $user->address                          =   $address;
        $user->password                         =   $encrypt_pass;
        $user->confirmation_code                =   $confirmation_code;
        $user->default_password                 =   0;
        $user->userAccessLevel                  =   4;
        $user->is_first_login                   =   0;
        $user->status                           =   2;
        $user->save();

        $user_id    =   $user->id;



        $details    =   [
            'name'=>$first_name,
            'confirmation_code'=>$confirmation_code
        ];

        if(Session::has('quote')) {


            $sessionData    =   Session::get('quote');

            for ($i=0; $i < count($sessionData); $i++) {
                $origin =   $sessionData['origin'];
                $destination =   $sessionData['destination'];
                $pickup_address =   $sessionData['pickup_address'];
                $cargo_description =   $sessionData['cargo_description'];
                $resourceType =   $sessionData['truckType'];
                $truck_capacity =   $sessionData['truckCapacity'];
                $pickup_date =   $sessionData['pickup_date'];
                $delivery_address =   $sessionData['delivery_address'];
                $truckNumber =   $sessionData['number_of_truck'];
                $pickupPlaceID =   $sessionData['pickup_placeId'];
                $deliveryPlaceID =   $sessionData['delivery_placeId'];
            }
            $or            =   State::where('state_description',$origin)->get();

            foreach($or as $origins){
                $origin_state_id    =  $origins->id;
            }

            $dest       =   State::where('state_description',$destination)->get();

            foreach($dest as $destinations){
                $destination_state_id   =   $destinations->id;
            }

            $route  =   Route::where('origin_state_id',$origin_state_id)
                ->where('destination_state_id',$destination_state_id)
                ->get();

            foreach($route as $routes){
                $route_id       =   $routes->id;
                $base_amount   =   $routes->customer_base_price;
                $service_charge   =   $routes->customer_service_charge;
            }

            $amount     =   $base_amount + $service_charge;

            $total_amount   =   $amount * $truckNumber;

            $request_amount     =   $total_amount;

            $ExchangeOrder  =   new Order;

            $ExchangeOrder->order_number                        =   $this->exchangeOrderNumber();
            $ExchangeOrder->number_of_trucks                    =   $truckNumber;
            $ExchangeOrder->user_id                             =   $user_id;
            $ExchangeOrder->exchange_route_id                   =   $route_id;
            $ExchangeOrder->exchange_truck_type_id              =   $resourceType;
            $ExchangeOrder->truck_capacity                      =   $truck_capacity;
            $ExchangeOrder->item_description                    =   $cargo_description;
            $ExchangeOrder->pickup_address                      =   $pickup_address;
            $ExchangeOrder->delivery_address                    =   $delivery_address;
            $ExchangeOrder->pickup_date                         =   $pickup_date;
            $ExchangeOrder->origin_place_id                     =   $pickupPlaceID;
            $ExchangeOrder->destination_place_id                =   $deliveryPlaceID;
            $ExchangeOrder->exchange_status_id                  =   11;
            $ExchangeOrder->save();

            $order_id   =   $ExchangeOrder->id;

            /*record payment*/
            $payment    =   new Payment;
            $payment->reference_number               =   $this->referenceNumber();
            $payment->amount                         =   $request_amount;
            $payment->user_id                        =   $user_id;
            $payment->exchange_order_id              =   $order_id;
            $payment->exchange_status_id             =   16;
            $payment->save();


            User::where('id',$user_id)
                ->update(
                    array(
                        'is_first_login'    => 1,
                    )
                );

            session()->forget('quote');


            Mail::to($email)->send(new confirmRegistration($details));


           return redirect('/login')->with('message','Your registration was successful and an email containing a verification link sent. Click the verification link to confirm your account');

        }else{

            Mail::to($email)->send(new confirmRegistration($details));

            return redirect('/login')->with('message','Your registration was successful and an email containing a verification link sent. Click the verification link to confirm your account');
        }
    }

    public function register_supplier_view(){
        return view('home.truck_owner');
    }

    public function register_new_supplier(Request $request){

        // validate form fields
        $this->validate($request, [
            'name'=>'required',
            'email'=>'required|email|unique:users,email',
            'password'=>'required|string|min:6|confirmed',
            'phone'=>'required|unique:users,phone',
        ]);

        $name       =       $request->name;
        $phone      =      $request->phone;
        $email      =      $request->email;
        $password   =   bcrypt($request->password);

        if(Session::has('supplier_details')){

            /*fetch data from session*/
            $sessionData    =   Session::get('supplier_details');

            $truckType        			                    =   $sessionData['truckType'];
            $number_of_truck        			            =   $sessionData['number_of_truck'];
            $coverage_area            			            =   $sessionData['coverageArea'];
            $supplierType        			                =   $sessionData['supplierType'];

            /*end data fetching*/

            if($sessionData['supplierType'] === "1"){
                $userAccessLevel    =   5;
            }elseif($sessionData['supplierType'] === "2"){
                $userAccessLevel    =   6;
            }

            $user                       =   new User;
            $user->name                 =   $name;
            $user->email                =   $email;
            $user->phone                =   $phone;
            $user->password             =   $password;
            $user->default_password     =   0;
            $user->userAccessLevel      =   $userAccessLevel;
            $user->save();

            $user_id    =  $user->id;

            /*save to truck_suppliers table*/
            $supplier   =   new TruckSupplier;

            $supplier->user_id                  =   $user_id;
            $supplier->truck_supplier_type_id   =   $supplierType;
            $supplier->exchange_status_id       =   25;
            $supplier->save();

            /*end save to truck_suppliers table and get id of last inserted*/
            $supplier_id    =   $supplier->id;

            for($x=0; $x<count($coverage_area); $x++){
                $idleCapacity                               =   new IdleCapacity;
                $idleCapacity->number_of_truck              =   $number_of_truck;
                $idleCapacity->truck_supplier_id            =   $supplier_id;
                $idleCapacity->exchange_truck_type_id       =   $truckType;
                $idleCapacity->exchange_coverage_area_id    =   $coverage_area[$x];
                $idleCapacity->exchange_status_id           =   27;
                $idleCapacity->user_id                      =   $user_id;
                $idleCapacity->save();
            }

            if($sessionData['supplierType'] === "2"){
                $wallet =   new Wallet;
                $wallet->amount                 =   0;
                $wallet->user_id                =   $user_id;
                $wallet->exchange_status_id     =   23;
                $wallet->save();
            }

            session()->forget('supplier_details');

            return redirect('/login');
        }else{
            return redirect('/login');
        }

    }

    public function submitRequest(Request $request){
        $validator = Validator::make($request->all(), [
            'email_address'=>'required|email',
            'coverageArea'=>'required',
            'phone_number'=>'required',
            'transporter_name'=>'required',
        ]);

        $email          =   $request->email_address;
        $coverageArea   =   $request->coverageArea;
        $phone_number   =   $request->phone_number;
        $transporter_name   =   $request->transporter_name;

        //if validate passes
        if ($validator->passes()){
            $transporter    =   new TransporterRequest;
            $transporter->transporter_name  =   $transporter_name;
            $transporter->transporter_email  =   $email;
            $transporter->transporter_phone  =   $phone_number;
            $transporter->transporter_route  =   json_encode($coverageArea);
            $transporter->exchange_status_id  =  11;
            $transporter->save();
            return response()->json(['success'=>'Successful! Someone from our team will contact you shortly']);

        }else{
            return response()->json(['error'=>$validator->errors()->all()]);
        }
    }

    public function unregisteredUser(){
        if(Session::has('quote')) {
            return view('exchange.home.unregistered');
        }else{
            return back();
        }
    }

    public function confirmationCode(){
        $i = 1;
        for($j=0; $j < $i; $j++){
            $result = '';
            $length = 7;
            for($x = 0; $x < $length; $x++) {
                $result .= mt_rand(0, 9);
            }

            $checkAvailable = User::where('confirmation_code', $result);
            if( $checkAvailable->count() < 1 ){
                return $result;
                break;
            }

            $i++;
        }
    }

    public function testMail(){
        return view('emails.exchange.confirm_registration');
    }

    public function confirmAccount($confirmation_code){
        User::where('confirmation_code',$confirmation_code)
            ->update(
                array(
                    'status'    => 1,
                )
            );
        return redirect('/login')->with('message','Your account has been confirmed. Please Login to view Quote.');
    }
}
