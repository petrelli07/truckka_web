<?php

namespace App\Http\Controllers\CarrierControllers;

use App\CarrierResourceDetails;
use App\CarrierUser;
use App\ClientOrder;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\Input;

class TrackingController extends Controller
{

    public $userId;
    public $companyID;
    public $truckDetail;
    public $orderNumber;
    public $orderOrigin;
    public $orderDest;
    public $resourceId;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function userInit()
    {

        $userID = Auth::user()->id;

        $companyID = CarrierUser::where('user_id', $userID)
            ->value('company_id');
        $this->userId = $userID;
        $this->companyID = $companyID;

    }

    public function allResources()
    {

        $allResources = CarrierResourceDetails::join('carrier_company_details', 'carrier_company_details.id', '=', 'carrier_resource_details.company_id');
        return $allResources;

    }

    public function getAttachedResources($value=''){
      
      $this->userInit();
      $allResources =$this->allResources()
                           ->where('company_id', '=', $this->companyID)
                           ->join('order_resources','order_resources.resource_id','=','carrier_resource_details.id')
                           ->join('service_requests','service_requests.id','=','order_resources.order_id')
                           ->groupBy('carrier_resource_details.id')
                           ->get();

      $data         = array(
                            'resources' => $allResources
                           );
      return view('carrier.tracker.allActiveResources',$data);
    }

    public function sortCompanyResources($companyID)
    {

        $resourceDetail = $this->allResources()
                               ->where('company_id', '=', $companyID);

        // Run query if user sorts by IMEI or PlateNUmber

        if (!is_null($this->truckDetail)) {
            $resourceDetail = $resourceDetail->where(function ($query) {

                $query->where('GPSID', '=', $this->truckdetail)
                      ->orWhere('plateNumber', '=', $this->truckdetail);
            });
        }

        // Run Query if user sorts based on Orders

        if (!is_null($this->orderNumber) || !is_null($this->orderOrigin) || !is_null($this->orderDest)) {

            $resourceDetail = $resourceDetail->join('order_resources', 'order_resources.resource_id', '=', 'carrier_resource_details.id')
                                            ->join('service_requests', 'service_requests.id', '=', 'order_resources.order_id');

        }

        // Run Query if user sorts based on order number

        if (!is_null($this->orderNumber)) {

            $resourceDetail = $resourceDetail->where('serviceIDNo', '=', $this->orderNumber);

        }

        // Run Query if user sets orderOrigin

        if (!is_null($this->orderOrigin)) {

            $resourceDetail = $resourceDetail->where('deliverFrom', '=', $this->orderOrigin);

        }

        // Run Query if user sets orderDestination

        if (!is_null($this->orderDest)) {

            $resourceDetail = $resourceDetail->where('deliverTo', '=', $this->orderDest);

        }

        //Sort by resourceId

        if (!is_null($this->resourceId)) {

            $resourceDetail = $resourceDetail->where('carrier_resource_details.id', '=', $this->resourceId);

        }

        return $resourceDetail->groupBy('carrier_resource_details.id')
            ->get();
    }

    public function getCarrierResourceView($resourceId = null)
    {

        $this->userInit();

        $this->truckdetail = Input::get('truckdetail');
        $this->orderNumber = Input::get('orderNumber');
        $this->orderOrigin = Input::get('orderOrigin');
        $this->orderDest = Input::get('orderDest');
        $this->resourceId = $resourceId;

        $locations = ClientOrder::get();

        $origins = array_unique(array_column($locations->toArray(), 'origin'));

        $destinations = array_unique(array_column($locations->toArray(), 'destination'));

        $resourcesDetails = $this->sortCompanyResources($this->companyID);

        //define map info
        $mapinfo = [];

        //extra info
        foreach ($resourcesDetails as $info) {
            $mapinfo[] = array(
                'id' => $info->resource_id,
                'name' => $info->contactName,
                'label' => $info->itemDescription,
                'gpsid' => $info->GPSID,
            );
        }

        // var_dump($resourcesDetails->toArray());
        // exit;
        $data = array(
            'mapinfo' => $mapinfo,
            'data' => $resourcesDetails,
            'locations' => array_unique(array_merge($origins, $destinations)),
        );

        return view('carrier.tracker.tracker', $data);
    }

}
