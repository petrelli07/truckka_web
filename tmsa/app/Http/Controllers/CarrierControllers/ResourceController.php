<?php

namespace App\Http\Controllers\CarrierControllers;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\DB; 
use Illuminate\Http\Request;
use Auth;
use Validator;
use App\CarrierUser;
use App\User;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class ResourceController extends BaseController{

	public function addResource(){
      $data                       = array();
      $data['resource_capacity']  = DB::table('resource_capacity')->get();
    	return view('carrier.add_resource',$data);
    }

  public function addnewResource(Request $details){


    	$validator = Validator::make($details->all(), [
            'platenumber'			=>	'required',
            'capacity'				=>	'required',
            'drivername'			=>	'required',
            'drivernumber'		=>	'required',
            'imei'            =>  'required'
      ]);

      if ($validator->passes()) {
      	 $userID     = Auth::user()->id;
         $carrierId  = CarrierUser::where('user_id',$userID)->value('company_id');
      	 $resourceDetails	=	array(
                                  'plateNumber'	     =>	$details->platenumber,
      														'driverName'	     =>	$details->drivername,
      														'driverPhoneNumber'=>	$details->drivernumber,
      														'capacity'		     =>	$details->capacity,
      														'company_id'	     =>	$carrierId,
                                  'GPSID'            => $details->imei
      													);

      	$saveResource 		= DB::table('carrier_resource_details')->insert($resourceDetails);
      }

    }

    public function viewAllResource(){
      $userID     = Auth::user()->id;
      $carrierId  = CarrierUser::where('user_id',$userID)->value('company_id');
      $data                       = array();
      $data['user_resources']     = DB::table('carrier_resource_details')
                                  ->select('carrier_resource_details.*', 'capacity.capacity_value')
                                  ->leftJoin('resource_capacity as capacity','capacity.id','=','carrier_resource_details.capacity')
                                  ->where('company_id',$carrierId)
                                  ->get();
      // var_dump($data['user_resources']);
      // exit; 
      return view('carrier.orders.myResources',$data); 
    }

    public function editResource(Request $resources,$singleResource = NULL){
       $userID  = Auth::user()->id;
      $company_id = CarrierUser::where('user_id',$userID)->value('company_id');
       if(is_null($singleResource)){ 

         $allResource      =  $resources->resource;

       }else{

        $allResource       =  array($singleResource);

       }

       $data['resource_capacity']  = DB::table('resource_capacity')->get();

       $data['user_resources']     = DB::table('resource_gps_details')
                                    ->join('resource_capacity','resource_capacity.id','=','resource_gps_details.capacity')
                                    ->where('user_id',$company_id)
                                    ->where(function ($query) use ($allResource){
                                      for ($i=0; $i < sizeof($allResource); $i++) {
                                        if ($i == 0) {
                                          $query->where('resource_gps_details.id','=',$allResource[$i]);
                                        }else{
                                          $query->orwhere('resource_gps_details.id','=',$allResource[$i]);
                                        }
                                      }
                                    })
                                    ->get();

      if ($data['user_resources']) {
          return view('carrier.editResource',$data);
      }else{
          return redirect()->route('all-resources');
      }

    }

    public function saveEditResource(Request $details){
      $validator = Validator::make($details->all(), [
            'platenumber'     =>  'required',
            'capacity'        =>  'required',
            'drivername'      =>  'required',
            'drivernumber'    =>  'required',
      ]);

      if ($validator->passes()) {
        $userID     = Auth::user()->id;
        $company_id = CarrierUser::where('user_id',$userID)->value('company_id');
        $resourceDetails  = array(
                                  'driverName'  =>  $details->drivername,
                                  'driverNumber'=>  $details->drivernumber,
                                  'capacity'    =>  $details->capacity,
                                );  
        $saveResource     = DB::table('resource_gps_details')
                                ->where('user_id',$carrierId)
                                ->where('plateNumber',$details->platenumber)
                                ->update($resourceDetails);
        if ($saveResource) {
         echo 'success';
        }
      }

    }

    public function deactivateResource(Request $resources,$singleResource = NULL){

       $userID            =  Auth::user()->id;
       $company_id        =  CarrierUser::where('user_id',$userID)->value('company_id');

       if(is_null($singleResource)){      
         $allResource      =  $resources->resource;
       }else{
        $allResource       =  array($singleResource);
       }

       $resourceDetails  = array(
                                  'resourceStatus'  =>  1,
                                );

       for ($i=0; $i < sizeof($allResource) ; $i++) {
         $updateStatus  = DB::table('carrier_resource_details')
                                ->where('company_id',$company_id)
                                ->where('id',$allResource[$i])
                                ->update($resourceDetails);
       }

       return redirect('/carrier/resources');
       /*header('Location:'.url('/all-resources'));
       exit;*/
    }

    public function activateResource(Request $resources,$singleResource = NULL){
       $carrierId        =  Auth::user()->id;

       if(is_null($singleResource)){      
         $allResource      =  $resources->resource;
       }else{
        $allResource       =  array($singleResource);
       }
       $resourceDetails  = array(
                                  'resourceStatus'  =>  0,
                                );

       for ($i=0; $i < sizeof($allResource) ; $i++) {
         $updateStatus  = DB::table('resource_gps_details')
                                ->where('user_id',$carrierId)
                                ->where('id',$allResource[$i])
                                ->update($resourceDetails);
       }

       return redirect('/all-resources');
       /*header('Location:'.url('/all-resources'));
       exit;*/
    }
}
