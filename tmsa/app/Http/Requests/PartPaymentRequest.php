<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PartPaymentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'waybill_number'=>'required|unique:truck_order_registries,waybill_number',
            'origin'=>'required',
            'destination'=>'required',
            'plate_number'=>'required',
            'waybill_file'=>'required|file|mimes:jpeg,jpg,png,pdf,docx,doc|max:2048',
        ];
    }
}
