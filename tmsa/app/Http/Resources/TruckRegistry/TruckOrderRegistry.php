<?php

namespace App\Http\Resources\TruckRegistry;

use Illuminate\Http\Resources\Json\JsonResource;

class TruckOrderRegistry extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'registry_number'=>$this->registry_number,
        ];
    }
}
