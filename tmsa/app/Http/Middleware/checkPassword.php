<?php

namespace App\Http\Middleware;

use Closure;
use Hash;
use Auth;

class checkPassword
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->default_password != "0") {
            return redirect('/changeYourPass');
        }
        return $next($request);
    }
}
