<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IncidentReport extends Model
{
    public function IncidentReportDetail() {
        return $this->hasMany('App\IncidentReportDetail');
    }
}
