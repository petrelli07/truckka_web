<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class spotOrderRoutePricing extends Model
{
    public function spotOrder()
    {
    	return $this->hasMany('App\spotOrder');
    }

    public function sorpStatus()
    {
    	return $this->belongsTo('App\sorpStatus');
    }
}
