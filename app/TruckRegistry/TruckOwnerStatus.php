<?php

namespace App\TruckRegistry;
use App\TruckRegistry\TruckOwner;

use Illuminate\Database\Eloquent\Model;

class TruckOwnerStatus extends Model
{
    public function TruckOwner(){
        return $this->hasMany(TruckOwner::class);
    }
}
