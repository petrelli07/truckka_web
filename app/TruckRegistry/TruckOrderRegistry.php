<?php

namespace App\TruckRegistry;
use App\TruckRegistry\RegistryStatus;
use App\TruckRegistry\Truck;

use Illuminate\Database\Eloquent\Model;

class TruckOrderRegistry extends Model
{
    public function RegistryStatus(){
        return $this->belongsTo(RegistryStatus::class);
    }

    public function Truck(){
        return $this->hasMany(Truck::class);
    }
}
