<?php

namespace App\TruckRegistry;
use App\TruckRegistry\Truck;

use Illuminate\Database\Eloquent\Model;

class TruckStatus extends Model
{
    public function Truck(){
        return $this->hasMany(Truck::class);
    }
}
