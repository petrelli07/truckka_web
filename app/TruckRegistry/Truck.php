<?php

namespace App\TruckRegistry;
use App\TruckRegistry\TruckStatus;
use App\TruckRegistry\TruckOwner;
use App\TruckRegistry\TruckOrderRegistry;

use Illuminate\Database\Eloquent\Model;

class Truck extends Model
{
    public function TruckStatus(){
        return $this->belongsTo(TruckStatus::class);
    }

    public function TruckOwner(){
        return $this->belongsTo(TruckOwner::class);
    }

    public function TruckOrderRegistry(){
        return $this->belongsTo(TruckOrderRegistry::class);
    }
}
