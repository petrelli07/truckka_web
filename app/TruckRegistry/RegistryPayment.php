<?php

namespace App\TruckRegistry;
use App\TruckRegistry\RegistryPaymentStatus;

use Illuminate\Database\Eloquent\Model;

class RegistryPayment extends Model
{
    public function RegistryPaymentStatus(){
        return $this->belongsTo(RegistryPaymentStatus::class);
    }
}
