<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class spotOrderStatus extends Model
{
    public function spotOrder()
    {
        return $this->hasMany('App\spotOrder');
    }
}
