<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class spotOrderReview extends Model
{
    public function spotOrder()
    {
    	return $this->belongsTo('App\spotOrder');
    }

    public function spotOrderReviewStatus()
    {
    	return $this->belongsTo('App\spotOrderReviewStatus');
    }

}
