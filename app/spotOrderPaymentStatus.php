<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class spotOrderPaymentStatus extends Model
{
    
    public function spotOrderPayment()
    {
    	return $this->hasMany('App\spotOrderPayment');
    }
}
