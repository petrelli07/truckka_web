<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class spotOrderInvoiceStatus extends Model
{
    public function spotOrderInvoice()
    {
    	return $this->hasMany('App\spotOrderInvoice');
    }
}
