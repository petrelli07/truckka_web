<?php

//354589073410155
function device_add($uniqueId="354589073410148",$name="dhtml",$id='-1',$phone='',$model='',$category='',$attributes='',$groupId=0) {
  $data = array(
    'id'=>$id,
    'name'=>$name,
    'uniqueId'=>$uniqueId,
    'phone'=>$phone,
    'model'=>$model,
    'groupId'=>$groupId,
    'category'=>$category,
    'attributes'=>$attributes
  );

  $data = array(
    'name'=>$name,
    'uniqueId'=>$uniqueId,
  );

  $result=trace_curl('devices',$data);

  if(!is_object($result)) {
    $response=array(
      'status'=>201,
      'message'=>"Device previously added",
    );
  } else {
    $response=array(
      'status'=>200,
      'message'=>"device added",
      'data'=>$result,
    );
  }

  return (object) $response;
}

function device_del($id="123") {
   return trace_del('devices/'.$id);
}


function toIso($time, $timeZone=null) {
  $time = is_numeric($time) ? $time : strtotime($time);
  $dateInLocal = date("Y-m-d H:i:s", $time);
  if($timeZone==null) {
    $dt = new DateTime($dateInLocal);
  } else {
    $dt = new DateTime($dateInLocal, new DateTimeZone($timeZone));
  }
  $dt->setTimezone(new DateTimeZone('UTC'));
  $utcTime = $dt->format('Y-m-d H:i:s');
  $returnObject = new DateTime($utcTime);
  $returnIso = substr($returnObject->format(DateTime::ATOM), 0, -6) . '.000Z';
  return $returnIso;
}


function trace_get($method,$params=array()) {
  $api=new TraceAPI();
  return $api->get($method,$params);
}

function trace_del($method) {
  $api=new TraceAPI();
  $result=$api->delt($method);
  if($result=='') {$result=['200'=>'success'];} else {$result=['404'=>'device not found'];}
  return (object) $result;
}

function trace_post($method,$params=array()) {
  $api=new TraceAPI();
  return $api->post($method,$params);
}

function trace_curl($method,$params='') {
  $api=new TraceAPI();
  return $api->curl($method,$params);
}

//process api result, find fields
function process_result($result,$fields) {

$data=[];
foreach($result as $info) {
$item=[];

foreach($fields as $field) {
$item["$field"]=$info->$field;
}

$data[]=$item;
}

return $data;
}

//build an array of a particular field inside multiple results
function build_parameters($result,$field) {
  $data=[];
  foreach($result as $info) {
  $info=(object) $info;
  $data[]=$info->$field;
  }

  return $data;
}
