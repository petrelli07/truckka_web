<?php
function find_gps_route($uniqueId) {
  if(is_array($uniqueId)) {
    $response=[];
    foreach($uniqueId as $device) {
      $response[$device]=find_gps_route($device);
    }
    return $response;
  }

  $result=array();

  //fetch matching device
  $device=trace_get('devices',array('uniqueId'=>$uniqueId));
  if($device==null) {return $result;}

  var_dump($device);


  $result['id']=$device[0]->id;
  $result['name']=$device[0]->name;
  $result['status']=$device[0]->status;
  $result['disabled']=$device[0]->disabled;
  $result['positionId']=$device[0]->positionId;

  //try find routes
  $routes=trace_get('reports/route',['deviceId'=>$result['id']]);
  //if($routes==null) {return $result;}

  var_dump($routes);
  die();

  $result['lat']=$location[0]->latitude;
  $result['lng']=$location[0]->longitude;
  $result['address']=$location[0]->address;
  $result['success']=true;

  return $result;

}


function find_gps_location($uniqueId) {
  if(is_array($uniqueId)) {
    $response=[];
    foreach($uniqueId as $device) {
      $response[$device]=find_gps_location($device);
    }
    return $response;
  }
  $result=array(
    'name'=>'',
    'uniqueId'=>$uniqueId,
    'status'=>'',
    'lng'=>0,
    'lat'=>0,
    'positionId'=>null,
    'address'=>null,
    'disabled'=>true,
    'success'=>false,
  );

  //fetch matching device
  $device=trace_get('devices',array('uniqueId'=>$uniqueId));
  if($device==null) {return $result;}

  $result['name']=$device[0]->name;
  $result['status']=$device[0]->status;
  $result['disabled']=$device[0]->disabled;
  $result['positionId']=$device[0]->positionId;

  //try find location
  $location=trace_get('positions',['id'=>$result['positionId']]);
  if($location==null) {return $result;}

  $result['lat']=$location[0]->latitude;
  $result['lng']=$location[0]->longitude;
  $result['address']=$location[0]->address;
  $result['success']=true;

  return $result;

}
