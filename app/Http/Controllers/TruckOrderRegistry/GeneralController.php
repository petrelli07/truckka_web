<?php

namespace App\Http\Controllers\TruckOrderRegistry;

use Illuminate\Http\Request;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class GeneralController extends Controller
{
   public function reset_password(Request $request){
       // validate form fields
       $this->validate($request, [
           'password'=>'required|string|min:6|confirmed',
       ]);
       $new_password    =   bcrypt($request->password);
       $user_id =   Auth::user()->id;

       User::where('id',$user_id)
           ->update(
               array(
                   'password' => $new_password
               )
           );

       return back()->withMessage('Password Reset Successful');
   }
}
