<?php

namespace App\Http\Controllers\TruckOrderRegistry\FieldOps;

use App\Bank;
use App\TruckRegistry\Truck;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\TruckRegistry\TruckOrderRegistry;
use App\TruckRegistry\RegistryRoutes;
use App\TruckRegistry\TruckOrderRegistryAudit;
use App\TruckRegistry\TruckOwner;
use App\User;
use Auth;
use Image;
use App\Notifications\TruckOrderRegistry\NewPartPaymentTransaction;
use App\Notifications\TruckOrderRegistry\NewBalancePaymentTransaction;


class FieldOperationsController extends Controller
{

//route and transporter statuses
    public $active      =   1; 
    public $inactive    =   2; 

// payment statuses
	public $partPaymentRequestStatus 			= 1;

	public $partPaymentOpApprovedStatus 		= 2;

	public $partPaymentFinApprovedtatus 		= 3;

	public $balancePaymentRequestStatus 		= 4;

	public $balancePaymentOpApprovedStatus 		= 5;

	public $balancePaymentFinApprovedStatus 	= 6;

	public $paymentComplete 					= 7;

    public $partPaymentDeclinedOps                          = 8;
    public $partPaymentDeclinedFin                          = 9;
    public $balancePaymentDeclinedOps                       = 10;
    public $balancePaymentDeclinedFin                       = 11;

//end payment statuses


    public function __construct()
    {
        $this->middleware('auth');
    }

    public function audit($message){
        $audit  =   new TruckOrderRegistryAudit;
        $audit->user_id     =   Auth::user()->id;
        $audit->description =   $message;
        $audit->save();
    }

    //send sms
    public function sendSms($phoneNumber,$message){
        $encoded_message    =   urlencode($message);
        $result = array();
        $url = 'https://www.bulksmsnigeria.com/api/v1/sms/create?api_token=Lk6hs0SzNhQJfVFOisboSBcq2VSX0CJehOdtC5diccvPyzUlUE7qE2a0Co82&from=Truckka&to='.$phoneNumber.'&body='.$encoded_message.'&dnd=2';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $request = curl_exec($ch);

        curl_close($ch);
        if ($request) {
           $result = json_decode($request, true);
        }
    }

    public function user_id(){
        return Auth::user()->id;
    }

	//////////// random registry number
	public function registryNumber(){
        $i = 1;
        for($j=0; $j < $i; $j++){
            $result = '';
            $length = 7;
            for($x = 0; $x < $length; $x++) {
                $result .= mt_rand(0, 9);
            }

            $checkAvailable = TruckOrderRegistry::where('registry_number', $result);
            if( $checkAvailable->count() < 1 ){
                return $result;
                break;
            }

            $i++;
        }
    }

    //get phone number for operations
    public function getOperationPhone(){
        $operations =   User::where('userAccessLevel',9)->value('phone');
        return $operations;
    }

    //get phone number for finance
    public function getFinancePhone(){
        $operations =   User::where('userAccessLevel',8)->value('phone');
        return $operations;
    }


	//  check if waybill number exists in database table and return count
	public function get_payment_status($waybill_number)
	 {
	 	// check if waybill number has requested part payment
	 	$checkPartPaymentStatus 			= TruckOrderRegistry::where('waybill_number',$waybill_number)
	 											->where('registry_status_id',$this->partPaymentRequestStatus)
	 											->get();

	 	// check if waybill number has part payment approved by operations
	 	$checkPartPaymentOpApprovedStatus 	= TruckOrderRegistry::where('waybill_number',$waybill_number)
	 											->where('registry_status_id',$this->partPaymentOpApprovedStatus)
	 											->get();

	 	// check if waybill number has part payment approved by finOps
	 	$checkPartPaymentFinApprovedStatus 	= TruckOrderRegistry::where('waybill_number',$waybill_number)
	 											->where('registry_status_id',$this->partPaymentFinApprovedtatus)
	 											->get();

	 	/////////////////// check if waybill number has requested balance
	 	$checkBalancePaymentApprovedStatus 	= TruckOrderRegistry::where('waybill_number',$waybill_number)
	 											->where('registry_status_id',$this->balancePaymentRequestStatus)
	 											->get();

	 	/////////////////// check if waybill number has balance payment approved by operations
	 	$checkBalancePaymentOpApprovedStatus = TruckOrderRegistry::where('waybill_number',$waybill_number)
	 											->where('registry_status_id',$this->balancePaymentOpApprovedStatus)
	 											->get();

	 	/////////////////// check if waybill number has balance payment approved by finOps
	 	$checkBalancePaymentFinApprovedStatus = TruckOrderRegistry::where('waybill_number',$waybill_number)
	 											->where('registry_status_id',$this->balancePaymentFinApprovedStatus)
	 											->get();

	 	/////////////////// check if waybill number has payment complete
	 	$checkPaymentComplete					= TruckOrderRegistry::where('waybill_number',$waybill_number)
	 											->where('registry_status_id',$this->paymentComplete)
	 											->get();

	 	/////////////////// check if part payment was declined by ops
	 	$partPaymentDeclinedOps					= TruckOrderRegistry::where('waybill_number',$waybill_number)
	 											->where('registry_status_id',$this->partPaymentDeclinedOps)
	 											->get();

	 	/////////////////// check if part payment was declined by finance
	 	$partPaymentDeclinedFin					= TruckOrderRegistry::where('waybill_number',$waybill_number)
	 											->where('registry_status_id',$this->partPaymentDeclinedFin)
	 											->get();

	 	/////////////////// check if balance payment was declined by ops
	 	$balancePaymentDeclinedOps					= TruckOrderRegistry::where('waybill_number',$waybill_number)
                                                    ->where('registry_status_id',$this->balancePaymentDeclinedOps)
                                                    ->get();

	 	/////////////////// check if part payment was declined by ops
	 	$balancePaymentDeclinedFin					= TruckOrderRegistry::where('waybill_number',$waybill_number)
	 											->where('registry_status_id',$this->balancePaymentDeclinedFin)
	 											->get();

	 	$statusQueryResults 	=	[
	 									'checkPartPaymentStatus' 				=> 	count($checkPartPaymentStatus),
	 									'checkPartPaymentOpApprovedStatus'		=>	count($checkPartPaymentOpApprovedStatus),
	 									'checkPartPaymentFinApprovedStatus'		=>	count($checkPartPaymentFinApprovedStatus),
	 									'checkBalancePaymentApprovedStatus'		=>	count($checkBalancePaymentApprovedStatus),
	 									'checkBalancePaymentOpApprovedStatus'	=>	count($checkBalancePaymentOpApprovedStatus),
	 									'checkBalancePaymentFinApprovedStatus'	=>	count($checkBalancePaymentFinApprovedStatus),
	 									'checkPaymentComplete'					=>	count($checkPaymentComplete),
	 									'partPaymentDeclinedOps'				=>	count($partPaymentDeclinedOps),
	 									'partPaymentDeclinedFin'				=>	count($partPaymentDeclinedFin),
	 									'balancePaymentDeclinedOps'				=>	count($balancePaymentDeclinedOps),
	 									'balancePaymentDeclinedFin'				=>	count($balancePaymentDeclinedFin),
	 								];

	 	return $statusQueryResults;
	 } 

	 public function confrm_part_payment_status_result($status)
	 {

    		//////// status results
	    	$checkPartPaymentStatus 					= $status['checkPartPaymentStatus'];
	    	$checkPartPaymentOpApprovedStatus 			= $status['checkPartPaymentOpApprovedStatus'];
	    	$checkPartPaymentFinApprovedStatus 			= $status['checkPartPaymentFinApprovedStatus'];
	    	$checkBalancePaymentApprovedStatus 			= $status['checkBalancePaymentApprovedStatus'];
	    	$checkBalancePaymentOpApprovedStatus 		= $status['checkBalancePaymentOpApprovedStatus'];
	    	$checkBalancePaymentFinApprovedStatus 		= $status['checkBalancePaymentFinApprovedStatus'];
	    	$checkPaymentComplete					 	= $status['checkPaymentComplete'];
	    	$partPaymentDeclinedOps					 	= $status['partPaymentDeclinedOps'];
	    	$partPaymentDeclinedFin					 	= $status['partPaymentDeclinedFin'];

	 	if(
	 		$checkPartPaymentStatus 					=== 0 
	 		&& $checkPartPaymentOpApprovedStatus 		=== 0 
	 		&& $checkPartPaymentFinApprovedStatus 		=== 0 
	 		&& $checkBalancePaymentApprovedStatus 		=== 0 
	 		&& $checkBalancePaymentOpApprovedStatus 	=== 0 
	 		&& $checkBalancePaymentFinApprovedStatus 	=== 0 
	 		&& $checkPaymentComplete 					=== 0
            || $partPaymentDeclinedOps                  === 1
            || $partPaymentDeclinedFin                  === 1
	 		)
	 	{
	 		$stausMessage = 'Confirmed';
	 		return $stausMessage;
    	}else{
	 		$statusMessage = 'Failed';
	 		return $statusMessage;
    	}

	 }

	 public function confrm_balance_payment_status_result($status)
	 {

    		//////// status results
	    	$checkPartPaymentStatus 					= $status['checkPartPaymentStatus'];
	    	$checkPartPaymentOpApprovedStatus 			= $status['checkPartPaymentOpApprovedStatus'];
	    	$checkPartPaymentFinApprovedStatus 			= $status['checkPartPaymentFinApprovedStatus'];
	    	$checkBalancePaymentApprovedStatus 			= $status['checkBalancePaymentApprovedStatus'];
	    	$checkBalancePaymentOpApprovedStatus 		= $status['checkBalancePaymentOpApprovedStatus'];
	    	$checkBalancePaymentFinApprovedStatus 		= $status['checkBalancePaymentFinApprovedStatus'];
	    	$checkPaymentComplete					 	= $status['checkPaymentComplete'];
            $balPaymentDeclinedOps					 	= $status['partPaymentDeclinedOps'];
            $balPaymentDeclinedFin					 	= $status['partPaymentDeclinedFin'];

	 	if(
	 		$checkPartPaymentStatus 					=== 0 
	 		&& $checkPartPaymentOpApprovedStatus 		=== 0 
	 		&& $checkPartPaymentFinApprovedStatus 		=== 1 
	 		&& $checkBalancePaymentApprovedStatus 		=== 0 
	 		&& $checkBalancePaymentOpApprovedStatus 	=== 0 
	 		&& $checkBalancePaymentFinApprovedStatus 	=== 0
	 		&& $checkPaymentComplete 					=== 0
            || $balPaymentDeclinedOps                   === 1
            || $balPaymentDeclinedFin                   === 1
	 		)
	 	{
	 		$stausMessage = 'Confirmed';
	 		return $stausMessage;
    	}else{
	 		$statusMessage = 'Failed';
	 		return $statusMessage;
    	}

	 }

	//////////////////  default landing page for field agents
    public function index()
    {
        $message    =   "Logged in";
        $this->audit($message);
    	$breadcrumb 	=	'Home';
    	return view('registry.fieldAgent.index',compact('breadcrumb'));
    }

    /////////////////////  part payment request view
    public function request_part_payment_view()
    {
        $routes = RegistryRoutes::where('registry_route_status_id',$this->active)->get();
    	$breadcrumb 	=	'Home';
    	return view('registry.fieldAgent.part_payment_request',compact('breadcrumb','routes'));
    }

// process part payment request form
    public function process_part_payment(Request $request)
    {

    	// validate form fields
    	$this->validate($request, [
            'waybill_number'=>'required|unique:truck_order_registries,waybill_number',
            'origin'=>'required',
            'destination'=>'required',
            'plate_number'=>'required',
            'waybill_file'=>'required|file|mimes:jpeg,jpg,png,pdf,docx,doc|max:512',
        ]);

        //check if truck exists in database
        $plateNumberQuery   =   Truck::where('plate_number',$request->plate_number)->get();
        $plateNumberCount  =   count($plateNumberQuery);

        if($plateNumberCount < 1){
            return back()->withMessage('This truck has not been registered!');
        }
        //end check

        //retrieve id of truck
        foreach($plateNumberQuery as $plateNumber){
            $truck_id   =   $plateNumber->id;
        }
        //end retrieve id of truck

        //query waybill number payment status
    	$status = $this->get_payment_status($request->waybill_number);

    	//confirm waybill number payment status
    	$status_confirmation = $this->confrm_part_payment_status_result($status);

    	// action after status is confirmed
    	if ($status_confirmation === 'Confirmed') {
    		
        // assign values to variables
        $waybill_number 			= 	$request->waybill_number;
        $origin 					= 	$request->origin;
        $destination 				= 	$request->destination;
        $recipient_phone_number     =   $request->recipient_phone_number;
        $amount_field_agent         =   $request->amount;
        $waybill_file				= 	$request->file('waybill_file');
        //$waybill_file				= 	$request->file('waybill_file')->store('public/registry_waybillFiles');
        $registry_status_id 		= 	$this->partPaymentRequestStatus;
        $registry_number 			=	$this->registryNumber();

            $originalImage  = $waybill_file;
            $thumbnailImage = Image::make($originalImage);
            $originalPath = public_path().'/waybillfiles/';
            $thumbnailImage->save($originalPath.$waybill_number.$originalImage->getClientOriginalName());
        //check if origin destination exists
        $checkRoute =   RegistryRoutes::where('origin',$origin)
                        ->where('destination',$destination)
                        ->where('registry_route_status_id',$this->active)
                        ->get();

        if (count($checkRoute) < 1) {
            return back()->withMessage('Error.This Route does not exist!');
        }

            //get price from registry_routes table
            foreach($checkRoute as $route){
                $amount_route   =   $route->price;
            }

            //compare price entered by field agent with price loaded from registry_routes table
            if($amount_field_agent <= $amount_route){
                 $final_amount  =   $amount_field_agent;
            }elseif($amount_field_agent > $amount_route){
                return back()->withMessage('Error.This amount entered is not correct for this transaction!');
            }

        $user_id	=	Auth::user()->id;

        //insert record into DB
        $save_record 				=	new TruckOrderRegistry;

        $save_record->registry_number 							    =	$registry_number;
        $save_record->agent_id 									    =	$user_id;
        $save_record->origin									    =	$origin;
        $save_record->destination								    =	$destination;
        $save_record->recipient_phone_number						=	$recipient_phone_number;
        $save_record->waybill_number							    =	$waybill_number;
        $save_record->waybill_file_origin						    =	$originalImage->getClientOriginalName();//$waybill_file;
        $save_record->registry_status_id						    =	$registry_status_id;
        $save_record->truck_id						                =	$truck_id;
        $save_record->amount						                =	$final_amount;

	    $save_record->save();

        //send ops message
        $user_name	=	Auth::user()->name;
        $message        =   'New Part Payment request sent by '.$user_name;
        $opsPhoneNumber =   $this->getOperationPhone();

        //call send sms method
        $this->sendSms($opsPhoneNumber,$message);

            $auditmessage    =   "New Part Payment Request";
            $this->audit($auditmessage);

            $user = User::find($user_id);
            $notification = $user->notify(new NewPartPaymentTransaction($waybill_number));
    		//return success message
    		return back()->withMessage('Successful.Please wait for approval');

    	}else{
    		//return error message
    		return back()->withMessage('This Waybill has been processed for payment!');
    	}
    }

    // part payment successful views
    public function all_payment_requests_view()
    {
    	$breadcrumb = 'All Requests';
    	$user_id 	=	Auth::user()->id;

    	//send requests and statuses to view
    	$requests 	= DB::table('truck_order_registries')
                          ->join('registry_statuses','registry_statuses.id','=',
                                  'truck_order_registries.registry_status_id')
			              ->where('truck_order_registries.agent_id',$user_id)
			              ->get();

    	return view('registry.fieldAgent.payment_history',compact('breadcrumb','requests'));
    }

    // request part payment balance view
    public function balance_payment_request_view()
    {
    	$breadcrumb 	=	'Balance Payment';
    	return view('registry.fieldAgent.balance_payment_request',compact('breadcrumb'));
    }

    // process part payment balance
    public function process_balance_payment(Request $request)
    {
    	// validate form fields
    	$this->validate($request, [
            'waybill_number'=>'required',
            'waybill_file'=>'file|mimes:jpeg,jpg,png,pdf,docx,doc|max:512'
        ]);

        //check if  a waybill file is attached
        if($request->file('waybill_file') == Null){

            $waybill_exists = TruckOrderRegistry::where('waybill_number',$request->waybill_number)
                ->get();
            //check if waybill number exists and get registry id and status
            if (count($waybill_exists)) {
                // get id of record
                foreach ($waybill_exists as $waybill) {
                    $registry_id 	=	$waybill->id;
                }
                $waybill_number 		= $request->waybill_number;
                $status 				= $this->get_payment_status($waybill_number);
                $status_confirmation 	= $this->confrm_balance_payment_status_result($status);

                if ($status_confirmation === 'Confirmed') {

                    $status 					= 	$this->balancePaymentRequestStatus;

                    TruckOrderRegistry::where('id',$registry_id)
                        ->update(
                            array(
                                'registry_status_id' => $status
                            )
                        );

                    //send ops message
                    $user_name	=	Auth::user()->name;
                    $message        =   'New Balance Payment request sent by '.$user_name;
                    $opsPhoneNumber =   $this->getOperationPhone();

                    //call send sms method
                    $this->sendSms($opsPhoneNumber,$message);

                    $user_id    =   $this->user_id();

                    $user = User::find($user_id);
                    $notification = $user->notify(new NewBalancePaymentTransaction($waybill_number));

                    $auditmessage    =   "New Balance Payment Request";
                    $this->audit($auditmessage);

                    return back()->withMessage('Balance Payment Request Successful');
                }else{
                    return back()->withMessage('This Waybill does not seem to have been processed earlier. Please check and try again');
                }

            }else{
                return back()->withMessage('This Waybill does not exist on our system. Please check and try again');
            }

        }else{
            $waybill_exists = TruckOrderRegistry::where('waybill_number',$request->waybill_number)
                ->get();
            //check if waybill number exists and get registry id and status
            if (count($waybill_exists)) {
                // get id of record
                foreach ($waybill_exists as $waybill) {
                    $registry_id 	=	$waybill->id;
                }
                $waybill_number 		= $request->waybill_number;
                $status 				= $this->get_payment_status($waybill_number);
                $status_confirmation 	= $this->confrm_balance_payment_status_result($status);

                if ($status_confirmation === 'Confirmed') {

                    $status 					= 	$this->balancePaymentRequestStatus;
                    $waybill_file				= 	$request->file('waybill_file');

                    $originalImage  = $waybill_file;
                    $thumbnailImage = Image::make($originalImage);
                    $originalPath = public_path().'/waybillfiles/';
                    $thumbnailImage->save($originalPath.$waybill_number.$originalImage->getClientOriginalName());

                    TruckOrderRegistry::where('id',$registry_id)
                        ->update(
                            array(
                                'registry_status_id' => $status,
                                'waybill_file_destination' => $originalImage->getClientOriginalName()//$waybill_file;,
                            )
                        );
                    $auditmessage    =   "New Balance Payment Request";
                    $this->audit($auditmessage);
                    return back()->withMessage('Balance Payment Request Successful');
                }else{
                    return back()->withMessage('This Waybill does not seem to have been processed earlier. Please check and try again');
                }

            }else{
                return back()->withMessage('This Waybill does not exist on our system. Please check and try again');
            }
        }//end check for waybill file upload
    }

    public function change_password_view(){
        $breadcrumb     =   'Change Your Password';
        return view('registry.fieldAgent.change_password',compact('breadcrumb'));
    }

    //create transporter view
    public function create_transporter_view(){
        $breadcrumb =   'Create Transporter';
        $banks      =   Bank::all();
        return view('registry.fieldAgent.transporter',compact('breadcrumb','banks'));
    }

    public function create_transporter_process(Request $request)
    {
        // validate form fields
        $this->validate($request, [
            'name'=>'required|unique:truck_owners,truck_owner_name',
            'phone_number'=>'required|unique:truck_owners,truck_owner_phone',
            'address'=>'required',
            'bank'=>'required',
            'account_number'=>'required',
            'account_name'=>'required',
        ]);

        $phone_number               =   $request->phone_number;
        $address                    =   $request->address;
        $name                       =   $request->name;
        $bank                       =   $request->bank;
        $account_number             =   $request->account_number;
        $account_name               =   $request->account_name;
        $agent_id                   =   Auth::user()->id;

        //retrieve bank_code from banks table
        $bank_object =   Bank::findOrFail($bank);
        $bank_code  =   $bank_object->bank_code;

        //add recipient via paystack api and retrieve recipient code from response

        $result = array();
        //Set other parameters as keys in the $postdata array
        $postdata = array(
            'type' => 'nuban',
            'name' => $name,
            'account_number' => $account_number,
            "bank_code" => $bank_code,
            "currency" => 'NGN',
        );

        $url = "https://api.paystack.co/transferrecipient";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postdata));  //Post Fields
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $headers = [
            //'Authorization: Bearer sk_live_c491d1a0c5b7c8273dad7798c58f727085ec835f',
            'Authorization: Bearer sk_test_28a937c41ad4dda08b5bb33c71fba1b2d5357b3d',
            'Content-Type: application/json'
        ];
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $request = curl_exec($ch);

        curl_close($ch);

        if ($request) {
            $result = json_decode($request, true);
            //retrieve recipient code
        }else{

            return back()->withMessage('An error occurred');
        }


        if($result['status']==true){

            $recipient_code =   $result['data']['recipient_code'];
            //insert into truck_owners table
            $truck_owner    =   new TruckOwner;

            $truck_owner->truck_owner_name                  =   $name;
            $truck_owner->truck_owner_address               =   $address;
            $truck_owner->truck_owner_phone                 =   $phone_number;
            $truck_owner->account_number                    =   $account_number;
            $truck_owner->account_name                      =   $account_name;
            $truck_owner->recipient_code                    =   $recipient_code;
            $truck_owner->bank_id                           =   $bank;
            $truck_owner->field_agent_id                    =   $agent_id;
            $truck_owner->truck_owner_status_id             =   $this->active;
            $truck_owner->save();

            $auditmessage    =   "New Transporter Created";
            $this->audit($auditmessage);

            return back()->withMessage('Transporter added successfully. Click link below to add trucks');
        }else{
            return back()->withMessage('An error occurred');
        }
    }

    public function truck_owners_view(){
        $breadcrumb     =   'Registered Truck Owners';
        $truck_owners 	=   TruckOwner::where('truck_owner_status_id',$this->active)->get();
        $truck_owners_details = array();
        foreach($truck_owners as $rows => $row) {
            $row['total']           = Truck::where('truck_owner_id',$row->id)->count();
            $truck_owners_details[] = $row;
        }
        return view('registry.fieldAgent.truck_owners',compact('truck_owners_details','breadcrumb'));
    }

    public function truck_by_owners_view($id){
        $breadcrumb         =       'Registered Truck ';
        $truck_by_owners 	=       Truck::where('truck_owner_id',$id)->get();
        $truck_owners_name  =       TruckOwner::where('id',$id)->value('truck_owner_name');

        return view('registry.fieldAgent.truck_by_owners',compact('truck_by_owners','truck_owners_name','breadcrumb'));

    }

    public function add_new_truck_view($id){
        $owner_id       =   $id;
        $breadcrumb     =   'Add New Truck';
        return view('registry.fieldAgent.add-truck',compact('owner_id','breadcrumb'));
    }

    public function add_truck_process(Request $request){

        // validate form fields
        $this->validate($request, [
            'driver_licence'=>'required|file|mimes:jpeg,jpg,png,pdf,docx,doc|max:2048',
            'vehicle_licence'=>'required|file|mimes:jpeg,jpg,png,pdf,docx,doc|max:2048',
            'truck_insurance_paper'=>'required|file|mimes:jpeg,jpg,png,pdf,docx,doc|max:2048',
            'git'=>'required|file|mimes:jpeg,jpg,png,pdf,docx,doc|max:2048',
            'plate_number'=>'required|unique:trucks,plate_number',
            'driver_name'=>'required',
            'driver_phone'=>'required',
            'type_of_truck'=>'required',
        ]);

        //check if truck exists in database
        $plateNumberCount  =   count(Truck::where('plate_number',$request->plate_number)->get());
        if($plateNumberCount > 0){
            return back()->withMessage('This truck has already been registered!');
        }
        //end check

        $owner_id                   =   $request->owner_id;
        $plate_number               =   $request->plate_number;
        $driver_name                =   $request->driver_name;
        $driver_phone               =   $request->driver_phone;
        $type_of_truck              =   $request->type_of_truck;

        //save files in filesystem

        /*save driver's license*/
        $originalImageDL  = $request->file('driver_licence');
        $thumbnailImage = Image::make($originalImageDL);
        $originalPath = public_path().'/transporter_docs/driver_licence/';
        $thumbnailImage->save($originalPath.$plate_number.$originalImageDL->getClientOriginalName());
        /*end*/

        /*save vehicle license*/
        $originalImageVL  = $request->file('vehicle_licence');
        $thumbnailImageVL = Image::make($originalImageVL);
        $originalPathVL = public_path().'/transporter_docs/vehicle_licence/';
        $thumbnailImageVL->save($originalPathVL.$plate_number.$originalImageVL->getClientOriginalName());
        /*end*/

        /*save vehicle insurance*/
        $originalImageVI  = $request->file('truck_insurance_paper');
        $thumbnailImageVI = Image::make($originalImageVI);
        $originalPathVI = public_path().'/transporter_docs/vehicle_insurance/';
        $thumbnailImageVI->save($originalPathVI.$plate_number.$originalImageVI->getClientOriginalName());
        /*end*/

        /*save git insurance*/
        $originalImageGIT  = $request->file('git');
        $thumbnailImageGIT = Image::make($originalImageGIT);
        $originalPathGIT = public_path().'/transporter_docs/git/';
        $thumbnailImageGIT->save($originalPathGIT.$plate_number.$originalImageGIT->getClientOriginalName());
        /*end*/

        //end file save

            $truck  =   new Truck;
            $truck->truck_owner_id                              =   $owner_id;
            $truck->truck_status_id                             =   $this->active;
            $truck->field_agent_id                              =   $this->user_id();
            $truck->plate_number                                =   $plate_number;
            $truck->driver_name                                 =   $driver_name;
            $truck->driver_phone                                =   $driver_phone;
            $truck->type_of_truck                               =   $type_of_truck;
            $truck->driver_license_doc                          =   $originalImageDL->getClientOriginalName();
            $truck->git_doc                                     =   $originalImageGIT->getClientOriginalName();
            $truck->vehicle_license_doc                         =   $originalImageVL->getClientOriginalName();;
            $truck->vehicle_insurance_doc                       =   $originalImageVI->getClientOriginalName();;
            $truck->save();

            $auditmessage    =   "New Truck ".$plate_number." Created";
            $this->audit($auditmessage);

            return back()->withMessage('Truck registration successful');

        //end file save
    }

    // view all truck
    public function all_truck_view()
    {
        $breadcrumb = 'All Trucks';
        //list of all the truck
        $requests 	= Truck::all();
        //end//
        //getting additional info abt the truck
        $truck_details = array();
        foreach($requests  as $rows => $row) {
            $row['truck_owner_name']    = TruckOwner::where('id',$row->truck_owner_id)->value('truck_owner_name');
            $truck_details[] = $row;
        }
        //end
        return view('registry.fieldAgent.all_truck',compact('breadcrumb','truck_details'));
    }
}
