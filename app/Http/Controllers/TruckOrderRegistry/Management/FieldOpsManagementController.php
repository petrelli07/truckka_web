<?php
////////////chel
namespace App\Http\Controllers\TruckOrderRegistry\Management;

use App\Notifications\TruckOrderRegistry\NewBalancePaymentTransaction;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use App\TruckRegistry\TruckOrderRegistry;
use App\TruckRegistry\TruckOrderRegistryAudit;
use App\Notifications\TruckOrderRegistry\NewPartPaymentApproval;

class FieldOpsManagementController extends Controller
{

////////route status
    public $active  =   1;
    public $inactive  =   2;

///////////////// payment statuses
    public $partPaymentRequestStatus            = 1;

    public $partPaymentOpApprovedStatus         = 2;

    public $partPaymentFinApprovedtatus         = 3;

    public $balancePaymentRequestStatus         = 4;

    public $balancePaymentOpApprovedStatus      = 5;

    public $balancePaymentFinApprovedStatus     = 6;

    public $paymentComplete                     = 7;

    public $partPaymentDeclinedOps                          = 8;
    public $partPaymentDeclinedFin                          = 9;
    public $balancePaymentDeclinedOps                       = 10;
    public $balancePaymentDeclinedFin                       = 11;
///////////////////// payment statuses
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function audit($message){
        $audit  =   new TruckOrderRegistryAudit;
        $audit->user_id     =   Auth::user()->id;
        $audit->description =   $message;
        $audit->save();
    }

    //send sms
    public function sendSms($phoneNumber,$message){
        $encoded_message    =   urlencode($message);
        $result = array();
        $url = 'https://www.bulksmsnigeria.com/api/v1/sms/create?api_token=Lk6hs0SzNhQJfVFOisboSBcq2VSX0CJehOdtC5diccvPyzUlUE7qE2a0Co82&from=Truckka&to='.$phoneNumber.'&body='.$encoded_message.'&dnd=2';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $request = curl_exec($ch);

        curl_close($ch);
        if ($request) {
            $result = json_decode($request, true);
        }
    }
    
    //landing page for management
    public function index() {
        $auditmessage    =   "Logged in";
        $this->audit($auditmessage);
        $results        =      TruckOrderRegistry::join('registry_statuses','registry_statuses.id','=',
                                                    'truck_order_registries.registry_status_id')
                                                    ->where('truck_order_registries.registry_status_id',
                                                    $this->partPaymentRequestStatus)
                                                    ->orWhere('truck_order_registries.registry_status_id',
                                                        $this->balancePaymentRequestStatus)
                                                    ->get();
        $breadcrumb ='Management';
        return view('registry.management.index',compact('breadcrumb','results'));
    }

    //processing page for view more
    public function viewMore($registry_number){
        $registry_detail   =   TruckOrderRegistry::where('registry_number',$registry_number)->get();

        foreach($registry_detail as $detail){
            $registry_number        =   $detail->registry_number;
            $registry_status_id     =   $detail->registry_status_id;
        }

        if($registry_status_id === $this->partPaymentRequestStatus){
            return redirect('/registry/ops/view_more_part_payment/'.$registry_number);
        }elseif($registry_status_id === $this->balancePaymentRequestStatus){
            return redirect('/registry/ops/view_more_balance/'.$registry_number);
        }

    }

    // get part payment requests
    private function getPartPaymentRequest(){
        $tds =      TruckOrderRegistry::join('registry_statuses','registry_statuses.id','=',
                              'truck_order_registries.registry_status_id')
                      ->where('truck_order_registries.registry_status_id',$this->partPaymentRequestStatus)
                      ->get();
        return $tds;
    }
    
    // part payment requests view
    public function viewPartPaymentRequest() {
        $breadcrumb ='All Approve Part Payment';
        $results = $this->getPartPaymentRequest();
        return view('registry.management.view-approve-part-payment',compact('breadcrumb','results'));
    }

    //get waybillfile for origin
    public function getWaybillFile($registry_number){
        $get_waybill_file   =   TruckOrderRegistry::join('trucks','trucks.id','=',
                                                    'truck_order_registries.truck_id')
                                                    ->join('users','users.id','=','truck_order_registries.agent_id')
                                                    ->where('registry_number',$registry_number)->get();
        foreach($get_waybill_file as $file){
            $waybillFile    =   $file->waybill_file_origin;
        }
        $data   =   ['filename'=>$waybillFile,'details'=>$get_waybill_file];
        return $data;
    }

    //view more part payment
    public function viewMorePartPayment($registry_number){
        $data       =   $this->getWaybillFile($registry_number);

        $details    =   $data['details'];

        $breadcrumb =   'Approve Part Payment';
        return view('registry.management.view_more_part_payment',compact('breadcrumb','ext','details'));
    }

// process part payment requests
    public function processPartPaymentRequest($registry_number){

        $registry   =   TruckOrderRegistry::where('registry_number',$registry_number)->get();
        foreach($registry as $reg){
            $post   =   $reg->id;
            $agent_id   =   $reg->agent_id;
            $waybill_number   =   $reg->waybill_number;
        }

        TruckOrderRegistry::where('id',$post)
                    ->update(
                        array(
                            'registry_status_id' => $this->partPaymentOpApprovedStatus
                            )
                        );

        //get agent phone number
        $agent_details  =   User::findOrFail($agent_id);
        //send ops message
        $message        =   'Part Payment for order '.$waybill_number.' has been approved by operations';
        $agentPhoneNumber =   $agent_details->phone;

        //call send sms method
        $this->sendSms($agentPhoneNumber,$message);

        $auditmessage    =   "Part Payment for ".$waybill_number." approved";
        $user_id        =   Auth::user()->id;
        $user = User::find($user_id);
        $notification = $user->notify(new NewPartPaymentApproval($waybill_number));
        $this->audit($auditmessage);
        return redirect('registry/view-approve-part-payment-mgt')->withInput()->with('response','Approved Successfully');
    }

//get balance payment requests
    private function getBalancePaymentRequest(){
        $tds =      TruckOrderRegistry::join('registry_statuses','registry_statuses.id','=',
                                        'truck_order_registries.registry_status_id')
                                        ->where('truck_order_registries.registry_status_id',
                                        $this->balancePaymentRequestStatus)->get();
        return $tds;
    }

//get balance payment request view
    public function viewBalancePaymentRequest() {
        $breadcrumb ='All Balance Payment Requests';
        $results = $this->getBalancePaymentRequest();
        return view('registry.management.view-approve-balance-payment',compact('breadcrumb','results'));
    }

    //getWaybillFileDestination
    public function getWaybillFileDestination($registry_number){
        $get_waybill_file   =   TruckOrderRegistry::join('trucks','trucks.id','=',
            'truck_order_registries.truck_id')
            ->join('users','users.id','=','truck_order_registries.agent_id')
            ->where('registry_number',$registry_number)->get();
        foreach($get_waybill_file as $file){
            $waybillFile    =   $file->waybill_file_destination;
        }
        $data   =   ['filename'=>$waybillFile,'details'=>$get_waybill_file];
        return $data;
    }

    //view more balance payment
    public function viewMoreBalancePayment($registry_number){
        $data       =   $this->getWaybillFileDestination($registry_number);

        if($data['filename'] == NULL){
            $sanitized_filename = NULL;
            $details    =   $data['details'];
        }else{
            $sanitized_filename =   $data['filename'];

            //sanitize string
            //$sanitized_filename     =   trim(explode('/', $data['filename'])[2]);
           //end sanitizer/*
            /*$file_path  =   storage_path().'\\app\\public\\registry_waybillFiles\\'.$sanitized_filename;
            $ext = pathinfo(storage_path().$file_path, PATHINFO_EXTENSION);*/

            $details    =   $data['details'];
        }

        $breadcrumb =   'Approve Balance Payment';
        return view('registry.management.view_more_balance_payment',compact('breadcrumb','sanitized_filename','details'));
    }
    //end view more balance

//////////////////process balance payment 
    public function processBalancePaymentRequest($waybill_number){

        $registry   =   TruckOrderRegistry::where('registry_number',$waybill_number)->get();
        foreach($registry as $reg){
            $post   =   $reg->id;
            $agent_id   =   $reg->agent_id;
        }

        TruckOrderRegistry::where('id',$post)
                    ->update(
                        array(
                            'registry_status_id' => $this->balancePaymentOpApprovedStatus
                            )
                        );
        //get agent phone number
        $agent_details  =   User::findOrFail($agent_id);
        //send ops message
        $message        =   'Balance Payment for order '.$waybill_number.' has been approved by operations';
        $agentPhoneNumber =   $agent_details->phone;

        //call send sms method
        $this->sendSms($agentPhoneNumber,$message);

        $auditmessage    =   "Balance Payment for ".$waybill_number." approved";
        $this->audit($auditmessage);

        $user_id    =   Auth::user()->id;
        $user = User::find($user_id);
        $notification = $user->notify(new NewBalancePaymentTransaction($waybill_number));

        return redirect('registry/view-approve-balance-payment-mgt')->withInput()->with('response','Approved Successfully');
    }

/////////////download waybill files
    public function downloadWaybillFile(Request $request)
    {
        $registry = TruckOrderRegistry::find($request->id);

        if ($request->fileType === 'download_destination') {
            //download destination waybill

            ///get storage path
            $storage_path = storage_path().'\\app\\registry_waybillFiles\\'.$registry->waybill_file_destination;

            /////////////check if file exists
            if (file_exists($storage_path)) {
                ///////////download
                return response()->download($storage_path);
            }else{
                //////return error message
                return back()->withInput()->with('response','File does not exist');
            }

        }elseif($request->fileType === 'download_origin'){
            //download origin waybill

            /////sanitize string 
            $sanitized_filename     =   trim(explode('/', $registry->waybill_file_origin)[1]);
            /////end sanitizer

            ///get storage path
            $storage_path = storage_path().'\\app\\registry_waybillFiles\\'.$sanitized_filename;

            /////////////check if file exists
            if (file_exists($storage_path)) {
                ///////////download
                return response()->download($storage_path);
            }else{
                //////return error message
                return back()->withInput()->with('response','File does not exist');
            }

        }
    }
//////////end waybillfile download

    public function partPaymentDeclinedOps($registry_number){
        $registry   =   TruckOrderRegistry::where('registry_number',$registry_number)->get();
        foreach($registry as $reg){
            $post   =   $reg->id;
            $agent_id   =   $reg->agent_id;
        }

        TruckOrderRegistry::where('id',$post)
            ->update(
                array(
                    'registry_status_id' => $this->partPaymentDeclinedOps
                )
            );

        //get agent phone number
        $agent_details  =   User::findOrFail($agent_id);
        //send ops message
        $message        =   'Part Payment for order '.$registry_number.' has been declined by operations';
        $agentPhoneNumber =   $agent_details->phone;

        //call send sms method
        $this->sendSms($agentPhoneNumber,$message);

        $auditmessage    =   "Part Payment for ".$registry_number." declined by operations";
        $this->audit($auditmessage);
        return redirect('registry/view-approve-part-payment-mgt')->withInput()->with('response','Declined Successfully');
    }

}
