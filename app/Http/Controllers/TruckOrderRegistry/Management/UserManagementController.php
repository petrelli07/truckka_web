<?php

namespace App\Http\Controllers\TruckOrderRegistry\Management;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Auth;

class UserManagementController extends Controller
{
    

    public function __construct()
    {
        $this->middleware('auth');
    }

	public function create_new_user_view()
	{

		$breadcrumb = 'Create user';

		return view('registry.management.create_new_registry_user',compact('breadcrumb'));
	}

    public function change_password_view(){
        $breadcrumb     =   'Change Your Password';
        return view('registry.management.change_password',compact('breadcrumb'));
    }

	public function create_new_user_process(Request $request)
	{
		///////////// validate form fields ///////////////
    	$this->validate($request, [
            'email'=>'required|unique:users,email',
            'name'=>'required',
            'userType'=>'required',
        ]);

        $email 		= $request->email;
        $name 		= $request->name;
        $userType 	= $request->userType;

        $user 	=	new User;

        $user->name 				=	$name;
        $user->email 				=	$email;
        $user->userAccessLevel 		=	$userType;
        $user->password 			=	bcrypt('12345');
        $user->default_password 	=	0;
        $user->status 				=	1;

        $user->save();

        return back()->withMessage('User Created Successfully');
		
	}


}
