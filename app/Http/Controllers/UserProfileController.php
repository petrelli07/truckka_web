<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Validator;
use App\User; 

class UserProfileController extends Controller{

    public function __construct(){
        // $this->middleware('auth');
    }

    public function UserProfile(){
      return view('user_profile');
    }

}
