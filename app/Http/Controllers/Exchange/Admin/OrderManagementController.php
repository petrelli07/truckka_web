<?php

namespace App\Http\Controllers\Exchange\Admin;

use App\Exchange\ExchangeOrderTruckDetail as TruckDetail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

use App\User;
use App\Exchange\ExchangeOrder;
use App\Exchange\ExchangeRoute as Route;
use App\Exchange\IdleCapacity;
use App\Exchange\State;
use App\Exchange\TruckSupplier;
use App\Exchange\ExchangeSupplierOrder;
use App\Exchange\ExchangePayment as Payment;
    use App\Exchange\ExchangeTransporterRequest as TransporterRequest;
use App\Exchange\ExchangeOrderTruckPayment as TruckPayment;
use Auth;
use Notification;
use App\Notifications\Exchange\NewQuote;
use Illuminate\Support\Facades\Mail;
use App\Mail\Exchange\truckDetails;


class OrderManagementController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $breadcrumb     =   'Exchange Order';
        $result         = ExchangeOrder::join('exchange_routes','exchange_orders.exchange_route_id','=',
                                'exchange_routes.id')
                                ->join('states as origin_state','exchange_routes.origin_state_id','origin_state.id')
                                ->join('states as destination_state','exchange_routes.destination_state_id','destination_state.id')
                                ->join('exchange_statuses','exchange_orders.exchange_status_id','exchange_statuses.id')
                                ->join('exchange_truck_types','exchange_orders.exchange_truck_type_id',
                                    '=','exchange_truck_types.id')
                                ->where('exchange_orders.exchange_status_id',10)
                                ->orwhere('exchange_orders.exchange_status_id',14)
                                ->select('exchange_orders.*','exchange_truck_types.*','origin_state.state_description as originStateName','destination_state.state_description as destStateName','exchange_statuses.*','exchange_routes.*')
                                ->get();
        $exchange_order_details = array();
        foreach($result as $rows => $row) {
            $row['user_name']           = User::where('id',$row->user_id)->value('name');
            $exchange_order_details[] = $row;
        }
        return view('exchange.admin.exchangeorder',compact('breadcrumb','exchange_order_details'));
    }

    public function matchSupplierOrder(Request $request){
        $supplier_id        =   $request->supplier_id;
        $order_id           =   $request->order_id;
        $order_details       =   ExchangeOrder::where('id',$order_id)->get();
        $supplier_details   =    TruckSupplier::where('id',$supplier_id)->get();

        foreach($order_details as $order){
            $order_id   =   $order->id;
            $route_id   =   $order->exchange_route_id;
            $order_number   =   $order->order_number;
        }

        $route_details  = Route::where('id',$route_id)->get();

        foreach($route_details as $details){
            $destination_state_id    =  $details->destination_state_id;
        }

        $state  =   State::where('id',$destination_state_id)->value('exchange_coverage_area_id');

        foreach($supplier_details as $supplier){
            $supplier_id    =   $supplier->id;
            $user_id        =   $supplier->user_id;
        }

        $supplier_order                                     =   new ExchangeSupplierOrder;
        $supplier_order->user_id                            = $user_id;
        $supplier_order->exchange_order_id                  = $order_id;
        $supplier_order->truck_supplier_id                  = $supplier_id;
        $supplier_order->exchange_status_id                 = 29;
        $supplier_order->save();

        /*IdleCapacity::where('truck_supplier_id',$supplier_id)
                        ->where('exchange_coverage_area_id',$state)
                        ->update(
                            array(
                                'exchange_status_id' => 28,
                            )
                        );*/


        ExchangeOrder::where('id',$order_id)
            ->update(
                array(
                    'exchange_status_id'    => 12,
                )
            );

        return redirect('/exchange/admin/home')->with('message','order matched successfully');

    }

    public function getAllExchangeorders(){
        $breadcrumb     =   'Exchange Order';
        $result = ExchangeOrder::join('exchange_routes','exchange_orders.exchange_route_id','=',
                                    'exchange_routes.id')
                                    ->join('exchange_truck_types','exchange_orders.exchange_truck_type_id',
                                        '=','exchange_truck_types.id')
                                    ->where('exchange_orders.exchange_status_id',10)
                                    ->get();
        $exchange_order_details = array();
        foreach($result as $rows => $row) {
            $row['user_name']           = User::where('id',$row->user_id)->value('name');
            $exchange_order_details[] = $row;
        }
        return view('exchange.admin.exchangeorder',compact('breadcrumb','exchange_order_details'));
    }



    public function BackgroundResultForSuitableIDLE(Request $request){

        $exchangeOrderDetails   =    ExchangeOrder::find($request->exchange_order_id);
        $breadcrumb         =  'Suitable IDLE for Exchange';
        $result             =   IdleCapacity::all();

        $idle_capacity_details = array();
        foreach($result as $rows => $row) {
            $user_id                    = TruckSupplier::where('id', $row->truck_supplier_id)->value('user_id');
            $row['user_name']           =  User::where('id',$user_id)->value('name');
            $row['user_id']             =  $user_id;
            //checking if a particular IDLE Capacity has been matach//
            $row['exchange_order_id']   = $request->exchange_order_id;
            $row['status']        =  $row->exchange_status_id;
            // end//
            $idle_capacity_details[] = $row;
        }
        return view('exchange.admin.suitableidle',compact('breadcrumb','idle_capacity_details'));
    }


    public function MatchIDEForExchange(Request $request){

        $checking_two = IdleCapacity::where('id', $request->idle_id)->value('exchange_status_id');
        if($checking_two == 1):

            return back()->withMessage('Already Matched');
        else:
            //inserting to db///
            $post = new ExchangeSupplierOrder;
            $post->user_id                          = $request->user_id;
            $post->exchange_order_id                = $request->exchange_order_id;
            $post->exchange_status_id               = $request->exchange_status_id;
            $post->save();
            //end of it//

            //updating the db for idle capacities//
            $post = IdleCapacity::find($request->idle_id);
            $post->exchange_status_id               = 1;
            $post->save();
            //end of it//

        endif;
        return back()->withMessage('Match successfully');
    }

    public function matchOrder($orderNumber){

        $order_details  =   ExchangeOrder::where('order_number',$orderNumber)->get();
        foreach($order_details as $order){
            $order_id   =   $order->id;
            $route_id   =   $order->exchange_route_id;
        }
        $route_details  = Route::where('id',$route_id)->get();
        foreach($route_details as $details){
            $origin_state_id    =  $details->origin_state_id;
            $destination_state_id    =  $details->destination_state_id;
        }
        $breadcrumb =   'Match Orders';
        $state  =   State::where('id',$destination_state_id)->value('exchange_coverage_area_id');
        $origin_coverage_id  =   State::where('id',$origin_state_id)->value('exchange_coverage_area_id');

        $supplier_match =   IdleCapacity::where('exchange_coverage_area_id',$state)
                                                    ->join('users','idle_capacities.user_id','users.id')
                                                    ->where('idle_capacities.exchange_status_id',27)
                                                   // ->where('idle_capacities.exchange_coverage_area_id',$state)
                                                    ->get();

        return view('exchange.admin.assign_orders',compact('breadcrumb','supplier_match','order_id'));

    }

    public function confirmOrder($reference_number){

        $order_id   =   ExchangeOrder::where('order_number',$reference_number)->value('id');
        $order_details   =   ExchangeOrder::where('order_number',$reference_number)
                                ->join('exchange_routes as routes','exchange_orders.exchange_route_id','routes.id')
                                ->select('exchange_orders.*','routes.*')
                                ->get();

        foreach($order_details as $order_detail){
            $amount         =   $order_detail->price;
            $truckNumber    =   $order_detail->number_of_trucks;
            $user_id        =   $order_detail->user_id;
        }


        $amount             =   $amount  *  $truckNumber;
        $service_charge     =   $amount * 0.1;
        $total_amount       =   $amount + $service_charge;

        $request_amount     =   $total_amount;
        $reference_number   =   $this->paymentIDNumber();

        /*record payment*/
        $payment    =   new Payment;
        $payment->reference_number               =  $reference_number;
        $payment->amount                         =   $request_amount;
        $payment->user_id                        =   $user_id;
        $payment->exchange_order_id              =   $order_id;
        $payment->exchange_status_id             =   16;
        $payment->save();

        $user   =   User::find($user_id);

        ExchangeOrder::where('id',$order_id)
            ->update(
                array(
                    'exchange_status_id'    => 9,
                )
            );
        $notification = $user->notify(new NewQuote($reference_number));
        return back()->with('message','Invoice Created Successfully');

    }

    public function matchTransporter($reference_number){
        $breadcrumb =   "Supplier Responses";
        $order_id       =   ExchangeOrder::where('order_number',$reference_number)->value('id');
        $responses  =    TruckDetail::where('exchange_order_id',$order_id)
                                      ->join('truck_suppliers','exchange_order_truck_details.truck_supplier_id','truck_suppliers.id')
                                      ->join('users','truck_suppliers.user_id','users.id')
        ->get();
        /*
        $order_id       =   ExchangeOrder::where('order_number',$reference_number)->value('id');*/
        return view('exchange.admin.view_responses',compact('responses','breadcrumb','reference_number'));
    }

    public function addResponse($reference_number){
        $breadcrumb =   "Attach Trucks";
        $transporters   =   User::where('userAccessLevel',5)->get();
        $order_id       =   ExchangeOrder::where('order_number',$reference_number)->value('id');
        return view('exchange.admin.match_trucks',compact('order_id','transporters','breadcrumb'));
    }

    public function submitResponse(Request $request){
        $order_id       =   $request->order_id;
        $plateNumber    =   $request->truck_plate_number;
        $driverName     =   $request->driver_name;
        $driverPhone    =   $request->driver_phone;
        $transporter    =   $request->transport_company;
        $imei           =   $request->imei;

        $order          =   ExchangeOrder::join('users','exchange_orders.user_id','users.id')
                                            ->where('exchange_orders.id',$order_id)
                                            ->get();

        foreach($order as $orders){
            $email          =   $orders->email;
            $truckType      =   $orders->exchange_truck_type_id;
            $order_number   =   $orders->order_number;
            $name           =   $orders->name;
        }


        for($x=0;$x< count($transporter); $x++){

            //supplier_id
            $supplier_id    =   TruckSupplier::where('user_id',$transporter[$x])->value('id');
            //end supplier_id

            //attach supplier to order
            $exchangeSupplierOrder =   new ExchangeSupplierOrder;
            $exchangeSupplierOrder->exchange_order_id   =   $order_id;
            $exchangeSupplierOrder->user_id             =   $transporter[$x];
            $exchangeSupplierOrder->truck_supplier_id   =   $supplier_id;
            $exchangeSupplierOrder->exchange_status_id  =   30;
            $exchangeSupplierOrder->save();
            //end attach supplier to order

            //attach truck details to order
            $orderTruckDetail   =   new TruckDetail;
            $orderTruckDetail->plate_number             =    $plateNumber[$x];
            $orderTruckDetail->driver_phone_number      =    $driverPhone[$x];
            $orderTruckDetail->driver_name              =    $driverName[$x];
            $orderTruckDetail->truck_supplier_id        =    $supplier_id;
            $orderTruckDetail->exchange_truck_type_id   =    $truckType;
            $orderTruckDetail->exchange_order_id        =    $order_id;
            $orderTruckDetail->exchange_status_id       =    33;
            $orderTruckDetail->save();
            $truck_detail_id    = $orderTruckDetail->id;

                //end attach truck details to order

            //enter truck payment details
            $truckPayment   =   new TruckPayment;
            $truckPayment->reference_number         =   $this->truckPaymentReference();
            $truckPayment->exchange_order_id        =   $order_id;
            $truckPayment->truck_detail_id          =   $truck_detail_id;
            $truckPayment->exchange_status_id       =   33;
            $truckPayment->save();
            //end enter truck payment details
        }

        ExchangeOrder::where('id',$order_id)
            ->update(
                array(
                    'exchange_status_id' => 14,
                )
            );

        $details    =   ['name'=>$name,'order_number'=>$order_number];

        Mail::to($email)->send(new truckDetails($details));
        return redirect('exchange/admin/add_response/'.$order_number)->with('message','Trucks Attached to Order');
        //return back()->with('message','Trucks Attached to Order');

    }

    public function transporterRequest(){
        return TransporterRequest::all();
    }

    public function truckPaymentReference(){
        $i = 1;
        for($j=0; $j < $i; $j++){
            $result = '';
            $length = 7;
            for($x = 0; $x < $length; $x++) {
                $result .= mt_rand(0, 9);
            }

            $checkAvailable = TruckPayment::where('reference_number', $result);
            if( $checkAvailable->count() < 1 ){
                return $result;
                break;
            }

            $i++;
        }
    }

    public function paymentIDNumber(){
        $i = 1;
        for($j=0; $j < $i; $j++){
            $result = '';
            $length = 7;
            for($x = 0; $x < $length; $x++) {
                $result .= mt_rand(0, 9);
            }

            $checkAvailable = Payment::where('reference_number', $result);
            if( $checkAvailable->count() < 1 ){
                return $result;
                break;
            }

            $i++;
        }
    }
}
