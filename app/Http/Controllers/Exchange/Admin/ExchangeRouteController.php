<?php

namespace App\Http\Controllers\Exchange\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Exchange\ExchangeRoute as Route;
use App\Exchange\State;

class ExchangeRouteController extends Controller
{
    public $active_route        =   1;
    public $inactive_route      =   2;

    public function allRoutes(){
        $breadcrumb =   "All Routes";
        $route  =   Route::join('exchange_statuses','exchange_routes.exchange_status_id','exchange_statuses.id')
                        ->join('states as origin_state','exchange_routes.origin_state_id','origin_state.id')
                        ->join('states as destination_state','exchange_routes.destination_state_id','destination_state.id')
                        ->select('origin_state.state_description as originStateName','destination_state.state_description as destStateName','exchange_statuses.*','exchange_routes.*')
                            ->get();
        return view('exchange.admin.all_routes',compact('route','breadcrumb'));
    }

    public function activeRoutes(){
        $route  =   Route::where('exchange_status_id',$this->active_route)
                    ->join('exchange_statuses','exchange_routes.exchange_status_id','exchange_statuses.id')
                    ->get();
        return $route;
    }

    public function inactiveRoutes(){
        $route  =   Route::where('exchange_status_id',$this->active_route)
                    ->join('exchange_statuses','exchange_routes.exchange_status_id','exchange_statuses.id')
                    ->get();
        return $route;
    }

    public function editRoute($route_id){
        $route_detail   =   Route::where('exchange_routes.id',$route_id)
                        ->join('states as origin_state','exchange_routes.origin_state_id','origin_state.id')
                        ->join('states as destination_state','exchange_routes.destination_state_id','destination_state.id')
                        ->select('origin_state.state_description as originStateName','destination_state.state_description as destStateName','exchange_routes.*')
                        ->get();

        $breadcrumb =   "Update Route";

        foreach($route_detail as $detail){
            $origin         =   $detail->originStateName;
            $destination    =   $detail->destStateName;
            $customer_base_price                    =   $detail->customer_base_price;
            $customer_service_charge                =   $detail->customer_service_charge;
            $transporter_service_charge             =   $detail->transporter_service_charge;
            $transporter_base_price                 =   $detail->transporter_base_price;
            $id             =   $detail->id;
        }

        $data   =   [
            'id'                                    => $id,
            'origin'                                => $origin,
            'destination'                           => $destination,
            'customer_base_price'                  => $customer_base_price,
            'customer_service_charge'              => $customer_service_charge,
            'transporter_service_charge'           => $transporter_service_charge,
            'transporter_base_price'               => $transporter_base_price,
        ];

        return view('exchange.admin.edit_route',compact('data','breadcrumb'));
    }

    public function updateRoute(Request $request){

        $this->validate($request, [
            'route_id'=>'required|numeric',
            'customer_base_price'=>'required',
            'customer_service_charge'=>'required',
            'transporter_service_charge'=>'required',
            'transporter_base_price'=>'required',
        ]);

        $route_id   =   $request->route_id;
        $transporter_base_price      =   $request->transporter_base_price;
        $transporter_service_charge      =   $request->transporter_service_charge;
        $customer_service_charge      =   $request->customer_service_charge;
        $customer_base_price      =   $request->customer_base_price;


        $sanitized_customer_base    =    $this->clean($customer_base_price);//removed all commas in price
        $sanitized_customer_service    =    $this->clean($customer_service_charge);//removed all commas in price
        $sanitized_transporter_base    =    $this->clean($transporter_base_price);//removed all commas in price
        $sanitized_transporter_service   =    $this->clean($transporter_service_charge);//removed all commas in price

        Route::where('id',$route_id)
            ->update(
                array(
                    'transporter_base_price'=>$sanitized_transporter_base,
                    'transporter_service_charge'=>$sanitized_transporter_service,
                    'customer_service_charge'=>$sanitized_customer_service,
                    'customer_base_price'=>$sanitized_customer_base
                )
            );
        return redirect('/exchange/admin/routes/all_routes')->with('message','Route updated Successfully');
    }

    public function createNewRouteView(){
        $states =  State::all();
        $breadcrumb =   "All Routes";
        return view('exchange.admin.create_new_route',compact('states','breadcrumb'));
    }

    public function createNewRoute(Request $request){

        $this->validate($request, [
            'origin'=>'required',
            'destination'=>'required',
            'customer_base_price'=>'required',
            'customer_service_charge'=>'required',
            'transporter_base_price'=>'required',
            'transporter_service_charge'=>'required',
        ]);
        //return $request->all();
        $origin                             =   $request->origin;
        $destination                        =   $request->destination;
        $customer_base_price                =   $request->customer_base_price;
        $customer_service_charge            =   $request->customer_service_charge;
        $transporter_base_fare              =   $request->transporter_base_price;
        $transporter_service_charge         =   $request->transporter_service_charge;


        if($origin == $destination){
            return back()->with('message','Origin and Destination can not have same values');
        }

        if($this->checkRouteExist($origin, $destination)){
            return back()->with('message','this route already exists');
        }


        $sanitized_customer_base    =    $this->clean($customer_base_price);//removed all commas in price
        $sanitized_customer_service    =    $this->clean($customer_service_charge);//removed all commas in price
        $sanitized_transporter_base    =    $this->clean($transporter_base_fare);//removed all commas in price
        $sanitized_transporter_service   =    $this->clean($transporter_service_charge);//removed all commas in price

        $route  =   new Route;
        $route->origin_state_id                         =   $origin;
        $route->destination_state_id                    =   $destination;
        $route->customer_base_price                     =   $sanitized_customer_base;
        $route->customer_service_charge                 =   $sanitized_customer_service;
        $route->transporter_base_price                  =   $sanitized_transporter_base;
        $route->transporter_service_charge              =   $sanitized_transporter_service;
        $route->exchange_status_id                      =   $this->active_route;
        $route->save();

        return back()->with('message','New Route Created Successfully');
    }

    public function activateRoute($route_id){
        Route::where('id',$route_id)
            ->update(
                array(
                    'exchange_status_id' => $this->active_route
                )
            );
        return back()->with('message','Route activated Successfully');
    }

    public function deactivateRoute($route_id){
        Route::where('id',$route_id)
            ->update(
                array(
                    'exchange_status_id' => $this->inactive_route
                )
            );
        return back()->with('message','Route Deactivated Successfully');
    }

    public function checkRouteExist($origin,$destination){

        $route_exist  =   Route::where('origin_state_id',$origin)
            ->where('destination_state_id',$destination)
            ->count();

        if($route_exist >= 1){
            return true;
        }
    }

    public function clean($string) {
        $string = str_replace(',', '', $string); // Replaces all spaces with hyphens.

        return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }

}
