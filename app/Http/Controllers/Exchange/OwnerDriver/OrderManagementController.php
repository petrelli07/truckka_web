<?php

namespace App\Http\Controllers\Exchange\OwnerDriver;

use App\Bank;
use App\Exchange\ExchangeOrder;
use App\Exchange\ExchangeSupplierOrder;
use App\Exchange\State;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Exchange\ExchangeSupplierOrder as SupplyOrder;
use App\Exchange\ExchangeOrderTruckDetail as TruckDetail;
use App\Exchange\ExchangeRoute as Route;
use App\Exchange\TruckSupplier as TruckSupplier;
use App\Exchange\ExchangeOrderTruckPayment as TruckPayment;
use Auth;
use Image;


class OrderManagementController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /*public function checkSupplierStatus($user_id){
        $supplierDetails =   TruckSupplier::where('user_id',$user_id)->get();
        foreach($supplierDetails as $supplier){
            $bank           =   $supplier->bank_id;
            $recipient_code =   $supplier->recipient_code;
            $account_number =   $supplier->account_number;
        }

        if($bank == Null || $recipient_code == Null || $account_number == Null){
            return "null";
        }else{
            return "not null";
        }

    }*/

    public function home(){
        /*$data = $this->checkSupplierStatus(Auth::user()->id);
        if($data === "null"){
            return redirect('/exchange/truck_owner/update_account_details');
        }elseif($data === "not null"){*/

            $breadcrumb =   'Home';
            $match_request  =  ExchangeSupplierOrder::where('exchange_supplier_orders.user_id',Auth::user()->id)
                ->join('exchange_orders','exchange_supplier_orders.exchange_order_id',
                    'exchange_orders.id')
                ->join('exchange_routes','exchange_orders.exchange_route_id','exchange_routes.id')
                ->join('states as origin_state','exchange_routes.origin_state_id','origin_state.id')
                ->join('states as destination_state','exchange_routes.destination_state_id','destination_state.id')
                ->join('exchange_statuses','exchange_orders.exchange_status_id','exchange_statuses.id')
                ->select('exchange_orders.*','exchange_supplier_orders.*','origin_state.state_description as originStateName','destination_state.state_description as destStateName','exchange_statuses.*')
                ->get();

            /*TruckDetail::where('exchange_order_id',$order_id)
                ->join('truck_suppliers','exchange_order_truck_details.truck_supplier_id','truck_suppliers.id')
                ->join('users','truck_suppliers.user_id','users.id')
                ->get();*/

            /*$match_request  =   SupplyOrder::where('exchange_supplier_orders.user_id',Auth::user()->id)
                                                ->join('exchange_orders','exchange_supplier_orders.exchange_order_id',
                                                    'exchange_orders.id')
                                                ->join('exchange_routes','exchange_orders.exchange_route_id','exchange_routes.id')
                                                ->join('states as origin_state','exchange_routes.origin_state_id','origin_state.id')
                                                ->join('states as destination_state','exchange_routes.destination_state_id','destination_state.id')
                                                ->join('exchange_statuses','exchange_orders.exchange_status_id','exchange_statuses.id')
                                                ->select('exchange_orders.*','exchange_supplier_orders.*','origin_state.state_description as originStateName','destination_state.state_description as destStateName','exchange_statuses.*')
                                                ->get();*/


            return view('exchange.truck_owners.index',compact('breadcrumb','match_request'));

        //}

    }

    public function addTrucks($order_number){
        $order_details  =   ExchangeOrder::where('order_number',$order_number)->get();
        foreach($order_details as $order){
            $truck_number   =   $order->number_of_trucks;
            $order_id       =   $order->id;
        }
        $breadcrumb =   'Home';
        return view('exchange.truck_owners.add-truck',compact('breadcrumb','truck_number','order_id'));
    }

    public function commitTrucks(Request $request){

        $this->validate($request, [
            'truck_plate_number.*'=>'required',
            'driver_name.*'=>'required',
            'driver_phone.*'=>'required|min:11',/*
            'driver_licence.*'=>'required|file|mimes:jpeg,jpg,png,pdf,docx,doc|max:512',
            'truck_insurance_paper.*'=>'required|file|mimes:jpeg,jpg,png,pdf,docx,doc|max:512',
            'vehicle_licence.*'=>'required|file|mimes:jpeg,jpg,png,pdf,docx,doc|max:512',
            'roadworthiness.*'=>'required|file|mimes:jpeg,jpg,png,pdf,docx,doc|max:512',*/
            'order_id'=>'required',
        ]);

       $supply   =   SupplyOrder::where('exchange_supplier_orders.user_id',Auth::user()->id)
                                ->join('exchange_orders','exchange_supplier_orders.exchange_order_id',
                                    'exchange_orders.id')
                                ->get();

        foreach($supply as $details){
            $supplier_id    =   $details->truck_supplier_id;
            $exchange_truck_type_id    =   $details->exchange_truck_type_id;
        }


        $plate_number               =   $request->truck_plate_number;
        $driver_name                =   $request->driver_name;
        $driver_phone               =   $request->driver_phone;
        $order_id                   =   $request->order_id;

            for($x=0; $x < count($plate_number); $x++){

                /*$originalImage_driver_licence  = $driver_licence;
                $thumbnailImage_driver_license  = Image::make($originalImage_driver_licence);
                $originalPath_driver_license    = public_path().'/exchange/exchange_driver_license/';
                $thumbnailImage_driver_license->save($originalPath_driver_license.$plate_number.$originalImage_driver_licence->getClientOriginalName());

                $originalImage_vehicle_licence      = $vehicle_licence;
                $thumbnailImage_vehicle_license     = Image::make($originalImage_vehicle_licence);
                $originalPath_vehicle_license       = public_path().'/exchange/exchange_vehicle_license/';
                $thumbnailImage_vehicle_license->save($originalPath_vehicle_license.$plate_number.$originalImage_vehicle_licence->getClientOriginalName());

                $originalImage_truck_insurance_paper    = $truck_insurance_paper;
                $thumbnailImage_truck_insurance_paper   = Image::make($originalImage_truck_insurance_paper);
                $originalPath_truck_insurance_paper     = public_path().'/exchange/exchange_truck_insurance_paper/';
                $thumbnailImage_truck_insurance_paper->save($originalPath_truck_insurance_paper.$plate_number.$originalImage_truck_insurance_paper->getClientOriginalName());

                $originalImage_roadworthiness     = $roadworthiness;
                $thumbnailImage_roadworthiness   = Image::make($originalImage_roadworthiness);
                $originalPath_roadworthiness     = public_path().'/exchange/exchange_roadworthiness/';
                $thumbnailImage_roadworthiness->save($originalPath_roadworthiness.$plate_number.$originalImage_roadworthiness->getClientOriginalName());*/


                $truck_detail                                           =   new TruckDetail;
                $truck_detail->plate_number                             =   $plate_number[$x];
                $truck_detail->driver_name                              =   $driver_name[$x];
                $truck_detail->driver_phone_number                      =   $driver_phone[$x];/*
                $truck_detail->vehicle_license                          =   $originalImage_vehicle_licence->getClientOriginalName();
                $truck_detail->drivers_license                          =   $originalImage_driver_licence->getClientOriginalName();
                $truck_detail->vehicle_insurance                        =   $originalImage_truck_insurance_paper->getClientOriginalName();
                $truck_detail->road_worthiness_permit                   =   $originalImage_roadworthiness->getClientOriginalName();*/
                $truck_detail->exchange_order_id                        =   $order_id;
                $truck_detail->truck_supplier_id                        =   $supplier_id;
                $truck_detail->exchange_truck_type_id                   =   $exchange_truck_type_id;
                $truck_detail->exchange_status_id                       =   33;
                $truck_detail->save();

                $truck_detail_id    =   $truck_detail->id;

                $truckPayment   =   new TruckPayment;
                $truckPayment->reference_number         =   $this->truckPaymentReference();
                $truckPayment->exchange_order_id        =   $order_id;
                $truckPayment->truck_detail_id          =   $truck_detail_id;
                $truckPayment->exchange_status_id       =   31;
                $truckPayment->save();
            }

            ExchangeOrder::where('id',$order_id)
                ->update(
                    array(
                        'exchange_status_id' => 14,
                    )
                );
        
            return redirect('/exchange/truck_owner_driver/home')->with('message','You can now see the order details');
    }

    public function viewOrderDetails($order_number){
        $order_details  =   ExchangeOrder::where('order_number',$order_number)->get();
        foreach($order_details as $details){
            $exchange_route_id  =   $details->exchange_route_id;
            $contact_phone      =   $details->contact_phone;
            $item_description   =   $details->item_description;
            $pickup_address     =   $details->pickup_address;
            $pickup_date        =   $details->pickup_date;
        }
        $route  =   Route::where('id',$exchange_route_id)->get();
        foreach($route as $routes){
            $origin_id  =   $routes->origin_state_id;
            $destination_id  =   $routes->destination_state_id;
        }
        $origin_state           =   State::where('id',$origin_id)->value('state_description');
        $destination_state      =   State::where('id',$destination_id)->value('state_description');

        $details    =   [
            'contact_phone'=>$contact_phone,
            'item_description'=>$item_description,
            'pickup_address'=>$pickup_address,
            'pickup_date'=>$pickup_date,
            'origin'=>$origin_state,
            'destination'=>$destination_state,
        ];

        $breadcrumb =   'Order Details';
        return view('exchange.truck_owners.view-details',compact('breadcrumb','details'));
    }

    public function updateAccountDetailsView(){

        $breadcrumb =   'Account Details';
        $banks      =   Bank::all();
        return view('exchange.truck_owners.account_details',compact('breadcrumb','banks'));
    }

    public function updateAccountDetails(Request $request){

        $this->validate($request, [
            'account_number'=>'required|min:10|max:10',
            'bank_id'=>'required',
            'account_name'=>'required',
        ]);

        $user_id    =   Auth::user()->id;

        $account_number =   $request->account_number;
        $bank_id        =   $request->bank_id;
        $account_name   =   $request->account_name;

        //retrieve bank_code from banks table
        $bank_object =   Bank::findOrFail($bank_id);
        $bank_code  =   $bank_object->bank_code;

        //add recipient via paystack api and retrieve recipient code from response

        $result = array();
        //Set other parameters as keys in the $postdata array
        $postdata = array(
            'type' => 'nuban',
            'name' => $account_name,
            'account_number' => $account_number,
            "bank_code" => $bank_code,
            "currency" => 'NGN',
        );

        $url = "https://api.paystack.co/transferrecipient";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postdata));  //Post Fields
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $headers = [
            //'Authorization: Bearer sk_live_c491d1a0c5b7c8273dad7798c58f727085ec835f',
            'Authorization: Bearer sk_test_28a937c41ad4dda08b5bb33c71fba1b2d5357b3d',
            'Content-Type: application/json'
        ];
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $request = curl_exec($ch);

        curl_close($ch);
        if ($request) {
            $result = json_decode($request, true);
            //retrieve recipient code
        }else{

            return back()->withMessage('An error occurred');
        }

        if($result['status']==true){

            $recipient_code =   $result['data']['recipient_code'];

            TruckSupplier::where('user_id',$user_id)
                ->update(
                    array(
                        'account_number'    => $account_number,
                        'recipient_code'    => $recipient_code,
                        'bank_id'           => $bank_id,
                    )
                );

            return redirect('/exchange/truck_owner_driver/home');

        }else{
            return back()->withMessage('An error occurred');
        }

    }

    public function truckPaymentReference(){
        $i = 1;
        for($j=0; $j < $i; $j++){
            $result = '';
            $length = 7;
            for($x = 0; $x < $length; $x++) {
                $result .= mt_rand(0, 9);
            }

            $checkAvailable = TruckPayment::where('reference_number', $result);
            if( $checkAvailable->count() < 1 ){
                return $result;
                break;
            }

            $i++;
        }
    }

}
