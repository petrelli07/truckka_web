<?php

namespace App\Http\Controllers\Auth;


use Illuminate\Http\Request;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Session, Mail, DB;
use App\Exchange\IdleCapacity;
use App\Exchange\ExchangeOrder as Order;
use App\Exchange\ExchangePayment as Payment;
use App\Exchange\TruckSupplier;
use App\Exchange\ExchangeWallet as Wallet;
use App\Exchange\ExchangeWalletHistory as WalletHistory;


class RegisterController extends Controller
{
	/*
	|--------------------------------------------------------------------------
	| Register Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users as well as their
	| validation and creation. By default this controller uses a trait to
	| provide this functionality without requiring any additional code.
	|
	*/

	use RegistersUsers;

	/**
	 * Where to redirect users after registration.
	 *
	 * @var string
	 */
	protected $redirectTo = '/login';

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	protected function validator(array $data)
	{
		return Validator::make($data, [
			'name' => 'required|string|max:255',
			'email' => 'required|string|email|max:255|unique:users',
			'password' => 'required|string|min:6|confirmed',
			'userType' => 'required',
		]);
	}

	/**
	 * Create a new user instance after a valid registration.
	 *
	 * @param  array  $data
	 * @return \App\User
	 */
	protected function create(array $data)
	{

		if (Session::has('quote')) {//if user has session data

                $user = User::create([
                    'name' => $data['name'],
                    'email' => $data['email'],
                    'phone' => $data['phone'],
                    'password' => bcrypt($data['password']),
                    'userAccessLevel' => $data['userType'],
                    'default_password' => '0',
                ]);


                $user_id    =  $user->id;

                if($data['userType'] === '4'){

                    $sessionData    =   Session::get('quote');

                    for ($i=0; $i < count($sessionData); $i++) {
                        $routeID        			            =   $sessionData['routeID'];
                        $truckNumberForm         			    =   $sessionData['truckNumberForm'];
                        $truckTypeIDForm             			=   $sessionData['truckTypeIDForm'];
                        $itemDescriptionForm        			=   $sessionData['itemDescriptionForm'];
                        $pickUpDateForm             			=   $sessionData['pickUpDateForm'];
                        $totalAmtForm             		        =   $sessionData['totalAmtForm'];
                    }

                    /*Payment Gateway here*/
                        //use paystack api to process payments here
                    /*End Payment Gateway*/

                    $ExchangeOrder  =   new Order;

                    $ExchangeOrder->order_number                        =   $this->exchangeOrderNumber();
                    $ExchangeOrder->number_of_trucks                    =   $truckNumberForm;
                    $ExchangeOrder->pickup_date                         =   date("Y-m-d", strtotime($pickUpDateForm));
                    $ExchangeOrder->item_description                    =   $itemDescriptionForm;
                    $ExchangeOrder->user_id                             =   $user_id;
                    $ExchangeOrder->exchange_route_id                   =   $routeID;
                    $ExchangeOrder->exchange_truck_type_id              =   $truckTypeIDForm;
                    $ExchangeOrder->exchange_status_id                  =   9;
                    $ExchangeOrder->save();

                    /*$ExchangeOrder->order_number                        =   $this->orderNumber();
                    $ExchangeOrder->number_of_trucks                    =   $truckNumber;
                    $ExchangeOrder->user_id                             =   $this->user_id();
                    $ExchangeOrder->exchange_route_id                   =   $route_id;
                    $ExchangeOrder->exchange_truck_type_id              =   $resourceType;
                    $ExchangeOrder->item_description                    =   $cargo_description;
                    $ExchangeOrder->pickup_address                      =   $pickup_address;
                    $ExchangeOrder->delivery_address                    =   $delivery_address;
                    $ExchangeOrder->pickup_date                         =   $pickup_date;
                    $ExchangeOrder->exchange_status_id                  =   11;
                    $ExchangeOrder->save();*/

                    session()->forget('quote');

                    //$order_id   =   $ExchangeOrder->id;

                    /*record payment*/
                    /*$payment    =   new Payment;
                    $payment->reference_number               =   $this->referenceNumber();
                    $payment->amount                         =   $totalAmtForm;
                    $payment->user_id                        =   $user_id;
                    $payment->exchange_order_id              =   $order_id;
                    $payment->exchange_status_id             =   16;
                    $payment->save();*/
                    /*end record payment*/

                    return $user;

                }//end user type check

		}else{//if user does not have session data

            if($data['userType'] == 5 || $data['userType'] == 6){ //if user is supplier or owner

                $user = User::create([
                    'name' => $data['name'],
                    'email' => $data['email'],
                    'phone' => $data['phone'],
                    'password' => bcrypt($data['password']),
                    'userAccessLevel' => $data['userType'],
                    'default_password' => '0',
                ]);

                $user_id    =  $user->id;

                /*fetch data from session*/
                $sessionData    =   Session::get('supplier_details');

                $truckType        			                    =   $sessionData['truckType'];
                $number_of_truck        			            =   $sessionData['number_of_truck'];
                $coverage_area            			            =   $sessionData['coverageArea'];
                $supplierType        			                =   $sessionData['supplierType'];
                $state        			                        =   $sessionData['state'];

                /*end data fetching*/

                /*save to truck_suppliers table*/
                $supplier   =   new TruckSupplier;

                $supplier->user_id                  =   $user_id;
                $supplier->truck_supplier_type_id   =   $supplierType;
                $supplier->exchange_status_id       =   25;
                $supplier->save();

                /*end save to truck_suppliers table and get id of last inserted*/

                $supplier_id    =   $supplier->id;

                for($x=0; $x<count($coverage_area); $x++){
                    $idleCapacity                               =   new IdleCapacity;
                    $idleCapacity->number_of_truck              =   $number_of_truck;
                    $idleCapacity->truck_supplier_id            =   $supplier_id;
                    $idleCapacity->exchange_truck_type_id       =   $truckType;
                    $idleCapacity->exchange_coverage_area_id    =   $coverage_area[$x];
                    $idleCapacity->exchange_status_id           =   27;
                    $idleCapacity->state_id                     =   $state;
                    $idleCapacity->user_id                      =   $user_id;
                    $idleCapacity->save();
                }


                if($data['userType'] == 5){//if owner
                    return $user;
                }elseif($data['userType'] == 6){//if supplier
                    //create wallet
                    $wallet =   new Wallet;
                    $wallet->amount                 =   0;
                    $wallet->user_id                =   $user_id;
                    $wallet->exchange_status_id     =   23;
                    $wallet->save();
                    return $user;
                }

                session()->forget('supplier_details');
            }else{//user could be a customer

                return User::create([
                    'name' => $data['name'],
                    'email' => $data['email'],
                    'phone' => $data['phone'],
                    'password' => bcrypt($data['password']),
                    'userAccessLevel' => $data['userType'],
                    'default_password' => '0',
                ]);
            }
		}
	}

   	public function exchangeOrderNumber(){
        $i = 1;
        for($j=0; $j < $i; $j++){
            $result = '';
            $length = 7;
            for($x = 0; $x < $length; $x++) {
                $result .= mt_rand(0, 9);
            }

            $checkAvailable = Order::where('order_number', $result);
            if( $checkAvailable->count() < 1 ){
                return $result;
                break;
            }

            $i++;
        }
    }

   	public function referenceNumber(){
        $i = 1;
        for($j=0; $j < $i; $j++){
            $result = '';
            $length = 7;
            for($x = 0; $x < $length; $x++) {
                $result .= mt_rand(0, 9);
            }
            $checkAvailable = Payment::where('reference_number', $result);
            if( $checkAvailable->count() < 1 ){
                return $result;
                break;
            }

            $i++;
        }
    }

}
