<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use Session, Mail, DB;
use App\Exchange\ExchangeOrder as Order;
use App\Exchange\ExchangePayment as Payment;
use App\Mail\Exchange\newOrder;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home1 screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    //protected $redirectTo = '/home1';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function redirectTo(){

        $role = Auth::user()->userAccessLevel;

        if(Session::has('quote')) {

            $sessionData    =   Session::get('quote');

            for ($i=0; $i < count($sessionData); $i++) {
                $origin =   $sessionData['origin'];
                $destination =   $sessionData['destination'];
                $pickup_address =   $sessionData['pickup_address'];
                $cargo_description =   $sessionData['cargo_description'];
                $resourceType =   $sessionData['truckType'];
                $pickup_date =   $sessionData['pickup_date'];
                $delivery_address =   $sessionData['delivery_address'];
                $truckNumber =   $sessionData['number_of_truck'];
                $pickupPlaceID =   $sessionData['pickup_placeId'];
                $deliveryPlaceID =   $sessionData['delivery_placeId'];
            }
            $or            =   State::where('state_description',$origin)->get();

            foreach($or as $origins){
                $origin_state_id    =  $origins->id;
            }

            $dest       =   State::where('state_description',$destination)->get();

            foreach($dest as $destinations){
                $destination_state_id   =   $destinations->id;
            }

            $route  =   Route::where('origin_state_id',$origin_state_id)
                ->where('destination_state_id',$destination_state_id)
                ->get();

            foreach($route as $routes){
                $route_id       =   $routes->id;
                $base_amount   =   $routes->price;
            }

            $amount             =   $base_amount  *  $truckNumber;
            $service_charge     =   $amount * 0.1;
            $total_amount       =   $amount + $service_charge;

            $request_amount     =   $total_amount;

            $order_number   =   $this->exchangeOrderNumber();
            $ExchangeOrder  =   new Order;

            $ExchangeOrder->order_number                        =   $order_number;
            $ExchangeOrder->number_of_trucks                    =   $truckNumber;
            $ExchangeOrder->user_id                             =   Auth::user()->id;
            $ExchangeOrder->exchange_route_id                   =   $route_id;
            $ExchangeOrder->exchange_truck_type_id              =   $resourceType;
            $ExchangeOrder->item_description                    =   $cargo_description;
            $ExchangeOrder->pickup_address                      =   $pickup_address;
            $ExchangeOrder->delivery_address                    =   $delivery_address;
            $ExchangeOrder->pickup_date                         =   $pickup_date;
            $ExchangeOrder->origin_place_id                     =   $pickupPlaceID;
            $ExchangeOrder->destination_place_id                =   $deliveryPlaceID;
            $ExchangeOrder->exchange_status_id                  =   11;
            $ExchangeOrder->save();

            $details    =   [
                'name'=>Auth::user()->name,
                'order_number'=>$order_number
            ];

            $order_id   =   $ExchangeOrder->id;

            /*record payment*/
            $payment    =   new Payment;
            $payment->reference_number               =   $this->referenceNumber();
            $payment->amount                         =   $request_amount;
            $payment->user_id                        =   Auth::user()->id;
            $payment->exchange_order_id              =   $order_id;
            $payment->exchange_status_id             =   16;
            $payment->save();

            User::where('id',Auth::user()->id)
                ->update(
                    array(
                        'is_first_login'    => 1,
                    )
                );

            session()->forget('quote');

            Mail::to(Auth::user()->email)->send(new newOrder($details));

        }

        switch ($role) {
            case 0:
                return '/home';
                break;
            case 1:
                return '/clientHome';
                break;
            case 2:
                return '/all-orders';
                break;
            case 3:
                return '/all_suppliers_supply_orders';
                break;
            case 4:
                return 'exchange/customer/home';
                break;
            case 5:
                return 'exchange/truck_owner_driver/home';
                break;
            case 6:
                return 'exchange/aggregator/home';
                break;
            case 7:
                return 'registry/fieldOps/home';
                break;
            case 8:
                return 'registry/finops-home';
                break;
            case 9:
                return 'registry/home-mgt';
                break;
            case 10;
                return 'exchange/admin/home';
            case 11;
                return 'registry/administrator/home';
            case 12;
                return 'tmsw/administrator/home';
            case 13;
                return 'tmsw/manager/home';
            case 14;
                return 'tmsw/desk_officer/home';
            case 15;
                return 'tmsw/finance/home';
            case 16;
                return 'tmsw/transporter/home';
            case 17;
                return 'tmsw/supplier/orders';
            case 18;
                return 'tmsw/superAdministrator/orders';
            default:
                return '/';
                break;
        }

    }

    public function referenceNumber(){
        $i = 1;
        for($j=0; $j < $i; $j++){
            $result = '';
            $length = 7;
            for($x = 0; $x < $length; $x++) {
                $result .= mt_rand(0, 9);
            }

            $checkAvailable = Payment::where('reference_number', $result);
            if( $checkAvailable->count() < 1 ){
                return $result;
                break;
            }

            $i++;
        }
    }

    public function exchangeOrderNumber(){
        $i = 1;
        for($j=0; $j < $i; $j++){
            $result = '';
            $length = 7;
            for($x = 0; $x < $length; $x++) {
                $result .= mt_rand(0, 9);
            }

            $checkAvailable = Order::where('order_number', $result);
            if( $checkAvailable->count() < 1 ){
                return $result;
                break;
            }

            $i++;
        }
    }
}
