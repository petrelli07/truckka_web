<?php

namespace App\Http\Controllers\Api\Registry\fieldOps;

use Illuminate\Http\Request;
use App\Http\Requests\PartPaymentRequest;
use App\Http\Controllers\Controller;
use App\TruckRegistry\TruckOrderRegistry;
use DB;
use App\Http\Resources\TruckRegistry\TruckOrderRegistry as TruckOrderRegistryResource;

class FieldOperationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $all    =    DB::table('truck_order_registries')
                        ->join('registry_statuses','registry_statuses.id','=',
                            'truck_order_registries.registry_status_id')
                        ->where('truck_order_registries.agent_id',2)
                        ->get();

        return TruckOrderRegistryResource::collection($all);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return "okay";
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(TruckOrderRegistry $TruckOrderRegistry)
    {
           //return new TruckOrderRegistryResource($TruckOrderRegistry);
        return $TruckOrderRegistry;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
