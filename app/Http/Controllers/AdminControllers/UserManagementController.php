<?php

namespace App\Http\Controllers\AdminControllers;
use App\Http\Controllers\Controller;

use App\User;
use Illuminate\Http\Request;
use DB;
use Validator;
use Auth;
use Excel;
use App\serviceRequest;
use App\CarrierDetail;
use App\CarrierResource;
use App\ClientRequiredResource;
use App\Company;
use App\UserComapany;
use App\Module;
use Notification;
use Notify;
use App\Notifications\newUser;
use App\Notifications\newCarrierAccount;
use App\Notifications\newClientAccount;
use \App\Http\Controllers\AdminControllers\UserManagementController as Admin;

class UserManagementController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create(){
        return view('admin.new_admin.orders.singleOrder');
    }

    public function createUniqueUser(int $userID, int $type){
      $data     = array('user_id' =>  $userID,
                        'type_id' =>  $type
                      );
      $insert   = DB::table('unique_users')
                    ->insert($data);
      return $insert;
    }

    public function resourceTypeCheck($rTypeArray,$clientID)
    {
        $resourceType = $rTypeArray;

        $companyID = $clientID;

        for($r=0;$r<count($resourceType); $r++){
        $checkResType = ClientRequiredResource::where('company_id',$clientID)->where('resourceType',$resourceType[$r])->count();
           if($checkResType == 0){
                DB::table("client_required_resources")->insert([
                    'resourceType' => $resourceType[$r],
                    'company_id' => $clientID,
                    'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
                    'updated_at' => \Carbon\Carbon::now()->toDateTimeString(),
                ]);
           }
        }
    }

    public function defaultAdminModules()
    {
        $defaultAdminModules   =   Module::where('userType',0)->where('status',1)->where('byDefault',1)->get(); 
        return $defaultAdminModules; 
    }

    public function defaultClientModules()
    {
        $defaultClientModules   =   Module::where('userType',1)
                                            ->where('status',1)
                                            ->where('byDefault',1)
                                            ->get();
        return $defaultClientModules;  
    }

    public function defaultCarreirModules(){

        $defaultCarreirModules  =   Module::where('userType',2)
                                            ->where('status',1)
                                            ->where('byDefault',1)
                                            ->get();
        return $defaultCarreirModules;  
    }

    public function testFunc()
    {
        return $this->defaultClientModules();
    }



    public function randomNumber(){

        $i = 1;

        for($j=0; $j < $i; $j++){
//            return random_int(1,10000);

            $result = '';

            $length = 7;

            for($x = 0; $x < $length; $x++) {
                $result .= mt_rand(0, 9);
            }

            $checkAvailable = CarrierResource::where('resource_id', $result);

            if( $checkAvailable->count() < 1 ){

                return $result;

                break;
            }
            $i++;

        }
    }

    public function randomNumberForPassword(){

        $i = 1;

        for($j=0; $j < $i; $j++){
//            return random_int(1,10000);

            $result = '';

            $length = 7;

            for($x = 0; $x < $length; $x++) {
                $result .= mt_rand(0, 9);
            }

            $checkAvailable = User::where('password', bcrypt($result));

            if( $checkAvailable->count() < 1 ){

                return $result;

                break;
            }
            $i++;

        }
    }

    public function createAdmin(Request $request){
         $name = $request->adminName;
         $email = $request->adminEmail;
         $category = $request->category;

         $defaultPassword = $this->randomNumberForPassword();

        //$defaultPassword = '$2y$10$a76F.mWAnjgTBqpUYeiO1OKwFRW1pc96VbShjxsuglKMCm4V/Xysm';

        $admin = DB::table("users")->insertGetId([
            'name' => $name,
            'email' => $email,
            'password' => bcrypt($defaultPassword),
            'default_password' => bcrypt($defaultPassword),
            'userAccessLevel' => $category,
            'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
            'updated_at' => \Carbon\Carbon::now()->toDateTimeString(),
        ]);

        if($admin){

            $user = User::find($admin);

            $notification = $user->notify(new newUser($defaultPassword));

            //Notification::send($admin, new newUser($defaultPassword));
            return response()->json(['success'=>'New Admin Created Successfully']);
       

        }else{
            return response()->json(['error'=>'Something Went Wrong']);
        }

    }


    public function createNewClient(Request $request){
        $createdByUser = Auth::user()->name;
        $createdByID = Auth::user()->id;

       $name = $request->name;
       $email = $request->email;
       $category = $request->category;

       $defaultPassword = $this->randomNumberForPassword();

       // var_dump($defaultPassword);

       $bcryptPass = bcrypt($defaultPassword); 

        $emailModal = $request->clientemail;
        $nameModal = $request->companyName;
        //$categoryModal = $request->clientcategory;
        $resourceType = $request->resourceType;
        $rcNumber = $request->rcNumber;
        $address = $request->address;
        $phone = $request->phone;

        $agentName = $request->agentName;
        $agentEmail = $request->agentEmail;

        //validate client
        $validatorClientCompany = Validator::make($request->all(), [
            'companyName'=>'required',
            'rcNumber'=>'required',
            'phone'=>'required',
            'address'=>'required',
            'clientForm'=>'required|file|mimes:xlsx',
            'clientPricingForm'=>'required|file|mimes:xlsx',

        ]);

        //if validate passes
        if ($validatorClientCompany->passes()) {

        $category = 0;

        $client = DB::table("companies")->insertGetId([
            'companyName' => $nameModal,
            'rcNumber' => $rcNumber,
            'category' => $category,
            'address' => $address,
            'phone' => $phone,
            'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
            'updated_at' => \Carbon\Carbon::now()->toDateTimeString(),
        ]);

        }else{
            return response()->json(['error'=>$validatorClientCompany->errors()->all()]);
        }

        ///client default user
        $insertUserDetailsID = DB::table("users")->insertGetId([
                'name' => $agentName,
                'email' => $agentEmail,
                'password' => $bcryptPass,
                'default_password' => $bcryptPass,
                'userAccessLevel' => 1,
                'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
                'updated_at' => \Carbon\Carbon::now()->toDateTimeString(),
            ]);

        $insertUserCompanyID = DB::table("user_comapanies")->insert([
                'user_id'    => $insertUserDetailsID,
                'company_id' => $client,
                'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
                'updated_at' => \Carbon\Carbon::now()->toDateTimeString(),
            ]);

        $user = User::find($insertUserDetailsID);
        $notification = $user->notify(new newClientAccount($defaultPassword));
        //end insert default user

        $clientID = $client;
        $walletOpenBal = 0;
        $defaultInvoiceID = 000000;
        $defaultDescription = "Default Opening Balance";

        //Create New Unique User
        $this->createUniqueUser($insertUserDetailsID, 1);
        //insert client default modules
        $defaultClientModules    =    $this->defaultClientModules();

        for ($i=0; $i < count($defaultClientModules); $i++) { 
            $clientID_moduleID[]    =   [
                'module_id'      => $defaultClientModules[$i]['id'],
                'company_id'     => $clientID 
             ];

             $clientDefaultUserAssignModules[]  =   [
                'module_id'     =>  $defaultClientModules[$i]['id'],
                'user_id'       =>  $insertUserDetailsID
             ];
        }

        if (!empty($clientID_moduleID) && !empty($clientDefaultUserAssignModules)) {
            DB::table('client_company_modules')->insert($clientID_moduleID);
            DB::table('user_permissions')->insert($clientDefaultUserAssignModules);
        }
        //end insert of default client modules

        $clientWallet = DB::table("client_wallets")->insert([
            'company_id' => $clientID,
            'user_id' => $createdByID,
            'invoice_id' => $defaultInvoiceID,
            'description' => $defaultDescription,
            'balance' => $walletOpenBal,
            'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
            'updated_at' => \Carbon\Carbon::now()->toDateTimeString(),
        ]);

        $companyName = $request->companyName;
        

        if ($validatorClientCompany->passes()) {

            if($request->hasFile('clientForm') && $request->hasFile('clientPricingForm')){

                $path = $request->file('clientForm')->getRealPath();
                $data = Excel::load($path)->get();

                $path2 = $request->file('clientPricingForm')->getRealPath();
                $data2 = Excel::load($path2)->get();

                if($data->count()){
                        foreach ($data as $value) {
                        $arr[] = [
                                'company_id' => $clientID,
                                'companyName' => $companyName,
                                'rcNumber' => $rcNumber,
                                'origin' => $value->origin,
                                'destination' => $value->destination,
                                'originRegion' => $value->originregion,
                                'destinationRegion' => $value->destregion,
                        ];

                        }

                        //$this->resourceTypeCheck($rTypeArray,$clientID);//call resourceTypeCheck Function

                    if(!empty($arr)){

                        DB::table('client_orders')->insert($arr);
                            
                        //return response()->json(['success'=>'New Client Created Successfully']);
                        
                    }else{
                        return response()->json(['error'=>'Something Went Wrong. Unable to create Client Orders']);
                    }
                }else{
                        return response()->json(['error'=>'File Path Not Found']);
                }//process excel file1

                if($data2->count()){
                        foreach ($data2 as $value) {
                        $formattedPrice = rtrim($value->clientprice,',');
                        $arrResTemp[] = [
                                'company_id' => $clientID,
                                'resourceType' => $value->resourcetype,
                                'origin' => $value->origin,
                                'destination' => $value->destination,
                                'price' => $formattedPrice
                        ];

                        }


                    if(!empty($arrResTemp)){

                        DB::table('client_required_resources')->insert($arrResTemp);
                            
                        return response()->json(['success'=>'New Client Created Successfully']);
                        
                    }else{
                        return response()->json(['error'=>'Something Went Wrong. Unable to create Client Resource Template']);
                    }
                }else{
                        return response()->json(['error'=>'File Path2 Not Found']);
                }//process excel file2

            }else{//if file does not exist
                return response()->json(['error'=>'Something Went Wrong. No file found']);
            }

        }else{//if client validation fails
            return response()->json(['error'=>$validatorClientCompany->errors()->all()]);
        }//end client reg
    }


    public function createNewCarrier(Request $request){

        $createdByUser = Auth::user()->name;
        $createdByID = Auth::user()->id;

        $name = $request->name;
        $email = $request->email;
        $category = $request->category;

        $defaultPassword = $this->randomNumberForPassword();
        $bcryptPass = bcrypt($defaultPassword);

        $rcNumber = $request->rcNumber;
        $emailModal = $request->email;
        $nameModal = $request->name;
        $categoryModal = $request->category;
        $companyName = $request->companyName;
        $settlementType = 0;

        $zone = $request->region;


        if ($request->hasFile('carrierResources')) {

                $resourceStatus = 0;
                //validate input fields
                $validator3 = Validator::make($request->all(), [
                    'companyName'=>'required',
                    'rcNumber'=>'required',
                    'name'=>'required',
                    'region.*'=>'required',
                    'email'=>'required|email|unique:users',
                    /*
                    'settlementType'=>'required',*/
                    'carrierPricing'=>'required|file|mimes:xlsx',
                ]);
                if ($validator3->passes()) {

                    $carrierCompanyID = DB::table("carrier_company_details")->insertGetId([
                        'company_name' => $companyName,
                        'rc_number' => $rcNumber
                    ]);

                    //insert into user table and get userID
                    $userID = DB::table("users")->insertGetId([
                        'name' => $nameModal,
                        'email' => $emailModal,
                        'password' => bcrypt($defaultPassword),
                        'default_password' => bcrypt($defaultPassword),
                        'userAccessLevel' => $categoryModal,
                        'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
                        'updated_at' => \Carbon\Carbon::now()->toDateTimeString(),
                    ]);

                   $carrierUser = DB::table('carrier_users')->insertGetId([
                        'company_id'=>$carrierCompanyID,
                        'user_id'   =>$userID,
                        'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
                        'updated_at' => \Carbon\Carbon::now()->toDateTimeString(),
                   ]); 

                            //Create New Unique User
                        $this->createUniqueUser($userID, 2);
                        //insert client default modules
                        $defaultCarrierModules    =    $this->defaultCarreirModules();

                        for ($i=0; $i < count($defaultCarrierModules); $i++) { 
                            $carrierID_moduleID[]    =   [
                                'module_id'      => $defaultCarrierModules[$i]['id'],
                                'company_id'     => $carrierCompanyID
                             ];

                             $clientDefaultUserAssignModules[]  =   [
                                'module_id'     =>  $defaultCarrierModules[$i]['id'],
                                'user_id'       =>  $userID
                             ];
                        }

                        if (!empty($carrierID_moduleID) && !empty($clientDefaultUserAssignModules)) {
                            DB::table('carrier_company_modules')->insert($carrierID_moduleID);
                            DB::table('user_permissions')->insert($clientDefaultUserAssignModules);
                        }

                    //default wallet and invoice details
                    $walletOpenBal      = 0;
                    $defaultInvoiceID   = 000000;
                    $defaultDescription = "Default Opening Balance";
                    $serviceID          = 0;    
                    $invoiceID          = 0;    

                    $carrierInvoiceID     = DB::table("carrier_company_invoices")->insertGetId([
                        'invoice_id'    =>          $invoiceID,
                        'service_id'    =>          $serviceID,
                        'carrier_id'    =>          $carrierCompanyID,
                        'status'        =>          1
                    ]);

                    //insert into company wallet
                    $carrierWallet =  DB::table("carrier_company_wallets")->insert([
                        'company_id'            => $carrierCompanyID,
                        'admin_id'              => $createdByID,
                        'invoice_id'            => $carrierInvoiceID,
                        'description'           => $defaultDescription,
                        'balance'               => $walletOpenBal,
                        'created_at'            => \Carbon\Carbon::now()->toDateTimeString(),
                        'updated_at'            => \Carbon\Carbon::now()->toDateTimeString(),
                    ]);

                    //insert carrier route price
                    $path3 = $request->file('carrierPricing')->getRealPath();
                    $data3 = Excel::load($path3)->get();
                    if($data3->count()){
                        foreach ($data3 as $value) {
                            $formattedPrice = rtrim($value->carrierprice,',');
                            $arrNew3[] = [
                                'carrier_id' => $carrierCompanyID,
                                'origin' => $value->origin,
                                'destination' => $value->destination,
                                'resourceType' => $value->resourcetype,
                                'price' => $formattedPrice,
                            ];
                        }
                        $carrRouteResPrices =  DB::table('carrier_company_route_prices')->insert($arrNew3);
                    }else{
                        return response()->json(['error'=>'Carrier File Error!']);
                    }

                    //insert carrier resource gps details
                    $pathCarrierResources = $request->file('carrierResources')->getRealPath();
                    $dataCarrierResources = Excel::load($pathCarrierResources)->get();
                    if($dataCarrierResources->count()){
                        foreach ($dataCarrierResources as $value) {
                            $arrNewCarrierResources[] = [
                                'company_id' => $carrierCompanyID,
                                'GPSID' => $value->gpsid,
                                'capacity' => $value->capacity,
                                'plateNumber' => $value->platenumber,
                                'resourceStatus' => $resourceStatus,
                                'driverName' => $value->drivername,
                                'driverPhoneNumber' => $value->driverphonenumber,
                                'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
                                'updated_at' => \Carbon\Carbon::now()->toDateTimeString(),
                            ];
                        }
                        $CarrierResourceDetails =  DB::table('carrier_resource_details')->insert($arrNewCarrierResources);
                    }else{
                        return response()->json(['error'=>'Carrier File Error!']);
                    }

                    for($i = 0; $i < count($zone); $i++){
                    $arrZones[] = [
                        'company_id' => $carrierCompanyID,
                        'regions_covered' => $request->region[$i],
                    ];
                    }

                    if (!empty($arrZones)) {
                         $carrier_regions =  DB::table('carrier_company_regions')->insert($arrZones);
                    }
                    //end carrier zones insert


                    if($userID && /*$resourceDetails &&*/ $carrier_regions){
                        //send carrier default user password
                        $user = User::find($userID);
                        $notification = $user->notify(new newCarrierAccount($defaultPassword));

                        return response()->json(['success'=>'New Haulage Provider Created Successfully']);

                    }else{
                        return response()->json(['error'=>'Something Went Wrong with DB Inserts']);
                    }

                }else{
                    return response()->json(['error'=>$validator3->errors()->all()]);
                }
        }else{


            $validator3 = Validator::make($request->all(), [
                    'companyName'=>'required',
                    'rcNumber'=>'required',
                    'name'=>'required',
                    'region.*'=>'required',
                    'email'=>'required|email|unique:users',
                    /*
                    'settlementType'=>'required',*/
                    'carrierPricing'=>'required|file|mimes:xlsx',
                ]);
                if ($validator3->passes()) {

                    $carrierCompanyID = DB::table("carrier_company_details")->insertGetId([
                        'company_name' => $companyName,
                        'rc_number' => $rcNumber
                    ]);

                    //insert into user table and get userID
                    $userID = DB::table("users")->insertGetId([
                        'name' => $nameModal,
                        'email' => $emailModal,
                        'password' => bcrypt($defaultPassword),
                        'default_password' => bcrypt($defaultPassword),
                        'userAccessLevel' => $categoryModal,
                        'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
                        'updated_at' => \Carbon\Carbon::now()->toDateTimeString(),
                    ]);

                    
                    $carrierUser = DB::table('carrier_users')->insertGetID([
                        'company_id'=>$carrierCompanyID,
                        'user_id'   =>$userID,
                        'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
                        'updated_at' => \Carbon\Carbon::now()->toDateTimeString(),
                   ]); 

                    $walletOpenBal = 0;
                    $defaultInvoiceID = 000000;
                    $defaultDescription = "Default Opening Balance";
                        //Create New Unique User
                    $this->createUniqueUser($userID, 2);
                    //insert client default modules
                    $defaultCarrierModules    =    $this->defaultCarreirModules();

                    for ($i=0; $i < count($defaultCarrierModules); $i++) { 
                        $carrierID_moduleID[]    =   [
                            'module_id'      => $defaultCarrierModules[$i]['id'],
                            'company_id'     => $carrierCompanyID
                         ];

                         $clientDefaultUserAssignModules[]  =   [
                            'module_id'     =>  $defaultCarrierModules[$i]['id'],
                            'user_id'       =>  $userID
                         ];
                    }

                    if (!empty($carrierID_moduleID) && !empty($clientDefaultUserAssignModules)) {
                        DB::table('carrier_company_modules')->insert($carrierID_moduleID);
                        DB::table('user_permissions')->insert($clientDefaultUserAssignModules);
                    }


                    //default wallet and invoice details
                    $walletOpenBal      = 0;
                    $defaultInvoiceID   = 000000;
                    $defaultDescription = "Default Opening Balance";
                    $serviceID          = 0;    
                    $invoiceID          = 0;    

                    $carrierInvoiceID     = DB::table("carrier_company_invoices")->insertGetId([
                        'invoice_id'    =>          $invoiceID,
                        'service_id'    =>          $serviceID,
                        'carrier_id'    =>          $carrierCompanyID,
                        'status'        =>          1
                    ]);

                    //insert into company wallet
                    $carrierWallet =  DB::table("carrier_company_wallets")->insert([
                        'company_id'            => $carrierCompanyID,
                        'admin_id'              => $createdByID,
                        'invoice_id'            => $carrierInvoiceID,
                        'description'           => $defaultDescription,
                        'balance'               => $walletOpenBal,
                        'created_at'            => \Carbon\Carbon::now()->toDateTimeString(),
                        'updated_at'            => \Carbon\Carbon::now()->toDateTimeString(),
                    ]);

                    //insert carrier route price
                    $path3 = $request->file('carrierPricing')->getRealPath();
                    $data3 = Excel::load($path3)->get();
                    if($data3->count()){
                        foreach ($data3 as $value) {
                            $formattedPrice = rtrim($value->carrierprice,',');
                            $arrNew3[] = [
                                'carrier_id' => $carrierCompanyID,
                                'origin' => $value->origin,
                                'destination' => $value->destination,
                                'resourceType' => $value->resourcetype,
                                'price' => $formattedPrice,
                            ];
                        }
                        $carrRouteResPrices =  DB::table('carrier_company_route_prices')->insert($arrNew3);
                    }else{
                        return response()->json(['error'=>'Carrier File Error!']);
                    }

                    // //insert carrier resource gps details
                    // $pathCarrierResources = $request->file('carrierResources')->getRealPath();
                    // $dataCarrierResources = Excel::load($pathCarrierResources)->get();
                    // if($dataCarrierResources->count()){
                    //     foreach ($dataCarrierResources as $value) {
                    //         $arrNewCarrierResources[] = [
                    //             'company_id' => $carrierCompanyID,
                    //             'GPSID' => $value->gpsid,
                    //             'capacity' => $value->capacity,
                    //             'plateNumber' => $value->platenumber,
                    //             'resourceStatus' => $resourceStatus,
                    //             'driverName' => $value->drivername,
                    //             'driverPhoneNumber' => $value->driverphonenumber,
                    //             'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
                    //             'updated_at' => \Carbon\Carbon::now()->toDateTimeString(),
                    //         ];
                    //     }
                    //     $CarrierResourceDetails =  DB::table('carrier_resource_details')->insert($arrNewCarrierResources);
                    // }else{
                    //     return response()->json(['error'=>'Carrier File Error!']);
                    // }

                    for($i = 0; $i < count($zone); $i++){
                        $arrZones[] = [
                            'company_id' => $carrierCompanyID,
                            'regions_covered' => $request->region[$i],
                        ];
                    }

                    if (!empty($arrZones)){
                         $carrier_regions =  DB::table('carrier_company_regions')->insert($arrZones);
                    }
                    //end carrier zones insert


                    if($userID && /*$resourceDetails &&*/ $carrier_regions){
                        //send carrier default user password
                        $user = User::find($userID);
                        $notification = $user->notify(new newCarrierAccount($defaultPassword));

                        return response()->json(['success'=>'New Haulage Provider Created Successfully']);

                    }else{
                        return response()->json(['error'=>'Something Went Wrong with DB Inserts']);
                    }

                }else{
                    return response()->json(['error'=>$validator3->errors()->all()]);
                }


        }

    }


    public function viewAllClients()
    {
        $category = 0;
        $allClients = Company::where('category',$category)->get();
        return view('admin.createClientAgent',['allClients'=>$allClients]);
    }

    public function createClientAgent($id){
        $companyID = $id;
        return view('admin.createClientAgent',['companyID'=>$companyID]);
    }

    public function saveClientAgent(Request $request)
    {
        $companyID = $request->companyID;
        $email = $request->email;
        $agentName = $request->agentName;/*
        $roleID = $request->roleID;*/
        $defaultPassword = $this->randomNumberForPassword();
        $bcryptPass = bcrypt($defaultPassword); 

        $validator = Validator::make($request->all(),[
            'email'=>'required|unique:users',
            'agentName'=>'required',
            'companyID'=>'required',/*
            'roleID'=>'required',*/
            ]);

        if ($validator->passes()) {

            $insertUserDetailsID = DB::table("users")->insertGetId([
                'name' => $agentName,
                'email' => $email,
                'password' => $bcryptPass,
                'default_password' => $bcryptPass,
                'userAccessLevel' => 1,
                'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
                'updated_at' => \Carbon\Carbon::now()->toDateTimeString(),
            ]);

            /*$insertUserRolesID = DB::table("user_roles")->insert([
                'user_id' => $insertUserDetailsID,
                'company_id' => $companyID,
                'role' => $roleID,
                'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
                'updated_at' => \Carbon\Carbon::now()->toDateTimeString(),
            ]);*/

            $insertUserCompanyID = DB::table("user_comapanies")->insert([
                'user_id' => $insertUserDetailsID,
                'company_id' => $companyID,
                'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
                'updated_at' => \Carbon\Carbon::now()->toDateTimeString(),
            ]);

            $user = User::find($insertUserDetailsID);
            $notification = $user->notify(new newClientAccount($defaultPassword));

            return response()->json(['success'=>'New Agent Created Successfully']);
        }else{
            return response()->json(['error'=>$validator->errors()->all()]);
        }
    }

    public function getAgentDetails($userID)
    {
        $userResult = array();
        $userGet = $userID;
        for ($i=0; $i < count($userGet); $i++) { 
           $userResult[] = User::where('id',$userGet[$i])->get(); 
        }
        return $userResult;
    }

    public function viewClientAgent($id)
    {
        $userAgent = UserComapany::where('company_id',$id)->get();
        $userID = array();
        foreach ($userAgent as $user) {
            $userID[]=$user->user_id;
        }
        $clientAgent = $this->getAgentDetails($userID);
        return view('admin.clientAgent',['clientAgent'=>$clientAgent]);
    }

}
