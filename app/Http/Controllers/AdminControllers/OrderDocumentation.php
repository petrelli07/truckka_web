<?php

namespace App\Http\Controllers\AdminControllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\CarrierDetail;
use App\HaulageResourceRequest;
use App\User;
use App\ServiceRequest;
use App\ResourceGpsDetail;
use App\OrderResourceGpsDetail;
use Auth;
use App\OrderDocument;
use App\OrderDocDest;
use App\InvoiceDetail;
use App\CarrierCompanyWallet;
use App\CarrierInvoice;
use App\FinalInvoice;
use App\OrderResource;
use App\OrderDocumentDestination;
use App\CarrierCompanyRoutePrice;
use Validator;
use DB;

class OrderDocumentation extends Controller
{

    public function saveOrderDocumentOrigin(Request $request){
        
        $orderDocument = $request->file('orderDocument');
        $service_id = $request->service_id;
        $timeIn = $request->timeIn;
        $timeOut = $request->timeOut;
        $atlNo = $request->atlNo;
        $cargoWeight = $request->cargoWeight;
        $cargoDescription = $request->cargoDescription;
        $comments = $request->comments;
        $user_id = Auth::user()->id;

        if($request->hasFile('orderDocument')){

            $validator = Validator::make($request->all(), [
                'timeIn.*'=>'required',
                'timeOut.*'=>'required',
                'cargoWeight.*'=>'required',
                'cargoDescription.*'=>'required',
                'atlNo.*'=>'required',
                'orderDocument.*'=>'required'
            ]);

            if($validator->passes()){
                
                for($i=0; $i<count($orderDocument); $i++){
                    $path = $request->file('orderDocument')[$i]->store('ordersDocs','dropbox');
                    $orderDoc = new OrderDocument;
                    $orderDoc->service_id = $service_id;
                    $orderDoc->user_id = $user_id;
                    $orderDoc->fileName = $path;
                    $orderDoc->timeIn = $timeIn[$i];
                    $orderDoc->atlNo = $atlNo[$i];
                    $orderDoc->timeOut = $timeOut[$i];
                    $orderDoc->cargoWeight = $cargoWeight[$i];
                    $orderDoc->cargoDescription = $cargoDescription[$i];
                    $orderDoc->comments = $comments[$i];
                    $orderDoc->GPSID = "00000000";
                    $orderDoc->mode = 0;
                    $saveUpload = $orderDoc->save();
                }

                $dataStatus = array(
                        'orderStatus' => 9
                    );

                    ServiceRequest::where(
                        'id',$service_id)
                        ->update($dataStatus);

                if ($saveUpload) {
                    return response()->json(['success'=>"Upload Successful"]);
                }else{
                    return response()->json(['error'=>"Something went wrong with the upload"]);
                }

            }else{
                return response()->json(['error'=>$validator->errors()->all()]);
            }

        }else{
                return response()->json(['error'=>"Please Upload a file for each order"]);

        }       
    }

    public function CarrierPrice($carrID1,$service_id){
        $carrID = $carrID1;
        $servID = $service_id;

        $orderDetails = ServiceRequest::where('id',$servID)->get();

        foreach ($orderDetails as $k) {
            $origin = $k->deliverFrom;
            $destination = $k->deliverTo;
        }

        $orderID = $servID;

        $requiredResources = HaulageResourceRequest::where('service_request_id',$orderID)
                                                    ->where('carrier_id',$carrID)
                                                    ->value('resourceTypeNumber');

        $HaulageResourceRequestStatus = HaulageResourceRequest::where('service_request_id',$orderID)
                                                    ->where('carrier_id',$carrID)
                                                    ->value('status');

        $jsonDecodeVal  = json_decode($requiredResources,1);

        $resourceType = $jsonDecodeVal['resourceType'];
        $resourceNumber = $jsonDecodeVal['resourceNumber'];

        $price =            CarrierRouteResourcePrice::where('carrier_id',$carrID)
                                                        ->where('origin',$origin)
                                                        ->where('destination',$destination)
                                                        ->where('resourceType',$resourceType)
                                                        ->value('price');

        return $finalPrice = $price * $resourceNumber;
    }

    public function viewOrderResources($serviceIDNo){
        $service_id         =    ServiceRequest::where('serviceIDNo', $serviceIDNo)->value('id');
        $orderResources     =    OrderResource::where('order_id', $service_id)->where('status',0)
                                ->join('carrier_resource_details', 'carrier_resource_details.id', 
                                        '=', 'order_resources.resource_id')
                                ->select('carrier_resource_details.*', 'order_resources.*')
                                ->get();
        //return $orderResources;
        return view('admin.documents.OrderResources', ['orderResources'=>$orderResources, 'service_id'=>$service_id]);
    }




    public function viewOrderResourcesDestination($serviceIDNo){
        $service_id         =    ServiceRequest::where('serviceIDNo', $serviceIDNo)->value('id');
        $orderResources     =    OrderResource::where('order_id', $service_id)->where('status',0)
                                ->join('carrier_resource_details', 'carrier_resource_details.id', 
                                        '=', 'order_resources.resource_id')
                                ->select('carrier_resource_details.*', 'order_resources.*')
                                ->get();
        //return $orderResources;
        /*$res = array();
        foreach ($orderResources as $order) {
            $res[] =$order->id;
        }
        $resourceDetail = $this->getResourceDetails($res);*/
        return view('admin.documents.OrderResourcesDest', ['orderResources'=>$orderResources, 'service_id'=>$service_id]);
    }




    public function getResourceDetails($res){
        $resourceDetails = array();
        for($i=0; $i<count($res); $i++){
            $resourceDetails[] = OrderResourceGpsDetail::where('id',$res[$i])->get();
        }
        return $resourceDetails;
    }





    public function saveOrderDocuments(Request $request){

        $orderDocument = $request->file('orderDocument');
        $service_id = $request->service_id;
        $timeIn = $request->timeIn;
        $timeOut = $request->timeOut;
        $atlNo = $request->atlNo;
        $cargoWeight = $request->cargoWeight;
        $cargoDescription = $request->cargoDescription;
        $comments = $request->comments;
        $gpsid = $request->gpsid;
        $user_id = Auth::user()->id;

        if($request->hasFile('orderDocument')){

            $validator = Validator::make($request->all(), [
                'timeIn.*'=>'required',
                'timeOut.*'=>'required',
                'cargoWeight.*'=>'required',
                'cargoDescription.*'=>'required',
                'atlNo.*'=>'required',
                'orderDocument.*'=>'required'
            ]);

            if($validator->passes()){

                for($i=0; $i<count($orderDocument); $i++){
                    $path = $request->file('orderDocument')[$i]->store('destDocs');
                    $orderDoc = new OrderDocumentDestination;
                    $orderDoc->service_id = $service_id;
                    $orderDoc->user_id = $user_id;
                    $orderDoc->fileName = $path;
                    $orderDoc->timeIn = $timeIn[$i];
                    $orderDoc->atlNo = $atlNo[$i];
                    $orderDoc->timeOut = $timeOut[$i];
                    $orderDoc->cargoWeight = $cargoWeight[$i];
                    $orderDoc->cargoDescription = $cargoDescription[$i];
                    $orderDoc->comments = $comments[$i];
                    $orderDoc->GPSID = "000000";
                    $orderDoc->mode = 0;
                    $saveUpload = $orderDoc->save();
                }

                $paymentType = ServiceRequest::where('id',$service_id)->value('paymentType');
                $serviceIDNo = ServiceRequest::where('id',$service_id)->value('serviceIDNo');
                $weight = ServiceRequest::where('id',$service_id)->value('estimatedWgt');
                $orderDate = ServiceRequest::where('id',$service_id)->value('created_at');


                if ($saveUpload) {

                    ///finalInvoice for Client

                    $dataStatus = array(
                        'orderStatus' => 10
                    );

                    ServiceRequest::where(
                        'id',$service_id)
                        ->update($dataStatus);

                $paymentType = ServiceRequest::where('id',$service_id)->value('paymentType');
                $serviceIDNo = ServiceRequest::where('id',$service_id)->value('serviceIDNo');
                $weight = ServiceRequest::where('id',$service_id)->value('estimatedWgt');
                $origin = ServiceRequest::where('id',$service_id)->value('deliverFrom');
                $destination = ServiceRequest::where('id',$service_id)->value('deliverTo');
                $userID = ServiceRequest::where('id',$service_id)->value('createdBy');
                $orderDate = ServiceRequest::where('id',$service_id)->value('created_at');


                $invoiceNo = $this->finalInvoiceNumber();

                $fileContent = InvoiceDetail::where('service_requests_id',$service_id)->value('orderDetail');

                $decodedFileContent = json_decode($fileContent, 1);

                $amt = $decodedFileContent['totalAmount'];

                /*$userID = Auth::user()->id;*/

                DB::table("final_invoices")->insert([
                    'user_id'=>$userID,
                    'invoice_no'=>$invoiceNo,
                    'service_id_no' => $serviceIDNo,
                    'orderDate' => $orderDate,
                    'weight' => $weight,
                    'status' => 0,
                    'description' =>  "Being Payment for Order No:".$serviceIDNo." Movement of goods from ".$origin." to ".$destination.".",
                    'totalAmount' => $amt,
                    'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
                    'updated_at' => \Carbon\Carbon::now()->toDateTimeString(),
                ]);
                //end client Invoice Create

                //get carrier ids
                $carriers = OrderResource::where('order_id', $service_id)->where('status',0)
                                            ->join('carrier_resource_details', 'carrier_resource_details.id', 
                                                    '=', 'order_resources.resource_id')
                                            ->select('carrier_resource_details.*', 'order_resources.*')
                                            ->get();

                for ($i=0; $i < count($carriers); $i++) { 
                    $carrierIDs[] = $carriers[$i]['company_id'];

                    $dataStatus = array(
                        'status' => 1
                    );

                    // OrderResource::where(
                    //     'order_id', $service_id)
                    //     ->update($dataStatus);
                }


                $uniqueCarrierIDArray = array();
                $uniqueCarrierIDArray[] = array_unique($carrierIDs);
                $carrierInvoiceNo = $this->carrierInvoice();
                //end get carrier ids

                $newCleanArr = array_values($uniqueCarrierIDArray[0]);

                ///create Carrier Invoices
                for ($x=0; $x < count($newCleanArr); $x++) { 

                    $carrierResourceRequestNo = OrderResource::where('order_id', $service_id)->where('status',0)
                                                            ->join('carrier_resource_details', 'carrier_resource_details.id', 
                                                                    '=', 'order_resources.resource_id')
                                                            ->select('carrier_resource_details.*', 'order_resources.*')
                                                            ->get();

                    //work on getting the number of trucks committed per carrier

                    $countResources = count($carrierResourceRequestNo);

                    $carrierResPrice = CarrierCompanyRoutePrice::where('carrier_id',$newCleanArr[$x])
                                        ->where('origin',$origin)
                                        ->where('destination',$destination)
                                        ->value('price');

                    $finalPrice = $countResources * $carrierResPrice;


                    $walletBalance = CarrierCompanyWallet::where('company_id',$newCleanArr[$x])
                                                    ->orderBy('created_at', 'desc')
                                                    ->value('balance');

                    $newBalance = $finalPrice + $walletBalance;

                    $description = "Being Payment for Order No:".$serviceIDNo." Movement of goods from ".$origin." to ".$destination.".";

                     $invoiceID = DB::table("carrier_company_invoices")->insertGetId([
                            'carrier_id' => $newCleanArr[$x],
                            'service_id' => $service_id,
                            'status' => 0,
                            'invoice_id' => $carrierInvoiceNo,
                            'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
                            'updated_at' => \Carbon\Carbon::now()->toDateTimeString(),
                    ]);

                     DB::table("carrier_company_wallets")->insert([
                            'company_id' => $newCleanArr[$x],
                            'admin_id' => $user_id,
                            'cr' => $finalPrice,
                            'description' => $description,
                            'invoice_id' => $invoiceID,
                            'balance' => $newBalance,
                            'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
                            'updated_at' => \Carbon\Carbon::now()->toDateTimeString(),
                    ]);      
                    
                }
                ///End Create Carrier Invoices

                    return response()->json(['success'=>"Upload Successful"]);
                }else{
                    return response()->json(['error'=>"Something went wrong with the upload"]);
                }

            }else{
                return response()->json(['error'=>$validator->errors()->all()]);
            }

        }else{
            return response()->json(['error'=>"Please Upload a file for each order"]);

        }
}

    public function UpdateResourceStat($array){
        $collect = array();
        for($i=0;$i<count($array);$i++){
            $collect[] = ResourceGpsDetail::where(
                'id',$array[$i])->value('id');
        }
        return $collect;
    }

    public function getCarrierID($service_id){
        $carriers = HaulageResourceRequest::where('service_request_id',$service_id)->get();
        $arr = array();
        for($i=0;$i<count($carriers);$i++){
            $arr[] = $carriers[$i]['carrier_id'];
        }
        return $arr;
    }

    public function finalInvoiceNumber(){

        $i = 1;

        for($j=0; $j < $i; $j++){
//            return random_int(1,10000);

            $result = '';

            $length = 7;

            for($x = 0; $x < $length; $x++) {
                $result .= mt_rand(0, 9);
            }

            $checkAvailable = FinalInvoice::where('invoice_no', $result);

            if( $checkAvailable->count() < 1 ){

                return $result;

                break;
            }

            $i++;

        }
    }

    public function carrierInvoice(){

        $i = 1;

        for($j=0; $j < $i; $j++){
//            return random_int(1,10000);

            $result = '';

            $length = 7;

            for($x = 0; $x < $length; $x++) {
                $result .= mt_rand(0, 9);
            }

            $checkAvailable = CarrierInvoice::where('invoice_id', $result);

            if( $checkAvailable->count() < 1 ){

                return $result;

                break;
            }

            $i++;

        }
    }




}
