<?php

namespace App\Http\Controllers\AdminControllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\IncidentReport;
use App\IncidentReportDetail;

class IncidentManagementController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function viewAllIncidents(){
        $incidents = IncidentReport::all();
        return view('admin.incidentManagement.allIncidents', ['incidents'=>$incidents]);
    }

    public function viewIncidentThread($id){
        $incidentReportDetail = IncidentReportDetail::where('incident_report_id', $id)->get();
        $incidentReport = IncidentReport::where('id', $id)->get();

        return view('admin.incidentManagement.incidentThread',['incidentReportDetail'=>$incidentReportDetail, 'incidentReport'=>$incidentReport]);
    }

    public function updateIncidentThread($id){
        $incidentReport = IncidentReport::where('id',$id)->value('id');
        $incidentReportID = IncidentReport::where('id',$id)->get();
        return view('admin.incidentManagement.updateIncidentThread', ['incidentReportID'=>$incidentReportID, 'incidentReport'=>$incidentReport]);
    }

    public function editIncidentThread(Request $request){

        $reportID = $request->reportID;
        $incidentComment = $request->incidentComment;

        $validator = validator::make($request->all(),[
                    'incidentComment' => 'required'
                    ]);

        if($validator->passes()){

            $update_incident_report_details = DB::table("incident_report_details")->insert([
                'incident_report_id' => $reportID,
                'remarks' => $incidentComment,
                'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
                'updated_at' => \Carbon\Carbon::now()->toDateTimeString(),
            ]);

            if($update_incident_report_details){
                return response()->json(['success'=>'Incidence Thread Updated']);
            }else{
                return response()->json(['success'=>'An error occurred']);
            }

        }else{
            return response()->json(['error'=>$validator->errors()->all()]);
        }
    }

    public function closeIncidentThread($id){

        $data = array(
            'status' => 1
        );

        $closeThread = IncidentReport::where(
            'id',$id)
            ->update($data);

        if($closeThread){
            return back()->with('message', 'Incident Closed');
        }else{
            return back()->with('message', 'Something Went wrong');
        }

    }

}
