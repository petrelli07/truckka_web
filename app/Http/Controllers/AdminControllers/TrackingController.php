<?php

namespace App\Http\Controllers\AdminControllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\ServiceRequest;
use App\OrderResourceGpsDetail;
use App\ResourceGpsDetail;
use App\CarrierResourceDetail;
use Mapper;

class TrackingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function testfn($arr){

        $joinRes = array();
        for($i = 0; $i < count($arr); ++$i){//get resource details

            $joinRes[$i] = DB::table('resource_gps_details')->where('id', '=', $arr[$i])
                ->get();
        }
        return $joinRes;

    }

    public function map(){
        return view('adminTrackerDemo');
    }
    
    public function fetchAllOrders(){
        $allRequests = serviceRequest::all();
        return view('admin.tracker.allOrdersGPS',['allRequests'=>$allRequests]);
    }

    public function fetchAllGPSDetails($id){
        //$int = (int)$id;

       $orderDetail = OrderResourceGpsDetail::where('service_requests_id',$id)->get();

        foreach($orderDetail as $carr){//get carrier ids from origin destination map
            $arr[] = [ $carr->resource_gps_details_id ];
        }

        $res =  $this->testfn($arr);

        return view('admin.tracker.orderResourceList',['res'=>$res]);

        /*$gpsDetails = DB::table('service_requests')
            ->join('order_resource_gps_details', 'service_requests.id', '=', 'order_resource_gps_details.service_requests_id')
            ->select('service_requests.*', 'order_resource_gps_details.*')->where('service_requests_id', '=', $id)
            ->get();*/

        //return $gpsDetails;

    }

    public function viewOnMap($gpsid){

        $u = 'http://80.241.215.74/api/api.php?api=user&ver=1.0&key=2EC94CFEC58F96F44CD6F3D9CA587C9F&cmd=OBJECT_GET_LOCATIONS,'.$gpsid;

        $firstURL = file_get_contents($u);

       $json = json_decode($firstURL, 1);

        foreach($json as $first){
            $lat = $first['lat'];
            $lng = $first['lng'];
        }

        $location = file_get_contents("http://80.241.215.74/api/api.php?api=user&ver=1.0&key=2EC94CFEC58F96F44CD6F3D9CA587C9F&cmd=GET_ADDRESS,".$lat.",".$lng);

        //return $location;

        Mapper::map($lat, $lng, ['zoom' => 18, 'center' => true, 'marker' => false]);
        Mapper::informationWindow($lat, $lng, $location);

            /*Mapper::map();*/

            return view('admin.tracker.location');

    }


    public function allResources()
    {
        //$resources = CarrierResourceDetail::all();

        $resources = CarrierResourceDetail::all();

        $mapinfo = [];

        foreach ($resources as $info) {
            $mapinfo[] = array(
                'id' => $info->id,
                'label' => $info->plateNumber,
                'gpsid' => $info->GPSID,
            );
        }


        // var_dump($resourcesDetails->toArray());
        // exit;
        $data = array(
            'mapinfo' => $mapinfo,
            'data' => $resources,
        );

        return view('admin.tracker.track',$data);
    }


}
