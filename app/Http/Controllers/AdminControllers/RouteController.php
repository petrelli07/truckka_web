<?php

namespace App\Http\Controllers\AdminControllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\CarrierInvoice;
use App\ClientInvoice;
use App\ClientOrder;
use Auth;
use App\ServiceRequest;
use App\carrierResource;
use App\CarrierDetail;
use App\User;
use App\HaulageResourceRequest;
use App\ResourceTypeNumber;
use App\carrierRouteMap;
use DB;
use Validator;
use Pagination;
/*use Notification;
use Notify;
use App\Notifications\newHaulageRequest;
use App\Notifications\newCarrierInvoice;
use App\Notifications\newClientInvoice;
use App\Notifications\Mobilized;*/
use App\carrierRegion;
use App\Route;

class RouteController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


	public function randomNumber(){

        $i = 1;

        for($j=0; $j < $i; $j++){
//            return random_int(1,10000);

            $result = '';

            $length = 3;

            for($x = 0; $x < $length; $x++) {
                $result .= mt_rand(1, 9);
            }

            $checkAvailable = Route::where('routeCode', $result);

            if( $checkAvailable->count() < 1 ){

                return $result;

                break;
            }
            $i++;

        }
    }



    public function newRoute()
    {
        return view('admin.newRoute');
    }

    public function newRouteUpload(Request $request)
    {
    	$validation = Validator::make($request->all(),[
    		'routeFile'=>'required|file|mimes:xlsx'
    		]);

        if ($validatorClientCompany->passes()) {

            if($request->hasFile('routeFile')){

                $path = $request->file('routeFile')->getRealPath();
                $data = Excel::load($path)->get();

                if($data->count()){
                    foreach ($data as $value) {
                        $arr[] = [
                        		'routeCode'=>$this->randomNumber,
                                'origin' => $origin,
                                'destination' => $value->destination
                        ];
                    }
                    
                if(!empty($arr)){
                    DB::table('client_orders')->insert($arr);
                        
                    return response()->json(['success'=>'New Client Created Successfully']);
                    
                }else{
                    return response()->json(['error'=>'Something Went Wrong. Unable to create']);
                }

                }else{
                        return response()->json(['error'=>'File Path Not Found']);
                }//process excel file
            }else{//if file does not exist
                return response()->json(['error'=>'Something Went Wrong. No file found']);
            }
        }else{//if client validation fails
            return response()->json(['error'=>$validatorClientCompany->errors()->all()]);
        }//end client reg
    }


}
