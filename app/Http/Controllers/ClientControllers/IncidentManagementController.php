<?php

namespace App\Http\Controllers\ClientControllers;

use App\Http\Controllers\Controller;
use App\IncidentReport;
use App\IncidentReportDetail;
use Illuminate\Http\Request;
use Auth;

class IncidentManagementController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function viewAllIncidents(){

        $userID = Auth::user()->id;
        $incidents = IncidentReport::where('clientID', $userID)->get();
        return view('client.incidentManagement.allIncidents', ['incidents'=>$incidents]);

    }

    public function viewIncidentThread($id){
        $incidentReportDetail = IncidentReportDetail::where('incident_report_id', $id)->get();
        $incidentReport = IncidentReport::where('id', $id)->get();

        return view('client.incidentManagement.incidentThread',['incidentReportDetail'=>$incidentReportDetail, 'incidentReport'=>$incidentReport]);
    }



}
