<?php

namespace App\Http\Controllers\TMSW\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\ManageClientSupplier;
use App\TMSW\tmswCompanyDetail;
use App\TMSW\tmswCompanyUser as UserComapany;
use App\TMSW\tmswClientSupplyOrder as ClientSupplyOrder ;
use App\ClientSupplyOrderQuote;
use App\TMSW\tmswSuppliersGroup as SuppliersGroup;
use App\TMSW\tmswSuppliersGroupList as SuppliersGroupList;
use App\TMSW\tmswSupplyRequest as SupplyRequest;
use App\TMSW\tmswClientSupplierDetail as ClientSupplierDetail;
use App;
use DB;
use Validator;
use App\User;
use App\Notifications\newClientAccount;
use App\Notifications\tmsw\newAccount;
use \App\Http\Controllers\AdminControllers\UserManagementController as Admin;
use \App\Http\Controllers\ClientControllers\UserManagementController as Client;
use Image;

class SupplierController extends Controller{
	public $userId;
    public $companyID;



    public function userAccessLevel(){
        $role  =   Auth::user()->userAccessLevel;
        switch ($role) {
            case 12:
                return 'administrator';
                break;
            case 13:
                return 'manager';
                break;
            case 14:
                return 'desk_officer';
                break;
            case 15:
                return 'finance';
                break;
            case 16:
                return 'transporter';
                break;
            case 17:
                return 'supplier';
                break;
            default:
                return '/';
                break;
        }
    }

    public function userInit(){
      
      //TODO get userId from table

      $userID           =   Auth::user()->id;
      
      $companyID        =   UserComapany::where('user_id',$userID)
                                        ->value('tmsw_company_detail_id');
      
      $this->userId     =   $userID;
      $this->companyID  =   $companyID;

    }

    public function activate(array $where,string $table){
    	DB::table($table)->where($where)->update(['status'=> '1']);
    }

    public function deactivate(array $where,string $table){
      DB::table($table)->where($where)->update(['status'=> '2']);
    }

    public function reject(array $where,string $table){
    	DB::table($table)->where($where)->update(['status'=> '3']);
    }

    public function approve(array $where,string $table){
    	DB::table($table)->where($where)->update(['status'=> '4']);
    }

    public function pending(array $where,string $table){
      DB::table($table)->where($where)->update(['status'=> '5']);
    }

    public function getIndex(){
        
        $breadcrumb = 'Supplier dashboard';

        $accessLevel    =   $this->userAccessLevel();
        return view('tmsw.supplier.index')
        			->withBreadcrumb($breadcrumb)
        			->withAccessLevel($accessLevel);
    }
	
	public function getGroups(){
	  
	     $this->userInit();

	     
	     $companyGroups   =  SuppliersGroup::where('company_id','=',$this->companyID)
	                                        ->get();
	     return $companyGroups;
	}



  public function createSupplierView(){

      $breadcrumb 			  =  'create supplier';

      $getGroups              =   $this->getGroups();      
      $data                   =   array('groups' => $getGroups,
      																	'supplier'=> null
    																		);

      $accessLevel    =   $this->userAccessLevel();

      return view('tmsw.client.supply.create-supplier',$data)
          ->withBreadcrumb($breadcrumb)
          ->withAccessLevel($accessLevel);
  }

  public function getSupplierGroup($supplierId,$companyId){
  	
  	return SuppliersGroupList::where('tmsw_suppliers_group_lists.supplier_id',$supplierId)
  														->where('tmsw_client_supplier_details.company_id',$companyId)
  														->join('tmsw_client_supplier_details','tmsw_client_supplier_details.id', '=','tmsw_suppliers_group_lists.supplier_id')
  														->get();
  }

  public function getSupplierDetails($supplierId){
  	 
  	return ClientSupplierDetail::where('company_id',$this->companyID)
      																					->join('users', 'tmsw_client_supplier_details.user_id', '=', 'users.id')
      																					->select('users.*','tmsw_client_supplier_details.*')
                                                ->get()
                                                ->first();
  }

  public function editSupplierView($supplierId){

  		$this->userInit();

      $breadcrumb 			  		=  'create supplier';
      $supplier								=		$this->getSupplierDetails($supplierId);
      $getGroups              =   $this->getGroups();
      $supplierGroups					=		array_column($this->getSupplierGroup($supplierId,$this->companyID)->toArray(), 'group_id');      
     
      $data                   =   array('groups' 					=> 	$getGroups,
      																	'supplier'				=>	$supplier,
      																	'selectedGroups'	=>	$supplierGroups
    																		);

      $accessLevel    =   $this->userAccessLevel();
      return view('tmsw.client.supply.create-supplier',$data)
          ->withBreadcrumb($breadcrumb)
          ->withAccessLevel($accessLevel);
  }

  public function editSupplier(Request $request, $supplierId){

  	  	$this->userInit();

        $validator             =    Validator::make($request->all(),[
            'supplier_name'    				=>   'required',
            'supplier_address'        =>   'required'
            ]);

        if ($validator->passes()) {

//////////////////Start Database Transaction/////////////////////////////////////////////

            DB::transaction(function() use($request,$supplierId){
                
                $admin                 =   new Admin();
                $companyID             =   $this->companyID;
                $address               =   $request->supplier_address;
                $groupIds              =   $request->group;

                $data 								 =	 array('supplierName'			=>  $request->supplier_name,
                																 'supplierAddress'	=>  $request->supplier_address
              																	);

                ClientSupplierDetail::where('id',$supplierId)->where('company_id',$this->companyID)->update($data);

                $saveGroupList  = SuppliersGroupList::where('supplier_id',$supplierId)->delete();

                foreach ($groupIds as $groupId) {
     
						      $supplierInsert = array('supplier_id'  => $supplierId,
						                              'group_id'     => $groupId
						                              );

						      $saveGroupList  = SuppliersGroupList::insert($supplierInsert);
					      }
                
                
            },2);

            return back()->withMessage('Successful');
        }else{
            return back()->withMessage('Not Successful');
        }


  }

  public function createNewSupplier(Request $request){
          
        $this->userInit();

        $validator             =    Validator::make($request->all(),[
            'email'   								=>   'required|unique:users',
            'supplier_name'    				=>   'required',
            'supplier_address'        =>   'required'
            ]);

        if ($validator->passes()) {

//////////////////Start Database Transaction/////////////////////////////////////////////
   try {
            DB::transaction(function() use($request){
                
                $admin                 =   new Admin();
                $companyID             =   $this->companyID;
                $userType              =   17;
                $email                 =   $request->email;
                $agentName             =   $request->name;
                $address               =   $request->supplier_address;
                $defaultPassword       =   $admin->randomNumberForPassword();
                $groupId               =   $request->group;
                $bcryptPass            =   bcrypt($defaultPassword); 

//////////////////////////Create New User///////////////////////////////////////////////
                
                $insertUserDetailsID    = DB::table("users")->insertGetId([
                    'name'              => $agentName,
                    'email'             => $email,
                    'password'          => $bcryptPass,
                    'default_password'  => $bcryptPass,
                    'userAccessLevel'   => 17,
                    'created_at'        => \Carbon\Carbon::now()->toDateTimeString(),
                    'updated_at'        => \Carbon\Carbon::now()->toDateTimeString(),
                ]);


/////////////////////////////////Save user into supplier's table and get the id////////////////////////////////////////////

                $supplier               = $this->createSupplier($insertUserDetailsID,$agentName,$address);

////////////////////////////////Add Supplier to Group /////////////////////////////////////////////////////

                if (is_array($groupId)) {
                  $supplierGroup        =  $this->addSupplierToGroup($groupId, $supplier);
                }

////////////////Send Mail to the User/////////////////////////////////////////////////////////////
               

                $user         	= User::find($insertUserDetailsID);
                
                	$notification = $user->notify(new newAccount($defaultPassword));
                	
                
                
            },2);
                } catch (\Exception $e) {
                	
                	return back()->withMessage('Not Successful');	
                }

            return back()->withMessage('Successful');
        }else{
            return back()->withMessage('Not Successful');
        }
    }

    public function createSupplier($userId,$name,$address){
      
      $this->userInit(); 

      $data           = array('user_id'         =>  $userId,
                              'company_id'      =>  $this->companyID,
                              'supplierAddress' =>  $address,
                              'supplierName'    =>  $name
                              );

      $saveSupplier   = ClientSupplierDetail::insertGetId($data);

      return $saveSupplier;
    }

    public function addSupplierToGroup($groupIds,$supplier){
     
      foreach ($groupIds as $groupId) {
     
	      $supplierInsert = array('supplier_id'  => $supplier,
	                              'group_id'     => $groupId
	                              );

	      $saveGroupList  = SuppliersGroupList::insert($supplierInsert);
      }

    }

    public function allSuppiers(){
      
      $this->userInit();

      $suppliersDetail =   ClientSupplierDetail::where('company_id',$this->companyID)
      																					->join('users', 'tmsw_client_supplier_details.user_id', '=', 'users.id')
      																					->select('users.*','tmsw_client_supplier_details.*')
                                                ->get();
      return $suppliersDetail;
    }

    public function allSuppliersView(){
       
      $breadcrumb = 'Suppliers';

      $suppliersDetails   = $this->allSuppiers();

      $data               = array('suppliers' => $suppliersDetails
                                );

        $accessLevel    =   $this->userAccessLevel();
      return view('tmsw.client.supply.all-Suppliers',$data)
          ->withBreadcrumb($breadcrumb)->withAccessLevel($accessLevel);

    }

    public function createSupplyView(){
      
      $this->userInit();

    	$companyGroups   =  $this->getGroups();
    	
      $breadcrumb = 'client';
      $data            =  array('groups' => $companyGroups);

        $accessLevel    =   $this->userAccessLevel();
      return view('tmsw.client.supply.create-supply-order',$data)->withBreadcrumb($breadcrumb)->withAccessLevel($accessLevel);
    }


    public function submitNewSupplyOrder(Request $request){


      $validator = Validator::make($request->all(), [
              
                'Item' 						=> 'required',
                'expiration'      => 'required',
                'quantity'        => 'required',
                'group'          => 'required',
                'file'          =>  'required|file|mimes:jpeg,jpg,png,pdf,docx,doc|max:512'
            ]);

      if($validator->passes()){
           
        $this->userInit(); 
        
        DB::transaction(function()use($request){
          

          $groupMembersList                 =  [];
          $purchaseOrderFilePath            =  $request->file('file');
         //$purchaseOrderFilePath = '/';
            $originalImage_purchaseOrderFile  = $purchaseOrderFilePath;
            $thumbnailImage_purchaseOrderFile  = Image::make($originalImage_purchaseOrderFile);
            $originalPath_purchase_order    = public_path().'/tmsw/clientPurchaseOrders/';
            $thumbnailImage_purchaseOrderFile->save($originalPath_purchase_order.$originalImage_purchaseOrderFile->getClientOriginalName());

          $purchase_order_filename          =   $originalImage_purchaseOrderFile->getClientOriginalName();
          $expiryDate                       =  date("Y-m-d", strtotime($request->expiration));
          $quantity                         =  $request->quantity;
          $itemDescription                  =  $request->Item;
          $groups                           =  $request->group;

            // Get group members of individual group and merge it  
          foreach ($groups as $groupId) {
            
            $groupMembers       = $this->getGroupMembers($groupId, $this->companyID)->toArray();
            $groupMembersList   = array_merge($groupMembersList, array_column($groupMembers, 'supplier_id'));

          }

          //Create new Supply Order
          $supplyOrderId         =  DB::table("tmsw_client_supply_orders")->insertGetId(
                [
                    'orderDescription'    => $itemDescription,
                    'orderQuantity'       => $quantity,
                    'purchaseOrder'       => $purchase_order_filename,
                    'expires_at'          => $expiryDate,
                    'client_user_id'      => $this->userId,
                    'client_company_id'   => $this->companyID,
                    'created_at'          => \Carbon\Carbon::now()->toDateTimeString(),
                    'updated_at'          => \Carbon\Carbon::now()->toDateTimeString(),
                ]);

          //Remove Duplicates and run a foreach loop 
          foreach (array_unique($groupMembersList) as $groupMember) {
          
     
            DB::table("tmsw_supply_requests")->insert(
                [
                    'client_supply_order_id'=>$supplyOrderId,
                    'client_id'            => $this->companyID,
                    'supplier_id'           => $groupMember,
                    'created_at'            => \Carbon\Carbon::now()->toDateTimeString(),
                    'updated_at'            => \Carbon\Carbon::now()->toDateTimeString(),
                ]);


          }
          	

        });

      	return back()->withMessage('Successful');     

	    }else{
        return back()->withMessage('Could not complete your request');         
	    }

    }
		
		public function getGroupMembers($groupId,$companyID){
		    
		      $suppliersGroupList    =  new SuppliersGroupList();

		      $groupMembers          =  $suppliersGroupList->join('tmsw_suppliers_groups','tmsw_suppliers_groups.id','=','tmsw_suppliers_group_lists.group_id')
		                                                        //Get Suppliers name and details
		                                                  ->join('tmsw_client_supplier_details','tmsw_client_supplier_details.id','=','tmsw_suppliers_group_lists.supplier_id')
		                                                  ->where('group_id','=',$groupId)
		                                                  ->where('tmsw_client_supplier_details.company_id',$companyID)
		                                                  ->select('*','tmsw_client_supplier_details.id')
		                                                  ->get();
		      return $groupMembers;
		}

		public function allClientSupplyOrders(){
    	
    	$this->userInit();

    	$breadcrumb  = 'suppliers';


    	$supplies    =	ClientSupplyOrder::where('client_company_id',$this->companyID)
    																		->join('users', 'tmsw_client_supply_orders.client_user_id', '=', 'users.id')
    																		->select('users.*','tmsw_client_supply_orders.*','tmsw_client_supply_orders.status as supply_status','tmsw_client_supply_orders.id as order_id')
    																		->get();

            $accessLevel    =   $this->userAccessLevel();
    	return view('tmsw.client.supply.all-supplies',compact('supplies'))->withBreadcrumb($breadcrumb)->withAccessLevel($accessLevel);
    }

    public function getSupplyOrderParticipants($orderId){
    	 
    	$this->userInit();

    	$breadcrumb  = 'suppliers';

    	$participants    =	SupplyRequest::where('client_supply_order_id',$orderId)
    																		->join('tmsw_client_supply_orders', 'tmsw_client_supply_orders.id', '=', 'tmsw_supply_requests.client_supply_order_id')
    																		->join('tmsw_client_supplier_details', 'tmsw_client_supplier_details.id', '=', 'tmsw_supply_requests.supplier_id')
    																		->where('tmsw_client_supply_orders.client_company_id',$this->companyID)
    																		->select('tmsw_client_supplier_details.*','tmsw_supply_requests.*','tmsw_supply_requests.status as request_status','tmsw_client_supply_orders.expires_at','tmsw_supply_requests.id as request_id')
    																		->get();

      $accessLevel    =   $this->userAccessLevel();
    	return view('tmsw.client.supply.all-supply-participants',compact('participants'))->withBreadcrumb($breadcrumb)->withAccessLevel($accessLevel);
    }

   	public function updateSupplierStatus($supplierId,$statusType){
   		
   		$this->userInit();
   		
   		$where 	=	array('company_id'=>	$this->companyID,
   										'id'				=>	$supplierId
   									);

   		$table 	=	'tmsw_client_supplier_details';

   		switch ($statusType) {
   			case 'activate':
   				$this->activate($where,$table);
   				break;
   			case 'deactivate':
   			   $this->deactivate($where,$table);
   			break;
   		}
	   	return back();
   	}

    public function updateSupplieStatus($supplyId,$statusType){
      
      $this->userInit();
      
      $where  = array('client_company_id' =>  $this->companyID,
                      'id'                =>  $supplyId
                    );

      $table  = 'tmsw_client_supply_orders';

      switch ($statusType) {
        case 'activate':
          $this->activate($where,$table);
          break;
        case 'deactivate':
           $this->deactivate($where,$table);
        break;
      }
      return back();
    }

   	public function updateGroupStatus($groupId,$statusType){
   		
   		$this->userInit();
   		
   		$where 	=	array('company_id'=>	$this->companyID,
   										'id'				=>	$groupId
   									);

   		$table 	=	'tmsw_suppliers_groups';

   		switch ($statusType) {
   			case 'activate':
   				$this->activate($where,$table);
   				break;
   			case 'deactivate':
   			   $this->deactivate($where,$table);
   			break;
   		}

	   	return back();
   	}

   	public function suppliersGroupView($supplierId){
   		
   		$this->userInit();

   		$title 		=	'Suppliers Groups';

    	$breadcrumb  = 'suppliers';

   		$groups 	=	suppliersGroupList::where('supplier_id',$supplierId)
   		 									->where('tmsw_suppliers_groups.company_id',$this->companyID)
   											->join('tmsw_suppliers_groups','tmsw_suppliers_groups.id','=','tmsw_suppliers_group_lists.group_id')
   											->select('tmsw_suppliers_group_lists.*','tmsw_suppliers_groups.*','tmsw_suppliers_groups.id as groupe')
   											->get();

   		$data 		=	array('groups'				=>	$groups,
   											'tableTitle'		=>	$title
   										);

        $accessLevel    =   $this->userAccessLevel();
    	return view('tmsw.client.supply.all-suppliers-group',$data)->withBreadcrumb($breadcrumb)->withAccessLevel($accessLevel);

   	}

   	public function createGroupView(){
    	
    	$suppliers	=		$this->allSuppiers();

    	$breadcrumb	=	'suppliers';

    	$data 			=		array('suppliers'	=>	$suppliers,
    												'group'			=>	null
    												);

        $accessLevel    =   $this->userAccessLevel();
    	return view('tmsw.client.supply.create-supply-group',$data)->withBreadcrumb($breadcrumb)->withAccessLevel($accessLevel);
   	}

   	public function createGroup(Request $request){
   		
   		$validator = Validator::make($request->all(), [
                'group_name' => 'required'
            ]);
        if ($validator->passes()) {
          
         	$this->userInit();

          $groupInsert      =  array( 'groupName' => $request->group_name,
                                      'company_id' =>$this->companyID
                                    );

          $suppliers        =  $request->suppliers;
         
          // Start Transaction
         
          DB::transaction(function()use($groupInsert,$request,$suppliers){
            
            $supplierInsert   =  array();
            
            // Save group and get its Id
            
            $groupId =  SuppliersGroup::insertGetId($groupInsert);
            
            //restructure array to a multidimensional array for mass assignment
            
            if (is_array($suppliers)) {
              foreach ($suppliers as $supplier ) {
                array_push($supplierInsert, array('supplier_id'  => $supplier,
                                                      'group_id' => $groupId
                                                  ));
              }
              $saveGroupList  = SuppliersGroupList::insert($supplierInsert);
            }
          });
            return back()->withMessage('Successful');
        }
   	}

   	public function allGroupsView(){

   		$breadcrumb	=	'suppliers';

   		$title 		=	'All Groups';
   		$groups 	=	$this->getGroups();
   		$data 		=	array('groups'				=>	$groups,
   											'tableTitle'		=>	$title
   										);

        $accessLevel    =   $this->userAccessLevel();
    	return view('tmsw.client.supply.all-suppliers-group',$data)->withBreadcrumb($breadcrumb)->withAccessLevel($accessLevel);
   	}

   	public function editGroupView($groupId){
   		
   		$this->userInit();
   		$breadcrumb	=	'suppliers';
   		$suppliers	=		$this->allSuppiers();

   		$group 			=		SuppliersGroup::where('id',$groupId)
   																	->where('company_id',$this->companyID)
   																	->get()
   																	->first();

   		$selectedSuppliers 	=	suppliersGroupList::where('group_id',$groupId)
   		 																				->where('tmsw_suppliers_groups.company_id',$this->companyID)
   																						->join('tmsw_suppliers_groups','tmsw_suppliers_groups.id','=','tmsw_suppliers_group_lists.group_id')
   																						->pluck('supplier_id');

    	$data 			=		array(
    												'suppliers'					=>	$suppliers,
    												'group'							=>	$group,
    												'selectedSuppliers'	=>	$selectedSuppliers->toArray()
    												);

        $accessLevel    =   $this->userAccessLevel();
   		return view('tmsw.client.supply.create-supply-group',$data)->withBreadcrumb($breadcrumb)->withAccessLevel($accessLevel);

   	}

   	public function editGroup(Request $request,$groupId){
   		
   		$validator = Validator::make($request->all(), [
                'group_name' => 'required'
            ]);
        if ($validator->passes()) {
          
         	$this->userInit();

          $groupInsert      =  array( 'groupName' => $request->group_name,
                                    );

          $suppliers        =  $request->suppliers;
         
          // Start Transaction
         
          DB::transaction(function()use($groupInsert,$request,$suppliers,$groupId){
            
            $supplierInsert   =  array();
            
            // Save group and get its Id
            SuppliersGroup::where('id',$groupId)
            								->where('company_id',$this->companyID)
            								->update($groupInsert);
            
            //restructure array to a multidimensional array for mass assignment
            SuppliersGroupList::where('group_id',$groupId)
            									->delete();

            if (is_array($suppliers)) {
              foreach ($suppliers as $supplier ) {
                array_push($supplierInsert, array('supplier_id'  	=> $supplier,
                                                   'group_id' 		=> $groupId
                                                  ));
              }
              $saveGroupList  = SuppliersGroupList::insert($supplierInsert);
            }
          });
            return back()->withMessage('Successful');
        }else{
        	  return back()->withMessage('unSuccessful');
        }
    }

    public function quoteStatus($supplierQuoteId,$statusType){
    	
    	$this->userInit();
   		
   		$where 	=	array('client_id'=>	$this->companyID,
   										'id'				=>	$supplierQuoteId,

   									);

   		$supply 	=	SupplyRequest::where('tmsw_supply_requests.id',$supplierQuoteId)
   																			->where('client_id',$this->companyID)
   																			->join('tmsw_client_supply_orders','tmsw_client_supply_orders.id','=','tmsw_supply_requests.client_supply_order_id')
   																			->select('expires_at')
   																			->get()
   																			->first();

   		if (strtotime(date("Y-m-d h:i:sa")) < strtotime($supply->expires_at)) {

	   		$table 	=	'tmsw_supply_requests';

	   		switch ($statusType) {
	   			case 'approve':
	   				$this->approve($where,$table);
	   				break;
	   			case 'reject':
	   			   $this->reject($where,$table);
	   			break;
	   		}
    }
		   	return back();
	}

    public function viewPurchaseOrder($id){
        $purchase_order = ClientSupplyOrder::where('id',$id)->value('purchaseOrder');
        $accessLevel    =   $this->userAccessLevel();
        $breadcrumb     =   "View Purchase Order";
        return view('tmsw.client.supply.purchase_order',compact('purchase_order','accessLevel','breadcrumb'));

    }

    public function viewQuote($id,$supply_id){
        $quote = SupplyRequest::where('id',$id)
                        ->where('client_supply_order_id',$supply_id)->value('supply_quotes');
        $accessLevel    =   $this->userAccessLevel();
        $breadcrumb     =   "View Quote";
        return view('tmsw.client.supply.view_quote',compact('quote','accessLevel','breadcrumb'));
    }

	// public function uploadQuote($quoteId){
		
	// 	$this->userInit();

	// 	$validator = Validator::make($request->all(), [
 //                'file' => 'required'
 //            		]);
	// 	if ($validator->passes()) {

	// 		$purchaseOrderFilePath            =  $request->file('file')->store('clientPurchaseOrders');

	// 		$data 				=	array('supply_quotes'=>	$purchaseOrderFilePath,
	// 													'status'			 =>	'4'
	// 												);

	// 		SupplyRequest::where('client_supply_order_id',$quoteId)
	// 									->where('tmsw_client_supplier_details.user_id',$this->userId)
	// 									->join('tmsw_client_supplier_details','tmsw_supply_requests.client_id','=','tmsw_client_supplier_details.company_id')
	// 									->update($data);



	// 	}

	// }
}

