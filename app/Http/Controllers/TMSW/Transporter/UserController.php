<?php

namespace App\Http\Controllers\TMSW\Transporter;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Bank;
use App\TMSW\tmswTransporter;
use App\TMSW\tmswCoverageArea;
use App\TMSW\tmswTransporterCapacity;
use App\User;
use App\TMSW\tmswCompanyUser as CompanyUser;
use Notification;
use App\Notifications\tmsw\newAccount;
use Auth;
use DB;

class UserController extends Controller
{
    

    public $active      =   1; 
    public $inactive    =   2; 

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function userAccessLevel(){
        $role  =   Auth::user()->userAccessLevel;
        switch ($role) {
            case 12:
                return 'administrator';
                break;
            case 13:
                return 'manager';
                break;
            case 14:
                return 'desk_officer';
                break;
            case 15:
                return 'finance';
                break;
            case 16:
                return 'transporter';
                break;
            case 17:
                return 'supplier';
                break;
            default:
            return '/';
            break;
        }
    }


    public function company_id(){
        $user_id    =   Auth::user()->id;
        $company_id =   CompanyUser::where('user_id',$user_id)->value('tmsw_company_detail_id');
        return $company_id;
    }


    private function mt_rand_str_password ($l, $c = 'abcdefghijklmnopqrstuvwxyz1234567890') {
		for ($s = '', $cl = strlen($c)-1, $i = 0; $i < $l; $s .= $c[mt_rand(0, $cl)], ++$i);
		return $s;
    }



    public function getIndex(){
        $breadcrumb = 'Transporter dashboard';
        return view('tmsw.transporter.index')
        ->withBreadcrumb($breadcrumb);
    }


    public function CreateTransporter(){
        $breadcrumb         = 'Transporter dashboard';
        $banks              = Bank::get();
        $coveragearea       = tmswCoverageArea::get();
        $accessLevel    =   $this->userAccessLevel();
        return view('tmsw.transporter.create-transporter')
        ->withBreadcrumb($breadcrumb)
        ->withBanks($banks)
        ->withCoveragearea($coveragearea)
        ->withAccessLevel($accessLevel);

    }


    public function create_transporter_process(Request $request)
    {


          // validate form fields
          $this->validate($request, [
            'name'=>'required',
            'phone_number'=>'required|unique:users,phone',
            'email'=>'required|email|unique:users,email',
            
        ]);


         ////  STARTING DB TRANSACTIONS ///
         DB::beginTransaction();
         try {


         //// INSERTING TO USERS TABLE ////
         $name   =   $request->name;
         $email   =   $request->email;
         $phone   =   $request->phone_number;
         $password   =   $this->mt_rand_str_password(8);
         $encrypt_pass   =   bcrypt($password);
 
         $user                       =   new User;
         $user->name                 =   $name;
         $user->email                =   $email;
         $user->phone                =   $phone;
         $user->password             =   $encrypt_pass;
         $user->default_password     =   0;
         $user->userAccessLevel      =   16; // transporter//
         $user->save();
         $userID = $user->id;

             $user->notify(new newAccount($password));
 
         ///// END OF IT //////////////////

         ///// THIS AREAS IS FOR NOTIFICATIONS///
        // $notification = $user->notify(new User($password));

        /// END OF IT////
        
 
         //// INSERTING TO COMPANIES_USERS TABLE ////
         $user                           =   new CompanyUser;
         $user->user_id                  =   $userID;
         $user->tmsw_company_detail_id   =   $this->company_id();
         $user->tmsw_status_id           =   1;
         $user->save();
          ///// END OF IT //////////////////

        // validate form fields
        $this->validate($request, [
            'name'=>'required|unique:tmsw_transporters,contact_name',
            'phone_number'=>'required|unique:tmsw_transporters,phone',
            'address'=>'required',
            'bank'=>'required',
            'account_number'=>'required',
            'account_name'=>'required',
        ]);

        $phone_number               =   $request->phone_number;
        $address                    =   $request->address;
        $name                       =   $request->name;
        $bank                       =   $request->bank;
        $account_number             =   $request->account_number;
        $account_name               =   $request->account_name;
        $company_name               =   $request->company_name;
        $coverage_area              =   $request->coverageArea;
        $user_id                    =   $userID;

        //retrieve bank_code from banks table
        $bank_object =   Bank::findOrFail($bank);
        $bank_code  =   $bank_object->bank_code;

        //add recipient via paystack api and retrieve recipient code from response

        $result = array();
        //Set other parameters as keys in the $postdata array
        $postdata = array(
            'type' => 'nuban',
            'name' => $name,
            'account_number' => $account_number,
            "bank_code" => $bank_code,
            "currency" => 'NGN',
        );

        $url = "https://api.paystack.co/transferrecipient";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postdata));  //Post Fields
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $headers = [
            //'Authorization: Bearer sk_live_c491d1a0c5b7c8273dad7798c58f727085ec835f',
            'Authorization: Bearer sk_test_28a937c41ad4dda08b5bb33c71fba1b2d5357b3d',
            'Content-Type: application/json'
        ];
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $request = curl_exec($ch);

        curl_close($ch);

        if ($request) {
            $result = json_decode($request, true);
            //retrieve recipient code
        }else{

            return back()->withMessage('An error occurred');
        }


        if($result['status']==true){

            $recipient_code =   $result['data']['recipient_code'];
            //insert into transporters table
            $transporter    =   new tmswTransporter ;

            $transporter->contact_name                      =   $name;
            $transporter->address                           =   $address;
            $transporter->phone                             =   $phone_number;
            $transporter->account_number                    =   $account_number;
            $transporter->account_name                      =   $account_name;
            $transporter->reference_number                  =   $recipient_code;
            $transporter->bank_id                           =   $bank;
            $transporter->user_id                           =   $user_id;
            $transporter->company_name                      =   $company_name;
            $transporter->tmsw_status_id                    =   $this->active;
            $transporter->save();

            $transporter_id =   $transporter->id;

            // $auditmessage    =   "New Transporter Created";
            // $this->audit($auditmessage);

             /*add to idle capacity*/
             for($x=0; $x<count($coverage_area); $x++){
                $tmswTransporterCapacity                               =   new tmswTransporterCapacity;
                $tmswTransporterCapacity->tmsw_transporter_id          =   $transporter_id;
                $tmswTransporterCapacity->tmsw_coverage_area_id        =   $coverage_area[$x];
                $tmswTransporterCapacity->save();
            }
            /*end insert into idle capacities*/
        }

        DB::commit();    // Commiting  ==> There is no problem whatsoever
        return back()->withMessage('Transporter added successfully.');

        } catch (\Exception $e) {
             return $e->getMessage();
        DB::rollback();   // rollbacking  ==> Something went wrong
        return back()->withMessage('Something went wrong');
    }

}


    public function ViewTransporter(){
        $breadcrumb         = 'Transporter dashboard';
        $transporter        = tmswTransporter::get();
        $coveragearea      = tmswCoverageArea::get();
        $accessLevel    =   $this->userAccessLevel();

        $transporterinfo = array();
        foreach($transporter as $rows => $row) {
        $row['status'] = user::where('id',$row->user_id)->value('status');
        $transporterinfo[] = $row;
        }

        return view('tmsw.transporter.view-transporter')
        ->withBreadcrumb($breadcrumb)
        ->withTransporterinfo($transporterinfo)
        ->withCoveragearea($coveragearea)
        ->withAccessLevel($accessLevel);

    }


    public function DeactivateTransporter(Request $request){
        
        $id = $request->id;
        $post = User::find($id);
        $post->status  = $this->inactive;
        $post->save();
        return back()->withMessage('User Deactivated');

   }

   public function ActivateTransporter(Request $request){
       
       $id = $request->id;
       $post = User::find($id);
       $post->status  = $this->active;
       $post->save();
       return back()->withMessage('User Activated');

   }

}
