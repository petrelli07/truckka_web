<?php

namespace App\Http\Controllers\TMSW\Transporter;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\TMSW\tmswRoute as Route;
use App\TMSW\tmswState as State;
use App\TMSW\tmswTruckType as TruckType;
use App\TMSW\tmswOrder as Order;
use App\TMSW\tmswCompanyUser as CompanyUser;
use Auth;
use App\TMSW\tmswTransporter as Transporter;
use App\TMSW\tmswTruckOrder as TruckOrder;
use App\TMSW\tmswInvoice as Invoice;
use Image;

class OrderController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function transporter_id(){
        $transporter_id   =   Transporter::where('user_id',$this->user_id())->value('id');
        return $transporter_id;
    }

    public function check_waybill_duplicate($waybill_number){
        $waybill    =   TruckOrder::where('waybillNumber',$waybill_number)->count();
        if($waybill > 0){
            return true;
        }
    }

    public function company_id(){
        $company_id =   CompanyUser::where('user_id',$this->user_id())->value('tmsw_company_detail_id');
        return $company_id;
    }

    public function user_id(){
        $user_id    =   Auth::user()->id;
        return $user_id;
    }

    public function getRequest(){
        $breadcrumb =   "Truck Requests";
        $user   =   Order::join('tmsw_routes','tmsw_orders.tmsw_route_id','=',
            'tmsw_routes.id')
            ->join('tmsw_states as origin_state','tmsw_routes.origin_state_id','origin_state.id')
            ->join('tmsw_states as destination_state','tmsw_routes.destination_state_id','destination_state.id')
            ->join('tmsw_truck_types','tmsw_orders.tmsw_truck_type_id',
                '=','tmsw_truck_types.id')
            ->join('tmsw_statuses','tmsw_orders.tmsw_status_id',
                '=','tmsw_statuses.id')
            ->select('tmsw_orders.*','tmsw_truck_types.*','origin_state.state_description as originStateName','destination_state.state_description as destStateName','tmsw_statuses.*','tmsw_routes.*')
            ->where('tmsw_orders.tmsw_company_detail_id',$this->company_id())
            ->where('tmsw_orders.tmsw_status_id',9)
            ->get();
        return view('tmsw.transporter.index',compact('user','breadcrumb'));
    }

    public function addTrucks($reference_number){
        $breadcrumb =   "Add Trucks";
        $order_id   =   Order::where('reference_number',$reference_number)->value('id');
        $truck_number   =   Order::where('reference_number',$reference_number)->value('truck_number');
        return view('tmsw.transporter.add-truck',compact('order_id','truck_number','breadcrumb'));
    }

    public function addTruckDetails(Request $request){

        $this->validate($request, [
            'truck_plate_number.*'=>'required',
            'driver_name.*'=>'required',
            'driver_phone.*'=>'required',
            'order_id'=>'required',
        ]);

        $transporter_id =   $this->transporter_id();
        //$truck_number   =   $request->truck_number;


            /**/

                $plate_number               =   $request->truck_plate_number;
                $driver_name                =   $request->driver_name;
                $driver_phone               =   $request->driver_phone;
                $order_id                   =    $request->order_id;

        for($x=0; $x < count($plate_number); $x++){
                $truck_detail                                               =   new TruckOrder;
                $truck_detail->plateNumber                                  =   $plate_number[$x];
                $truck_detail->driverName                                   =   $driver_name[$x];
                $truck_detail->driverPhone                                  =   $driver_phone[$x];
                $truck_detail->tmsw_order_id                                =   $order_id;
                $truck_detail->tmsw_transporter_id                          =   $transporter_id;
                $truck_detail->tmsw_status_id                               =   11;
                $truck_detail->save();
            }

            /*Order::where('id',$order_id)
                ->update(
                    array(
                        'tmsw_status_id' => 10,
                    )
                );*/

            return redirect('/tmsw/transporter/home')->with('message','Truck Details Entered Successfully');
    }

    public function activeOrders(){
        $breadcrumb =   "Truck Requests";
        $user       =   TruckOrder::join('tmsw_orders','tmsw_truck_orders.tmsw_order_id','tmsw_orders.id')
            ->join('tmsw_routes','tmsw_orders.tmsw_route_id','=',
            'tmsw_routes.id')
            ->join('tmsw_states as origin_state','tmsw_routes.origin_state_id','origin_state.id')
            ->join('tmsw_states as destination_state','tmsw_routes.destination_state_id','destination_state.id')
            ->join('tmsw_truck_types','tmsw_orders.tmsw_truck_type_id',
                '=','tmsw_truck_types.id')
            ->join('tmsw_statuses','tmsw_orders.tmsw_status_id',
                '=','tmsw_statuses.id')
            ->select('tmsw_orders.*','tmsw_truck_types.*','origin_state.state_description as originStateName','destination_state.state_description as destStateName','tmsw_statuses.*','tmsw_routes.*','tmsw_truck_orders.*')
            ->where('tmsw_transporter_id',$this->transporter_id())
            ->get();

        return view('tmsw.transporter.payment-history',compact('user','breadcrumb'));
    }

    public function uploadWaybill($id){
        $waybill_number    =   TruckOrder::where('id',$id)->value('waybillNumber');
        $breadcrumb =   "Upload Waybill";
        if($waybill_number == Null){
            return view('tmsw.transporter.add_waybill',compact('breadcrumb','id'));
        }else{
            return back()->with('message','Waybill Already Submitted');
        }
    }

    public function submitWaybill(Request $request){
        $this->validate($request, [
            'waybill_number'=>'required',
            'waybill_file'=>'required',
        ]);

        $waybillnumber      =   $request->waybill_number;
        $truck_order_id     =   $request->truck_order_id;
        $waybillFile        =   $request->file('waybill_file');

        $check_duplicate    =   $this->check_waybill_duplicate($waybillnumber);

        if(!$check_duplicate){

            $originalImage_waybillfile  = $waybillFile;
            $thumbnailImage_waybillfile  = Image::make($originalImage_waybillfile);
            $originalPath_waybillnumber    = public_path().'/tmsw/waybills/';
            $thumbnailImage_waybillfile->save($originalPath_waybillnumber.$waybillnumber.$originalImage_waybillfile->getClientOriginalName());

            $waybillupload  =   $originalImage_waybillfile->getClientOriginalName();

            TruckOrder::where('id',$truck_order_id)
                ->update(
                    array(
                        'waybillFile'               => $waybillupload,
                        'waybillNumber'             => $waybillnumber,
                        'tmsw_status_id'            => 12,
                    )
                );

            $invoice    =   new Invoice;
            $invoice->invoice_reference     =   $this->invoice_reference();
            $invoice->tmsw_truck_order_id   =   $truck_order_id;
            $invoice->tmsw_status_id        =   14;
            $invoice->save();

            return redirect('/tmsw/transporter/home')->with('message','An Invoice for Payment has been Created');
        }else{
            return back()->with('message','Duplicate Waybill Number');
        }

    }

    public function invoices(){
        $user  =   Invoice::join('tmsw_invoices','')
            ->join('tmsw_orders','tmsw_truck_orders.tmsw_order_id','tmsw_orders.id')
            ->join('tmsw_routes','tmsw_orders.tmsw_route_id','=',
                'tmsw_routes.id')
            ->join('tmsw_states as origin_state','tmsw_routes.origin_state_id','origin_state.id')
            ->join('tmsw_states as destination_state','tmsw_routes.destination_state_id','destination_state.id')
            ->join('tmsw_truck_types','tmsw_orders.tmsw_truck_type_id',
                '=','tmsw_truck_types.id')
            ->join('tmsw_statuses','tmsw_orders.tmsw_status_id',
                '=','tmsw_statuses.id')
            ->select('tmsw_orders.*','tmsw_truck_types.*','origin_state.state_description as originStateName','destination_state.state_description as destStateName','tmsw_statuses.*','tmsw_routes.*','tmsw_truck_orders.*')
            ->where('tmsw_transporter_id',$this->transporter_id())
            ->get();
    }

    public function invoiceDetail($id){
       $invoice    = Invoice::where('tmsw_invoices.tmsw_truck_order_id',$id)
                        ->join('tmsw_truck_orders as orders','tmsw_invoices.tmsw_truck_order_id','orders.id')
                        ->join('tmsw_transporters','orders.tmsw_transporter_id','tmsw_transporters.id')
                        ->join('banks','tmsw_transporters.bank_id','banks.id')
                        ->join('tmsw_orders','orders.tmsw_order_id','tmsw_orders.id')
                        ->join('tmsw_routes','tmsw_orders.tmsw_route_id','=',
                            'tmsw_routes.id')
                        ->join('tmsw_states as origin_state','tmsw_routes.origin_state_id','origin_state.id')
                        ->join('tmsw_states as destination_state','tmsw_routes.destination_state_id','destination_state.id')
                        ->join('tmsw_truck_types','tmsw_orders.tmsw_truck_type_id',
                            '=','tmsw_truck_types.id')
                        ->join('tmsw_statuses','tmsw_invoices.tmsw_status_id',
                            '=','tmsw_statuses.id')
                        ->select('tmsw_orders.*','tmsw_truck_types.*','origin_state.state_description as originStateName','destination_state.state_description as destStateName','tmsw_statuses.*','tmsw_routes.*','orders.*','tmsw_invoices.*','tmsw_transporters.*','banks.*')
                    ->get();
        $breadcrumb =   "Invoice Details";
        return view('tmsw.transporter.invoice_details',compact('breadcrumb','invoice'));
    }

    public function invoice_reference(){
        $i = 1;
        for($j=0; $j < $i; $j++){
            $result = '';
            $length = 7;
            for($x = 0; $x < $length; $x++) {
                $result .= mt_rand(0, 9);
            }

            $checkAvailable = Invoice::where('invoice_reference', $result);
            if( $checkAvailable->count() < 1 ){
                return $result;
                break;
            }

            $i++;
        }
    }
}
