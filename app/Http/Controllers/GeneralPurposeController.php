<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Validator;
use Charts;
use DB, App\User, App\ServiceRequest, App\UserComapany, App\simpleInvoice; 



class GeneralPurposeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function changePass(){
            return view('passwords.resetPassword');
    }

    /*public function resetPassword(Request $request){

        $userPassword = Auth::user()->password;
        $id = Auth::user()->id;
        $type       =       User::where('id','=',$id)->get()[0];
        $newPassword = bcrypt($request->password);

        $validator = Validator::make($request->all(), [
            'password' => 'required|string|min:6|confirmed',
        ]); 

        if($validator->passes()){

        $changeDefaultPassword = array(
            'default_password' => "0",
            'password'=>$newPassword,
        );

        User::where('id',$id)->update($changeDefaultPassword);
            $redircect =       '';
            // use switch case later

            if ($type->userAccessLevel == 0) {
                $redirect = 'home1';
            }else if ($type->userAccessLevel == 1) {
                $redirect =  'clientHome';
            }
            else if ($type->userAccessLevel == 2) {
                $redirect =  'carrier';
            }
            else if ($type->userAccessLevel == 3) {
                $redirect =    'all_suppliers_supply_orders';
            }

            return  redirect('/'.$redirect);

        }else{
            return    redirect('/');
        }

    }*/

    public function resetPassword(Request $request){

    $userPassword = Auth::user()->password;
    $id = Auth::user()->id;
    $type = User::where('id','=',$id)->get()[0];
    $newPassword = bcrypt($request->password);

    $validator = Validator::make($request->all(), [
    'password' => 'required|string|min:6|confirmed',
    ]); 

    if($validator->passes()){

    $changeDefaultPassword = array(
    'default_password' => "0",
    'password'=>$newPassword,
    );

    User::where('id',$id)->update($changeDefaultPassword);
    $redircect = '';
    // use switch case later

    if ($type->userAccessLevel == 0) {
    $redirect = 'home1';
    }else if ($type->userAccessLevel == 1) {
    $redirect = 'clientHome';
    }
    else if ($type->userAccessLevel == 2) {
    $redirect = 'carrier';
    }
    else if ($type->userAccessLevel == 3) {
    $redirect = 'all_suppliers_supply_orders';
    }

    return redirect('/'.$redirect);

    }else{
    return redirect('/');
    }

    }

    public function performance()
    {

        $userID             = Auth::user()->id;

        $companyID          = UserComapany::where('user_id',$userID)->value('company_id');

        $orderDetails       = DB::table('service_requests')
                                    ->join('user_comapanies', 'user_comapanies.user_id', '=', 'service_requests.createdBy')
                                    ->where('user_comapanies.company_id','=',$companyID)
                                    // ->select('haulage_resource_requests.*', 'service_requests.*')
                                    ->get();

        $averageCostPerDay  =  DB::table('simple_invoices')
                                    ->where('company_id', $companyID)
                                    ->get();


        $paid               =  simpleInvoice::where('company_id',$companyID)
                                    ->where('status',1)->get();

        $unpaid             =  simpleInvoice::where('company_id',$companyID)
                                    ->where('status',0)->get();

        $pie_chart          = Charts::create('pie', 'highcharts')
                                    ->title('Invoices')
                                    ->labels(['Paid Invoices', 'Unpaid Invoices'])
                                    ->values([count($paid),count($unpaid)])
                                    ->dimensions(1000,500)
                                    ->responsive(true);



        $chart              = Charts::database($orderDetails, 'bar', 'highcharts')
                                        ->title('Orders')
                                        ->elementLabel("Total Orders")
                                        ->GroupByDay();

        $costPerDay         = Charts::database($averageCostPerDay, 'line', 'highcharts')
                                        ->title('Average Cost Per Day')
                                        ->elementLabel("Average Cost Per Day")
                                        ->GroupByDay();

        return view('client.performance.performance',compact('chart','pie_chart','costPerDay'));
    }

    public function unauthorized()
    {
            return view('unauthorized');
    }

    public function viewMap(){
        return view('trackerdemo');
    }

    public function testInvoice(){
        return view('client.finance.invoice_new');
    }
    
  public function conInv()
  {
      return view('client.finance.consolidated_inv');
  }

}
