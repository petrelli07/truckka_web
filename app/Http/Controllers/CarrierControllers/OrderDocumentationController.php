<?php
namespace App\Http\Controllers\CarrierControllers;


use Illuminate\Http\Request;
use App\serviceRequest; 
use App\ResourceGpsDetail; 
use App\OrderResourceGpsDetail; 
use Auth;
use App\OrderDocument;
use App\OrderDocDest;
use App\CarrierCreditNote;
use App\InvoiceDetail;
use Validator;
use App\Http\Controllers\Controller;

class OrderDocumentationController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function allOrders(){
    	$createdByID = Auth::user()->id;
        $allRequests = serviceRequest::where('createdBy',$createdByID)->paginate(10);
        $countRequests = serviceRequest::where('createdBy',$createdByID)->count();
        return view('documents.allOrdersDocs',['allRequests'=>$allRequests,'countRequests'=>$countRequests]);
    }

    public function getResourceDetails($res){
    	$resourceDetails = array();
    	for($i=0; $i<count($res); $i++){
    		$resourceDetails[] = ResourceGpsDetail::where('id',$res[$i])->get();
    	}
    	return $resourceDetails;
    }

    public function viewOrderResources($serviceIDNo){
    	$service_id = serviceRequest::where('serviceIDNo', $serviceIDNo)->value('id'); 
    	$orderResources =  OrderResourceGpsDetail::where('service_requests_id', $service_id)->get();
    	$res = array();
    	foreach ($orderResources as $order) {
    		$res[] =$order->resource_gps_details_id;
    	}
    	$resourceDetail = $this->getResourceDetails($res);
    	return view('documents.OrderResources', ['resourceDetail'=>$resourceDetail, 'service_id'=>$service_id]);
    }

    public function saveOrderDocuments(Request $request){
    	
    	$orderDocument = $request->file('orderDocument');
    	$service_id = $request->service_id;
    	$timeIn = $request->timeIn;
    	$timeOut = $request->timeOut;
    	$cargoWeight = $request->cargoWeight;
    	$cargoDescription = $request->cargoDescription;
    	$comments = $request->comments;
    	$gpsid = $request->gpsid;
    	
    	$orderDocumentdest = $request->file('orderDocumentdest');
    	$timeIndest = $request->timeIndest;
    	$timeOutdest = $request->timeOutdest;
    	$cargoWeightdest = $request->cargoWeightdest;
    	$cargoDescriptiondest = $request->cargoDescriptiondest;
    	$commentsdest = $request->commentsdest;
    	$gpsiddest = $request->gpsiddest;

    	$user_id = Auth::user()->id;

    	if($request->hasFile('orderDocument')){

			$validator = Validator::make($request->all(), [
	            'timeIn.*'=>'required',
	            'timeOut.*'=>'required',
	            'cargoWeight.*'=>'required',
	            'cargoDescription.*'=>'required',
	            'gpsid.*'=>'required',
	            'orderDocument.*'=>'required|file|mimes:jpeg,jpg,pdf',

	            'timeIndest.*'=>'required',
	            'timeOutdest.*'=>'required',
	            'cargoWeightdest.*'=>'required',
	            'cargoDescriptiondest.*'=>'required',
	            'gpsiddest.*'=>'required',
	            'orderDocumentdest.*'=>'required|file|mimes:jpeg,jpg,pdf'
        	]);

        	
        	if($validator->passes()){
				
	    	for($i=0; $i<count($orderDocument); $i++){
	    		$path = $request->file('orderDocument')[$i]->store('ordersDocs');
	    		$orderDoc = new OrderDocument;
	    		$orderDoc->service_id = $service_id;
	    		$orderDoc->user_id = $user_id;
	    		$orderDoc->fileName = $path;
	    		$orderDoc->timeIn = $timeIn[$i];
	    		$orderDoc->timeOut = $timeOut[$i];
	    		$orderDoc->cargoWeight = $cargoWeight[$i];
	    		$orderDoc->cargoDescription = $cargoDescription[$i];
	    		$orderDoc->comments = $comments[$i];
	    		$orderDoc->GPSID = $gpsid[$i];
	    		$orderDoc->mode = 1;
	    		$saveUpload = $orderDoc->save();

	    		$pathdest = $request->file('orderDocumentdest')[$i]->store('ordersDocumentDest');
	    		$orderDocdest = new OrderDocDest;
	    		$orderDocdest->service_id = $service_id;
	    		$orderDocdest->user_id = $user_id;
	    		$orderDocdest->fileName = $pathdest;
	    		$orderDocdest->timeIn = $timeIndest[$i];
	    		$orderDocdest->timeOut = $timeOutdest[$i];
	    		$orderDocdest->cargoWeight = $cargoWeightdest[$i];
	    		$orderDocdest->cargoDescription = $cargoDescriptiondest[$i];
	    		$orderDocdest->comments = $commentsdest[$i];
	    		$orderDocdest->GPSID = $gpsiddest[$i];
	    		$orderDocdest->mode = 1;
	    		$saveUploadDest = $orderDoc->save();
	    	}
    	
		    if ($saveUpload && $saveUploadDest) {
		    	
		    	$data = array(
		            'carrierDocStatus' => 1
		        );

		        serviceRequest::where(
	            'id',$service_id)
	            ->update($data);

	            $invoiceDetailsID = InvoiceDetail::where('service_requests_id',$service_id)->value('id');

	            $createCreditNote = new CarrierCreditNote;
	            $createCreditNote->service_requests_id = $service_id;
	            $createCreditNote->invoice_details_id = $invoiceDetailsID;
	            $createCreditNote->save();
		        
		        return response()->json(['success'=>"Upload Successful"]);

		    }else{
		    	return response()->json(['error'=>"Something went wrong with the upload"]);
		    }

        	}else{
        		return response()->json(['error'=>$validator->errors()->all()]);
        	}

    	}else{
	    	return response()->json(['error'=>"Please Upload a file for each order"]);
	    }
    }

}
