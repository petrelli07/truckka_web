<?php

namespace App\Http\Controllers\CarrierControllers;

use App\Http\Controllers\Controller;
use App\carrierResource;
use Illuminate\Http\Request;
use App\User;
use Auth;
use App\serviceRequest;
use App\Invoice;
use App\TimeLog;
use App\CarrierInvoice;
use DB;
use Validator;
use App\HaulageResourceRequest;
use App\ResourceTypeNumber;
use File;
use App\ResourceGpsDetail;
use App\OrderResourceGpsDetail;
use Notification;
use Notify;
use App\Notifications\HaulageInProgress;
use App\orderToken;
use App\InvoiceDetail;
use App\CarrierRouteResourcePrice;
use App\Notifications\newClientInvoice;

class ServiceRequestController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /*public function changePass(){
        return view('passwords.changePassword');
    }*/

    public function randomNumber(){

        $i = 1;

        for($j=0; $j < $i; $j++){
//            return random_int(1,10000);

            $result = '';

            $length = 7;

            for($x = 0; $x < $length; $x++) {
                $result .= mt_rand(0, 9);
            }

            $checkAvailable = serviceRequest::where('serviceIDNo', $result);

            if( $checkAvailable->count() < 1 ){

                return $result;

                break;
            }
            $i++;

        }
    }

    public function resetPassword(Request $request){

        $userPassword = Auth::user()->password;
        $id = Auth::user()->id;
        $newPassword = bcrypt($request->password);

        $validator = Validator::make($request->all(), [
            'password' => 'required|string|min:6|confirmed',
        ]); 

        if($validator->passes()){

        $changeDefaultPassword = array(
            'default_password' => "0",
            'password'=>$newPassword,
        );
        User::where('id',$id)->update($changeDefaultPassword);
            return response()->json(['success'=>'Password Reset Successful']);
        }else{
            return response()->json(['error'=>$validator->errors()->all()]);
        }

    }


    public function testfn($test){

        $joinRes = array();
        for($i = 0; $i < count($test); ++$i){//get resource details

            $joinRes[$i] = DB::table('service_requests')->where('id', '=', $test[$i])
                ->get();
        }
        return $joinRes;
    }

    public function allServiceRequests(){
        $user_id = Auth::user()->id;
        $haulageRequests = HaulageResourceRequest::where('carrier_id', $user_id)->get();

        $arr = array();

        foreach($haulageRequests as $carr){//get carrier ids from origin destyination map
            $arr[] = [ $carr->service_request_id ];
        }

        $haulageRequests =  $this->testfn($arr);
        return view('carrier.resourceRequests.allRequests', ['haulageRequests'=>$haulageRequests]);
    }

    public function viewOrder($id){

        $orderDetails = serviceRequest::where('serviceIDNo',$id)->get();

        foreach ($orderDetails as $k) {
            $origin = $k->deliverFrom;
            $destination = $k->deliverTo;
        }

        $orderID = serviceRequest::where('serviceIDNo',$id)->value('id');
        $carrier_id = Auth::user()->id;

        $requiredResources = HaulageResourceRequest::where('service_request_id',$orderID)->where('carrier_id',$carrier_id)->value('resourceTypeNumber');

        $HaulageResourceRequestStatus = HaulageResourceRequest::where('service_request_id',$orderID)->where('carrier_id',$carrier_id)->value('status');

        $jsonDecodeVal  = json_decode($requiredResources,1);
        
        $resourceType = $jsonDecodeVal['resourceType'];
        $resourceNumber = $jsonDecodeVal['resourceNumber'];

        $price = CarrierRouteResourcePrice::where('carrier_id',$carrier_id)->where('origin',$origin)->where('destination',$destination)->where('resourceType',$resourceType)->value('price');
        $finalPrice = $price * $resourceNumber;

        $loggedInUserID = Auth::user()->id;

        $resourceGpsDetails15 = ResourceGpsDetail::where('user_id', $loggedInUserID)->where('resourceType','15ton')->where('resourceStatus',0)->get();
        $resourceGpsDetails30 = ResourceGpsDetail::where('user_id', $loggedInUserID)->where('resourceType','30ton')->where('resourceStatus',0)->get();
        $resourceGpsDetails40 = ResourceGpsDetail::where('user_id', $loggedInUserID)->where('resourceType','40ton')->where('resourceStatus',0)->get();

        $findID = serviceRequest::find($orderID);

        $orderResourceRequest = $findID->ResourceTypeNumber()->get();

        if($orderDetails){
            return view('orders.orderDetails', ['orderDetails'=>$orderDetails, 'orderResourceRequest'=>$orderResourceRequest,'resourceType'=>$resourceType,'resourceNumber'=>$resourceNumber, 'finalPrice'=>$finalPrice,'resourceGpsDetails15'=>$resourceGpsDetails15, 'resourceGpsDetails30'=>$resourceGpsDetails30,'resourceGpsDetails40'=>$resourceGpsDetails40,'HaulageResourceRequestStatus'=>$HaulageResourceRequestStatus]);
        }else{
            return redirect('/viewOrders')->with('message', 'Order Not Found');
        }

    }

    public function creditNote($id)
    {
        $orderDetails = serviceRequest::where('serviceIDNo',$id)->get();
        $orderID = serviceRequest::where('serviceIDNo',$id)->value('id');
        //$price = serviceRequest::where('serviceIDNo',$id)->value('id');

        $fileContent = InvoiceDetail::where('service_requests_id',$orderID)->value('orderDetail');

        $decodedFileContent = json_decode($fileContent,1);

        $amt = $decodedFileContent['amountPlusVat'];
        $amtless = $amt * 0.15;

        $price = $amt - $amtless;

        if($orderDetails){
            return view('carrier.creditNote', ['orderDetails'=>$orderDetails,'price'=>$price]);
        }else{
            return redirect('/viewOrders')->with('message', 'Order Not Found');
        }
    }

    public function updateResouceStatus($resourceID){
        for($i=0; $i<count($resourceID); $i++){
            $dataresourceGpsDetails = array(
                'resourceStatus' => 1
                );

                $resourceGpsDetails = ResourceGpsDetail::where(
                'id',$resourceID[$i])
                ->update($dataresourceGpsDetails);
        }
                
    }

    public function confirmRequest(Request $request){

        $user_id = Auth::user()->id;

        $service_id_no = $request->serviceIDNo;

        $resourceGpsDetails = $request->resourceGpsId;
        $resourceGpsDetailsCount = count($resourceGpsDetails);

        $validator = Validator::make($request->all(), [

            'resourceGpsId'=>'required',

        ]);

        $carrier_id = Auth::user()->id;

        $orderID = serviceRequest::where('id',$service_id_no)->value('id');

        $SIDNO = serviceRequest::where('id',$service_id_no)->value('serviceIDNo');

        $carrierResources = carrierResource::where('user_id',$user_id)->value('resourcetypeNumber');

        $jsonDecodeRTN = json_decode($carrierResources,1);

        $rest = $jsonDecodeRTN[1]['resourceNumber'];


        $emptyArr = array();
        $emptyArr2 = array();

        for($b=0;$b<count($jsonDecodeRTN);$b++){
            $emptyArr[] = $jsonDecodeRTN[$b];
        }

        $resourceTypeNumber = ResourceTypeNumber::where('id',$service_id_no)->get();

        foreach($resourceTypeNumber as $rt){
            $type = $rt->resourceType;
            $num = $rt->resourceNumber;
        }
        
        foreach ($emptyArr as $key => $entry) {
            if ($entry['resourceType'] == "30ton") {
                $jsonDecodeRTN[$key]['resourceNumber'] = $rest - $num;
            }
        }

        $reencode = json_encode($jsonDecodeRTN);

        $updateCRN = array(
            'resourcetypeNumber' => $reencode
        );

        $updateCRNAction= carrierResource::where('user_id',$user_id)
            ->update($updateCRN);

        if($validator->passes()){

            $resourceID = array();

            for($i = 0; $resourceGpsDetailsCount > $i; $i++){

                $OrderResourceGpsDetail = new OrderResourceGpsDetail;
                $OrderResourceGpsDetail->service_requests_id = $service_id_no;
                $OrderResourceGpsDetail->resource_gps_details_id = $resourceGpsDetails[$i];
                $OrderResourceGpsDetail->save();

                $resourceID[] = $resourceGpsDetails[$i];
            }

            $this->updateResouceStatus($resourceID);

            $dataHR = array(
                'status' => 1
            );

            $updateHaulageResourceRequest = HaulageResourceRequest::where(
            'service_request_id',$orderID)->where('carrier_id',$carrier_id)
            ->update($dataHR);

                
            $haulReq = HaulageResourceRequest::where('service_request_id',$orderID)->get();

            $getStat = array();

            for ($i=0; $i < count($haulReq); $i++) { 
                $getStat[] = $haulReq[$i]['status'];
            }

            if (count(array_unique($getStat)) == 1) {

                $data = array(
                    'orderStatus' => 2
                );

                $updateEscalation = serviceRequest::where(
                    'id',$service_id_no)
                    ->update($data);

                $this->createInvoiceForClient($SIDNO);

                return response()->json(['success'=>'Order confirmed']);
                
            }else{
                return response()->json(['success'=>'Order confirmed']);
            }

        }else{
            return response()->json(['error'=>$validator->errors()->all()]);
        }

    }

    public function generateToken($id){
        $serviceId = serviceRequest::where('serviceIDNo',$id)->get();
        return view('carrier.token',['serviceId'=>$serviceId]);
    }

    public function createInvoiceForClient($id){

        $serviceID = serviceRequest::where('serviceIDNo',$id)->value('id');

        //$serviceIDCreatedBy = serviceRequest::where('serviceIDNo',$id)->value('createdBy');

        $user_id = serviceRequest::where('serviceIDNo',$id)->value('createdBy');

        $rand = $this->randomNumber();

        $serviceReq = DB::table("invoices")->insert([
            'invoice_id' => $rand,
            'service_id' => $serviceID,
            'client_id' => $user_id,
            'status' => 0,
            'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
            'updated_at' => \Carbon\Carbon::now()->toDateTimeString(),
        ]);

        $data = array(
            'orderStatus' => 3
        );

        $updateStatus = serviceRequest::where(
            'serviceIDNo',$id)
            ->update($data);

        /*if($serviceReq && $updateStatus){*/

            $user = User::find($user_id);

            $notification = $user->notify(new newClientInvoice($id));

            /*return back()->with('message', 'Invoice sent successfully');
        }else{
            return back()->with('message', 'Something went wrong');
        }*/

    }



}
