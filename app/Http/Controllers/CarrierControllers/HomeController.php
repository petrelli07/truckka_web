<?php

namespace App\Http\Controllers\CarrierControllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB; 
use Illuminate\Http\Request;
use Auth;
use App\User;
use App\CarrierOrderResourceGpsDetail;
use App\serviceRequest;
use App\CarrierCompanyWallet;
use App\CarrierCompanyInvoice;
use App\CarrierUser;
use App\CarrierHaulageResourceRequest;
use Paginate;


class HomeController extends Controller
{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function userSummary($company_id){

		$orders             =   DB::table('carrier_order_resource_gps_details')
															->where('carrier_id',$company_id)
															->distinct()
															->get(['service_requests_id'])
															->count();

		$completedOrders    =   DB::table('carrier_invoices')
															->where('carrier_id',$company_id)
															->distinct()
															->get(['service_id'])
															->count();
															
		$incompleted        =   $orders - $completedOrders;

		$walletBalance      =   CarrierCompanyWallet::where('company_id',$company_id)
																								->orderBy('created_at', 'desc')
																								->first();
											
		$data               =   array(  'allOrders'             =>  $orders,
										'completedOrders'       =>  $completedOrders,
										'incompletedOrders'     =>  $incompleted,
										'walletBalance'         =>  $walletBalance
									 );
		
		return $data;
	}

	public function getOrderDetails($order_id){
		$orderDetails = array();
		foreach ($order_id as $order_id) {
			array_push($orderDetails, serviceRequest::where('id',$order_id->service_requests_id)->get()[0]);
		}
		return $orderDetails;
	}
	
	public function index(){
		$userID         =   Auth::user()->id;
		$company_id 		= 	CarrierUser::where('user_id',$userID)
																		->value('company_id');
		$userSummary    =   $this->userSummary($company_id);
		$order_id       =   [];

		/*$OrderResourceGpsDetail = DB::table('carrier_order_resource_gps_details')
								->where('carrier_id',$company_id)
								->distinct()
								->get(['service_requests_id']);
		
		$orders 			= $this->getOrderDetails($OrderResourceGpsDetail);*/
		$orders 		=	CarrierHaulageResourceRequest::where('carrier_id',$company_id)
			            ->join('service_requests', 'carrier_haulage_resource_requests.service_request_id', '=', 'service_requests.id')
			            ->select('carrier_haulage_resource_requests.*', 'service_requests.*')
			            ->get();
		
		return view('carrier.index',compact('orders','userSummary'));
	}

	public function map(){
        return view('carrierTrackerDemo');
    }

	public function viewOrderDetails($serviceIDNo)
	{  
		$serviceId 		= serviceRequest::where('serviceIDNo',$serviceIDNo)->value('id');
		$userID 			= Auth::user()->id;
		$company_id 	= CarrierUser::where('user_id',$userID)->value('company_id');
		$orderDetails = serviceRequest::where('serviceIDNo',$serviceIDNo)->get();
		$finalResDets = CarrierOrderResourceGpsDetail::where('service_requests_id',$serviceId)->where('carrier_id',$company_id)->get();

			return view('carrier.orders.singleOrder', ['orderDetails'=>$orderDetails,'finalResDets'=>$finalResDets]);        
	}

	public function transactionWallet(){

		$userID                     =   Auth::user()->id;
		$company_id 								= 	CarrierUser::where('user_id',$userID)->value('company_id');
		$walletTransactions         =   CarrierCompanyWallet::where('company_id',$company_id)
																												->join('carrier_company_invoices', 'carrier_company_invoices.id', '=', 'carrier_company_wallets.invoice_id')
																												->get();
		$data                       =   array('walletTransactions' => $walletTransactions);                          
		return view('carrier.orders.walletTrans',$data);
	}

	public function viewInvoice($reqInvoice)
	{

		$userID                     =   Auth::user()->id;
		$company_id 								= 	CarrierUser::where('user_id',$userID)->value('company_id');

		$invoice_id                 =   $reqInvoice;
		$invoiceDetails             =   CarrierCompanyInvoice::where('carrier_company_invoices.invoice_id',$invoice_id)
										->where('carrier_company_invoices.carrier_id',$company_id)
										->join('service_requests', 'carrier_invoices.service_id', '=', 'service_requests.id')
										->join('carrier_wallets', 'carrier_invoices.id', '=', 'carrier_wallets.invoice_id')
										->get();
		$data                       =   array('details' => $invoiceDetails);
		// var_dump($invoiceDetails);
		// exit;
		return view('carrier.finance.invoice_new',$data);                   
	}

	public function allHaulageRequests()
	{
		$userID 	= Auth::user()->id;
		$company_id = CarrierUser::where('user_id',$userID)->value('company_id');

		$requests 	=	CarrierHaulageResourceRequest::where('carrier_id',$company_id)
			            ->join('service_requests', 'carrier_haulage_resource_requests.service_request_id', '=', 'service_requests.id')
			            ->select('carrier_haulage_resource_requests.*', 'service_requests.*')
			            ->get();
		return 	view('carrier.orders.resourceRequest',compact('requests'));
	}

	public function orderDetails($id)
	{
		
		$userID 	= Auth::user()->id;
		$company_id = CarrierUser::where('user_id',$userID)->value('company_id');
		$order_id 	= serviceRequest::where('serviceIDNo',$id)->value('id');

		$orderDetails = CarrierHaulageResourceRequest::where('carrier_id',$company_id)
			            ->join('service_requests', 'carrier_haulage_resource_requests.service_request_id', '=', 'service_requests.id')
			            ->select('carrier_haulage_resource_requests.*', 'service_requests.*')
			            ->get();

		return 	view('carrier.orders.singleOrder',compact('orderDetails'));
	}



	
}
