<?php

namespace App\Http\Controllers\CarrierControllers;

use Illuminate\Http\Request;
use App\IncidentReport;
use App\IncidentReportDetail;
use Auth;
use App\ServiceRequest;
use Validator;
use DB;
use App\ResourceGpsDetail;
use App\OrderResourceGpsDetail;
use App\Http\Controllers\Controller;

class IncidentManagementController extends Controller
{

    public function randomNumber(){

        $i = 1;

        for($j=0; $j < $i; $j++){
//            return random_int(1,10000);

            $result = '';

            $length = 7;

            for($x = 0; $x < $length; $x++) {
                $result .= mt_rand(0, 9);
            }

            $checkAvailable = serviceRequest::where('serviceIDNo', $result);

            if( $checkAvailable->count() < 1 ){

                return $result;

                break;
            }
            $i++;

        }
    }

    public function logIncident(Request $request){

        $serviceIDNo = $request->serviceNo;
        $description = $request->description;
        $selectResource = $request->selectResource;
        $status = 0;



        $validator = Validator::make($request->all(), [
            'serviceNo'=>'required',
            'description'=>'required',
            'selectResource'=>'required',
        ]);

        if($validator->passes()){

            $serviceIDNoCheck = ServiceRequest::where('serviceIDNo', $serviceIDNo)->get();

            foreach($serviceIDNoCheck as $service){
                $clientID = $service->createdBy;
            }

            $serviceIDNoCheckCount = count($serviceIDNoCheck);
            $userID = Auth::user()->id;

            if($serviceIDNoCheckCount > 0){
                $rand = $this->randomNumber();
                $incidentTitle = $serviceIDNo.' - '.$selectResource.' - '.$rand;
                $incident_report_id = DB::table("incident_reports")->insertGetId([
                    'incidentReportIDNo' => $rand,
                    'service_request_id' => $serviceIDNo,
                    'incident_title' => $incidentTitle,
                    'clientID' => $clientID,
                    'createdBy' => $userID,
                    'status' => $status,
                    'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
                    'updated_at' => \Carbon\Carbon::now()->toDateTimeString(),
                ]);

                if($incident_report_id){

                    $incident_report_details = DB::table("incident_report_details")->insert([
                        'incident_report_id' => $incident_report_id,
                        'remarks' => $description,
                        'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
                        'updated_at' => \Carbon\Carbon::now()->toDateTimeString(),
                    ]);

                    if($incident_report_details){
                        return response()->json(['success'=>'Incident logged Successfully']);
                    }else{

                        return response()->json(['error'=>"Something went wrong!"]);

                    }

                }else{

                    return response()->json(['error'=>"Something went wrong!"]);
                }

            }else{
                return response()->json(['error'=>"Order ID No Does Not Exist!"]);
            }

        }else{

            return response()->json(['error'=>$validator->errors()->all()]);

        }
    }

    public function viewAllIncidents(){
         $userID = Auth::user()->id;
        $incidents = IncidentReport::where('createdBy', $userID)->get();
        return view('incidentManagement.allIncidents', ['incidents'=>$incidents]);
    }

    public function createNewIncident(Request $request){
            $serviceIDNo = $request->serviceRequestNo;
        $description = $request->description;
        $status = 0;

        $serviceIDNoCheck = ServiceRequest::where('serviceIDNo', $serviceIDNo)->value('id');

        $resDet = array();

        $foo =  OrderResourceGpsDetail::where('service_requests_id', $serviceIDNoCheck)->get();

        foreach($foo as $foos){
            $resDet[] = $foos->resource_gps_details_id;
        }

        $gpsDet = array();

        for($i=0; count($resDet) > $i; $i++){
            $gpsDet[] = ResourceGpsDetail::where('id',$resDet[$i])->get();
        }

        return response()->json([$gpsDet]);
    }

    public function viewIncidentThread($id){
        $incidentReportDetail = IncidentReportDetail::where('incident_report_id', $id)->get();
        $incidentReport = IncidentReport::where('id', $id)->get();

        return view('carrier.incidentManagement.incidentThread',['incidentReportDetail'=>$incidentReportDetail, 'incidentReport'=>$incidentReport]);
    }

    public function updateIncidentThread($id){
        $incidentReport = IncidentReport::where('id',$id)->value('id');
        $incidentReportID = IncidentReport::where('id',$id)->get();
        return view('incidentManagement.updateIncidentThread', ['incidentReportID'=>$incidentReportID, 'incidentReport'=>$incidentReport]);
    }

    public function editIncidentThread(Request $request){

        $reportID = $request->reportID;
        $incidentComment = $request->incidentComment;


        $update_incident_report_details = DB::table("incident_report_details")->insert([
            'incident_report_id' => $reportID,
            'remarks' => $incidentComment,
            'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
            'updated_at' => \Carbon\Carbon::now()->toDateTimeString(),
        ]);

        if($update_incident_report_details){
            return response()->json(['success'=>'Incidence Thread Updated']);
        }else{
            return response()->json(['success'=>'An error occurred']);
        }


    }

    public function closeIncidentThread($id){

        $data = array(
            'status' => 1
        );

        $closeThread = IncidentReport::where(
            'id',$id)
            ->update($data);

        if($closeThread){
            return back()->with('message', 'Incident Closed');
        }else{
            return back()->with('message', 'Something Went wrong');
        }

    }

}
