<?php
namespace App\Http\Controllers\CarrierControllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB; 
use Illuminate\Http\Request;
use Auth;
use Validator;
use App\User;
use App\CarrierUser;
use App\ServiceRequest;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class ordersController extends BaseController{

	public function allOrders(){


		$userID 		= Auth::user()->id;
		$company_id = CarrierUser::where('user_id',$userID)->value('company_id');
		$data				=	array();
		$orders 		=	'';

		$orders 		=	DB::table('carrier_haulage_resource_requests')
                        ->join('service_requests as order','order.id','=','carrier_haulage_resource_requests.service_request_id')
                        ->select('*',DB::raw( "( SELECT count(*) FROM order_resources WHERE order_resources.order_id = order.id AND carrier_haulage_resource_requests.carrier_id =".$company_id." ) as resource_number"))

                        // ->join('carrier_resource_details as resource_details','resource_details.id','=','carrier_haulage_resource_requests.resource_details_id')
                        ->where('carrier_haulage_resource_requests.carrier_id',$company_id)
                        ->groupBy('service_request_id')
                        ->get();
     // var_dump($orders);
     // exit;
   	$data['orders']	=	$orders;
   	return view('carrier.orders.myOrders',$data);

	}

	public function addResourceTo(Request $request,$singleOrder = NULL){
		
		$userID 		= Auth::user()->id;
		$carrierId 	= CarrierUser::where('user_id',$userID)->value('company_id');    
		

      $allOrders      		=  $request->orderId;

	    $allOrders       		=  array($singleOrder);


			
	  	$getResources 					= 	DB::table('carrier_resource_details')
                                  ->select('carrier_resource_details.*', 'capacity.capacity_value')
                                  ->leftJoin('resource_capacity as capacity','capacity.id','=','carrier_resource_details.capacity')
                                  ->where('company_id',$carrierId)
                                  ->where('resourceStatus','=','0')
                                  ->get();


	  	$getOrders							=	 	DB::table('carrier_haulage_resource_requests')
		  														->select('*')

		  														->selectRaw("( SELECT GROUP_CONCAT(resource_id SEPARATOR ',') FROM order_resources WHERE order_resources.order_id = order.id AND carrier_haulage_resource_requests.carrier_id =".$carrierId." ) as attachResource")

		  														->join('carrier_resource_details','carrier_resource_details.company_id','=','carrier_haulage_resource_requests.carrier_id')

		  														->join('service_requests as order','order.id','=','carrier_haulage_resource_requests.service_request_id')

		  														->where('carrier_haulage_resource_requests.status','=', '0')

		  														->where(function ($query) use ($allOrders){

		  															 for ($i=0; $i < sizeof($allOrders); $i++) { 
																		  	if ($i > 0) {
																		  			$query->orwhere('carrier_haulage_resource_requests.service_request_id', '=', $allOrders[$i]);
																		  	}else{
																		  			$query->where('carrier_haulage_resource_requests.service_request_id', '=', $allOrders[$i]);
																		  	}
																		  }
		  														})
		  														->where('carrier_haulage_resource_requests.carrier_id','=',$carrierId)
		  														->groupBy('serviceIDNo')
	  															->get();
	 

	  $data	=		array('resources' 	=> $getResources,
	  								'orders'	 		=>	$getOrders
										);
	 	
		return view('carrier.orders.addResourceTo',$data);
}

	public function reachedLimit($orderId){
		
		$userID 						= Auth::user()->id;
		$companyId 					= CarrierUser::where('user_id',$userID)->value('company_id');
		
		$attachedResources 	=	DB::table('order_resources')
														->join('carrier_resource_details','carrier_resource_details.id','=','order_resources.resource_id')
														->where('company_id',$companyId)
														->where('order_id',$orderId)
														->get();

		$limit 							=		DB::table('carrier_haulage_resource_requests')
														->where('carrier_id','=',$companyId)
														->where('service_request_id','=',$orderId)
														->get();

		if (sizeof($attachedResources) < $limit[0]->resource_limit) {
			return false;
		}
			return true;
	}

	public function attachResource(Request $request){
			$validator = Validator::make($request->all(), [
            'resources'     	=>  'required',
            'orderId'        	=>  'required',
           
      ]);

		 if ($validator->passes()) {

					$userID 						= Auth::user()->id;
					$company_id 				= CarrierUser::where('user_id',$userID)->value('company_id');        
					$resources 					=	$request->resources;

        for ($i=0; $i < sizeof($resources) ; $i++) { 
	        
	        if(!$this->reachedLimit($request->orderId)) {
		        $resourceDetails  	= array(
		                                  'resource_id'  						=>  $resources[$i],
		                                  'order_id'								=>  $request->orderId,
		                                  'created_at'							=> \Carbon\Carbon::now()->toDateTimeString()
		                                );  

		        $attachResource     = DB::table('order_resources')
		                                ->insert($resourceDetails);
		        if ($attachResource) {

		        	 $dataStatus = array(
	                        'orderStatus' => 9
	                    );

	            ServiceRequest::where('id',$request->orderId)
	                					->update($dataStatus);

		         echo 'success';
		        }
	        }

        }
      }
	}

}