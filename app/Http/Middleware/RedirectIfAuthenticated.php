<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;


class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {

        if (Auth::guard($guard)->check()) {
            $role = Auth::user()->userAccessLevel;

            switch ($role) {
                case 0:
                    return '/home';
                    break;
                case 1:
                    return '/clientHome';
                    break;
                case 2:
                    return '/all-orders';
                    break;
                case 3:
                    return '/all_suppliers_supply_orders';
                    break;
                case 4:
                    return 'exchange/customer/home';
                    break;
                case 5:
                    return 'exchange/truck_owner_driver/home';
                    break;
                case 6:
                    return 'exchange/aggregator/home';
                    break;
                case 7:
                    return 'registry/fieldOps/home';
                    break;
                case 8:
                    return 'registry/finops-home';
                    break;
                case 9:
                    return 'registry/home-mgt';
                    break;
                case 10;
                    return 'exchange/admin/home';
                case 11;
                    return 'registry/administrator/home';
                default:
                    return '/';
                    break;
            }
            //return redirect('/home1');
        }

        return $next($request);
    }
}
