<?php
namespace App\Http\Middleware;

use Closure;
use Auth;
use DB;
class permissionChecker
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next){
        $userId             = Auth::user()->id;
        $controllerPointer  = explode("@", $request->route()->getAction()['controller']); 
        $namespace          = $controllerPointer[0].'\\'.$controllerPointer[1];
        
        $query              = DB::table('user_permissions')
                              ->join('modules','modules.id','=','user_permissions.module_id')
                              ->where('user_permissions.user_id', '=', $userId)
                              ->where('modules.moduleMethod','=',$namespace)
                              ->count();

        if ($query < 1) {
            return redirect('/');
        }
        // var_dump($request);
        // exit;
        // if (Auth::user()->userAccessLevel != 0) {
        // }
        return $next($request);
    }
}
