<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use DB;
use App\ClientActivityLog;
use App\UserComapany;

class Client
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->userAccessLevel != 1) {
            return redirect('/unauthorized');
        }

        $userID         = Auth::user()->id;
        $name           = Auth::user()->name;

        $cID            = UserComapany::where('user_id',$userID)->value('company_id');

        $companyId      =   DB::table('user_comapanies')
                            ->where('user_id', '=', $userID)
                            ->select('company_id')
                            ->get();


        $activity = new ClientActivityLog;

        $activity->company_id   = $cID;
        $activity->user_id      = $userID;
        $activity->description  = $name." logged in";
        $activity->save();

        session(['companyId' => $companyId[0]->company_id]);


        return $next($request);
    }
}
