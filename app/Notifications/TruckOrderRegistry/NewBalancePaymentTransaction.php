<?php

namespace App\Notifications\TruckOrderRegistry;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NewBalancePaymentTransaction extends Notification
{
    use Queueable;

    protected $waybillnumber;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($waybill_number)
    {
        $this->waybillnumber    =   $waybill_number;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                        ->line('Acknowledged!')
                        ->line('Your request for balance payment for waybill number: '.$this->waybillnumber.' has been received')
                        ->line('Regards,')
                        ->line('Truckka');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
