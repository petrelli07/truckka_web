<?php

namespace App\Notifications\tmsw;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\tmsw\tmswOrder as Order;

class newOrder extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    protected $reference_number;

    public function __construct($reference_number)
    {
        $this->reference_number = $reference_number;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $order  =   Order::join('tmsw_routes','tmsw_orders.tmsw_route_id','=',
            'tmsw_routes.id')
            ->join('tmsw_states as origin_state','tmsw_routes.origin_state_id','origin_state.id')
            ->join('tmsw_states as destination_state','tmsw_routes.destination_state_id','destination_state.id')
            ->join('tmsw_truck_types','tmsw_orders.tmsw_truck_type_id',
                '=','tmsw_truck_types.id')
            ->join('tmsw_statuses','tmsw_orders.tmsw_status_id',
                '=','tmsw_statuses.id')
            ->select('tmsw_orders.*','tmsw_truck_types.*','origin_state.state_description as originStateName','destination_state.state_description as destStateName','tmsw_statuses.*','tmsw_routes.*')
            ->where('tmsw_orders.reference_number',$this->reference_number)
            ->get();

        foreach($order as $orders){
            $origin = $orders['originStateName'];
            $destination = $orders['destStateName'];
            $number_of_trucks = $orders['truck_number'];
            $contact_phone = $orders['contact_phone'];
        }

        return (new MailMessage)
            ->line('Alert!')
            ->line('A new order was just created with the following details:')
            ->line('Origin:'.$origin)
            ->line('Destination:'.$destination)
            ->line('Number of Trucks:'.$number_of_trucks)
            ->line('Contact Phone:'.$contact_phone);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
