<?php

use Illuminate\Database\Seeder;

class UniqueTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        DB::table('unique_users_type')->insert([
            'typeName' => 'Manager'
        ]);
    }
}
