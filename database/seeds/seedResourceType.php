<?php

use Illuminate\Database\Seeder;

class seedResourceType extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        DB::table('resource_capacity')->insert([
            'capacity_value' => '30 Tons'
        ]);
    }
}
