<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //$this->call(status::class);
        $this->call(UsersTableSeeder::class);
        $this->call(seedResourceType::class);
        $this->call(UniqueTableSeeder::class);
        $this->call(UniqueSeeder::class);
    }
}
