<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyHaulageResourceRequests extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('haulage_resource_requests', function (Blueprint $table) {
            $table->string('resourceTypeNumber', 150)->nullable()->change();
            $table->integer('resource_limit');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('haulage_resource_requests', function (Blueprint $table) {
            //
        });
    }
}
