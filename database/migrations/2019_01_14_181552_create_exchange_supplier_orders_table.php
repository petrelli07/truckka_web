<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExchangeSupplierOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exchange_supplier_orders', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('user_id')
                ->unsigned()
                ->index();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->integer('exchange_order_id')
                ->unsigned()
                ->index();
            $table->foreign('exchange_order_id')
                ->references('id')
                ->on('exchange_orders');

            $table->integer('truck_supplier_id')
                ->unsigned()
                ->index();
            $table->foreign('truck_supplier_id')
                ->references('id')
                ->on('truck_suppliers')
                ->onDelete('cascade');

            $table->integer('exchange_status_id')
                ->unsigned()
                ->index();
            $table->foreign('exchange_status_id')
                ->references('id')
                ->on('exchange_statuses')
                ->onDelete('cascade');

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exchange_supplier_orders');
    }
}
