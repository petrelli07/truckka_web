<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTmswTruckOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tmsw_truck_orders', function (Blueprint $table) {
            $table->increments('id');

            $table->string('waybillFile')->nullable();
            $table->string('waybillNumber')->nullable();
            $table->string('plateNumber');
            $table->string('driverName');
            $table->string('driverPhone');
            $table->string('imei')->nullable();

            $table->integer('tmsw_order_id')
                ->unsigned()
                ->index();
            $table->foreign('tmsw_order_id')
                ->references('id')
                ->on('tmsw_orders')
                ->onDelete('cascade');

            $table->integer('tmsw_transporter_id')
                ->unsigned()
                ->index();
            $table->foreign('tmsw_transporter_id')
                ->references('id')
                ->on('tmsw_transporters')
                ->onDelete('cascade');

            $table->integer('tmsw_status_id')
                ->unsigned()
                ->index();
            $table->foreign('tmsw_status_id')
                ->references('id')
                ->on('tmsw_statuses')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tmsw_truck_orders');
    }
}
