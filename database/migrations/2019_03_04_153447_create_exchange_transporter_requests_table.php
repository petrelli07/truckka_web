<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExchangeTransporterRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exchange_transporter_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->text('transporter_name');
            $table->string('transporter_email');
            $table->string('transporter_phone');
            $table->string('transporter_route');

            $table->integer('exchange_status_id')
                ->unsigned()
                ->index();
            $table->foreign('exchange_status_id')
                ->references('id')
                ->on('exchange_statuses')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exchange_transporter_requests');
    }
}
