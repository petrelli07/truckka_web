<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSpotOrderIdToSpotOrderPayments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('spot_order_payments', function (Blueprint $table) {
            $table->integer('spot_order_id')->unsigned()->after('user_id');
            $table->foreign('spot_order_id')->references('id')->on('spot_orders');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('spot_order_payments', function (Blueprint $table) {
            $table->dropForeign('spot_order_payments_spot_order_id_foreign');
            $table->dropColumn('spot_order_id');
        });
    }
}
