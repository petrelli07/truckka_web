<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarrierOrderResourceGpsDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carrier_order_resource_gps_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('carrier_id')->unsigned()->index();
            $table->foreign('carrier_id')->references('id')->on('carrier_company_details')->onDelete('cascade');
            $table->string('plateNumber');
            $table->string('carrierName');
            $table->string('driverName');
            $table->integer('resource_details_id')->unsigned()->index();
            $table->foreign('resource_details_id')->references('id')->on('carrier_resource_details')->onDelete('cascade');
            $table->integer('service_requests_id')->unsigned()->index();
            $table->foreign('service_requests_id')->references('id')->on('service_requests')->onDelete('cascade');
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carrier_order_resource_gps_details');
    }
}
