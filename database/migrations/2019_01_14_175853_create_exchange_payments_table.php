<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExchangePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exchange_payments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('reference_number');
            $table->decimal('amount',10,2);

            $table->integer('user_id')
                ->unsigned()
                ->index();
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->integer('exchange_order_id')
                ->unsigned()
                ->index();
            $table->foreign('exchange_order_id')
                ->references('id')
                ->on('exchange_orders')
                ->onDelete('cascade');

            $table->integer('exchange_status_id')
                ->unsigned()
                ->index();
            $table->foreign('exchange_status_id')
                ->references('id')
                ->on('exchange_statuses')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exchange_payments');
    }
}
