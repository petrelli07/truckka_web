<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTruckOwnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('truck_owners', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('truck_owner_status_id')
                ->unsigned()
                ->index();
            $table->foreign('truck_owner_status_id')
                ->references('id')
                ->on('truck_owner_statuses')
                ->onDelete('cascade');

            $table->integer('field_agent_id')
                ->unsigned()
                ->index();
            $table->foreign('field_agent_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->integer('finance_officer_id')
                ->unsigned()
                ->index()->nullable();
            $table->foreign('finance_officer_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->integer('bank_id')
                ->unsigned()
                ->index()->nullable();
            $table->foreign('bank_id')
                ->references('id')
                ->on('banks')
                ->onDelete('cascade');

            $table->string('truck_owner_name');
            $table->string('truck_owner_address');
            $table->string('truck_owner_phone');
            $table->string('account_number');
            $table->string('account_name');
            $table->string('recipient_code')->nullable();
            //$table->string('bank_name');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('truck_owners');
    }
}
