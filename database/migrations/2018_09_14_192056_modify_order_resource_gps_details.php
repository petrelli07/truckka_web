<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyOrderResourceGpsDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_resource_gps_details', function (Blueprint $table) {
            $table->string('plateNumber', 150)->nullable()->change();
            $table->string('carrierName', 150)->nullable()->change();
            $table->string('driverName', 150)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_resource_gps_details', function (Blueprint $table) {
            //
        });
    }
}
