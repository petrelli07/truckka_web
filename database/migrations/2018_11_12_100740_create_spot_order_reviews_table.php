<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpotOrderReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spot_order_reviews', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('spot_order_id')
                    ->unsigned()
                    ->index();
            $table->foreign('spot_order_id')
                    ->references('id')
                    ->on('spot_orders')
                    ->onDelete('cascade');

            $table->integer('spot_order_review_status_id')
                    ->unsigned()
                    ->index();
            $table->foreign('spot_order_review_status_id')
                    ->references('id')
                    ->on('spot_order_review_statuses')
                    ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spot_order_reviews');
    }
}
