<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientSupplyOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_supply_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('client_user_id');
            $table->integer('client_company_id')->unsigned()->index();
            $table->foreign('client_company_id')->references('id')->on('companies')->onDelete('cascade');
            $table->integer('supplier_id')->unsigned()->index();
            $table->foreign('supplier_id')->references('id')->on('users')->onDelete('cascade');
            $table->text('orderDescription');
            $table->float('orderQuantity',10,2);
            $table->string('purchaseOrder');
            $table->integer('status')->default(0);
            $table->date('expires_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_supply_orders');
    }
}
