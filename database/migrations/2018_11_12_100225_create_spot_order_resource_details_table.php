<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpotOrderResourceDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spot_order_resource_details', function (Blueprint $table) {
            $table->increments('id');
            $table->string('driverName');
            $table->string('driverPhoneNumber');
            $table->string('plateNumber');
            $table->string('trackerImei')->nullable();

            $table->integer('spot_order_resource_type_id')
                    ->unsigned()
                    ->index();
            $table->foreign('spot_order_resource_type_id')
                    ->references('id')
                    ->on('spot_order_resource_types')
                    ->onDelete('cascade');

            $table->integer('sord_status_id')
                    ->unsigned()
                    ->index();

            $table->foreign('sord_status_id')
                    ->references('id')
                    ->on('sord_statuses')
                    ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spot_order_resource_details');
    }
}
