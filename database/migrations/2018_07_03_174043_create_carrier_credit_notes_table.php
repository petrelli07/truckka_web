<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarrierCreditNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carrier_credit_notes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('service_requests_id')->unsigned()->index();
            $table->foreign('service_requests_id')->references('id')->on('service_requests')->onDelete('cascade');
            $table->integer('invoice_details_id')->unsigned()->index();
            $table->foreign('invoice_details_id')->references('id')->on('invoice_details')->onDelete('cascade');
            $table->integer('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carrier_credit_notes');
    }
}
