<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResourceTypeNumbersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resource_type_numbers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('resourceType');
            $table->string('resourceNumber');
            $table->integer('service_request_id')->unsigned()->index();
            $table->foreign('service_request_id')->references('id')->on('service_requests')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resource_type_numbers');
    }
}
