<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExchangeOrderTruckDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exchange_order_truck_details', function (Blueprint $table) {

            $table->increments('id');
            $table->string('plate_number');
            $table->string('driver_name');
            $table->string('driver_phone_number');
            $table->string('tracker_imei')->nullable();
            $table->string('vehicle_license')->nullable();
            $table->string('drivers_license')->nullable();
            $table->string('vehicle_insurance')->nullable();
            $table->string('road_worthiness_permit')->nullable();


            $table->integer('exchange_order_id')
                ->unsigned()
                ->index();
            $table->foreign('exchange_order_id')
                ->references('id')
                ->on('exchange_orders');

            $table->integer('truck_supplier_id')
                ->unsigned()
                ->index();
            $table->foreign('truck_supplier_id')
                ->references('id')
                ->on('truck_suppliers')
                ->onDelete('cascade');

            $table->integer('exchange_truck_type_id')
                ->unsigned()
                ->index();
            $table->foreign('exchange_truck_type_id')
                ->references('id')
                ->on('exchange_truck_types')
                ->onDelete('cascade');

            $table->integer('exchange_status_id')
                ->unsigned()
                ->index();
            $table->foreign('exchange_status_id')
                ->references('id')
                ->on('exchange_statuses')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exchange_order_truck_details');
    }
}
