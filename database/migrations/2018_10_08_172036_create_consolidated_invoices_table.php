<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConsolidatedInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consolidated_invoices', function (Blueprint $table) {

            $table->increments('id');

            $table->longText('waybillFiles');

            $table->integer('company_id')
                  ->unsigned()
                  ->index();

            $table->foreign('company_id')
                  ->references('id')
                  ->on('companies')
                  ->onDelete('cascade');

            $table->integer('invoiceNo');
            $table->longText('resourceDetails');
            $table->integer('status')->default(0);
            $table->longText('price');
            $table->text('order_ids');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consolidated_invoices');
    }
}
