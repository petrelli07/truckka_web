<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpotOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spot_orders', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('order_number');
            $table->longText('item_description');
            $table->date('pickup_date');
            $table->integer('weight');
            

            $table->integer('spot_order_resource_type_id')
                    ->unsigned()
                    ->index();
            $table->foreign('spot_order_resource_type_id')
                    ->references('id')
                    ->on('spot_order_resource_types')
                    ->onDelete('cascade');


            $table->integer('user_id')
                    ->unsigned()
                    ->index();
            $table->foreign('user_id')
                    ->references('id')
                    ->on('users')
                    ->onDelete('cascade');

            $table->integer('spot_order_route_pricing_id')
                    ->unsigned()
                    ->index();
            $table->foreign('spot_order_route_pricing_id')
                    ->references('id')
                    ->on('spot_order_route_pricings')
                    ->onDelete('cascade');

            $table->integer('spot_order_status_id')
                    ->unsigned()
                    ->index();
            $table->foreign('spot_order_status_id')
                    ->references('id')
                    ->on('spot_order_statuses')
                    ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spot_orders');
    }
}
