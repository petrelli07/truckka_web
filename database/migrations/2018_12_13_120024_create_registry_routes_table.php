<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegistryRoutesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registry_routes', function (Blueprint $table) {
            $table->increments('id');

            $table->text('origin');
            $table->text('destination');
            $table->string('price');

            $table->integer('registry_route_status_id')
                ->unsigned()
                ->index();
            $table->foreign('registry_route_status_id')
                ->references('id')
                ->on('registry_route_statuses')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registry_routes');
    }
}
